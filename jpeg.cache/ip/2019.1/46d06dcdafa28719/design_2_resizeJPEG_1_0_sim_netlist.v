// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (lin64) Build 2552052 Fri May 24 14:47:09 MDT 2019
// Date        : Sat Sep 25 00:43:00 2021
// Host        : alex-HP-Compaq-8200-Elite-CMT-PC running 64-bit Ubuntu 20.04.2 LTS
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_2_resizeJPEG_1_0_sim_netlist.v
// Design      : design_2_resizeJPEG_1_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7z020clg484-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_2_resizeJPEG_1_0,resizeJPEG_v1_0,{}" *) (* DowngradeIPIdentifiedWarnings = "yes" *) (* X_CORE_INFO = "resizeJPEG_v1_0,Vivado 2019.1" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (s00_axis_aclk,
    m00_axis_aclk,
    s00_axis_aresetn,
    s00_axis_tready,
    s00_axis_tdata,
    s00_axis_tstrb,
    s00_axis_tlast,
    s00_axis_tvalid,
    M_AXIS_TUSER,
    m00_axis_aresetn,
    m00_axis_tvalid,
    m00_axis_tdata,
    m00_axis_tstrb,
    m00_axis_tlast,
    m00_axis_tready);
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 s00_axis_aclk CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME s00_axis_aclk, ASSOCIATED_RESET s00_axis_aresetn, ASSOCIATED_BUSIF saxis, FREQ_HZ 200000000, PHASE 0.000, CLK_DOMAIN design_2_processing_system7_0_0_FCLK_CLK0, INSERT_VIP 0" *) input s00_axis_aclk;
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 m00_axis_aclk CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME m00_axis_aclk, ASSOCIATED_RESET m00_axis_aresetn, ASSOCIATED_BUSIF maxis, FREQ_HZ 200000000, PHASE 0.000, CLK_DOMAIN design_2_processing_system7_0_0_FCLK_CLK0, INSERT_VIP 0" *) input m00_axis_aclk;
  (* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 s00_axis_aresetn RST" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME s00_axis_aresetn, POLARITY ACTIVE_LOW, INSERT_VIP 0" *) input s00_axis_aresetn;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 saxis TREADY" *) output s00_axis_tready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 saxis TDATA" *) input [31:0]s00_axis_tdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 saxis TSTRB" *) input [3:0]s00_axis_tstrb;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 saxis TLAST" *) input s00_axis_tlast;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 saxis TVALID" *) input s00_axis_tvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 saxis TUSER" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME saxis, TDATA_NUM_BYTES 4, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 48, HAS_TREADY 1, HAS_TSTRB 1, HAS_TKEEP 0, HAS_TLAST 1, FREQ_HZ 200000000, PHASE 0.000, CLK_DOMAIN design_2_processing_system7_0_0_FCLK_CLK0, LAYERED_METADATA undef, INSERT_VIP 0" *) input [47:0]M_AXIS_TUSER;
  (* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 m00_axis_aresetn RST" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME m00_axis_aresetn, POLARITY ACTIVE_LOW, INSERT_VIP 0" *) input m00_axis_aresetn;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 maxis TVALID" *) output m00_axis_tvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 maxis TDATA" *) output [31:0]m00_axis_tdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 maxis TSTRB" *) output [3:0]m00_axis_tstrb;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 maxis TLAST" *) output m00_axis_tlast;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 maxis TREADY" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME maxis, TDATA_NUM_BYTES 4, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 1, HAS_TKEEP 0, HAS_TLAST 1, FREQ_HZ 200000000, PHASE 0.000, CLK_DOMAIN design_2_processing_system7_0_0_FCLK_CLK0, LAYERED_METADATA undef, INSERT_VIP 0" *) input m00_axis_tready;

  wire \<const0> ;
  wire \<const1> ;
  wire [47:0]M_AXIS_TUSER;
  wire [24:0]\^m00_axis_tdata ;
  wire s00_axis_aclk;
  wire s00_axis_aresetn;
  wire [31:0]s00_axis_tdata;

  assign m00_axis_tdata[31] = \<const0> ;
  assign m00_axis_tdata[30] = \<const0> ;
  assign m00_axis_tdata[29] = \<const0> ;
  assign m00_axis_tdata[28] = \<const0> ;
  assign m00_axis_tdata[27] = \<const0> ;
  assign m00_axis_tdata[26] = \<const0> ;
  assign m00_axis_tdata[25] = \<const0> ;
  assign m00_axis_tdata[24:0] = \^m00_axis_tdata [24:0];
  assign s00_axis_tready = \<const1> ;
  GND GND
       (.G(\<const0> ));
  VCC VCC
       (.P(\<const1> ));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_resizeJPEG_v1_0 inst
       (.M_AXIS_TUSER(M_AXIS_TUSER[16]),
        .m00_axis_tdata(\^m00_axis_tdata ),
        .s00_axis_aclk(s00_axis_aclk),
        .s00_axis_aresetn(s00_axis_aresetn),
        .s00_axis_tdata(s00_axis_tdata[23:0]));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_resizeJPEG_v1_0
   (m00_axis_tdata,
    s00_axis_aresetn,
    M_AXIS_TUSER,
    s00_axis_tdata,
    s00_axis_aclk);
  output [24:0]m00_axis_tdata;
  input s00_axis_aresetn;
  input [0:0]M_AXIS_TUSER;
  input [23:0]s00_axis_tdata;
  input s00_axis_aclk;

  wire [7:0]BLUE;
  wire [7:0]GREEN;
  wire \GREEN[7]_i_1_n_0 ;
  wire [0:0]M_AXIS_TUSER;
  wire [7:0]RED;
  wire [24:0]m00_axis_tdata;
  wire [8:1]m00_axis_tdata_r2;
  wire [7:0]m00_axis_tdata_r21_out;
  wire [8:8]m00_axis_tdata_r21_out__0;
  wire [0:0]m00_axis_tdata_r2__0;
  wire [7:1]m00_axis_tdata_r3;
  wire [8:0]m00_axis_tdata_r3__0;
  wire \m00_axis_tdata_r[11]_i_2_n_0 ;
  wire \m00_axis_tdata_r[11]_i_3_n_0 ;
  wire \m00_axis_tdata_r[11]_i_4_n_0 ;
  wire \m00_axis_tdata_r[11]_i_5_n_0 ;
  wire \m00_axis_tdata_r[15]_i_2_n_0 ;
  wire \m00_axis_tdata_r[15]_i_3_n_0 ;
  wire \m00_axis_tdata_r[15]_i_4_n_0 ;
  wire \m00_axis_tdata_r[15]_i_5_n_0 ;
  wire \m00_axis_tdata_r[16]_i_1_n_0 ;
  wire \m00_axis_tdata_r[19]_i_2_n_0 ;
  wire \m00_axis_tdata_r[19]_i_3_n_0 ;
  wire \m00_axis_tdata_r[19]_i_4_n_0 ;
  wire \m00_axis_tdata_r[19]_i_5_n_0 ;
  wire \m00_axis_tdata_r[23]_i_2_n_0 ;
  wire \m00_axis_tdata_r[23]_i_3_n_0 ;
  wire \m00_axis_tdata_r[23]_i_4_n_0 ;
  wire \m00_axis_tdata_r[23]_i_5_n_0 ;
  wire \m00_axis_tdata_r[24]_i_1_n_0 ;
  wire \m00_axis_tdata_r[3]_i_2_n_0 ;
  wire \m00_axis_tdata_r[3]_i_3_n_0 ;
  wire \m00_axis_tdata_r[3]_i_4_n_0 ;
  wire \m00_axis_tdata_r[3]_i_5_n_0 ;
  wire \m00_axis_tdata_r[7]_i_2_n_0 ;
  wire \m00_axis_tdata_r[7]_i_3_n_0 ;
  wire \m00_axis_tdata_r[7]_i_4_n_0 ;
  wire \m00_axis_tdata_r[7]_i_5_n_0 ;
  wire \m00_axis_tdata_r[8]_i_1_n_0 ;
  wire \m00_axis_tdata_r_reg[11]_i_1_n_0 ;
  wire \m00_axis_tdata_r_reg[11]_i_1_n_1 ;
  wire \m00_axis_tdata_r_reg[11]_i_1_n_2 ;
  wire \m00_axis_tdata_r_reg[11]_i_1_n_3 ;
  wire \m00_axis_tdata_r_reg[15]_i_1_n_0 ;
  wire \m00_axis_tdata_r_reg[15]_i_1_n_1 ;
  wire \m00_axis_tdata_r_reg[15]_i_1_n_2 ;
  wire \m00_axis_tdata_r_reg[15]_i_1_n_3 ;
  wire \m00_axis_tdata_r_reg[19]_i_1_n_0 ;
  wire \m00_axis_tdata_r_reg[19]_i_1_n_1 ;
  wire \m00_axis_tdata_r_reg[19]_i_1_n_2 ;
  wire \m00_axis_tdata_r_reg[19]_i_1_n_3 ;
  wire \m00_axis_tdata_r_reg[23]_i_1_n_0 ;
  wire \m00_axis_tdata_r_reg[23]_i_1_n_1 ;
  wire \m00_axis_tdata_r_reg[23]_i_1_n_2 ;
  wire \m00_axis_tdata_r_reg[23]_i_1_n_3 ;
  wire \m00_axis_tdata_r_reg[3]_i_1_n_0 ;
  wire \m00_axis_tdata_r_reg[3]_i_1_n_1 ;
  wire \m00_axis_tdata_r_reg[3]_i_1_n_2 ;
  wire \m00_axis_tdata_r_reg[3]_i_1_n_3 ;
  wire \m00_axis_tdata_r_reg[7]_i_1_n_0 ;
  wire \m00_axis_tdata_r_reg[7]_i_1_n_1 ;
  wire \m00_axis_tdata_r_reg[7]_i_1_n_2 ;
  wire \m00_axis_tdata_r_reg[7]_i_1_n_3 ;
  wire s00_axis_aclk;
  wire s00_axis_aresetn;
  wire [23:0]s00_axis_tdata;
  wire [3:1]\NLW_m00_axis_tdata_r_reg[16]_i_2_CO_UNCONNECTED ;
  wire [3:0]\NLW_m00_axis_tdata_r_reg[16]_i_2_O_UNCONNECTED ;
  wire [3:1]\NLW_m00_axis_tdata_r_reg[24]_i_2_CO_UNCONNECTED ;
  wire [3:0]\NLW_m00_axis_tdata_r_reg[24]_i_2_O_UNCONNECTED ;
  wire [3:1]\NLW_m00_axis_tdata_r_reg[8]_i_2_CO_UNCONNECTED ;
  wire [3:0]\NLW_m00_axis_tdata_r_reg[8]_i_2_O_UNCONNECTED ;

  FDRE \BLUE_reg[0] 
       (.C(s00_axis_aclk),
        .CE(\GREEN[7]_i_1_n_0 ),
        .D(s00_axis_tdata[0]),
        .Q(BLUE[0]),
        .R(1'b0));
  FDRE \BLUE_reg[1] 
       (.C(s00_axis_aclk),
        .CE(\GREEN[7]_i_1_n_0 ),
        .D(s00_axis_tdata[1]),
        .Q(BLUE[1]),
        .R(1'b0));
  FDRE \BLUE_reg[2] 
       (.C(s00_axis_aclk),
        .CE(\GREEN[7]_i_1_n_0 ),
        .D(s00_axis_tdata[2]),
        .Q(BLUE[2]),
        .R(1'b0));
  FDRE \BLUE_reg[3] 
       (.C(s00_axis_aclk),
        .CE(\GREEN[7]_i_1_n_0 ),
        .D(s00_axis_tdata[3]),
        .Q(BLUE[3]),
        .R(1'b0));
  FDRE \BLUE_reg[4] 
       (.C(s00_axis_aclk),
        .CE(\GREEN[7]_i_1_n_0 ),
        .D(s00_axis_tdata[4]),
        .Q(BLUE[4]),
        .R(1'b0));
  FDRE \BLUE_reg[5] 
       (.C(s00_axis_aclk),
        .CE(\GREEN[7]_i_1_n_0 ),
        .D(s00_axis_tdata[5]),
        .Q(BLUE[5]),
        .R(1'b0));
  FDRE \BLUE_reg[6] 
       (.C(s00_axis_aclk),
        .CE(\GREEN[7]_i_1_n_0 ),
        .D(s00_axis_tdata[6]),
        .Q(BLUE[6]),
        .R(1'b0));
  FDRE \BLUE_reg[7] 
       (.C(s00_axis_aclk),
        .CE(\GREEN[7]_i_1_n_0 ),
        .D(s00_axis_tdata[7]),
        .Q(BLUE[7]),
        .R(1'b0));
  LUT2 #(
    .INIT(4'h2)) 
    \GREEN[7]_i_1 
       (.I0(s00_axis_aresetn),
        .I1(M_AXIS_TUSER),
        .O(\GREEN[7]_i_1_n_0 ));
  FDRE \GREEN_reg[0] 
       (.C(s00_axis_aclk),
        .CE(\GREEN[7]_i_1_n_0 ),
        .D(s00_axis_tdata[8]),
        .Q(GREEN[0]),
        .R(1'b0));
  FDRE \GREEN_reg[1] 
       (.C(s00_axis_aclk),
        .CE(\GREEN[7]_i_1_n_0 ),
        .D(s00_axis_tdata[9]),
        .Q(GREEN[1]),
        .R(1'b0));
  FDRE \GREEN_reg[2] 
       (.C(s00_axis_aclk),
        .CE(\GREEN[7]_i_1_n_0 ),
        .D(s00_axis_tdata[10]),
        .Q(GREEN[2]),
        .R(1'b0));
  FDRE \GREEN_reg[3] 
       (.C(s00_axis_aclk),
        .CE(\GREEN[7]_i_1_n_0 ),
        .D(s00_axis_tdata[11]),
        .Q(GREEN[3]),
        .R(1'b0));
  FDRE \GREEN_reg[4] 
       (.C(s00_axis_aclk),
        .CE(\GREEN[7]_i_1_n_0 ),
        .D(s00_axis_tdata[12]),
        .Q(GREEN[4]),
        .R(1'b0));
  FDRE \GREEN_reg[5] 
       (.C(s00_axis_aclk),
        .CE(\GREEN[7]_i_1_n_0 ),
        .D(s00_axis_tdata[13]),
        .Q(GREEN[5]),
        .R(1'b0));
  FDRE \GREEN_reg[6] 
       (.C(s00_axis_aclk),
        .CE(\GREEN[7]_i_1_n_0 ),
        .D(s00_axis_tdata[14]),
        .Q(GREEN[6]),
        .R(1'b0));
  FDRE \GREEN_reg[7] 
       (.C(s00_axis_aclk),
        .CE(\GREEN[7]_i_1_n_0 ),
        .D(s00_axis_tdata[15]),
        .Q(GREEN[7]),
        .R(1'b0));
  FDRE \RED_reg[0] 
       (.C(s00_axis_aclk),
        .CE(\GREEN[7]_i_1_n_0 ),
        .D(s00_axis_tdata[16]),
        .Q(RED[0]),
        .R(1'b0));
  FDRE \RED_reg[1] 
       (.C(s00_axis_aclk),
        .CE(\GREEN[7]_i_1_n_0 ),
        .D(s00_axis_tdata[17]),
        .Q(RED[1]),
        .R(1'b0));
  FDRE \RED_reg[2] 
       (.C(s00_axis_aclk),
        .CE(\GREEN[7]_i_1_n_0 ),
        .D(s00_axis_tdata[18]),
        .Q(RED[2]),
        .R(1'b0));
  FDRE \RED_reg[3] 
       (.C(s00_axis_aclk),
        .CE(\GREEN[7]_i_1_n_0 ),
        .D(s00_axis_tdata[19]),
        .Q(RED[3]),
        .R(1'b0));
  FDRE \RED_reg[4] 
       (.C(s00_axis_aclk),
        .CE(\GREEN[7]_i_1_n_0 ),
        .D(s00_axis_tdata[20]),
        .Q(RED[4]),
        .R(1'b0));
  FDRE \RED_reg[5] 
       (.C(s00_axis_aclk),
        .CE(\GREEN[7]_i_1_n_0 ),
        .D(s00_axis_tdata[21]),
        .Q(RED[5]),
        .R(1'b0));
  FDRE \RED_reg[6] 
       (.C(s00_axis_aclk),
        .CE(\GREEN[7]_i_1_n_0 ),
        .D(s00_axis_tdata[22]),
        .Q(RED[6]),
        .R(1'b0));
  FDRE \RED_reg[7] 
       (.C(s00_axis_aclk),
        .CE(\GREEN[7]_i_1_n_0 ),
        .D(s00_axis_tdata[23]),
        .Q(RED[7]),
        .R(1'b0));
  LUT2 #(
    .INIT(4'h6)) 
    \m00_axis_tdata_r[11]_i_2 
       (.I0(s00_axis_tdata[11]),
        .I1(GREEN[3]),
        .O(\m00_axis_tdata_r[11]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \m00_axis_tdata_r[11]_i_3 
       (.I0(s00_axis_tdata[10]),
        .I1(GREEN[2]),
        .O(\m00_axis_tdata_r[11]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \m00_axis_tdata_r[11]_i_4 
       (.I0(s00_axis_tdata[9]),
        .I1(GREEN[1]),
        .O(\m00_axis_tdata_r[11]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \m00_axis_tdata_r[11]_i_5 
       (.I0(s00_axis_tdata[8]),
        .I1(GREEN[0]),
        .O(\m00_axis_tdata_r[11]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \m00_axis_tdata_r[15]_i_2 
       (.I0(s00_axis_tdata[15]),
        .I1(GREEN[7]),
        .O(\m00_axis_tdata_r[15]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \m00_axis_tdata_r[15]_i_3 
       (.I0(s00_axis_tdata[14]),
        .I1(GREEN[6]),
        .O(\m00_axis_tdata_r[15]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \m00_axis_tdata_r[15]_i_4 
       (.I0(s00_axis_tdata[13]),
        .I1(GREEN[5]),
        .O(\m00_axis_tdata_r[15]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \m00_axis_tdata_r[15]_i_5 
       (.I0(s00_axis_tdata[12]),
        .I1(GREEN[4]),
        .O(\m00_axis_tdata_r[15]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'hE)) 
    \m00_axis_tdata_r[16]_i_1 
       (.I0(m00_axis_tdata_r2__0),
        .I1(m00_axis_tdata_r3__0[8]),
        .O(\m00_axis_tdata_r[16]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \m00_axis_tdata_r[19]_i_2 
       (.I0(s00_axis_tdata[3]),
        .I1(BLUE[3]),
        .O(\m00_axis_tdata_r[19]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \m00_axis_tdata_r[19]_i_3 
       (.I0(s00_axis_tdata[2]),
        .I1(BLUE[2]),
        .O(\m00_axis_tdata_r[19]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \m00_axis_tdata_r[19]_i_4 
       (.I0(s00_axis_tdata[1]),
        .I1(BLUE[1]),
        .O(\m00_axis_tdata_r[19]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \m00_axis_tdata_r[19]_i_5 
       (.I0(s00_axis_tdata[0]),
        .I1(BLUE[0]),
        .O(\m00_axis_tdata_r[19]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \m00_axis_tdata_r[23]_i_2 
       (.I0(s00_axis_tdata[7]),
        .I1(BLUE[7]),
        .O(\m00_axis_tdata_r[23]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \m00_axis_tdata_r[23]_i_3 
       (.I0(s00_axis_tdata[6]),
        .I1(BLUE[6]),
        .O(\m00_axis_tdata_r[23]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \m00_axis_tdata_r[23]_i_4 
       (.I0(s00_axis_tdata[5]),
        .I1(BLUE[5]),
        .O(\m00_axis_tdata_r[23]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \m00_axis_tdata_r[23]_i_5 
       (.I0(s00_axis_tdata[4]),
        .I1(BLUE[4]),
        .O(\m00_axis_tdata_r[23]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_axis_tdata_r[24]_i_1 
       (.I0(s00_axis_aresetn),
        .I1(M_AXIS_TUSER),
        .O(\m00_axis_tdata_r[24]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \m00_axis_tdata_r[3]_i_2 
       (.I0(s00_axis_tdata[19]),
        .I1(RED[3]),
        .O(\m00_axis_tdata_r[3]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \m00_axis_tdata_r[3]_i_3 
       (.I0(s00_axis_tdata[18]),
        .I1(RED[2]),
        .O(\m00_axis_tdata_r[3]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \m00_axis_tdata_r[3]_i_4 
       (.I0(s00_axis_tdata[17]),
        .I1(RED[1]),
        .O(\m00_axis_tdata_r[3]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \m00_axis_tdata_r[3]_i_5 
       (.I0(s00_axis_tdata[16]),
        .I1(RED[0]),
        .O(\m00_axis_tdata_r[3]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \m00_axis_tdata_r[7]_i_2 
       (.I0(s00_axis_tdata[23]),
        .I1(RED[7]),
        .O(\m00_axis_tdata_r[7]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \m00_axis_tdata_r[7]_i_3 
       (.I0(s00_axis_tdata[22]),
        .I1(RED[6]),
        .O(\m00_axis_tdata_r[7]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \m00_axis_tdata_r[7]_i_4 
       (.I0(s00_axis_tdata[21]),
        .I1(RED[5]),
        .O(\m00_axis_tdata_r[7]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \m00_axis_tdata_r[7]_i_5 
       (.I0(s00_axis_tdata[20]),
        .I1(RED[4]),
        .O(\m00_axis_tdata_r[7]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'hE)) 
    \m00_axis_tdata_r[8]_i_1 
       (.I0(m00_axis_tdata_r21_out__0),
        .I1(m00_axis_tdata_r3__0[0]),
        .O(\m00_axis_tdata_r[8]_i_1_n_0 ));
  FDRE \m00_axis_tdata_r_reg[0] 
       (.C(s00_axis_aclk),
        .CE(\m00_axis_tdata_r[24]_i_1_n_0 ),
        .D(m00_axis_tdata_r21_out[0]),
        .Q(m00_axis_tdata[0]),
        .R(1'b0));
  FDRE \m00_axis_tdata_r_reg[10] 
       (.C(s00_axis_aclk),
        .CE(\m00_axis_tdata_r[24]_i_1_n_0 ),
        .D(m00_axis_tdata_r3[2]),
        .Q(m00_axis_tdata[10]),
        .R(1'b0));
  FDRE \m00_axis_tdata_r_reg[11] 
       (.C(s00_axis_aclk),
        .CE(\m00_axis_tdata_r[24]_i_1_n_0 ),
        .D(m00_axis_tdata_r3[3]),
        .Q(m00_axis_tdata[11]),
        .R(1'b0));
  CARRY4 \m00_axis_tdata_r_reg[11]_i_1 
       (.CI(1'b0),
        .CO({\m00_axis_tdata_r_reg[11]_i_1_n_0 ,\m00_axis_tdata_r_reg[11]_i_1_n_1 ,\m00_axis_tdata_r_reg[11]_i_1_n_2 ,\m00_axis_tdata_r_reg[11]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI(s00_axis_tdata[11:8]),
        .O({m00_axis_tdata_r3[3:1],m00_axis_tdata_r3__0[0]}),
        .S({\m00_axis_tdata_r[11]_i_2_n_0 ,\m00_axis_tdata_r[11]_i_3_n_0 ,\m00_axis_tdata_r[11]_i_4_n_0 ,\m00_axis_tdata_r[11]_i_5_n_0 }));
  FDRE \m00_axis_tdata_r_reg[12] 
       (.C(s00_axis_aclk),
        .CE(\m00_axis_tdata_r[24]_i_1_n_0 ),
        .D(m00_axis_tdata_r3[4]),
        .Q(m00_axis_tdata[12]),
        .R(1'b0));
  FDRE \m00_axis_tdata_r_reg[13] 
       (.C(s00_axis_aclk),
        .CE(\m00_axis_tdata_r[24]_i_1_n_0 ),
        .D(m00_axis_tdata_r3[5]),
        .Q(m00_axis_tdata[13]),
        .R(1'b0));
  FDRE \m00_axis_tdata_r_reg[14] 
       (.C(s00_axis_aclk),
        .CE(\m00_axis_tdata_r[24]_i_1_n_0 ),
        .D(m00_axis_tdata_r3[6]),
        .Q(m00_axis_tdata[14]),
        .R(1'b0));
  FDRE \m00_axis_tdata_r_reg[15] 
       (.C(s00_axis_aclk),
        .CE(\m00_axis_tdata_r[24]_i_1_n_0 ),
        .D(m00_axis_tdata_r3[7]),
        .Q(m00_axis_tdata[15]),
        .R(1'b0));
  CARRY4 \m00_axis_tdata_r_reg[15]_i_1 
       (.CI(\m00_axis_tdata_r_reg[11]_i_1_n_0 ),
        .CO({\m00_axis_tdata_r_reg[15]_i_1_n_0 ,\m00_axis_tdata_r_reg[15]_i_1_n_1 ,\m00_axis_tdata_r_reg[15]_i_1_n_2 ,\m00_axis_tdata_r_reg[15]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI(s00_axis_tdata[15:12]),
        .O(m00_axis_tdata_r3[7:4]),
        .S({\m00_axis_tdata_r[15]_i_2_n_0 ,\m00_axis_tdata_r[15]_i_3_n_0 ,\m00_axis_tdata_r[15]_i_4_n_0 ,\m00_axis_tdata_r[15]_i_5_n_0 }));
  FDRE \m00_axis_tdata_r_reg[16] 
       (.C(s00_axis_aclk),
        .CE(\m00_axis_tdata_r[24]_i_1_n_0 ),
        .D(\m00_axis_tdata_r[16]_i_1_n_0 ),
        .Q(m00_axis_tdata[16]),
        .R(1'b0));
  CARRY4 \m00_axis_tdata_r_reg[16]_i_2 
       (.CI(\m00_axis_tdata_r_reg[15]_i_1_n_0 ),
        .CO({\NLW_m00_axis_tdata_r_reg[16]_i_2_CO_UNCONNECTED [3:1],m00_axis_tdata_r3__0[8]}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_m00_axis_tdata_r_reg[16]_i_2_O_UNCONNECTED [3:0]),
        .S({1'b0,1'b0,1'b0,1'b1}));
  FDRE \m00_axis_tdata_r_reg[17] 
       (.C(s00_axis_aclk),
        .CE(\m00_axis_tdata_r[24]_i_1_n_0 ),
        .D(m00_axis_tdata_r2[1]),
        .Q(m00_axis_tdata[17]),
        .R(1'b0));
  FDRE \m00_axis_tdata_r_reg[18] 
       (.C(s00_axis_aclk),
        .CE(\m00_axis_tdata_r[24]_i_1_n_0 ),
        .D(m00_axis_tdata_r2[2]),
        .Q(m00_axis_tdata[18]),
        .R(1'b0));
  FDRE \m00_axis_tdata_r_reg[19] 
       (.C(s00_axis_aclk),
        .CE(\m00_axis_tdata_r[24]_i_1_n_0 ),
        .D(m00_axis_tdata_r2[3]),
        .Q(m00_axis_tdata[19]),
        .R(1'b0));
  CARRY4 \m00_axis_tdata_r_reg[19]_i_1 
       (.CI(1'b0),
        .CO({\m00_axis_tdata_r_reg[19]_i_1_n_0 ,\m00_axis_tdata_r_reg[19]_i_1_n_1 ,\m00_axis_tdata_r_reg[19]_i_1_n_2 ,\m00_axis_tdata_r_reg[19]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI(s00_axis_tdata[3:0]),
        .O({m00_axis_tdata_r2[3:1],m00_axis_tdata_r2__0}),
        .S({\m00_axis_tdata_r[19]_i_2_n_0 ,\m00_axis_tdata_r[19]_i_3_n_0 ,\m00_axis_tdata_r[19]_i_4_n_0 ,\m00_axis_tdata_r[19]_i_5_n_0 }));
  FDRE \m00_axis_tdata_r_reg[1] 
       (.C(s00_axis_aclk),
        .CE(\m00_axis_tdata_r[24]_i_1_n_0 ),
        .D(m00_axis_tdata_r21_out[1]),
        .Q(m00_axis_tdata[1]),
        .R(1'b0));
  FDRE \m00_axis_tdata_r_reg[20] 
       (.C(s00_axis_aclk),
        .CE(\m00_axis_tdata_r[24]_i_1_n_0 ),
        .D(m00_axis_tdata_r2[4]),
        .Q(m00_axis_tdata[20]),
        .R(1'b0));
  FDRE \m00_axis_tdata_r_reg[21] 
       (.C(s00_axis_aclk),
        .CE(\m00_axis_tdata_r[24]_i_1_n_0 ),
        .D(m00_axis_tdata_r2[5]),
        .Q(m00_axis_tdata[21]),
        .R(1'b0));
  FDRE \m00_axis_tdata_r_reg[22] 
       (.C(s00_axis_aclk),
        .CE(\m00_axis_tdata_r[24]_i_1_n_0 ),
        .D(m00_axis_tdata_r2[6]),
        .Q(m00_axis_tdata[22]),
        .R(1'b0));
  FDRE \m00_axis_tdata_r_reg[23] 
       (.C(s00_axis_aclk),
        .CE(\m00_axis_tdata_r[24]_i_1_n_0 ),
        .D(m00_axis_tdata_r2[7]),
        .Q(m00_axis_tdata[23]),
        .R(1'b0));
  CARRY4 \m00_axis_tdata_r_reg[23]_i_1 
       (.CI(\m00_axis_tdata_r_reg[19]_i_1_n_0 ),
        .CO({\m00_axis_tdata_r_reg[23]_i_1_n_0 ,\m00_axis_tdata_r_reg[23]_i_1_n_1 ,\m00_axis_tdata_r_reg[23]_i_1_n_2 ,\m00_axis_tdata_r_reg[23]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI(s00_axis_tdata[7:4]),
        .O(m00_axis_tdata_r2[7:4]),
        .S({\m00_axis_tdata_r[23]_i_2_n_0 ,\m00_axis_tdata_r[23]_i_3_n_0 ,\m00_axis_tdata_r[23]_i_4_n_0 ,\m00_axis_tdata_r[23]_i_5_n_0 }));
  FDRE \m00_axis_tdata_r_reg[24] 
       (.C(s00_axis_aclk),
        .CE(\m00_axis_tdata_r[24]_i_1_n_0 ),
        .D(m00_axis_tdata_r2[8]),
        .Q(m00_axis_tdata[24]),
        .R(1'b0));
  CARRY4 \m00_axis_tdata_r_reg[24]_i_2 
       (.CI(\m00_axis_tdata_r_reg[23]_i_1_n_0 ),
        .CO({\NLW_m00_axis_tdata_r_reg[24]_i_2_CO_UNCONNECTED [3:1],m00_axis_tdata_r2[8]}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_m00_axis_tdata_r_reg[24]_i_2_O_UNCONNECTED [3:0]),
        .S({1'b0,1'b0,1'b0,1'b1}));
  FDRE \m00_axis_tdata_r_reg[2] 
       (.C(s00_axis_aclk),
        .CE(\m00_axis_tdata_r[24]_i_1_n_0 ),
        .D(m00_axis_tdata_r21_out[2]),
        .Q(m00_axis_tdata[2]),
        .R(1'b0));
  FDRE \m00_axis_tdata_r_reg[3] 
       (.C(s00_axis_aclk),
        .CE(\m00_axis_tdata_r[24]_i_1_n_0 ),
        .D(m00_axis_tdata_r21_out[3]),
        .Q(m00_axis_tdata[3]),
        .R(1'b0));
  CARRY4 \m00_axis_tdata_r_reg[3]_i_1 
       (.CI(1'b0),
        .CO({\m00_axis_tdata_r_reg[3]_i_1_n_0 ,\m00_axis_tdata_r_reg[3]_i_1_n_1 ,\m00_axis_tdata_r_reg[3]_i_1_n_2 ,\m00_axis_tdata_r_reg[3]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI(s00_axis_tdata[19:16]),
        .O(m00_axis_tdata_r21_out[3:0]),
        .S({\m00_axis_tdata_r[3]_i_2_n_0 ,\m00_axis_tdata_r[3]_i_3_n_0 ,\m00_axis_tdata_r[3]_i_4_n_0 ,\m00_axis_tdata_r[3]_i_5_n_0 }));
  FDRE \m00_axis_tdata_r_reg[4] 
       (.C(s00_axis_aclk),
        .CE(\m00_axis_tdata_r[24]_i_1_n_0 ),
        .D(m00_axis_tdata_r21_out[4]),
        .Q(m00_axis_tdata[4]),
        .R(1'b0));
  FDRE \m00_axis_tdata_r_reg[5] 
       (.C(s00_axis_aclk),
        .CE(\m00_axis_tdata_r[24]_i_1_n_0 ),
        .D(m00_axis_tdata_r21_out[5]),
        .Q(m00_axis_tdata[5]),
        .R(1'b0));
  FDRE \m00_axis_tdata_r_reg[6] 
       (.C(s00_axis_aclk),
        .CE(\m00_axis_tdata_r[24]_i_1_n_0 ),
        .D(m00_axis_tdata_r21_out[6]),
        .Q(m00_axis_tdata[6]),
        .R(1'b0));
  FDRE \m00_axis_tdata_r_reg[7] 
       (.C(s00_axis_aclk),
        .CE(\m00_axis_tdata_r[24]_i_1_n_0 ),
        .D(m00_axis_tdata_r21_out[7]),
        .Q(m00_axis_tdata[7]),
        .R(1'b0));
  CARRY4 \m00_axis_tdata_r_reg[7]_i_1 
       (.CI(\m00_axis_tdata_r_reg[3]_i_1_n_0 ),
        .CO({\m00_axis_tdata_r_reg[7]_i_1_n_0 ,\m00_axis_tdata_r_reg[7]_i_1_n_1 ,\m00_axis_tdata_r_reg[7]_i_1_n_2 ,\m00_axis_tdata_r_reg[7]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI(s00_axis_tdata[23:20]),
        .O(m00_axis_tdata_r21_out[7:4]),
        .S({\m00_axis_tdata_r[7]_i_2_n_0 ,\m00_axis_tdata_r[7]_i_3_n_0 ,\m00_axis_tdata_r[7]_i_4_n_0 ,\m00_axis_tdata_r[7]_i_5_n_0 }));
  FDRE \m00_axis_tdata_r_reg[8] 
       (.C(s00_axis_aclk),
        .CE(\m00_axis_tdata_r[24]_i_1_n_0 ),
        .D(\m00_axis_tdata_r[8]_i_1_n_0 ),
        .Q(m00_axis_tdata[8]),
        .R(1'b0));
  CARRY4 \m00_axis_tdata_r_reg[8]_i_2 
       (.CI(\m00_axis_tdata_r_reg[7]_i_1_n_0 ),
        .CO({\NLW_m00_axis_tdata_r_reg[8]_i_2_CO_UNCONNECTED [3:1],m00_axis_tdata_r21_out__0}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_m00_axis_tdata_r_reg[8]_i_2_O_UNCONNECTED [3:0]),
        .S({1'b0,1'b0,1'b0,1'b1}));
  FDRE \m00_axis_tdata_r_reg[9] 
       (.C(s00_axis_aclk),
        .CE(\m00_axis_tdata_r[24]_i_1_n_0 ),
        .D(m00_axis_tdata_r3[1]),
        .Q(m00_axis_tdata[9]),
        .R(1'b0));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
