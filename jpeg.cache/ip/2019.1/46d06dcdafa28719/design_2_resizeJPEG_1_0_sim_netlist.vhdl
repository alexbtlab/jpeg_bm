-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1 (lin64) Build 2552052 Fri May 24 14:47:09 MDT 2019
-- Date        : Sat Sep 25 00:43:00 2021
-- Host        : alex-HP-Compaq-8200-Elite-CMT-PC running 64-bit Ubuntu 20.04.2 LTS
-- Command     : write_vhdl -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_2_resizeJPEG_1_0_sim_netlist.vhdl
-- Design      : design_2_resizeJPEG_1_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7z020clg484-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_resizeJPEG_v1_0 is
  port (
    m00_axis_tdata : out STD_LOGIC_VECTOR ( 24 downto 0 );
    s00_axis_aresetn : in STD_LOGIC;
    M_AXIS_TUSER : in STD_LOGIC_VECTOR ( 0 to 0 );
    s00_axis_tdata : in STD_LOGIC_VECTOR ( 23 downto 0 );
    s00_axis_aclk : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_resizeJPEG_v1_0;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_resizeJPEG_v1_0 is
  signal BLUE : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal GREEN : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal \GREEN[7]_i_1_n_0\ : STD_LOGIC;
  signal RED : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal m00_axis_tdata_r2 : STD_LOGIC_VECTOR ( 8 downto 1 );
  signal m00_axis_tdata_r21_out : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal \m00_axis_tdata_r21_out__0\ : STD_LOGIC_VECTOR ( 8 to 8 );
  signal \m00_axis_tdata_r2__0\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal m00_axis_tdata_r3 : STD_LOGIC_VECTOR ( 7 downto 1 );
  signal \m00_axis_tdata_r3__0\ : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal \m00_axis_tdata_r[11]_i_2_n_0\ : STD_LOGIC;
  signal \m00_axis_tdata_r[11]_i_3_n_0\ : STD_LOGIC;
  signal \m00_axis_tdata_r[11]_i_4_n_0\ : STD_LOGIC;
  signal \m00_axis_tdata_r[11]_i_5_n_0\ : STD_LOGIC;
  signal \m00_axis_tdata_r[15]_i_2_n_0\ : STD_LOGIC;
  signal \m00_axis_tdata_r[15]_i_3_n_0\ : STD_LOGIC;
  signal \m00_axis_tdata_r[15]_i_4_n_0\ : STD_LOGIC;
  signal \m00_axis_tdata_r[15]_i_5_n_0\ : STD_LOGIC;
  signal \m00_axis_tdata_r[16]_i_1_n_0\ : STD_LOGIC;
  signal \m00_axis_tdata_r[19]_i_2_n_0\ : STD_LOGIC;
  signal \m00_axis_tdata_r[19]_i_3_n_0\ : STD_LOGIC;
  signal \m00_axis_tdata_r[19]_i_4_n_0\ : STD_LOGIC;
  signal \m00_axis_tdata_r[19]_i_5_n_0\ : STD_LOGIC;
  signal \m00_axis_tdata_r[23]_i_2_n_0\ : STD_LOGIC;
  signal \m00_axis_tdata_r[23]_i_3_n_0\ : STD_LOGIC;
  signal \m00_axis_tdata_r[23]_i_4_n_0\ : STD_LOGIC;
  signal \m00_axis_tdata_r[23]_i_5_n_0\ : STD_LOGIC;
  signal \m00_axis_tdata_r[24]_i_1_n_0\ : STD_LOGIC;
  signal \m00_axis_tdata_r[3]_i_2_n_0\ : STD_LOGIC;
  signal \m00_axis_tdata_r[3]_i_3_n_0\ : STD_LOGIC;
  signal \m00_axis_tdata_r[3]_i_4_n_0\ : STD_LOGIC;
  signal \m00_axis_tdata_r[3]_i_5_n_0\ : STD_LOGIC;
  signal \m00_axis_tdata_r[7]_i_2_n_0\ : STD_LOGIC;
  signal \m00_axis_tdata_r[7]_i_3_n_0\ : STD_LOGIC;
  signal \m00_axis_tdata_r[7]_i_4_n_0\ : STD_LOGIC;
  signal \m00_axis_tdata_r[7]_i_5_n_0\ : STD_LOGIC;
  signal \m00_axis_tdata_r[8]_i_1_n_0\ : STD_LOGIC;
  signal \m00_axis_tdata_r_reg[11]_i_1_n_0\ : STD_LOGIC;
  signal \m00_axis_tdata_r_reg[11]_i_1_n_1\ : STD_LOGIC;
  signal \m00_axis_tdata_r_reg[11]_i_1_n_2\ : STD_LOGIC;
  signal \m00_axis_tdata_r_reg[11]_i_1_n_3\ : STD_LOGIC;
  signal \m00_axis_tdata_r_reg[15]_i_1_n_0\ : STD_LOGIC;
  signal \m00_axis_tdata_r_reg[15]_i_1_n_1\ : STD_LOGIC;
  signal \m00_axis_tdata_r_reg[15]_i_1_n_2\ : STD_LOGIC;
  signal \m00_axis_tdata_r_reg[15]_i_1_n_3\ : STD_LOGIC;
  signal \m00_axis_tdata_r_reg[19]_i_1_n_0\ : STD_LOGIC;
  signal \m00_axis_tdata_r_reg[19]_i_1_n_1\ : STD_LOGIC;
  signal \m00_axis_tdata_r_reg[19]_i_1_n_2\ : STD_LOGIC;
  signal \m00_axis_tdata_r_reg[19]_i_1_n_3\ : STD_LOGIC;
  signal \m00_axis_tdata_r_reg[23]_i_1_n_0\ : STD_LOGIC;
  signal \m00_axis_tdata_r_reg[23]_i_1_n_1\ : STD_LOGIC;
  signal \m00_axis_tdata_r_reg[23]_i_1_n_2\ : STD_LOGIC;
  signal \m00_axis_tdata_r_reg[23]_i_1_n_3\ : STD_LOGIC;
  signal \m00_axis_tdata_r_reg[3]_i_1_n_0\ : STD_LOGIC;
  signal \m00_axis_tdata_r_reg[3]_i_1_n_1\ : STD_LOGIC;
  signal \m00_axis_tdata_r_reg[3]_i_1_n_2\ : STD_LOGIC;
  signal \m00_axis_tdata_r_reg[3]_i_1_n_3\ : STD_LOGIC;
  signal \m00_axis_tdata_r_reg[7]_i_1_n_0\ : STD_LOGIC;
  signal \m00_axis_tdata_r_reg[7]_i_1_n_1\ : STD_LOGIC;
  signal \m00_axis_tdata_r_reg[7]_i_1_n_2\ : STD_LOGIC;
  signal \m00_axis_tdata_r_reg[7]_i_1_n_3\ : STD_LOGIC;
  signal \NLW_m00_axis_tdata_r_reg[16]_i_2_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_m00_axis_tdata_r_reg[16]_i_2_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_m00_axis_tdata_r_reg[24]_i_2_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_m00_axis_tdata_r_reg[24]_i_2_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_m00_axis_tdata_r_reg[8]_i_2_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_m00_axis_tdata_r_reg[8]_i_2_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
begin
\BLUE_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => \GREEN[7]_i_1_n_0\,
      D => s00_axis_tdata(0),
      Q => BLUE(0),
      R => '0'
    );
\BLUE_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => \GREEN[7]_i_1_n_0\,
      D => s00_axis_tdata(1),
      Q => BLUE(1),
      R => '0'
    );
\BLUE_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => \GREEN[7]_i_1_n_0\,
      D => s00_axis_tdata(2),
      Q => BLUE(2),
      R => '0'
    );
\BLUE_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => \GREEN[7]_i_1_n_0\,
      D => s00_axis_tdata(3),
      Q => BLUE(3),
      R => '0'
    );
\BLUE_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => \GREEN[7]_i_1_n_0\,
      D => s00_axis_tdata(4),
      Q => BLUE(4),
      R => '0'
    );
\BLUE_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => \GREEN[7]_i_1_n_0\,
      D => s00_axis_tdata(5),
      Q => BLUE(5),
      R => '0'
    );
\BLUE_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => \GREEN[7]_i_1_n_0\,
      D => s00_axis_tdata(6),
      Q => BLUE(6),
      R => '0'
    );
\BLUE_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => \GREEN[7]_i_1_n_0\,
      D => s00_axis_tdata(7),
      Q => BLUE(7),
      R => '0'
    );
\GREEN[7]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => s00_axis_aresetn,
      I1 => M_AXIS_TUSER(0),
      O => \GREEN[7]_i_1_n_0\
    );
\GREEN_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => \GREEN[7]_i_1_n_0\,
      D => s00_axis_tdata(8),
      Q => GREEN(0),
      R => '0'
    );
\GREEN_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => \GREEN[7]_i_1_n_0\,
      D => s00_axis_tdata(9),
      Q => GREEN(1),
      R => '0'
    );
\GREEN_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => \GREEN[7]_i_1_n_0\,
      D => s00_axis_tdata(10),
      Q => GREEN(2),
      R => '0'
    );
\GREEN_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => \GREEN[7]_i_1_n_0\,
      D => s00_axis_tdata(11),
      Q => GREEN(3),
      R => '0'
    );
\GREEN_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => \GREEN[7]_i_1_n_0\,
      D => s00_axis_tdata(12),
      Q => GREEN(4),
      R => '0'
    );
\GREEN_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => \GREEN[7]_i_1_n_0\,
      D => s00_axis_tdata(13),
      Q => GREEN(5),
      R => '0'
    );
\GREEN_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => \GREEN[7]_i_1_n_0\,
      D => s00_axis_tdata(14),
      Q => GREEN(6),
      R => '0'
    );
\GREEN_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => \GREEN[7]_i_1_n_0\,
      D => s00_axis_tdata(15),
      Q => GREEN(7),
      R => '0'
    );
\RED_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => \GREEN[7]_i_1_n_0\,
      D => s00_axis_tdata(16),
      Q => RED(0),
      R => '0'
    );
\RED_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => \GREEN[7]_i_1_n_0\,
      D => s00_axis_tdata(17),
      Q => RED(1),
      R => '0'
    );
\RED_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => \GREEN[7]_i_1_n_0\,
      D => s00_axis_tdata(18),
      Q => RED(2),
      R => '0'
    );
\RED_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => \GREEN[7]_i_1_n_0\,
      D => s00_axis_tdata(19),
      Q => RED(3),
      R => '0'
    );
\RED_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => \GREEN[7]_i_1_n_0\,
      D => s00_axis_tdata(20),
      Q => RED(4),
      R => '0'
    );
\RED_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => \GREEN[7]_i_1_n_0\,
      D => s00_axis_tdata(21),
      Q => RED(5),
      R => '0'
    );
\RED_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => \GREEN[7]_i_1_n_0\,
      D => s00_axis_tdata(22),
      Q => RED(6),
      R => '0'
    );
\RED_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => \GREEN[7]_i_1_n_0\,
      D => s00_axis_tdata(23),
      Q => RED(7),
      R => '0'
    );
\m00_axis_tdata_r[11]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => s00_axis_tdata(11),
      I1 => GREEN(3),
      O => \m00_axis_tdata_r[11]_i_2_n_0\
    );
\m00_axis_tdata_r[11]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => s00_axis_tdata(10),
      I1 => GREEN(2),
      O => \m00_axis_tdata_r[11]_i_3_n_0\
    );
\m00_axis_tdata_r[11]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => s00_axis_tdata(9),
      I1 => GREEN(1),
      O => \m00_axis_tdata_r[11]_i_4_n_0\
    );
\m00_axis_tdata_r[11]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => s00_axis_tdata(8),
      I1 => GREEN(0),
      O => \m00_axis_tdata_r[11]_i_5_n_0\
    );
\m00_axis_tdata_r[15]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => s00_axis_tdata(15),
      I1 => GREEN(7),
      O => \m00_axis_tdata_r[15]_i_2_n_0\
    );
\m00_axis_tdata_r[15]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => s00_axis_tdata(14),
      I1 => GREEN(6),
      O => \m00_axis_tdata_r[15]_i_3_n_0\
    );
\m00_axis_tdata_r[15]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => s00_axis_tdata(13),
      I1 => GREEN(5),
      O => \m00_axis_tdata_r[15]_i_4_n_0\
    );
\m00_axis_tdata_r[15]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => s00_axis_tdata(12),
      I1 => GREEN(4),
      O => \m00_axis_tdata_r[15]_i_5_n_0\
    );
\m00_axis_tdata_r[16]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => \m00_axis_tdata_r2__0\(0),
      I1 => \m00_axis_tdata_r3__0\(8),
      O => \m00_axis_tdata_r[16]_i_1_n_0\
    );
\m00_axis_tdata_r[19]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => s00_axis_tdata(3),
      I1 => BLUE(3),
      O => \m00_axis_tdata_r[19]_i_2_n_0\
    );
\m00_axis_tdata_r[19]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => s00_axis_tdata(2),
      I1 => BLUE(2),
      O => \m00_axis_tdata_r[19]_i_3_n_0\
    );
\m00_axis_tdata_r[19]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => s00_axis_tdata(1),
      I1 => BLUE(1),
      O => \m00_axis_tdata_r[19]_i_4_n_0\
    );
\m00_axis_tdata_r[19]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => s00_axis_tdata(0),
      I1 => BLUE(0),
      O => \m00_axis_tdata_r[19]_i_5_n_0\
    );
\m00_axis_tdata_r[23]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => s00_axis_tdata(7),
      I1 => BLUE(7),
      O => \m00_axis_tdata_r[23]_i_2_n_0\
    );
\m00_axis_tdata_r[23]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => s00_axis_tdata(6),
      I1 => BLUE(6),
      O => \m00_axis_tdata_r[23]_i_3_n_0\
    );
\m00_axis_tdata_r[23]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => s00_axis_tdata(5),
      I1 => BLUE(5),
      O => \m00_axis_tdata_r[23]_i_4_n_0\
    );
\m00_axis_tdata_r[23]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => s00_axis_tdata(4),
      I1 => BLUE(4),
      O => \m00_axis_tdata_r[23]_i_5_n_0\
    );
\m00_axis_tdata_r[24]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => s00_axis_aresetn,
      I1 => M_AXIS_TUSER(0),
      O => \m00_axis_tdata_r[24]_i_1_n_0\
    );
\m00_axis_tdata_r[3]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => s00_axis_tdata(19),
      I1 => RED(3),
      O => \m00_axis_tdata_r[3]_i_2_n_0\
    );
\m00_axis_tdata_r[3]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => s00_axis_tdata(18),
      I1 => RED(2),
      O => \m00_axis_tdata_r[3]_i_3_n_0\
    );
\m00_axis_tdata_r[3]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => s00_axis_tdata(17),
      I1 => RED(1),
      O => \m00_axis_tdata_r[3]_i_4_n_0\
    );
\m00_axis_tdata_r[3]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => s00_axis_tdata(16),
      I1 => RED(0),
      O => \m00_axis_tdata_r[3]_i_5_n_0\
    );
\m00_axis_tdata_r[7]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => s00_axis_tdata(23),
      I1 => RED(7),
      O => \m00_axis_tdata_r[7]_i_2_n_0\
    );
\m00_axis_tdata_r[7]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => s00_axis_tdata(22),
      I1 => RED(6),
      O => \m00_axis_tdata_r[7]_i_3_n_0\
    );
\m00_axis_tdata_r[7]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => s00_axis_tdata(21),
      I1 => RED(5),
      O => \m00_axis_tdata_r[7]_i_4_n_0\
    );
\m00_axis_tdata_r[7]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => s00_axis_tdata(20),
      I1 => RED(4),
      O => \m00_axis_tdata_r[7]_i_5_n_0\
    );
\m00_axis_tdata_r[8]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => \m00_axis_tdata_r21_out__0\(8),
      I1 => \m00_axis_tdata_r3__0\(0),
      O => \m00_axis_tdata_r[8]_i_1_n_0\
    );
\m00_axis_tdata_r_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => \m00_axis_tdata_r[24]_i_1_n_0\,
      D => m00_axis_tdata_r21_out(0),
      Q => m00_axis_tdata(0),
      R => '0'
    );
\m00_axis_tdata_r_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => \m00_axis_tdata_r[24]_i_1_n_0\,
      D => m00_axis_tdata_r3(2),
      Q => m00_axis_tdata(10),
      R => '0'
    );
\m00_axis_tdata_r_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => \m00_axis_tdata_r[24]_i_1_n_0\,
      D => m00_axis_tdata_r3(3),
      Q => m00_axis_tdata(11),
      R => '0'
    );
\m00_axis_tdata_r_reg[11]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \m00_axis_tdata_r_reg[11]_i_1_n_0\,
      CO(2) => \m00_axis_tdata_r_reg[11]_i_1_n_1\,
      CO(1) => \m00_axis_tdata_r_reg[11]_i_1_n_2\,
      CO(0) => \m00_axis_tdata_r_reg[11]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => s00_axis_tdata(11 downto 8),
      O(3 downto 1) => m00_axis_tdata_r3(3 downto 1),
      O(0) => \m00_axis_tdata_r3__0\(0),
      S(3) => \m00_axis_tdata_r[11]_i_2_n_0\,
      S(2) => \m00_axis_tdata_r[11]_i_3_n_0\,
      S(1) => \m00_axis_tdata_r[11]_i_4_n_0\,
      S(0) => \m00_axis_tdata_r[11]_i_5_n_0\
    );
\m00_axis_tdata_r_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => \m00_axis_tdata_r[24]_i_1_n_0\,
      D => m00_axis_tdata_r3(4),
      Q => m00_axis_tdata(12),
      R => '0'
    );
\m00_axis_tdata_r_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => \m00_axis_tdata_r[24]_i_1_n_0\,
      D => m00_axis_tdata_r3(5),
      Q => m00_axis_tdata(13),
      R => '0'
    );
\m00_axis_tdata_r_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => \m00_axis_tdata_r[24]_i_1_n_0\,
      D => m00_axis_tdata_r3(6),
      Q => m00_axis_tdata(14),
      R => '0'
    );
\m00_axis_tdata_r_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => \m00_axis_tdata_r[24]_i_1_n_0\,
      D => m00_axis_tdata_r3(7),
      Q => m00_axis_tdata(15),
      R => '0'
    );
\m00_axis_tdata_r_reg[15]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \m00_axis_tdata_r_reg[11]_i_1_n_0\,
      CO(3) => \m00_axis_tdata_r_reg[15]_i_1_n_0\,
      CO(2) => \m00_axis_tdata_r_reg[15]_i_1_n_1\,
      CO(1) => \m00_axis_tdata_r_reg[15]_i_1_n_2\,
      CO(0) => \m00_axis_tdata_r_reg[15]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => s00_axis_tdata(15 downto 12),
      O(3 downto 0) => m00_axis_tdata_r3(7 downto 4),
      S(3) => \m00_axis_tdata_r[15]_i_2_n_0\,
      S(2) => \m00_axis_tdata_r[15]_i_3_n_0\,
      S(1) => \m00_axis_tdata_r[15]_i_4_n_0\,
      S(0) => \m00_axis_tdata_r[15]_i_5_n_0\
    );
\m00_axis_tdata_r_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => \m00_axis_tdata_r[24]_i_1_n_0\,
      D => \m00_axis_tdata_r[16]_i_1_n_0\,
      Q => m00_axis_tdata(16),
      R => '0'
    );
\m00_axis_tdata_r_reg[16]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => \m00_axis_tdata_r_reg[15]_i_1_n_0\,
      CO(3 downto 1) => \NLW_m00_axis_tdata_r_reg[16]_i_2_CO_UNCONNECTED\(3 downto 1),
      CO(0) => \m00_axis_tdata_r3__0\(8),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_m00_axis_tdata_r_reg[16]_i_2_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => B"0001"
    );
\m00_axis_tdata_r_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => \m00_axis_tdata_r[24]_i_1_n_0\,
      D => m00_axis_tdata_r2(1),
      Q => m00_axis_tdata(17),
      R => '0'
    );
\m00_axis_tdata_r_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => \m00_axis_tdata_r[24]_i_1_n_0\,
      D => m00_axis_tdata_r2(2),
      Q => m00_axis_tdata(18),
      R => '0'
    );
\m00_axis_tdata_r_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => \m00_axis_tdata_r[24]_i_1_n_0\,
      D => m00_axis_tdata_r2(3),
      Q => m00_axis_tdata(19),
      R => '0'
    );
\m00_axis_tdata_r_reg[19]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \m00_axis_tdata_r_reg[19]_i_1_n_0\,
      CO(2) => \m00_axis_tdata_r_reg[19]_i_1_n_1\,
      CO(1) => \m00_axis_tdata_r_reg[19]_i_1_n_2\,
      CO(0) => \m00_axis_tdata_r_reg[19]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => s00_axis_tdata(3 downto 0),
      O(3 downto 1) => m00_axis_tdata_r2(3 downto 1),
      O(0) => \m00_axis_tdata_r2__0\(0),
      S(3) => \m00_axis_tdata_r[19]_i_2_n_0\,
      S(2) => \m00_axis_tdata_r[19]_i_3_n_0\,
      S(1) => \m00_axis_tdata_r[19]_i_4_n_0\,
      S(0) => \m00_axis_tdata_r[19]_i_5_n_0\
    );
\m00_axis_tdata_r_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => \m00_axis_tdata_r[24]_i_1_n_0\,
      D => m00_axis_tdata_r21_out(1),
      Q => m00_axis_tdata(1),
      R => '0'
    );
\m00_axis_tdata_r_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => \m00_axis_tdata_r[24]_i_1_n_0\,
      D => m00_axis_tdata_r2(4),
      Q => m00_axis_tdata(20),
      R => '0'
    );
\m00_axis_tdata_r_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => \m00_axis_tdata_r[24]_i_1_n_0\,
      D => m00_axis_tdata_r2(5),
      Q => m00_axis_tdata(21),
      R => '0'
    );
\m00_axis_tdata_r_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => \m00_axis_tdata_r[24]_i_1_n_0\,
      D => m00_axis_tdata_r2(6),
      Q => m00_axis_tdata(22),
      R => '0'
    );
\m00_axis_tdata_r_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => \m00_axis_tdata_r[24]_i_1_n_0\,
      D => m00_axis_tdata_r2(7),
      Q => m00_axis_tdata(23),
      R => '0'
    );
\m00_axis_tdata_r_reg[23]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \m00_axis_tdata_r_reg[19]_i_1_n_0\,
      CO(3) => \m00_axis_tdata_r_reg[23]_i_1_n_0\,
      CO(2) => \m00_axis_tdata_r_reg[23]_i_1_n_1\,
      CO(1) => \m00_axis_tdata_r_reg[23]_i_1_n_2\,
      CO(0) => \m00_axis_tdata_r_reg[23]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => s00_axis_tdata(7 downto 4),
      O(3 downto 0) => m00_axis_tdata_r2(7 downto 4),
      S(3) => \m00_axis_tdata_r[23]_i_2_n_0\,
      S(2) => \m00_axis_tdata_r[23]_i_3_n_0\,
      S(1) => \m00_axis_tdata_r[23]_i_4_n_0\,
      S(0) => \m00_axis_tdata_r[23]_i_5_n_0\
    );
\m00_axis_tdata_r_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => \m00_axis_tdata_r[24]_i_1_n_0\,
      D => m00_axis_tdata_r2(8),
      Q => m00_axis_tdata(24),
      R => '0'
    );
\m00_axis_tdata_r_reg[24]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => \m00_axis_tdata_r_reg[23]_i_1_n_0\,
      CO(3 downto 1) => \NLW_m00_axis_tdata_r_reg[24]_i_2_CO_UNCONNECTED\(3 downto 1),
      CO(0) => m00_axis_tdata_r2(8),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_m00_axis_tdata_r_reg[24]_i_2_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => B"0001"
    );
\m00_axis_tdata_r_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => \m00_axis_tdata_r[24]_i_1_n_0\,
      D => m00_axis_tdata_r21_out(2),
      Q => m00_axis_tdata(2),
      R => '0'
    );
\m00_axis_tdata_r_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => \m00_axis_tdata_r[24]_i_1_n_0\,
      D => m00_axis_tdata_r21_out(3),
      Q => m00_axis_tdata(3),
      R => '0'
    );
\m00_axis_tdata_r_reg[3]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \m00_axis_tdata_r_reg[3]_i_1_n_0\,
      CO(2) => \m00_axis_tdata_r_reg[3]_i_1_n_1\,
      CO(1) => \m00_axis_tdata_r_reg[3]_i_1_n_2\,
      CO(0) => \m00_axis_tdata_r_reg[3]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => s00_axis_tdata(19 downto 16),
      O(3 downto 0) => m00_axis_tdata_r21_out(3 downto 0),
      S(3) => \m00_axis_tdata_r[3]_i_2_n_0\,
      S(2) => \m00_axis_tdata_r[3]_i_3_n_0\,
      S(1) => \m00_axis_tdata_r[3]_i_4_n_0\,
      S(0) => \m00_axis_tdata_r[3]_i_5_n_0\
    );
\m00_axis_tdata_r_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => \m00_axis_tdata_r[24]_i_1_n_0\,
      D => m00_axis_tdata_r21_out(4),
      Q => m00_axis_tdata(4),
      R => '0'
    );
\m00_axis_tdata_r_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => \m00_axis_tdata_r[24]_i_1_n_0\,
      D => m00_axis_tdata_r21_out(5),
      Q => m00_axis_tdata(5),
      R => '0'
    );
\m00_axis_tdata_r_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => \m00_axis_tdata_r[24]_i_1_n_0\,
      D => m00_axis_tdata_r21_out(6),
      Q => m00_axis_tdata(6),
      R => '0'
    );
\m00_axis_tdata_r_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => \m00_axis_tdata_r[24]_i_1_n_0\,
      D => m00_axis_tdata_r21_out(7),
      Q => m00_axis_tdata(7),
      R => '0'
    );
\m00_axis_tdata_r_reg[7]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \m00_axis_tdata_r_reg[3]_i_1_n_0\,
      CO(3) => \m00_axis_tdata_r_reg[7]_i_1_n_0\,
      CO(2) => \m00_axis_tdata_r_reg[7]_i_1_n_1\,
      CO(1) => \m00_axis_tdata_r_reg[7]_i_1_n_2\,
      CO(0) => \m00_axis_tdata_r_reg[7]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => s00_axis_tdata(23 downto 20),
      O(3 downto 0) => m00_axis_tdata_r21_out(7 downto 4),
      S(3) => \m00_axis_tdata_r[7]_i_2_n_0\,
      S(2) => \m00_axis_tdata_r[7]_i_3_n_0\,
      S(1) => \m00_axis_tdata_r[7]_i_4_n_0\,
      S(0) => \m00_axis_tdata_r[7]_i_5_n_0\
    );
\m00_axis_tdata_r_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => \m00_axis_tdata_r[24]_i_1_n_0\,
      D => \m00_axis_tdata_r[8]_i_1_n_0\,
      Q => m00_axis_tdata(8),
      R => '0'
    );
\m00_axis_tdata_r_reg[8]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => \m00_axis_tdata_r_reg[7]_i_1_n_0\,
      CO(3 downto 1) => \NLW_m00_axis_tdata_r_reg[8]_i_2_CO_UNCONNECTED\(3 downto 1),
      CO(0) => \m00_axis_tdata_r21_out__0\(8),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_m00_axis_tdata_r_reg[8]_i_2_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => B"0001"
    );
\m00_axis_tdata_r_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axis_aclk,
      CE => \m00_axis_tdata_r[24]_i_1_n_0\,
      D => m00_axis_tdata_r3(1),
      Q => m00_axis_tdata(9),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  port (
    s00_axis_aclk : in STD_LOGIC;
    m00_axis_aclk : in STD_LOGIC;
    s00_axis_aresetn : in STD_LOGIC;
    s00_axis_tready : out STD_LOGIC;
    s00_axis_tdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s00_axis_tstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s00_axis_tlast : in STD_LOGIC;
    s00_axis_tvalid : in STD_LOGIC;
    M_AXIS_TUSER : in STD_LOGIC_VECTOR ( 47 downto 0 );
    m00_axis_aresetn : in STD_LOGIC;
    m00_axis_tvalid : out STD_LOGIC;
    m00_axis_tdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    m00_axis_tstrb : out STD_LOGIC_VECTOR ( 3 downto 0 );
    m00_axis_tlast : out STD_LOGIC;
    m00_axis_tready : in STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "design_2_resizeJPEG_1_0,resizeJPEG_v1_0,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "yes";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "resizeJPEG_v1_0,Vivado 2019.1";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  signal \<const0>\ : STD_LOGIC;
  signal \<const1>\ : STD_LOGIC;
  signal \^m00_axis_tdata\ : STD_LOGIC_VECTOR ( 24 downto 0 );
  attribute X_INTERFACE_INFO : string;
  attribute X_INTERFACE_INFO of m00_axis_aclk : signal is "xilinx.com:signal:clock:1.0 m00_axis_aclk CLK";
  attribute X_INTERFACE_PARAMETER : string;
  attribute X_INTERFACE_PARAMETER of m00_axis_aclk : signal is "XIL_INTERFACENAME m00_axis_aclk, ASSOCIATED_RESET m00_axis_aresetn, ASSOCIATED_BUSIF maxis, FREQ_HZ 200000000, PHASE 0.000, CLK_DOMAIN design_2_processing_system7_0_0_FCLK_CLK0, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of m00_axis_aresetn : signal is "xilinx.com:signal:reset:1.0 m00_axis_aresetn RST";
  attribute X_INTERFACE_PARAMETER of m00_axis_aresetn : signal is "XIL_INTERFACENAME m00_axis_aresetn, POLARITY ACTIVE_LOW, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of m00_axis_tlast : signal is "xilinx.com:interface:axis:1.0 maxis TLAST";
  attribute X_INTERFACE_INFO of m00_axis_tready : signal is "xilinx.com:interface:axis:1.0 maxis TREADY";
  attribute X_INTERFACE_PARAMETER of m00_axis_tready : signal is "XIL_INTERFACENAME maxis, TDATA_NUM_BYTES 4, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 1, HAS_TKEEP 0, HAS_TLAST 1, FREQ_HZ 200000000, PHASE 0.000, CLK_DOMAIN design_2_processing_system7_0_0_FCLK_CLK0, LAYERED_METADATA undef, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of m00_axis_tvalid : signal is "xilinx.com:interface:axis:1.0 maxis TVALID";
  attribute X_INTERFACE_INFO of s00_axis_aclk : signal is "xilinx.com:signal:clock:1.0 s00_axis_aclk CLK";
  attribute X_INTERFACE_PARAMETER of s00_axis_aclk : signal is "XIL_INTERFACENAME s00_axis_aclk, ASSOCIATED_RESET s00_axis_aresetn, ASSOCIATED_BUSIF saxis, FREQ_HZ 200000000, PHASE 0.000, CLK_DOMAIN design_2_processing_system7_0_0_FCLK_CLK0, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of s00_axis_aresetn : signal is "xilinx.com:signal:reset:1.0 s00_axis_aresetn RST";
  attribute X_INTERFACE_PARAMETER of s00_axis_aresetn : signal is "XIL_INTERFACENAME s00_axis_aresetn, POLARITY ACTIVE_LOW, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of s00_axis_tlast : signal is "xilinx.com:interface:axis:1.0 saxis TLAST";
  attribute X_INTERFACE_INFO of s00_axis_tready : signal is "xilinx.com:interface:axis:1.0 saxis TREADY";
  attribute X_INTERFACE_INFO of s00_axis_tvalid : signal is "xilinx.com:interface:axis:1.0 saxis TVALID";
  attribute X_INTERFACE_INFO of M_AXIS_TUSER : signal is "xilinx.com:interface:axis:1.0 saxis TUSER";
  attribute X_INTERFACE_PARAMETER of M_AXIS_TUSER : signal is "XIL_INTERFACENAME saxis, TDATA_NUM_BYTES 4, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 48, HAS_TREADY 1, HAS_TSTRB 1, HAS_TKEEP 0, HAS_TLAST 1, FREQ_HZ 200000000, PHASE 0.000, CLK_DOMAIN design_2_processing_system7_0_0_FCLK_CLK0, LAYERED_METADATA undef, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of m00_axis_tdata : signal is "xilinx.com:interface:axis:1.0 maxis TDATA";
  attribute X_INTERFACE_INFO of m00_axis_tstrb : signal is "xilinx.com:interface:axis:1.0 maxis TSTRB";
  attribute X_INTERFACE_INFO of s00_axis_tdata : signal is "xilinx.com:interface:axis:1.0 saxis TDATA";
  attribute X_INTERFACE_INFO of s00_axis_tstrb : signal is "xilinx.com:interface:axis:1.0 saxis TSTRB";
begin
  m00_axis_tdata(31) <= \<const0>\;
  m00_axis_tdata(30) <= \<const0>\;
  m00_axis_tdata(29) <= \<const0>\;
  m00_axis_tdata(28) <= \<const0>\;
  m00_axis_tdata(27) <= \<const0>\;
  m00_axis_tdata(26) <= \<const0>\;
  m00_axis_tdata(25) <= \<const0>\;
  m00_axis_tdata(24 downto 0) <= \^m00_axis_tdata\(24 downto 0);
  s00_axis_tready <= \<const1>\;
  m00_axis_tlast <= 'Z';
  m00_axis_tvalid <= 'Z';
  m00_axis_tstrb(0) <= 'Z';
  m00_axis_tstrb(1) <= 'Z';
  m00_axis_tstrb(2) <= 'Z';
  m00_axis_tstrb(3) <= 'Z';
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
VCC: unisim.vcomponents.VCC
     port map (
      P => \<const1>\
    );
inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_resizeJPEG_v1_0
     port map (
      M_AXIS_TUSER(0) => M_AXIS_TUSER(16),
      m00_axis_tdata(24 downto 0) => \^m00_axis_tdata\(24 downto 0),
      s00_axis_aclk => s00_axis_aclk,
      s00_axis_aresetn => s00_axis_aresetn,
      s00_axis_tdata(23 downto 0) => s00_axis_tdata(23 downto 0)
    );
end STRUCTURE;
