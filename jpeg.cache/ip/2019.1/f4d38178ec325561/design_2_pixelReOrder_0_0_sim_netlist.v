// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (lin64) Build 2552052 Fri May 24 14:47:09 MDT 2019
// Date        : Sat Sep 25 17:51:13 2021
// Host        : alex-HP-Compaq-8200-Elite-CMT-PC running 64-bit Ubuntu 20.04.2 LTS
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_2_pixelReOrder_0_0_sim_netlist.v
// Design      : design_2_pixelReOrder_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7z020clg484-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_2_pixelReOrder_0_0,pixelReOrder_v1_0,{}" *) (* DowngradeIPIdentifiedWarnings = "yes" *) (* X_CORE_INFO = "pixelReOrder_v1_0,Vivado 2019.1" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (m00_axis_aclk,
    m00_axis_aresetn,
    m00_axis_tvalid,
    m00_axis_tdata,
    m00_axis_tstrb,
    m00_axis_tlast,
    m00_axis_tready,
    s00_axis_aclk,
    s00_axis_aresetn,
    s00_axis_tready,
    s00_axis_tdata,
    s00_axis_tuser,
    s00_axis_tstrb,
    s00_axis_tlast,
    s00_axis_tvalid);
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 M00_AXIS_CLK CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME M00_AXIS_CLK, ASSOCIATED_BUSIF M00_AXIS, ASSOCIATED_RESET m00_axis_aresetn, FREQ_HZ 200000000, PHASE 0.000, CLK_DOMAIN design_2_processing_system7_0_0_FCLK_CLK0, INSERT_VIP 0" *) input m00_axis_aclk;
  (* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 M00_AXIS_RST RST" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME M00_AXIS_RST, POLARITY ACTIVE_LOW, INSERT_VIP 0" *) input m00_axis_aresetn;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 M00_AXIS TVALID" *) output m00_axis_tvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 M00_AXIS TDATA" *) output [31:0]m00_axis_tdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 M00_AXIS TSTRB" *) output [3:0]m00_axis_tstrb;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 M00_AXIS TLAST" *) output m00_axis_tlast;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 M00_AXIS TREADY" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME M00_AXIS, WIZ_DATA_WIDTH 32, TDATA_NUM_BYTES 4, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 1, HAS_TKEEP 0, HAS_TLAST 1, FREQ_HZ 200000000, PHASE 0.000, CLK_DOMAIN design_2_processing_system7_0_0_FCLK_CLK0, LAYERED_METADATA undef, INSERT_VIP 0" *) input m00_axis_tready;
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 S00_AXIS_CLK CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME S00_AXIS_CLK, ASSOCIATED_BUSIF S00_AXIS, ASSOCIATED_RESET s00_axis_aresetn, FREQ_HZ 200000000, PHASE 0.000, CLK_DOMAIN design_2_processing_system7_0_0_FCLK_CLK0, INSERT_VIP 0" *) input s00_axis_aclk;
  (* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 S00_AXIS_RST RST" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME S00_AXIS_RST, POLARITY ACTIVE_LOW, INSERT_VIP 0" *) input s00_axis_aresetn;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 S00_AXIS TREADY" *) output s00_axis_tready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 S00_AXIS TDATA" *) input [31:0]s00_axis_tdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 S00_AXIS TUSER" *) input [47:0]s00_axis_tuser;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 S00_AXIS TSTRB" *) input [3:0]s00_axis_tstrb;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 S00_AXIS TLAST" *) input s00_axis_tlast;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 S00_AXIS TVALID" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME S00_AXIS, WIZ_DATA_WIDTH 32, TDATA_NUM_BYTES 4, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 48, HAS_TREADY 1, HAS_TSTRB 1, HAS_TKEEP 0, HAS_TLAST 1, FREQ_HZ 200000000, PHASE 0.000, CLK_DOMAIN design_2_processing_system7_0_0_FCLK_CLK0, LAYERED_METADATA undef, INSERT_VIP 0" *) input s00_axis_tvalid;

  wire \<const1> ;
  wire m00_axis_aclk;
  wire [31:0]m00_axis_tdata;
  wire m00_axis_tvalid;
  wire [31:0]s00_axis_tdata;
  wire [47:0]s00_axis_tuser;
  wire s00_axis_tvalid;

  assign m00_axis_tlast = \<const1> ;
  assign s00_axis_tready = \<const1> ;
  VCC VCC
       (.P(\<const1> ));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_pixelReOrder_v1_0 inst
       (.m00_axis_aclk(m00_axis_aclk),
        .m00_axis_tdata(m00_axis_tdata),
        .m00_axis_tvalid(m00_axis_tvalid),
        .s00_axis_tdata(s00_axis_tdata),
        .s00_axis_tuser({s00_axis_tuser[31:16],s00_axis_tuser[10:0]}),
        .s00_axis_tvalid(s00_axis_tvalid));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_pixelReOrder_v1_0
   (m00_axis_tvalid,
    m00_axis_tdata,
    s00_axis_tvalid,
    s00_axis_tuser,
    m00_axis_aclk,
    s00_axis_tdata);
  output m00_axis_tvalid;
  output [31:0]m00_axis_tdata;
  input s00_axis_tvalid;
  input [26:0]s00_axis_tuser;
  input m00_axis_aclk;
  input [31:0]s00_axis_tdata;

  wire [31:0]DI0;
  wire [31:0]DOUT0;
  wire [31:0]DOUT1_w;
  wire [31:0]DOUT2_w;
  wire [31:0]DOUT4_w;
  wire [31:0]DOUT5_w;
  wire [31:0]DOUT6_w;
  wire [31:0]DOUT8_w;
  wire [31:0]DOUT9_w;
  wire [31:0]DOUTA_w;
  wire [31:0]DOUTC_w;
  wire [31:0]DOUTD_w;
  wire [31:0]DOUTE_w;
  wire [15:0]EN_r;
  wire \EN_r[0]_i_1_n_0 ;
  wire \EN_r[11]_i_1_n_0 ;
  wire \EN_r[11]_i_2_n_0 ;
  wire \EN_r[13]_i_1_n_0 ;
  wire \EN_r[14]_i_1_n_0 ;
  wire \EN_r[15]_i_1_n_0 ;
  wire \EN_r[15]_i_2_n_0 ;
  wire \EN_r[15]_i_3_n_0 ;
  wire \EN_r[1]_i_1_n_0 ;
  wire \EN_r[6]_i_1_n_0 ;
  wire \EN_r[7]_i_1_n_0 ;
  wire \EN_r[8]_i_1_n_0 ;
  wire \EN_r[9]_i_1_n_0 ;
  wire \EN_r[9]_i_2_n_0 ;
  wire WE_r;
  wire [10:0]adr_wr;
  wire \adr_wr_reg[0]_rep_n_0 ;
  wire \adr_wr_reg[10]_rep_n_0 ;
  wire \adr_wr_reg[1]_rep_n_0 ;
  wire \adr_wr_reg[2]_rep_n_0 ;
  wire \adr_wr_reg[3]_rep_n_0 ;
  wire \adr_wr_reg[4]_rep_n_0 ;
  wire \adr_wr_reg[5]_rep_n_0 ;
  wire \adr_wr_reg[6]_rep_n_0 ;
  wire \adr_wr_reg[7]_rep_n_0 ;
  wire \adr_wr_reg[8]_rep_n_0 ;
  wire \adr_wr_reg[9]_rep_n_0 ;
  wire [15:0]cnt_inner_valid;
  wire cnt_inner_valid0_carry__0_n_0;
  wire cnt_inner_valid0_carry__0_n_1;
  wire cnt_inner_valid0_carry__0_n_2;
  wire cnt_inner_valid0_carry__0_n_3;
  wire cnt_inner_valid0_carry__1_n_0;
  wire cnt_inner_valid0_carry__1_n_1;
  wire cnt_inner_valid0_carry__1_n_2;
  wire cnt_inner_valid0_carry__1_n_3;
  wire cnt_inner_valid0_carry__2_n_2;
  wire cnt_inner_valid0_carry__2_n_3;
  wire cnt_inner_valid0_carry_n_0;
  wire cnt_inner_valid0_carry_n_1;
  wire cnt_inner_valid0_carry_n_2;
  wire cnt_inner_valid0_carry_n_3;
  wire \cnt_inner_valid[15]_i_3_n_0 ;
  wire \cnt_inner_valid_reg_n_0_[0] ;
  wire \cnt_inner_valid_reg_n_0_[10] ;
  wire \cnt_inner_valid_reg_n_0_[11] ;
  wire \cnt_inner_valid_reg_n_0_[12] ;
  wire \cnt_inner_valid_reg_n_0_[13] ;
  wire \cnt_inner_valid_reg_n_0_[14] ;
  wire \cnt_inner_valid_reg_n_0_[15] ;
  wire \cnt_inner_valid_reg_n_0_[1] ;
  wire \cnt_inner_valid_reg_n_0_[2] ;
  wire \cnt_inner_valid_reg_n_0_[3] ;
  wire \cnt_inner_valid_reg_n_0_[4] ;
  wire \cnt_inner_valid_reg_n_0_[5] ;
  wire \cnt_inner_valid_reg_n_0_[6] ;
  wire \cnt_inner_valid_reg_n_0_[7] ;
  wire \cnt_inner_valid_reg_n_0_[8] ;
  wire \cnt_inner_valid_reg_n_0_[9] ;
  wire [15:1]data0;
  wire m00_axis_aclk;
  wire [31:0]m00_axis_tdata;
  wire \m00_axis_tdata_r[31]_i_1_n_0 ;
  wire \m00_axis_tdata_r[31]_i_3_n_0 ;
  wire \m00_axis_tdata_r[31]_i_4_n_0 ;
  wire m00_axis_tvalid;
  wire m00_axis_tvalid_r0;
  wire m00_axis_tvalid_r_i_2_n_0;
  wire m00_axis_tvalid_r_i_3_n_0;
  wire m00_axis_tvalid_r_i_4_n_0;
  wire p_0_in;
  wire rams_sp_rf_rst_inst3_n_0;
  wire rams_sp_rf_rst_inst3_n_1;
  wire rams_sp_rf_rst_inst3_n_10;
  wire rams_sp_rf_rst_inst3_n_11;
  wire rams_sp_rf_rst_inst3_n_12;
  wire rams_sp_rf_rst_inst3_n_13;
  wire rams_sp_rf_rst_inst3_n_14;
  wire rams_sp_rf_rst_inst3_n_15;
  wire rams_sp_rf_rst_inst3_n_16;
  wire rams_sp_rf_rst_inst3_n_17;
  wire rams_sp_rf_rst_inst3_n_18;
  wire rams_sp_rf_rst_inst3_n_19;
  wire rams_sp_rf_rst_inst3_n_2;
  wire rams_sp_rf_rst_inst3_n_20;
  wire rams_sp_rf_rst_inst3_n_21;
  wire rams_sp_rf_rst_inst3_n_22;
  wire rams_sp_rf_rst_inst3_n_23;
  wire rams_sp_rf_rst_inst3_n_24;
  wire rams_sp_rf_rst_inst3_n_25;
  wire rams_sp_rf_rst_inst3_n_26;
  wire rams_sp_rf_rst_inst3_n_27;
  wire rams_sp_rf_rst_inst3_n_28;
  wire rams_sp_rf_rst_inst3_n_29;
  wire rams_sp_rf_rst_inst3_n_3;
  wire rams_sp_rf_rst_inst3_n_30;
  wire rams_sp_rf_rst_inst3_n_31;
  wire rams_sp_rf_rst_inst3_n_4;
  wire rams_sp_rf_rst_inst3_n_5;
  wire rams_sp_rf_rst_inst3_n_6;
  wire rams_sp_rf_rst_inst3_n_7;
  wire rams_sp_rf_rst_inst3_n_8;
  wire rams_sp_rf_rst_inst3_n_9;
  wire rams_sp_rf_rst_inst7_n_0;
  wire rams_sp_rf_rst_inst7_n_1;
  wire rams_sp_rf_rst_inst7_n_10;
  wire rams_sp_rf_rst_inst7_n_11;
  wire rams_sp_rf_rst_inst7_n_12;
  wire rams_sp_rf_rst_inst7_n_13;
  wire rams_sp_rf_rst_inst7_n_14;
  wire rams_sp_rf_rst_inst7_n_15;
  wire rams_sp_rf_rst_inst7_n_16;
  wire rams_sp_rf_rst_inst7_n_17;
  wire rams_sp_rf_rst_inst7_n_18;
  wire rams_sp_rf_rst_inst7_n_19;
  wire rams_sp_rf_rst_inst7_n_2;
  wire rams_sp_rf_rst_inst7_n_20;
  wire rams_sp_rf_rst_inst7_n_21;
  wire rams_sp_rf_rst_inst7_n_22;
  wire rams_sp_rf_rst_inst7_n_23;
  wire rams_sp_rf_rst_inst7_n_24;
  wire rams_sp_rf_rst_inst7_n_25;
  wire rams_sp_rf_rst_inst7_n_26;
  wire rams_sp_rf_rst_inst7_n_27;
  wire rams_sp_rf_rst_inst7_n_28;
  wire rams_sp_rf_rst_inst7_n_29;
  wire rams_sp_rf_rst_inst7_n_3;
  wire rams_sp_rf_rst_inst7_n_30;
  wire rams_sp_rf_rst_inst7_n_31;
  wire rams_sp_rf_rst_inst7_n_4;
  wire rams_sp_rf_rst_inst7_n_5;
  wire rams_sp_rf_rst_inst7_n_6;
  wire rams_sp_rf_rst_inst7_n_7;
  wire rams_sp_rf_rst_inst7_n_8;
  wire rams_sp_rf_rst_inst7_n_9;
  wire rams_sp_rf_rst_instb_n_0;
  wire rams_sp_rf_rst_instb_n_1;
  wire rams_sp_rf_rst_instb_n_10;
  wire rams_sp_rf_rst_instb_n_11;
  wire rams_sp_rf_rst_instb_n_12;
  wire rams_sp_rf_rst_instb_n_13;
  wire rams_sp_rf_rst_instb_n_14;
  wire rams_sp_rf_rst_instb_n_15;
  wire rams_sp_rf_rst_instb_n_16;
  wire rams_sp_rf_rst_instb_n_17;
  wire rams_sp_rf_rst_instb_n_18;
  wire rams_sp_rf_rst_instb_n_19;
  wire rams_sp_rf_rst_instb_n_2;
  wire rams_sp_rf_rst_instb_n_20;
  wire rams_sp_rf_rst_instb_n_21;
  wire rams_sp_rf_rst_instb_n_22;
  wire rams_sp_rf_rst_instb_n_23;
  wire rams_sp_rf_rst_instb_n_24;
  wire rams_sp_rf_rst_instb_n_25;
  wire rams_sp_rf_rst_instb_n_26;
  wire rams_sp_rf_rst_instb_n_27;
  wire rams_sp_rf_rst_instb_n_28;
  wire rams_sp_rf_rst_instb_n_29;
  wire rams_sp_rf_rst_instb_n_3;
  wire rams_sp_rf_rst_instb_n_30;
  wire rams_sp_rf_rst_instb_n_31;
  wire rams_sp_rf_rst_instb_n_4;
  wire rams_sp_rf_rst_instb_n_5;
  wire rams_sp_rf_rst_instb_n_6;
  wire rams_sp_rf_rst_instb_n_7;
  wire rams_sp_rf_rst_instb_n_8;
  wire rams_sp_rf_rst_instb_n_9;
  wire rams_sp_rf_rst_instf_n_0;
  wire rams_sp_rf_rst_instf_n_1;
  wire rams_sp_rf_rst_instf_n_10;
  wire rams_sp_rf_rst_instf_n_11;
  wire rams_sp_rf_rst_instf_n_12;
  wire rams_sp_rf_rst_instf_n_13;
  wire rams_sp_rf_rst_instf_n_14;
  wire rams_sp_rf_rst_instf_n_15;
  wire rams_sp_rf_rst_instf_n_16;
  wire rams_sp_rf_rst_instf_n_17;
  wire rams_sp_rf_rst_instf_n_18;
  wire rams_sp_rf_rst_instf_n_19;
  wire rams_sp_rf_rst_instf_n_2;
  wire rams_sp_rf_rst_instf_n_20;
  wire rams_sp_rf_rst_instf_n_21;
  wire rams_sp_rf_rst_instf_n_22;
  wire rams_sp_rf_rst_instf_n_23;
  wire rams_sp_rf_rst_instf_n_24;
  wire rams_sp_rf_rst_instf_n_25;
  wire rams_sp_rf_rst_instf_n_26;
  wire rams_sp_rf_rst_instf_n_27;
  wire rams_sp_rf_rst_instf_n_28;
  wire rams_sp_rf_rst_instf_n_29;
  wire rams_sp_rf_rst_instf_n_3;
  wire rams_sp_rf_rst_instf_n_30;
  wire rams_sp_rf_rst_instf_n_31;
  wire rams_sp_rf_rst_instf_n_4;
  wire rams_sp_rf_rst_instf_n_5;
  wire rams_sp_rf_rst_instf_n_6;
  wire rams_sp_rf_rst_instf_n_7;
  wire rams_sp_rf_rst_instf_n_8;
  wire rams_sp_rf_rst_instf_n_9;
  wire [31:0]s00_axis_tdata;
  wire [26:0]s00_axis_tuser;
  wire s00_axis_tvalid;
  wire square_r;
  wire square_r_i_1_n_0;
  wire [3:2]NLW_cnt_inner_valid0_carry__2_CO_UNCONNECTED;
  wire [3:3]NLW_cnt_inner_valid0_carry__2_O_UNCONNECTED;

  FDRE \DI0_reg[0] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_tvalid),
        .D(s00_axis_tdata[0]),
        .Q(DI0[0]),
        .R(1'b0));
  FDRE \DI0_reg[10] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_tvalid),
        .D(s00_axis_tdata[10]),
        .Q(DI0[10]),
        .R(1'b0));
  FDRE \DI0_reg[11] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_tvalid),
        .D(s00_axis_tdata[11]),
        .Q(DI0[11]),
        .R(1'b0));
  FDRE \DI0_reg[12] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_tvalid),
        .D(s00_axis_tdata[12]),
        .Q(DI0[12]),
        .R(1'b0));
  FDRE \DI0_reg[13] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_tvalid),
        .D(s00_axis_tdata[13]),
        .Q(DI0[13]),
        .R(1'b0));
  FDRE \DI0_reg[14] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_tvalid),
        .D(s00_axis_tdata[14]),
        .Q(DI0[14]),
        .R(1'b0));
  FDRE \DI0_reg[15] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_tvalid),
        .D(s00_axis_tdata[15]),
        .Q(DI0[15]),
        .R(1'b0));
  FDRE \DI0_reg[16] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_tvalid),
        .D(s00_axis_tdata[16]),
        .Q(DI0[16]),
        .R(1'b0));
  FDRE \DI0_reg[17] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_tvalid),
        .D(s00_axis_tdata[17]),
        .Q(DI0[17]),
        .R(1'b0));
  FDRE \DI0_reg[18] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_tvalid),
        .D(s00_axis_tdata[18]),
        .Q(DI0[18]),
        .R(1'b0));
  FDRE \DI0_reg[19] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_tvalid),
        .D(s00_axis_tdata[19]),
        .Q(DI0[19]),
        .R(1'b0));
  FDRE \DI0_reg[1] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_tvalid),
        .D(s00_axis_tdata[1]),
        .Q(DI0[1]),
        .R(1'b0));
  FDRE \DI0_reg[20] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_tvalid),
        .D(s00_axis_tdata[20]),
        .Q(DI0[20]),
        .R(1'b0));
  FDRE \DI0_reg[21] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_tvalid),
        .D(s00_axis_tdata[21]),
        .Q(DI0[21]),
        .R(1'b0));
  FDRE \DI0_reg[22] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_tvalid),
        .D(s00_axis_tdata[22]),
        .Q(DI0[22]),
        .R(1'b0));
  FDRE \DI0_reg[23] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_tvalid),
        .D(s00_axis_tdata[23]),
        .Q(DI0[23]),
        .R(1'b0));
  FDRE \DI0_reg[24] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_tvalid),
        .D(s00_axis_tdata[24]),
        .Q(DI0[24]),
        .R(1'b0));
  FDRE \DI0_reg[25] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_tvalid),
        .D(s00_axis_tdata[25]),
        .Q(DI0[25]),
        .R(1'b0));
  FDRE \DI0_reg[26] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_tvalid),
        .D(s00_axis_tdata[26]),
        .Q(DI0[26]),
        .R(1'b0));
  FDRE \DI0_reg[27] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_tvalid),
        .D(s00_axis_tdata[27]),
        .Q(DI0[27]),
        .R(1'b0));
  FDRE \DI0_reg[28] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_tvalid),
        .D(s00_axis_tdata[28]),
        .Q(DI0[28]),
        .R(1'b0));
  FDRE \DI0_reg[29] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_tvalid),
        .D(s00_axis_tdata[29]),
        .Q(DI0[29]),
        .R(1'b0));
  FDRE \DI0_reg[2] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_tvalid),
        .D(s00_axis_tdata[2]),
        .Q(DI0[2]),
        .R(1'b0));
  FDRE \DI0_reg[30] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_tvalid),
        .D(s00_axis_tdata[30]),
        .Q(DI0[30]),
        .R(1'b0));
  FDRE \DI0_reg[31] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_tvalid),
        .D(s00_axis_tdata[31]),
        .Q(DI0[31]),
        .R(1'b0));
  FDRE \DI0_reg[3] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_tvalid),
        .D(s00_axis_tdata[3]),
        .Q(DI0[3]),
        .R(1'b0));
  FDRE \DI0_reg[4] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_tvalid),
        .D(s00_axis_tdata[4]),
        .Q(DI0[4]),
        .R(1'b0));
  FDRE \DI0_reg[5] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_tvalid),
        .D(s00_axis_tdata[5]),
        .Q(DI0[5]),
        .R(1'b0));
  FDRE \DI0_reg[6] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_tvalid),
        .D(s00_axis_tdata[6]),
        .Q(DI0[6]),
        .R(1'b0));
  FDRE \DI0_reg[7] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_tvalid),
        .D(s00_axis_tdata[7]),
        .Q(DI0[7]),
        .R(1'b0));
  FDRE \DI0_reg[8] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_tvalid),
        .D(s00_axis_tdata[8]),
        .Q(DI0[8]),
        .R(1'b0));
  FDRE \DI0_reg[9] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_tvalid),
        .D(s00_axis_tdata[9]),
        .Q(DI0[9]),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    \EN_r[0]_i_1 
       (.I0(\cnt_inner_valid_reg_n_0_[4] ),
        .I1(\cnt_inner_valid_reg_n_0_[7] ),
        .I2(\cnt_inner_valid_reg_n_0_[9] ),
        .I3(\cnt_inner_valid_reg_n_0_[8] ),
        .I4(\cnt_inner_valid_reg_n_0_[10] ),
        .I5(m00_axis_tvalid_r_i_2_n_0),
        .O(\EN_r[0]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h8AAA)) 
    \EN_r[11]_i_1 
       (.I0(s00_axis_tvalid),
        .I1(\EN_r[11]_i_2_n_0 ),
        .I2(\EN_r[9]_i_2_n_0 ),
        .I3(\cnt_inner_valid_reg_n_0_[5] ),
        .O(\EN_r[11]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \EN_r[11]_i_2 
       (.I0(\cnt_inner_valid_reg_n_0_[12] ),
        .I1(\cnt_inner_valid_reg_n_0_[13] ),
        .I2(\cnt_inner_valid_reg_n_0_[15] ),
        .I3(\cnt_inner_valid_reg_n_0_[14] ),
        .I4(\cnt_inner_valid_reg_n_0_[11] ),
        .I5(\cnt_inner_valid_reg_n_0_[6] ),
        .O(\EN_r[11]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'h8AAA)) 
    \EN_r[13]_i_1 
       (.I0(s00_axis_tvalid),
        .I1(\cnt_inner_valid_reg_n_0_[5] ),
        .I2(\EN_r[15]_i_3_n_0 ),
        .I3(\EN_r[9]_i_2_n_0 ),
        .O(\EN_r[13]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \EN_r[14]_i_1 
       (.I0(\cnt_inner_valid_reg_n_0_[7] ),
        .I1(\cnt_inner_valid_reg_n_0_[4] ),
        .O(\EN_r[14]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h2AAA)) 
    \EN_r[15]_i_1 
       (.I0(s00_axis_tvalid),
        .I1(\EN_r[15]_i_3_n_0 ),
        .I2(\EN_r[9]_i_2_n_0 ),
        .I3(\cnt_inner_valid_reg_n_0_[5] ),
        .O(\EN_r[15]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \EN_r[15]_i_2 
       (.I0(\cnt_inner_valid_reg_n_0_[4] ),
        .I1(\cnt_inner_valid_reg_n_0_[7] ),
        .O(\EN_r[15]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000002)) 
    \EN_r[15]_i_3 
       (.I0(\cnt_inner_valid_reg_n_0_[6] ),
        .I1(\cnt_inner_valid_reg_n_0_[12] ),
        .I2(\cnt_inner_valid_reg_n_0_[13] ),
        .I3(\cnt_inner_valid_reg_n_0_[15] ),
        .I4(\cnt_inner_valid_reg_n_0_[14] ),
        .I5(\cnt_inner_valid_reg_n_0_[11] ),
        .O(\EN_r[15]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000002)) 
    \EN_r[1]_i_1 
       (.I0(\cnt_inner_valid_reg_n_0_[4] ),
        .I1(\cnt_inner_valid_reg_n_0_[7] ),
        .I2(\cnt_inner_valid_reg_n_0_[9] ),
        .I3(\cnt_inner_valid_reg_n_0_[8] ),
        .I4(\cnt_inner_valid_reg_n_0_[10] ),
        .I5(m00_axis_tvalid_r_i_2_n_0),
        .O(\EN_r[1]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    \EN_r[6]_i_1 
       (.I0(\cnt_inner_valid_reg_n_0_[4] ),
        .I1(\cnt_inner_valid_reg_n_0_[7] ),
        .O(\EN_r[6]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \EN_r[7]_i_1 
       (.I0(\cnt_inner_valid_reg_n_0_[4] ),
        .I1(\cnt_inner_valid_reg_n_0_[7] ),
        .O(\EN_r[7]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT4 #(
    .INIT(16'h0200)) 
    \EN_r[8]_i_1 
       (.I0(\EN_r[9]_i_2_n_0 ),
        .I1(m00_axis_tvalid_r_i_2_n_0),
        .I2(\cnt_inner_valid_reg_n_0_[4] ),
        .I3(\cnt_inner_valid_reg_n_0_[7] ),
        .O(\EN_r[8]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT4 #(
    .INIT(16'h0080)) 
    \EN_r[9]_i_1 
       (.I0(\cnt_inner_valid_reg_n_0_[4] ),
        .I1(\cnt_inner_valid_reg_n_0_[7] ),
        .I2(\EN_r[9]_i_2_n_0 ),
        .I3(m00_axis_tvalid_r_i_2_n_0),
        .O(\EN_r[9]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT4 #(
    .INIT(16'h0100)) 
    \EN_r[9]_i_2 
       (.I0(\cnt_inner_valid_reg_n_0_[10] ),
        .I1(\cnt_inner_valid_reg_n_0_[8] ),
        .I2(\cnt_inner_valid_reg_n_0_[9] ),
        .I3(m00_axis_tvalid_r_i_3_n_0),
        .O(\EN_r[9]_i_2_n_0 ));
  FDRE \EN_r_reg[0] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_tvalid),
        .D(\EN_r[0]_i_1_n_0 ),
        .Q(EN_r[0]),
        .R(1'b0));
  FDRE \EN_r_reg[10] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_tvalid),
        .D(\EN_r[14]_i_1_n_0 ),
        .Q(EN_r[10]),
        .R(\EN_r[11]_i_1_n_0 ));
  FDRE \EN_r_reg[11] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_tvalid),
        .D(\EN_r[15]_i_2_n_0 ),
        .Q(EN_r[11]),
        .R(\EN_r[11]_i_1_n_0 ));
  FDRE \EN_r_reg[12] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_tvalid),
        .D(\EN_r[14]_i_1_n_0 ),
        .Q(EN_r[12]),
        .R(\EN_r[13]_i_1_n_0 ));
  FDRE \EN_r_reg[13] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_tvalid),
        .D(\EN_r[15]_i_2_n_0 ),
        .Q(EN_r[13]),
        .R(\EN_r[13]_i_1_n_0 ));
  FDRE \EN_r_reg[14] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_tvalid),
        .D(\EN_r[14]_i_1_n_0 ),
        .Q(EN_r[14]),
        .R(\EN_r[15]_i_1_n_0 ));
  FDRE \EN_r_reg[15] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_tvalid),
        .D(\EN_r[15]_i_2_n_0 ),
        .Q(EN_r[15]),
        .R(\EN_r[15]_i_1_n_0 ));
  FDRE \EN_r_reg[1] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_tvalid),
        .D(\EN_r[1]_i_1_n_0 ),
        .Q(EN_r[1]),
        .R(1'b0));
  FDRE \EN_r_reg[2] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_tvalid),
        .D(\EN_r[6]_i_1_n_0 ),
        .Q(EN_r[2]),
        .R(\EN_r[11]_i_1_n_0 ));
  FDRE \EN_r_reg[3] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_tvalid),
        .D(\EN_r[7]_i_1_n_0 ),
        .Q(EN_r[3]),
        .R(\EN_r[11]_i_1_n_0 ));
  FDRE \EN_r_reg[4] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_tvalid),
        .D(\EN_r[6]_i_1_n_0 ),
        .Q(EN_r[4]),
        .R(\EN_r[13]_i_1_n_0 ));
  FDRE \EN_r_reg[5] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_tvalid),
        .D(\EN_r[7]_i_1_n_0 ),
        .Q(EN_r[5]),
        .R(\EN_r[13]_i_1_n_0 ));
  FDRE \EN_r_reg[6] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_tvalid),
        .D(\EN_r[6]_i_1_n_0 ),
        .Q(EN_r[6]),
        .R(\EN_r[15]_i_1_n_0 ));
  FDRE \EN_r_reg[7] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_tvalid),
        .D(\EN_r[7]_i_1_n_0 ),
        .Q(EN_r[7]),
        .R(\EN_r[15]_i_1_n_0 ));
  FDRE \EN_r_reg[8] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_tvalid),
        .D(\EN_r[8]_i_1_n_0 ),
        .Q(EN_r[8]),
        .R(1'b0));
  FDRE \EN_r_reg[9] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_tvalid),
        .D(\EN_r[9]_i_1_n_0 ),
        .Q(EN_r[9]),
        .R(1'b0));
  FDRE WE_r_reg
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(s00_axis_tvalid),
        .Q(WE_r),
        .R(1'b0));
  (* ORIG_CELL_NAME = "adr_wr_reg[0]" *) 
  FDSE \adr_wr_reg[0] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(s00_axis_tuser[0]),
        .Q(adr_wr[0]),
        .S(p_0_in));
  (* ORIG_CELL_NAME = "adr_wr_reg[0]" *) 
  FDSE \adr_wr_reg[0]_rep 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(s00_axis_tuser[0]),
        .Q(\adr_wr_reg[0]_rep_n_0 ),
        .S(p_0_in));
  (* ORIG_CELL_NAME = "adr_wr_reg[10]" *) 
  FDSE \adr_wr_reg[10] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(s00_axis_tuser[10]),
        .Q(adr_wr[10]),
        .S(p_0_in));
  (* ORIG_CELL_NAME = "adr_wr_reg[10]" *) 
  FDSE \adr_wr_reg[10]_rep 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(s00_axis_tuser[10]),
        .Q(\adr_wr_reg[10]_rep_n_0 ),
        .S(p_0_in));
  (* ORIG_CELL_NAME = "adr_wr_reg[1]" *) 
  FDSE \adr_wr_reg[1] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(s00_axis_tuser[1]),
        .Q(adr_wr[1]),
        .S(p_0_in));
  (* ORIG_CELL_NAME = "adr_wr_reg[1]" *) 
  FDSE \adr_wr_reg[1]_rep 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(s00_axis_tuser[1]),
        .Q(\adr_wr_reg[1]_rep_n_0 ),
        .S(p_0_in));
  (* ORIG_CELL_NAME = "adr_wr_reg[2]" *) 
  FDSE \adr_wr_reg[2] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(s00_axis_tuser[2]),
        .Q(adr_wr[2]),
        .S(p_0_in));
  (* ORIG_CELL_NAME = "adr_wr_reg[2]" *) 
  FDSE \adr_wr_reg[2]_rep 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(s00_axis_tuser[2]),
        .Q(\adr_wr_reg[2]_rep_n_0 ),
        .S(p_0_in));
  (* ORIG_CELL_NAME = "adr_wr_reg[3]" *) 
  FDSE \adr_wr_reg[3] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(s00_axis_tuser[3]),
        .Q(adr_wr[3]),
        .S(p_0_in));
  (* ORIG_CELL_NAME = "adr_wr_reg[3]" *) 
  FDSE \adr_wr_reg[3]_rep 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(s00_axis_tuser[3]),
        .Q(\adr_wr_reg[3]_rep_n_0 ),
        .S(p_0_in));
  (* ORIG_CELL_NAME = "adr_wr_reg[4]" *) 
  FDSE \adr_wr_reg[4] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(s00_axis_tuser[4]),
        .Q(adr_wr[4]),
        .S(p_0_in));
  (* ORIG_CELL_NAME = "adr_wr_reg[4]" *) 
  FDSE \adr_wr_reg[4]_rep 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(s00_axis_tuser[4]),
        .Q(\adr_wr_reg[4]_rep_n_0 ),
        .S(p_0_in));
  (* ORIG_CELL_NAME = "adr_wr_reg[5]" *) 
  FDSE \adr_wr_reg[5] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(s00_axis_tuser[5]),
        .Q(adr_wr[5]),
        .S(p_0_in));
  (* ORIG_CELL_NAME = "adr_wr_reg[5]" *) 
  FDSE \adr_wr_reg[5]_rep 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(s00_axis_tuser[5]),
        .Q(\adr_wr_reg[5]_rep_n_0 ),
        .S(p_0_in));
  (* ORIG_CELL_NAME = "adr_wr_reg[6]" *) 
  FDSE \adr_wr_reg[6] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(s00_axis_tuser[6]),
        .Q(adr_wr[6]),
        .S(p_0_in));
  (* ORIG_CELL_NAME = "adr_wr_reg[6]" *) 
  FDSE \adr_wr_reg[6]_rep 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(s00_axis_tuser[6]),
        .Q(\adr_wr_reg[6]_rep_n_0 ),
        .S(p_0_in));
  (* ORIG_CELL_NAME = "adr_wr_reg[7]" *) 
  FDSE \adr_wr_reg[7] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(s00_axis_tuser[7]),
        .Q(adr_wr[7]),
        .S(p_0_in));
  (* ORIG_CELL_NAME = "adr_wr_reg[7]" *) 
  FDSE \adr_wr_reg[7]_rep 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(s00_axis_tuser[7]),
        .Q(\adr_wr_reg[7]_rep_n_0 ),
        .S(p_0_in));
  (* ORIG_CELL_NAME = "adr_wr_reg[8]" *) 
  FDSE \adr_wr_reg[8] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(s00_axis_tuser[8]),
        .Q(adr_wr[8]),
        .S(p_0_in));
  (* ORIG_CELL_NAME = "adr_wr_reg[8]" *) 
  FDSE \adr_wr_reg[8]_rep 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(s00_axis_tuser[8]),
        .Q(\adr_wr_reg[8]_rep_n_0 ),
        .S(p_0_in));
  (* ORIG_CELL_NAME = "adr_wr_reg[9]" *) 
  FDSE \adr_wr_reg[9] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(s00_axis_tuser[9]),
        .Q(adr_wr[9]),
        .S(p_0_in));
  (* ORIG_CELL_NAME = "adr_wr_reg[9]" *) 
  FDSE \adr_wr_reg[9]_rep 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(s00_axis_tuser[9]),
        .Q(\adr_wr_reg[9]_rep_n_0 ),
        .S(p_0_in));
  CARRY4 cnt_inner_valid0_carry
       (.CI(1'b0),
        .CO({cnt_inner_valid0_carry_n_0,cnt_inner_valid0_carry_n_1,cnt_inner_valid0_carry_n_2,cnt_inner_valid0_carry_n_3}),
        .CYINIT(\cnt_inner_valid_reg_n_0_[0] ),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(data0[4:1]),
        .S({\cnt_inner_valid_reg_n_0_[4] ,\cnt_inner_valid_reg_n_0_[3] ,\cnt_inner_valid_reg_n_0_[2] ,\cnt_inner_valid_reg_n_0_[1] }));
  CARRY4 cnt_inner_valid0_carry__0
       (.CI(cnt_inner_valid0_carry_n_0),
        .CO({cnt_inner_valid0_carry__0_n_0,cnt_inner_valid0_carry__0_n_1,cnt_inner_valid0_carry__0_n_2,cnt_inner_valid0_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(data0[8:5]),
        .S({\cnt_inner_valid_reg_n_0_[8] ,\cnt_inner_valid_reg_n_0_[7] ,\cnt_inner_valid_reg_n_0_[6] ,\cnt_inner_valid_reg_n_0_[5] }));
  CARRY4 cnt_inner_valid0_carry__1
       (.CI(cnt_inner_valid0_carry__0_n_0),
        .CO({cnt_inner_valid0_carry__1_n_0,cnt_inner_valid0_carry__1_n_1,cnt_inner_valid0_carry__1_n_2,cnt_inner_valid0_carry__1_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(data0[12:9]),
        .S({\cnt_inner_valid_reg_n_0_[12] ,\cnt_inner_valid_reg_n_0_[11] ,\cnt_inner_valid_reg_n_0_[10] ,\cnt_inner_valid_reg_n_0_[9] }));
  CARRY4 cnt_inner_valid0_carry__2
       (.CI(cnt_inner_valid0_carry__1_n_0),
        .CO({NLW_cnt_inner_valid0_carry__2_CO_UNCONNECTED[3:2],cnt_inner_valid0_carry__2_n_2,cnt_inner_valid0_carry__2_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({NLW_cnt_inner_valid0_carry__2_O_UNCONNECTED[3],data0[15:13]}),
        .S({1'b0,\cnt_inner_valid_reg_n_0_[15] ,\cnt_inner_valid_reg_n_0_[14] ,\cnt_inner_valid_reg_n_0_[13] }));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT5 #(
    .INIT(32'h00FF00FE)) 
    \cnt_inner_valid[0]_i_1 
       (.I0(\cnt_inner_valid_reg_n_0_[3] ),
        .I1(\cnt_inner_valid_reg_n_0_[2] ),
        .I2(\cnt_inner_valid_reg_n_0_[1] ),
        .I3(\cnt_inner_valid_reg_n_0_[0] ),
        .I4(\cnt_inner_valid[15]_i_3_n_0 ),
        .O(cnt_inner_valid[0]));
  LUT6 #(
    .INIT(64'hFFFFFFFE00000000)) 
    \cnt_inner_valid[10]_i_1 
       (.I0(\cnt_inner_valid[15]_i_3_n_0 ),
        .I1(\cnt_inner_valid_reg_n_0_[0] ),
        .I2(\cnt_inner_valid_reg_n_0_[3] ),
        .I3(\cnt_inner_valid_reg_n_0_[2] ),
        .I4(\cnt_inner_valid_reg_n_0_[1] ),
        .I5(data0[10]),
        .O(cnt_inner_valid[10]));
  LUT6 #(
    .INIT(64'hFFFFFFFE00000000)) 
    \cnt_inner_valid[11]_i_1 
       (.I0(\cnt_inner_valid[15]_i_3_n_0 ),
        .I1(\cnt_inner_valid_reg_n_0_[0] ),
        .I2(\cnt_inner_valid_reg_n_0_[3] ),
        .I3(\cnt_inner_valid_reg_n_0_[2] ),
        .I4(\cnt_inner_valid_reg_n_0_[1] ),
        .I5(data0[11]),
        .O(cnt_inner_valid[11]));
  LUT6 #(
    .INIT(64'hFFFFFFFE00000000)) 
    \cnt_inner_valid[12]_i_1 
       (.I0(\cnt_inner_valid[15]_i_3_n_0 ),
        .I1(\cnt_inner_valid_reg_n_0_[0] ),
        .I2(\cnt_inner_valid_reg_n_0_[3] ),
        .I3(\cnt_inner_valid_reg_n_0_[2] ),
        .I4(\cnt_inner_valid_reg_n_0_[1] ),
        .I5(data0[12]),
        .O(cnt_inner_valid[12]));
  LUT6 #(
    .INIT(64'hFFFFFFFE00000000)) 
    \cnt_inner_valid[13]_i_1 
       (.I0(\cnt_inner_valid[15]_i_3_n_0 ),
        .I1(\cnt_inner_valid_reg_n_0_[0] ),
        .I2(\cnt_inner_valid_reg_n_0_[3] ),
        .I3(\cnt_inner_valid_reg_n_0_[2] ),
        .I4(\cnt_inner_valid_reg_n_0_[1] ),
        .I5(data0[13]),
        .O(cnt_inner_valid[13]));
  LUT6 #(
    .INIT(64'hFFFFFFFE00000000)) 
    \cnt_inner_valid[14]_i_1 
       (.I0(\cnt_inner_valid[15]_i_3_n_0 ),
        .I1(\cnt_inner_valid_reg_n_0_[0] ),
        .I2(\cnt_inner_valid_reg_n_0_[3] ),
        .I3(\cnt_inner_valid_reg_n_0_[2] ),
        .I4(\cnt_inner_valid_reg_n_0_[1] ),
        .I5(data0[14]),
        .O(cnt_inner_valid[14]));
  LUT1 #(
    .INIT(2'h1)) 
    \cnt_inner_valid[15]_i_1 
       (.I0(s00_axis_tvalid),
        .O(p_0_in));
  LUT6 #(
    .INIT(64'hFFFFFFFE00000000)) 
    \cnt_inner_valid[15]_i_2 
       (.I0(\cnt_inner_valid[15]_i_3_n_0 ),
        .I1(\cnt_inner_valid_reg_n_0_[0] ),
        .I2(\cnt_inner_valid_reg_n_0_[3] ),
        .I3(\cnt_inner_valid_reg_n_0_[2] ),
        .I4(\cnt_inner_valid_reg_n_0_[1] ),
        .I5(data0[15]),
        .O(cnt_inner_valid[15]));
  LUT6 #(
    .INIT(64'hFFFFFF7FFFFFFFFF)) 
    \cnt_inner_valid[15]_i_3 
       (.I0(\cnt_inner_valid_reg_n_0_[8] ),
        .I1(\cnt_inner_valid_reg_n_0_[9] ),
        .I2(\cnt_inner_valid_reg_n_0_[10] ),
        .I3(m00_axis_tvalid_r_i_2_n_0),
        .I4(\cnt_inner_valid_reg_n_0_[4] ),
        .I5(\cnt_inner_valid_reg_n_0_[7] ),
        .O(\cnt_inner_valid[15]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFE00000000)) 
    \cnt_inner_valid[1]_i_1 
       (.I0(\cnt_inner_valid[15]_i_3_n_0 ),
        .I1(\cnt_inner_valid_reg_n_0_[0] ),
        .I2(\cnt_inner_valid_reg_n_0_[3] ),
        .I3(\cnt_inner_valid_reg_n_0_[2] ),
        .I4(\cnt_inner_valid_reg_n_0_[1] ),
        .I5(data0[1]),
        .O(cnt_inner_valid[1]));
  LUT6 #(
    .INIT(64'hFFFFFFFE00000000)) 
    \cnt_inner_valid[2]_i_1 
       (.I0(\cnt_inner_valid[15]_i_3_n_0 ),
        .I1(\cnt_inner_valid_reg_n_0_[0] ),
        .I2(\cnt_inner_valid_reg_n_0_[3] ),
        .I3(\cnt_inner_valid_reg_n_0_[2] ),
        .I4(\cnt_inner_valid_reg_n_0_[1] ),
        .I5(data0[2]),
        .O(cnt_inner_valid[2]));
  LUT6 #(
    .INIT(64'hFFFFFFFE00000000)) 
    \cnt_inner_valid[3]_i_1 
       (.I0(\cnt_inner_valid[15]_i_3_n_0 ),
        .I1(\cnt_inner_valid_reg_n_0_[0] ),
        .I2(\cnt_inner_valid_reg_n_0_[3] ),
        .I3(\cnt_inner_valid_reg_n_0_[2] ),
        .I4(\cnt_inner_valid_reg_n_0_[1] ),
        .I5(data0[3]),
        .O(cnt_inner_valid[3]));
  LUT6 #(
    .INIT(64'hFFFFFFFE00000000)) 
    \cnt_inner_valid[4]_i_1 
       (.I0(\cnt_inner_valid[15]_i_3_n_0 ),
        .I1(\cnt_inner_valid_reg_n_0_[0] ),
        .I2(\cnt_inner_valid_reg_n_0_[3] ),
        .I3(\cnt_inner_valid_reg_n_0_[2] ),
        .I4(\cnt_inner_valid_reg_n_0_[1] ),
        .I5(data0[4]),
        .O(cnt_inner_valid[4]));
  LUT6 #(
    .INIT(64'hFFFFFFFE00000000)) 
    \cnt_inner_valid[5]_i_1 
       (.I0(\cnt_inner_valid[15]_i_3_n_0 ),
        .I1(\cnt_inner_valid_reg_n_0_[0] ),
        .I2(\cnt_inner_valid_reg_n_0_[3] ),
        .I3(\cnt_inner_valid_reg_n_0_[2] ),
        .I4(\cnt_inner_valid_reg_n_0_[1] ),
        .I5(data0[5]),
        .O(cnt_inner_valid[5]));
  LUT6 #(
    .INIT(64'hFFFFFFFE00000000)) 
    \cnt_inner_valid[6]_i_1 
       (.I0(\cnt_inner_valid[15]_i_3_n_0 ),
        .I1(\cnt_inner_valid_reg_n_0_[0] ),
        .I2(\cnt_inner_valid_reg_n_0_[3] ),
        .I3(\cnt_inner_valid_reg_n_0_[2] ),
        .I4(\cnt_inner_valid_reg_n_0_[1] ),
        .I5(data0[6]),
        .O(cnt_inner_valid[6]));
  LUT6 #(
    .INIT(64'hFFFFFFFE00000000)) 
    \cnt_inner_valid[7]_i_1 
       (.I0(\cnt_inner_valid[15]_i_3_n_0 ),
        .I1(\cnt_inner_valid_reg_n_0_[0] ),
        .I2(\cnt_inner_valid_reg_n_0_[3] ),
        .I3(\cnt_inner_valid_reg_n_0_[2] ),
        .I4(\cnt_inner_valid_reg_n_0_[1] ),
        .I5(data0[7]),
        .O(cnt_inner_valid[7]));
  LUT6 #(
    .INIT(64'hFFFFFFFE00000000)) 
    \cnt_inner_valid[8]_i_1 
       (.I0(\cnt_inner_valid[15]_i_3_n_0 ),
        .I1(\cnt_inner_valid_reg_n_0_[0] ),
        .I2(\cnt_inner_valid_reg_n_0_[3] ),
        .I3(\cnt_inner_valid_reg_n_0_[2] ),
        .I4(\cnt_inner_valid_reg_n_0_[1] ),
        .I5(data0[8]),
        .O(cnt_inner_valid[8]));
  LUT6 #(
    .INIT(64'hFFFFFFFE00000000)) 
    \cnt_inner_valid[9]_i_1 
       (.I0(\cnt_inner_valid[15]_i_3_n_0 ),
        .I1(\cnt_inner_valid_reg_n_0_[0] ),
        .I2(\cnt_inner_valid_reg_n_0_[3] ),
        .I3(\cnt_inner_valid_reg_n_0_[2] ),
        .I4(\cnt_inner_valid_reg_n_0_[1] ),
        .I5(data0[9]),
        .O(cnt_inner_valid[9]));
  FDRE \cnt_inner_valid_reg[0] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(cnt_inner_valid[0]),
        .Q(\cnt_inner_valid_reg_n_0_[0] ),
        .R(p_0_in));
  FDRE \cnt_inner_valid_reg[10] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(cnt_inner_valid[10]),
        .Q(\cnt_inner_valid_reg_n_0_[10] ),
        .R(p_0_in));
  FDRE \cnt_inner_valid_reg[11] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(cnt_inner_valid[11]),
        .Q(\cnt_inner_valid_reg_n_0_[11] ),
        .R(p_0_in));
  FDRE \cnt_inner_valid_reg[12] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(cnt_inner_valid[12]),
        .Q(\cnt_inner_valid_reg_n_0_[12] ),
        .R(p_0_in));
  FDRE \cnt_inner_valid_reg[13] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(cnt_inner_valid[13]),
        .Q(\cnt_inner_valid_reg_n_0_[13] ),
        .R(p_0_in));
  FDRE \cnt_inner_valid_reg[14] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(cnt_inner_valid[14]),
        .Q(\cnt_inner_valid_reg_n_0_[14] ),
        .R(p_0_in));
  FDRE \cnt_inner_valid_reg[15] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(cnt_inner_valid[15]),
        .Q(\cnt_inner_valid_reg_n_0_[15] ),
        .R(p_0_in));
  FDRE \cnt_inner_valid_reg[1] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(cnt_inner_valid[1]),
        .Q(\cnt_inner_valid_reg_n_0_[1] ),
        .R(p_0_in));
  FDRE \cnt_inner_valid_reg[2] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(cnt_inner_valid[2]),
        .Q(\cnt_inner_valid_reg_n_0_[2] ),
        .R(p_0_in));
  FDRE \cnt_inner_valid_reg[3] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(cnt_inner_valid[3]),
        .Q(\cnt_inner_valid_reg_n_0_[3] ),
        .R(p_0_in));
  FDRE \cnt_inner_valid_reg[4] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(cnt_inner_valid[4]),
        .Q(\cnt_inner_valid_reg_n_0_[4] ),
        .R(p_0_in));
  FDRE \cnt_inner_valid_reg[5] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(cnt_inner_valid[5]),
        .Q(\cnt_inner_valid_reg_n_0_[5] ),
        .R(p_0_in));
  FDRE \cnt_inner_valid_reg[6] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(cnt_inner_valid[6]),
        .Q(\cnt_inner_valid_reg_n_0_[6] ),
        .R(p_0_in));
  FDRE \cnt_inner_valid_reg[7] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(cnt_inner_valid[7]),
        .Q(\cnt_inner_valid_reg_n_0_[7] ),
        .R(p_0_in));
  FDRE \cnt_inner_valid_reg[8] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(cnt_inner_valid[8]),
        .Q(\cnt_inner_valid_reg_n_0_[8] ),
        .R(p_0_in));
  FDRE \cnt_inner_valid_reg[9] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(cnt_inner_valid[9]),
        .Q(\cnt_inner_valid_reg_n_0_[9] ),
        .R(p_0_in));
  LUT3 #(
    .INIT(8'h20)) 
    \m00_axis_tdata_r[31]_i_1 
       (.I0(\m00_axis_tdata_r[31]_i_3_n_0 ),
        .I1(\m00_axis_tdata_r[31]_i_4_n_0 ),
        .I2(s00_axis_tvalid),
        .O(\m00_axis_tdata_r[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    \m00_axis_tdata_r[31]_i_3 
       (.I0(s00_axis_tuser[19]),
        .I1(s00_axis_tuser[24]),
        .I2(s00_axis_tuser[26]),
        .I3(s00_axis_tuser[21]),
        .I4(s00_axis_tuser[25]),
        .I5(s00_axis_tuser[22]),
        .O(\m00_axis_tdata_r[31]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFE0FFFF)) 
    \m00_axis_tdata_r[31]_i_4 
       (.I0(s00_axis_tuser[16]),
        .I1(s00_axis_tuser[17]),
        .I2(s00_axis_tuser[18]),
        .I3(s00_axis_tuser[23]),
        .I4(s00_axis_tuser[15]),
        .I5(s00_axis_tuser[20]),
        .O(\m00_axis_tdata_r[31]_i_4_n_0 ));
  FDRE \m00_axis_tdata_r_reg[0] 
       (.C(m00_axis_aclk),
        .CE(\m00_axis_tdata_r[31]_i_1_n_0 ),
        .D(rams_sp_rf_rst_inst7_n_8),
        .Q(m00_axis_tdata[0]),
        .R(1'b0));
  FDRE \m00_axis_tdata_r_reg[10] 
       (.C(m00_axis_aclk),
        .CE(\m00_axis_tdata_r[31]_i_1_n_0 ),
        .D(rams_sp_rf_rst_instf_n_14),
        .Q(m00_axis_tdata[10]),
        .R(1'b0));
  FDRE \m00_axis_tdata_r_reg[11] 
       (.C(m00_axis_aclk),
        .CE(\m00_axis_tdata_r[31]_i_1_n_0 ),
        .D(rams_sp_rf_rst_inst7_n_5),
        .Q(m00_axis_tdata[11]),
        .R(1'b0));
  FDRE \m00_axis_tdata_r_reg[12] 
       (.C(m00_axis_aclk),
        .CE(\m00_axis_tdata_r[31]_i_1_n_0 ),
        .D(rams_sp_rf_rst_instf_n_13),
        .Q(m00_axis_tdata[12]),
        .R(1'b0));
  FDRE \m00_axis_tdata_r_reg[13] 
       (.C(m00_axis_aclk),
        .CE(\m00_axis_tdata_r[31]_i_1_n_0 ),
        .D(rams_sp_rf_rst_instf_n_12),
        .Q(m00_axis_tdata[13]),
        .R(1'b0));
  FDRE \m00_axis_tdata_r_reg[14] 
       (.C(m00_axis_aclk),
        .CE(\m00_axis_tdata_r[31]_i_1_n_0 ),
        .D(rams_sp_rf_rst_instf_n_11),
        .Q(m00_axis_tdata[14]),
        .R(1'b0));
  FDRE \m00_axis_tdata_r_reg[15] 
       (.C(m00_axis_aclk),
        .CE(\m00_axis_tdata_r[31]_i_1_n_0 ),
        .D(rams_sp_rf_rst_inst7_n_4),
        .Q(m00_axis_tdata[15]),
        .R(1'b0));
  FDRE \m00_axis_tdata_r_reg[16] 
       (.C(m00_axis_aclk),
        .CE(\m00_axis_tdata_r[31]_i_1_n_0 ),
        .D(rams_sp_rf_rst_inst7_n_3),
        .Q(m00_axis_tdata[16]),
        .R(1'b0));
  FDRE \m00_axis_tdata_r_reg[17] 
       (.C(m00_axis_aclk),
        .CE(\m00_axis_tdata_r[31]_i_1_n_0 ),
        .D(rams_sp_rf_rst_instf_n_10),
        .Q(m00_axis_tdata[17]),
        .R(1'b0));
  FDRE \m00_axis_tdata_r_reg[18] 
       (.C(m00_axis_aclk),
        .CE(\m00_axis_tdata_r[31]_i_1_n_0 ),
        .D(rams_sp_rf_rst_instf_n_9),
        .Q(m00_axis_tdata[18]),
        .R(1'b0));
  FDRE \m00_axis_tdata_r_reg[19] 
       (.C(m00_axis_aclk),
        .CE(\m00_axis_tdata_r[31]_i_1_n_0 ),
        .D(rams_sp_rf_rst_instf_n_8),
        .Q(m00_axis_tdata[19]),
        .R(1'b0));
  FDRE \m00_axis_tdata_r_reg[1] 
       (.C(m00_axis_aclk),
        .CE(\m00_axis_tdata_r[31]_i_1_n_0 ),
        .D(rams_sp_rf_rst_instf_n_21),
        .Q(m00_axis_tdata[1]),
        .R(1'b0));
  FDRE \m00_axis_tdata_r_reg[20] 
       (.C(m00_axis_aclk),
        .CE(\m00_axis_tdata_r[31]_i_1_n_0 ),
        .D(rams_sp_rf_rst_inst7_n_2),
        .Q(m00_axis_tdata[20]),
        .R(1'b0));
  FDRE \m00_axis_tdata_r_reg[21] 
       (.C(m00_axis_aclk),
        .CE(\m00_axis_tdata_r[31]_i_1_n_0 ),
        .D(rams_sp_rf_rst_instf_n_7),
        .Q(m00_axis_tdata[21]),
        .R(1'b0));
  FDRE \m00_axis_tdata_r_reg[22] 
       (.C(m00_axis_aclk),
        .CE(\m00_axis_tdata_r[31]_i_1_n_0 ),
        .D(rams_sp_rf_rst_instf_n_6),
        .Q(m00_axis_tdata[22]),
        .R(1'b0));
  FDRE \m00_axis_tdata_r_reg[23] 
       (.C(m00_axis_aclk),
        .CE(\m00_axis_tdata_r[31]_i_1_n_0 ),
        .D(rams_sp_rf_rst_instf_n_5),
        .Q(m00_axis_tdata[23]),
        .R(1'b0));
  FDRE \m00_axis_tdata_r_reg[24] 
       (.C(m00_axis_aclk),
        .CE(\m00_axis_tdata_r[31]_i_1_n_0 ),
        .D(rams_sp_rf_rst_instf_n_4),
        .Q(m00_axis_tdata[24]),
        .R(1'b0));
  FDRE \m00_axis_tdata_r_reg[25] 
       (.C(m00_axis_aclk),
        .CE(\m00_axis_tdata_r[31]_i_1_n_0 ),
        .D(rams_sp_rf_rst_inst7_n_1),
        .Q(m00_axis_tdata[25]),
        .R(1'b0));
  FDRE \m00_axis_tdata_r_reg[26] 
       (.C(m00_axis_aclk),
        .CE(\m00_axis_tdata_r[31]_i_1_n_0 ),
        .D(rams_sp_rf_rst_instf_n_3),
        .Q(m00_axis_tdata[26]),
        .R(1'b0));
  FDRE \m00_axis_tdata_r_reg[27] 
       (.C(m00_axis_aclk),
        .CE(\m00_axis_tdata_r[31]_i_1_n_0 ),
        .D(rams_sp_rf_rst_inst7_n_0),
        .Q(m00_axis_tdata[27]),
        .R(1'b0));
  FDRE \m00_axis_tdata_r_reg[28] 
       (.C(m00_axis_aclk),
        .CE(\m00_axis_tdata_r[31]_i_1_n_0 ),
        .D(rams_sp_rf_rst_instf_n_2),
        .Q(m00_axis_tdata[28]),
        .R(1'b0));
  FDRE \m00_axis_tdata_r_reg[29] 
       (.C(m00_axis_aclk),
        .CE(\m00_axis_tdata_r[31]_i_1_n_0 ),
        .D(rams_sp_rf_rst_instf_n_1),
        .Q(m00_axis_tdata[29]),
        .R(1'b0));
  FDRE \m00_axis_tdata_r_reg[2] 
       (.C(m00_axis_aclk),
        .CE(\m00_axis_tdata_r[31]_i_1_n_0 ),
        .D(rams_sp_rf_rst_instf_n_20),
        .Q(m00_axis_tdata[2]),
        .R(1'b0));
  FDRE \m00_axis_tdata_r_reg[30] 
       (.C(m00_axis_aclk),
        .CE(\m00_axis_tdata_r[31]_i_1_n_0 ),
        .D(rams_sp_rf_rst_instf_n_0),
        .Q(m00_axis_tdata[30]),
        .R(1'b0));
  FDRE \m00_axis_tdata_r_reg[31] 
       (.C(m00_axis_aclk),
        .CE(\m00_axis_tdata_r[31]_i_1_n_0 ),
        .D(rams_sp_rf_rst_inst3_n_0),
        .Q(m00_axis_tdata[31]),
        .R(1'b0));
  FDRE \m00_axis_tdata_r_reg[3] 
       (.C(m00_axis_aclk),
        .CE(\m00_axis_tdata_r[31]_i_1_n_0 ),
        .D(rams_sp_rf_rst_instf_n_19),
        .Q(m00_axis_tdata[3]),
        .R(1'b0));
  FDRE \m00_axis_tdata_r_reg[4] 
       (.C(m00_axis_aclk),
        .CE(\m00_axis_tdata_r[31]_i_1_n_0 ),
        .D(rams_sp_rf_rst_inst7_n_7),
        .Q(m00_axis_tdata[4]),
        .R(1'b0));
  FDRE \m00_axis_tdata_r_reg[5] 
       (.C(m00_axis_aclk),
        .CE(\m00_axis_tdata_r[31]_i_1_n_0 ),
        .D(rams_sp_rf_rst_instf_n_18),
        .Q(m00_axis_tdata[5]),
        .R(1'b0));
  FDRE \m00_axis_tdata_r_reg[6] 
       (.C(m00_axis_aclk),
        .CE(\m00_axis_tdata_r[31]_i_1_n_0 ),
        .D(rams_sp_rf_rst_instf_n_17),
        .Q(m00_axis_tdata[6]),
        .R(1'b0));
  FDRE \m00_axis_tdata_r_reg[7] 
       (.C(m00_axis_aclk),
        .CE(\m00_axis_tdata_r[31]_i_1_n_0 ),
        .D(rams_sp_rf_rst_instf_n_16),
        .Q(m00_axis_tdata[7]),
        .R(1'b0));
  FDRE \m00_axis_tdata_r_reg[8] 
       (.C(m00_axis_aclk),
        .CE(\m00_axis_tdata_r[31]_i_1_n_0 ),
        .D(rams_sp_rf_rst_instf_n_15),
        .Q(m00_axis_tdata[8]),
        .R(1'b0));
  FDRE \m00_axis_tdata_r_reg[9] 
       (.C(m00_axis_aclk),
        .CE(\m00_axis_tdata_r[31]_i_1_n_0 ),
        .D(rams_sp_rf_rst_inst7_n_6),
        .Q(m00_axis_tdata[9]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'h00100040)) 
    m00_axis_tvalid_r_i_1
       (.I0(m00_axis_tvalid_r_i_2_n_0),
        .I1(m00_axis_tvalid_r_i_3_n_0),
        .I2(m00_axis_tvalid_r_i_4_n_0),
        .I3(\cnt_inner_valid_reg_n_0_[7] ),
        .I4(\cnt_inner_valid_reg_n_0_[4] ),
        .O(m00_axis_tvalid_r0));
  LUT2 #(
    .INIT(4'hE)) 
    m00_axis_tvalid_r_i_2
       (.I0(\EN_r[11]_i_2_n_0 ),
        .I1(\cnt_inner_valid_reg_n_0_[5] ),
        .O(m00_axis_tvalid_r_i_2_n_0));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT4 #(
    .INIT(16'hFFFE)) 
    m00_axis_tvalid_r_i_3
       (.I0(\cnt_inner_valid_reg_n_0_[1] ),
        .I1(\cnt_inner_valid_reg_n_0_[2] ),
        .I2(\cnt_inner_valid_reg_n_0_[3] ),
        .I3(\cnt_inner_valid_reg_n_0_[0] ),
        .O(m00_axis_tvalid_r_i_3_n_0));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT3 #(
    .INIT(8'h01)) 
    m00_axis_tvalid_r_i_4
       (.I0(\cnt_inner_valid_reg_n_0_[9] ),
        .I1(\cnt_inner_valid_reg_n_0_[8] ),
        .I2(\cnt_inner_valid_reg_n_0_[10] ),
        .O(m00_axis_tvalid_r_i_4_n_0));
  FDRE m00_axis_tvalid_r_reg
       (.C(m00_axis_aclk),
        .CE(s00_axis_tvalid),
        .D(m00_axis_tvalid_r0),
        .Q(m00_axis_tvalid),
        .R(1'b0));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst rams_sp_rf_rst_inst0
       (.ADDRARDADDR(adr_wr),
        .Q(EN_r[0]),
        .WE_r(WE_r),
        .addr({\adr_wr_reg[10]_rep_n_0 ,\adr_wr_reg[9]_rep_n_0 ,\adr_wr_reg[8]_rep_n_0 ,\adr_wr_reg[7]_rep_n_0 ,\adr_wr_reg[6]_rep_n_0 ,\adr_wr_reg[5]_rep_n_0 ,\adr_wr_reg[4]_rep_n_0 ,\adr_wr_reg[3]_rep_n_0 ,\adr_wr_reg[2]_rep_n_0 ,\adr_wr_reg[1]_rep_n_0 ,\adr_wr_reg[0]_rep_n_0 }),
        .dout0_out(DOUT0),
        .m00_axis_aclk(m00_axis_aclk),
        .ram_reg_1_0(DI0),
        .square_r(square_r));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_0 rams_sp_rf_rst_inst1
       (.ADDRARDADDR(adr_wr),
        .Q(EN_r[1]),
        .WE_r(WE_r),
        .addr({\adr_wr_reg[10]_rep_n_0 ,\adr_wr_reg[9]_rep_n_0 ,\adr_wr_reg[8]_rep_n_0 ,\adr_wr_reg[7]_rep_n_0 ,\adr_wr_reg[6]_rep_n_0 ,\adr_wr_reg[5]_rep_n_0 ,\adr_wr_reg[4]_rep_n_0 ,\adr_wr_reg[3]_rep_n_0 ,\adr_wr_reg[2]_rep_n_0 ,\adr_wr_reg[1]_rep_n_0 ,\adr_wr_reg[0]_rep_n_0 }),
        .dout0_out(DOUT1_w),
        .m00_axis_aclk(m00_axis_aclk),
        .ram_reg_1_0(DI0),
        .square_r(square_r));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_1 rams_sp_rf_rst_inst2
       (.ADDRARDADDR(adr_wr),
        .Q(DI0),
        .WE_r(WE_r),
        .addr({\adr_wr_reg[10]_rep_n_0 ,\adr_wr_reg[9]_rep_n_0 ,\adr_wr_reg[8]_rep_n_0 ,\adr_wr_reg[7]_rep_n_0 ,\adr_wr_reg[6]_rep_n_0 ,\adr_wr_reg[5]_rep_n_0 ,\adr_wr_reg[4]_rep_n_0 ,\adr_wr_reg[3]_rep_n_0 ,\adr_wr_reg[2]_rep_n_0 ,\adr_wr_reg[1]_rep_n_0 ,\adr_wr_reg[0]_rep_n_0 }),
        .dout0_out(DOUT2_w),
        .en(EN_r[2]),
        .m00_axis_aclk(m00_axis_aclk),
        .square_r(square_r));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_2 rams_sp_rf_rst_inst3
       (.ADDRARDADDR(adr_wr),
        .D(rams_sp_rf_rst_inst3_n_0),
        .Q(DI0),
        .WE_r(WE_r),
        .addr({\adr_wr_reg[10]_rep_n_0 ,\adr_wr_reg[9]_rep_n_0 ,\adr_wr_reg[8]_rep_n_0 ,\adr_wr_reg[7]_rep_n_0 ,\adr_wr_reg[6]_rep_n_0 ,\adr_wr_reg[5]_rep_n_0 ,\adr_wr_reg[4]_rep_n_0 ,\adr_wr_reg[3]_rep_n_0 ,\adr_wr_reg[2]_rep_n_0 ,\adr_wr_reg[1]_rep_n_0 ,\adr_wr_reg[0]_rep_n_0 }),
        .dout0_out(DOUT2_w),
        .en(EN_r[3]),
        .m00_axis_aclk(m00_axis_aclk),
        .\m00_axis_tdata_r_reg[10] (rams_sp_rf_rst_inst7_n_24),
        .\m00_axis_tdata_r_reg[12] (rams_sp_rf_rst_inst7_n_23),
        .\m00_axis_tdata_r_reg[13] (rams_sp_rf_rst_inst7_n_22),
        .\m00_axis_tdata_r_reg[14] (rams_sp_rf_rst_inst7_n_21),
        .\m00_axis_tdata_r_reg[17] (rams_sp_rf_rst_inst7_n_20),
        .\m00_axis_tdata_r_reg[18] (rams_sp_rf_rst_inst7_n_19),
        .\m00_axis_tdata_r_reg[19] (rams_sp_rf_rst_inst7_n_18),
        .\m00_axis_tdata_r_reg[1] (rams_sp_rf_rst_inst7_n_31),
        .\m00_axis_tdata_r_reg[21] (rams_sp_rf_rst_inst7_n_17),
        .\m00_axis_tdata_r_reg[22] (rams_sp_rf_rst_inst7_n_16),
        .\m00_axis_tdata_r_reg[23] (rams_sp_rf_rst_inst7_n_15),
        .\m00_axis_tdata_r_reg[24] (rams_sp_rf_rst_inst7_n_14),
        .\m00_axis_tdata_r_reg[26] (rams_sp_rf_rst_inst7_n_13),
        .\m00_axis_tdata_r_reg[28] (rams_sp_rf_rst_inst7_n_12),
        .\m00_axis_tdata_r_reg[29] (rams_sp_rf_rst_inst7_n_11),
        .\m00_axis_tdata_r_reg[2] (rams_sp_rf_rst_inst7_n_30),
        .\m00_axis_tdata_r_reg[30] (rams_sp_rf_rst_inst7_n_10),
        .\m00_axis_tdata_r_reg[31] (rams_sp_rf_rst_inst7_n_9),
        .\m00_axis_tdata_r_reg[31]_0 (rams_sp_rf_rst_instf_n_22),
        .\m00_axis_tdata_r_reg[31]_1 (rams_sp_rf_rst_instb_n_0),
        .\m00_axis_tdata_r_reg[31]_2 (DOUT1_w),
        .\m00_axis_tdata_r_reg[31]_3 (DOUT0),
        .\m00_axis_tdata_r_reg[3] (rams_sp_rf_rst_inst7_n_29),
        .\m00_axis_tdata_r_reg[5] (rams_sp_rf_rst_inst7_n_28),
        .\m00_axis_tdata_r_reg[6] (rams_sp_rf_rst_inst7_n_27),
        .\m00_axis_tdata_r_reg[7] (rams_sp_rf_rst_inst7_n_26),
        .\m00_axis_tdata_r_reg[8] (rams_sp_rf_rst_inst7_n_25),
        .ram_reg_0_0(rams_sp_rf_rst_inst3_n_15),
        .ram_reg_0_1(rams_sp_rf_rst_inst3_n_16),
        .ram_reg_0_2(rams_sp_rf_rst_inst3_n_20),
        .ram_reg_0_3(rams_sp_rf_rst_inst3_n_22),
        .ram_reg_0_4(rams_sp_rf_rst_inst3_n_27),
        .ram_reg_0_5(rams_sp_rf_rst_inst3_n_31),
        .ram_reg_1_0(rams_sp_rf_rst_inst3_n_4),
        .ram_reg_1_1(rams_sp_rf_rst_inst3_n_6),
        .ram_reg_1_2(rams_sp_rf_rst_inst3_n_11),
        .s00_axis_tuser(s00_axis_tuser[14:11]),
        .\s00_axis_tuser[18] (rams_sp_rf_rst_inst3_n_1),
        .\s00_axis_tuser[18]_0 (rams_sp_rf_rst_inst3_n_2),
        .\s00_axis_tuser[18]_1 (rams_sp_rf_rst_inst3_n_3),
        .\s00_axis_tuser[18]_10 (rams_sp_rf_rst_inst3_n_17),
        .\s00_axis_tuser[18]_11 (rams_sp_rf_rst_inst3_n_18),
        .\s00_axis_tuser[18]_12 (rams_sp_rf_rst_inst3_n_19),
        .\s00_axis_tuser[18]_13 (rams_sp_rf_rst_inst3_n_21),
        .\s00_axis_tuser[18]_14 (rams_sp_rf_rst_inst3_n_23),
        .\s00_axis_tuser[18]_15 (rams_sp_rf_rst_inst3_n_24),
        .\s00_axis_tuser[18]_16 (rams_sp_rf_rst_inst3_n_25),
        .\s00_axis_tuser[18]_17 (rams_sp_rf_rst_inst3_n_26),
        .\s00_axis_tuser[18]_18 (rams_sp_rf_rst_inst3_n_28),
        .\s00_axis_tuser[18]_19 (rams_sp_rf_rst_inst3_n_29),
        .\s00_axis_tuser[18]_2 (rams_sp_rf_rst_inst3_n_5),
        .\s00_axis_tuser[18]_20 (rams_sp_rf_rst_inst3_n_30),
        .\s00_axis_tuser[18]_3 (rams_sp_rf_rst_inst3_n_7),
        .\s00_axis_tuser[18]_4 (rams_sp_rf_rst_inst3_n_8),
        .\s00_axis_tuser[18]_5 (rams_sp_rf_rst_inst3_n_9),
        .\s00_axis_tuser[18]_6 (rams_sp_rf_rst_inst3_n_10),
        .\s00_axis_tuser[18]_7 (rams_sp_rf_rst_inst3_n_12),
        .\s00_axis_tuser[18]_8 (rams_sp_rf_rst_inst3_n_13),
        .\s00_axis_tuser[18]_9 (rams_sp_rf_rst_inst3_n_14),
        .square_r(square_r));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_3 rams_sp_rf_rst_inst4
       (.ADDRARDADDR(adr_wr),
        .Q(DI0),
        .WE_r(WE_r),
        .addr({\adr_wr_reg[10]_rep_n_0 ,\adr_wr_reg[9]_rep_n_0 ,\adr_wr_reg[8]_rep_n_0 ,\adr_wr_reg[7]_rep_n_0 ,\adr_wr_reg[6]_rep_n_0 ,\adr_wr_reg[5]_rep_n_0 ,\adr_wr_reg[4]_rep_n_0 ,\adr_wr_reg[3]_rep_n_0 ,\adr_wr_reg[2]_rep_n_0 ,\adr_wr_reg[1]_rep_n_0 ,\adr_wr_reg[0]_rep_n_0 }),
        .dout0_out(DOUT4_w),
        .en(EN_r[4]),
        .m00_axis_aclk(m00_axis_aclk),
        .square_r(square_r));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_4 rams_sp_rf_rst_inst5
       (.ADDRARDADDR(adr_wr),
        .Q(DI0),
        .WE_r(WE_r),
        .addr({\adr_wr_reg[10]_rep_n_0 ,\adr_wr_reg[9]_rep_n_0 ,\adr_wr_reg[8]_rep_n_0 ,\adr_wr_reg[7]_rep_n_0 ,\adr_wr_reg[6]_rep_n_0 ,\adr_wr_reg[5]_rep_n_0 ,\adr_wr_reg[4]_rep_n_0 ,\adr_wr_reg[3]_rep_n_0 ,\adr_wr_reg[2]_rep_n_0 ,\adr_wr_reg[1]_rep_n_0 ,\adr_wr_reg[0]_rep_n_0 }),
        .dout0_out(DOUT5_w),
        .en(EN_r[5]),
        .m00_axis_aclk(m00_axis_aclk),
        .square_r(square_r));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_5 rams_sp_rf_rst_inst6
       (.ADDRARDADDR(adr_wr),
        .Q(DI0),
        .WE_r(WE_r),
        .addr({\adr_wr_reg[10]_rep_n_0 ,\adr_wr_reg[9]_rep_n_0 ,\adr_wr_reg[8]_rep_n_0 ,\adr_wr_reg[7]_rep_n_0 ,\adr_wr_reg[6]_rep_n_0 ,\adr_wr_reg[5]_rep_n_0 ,\adr_wr_reg[4]_rep_n_0 ,\adr_wr_reg[3]_rep_n_0 ,\adr_wr_reg[2]_rep_n_0 ,\adr_wr_reg[1]_rep_n_0 ,\adr_wr_reg[0]_rep_n_0 }),
        .dout0_out(DOUT6_w),
        .en(EN_r[6]),
        .m00_axis_aclk(m00_axis_aclk),
        .square_r(square_r));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_6 rams_sp_rf_rst_inst7
       (.ADDRARDADDR(adr_wr),
        .D({rams_sp_rf_rst_inst7_n_0,rams_sp_rf_rst_inst7_n_1,rams_sp_rf_rst_inst7_n_2,rams_sp_rf_rst_inst7_n_3,rams_sp_rf_rst_inst7_n_4,rams_sp_rf_rst_inst7_n_5,rams_sp_rf_rst_inst7_n_6,rams_sp_rf_rst_inst7_n_7,rams_sp_rf_rst_inst7_n_8}),
        .Q(DI0),
        .WE_r(WE_r),
        .addr({\adr_wr_reg[10]_rep_n_0 ,\adr_wr_reg[9]_rep_n_0 ,\adr_wr_reg[8]_rep_n_0 ,\adr_wr_reg[7]_rep_n_0 ,\adr_wr_reg[6]_rep_n_0 ,\adr_wr_reg[5]_rep_n_0 ,\adr_wr_reg[4]_rep_n_0 ,\adr_wr_reg[3]_rep_n_0 ,\adr_wr_reg[2]_rep_n_0 ,\adr_wr_reg[1]_rep_n_0 ,\adr_wr_reg[0]_rep_n_0 }),
        .dout0_out(DOUT6_w),
        .en(EN_r[7]),
        .m00_axis_aclk(m00_axis_aclk),
        .\m00_axis_tdata_r_reg[0] (rams_sp_rf_rst_inst3_n_31),
        .\m00_axis_tdata_r_reg[0]_0 (rams_sp_rf_rst_instb_n_31),
        .\m00_axis_tdata_r_reg[11] (rams_sp_rf_rst_inst3_n_20),
        .\m00_axis_tdata_r_reg[11]_0 (rams_sp_rf_rst_instb_n_20),
        .\m00_axis_tdata_r_reg[15] (rams_sp_rf_rst_inst3_n_16),
        .\m00_axis_tdata_r_reg[15]_0 (rams_sp_rf_rst_instb_n_16),
        .\m00_axis_tdata_r_reg[16] (rams_sp_rf_rst_inst3_n_15),
        .\m00_axis_tdata_r_reg[16]_0 (rams_sp_rf_rst_instb_n_15),
        .\m00_axis_tdata_r_reg[20] (rams_sp_rf_rst_inst3_n_11),
        .\m00_axis_tdata_r_reg[20]_0 (rams_sp_rf_rst_instb_n_11),
        .\m00_axis_tdata_r_reg[25] (rams_sp_rf_rst_inst3_n_6),
        .\m00_axis_tdata_r_reg[25]_0 (rams_sp_rf_rst_instb_n_6),
        .\m00_axis_tdata_r_reg[27] (rams_sp_rf_rst_inst3_n_4),
        .\m00_axis_tdata_r_reg[27]_0 (rams_sp_rf_rst_instb_n_4),
        .\m00_axis_tdata_r_reg[31] (DOUT5_w),
        .\m00_axis_tdata_r_reg[31]_0 (DOUT4_w),
        .\m00_axis_tdata_r_reg[4] (rams_sp_rf_rst_inst3_n_27),
        .\m00_axis_tdata_r_reg[4]_0 (rams_sp_rf_rst_instb_n_27),
        .\m00_axis_tdata_r_reg[9] (rams_sp_rf_rst_inst3_n_22),
        .\m00_axis_tdata_r_reg[9]_0 (rams_sp_rf_rst_instb_n_22),
        .ram_reg_0_0(rams_sp_rf_rst_inst7_n_20),
        .ram_reg_0_1(rams_sp_rf_rst_inst7_n_21),
        .ram_reg_0_10(rams_sp_rf_rst_inst7_n_30),
        .ram_reg_0_11(rams_sp_rf_rst_inst7_n_31),
        .ram_reg_0_2(rams_sp_rf_rst_inst7_n_22),
        .ram_reg_0_3(rams_sp_rf_rst_inst7_n_23),
        .ram_reg_0_4(rams_sp_rf_rst_inst7_n_24),
        .ram_reg_0_5(rams_sp_rf_rst_inst7_n_25),
        .ram_reg_0_6(rams_sp_rf_rst_inst7_n_26),
        .ram_reg_0_7(rams_sp_rf_rst_inst7_n_27),
        .ram_reg_0_8(rams_sp_rf_rst_inst7_n_28),
        .ram_reg_0_9(rams_sp_rf_rst_inst7_n_29),
        .ram_reg_1_0(rams_sp_rf_rst_inst7_n_9),
        .ram_reg_1_1(rams_sp_rf_rst_inst7_n_10),
        .ram_reg_1_10(rams_sp_rf_rst_inst7_n_19),
        .ram_reg_1_2(rams_sp_rf_rst_inst7_n_11),
        .ram_reg_1_3(rams_sp_rf_rst_inst7_n_12),
        .ram_reg_1_4(rams_sp_rf_rst_inst7_n_13),
        .ram_reg_1_5(rams_sp_rf_rst_inst7_n_14),
        .ram_reg_1_6(rams_sp_rf_rst_inst7_n_15),
        .ram_reg_1_7(rams_sp_rf_rst_inst7_n_16),
        .ram_reg_1_8(rams_sp_rf_rst_inst7_n_17),
        .ram_reg_1_9(rams_sp_rf_rst_inst7_n_18),
        .s00_axis_tuser(s00_axis_tuser[14:11]),
        .square_r(square_r));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_7 rams_sp_rf_rst_inst8
       (.ADDRARDADDR(adr_wr),
        .Q(EN_r[8]),
        .WE_r(WE_r),
        .addr({\adr_wr_reg[10]_rep_n_0 ,\adr_wr_reg[9]_rep_n_0 ,\adr_wr_reg[8]_rep_n_0 ,\adr_wr_reg[7]_rep_n_0 ,\adr_wr_reg[6]_rep_n_0 ,\adr_wr_reg[5]_rep_n_0 ,\adr_wr_reg[4]_rep_n_0 ,\adr_wr_reg[3]_rep_n_0 ,\adr_wr_reg[2]_rep_n_0 ,\adr_wr_reg[1]_rep_n_0 ,\adr_wr_reg[0]_rep_n_0 }),
        .dout0_out(DOUT8_w),
        .m00_axis_aclk(m00_axis_aclk),
        .ram_reg_1_0(DI0),
        .square_r(square_r));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_8 rams_sp_rf_rst_inst9
       (.ADDRARDADDR(adr_wr),
        .Q(EN_r[9]),
        .WE_r(WE_r),
        .addr({\adr_wr_reg[10]_rep_n_0 ,\adr_wr_reg[9]_rep_n_0 ,\adr_wr_reg[8]_rep_n_0 ,\adr_wr_reg[7]_rep_n_0 ,\adr_wr_reg[6]_rep_n_0 ,\adr_wr_reg[5]_rep_n_0 ,\adr_wr_reg[4]_rep_n_0 ,\adr_wr_reg[3]_rep_n_0 ,\adr_wr_reg[2]_rep_n_0 ,\adr_wr_reg[1]_rep_n_0 ,\adr_wr_reg[0]_rep_n_0 }),
        .dout0_out(DOUT9_w),
        .m00_axis_aclk(m00_axis_aclk),
        .ram_reg_1_0(DI0),
        .square_r(square_r));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_9 rams_sp_rf_rst_insta
       (.ADDRARDADDR(adr_wr),
        .Q(DI0),
        .WE_r(WE_r),
        .addr({\adr_wr_reg[10]_rep_n_0 ,\adr_wr_reg[9]_rep_n_0 ,\adr_wr_reg[8]_rep_n_0 ,\adr_wr_reg[7]_rep_n_0 ,\adr_wr_reg[6]_rep_n_0 ,\adr_wr_reg[5]_rep_n_0 ,\adr_wr_reg[4]_rep_n_0 ,\adr_wr_reg[3]_rep_n_0 ,\adr_wr_reg[2]_rep_n_0 ,\adr_wr_reg[1]_rep_n_0 ,\adr_wr_reg[0]_rep_n_0 }),
        .dout0_out(DOUTA_w),
        .en(EN_r[10]),
        .m00_axis_aclk(m00_axis_aclk),
        .square_r(square_r));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_10 rams_sp_rf_rst_instb
       (.ADDRARDADDR(adr_wr),
        .Q(DI0),
        .WE_r(WE_r),
        .addr({\adr_wr_reg[10]_rep_n_0 ,\adr_wr_reg[9]_rep_n_0 ,\adr_wr_reg[8]_rep_n_0 ,\adr_wr_reg[7]_rep_n_0 ,\adr_wr_reg[6]_rep_n_0 ,\adr_wr_reg[5]_rep_n_0 ,\adr_wr_reg[4]_rep_n_0 ,\adr_wr_reg[3]_rep_n_0 ,\adr_wr_reg[2]_rep_n_0 ,\adr_wr_reg[1]_rep_n_0 ,\adr_wr_reg[0]_rep_n_0 }),
        .dout0_out(DOUTA_w),
        .en(EN_r[11]),
        .m00_axis_aclk(m00_axis_aclk),
        .\m00_axis_tdata_r_reg[0] (rams_sp_rf_rst_instf_n_31),
        .\m00_axis_tdata_r_reg[11] (rams_sp_rf_rst_instf_n_28),
        .\m00_axis_tdata_r_reg[15] (rams_sp_rf_rst_instf_n_27),
        .\m00_axis_tdata_r_reg[16] (rams_sp_rf_rst_instf_n_26),
        .\m00_axis_tdata_r_reg[20] (rams_sp_rf_rst_instf_n_25),
        .\m00_axis_tdata_r_reg[25] (rams_sp_rf_rst_instf_n_24),
        .\m00_axis_tdata_r_reg[27] (rams_sp_rf_rst_instf_n_23),
        .\m00_axis_tdata_r_reg[31] (DOUT9_w),
        .\m00_axis_tdata_r_reg[31]_0 (DOUT8_w),
        .\m00_axis_tdata_r_reg[4] (rams_sp_rf_rst_instf_n_30),
        .\m00_axis_tdata_r_reg[9] (rams_sp_rf_rst_instf_n_29),
        .ram_reg_0_0(rams_sp_rf_rst_instb_n_14),
        .ram_reg_0_1(rams_sp_rf_rst_instb_n_17),
        .ram_reg_0_10(rams_sp_rf_rst_instb_n_29),
        .ram_reg_0_11(rams_sp_rf_rst_instb_n_30),
        .ram_reg_0_2(rams_sp_rf_rst_instb_n_18),
        .ram_reg_0_3(rams_sp_rf_rst_instb_n_19),
        .ram_reg_0_4(rams_sp_rf_rst_instb_n_21),
        .ram_reg_0_5(rams_sp_rf_rst_instb_n_23),
        .ram_reg_0_6(rams_sp_rf_rst_instb_n_24),
        .ram_reg_0_7(rams_sp_rf_rst_instb_n_25),
        .ram_reg_0_8(rams_sp_rf_rst_instb_n_26),
        .ram_reg_0_9(rams_sp_rf_rst_instb_n_28),
        .ram_reg_1_0(rams_sp_rf_rst_instb_n_0),
        .ram_reg_1_1(rams_sp_rf_rst_instb_n_1),
        .ram_reg_1_10(rams_sp_rf_rst_instb_n_13),
        .ram_reg_1_2(rams_sp_rf_rst_instb_n_2),
        .ram_reg_1_3(rams_sp_rf_rst_instb_n_3),
        .ram_reg_1_4(rams_sp_rf_rst_instb_n_5),
        .ram_reg_1_5(rams_sp_rf_rst_instb_n_7),
        .ram_reg_1_6(rams_sp_rf_rst_instb_n_8),
        .ram_reg_1_7(rams_sp_rf_rst_instb_n_9),
        .ram_reg_1_8(rams_sp_rf_rst_instb_n_10),
        .ram_reg_1_9(rams_sp_rf_rst_instb_n_12),
        .s00_axis_tuser(s00_axis_tuser[13:11]),
        .\s00_axis_tuser[18] (rams_sp_rf_rst_instb_n_4),
        .\s00_axis_tuser[18]_0 (rams_sp_rf_rst_instb_n_6),
        .\s00_axis_tuser[18]_1 (rams_sp_rf_rst_instb_n_11),
        .\s00_axis_tuser[18]_2 (rams_sp_rf_rst_instb_n_15),
        .\s00_axis_tuser[18]_3 (rams_sp_rf_rst_instb_n_16),
        .\s00_axis_tuser[18]_4 (rams_sp_rf_rst_instb_n_20),
        .\s00_axis_tuser[18]_5 (rams_sp_rf_rst_instb_n_22),
        .\s00_axis_tuser[18]_6 (rams_sp_rf_rst_instb_n_27),
        .\s00_axis_tuser[18]_7 (rams_sp_rf_rst_instb_n_31),
        .square_r(square_r));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_11 rams_sp_rf_rst_instc
       (.ADDRARDADDR(adr_wr),
        .Q(DI0),
        .WE_r(WE_r),
        .addr({\adr_wr_reg[10]_rep_n_0 ,\adr_wr_reg[9]_rep_n_0 ,\adr_wr_reg[8]_rep_n_0 ,\adr_wr_reg[7]_rep_n_0 ,\adr_wr_reg[6]_rep_n_0 ,\adr_wr_reg[5]_rep_n_0 ,\adr_wr_reg[4]_rep_n_0 ,\adr_wr_reg[3]_rep_n_0 ,\adr_wr_reg[2]_rep_n_0 ,\adr_wr_reg[1]_rep_n_0 ,\adr_wr_reg[0]_rep_n_0 }),
        .dout0_out(DOUTC_w),
        .en(EN_r[12]),
        .m00_axis_aclk(m00_axis_aclk),
        .square_r(square_r));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_12 rams_sp_rf_rst_instd
       (.ADDRARDADDR(adr_wr),
        .Q(DI0),
        .WE_r(WE_r),
        .addr({\adr_wr_reg[10]_rep_n_0 ,\adr_wr_reg[9]_rep_n_0 ,\adr_wr_reg[8]_rep_n_0 ,\adr_wr_reg[7]_rep_n_0 ,\adr_wr_reg[6]_rep_n_0 ,\adr_wr_reg[5]_rep_n_0 ,\adr_wr_reg[4]_rep_n_0 ,\adr_wr_reg[3]_rep_n_0 ,\adr_wr_reg[2]_rep_n_0 ,\adr_wr_reg[1]_rep_n_0 ,\adr_wr_reg[0]_rep_n_0 }),
        .dout0_out(DOUTD_w),
        .en(EN_r[13]),
        .m00_axis_aclk(m00_axis_aclk),
        .square_r(square_r));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_13 rams_sp_rf_rst_inste
       (.ADDRARDADDR(adr_wr),
        .Q(DI0),
        .WE_r(WE_r),
        .addr({\adr_wr_reg[10]_rep_n_0 ,\adr_wr_reg[9]_rep_n_0 ,\adr_wr_reg[8]_rep_n_0 ,\adr_wr_reg[7]_rep_n_0 ,\adr_wr_reg[6]_rep_n_0 ,\adr_wr_reg[5]_rep_n_0 ,\adr_wr_reg[4]_rep_n_0 ,\adr_wr_reg[3]_rep_n_0 ,\adr_wr_reg[2]_rep_n_0 ,\adr_wr_reg[1]_rep_n_0 ,\adr_wr_reg[0]_rep_n_0 }),
        .dout0_out(DOUTE_w),
        .en(EN_r[14]),
        .m00_axis_aclk(m00_axis_aclk),
        .square_r(square_r));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_14 rams_sp_rf_rst_instf
       (.ADDRARDADDR(adr_wr),
        .D({rams_sp_rf_rst_instf_n_0,rams_sp_rf_rst_instf_n_1,rams_sp_rf_rst_instf_n_2,rams_sp_rf_rst_instf_n_3,rams_sp_rf_rst_instf_n_4,rams_sp_rf_rst_instf_n_5,rams_sp_rf_rst_instf_n_6,rams_sp_rf_rst_instf_n_7,rams_sp_rf_rst_instf_n_8,rams_sp_rf_rst_instf_n_9,rams_sp_rf_rst_instf_n_10,rams_sp_rf_rst_instf_n_11,rams_sp_rf_rst_instf_n_12,rams_sp_rf_rst_instf_n_13,rams_sp_rf_rst_instf_n_14,rams_sp_rf_rst_instf_n_15,rams_sp_rf_rst_instf_n_16,rams_sp_rf_rst_instf_n_17,rams_sp_rf_rst_instf_n_18,rams_sp_rf_rst_instf_n_19,rams_sp_rf_rst_instf_n_20,rams_sp_rf_rst_instf_n_21}),
        .Q(DI0),
        .WE_r(WE_r),
        .addr({\adr_wr_reg[10]_rep_n_0 ,\adr_wr_reg[9]_rep_n_0 ,\adr_wr_reg[8]_rep_n_0 ,\adr_wr_reg[7]_rep_n_0 ,\adr_wr_reg[6]_rep_n_0 ,\adr_wr_reg[5]_rep_n_0 ,\adr_wr_reg[4]_rep_n_0 ,\adr_wr_reg[3]_rep_n_0 ,\adr_wr_reg[2]_rep_n_0 ,\adr_wr_reg[1]_rep_n_0 ,\adr_wr_reg[0]_rep_n_0 }),
        .dout0_out(DOUTE_w),
        .en(EN_r[15]),
        .m00_axis_aclk(m00_axis_aclk),
        .\m00_axis_tdata_r_reg[10] (rams_sp_rf_rst_instb_n_21),
        .\m00_axis_tdata_r_reg[10]_0 (rams_sp_rf_rst_inst3_n_21),
        .\m00_axis_tdata_r_reg[12] (rams_sp_rf_rst_instb_n_19),
        .\m00_axis_tdata_r_reg[12]_0 (rams_sp_rf_rst_inst3_n_19),
        .\m00_axis_tdata_r_reg[13] (rams_sp_rf_rst_instb_n_18),
        .\m00_axis_tdata_r_reg[13]_0 (rams_sp_rf_rst_inst3_n_18),
        .\m00_axis_tdata_r_reg[14] (rams_sp_rf_rst_instb_n_17),
        .\m00_axis_tdata_r_reg[14]_0 (rams_sp_rf_rst_inst3_n_17),
        .\m00_axis_tdata_r_reg[17] (rams_sp_rf_rst_instb_n_14),
        .\m00_axis_tdata_r_reg[17]_0 (rams_sp_rf_rst_inst3_n_14),
        .\m00_axis_tdata_r_reg[18] (rams_sp_rf_rst_instb_n_13),
        .\m00_axis_tdata_r_reg[18]_0 (rams_sp_rf_rst_inst3_n_13),
        .\m00_axis_tdata_r_reg[19] (rams_sp_rf_rst_instb_n_12),
        .\m00_axis_tdata_r_reg[19]_0 (rams_sp_rf_rst_inst3_n_12),
        .\m00_axis_tdata_r_reg[1] (rams_sp_rf_rst_instb_n_30),
        .\m00_axis_tdata_r_reg[1]_0 (rams_sp_rf_rst_inst3_n_30),
        .\m00_axis_tdata_r_reg[21] (rams_sp_rf_rst_instb_n_10),
        .\m00_axis_tdata_r_reg[21]_0 (rams_sp_rf_rst_inst3_n_10),
        .\m00_axis_tdata_r_reg[22] (rams_sp_rf_rst_instb_n_9),
        .\m00_axis_tdata_r_reg[22]_0 (rams_sp_rf_rst_inst3_n_9),
        .\m00_axis_tdata_r_reg[23] (rams_sp_rf_rst_instb_n_8),
        .\m00_axis_tdata_r_reg[23]_0 (rams_sp_rf_rst_inst3_n_8),
        .\m00_axis_tdata_r_reg[24] (rams_sp_rf_rst_instb_n_7),
        .\m00_axis_tdata_r_reg[24]_0 (rams_sp_rf_rst_inst3_n_7),
        .\m00_axis_tdata_r_reg[26] (rams_sp_rf_rst_instb_n_5),
        .\m00_axis_tdata_r_reg[26]_0 (rams_sp_rf_rst_inst3_n_5),
        .\m00_axis_tdata_r_reg[28] (rams_sp_rf_rst_instb_n_3),
        .\m00_axis_tdata_r_reg[28]_0 (rams_sp_rf_rst_inst3_n_3),
        .\m00_axis_tdata_r_reg[29] (rams_sp_rf_rst_instb_n_2),
        .\m00_axis_tdata_r_reg[29]_0 (rams_sp_rf_rst_inst3_n_2),
        .\m00_axis_tdata_r_reg[2] (rams_sp_rf_rst_instb_n_29),
        .\m00_axis_tdata_r_reg[2]_0 (rams_sp_rf_rst_inst3_n_29),
        .\m00_axis_tdata_r_reg[30] (rams_sp_rf_rst_instb_n_1),
        .\m00_axis_tdata_r_reg[30]_0 (rams_sp_rf_rst_inst3_n_1),
        .\m00_axis_tdata_r_reg[31] (DOUTD_w),
        .\m00_axis_tdata_r_reg[31]_0 (DOUTC_w),
        .\m00_axis_tdata_r_reg[3] (rams_sp_rf_rst_instb_n_28),
        .\m00_axis_tdata_r_reg[3]_0 (rams_sp_rf_rst_inst3_n_28),
        .\m00_axis_tdata_r_reg[5] (rams_sp_rf_rst_instb_n_26),
        .\m00_axis_tdata_r_reg[5]_0 (rams_sp_rf_rst_inst3_n_26),
        .\m00_axis_tdata_r_reg[6] (rams_sp_rf_rst_instb_n_25),
        .\m00_axis_tdata_r_reg[6]_0 (rams_sp_rf_rst_inst3_n_25),
        .\m00_axis_tdata_r_reg[7] (rams_sp_rf_rst_instb_n_24),
        .\m00_axis_tdata_r_reg[7]_0 (rams_sp_rf_rst_inst3_n_24),
        .\m00_axis_tdata_r_reg[8] (rams_sp_rf_rst_instb_n_23),
        .\m00_axis_tdata_r_reg[8]_0 (rams_sp_rf_rst_inst3_n_23),
        .ram_reg_0_0(rams_sp_rf_rst_instf_n_26),
        .ram_reg_0_1(rams_sp_rf_rst_instf_n_27),
        .ram_reg_0_2(rams_sp_rf_rst_instf_n_28),
        .ram_reg_0_3(rams_sp_rf_rst_instf_n_29),
        .ram_reg_0_4(rams_sp_rf_rst_instf_n_30),
        .ram_reg_0_5(rams_sp_rf_rst_instf_n_31),
        .ram_reg_1_0(rams_sp_rf_rst_instf_n_22),
        .ram_reg_1_1(rams_sp_rf_rst_instf_n_23),
        .ram_reg_1_2(rams_sp_rf_rst_instf_n_24),
        .ram_reg_1_3(rams_sp_rf_rst_instf_n_25),
        .s00_axis_tuser(s00_axis_tuser[14:11]),
        .square_r(square_r));
  LUT3 #(
    .INIT(8'h3A)) 
    square_r_i_1
       (.I0(square_r),
        .I1(s00_axis_tuser[15]),
        .I2(s00_axis_tvalid),
        .O(square_r_i_1_n_0));
  FDRE square_r_reg
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(square_r_i_1_n_0),
        .Q(square_r),
        .R(1'b0));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst
   (dout0_out,
    m00_axis_aclk,
    Q,
    ADDRARDADDR,
    ram_reg_1_0,
    addr,
    WE_r,
    square_r);
  output [31:0]dout0_out;
  input m00_axis_aclk;
  input [0:0]Q;
  input [10:0]ADDRARDADDR;
  input [31:0]ram_reg_1_0;
  input [10:0]addr;
  input WE_r;
  input square_r;

  wire [10:0]ADDRARDADDR;
  wire [0:0]Q;
  wire WE_r;
  wire [10:0]addr;
  wire [31:0]dout0_out;
  wire m00_axis_aclk;
  wire ram_reg_0_i_1_n_0;
  wire [31:0]ram_reg_1_0;
  wire square_r;
  wire NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED;
  wire NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED;
  wire NLW_ram_reg_0_DBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_SBITERR_UNCONNECTED;
  wire [31:16]NLW_ram_reg_0_DOADO_UNCONNECTED;
  wire [31:0]NLW_ram_reg_0_DOBDO_UNCONNECTED;
  wire [3:2]NLW_ram_reg_0_DOPADOP_UNCONNECTED;
  wire [3:0]NLW_ram_reg_0_DOPBDOP_UNCONNECTED;
  wire [7:0]NLW_ram_reg_0_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_ram_reg_0_RDADDRECC_UNCONNECTED;
  wire NLW_ram_reg_1_CASCADEOUTA_UNCONNECTED;
  wire NLW_ram_reg_1_CASCADEOUTB_UNCONNECTED;
  wire NLW_ram_reg_1_DBITERR_UNCONNECTED;
  wire NLW_ram_reg_1_INJECTDBITERR_UNCONNECTED;
  wire NLW_ram_reg_1_INJECTSBITERR_UNCONNECTED;
  wire NLW_ram_reg_1_SBITERR_UNCONNECTED;
  wire [31:14]NLW_ram_reg_1_DOADO_UNCONNECTED;
  wire [31:0]NLW_ram_reg_1_DOBDO_UNCONNECTED;
  wire [3:0]NLW_ram_reg_1_DOPADOP_UNCONNECTED;
  wire [3:0]NLW_ram_reg_1_DOPBDOP_UNCONNECTED;
  wire [7:0]NLW_ram_reg_1_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_ram_reg_1_RDADDRECC_UNCONNECTED;

  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p2_d16" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "61472" *) 
  (* RTL_RAM_NAME = "ram" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "2047" *) 
  (* bram_slice_begin = "0" *) 
  (* bram_slice_end = "17" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "2047" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "17" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(18),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(18),
    .WRITE_WIDTH_B(0)) 
    ram_reg_0
       (.ADDRARDADDR({1'b1,ADDRARDADDR,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CASCADEINA(1'b1),
        .CASCADEINB(1'b0),
        .CASCADEOUTA(NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED),
        .CASCADEOUTB(NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED),
        .CLKARDCLK(m00_axis_aclk),
        .CLKBWRCLK(1'b0),
        .DBITERR(NLW_ram_reg_0_DBITERR_UNCONNECTED),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,ram_reg_1_0[15:0]}),
        .DIBDI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0,ram_reg_1_0[17:16]}),
        .DIPBDIP({1'b1,1'b1,1'b1,1'b1}),
        .DOADO({NLW_ram_reg_0_DOADO_UNCONNECTED[31:16],dout0_out[15:0]}),
        .DOBDO(NLW_ram_reg_0_DOBDO_UNCONNECTED[31:0]),
        .DOPADOP({NLW_ram_reg_0_DOPADOP_UNCONNECTED[3:2],dout0_out[17:16]}),
        .DOPBDOP(NLW_ram_reg_0_DOPBDOP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_ram_reg_0_ECCPARITY_UNCONNECTED[7:0]),
        .ENARDEN(Q),
        .ENBWREN(1'b0),
        .INJECTDBITERR(NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED),
        .INJECTSBITERR(NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED),
        .RDADDRECC(NLW_ram_reg_0_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_ram_reg_0_SBITERR_UNCONNECTED),
        .WEA({ram_reg_0_i_1_n_0,ram_reg_0_i_1_n_0,ram_reg_0_i_1_n_0,ram_reg_0_i_1_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
  LUT3 #(
    .INIT(8'h80)) 
    ram_reg_0_i_1
       (.I0(Q),
        .I1(WE_r),
        .I2(square_r),
        .O(ram_reg_0_i_1_n_0));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d14" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "61472" *) 
  (* RTL_RAM_NAME = "ram" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "2047" *) 
  (* bram_slice_begin = "18" *) 
  (* bram_slice_end = "31" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "2047" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "18" *) 
  (* ram_slice_end = "31" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(18),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(18),
    .WRITE_WIDTH_B(0)) 
    ram_reg_1
       (.ADDRARDADDR({1'b1,addr,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CASCADEINA(1'b1),
        .CASCADEINB(1'b0),
        .CASCADEOUTA(NLW_ram_reg_1_CASCADEOUTA_UNCONNECTED),
        .CASCADEOUTB(NLW_ram_reg_1_CASCADEOUTB_UNCONNECTED),
        .CLKARDCLK(m00_axis_aclk),
        .CLKBWRCLK(1'b0),
        .DBITERR(NLW_ram_reg_1_DBITERR_UNCONNECTED),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,ram_reg_1_0[31:18]}),
        .DIBDI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0,1'b0,1'b0}),
        .DIPBDIP({1'b1,1'b1,1'b1,1'b1}),
        .DOADO({NLW_ram_reg_1_DOADO_UNCONNECTED[31:14],dout0_out[31:18]}),
        .DOBDO(NLW_ram_reg_1_DOBDO_UNCONNECTED[31:0]),
        .DOPADOP(NLW_ram_reg_1_DOPADOP_UNCONNECTED[3:0]),
        .DOPBDOP(NLW_ram_reg_1_DOPBDOP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_ram_reg_1_ECCPARITY_UNCONNECTED[7:0]),
        .ENARDEN(Q),
        .ENBWREN(1'b0),
        .INJECTDBITERR(NLW_ram_reg_1_INJECTDBITERR_UNCONNECTED),
        .INJECTSBITERR(NLW_ram_reg_1_INJECTSBITERR_UNCONNECTED),
        .RDADDRECC(NLW_ram_reg_1_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_ram_reg_1_SBITERR_UNCONNECTED),
        .WEA({ram_reg_0_i_1_n_0,ram_reg_0_i_1_n_0,ram_reg_0_i_1_n_0,ram_reg_0_i_1_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
endmodule

(* ORIG_REF_NAME = "rams_sp_rf_rst" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_0
   (dout0_out,
    m00_axis_aclk,
    Q,
    ADDRARDADDR,
    ram_reg_1_0,
    addr,
    WE_r,
    square_r);
  output [31:0]dout0_out;
  input m00_axis_aclk;
  input [0:0]Q;
  input [10:0]ADDRARDADDR;
  input [31:0]ram_reg_1_0;
  input [10:0]addr;
  input WE_r;
  input square_r;

  wire [10:0]ADDRARDADDR;
  wire [0:0]Q;
  wire WE_r;
  wire [10:0]addr;
  wire [31:0]dout0_out;
  wire m00_axis_aclk;
  wire ram_reg_0_i_1__0_n_0;
  wire [31:0]ram_reg_1_0;
  wire square_r;
  wire NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED;
  wire NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED;
  wire NLW_ram_reg_0_DBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_SBITERR_UNCONNECTED;
  wire [31:16]NLW_ram_reg_0_DOADO_UNCONNECTED;
  wire [31:0]NLW_ram_reg_0_DOBDO_UNCONNECTED;
  wire [3:2]NLW_ram_reg_0_DOPADOP_UNCONNECTED;
  wire [3:0]NLW_ram_reg_0_DOPBDOP_UNCONNECTED;
  wire [7:0]NLW_ram_reg_0_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_ram_reg_0_RDADDRECC_UNCONNECTED;
  wire NLW_ram_reg_1_CASCADEOUTA_UNCONNECTED;
  wire NLW_ram_reg_1_CASCADEOUTB_UNCONNECTED;
  wire NLW_ram_reg_1_DBITERR_UNCONNECTED;
  wire NLW_ram_reg_1_INJECTDBITERR_UNCONNECTED;
  wire NLW_ram_reg_1_INJECTSBITERR_UNCONNECTED;
  wire NLW_ram_reg_1_SBITERR_UNCONNECTED;
  wire [31:14]NLW_ram_reg_1_DOADO_UNCONNECTED;
  wire [31:0]NLW_ram_reg_1_DOBDO_UNCONNECTED;
  wire [3:0]NLW_ram_reg_1_DOPADOP_UNCONNECTED;
  wire [3:0]NLW_ram_reg_1_DOPBDOP_UNCONNECTED;
  wire [7:0]NLW_ram_reg_1_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_ram_reg_1_RDADDRECC_UNCONNECTED;

  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p2_d16" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "61472" *) 
  (* RTL_RAM_NAME = "ram" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "2047" *) 
  (* bram_slice_begin = "0" *) 
  (* bram_slice_end = "17" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "2047" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "17" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(18),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(18),
    .WRITE_WIDTH_B(0)) 
    ram_reg_0
       (.ADDRARDADDR({1'b1,ADDRARDADDR,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CASCADEINA(1'b1),
        .CASCADEINB(1'b0),
        .CASCADEOUTA(NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED),
        .CASCADEOUTB(NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED),
        .CLKARDCLK(m00_axis_aclk),
        .CLKBWRCLK(1'b0),
        .DBITERR(NLW_ram_reg_0_DBITERR_UNCONNECTED),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,ram_reg_1_0[15:0]}),
        .DIBDI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0,ram_reg_1_0[17:16]}),
        .DIPBDIP({1'b1,1'b1,1'b1,1'b1}),
        .DOADO({NLW_ram_reg_0_DOADO_UNCONNECTED[31:16],dout0_out[15:0]}),
        .DOBDO(NLW_ram_reg_0_DOBDO_UNCONNECTED[31:0]),
        .DOPADOP({NLW_ram_reg_0_DOPADOP_UNCONNECTED[3:2],dout0_out[17:16]}),
        .DOPBDOP(NLW_ram_reg_0_DOPBDOP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_ram_reg_0_ECCPARITY_UNCONNECTED[7:0]),
        .ENARDEN(Q),
        .ENBWREN(1'b0),
        .INJECTDBITERR(NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED),
        .INJECTSBITERR(NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED),
        .RDADDRECC(NLW_ram_reg_0_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_ram_reg_0_SBITERR_UNCONNECTED),
        .WEA({ram_reg_0_i_1__0_n_0,ram_reg_0_i_1__0_n_0,ram_reg_0_i_1__0_n_0,ram_reg_0_i_1__0_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
  LUT3 #(
    .INIT(8'h80)) 
    ram_reg_0_i_1__0
       (.I0(Q),
        .I1(WE_r),
        .I2(square_r),
        .O(ram_reg_0_i_1__0_n_0));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d14" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "61472" *) 
  (* RTL_RAM_NAME = "ram" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "2047" *) 
  (* bram_slice_begin = "18" *) 
  (* bram_slice_end = "31" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "2047" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "18" *) 
  (* ram_slice_end = "31" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(18),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(18),
    .WRITE_WIDTH_B(0)) 
    ram_reg_1
       (.ADDRARDADDR({1'b1,addr,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CASCADEINA(1'b1),
        .CASCADEINB(1'b0),
        .CASCADEOUTA(NLW_ram_reg_1_CASCADEOUTA_UNCONNECTED),
        .CASCADEOUTB(NLW_ram_reg_1_CASCADEOUTB_UNCONNECTED),
        .CLKARDCLK(m00_axis_aclk),
        .CLKBWRCLK(1'b0),
        .DBITERR(NLW_ram_reg_1_DBITERR_UNCONNECTED),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,ram_reg_1_0[31:18]}),
        .DIBDI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0,1'b0,1'b0}),
        .DIPBDIP({1'b1,1'b1,1'b1,1'b1}),
        .DOADO({NLW_ram_reg_1_DOADO_UNCONNECTED[31:14],dout0_out[31:18]}),
        .DOBDO(NLW_ram_reg_1_DOBDO_UNCONNECTED[31:0]),
        .DOPADOP(NLW_ram_reg_1_DOPADOP_UNCONNECTED[3:0]),
        .DOPBDOP(NLW_ram_reg_1_DOPBDOP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_ram_reg_1_ECCPARITY_UNCONNECTED[7:0]),
        .ENARDEN(Q),
        .ENBWREN(1'b0),
        .INJECTDBITERR(NLW_ram_reg_1_INJECTDBITERR_UNCONNECTED),
        .INJECTSBITERR(NLW_ram_reg_1_INJECTSBITERR_UNCONNECTED),
        .RDADDRECC(NLW_ram_reg_1_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_ram_reg_1_SBITERR_UNCONNECTED),
        .WEA({ram_reg_0_i_1__0_n_0,ram_reg_0_i_1__0_n_0,ram_reg_0_i_1__0_n_0,ram_reg_0_i_1__0_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
endmodule

(* ORIG_REF_NAME = "rams_sp_rf_rst" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_1
   (dout0_out,
    m00_axis_aclk,
    en,
    ADDRARDADDR,
    Q,
    addr,
    WE_r,
    square_r);
  output [31:0]dout0_out;
  input m00_axis_aclk;
  input en;
  input [10:0]ADDRARDADDR;
  input [31:0]Q;
  input [10:0]addr;
  input WE_r;
  input square_r;

  wire [10:0]ADDRARDADDR;
  wire [31:0]Q;
  wire WE_r;
  wire [10:0]addr;
  wire [31:0]dout0_out;
  wire en;
  wire m00_axis_aclk;
  wire ram_reg_0_i_1__1_n_0;
  wire square_r;
  wire NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED;
  wire NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED;
  wire NLW_ram_reg_0_DBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_SBITERR_UNCONNECTED;
  wire [31:16]NLW_ram_reg_0_DOADO_UNCONNECTED;
  wire [31:0]NLW_ram_reg_0_DOBDO_UNCONNECTED;
  wire [3:2]NLW_ram_reg_0_DOPADOP_UNCONNECTED;
  wire [3:0]NLW_ram_reg_0_DOPBDOP_UNCONNECTED;
  wire [7:0]NLW_ram_reg_0_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_ram_reg_0_RDADDRECC_UNCONNECTED;
  wire NLW_ram_reg_1_CASCADEOUTA_UNCONNECTED;
  wire NLW_ram_reg_1_CASCADEOUTB_UNCONNECTED;
  wire NLW_ram_reg_1_DBITERR_UNCONNECTED;
  wire NLW_ram_reg_1_INJECTDBITERR_UNCONNECTED;
  wire NLW_ram_reg_1_INJECTSBITERR_UNCONNECTED;
  wire NLW_ram_reg_1_SBITERR_UNCONNECTED;
  wire [31:14]NLW_ram_reg_1_DOADO_UNCONNECTED;
  wire [31:0]NLW_ram_reg_1_DOBDO_UNCONNECTED;
  wire [3:0]NLW_ram_reg_1_DOPADOP_UNCONNECTED;
  wire [3:0]NLW_ram_reg_1_DOPBDOP_UNCONNECTED;
  wire [7:0]NLW_ram_reg_1_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_ram_reg_1_RDADDRECC_UNCONNECTED;

  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p2_d16" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "61472" *) 
  (* RTL_RAM_NAME = "ram" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "2047" *) 
  (* bram_slice_begin = "0" *) 
  (* bram_slice_end = "17" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "2047" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "17" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(18),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(18),
    .WRITE_WIDTH_B(0)) 
    ram_reg_0
       (.ADDRARDADDR({1'b1,ADDRARDADDR,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CASCADEINA(1'b1),
        .CASCADEINB(1'b0),
        .CASCADEOUTA(NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED),
        .CASCADEOUTB(NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED),
        .CLKARDCLK(m00_axis_aclk),
        .CLKBWRCLK(1'b0),
        .DBITERR(NLW_ram_reg_0_DBITERR_UNCONNECTED),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,Q[15:0]}),
        .DIBDI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0,Q[17:16]}),
        .DIPBDIP({1'b1,1'b1,1'b1,1'b1}),
        .DOADO({NLW_ram_reg_0_DOADO_UNCONNECTED[31:16],dout0_out[15:0]}),
        .DOBDO(NLW_ram_reg_0_DOBDO_UNCONNECTED[31:0]),
        .DOPADOP({NLW_ram_reg_0_DOPADOP_UNCONNECTED[3:2],dout0_out[17:16]}),
        .DOPBDOP(NLW_ram_reg_0_DOPBDOP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_ram_reg_0_ECCPARITY_UNCONNECTED[7:0]),
        .ENARDEN(en),
        .ENBWREN(1'b0),
        .INJECTDBITERR(NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED),
        .INJECTSBITERR(NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED),
        .RDADDRECC(NLW_ram_reg_0_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_ram_reg_0_SBITERR_UNCONNECTED),
        .WEA({ram_reg_0_i_1__1_n_0,ram_reg_0_i_1__1_n_0,ram_reg_0_i_1__1_n_0,ram_reg_0_i_1__1_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
  LUT3 #(
    .INIT(8'h80)) 
    ram_reg_0_i_1__1
       (.I0(en),
        .I1(WE_r),
        .I2(square_r),
        .O(ram_reg_0_i_1__1_n_0));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d14" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "61472" *) 
  (* RTL_RAM_NAME = "ram" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "2047" *) 
  (* bram_slice_begin = "18" *) 
  (* bram_slice_end = "31" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "2047" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "18" *) 
  (* ram_slice_end = "31" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(18),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(18),
    .WRITE_WIDTH_B(0)) 
    ram_reg_1
       (.ADDRARDADDR({1'b1,addr,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CASCADEINA(1'b1),
        .CASCADEINB(1'b0),
        .CASCADEOUTA(NLW_ram_reg_1_CASCADEOUTA_UNCONNECTED),
        .CASCADEOUTB(NLW_ram_reg_1_CASCADEOUTB_UNCONNECTED),
        .CLKARDCLK(m00_axis_aclk),
        .CLKBWRCLK(1'b0),
        .DBITERR(NLW_ram_reg_1_DBITERR_UNCONNECTED),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,Q[31:18]}),
        .DIBDI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0,1'b0,1'b0}),
        .DIPBDIP({1'b1,1'b1,1'b1,1'b1}),
        .DOADO({NLW_ram_reg_1_DOADO_UNCONNECTED[31:14],dout0_out[31:18]}),
        .DOBDO(NLW_ram_reg_1_DOBDO_UNCONNECTED[31:0]),
        .DOPADOP(NLW_ram_reg_1_DOPADOP_UNCONNECTED[3:0]),
        .DOPBDOP(NLW_ram_reg_1_DOPBDOP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_ram_reg_1_ECCPARITY_UNCONNECTED[7:0]),
        .ENARDEN(en),
        .ENBWREN(1'b0),
        .INJECTDBITERR(NLW_ram_reg_1_INJECTDBITERR_UNCONNECTED),
        .INJECTSBITERR(NLW_ram_reg_1_INJECTSBITERR_UNCONNECTED),
        .RDADDRECC(NLW_ram_reg_1_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_ram_reg_1_SBITERR_UNCONNECTED),
        .WEA({ram_reg_0_i_1__1_n_0,ram_reg_0_i_1__1_n_0,ram_reg_0_i_1__1_n_0,ram_reg_0_i_1__1_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
endmodule

(* ORIG_REF_NAME = "rams_sp_rf_rst" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_10
   (ram_reg_1_0,
    ram_reg_1_1,
    ram_reg_1_2,
    ram_reg_1_3,
    \s00_axis_tuser[18] ,
    ram_reg_1_4,
    \s00_axis_tuser[18]_0 ,
    ram_reg_1_5,
    ram_reg_1_6,
    ram_reg_1_7,
    ram_reg_1_8,
    \s00_axis_tuser[18]_1 ,
    ram_reg_1_9,
    ram_reg_1_10,
    ram_reg_0_0,
    \s00_axis_tuser[18]_2 ,
    \s00_axis_tuser[18]_3 ,
    ram_reg_0_1,
    ram_reg_0_2,
    ram_reg_0_3,
    \s00_axis_tuser[18]_4 ,
    ram_reg_0_4,
    \s00_axis_tuser[18]_5 ,
    ram_reg_0_5,
    ram_reg_0_6,
    ram_reg_0_7,
    ram_reg_0_8,
    \s00_axis_tuser[18]_6 ,
    ram_reg_0_9,
    ram_reg_0_10,
    ram_reg_0_11,
    \s00_axis_tuser[18]_7 ,
    dout0_out,
    s00_axis_tuser,
    \m00_axis_tdata_r_reg[31] ,
    \m00_axis_tdata_r_reg[31]_0 ,
    \m00_axis_tdata_r_reg[27] ,
    \m00_axis_tdata_r_reg[25] ,
    \m00_axis_tdata_r_reg[20] ,
    \m00_axis_tdata_r_reg[16] ,
    \m00_axis_tdata_r_reg[15] ,
    \m00_axis_tdata_r_reg[11] ,
    \m00_axis_tdata_r_reg[9] ,
    \m00_axis_tdata_r_reg[4] ,
    \m00_axis_tdata_r_reg[0] ,
    m00_axis_aclk,
    en,
    ADDRARDADDR,
    Q,
    addr,
    WE_r,
    square_r);
  output ram_reg_1_0;
  output ram_reg_1_1;
  output ram_reg_1_2;
  output ram_reg_1_3;
  output \s00_axis_tuser[18] ;
  output ram_reg_1_4;
  output \s00_axis_tuser[18]_0 ;
  output ram_reg_1_5;
  output ram_reg_1_6;
  output ram_reg_1_7;
  output ram_reg_1_8;
  output \s00_axis_tuser[18]_1 ;
  output ram_reg_1_9;
  output ram_reg_1_10;
  output ram_reg_0_0;
  output \s00_axis_tuser[18]_2 ;
  output \s00_axis_tuser[18]_3 ;
  output ram_reg_0_1;
  output ram_reg_0_2;
  output ram_reg_0_3;
  output \s00_axis_tuser[18]_4 ;
  output ram_reg_0_4;
  output \s00_axis_tuser[18]_5 ;
  output ram_reg_0_5;
  output ram_reg_0_6;
  output ram_reg_0_7;
  output ram_reg_0_8;
  output \s00_axis_tuser[18]_6 ;
  output ram_reg_0_9;
  output ram_reg_0_10;
  output ram_reg_0_11;
  output \s00_axis_tuser[18]_7 ;
  input [31:0]dout0_out;
  input [2:0]s00_axis_tuser;
  input [31:0]\m00_axis_tdata_r_reg[31] ;
  input [31:0]\m00_axis_tdata_r_reg[31]_0 ;
  input \m00_axis_tdata_r_reg[27] ;
  input \m00_axis_tdata_r_reg[25] ;
  input \m00_axis_tdata_r_reg[20] ;
  input \m00_axis_tdata_r_reg[16] ;
  input \m00_axis_tdata_r_reg[15] ;
  input \m00_axis_tdata_r_reg[11] ;
  input \m00_axis_tdata_r_reg[9] ;
  input \m00_axis_tdata_r_reg[4] ;
  input \m00_axis_tdata_r_reg[0] ;
  input m00_axis_aclk;
  input en;
  input [10:0]ADDRARDADDR;
  input [31:0]Q;
  input [10:0]addr;
  input WE_r;
  input square_r;

  wire [10:0]ADDRARDADDR;
  wire [31:0]DOUTB_w;
  wire [31:0]Q;
  wire WE_r;
  wire [10:0]addr;
  wire [31:0]dout0_out;
  wire en;
  wire m00_axis_aclk;
  wire \m00_axis_tdata_r[0]_i_5_n_0 ;
  wire \m00_axis_tdata_r[11]_i_5_n_0 ;
  wire \m00_axis_tdata_r[15]_i_5_n_0 ;
  wire \m00_axis_tdata_r[16]_i_5_n_0 ;
  wire \m00_axis_tdata_r[20]_i_5_n_0 ;
  wire \m00_axis_tdata_r[25]_i_5_n_0 ;
  wire \m00_axis_tdata_r[27]_i_5_n_0 ;
  wire \m00_axis_tdata_r[4]_i_5_n_0 ;
  wire \m00_axis_tdata_r[9]_i_5_n_0 ;
  wire \m00_axis_tdata_r_reg[0] ;
  wire \m00_axis_tdata_r_reg[11] ;
  wire \m00_axis_tdata_r_reg[15] ;
  wire \m00_axis_tdata_r_reg[16] ;
  wire \m00_axis_tdata_r_reg[20] ;
  wire \m00_axis_tdata_r_reg[25] ;
  wire \m00_axis_tdata_r_reg[27] ;
  wire [31:0]\m00_axis_tdata_r_reg[31] ;
  wire [31:0]\m00_axis_tdata_r_reg[31]_0 ;
  wire \m00_axis_tdata_r_reg[4] ;
  wire \m00_axis_tdata_r_reg[9] ;
  wire ram_reg_0_0;
  wire ram_reg_0_1;
  wire ram_reg_0_10;
  wire ram_reg_0_11;
  wire ram_reg_0_2;
  wire ram_reg_0_3;
  wire ram_reg_0_4;
  wire ram_reg_0_5;
  wire ram_reg_0_6;
  wire ram_reg_0_7;
  wire ram_reg_0_8;
  wire ram_reg_0_9;
  wire ram_reg_0_i_1__10_n_0;
  wire ram_reg_1_0;
  wire ram_reg_1_1;
  wire ram_reg_1_10;
  wire ram_reg_1_2;
  wire ram_reg_1_3;
  wire ram_reg_1_4;
  wire ram_reg_1_5;
  wire ram_reg_1_6;
  wire ram_reg_1_7;
  wire ram_reg_1_8;
  wire ram_reg_1_9;
  wire [2:0]s00_axis_tuser;
  wire \s00_axis_tuser[18] ;
  wire \s00_axis_tuser[18]_0 ;
  wire \s00_axis_tuser[18]_1 ;
  wire \s00_axis_tuser[18]_2 ;
  wire \s00_axis_tuser[18]_3 ;
  wire \s00_axis_tuser[18]_4 ;
  wire \s00_axis_tuser[18]_5 ;
  wire \s00_axis_tuser[18]_6 ;
  wire \s00_axis_tuser[18]_7 ;
  wire square_r;
  wire NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED;
  wire NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED;
  wire NLW_ram_reg_0_DBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_SBITERR_UNCONNECTED;
  wire [31:16]NLW_ram_reg_0_DOADO_UNCONNECTED;
  wire [31:0]NLW_ram_reg_0_DOBDO_UNCONNECTED;
  wire [3:2]NLW_ram_reg_0_DOPADOP_UNCONNECTED;
  wire [3:0]NLW_ram_reg_0_DOPBDOP_UNCONNECTED;
  wire [7:0]NLW_ram_reg_0_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_ram_reg_0_RDADDRECC_UNCONNECTED;
  wire NLW_ram_reg_1_CASCADEOUTA_UNCONNECTED;
  wire NLW_ram_reg_1_CASCADEOUTB_UNCONNECTED;
  wire NLW_ram_reg_1_DBITERR_UNCONNECTED;
  wire NLW_ram_reg_1_INJECTDBITERR_UNCONNECTED;
  wire NLW_ram_reg_1_INJECTSBITERR_UNCONNECTED;
  wire NLW_ram_reg_1_SBITERR_UNCONNECTED;
  wire [31:14]NLW_ram_reg_1_DOADO_UNCONNECTED;
  wire [31:0]NLW_ram_reg_1_DOBDO_UNCONNECTED;
  wire [3:0]NLW_ram_reg_1_DOPADOP_UNCONNECTED;
  wire [3:0]NLW_ram_reg_1_DOPBDOP_UNCONNECTED;
  wire [7:0]NLW_ram_reg_1_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_ram_reg_1_RDADDRECC_UNCONNECTED;

  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata_r[0]_i_5 
       (.I0(DOUTB_w[0]),
        .I1(dout0_out[0]),
        .I2(s00_axis_tuser[1]),
        .I3(\m00_axis_tdata_r_reg[31] [0]),
        .I4(s00_axis_tuser[0]),
        .I5(\m00_axis_tdata_r_reg[31]_0 [0]),
        .O(\m00_axis_tdata_r[0]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata_r[10]_i_3 
       (.I0(DOUTB_w[10]),
        .I1(dout0_out[10]),
        .I2(s00_axis_tuser[1]),
        .I3(\m00_axis_tdata_r_reg[31] [10]),
        .I4(s00_axis_tuser[0]),
        .I5(\m00_axis_tdata_r_reg[31]_0 [10]),
        .O(ram_reg_0_4));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata_r[11]_i_5 
       (.I0(DOUTB_w[11]),
        .I1(dout0_out[11]),
        .I2(s00_axis_tuser[1]),
        .I3(\m00_axis_tdata_r_reg[31] [11]),
        .I4(s00_axis_tuser[0]),
        .I5(\m00_axis_tdata_r_reg[31]_0 [11]),
        .O(\m00_axis_tdata_r[11]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata_r[12]_i_3 
       (.I0(DOUTB_w[12]),
        .I1(dout0_out[12]),
        .I2(s00_axis_tuser[1]),
        .I3(\m00_axis_tdata_r_reg[31] [12]),
        .I4(s00_axis_tuser[0]),
        .I5(\m00_axis_tdata_r_reg[31]_0 [12]),
        .O(ram_reg_0_3));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata_r[13]_i_3 
       (.I0(DOUTB_w[13]),
        .I1(dout0_out[13]),
        .I2(s00_axis_tuser[1]),
        .I3(\m00_axis_tdata_r_reg[31] [13]),
        .I4(s00_axis_tuser[0]),
        .I5(\m00_axis_tdata_r_reg[31]_0 [13]),
        .O(ram_reg_0_2));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata_r[14]_i_3 
       (.I0(DOUTB_w[14]),
        .I1(dout0_out[14]),
        .I2(s00_axis_tuser[1]),
        .I3(\m00_axis_tdata_r_reg[31] [14]),
        .I4(s00_axis_tuser[0]),
        .I5(\m00_axis_tdata_r_reg[31]_0 [14]),
        .O(ram_reg_0_1));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata_r[15]_i_5 
       (.I0(DOUTB_w[15]),
        .I1(dout0_out[15]),
        .I2(s00_axis_tuser[1]),
        .I3(\m00_axis_tdata_r_reg[31] [15]),
        .I4(s00_axis_tuser[0]),
        .I5(\m00_axis_tdata_r_reg[31]_0 [15]),
        .O(\m00_axis_tdata_r[15]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata_r[16]_i_5 
       (.I0(DOUTB_w[16]),
        .I1(dout0_out[16]),
        .I2(s00_axis_tuser[1]),
        .I3(\m00_axis_tdata_r_reg[31] [16]),
        .I4(s00_axis_tuser[0]),
        .I5(\m00_axis_tdata_r_reg[31]_0 [16]),
        .O(\m00_axis_tdata_r[16]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata_r[17]_i_3 
       (.I0(DOUTB_w[17]),
        .I1(dout0_out[17]),
        .I2(s00_axis_tuser[1]),
        .I3(\m00_axis_tdata_r_reg[31] [17]),
        .I4(s00_axis_tuser[0]),
        .I5(\m00_axis_tdata_r_reg[31]_0 [17]),
        .O(ram_reg_0_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata_r[18]_i_3 
       (.I0(DOUTB_w[18]),
        .I1(dout0_out[18]),
        .I2(s00_axis_tuser[1]),
        .I3(\m00_axis_tdata_r_reg[31] [18]),
        .I4(s00_axis_tuser[0]),
        .I5(\m00_axis_tdata_r_reg[31]_0 [18]),
        .O(ram_reg_1_10));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata_r[19]_i_3 
       (.I0(DOUTB_w[19]),
        .I1(dout0_out[19]),
        .I2(s00_axis_tuser[1]),
        .I3(\m00_axis_tdata_r_reg[31] [19]),
        .I4(s00_axis_tuser[0]),
        .I5(\m00_axis_tdata_r_reg[31]_0 [19]),
        .O(ram_reg_1_9));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata_r[1]_i_3 
       (.I0(DOUTB_w[1]),
        .I1(dout0_out[1]),
        .I2(s00_axis_tuser[1]),
        .I3(\m00_axis_tdata_r_reg[31] [1]),
        .I4(s00_axis_tuser[0]),
        .I5(\m00_axis_tdata_r_reg[31]_0 [1]),
        .O(ram_reg_0_11));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata_r[20]_i_5 
       (.I0(DOUTB_w[20]),
        .I1(dout0_out[20]),
        .I2(s00_axis_tuser[1]),
        .I3(\m00_axis_tdata_r_reg[31] [20]),
        .I4(s00_axis_tuser[0]),
        .I5(\m00_axis_tdata_r_reg[31]_0 [20]),
        .O(\m00_axis_tdata_r[20]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata_r[21]_i_3 
       (.I0(DOUTB_w[21]),
        .I1(dout0_out[21]),
        .I2(s00_axis_tuser[1]),
        .I3(\m00_axis_tdata_r_reg[31] [21]),
        .I4(s00_axis_tuser[0]),
        .I5(\m00_axis_tdata_r_reg[31]_0 [21]),
        .O(ram_reg_1_8));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata_r[22]_i_3 
       (.I0(DOUTB_w[22]),
        .I1(dout0_out[22]),
        .I2(s00_axis_tuser[1]),
        .I3(\m00_axis_tdata_r_reg[31] [22]),
        .I4(s00_axis_tuser[0]),
        .I5(\m00_axis_tdata_r_reg[31]_0 [22]),
        .O(ram_reg_1_7));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata_r[23]_i_3 
       (.I0(DOUTB_w[23]),
        .I1(dout0_out[23]),
        .I2(s00_axis_tuser[1]),
        .I3(\m00_axis_tdata_r_reg[31] [23]),
        .I4(s00_axis_tuser[0]),
        .I5(\m00_axis_tdata_r_reg[31]_0 [23]),
        .O(ram_reg_1_6));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata_r[24]_i_3 
       (.I0(DOUTB_w[24]),
        .I1(dout0_out[24]),
        .I2(s00_axis_tuser[1]),
        .I3(\m00_axis_tdata_r_reg[31] [24]),
        .I4(s00_axis_tuser[0]),
        .I5(\m00_axis_tdata_r_reg[31]_0 [24]),
        .O(ram_reg_1_5));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata_r[25]_i_5 
       (.I0(DOUTB_w[25]),
        .I1(dout0_out[25]),
        .I2(s00_axis_tuser[1]),
        .I3(\m00_axis_tdata_r_reg[31] [25]),
        .I4(s00_axis_tuser[0]),
        .I5(\m00_axis_tdata_r_reg[31]_0 [25]),
        .O(\m00_axis_tdata_r[25]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata_r[26]_i_3 
       (.I0(DOUTB_w[26]),
        .I1(dout0_out[26]),
        .I2(s00_axis_tuser[1]),
        .I3(\m00_axis_tdata_r_reg[31] [26]),
        .I4(s00_axis_tuser[0]),
        .I5(\m00_axis_tdata_r_reg[31]_0 [26]),
        .O(ram_reg_1_4));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata_r[27]_i_5 
       (.I0(DOUTB_w[27]),
        .I1(dout0_out[27]),
        .I2(s00_axis_tuser[1]),
        .I3(\m00_axis_tdata_r_reg[31] [27]),
        .I4(s00_axis_tuser[0]),
        .I5(\m00_axis_tdata_r_reg[31]_0 [27]),
        .O(\m00_axis_tdata_r[27]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata_r[28]_i_3 
       (.I0(DOUTB_w[28]),
        .I1(dout0_out[28]),
        .I2(s00_axis_tuser[1]),
        .I3(\m00_axis_tdata_r_reg[31] [28]),
        .I4(s00_axis_tuser[0]),
        .I5(\m00_axis_tdata_r_reg[31]_0 [28]),
        .O(ram_reg_1_3));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata_r[29]_i_3 
       (.I0(DOUTB_w[29]),
        .I1(dout0_out[29]),
        .I2(s00_axis_tuser[1]),
        .I3(\m00_axis_tdata_r_reg[31] [29]),
        .I4(s00_axis_tuser[0]),
        .I5(\m00_axis_tdata_r_reg[31]_0 [29]),
        .O(ram_reg_1_2));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata_r[2]_i_3 
       (.I0(DOUTB_w[2]),
        .I1(dout0_out[2]),
        .I2(s00_axis_tuser[1]),
        .I3(\m00_axis_tdata_r_reg[31] [2]),
        .I4(s00_axis_tuser[0]),
        .I5(\m00_axis_tdata_r_reg[31]_0 [2]),
        .O(ram_reg_0_10));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata_r[30]_i_3 
       (.I0(DOUTB_w[30]),
        .I1(dout0_out[30]),
        .I2(s00_axis_tuser[1]),
        .I3(\m00_axis_tdata_r_reg[31] [30]),
        .I4(s00_axis_tuser[0]),
        .I5(\m00_axis_tdata_r_reg[31]_0 [30]),
        .O(ram_reg_1_1));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata_r[31]_i_8 
       (.I0(DOUTB_w[31]),
        .I1(dout0_out[31]),
        .I2(s00_axis_tuser[1]),
        .I3(\m00_axis_tdata_r_reg[31] [31]),
        .I4(s00_axis_tuser[0]),
        .I5(\m00_axis_tdata_r_reg[31]_0 [31]),
        .O(ram_reg_1_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata_r[3]_i_3 
       (.I0(DOUTB_w[3]),
        .I1(dout0_out[3]),
        .I2(s00_axis_tuser[1]),
        .I3(\m00_axis_tdata_r_reg[31] [3]),
        .I4(s00_axis_tuser[0]),
        .I5(\m00_axis_tdata_r_reg[31]_0 [3]),
        .O(ram_reg_0_9));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata_r[4]_i_5 
       (.I0(DOUTB_w[4]),
        .I1(dout0_out[4]),
        .I2(s00_axis_tuser[1]),
        .I3(\m00_axis_tdata_r_reg[31] [4]),
        .I4(s00_axis_tuser[0]),
        .I5(\m00_axis_tdata_r_reg[31]_0 [4]),
        .O(\m00_axis_tdata_r[4]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata_r[5]_i_3 
       (.I0(DOUTB_w[5]),
        .I1(dout0_out[5]),
        .I2(s00_axis_tuser[1]),
        .I3(\m00_axis_tdata_r_reg[31] [5]),
        .I4(s00_axis_tuser[0]),
        .I5(\m00_axis_tdata_r_reg[31]_0 [5]),
        .O(ram_reg_0_8));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata_r[6]_i_3 
       (.I0(DOUTB_w[6]),
        .I1(dout0_out[6]),
        .I2(s00_axis_tuser[1]),
        .I3(\m00_axis_tdata_r_reg[31] [6]),
        .I4(s00_axis_tuser[0]),
        .I5(\m00_axis_tdata_r_reg[31]_0 [6]),
        .O(ram_reg_0_7));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata_r[7]_i_3 
       (.I0(DOUTB_w[7]),
        .I1(dout0_out[7]),
        .I2(s00_axis_tuser[1]),
        .I3(\m00_axis_tdata_r_reg[31] [7]),
        .I4(s00_axis_tuser[0]),
        .I5(\m00_axis_tdata_r_reg[31]_0 [7]),
        .O(ram_reg_0_6));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata_r[8]_i_3 
       (.I0(DOUTB_w[8]),
        .I1(dout0_out[8]),
        .I2(s00_axis_tuser[1]),
        .I3(\m00_axis_tdata_r_reg[31] [8]),
        .I4(s00_axis_tuser[0]),
        .I5(\m00_axis_tdata_r_reg[31]_0 [8]),
        .O(ram_reg_0_5));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata_r[9]_i_5 
       (.I0(DOUTB_w[9]),
        .I1(dout0_out[9]),
        .I2(s00_axis_tuser[1]),
        .I3(\m00_axis_tdata_r_reg[31] [9]),
        .I4(s00_axis_tuser[0]),
        .I5(\m00_axis_tdata_r_reg[31]_0 [9]),
        .O(\m00_axis_tdata_r[9]_i_5_n_0 ));
  MUXF7 \m00_axis_tdata_r_reg[0]_i_4 
       (.I0(\m00_axis_tdata_r[0]_i_5_n_0 ),
        .I1(\m00_axis_tdata_r_reg[0] ),
        .O(\s00_axis_tuser[18]_7 ),
        .S(s00_axis_tuser[2]));
  MUXF7 \m00_axis_tdata_r_reg[11]_i_4 
       (.I0(\m00_axis_tdata_r[11]_i_5_n_0 ),
        .I1(\m00_axis_tdata_r_reg[11] ),
        .O(\s00_axis_tuser[18]_4 ),
        .S(s00_axis_tuser[2]));
  MUXF7 \m00_axis_tdata_r_reg[15]_i_4 
       (.I0(\m00_axis_tdata_r[15]_i_5_n_0 ),
        .I1(\m00_axis_tdata_r_reg[15] ),
        .O(\s00_axis_tuser[18]_3 ),
        .S(s00_axis_tuser[2]));
  MUXF7 \m00_axis_tdata_r_reg[16]_i_4 
       (.I0(\m00_axis_tdata_r[16]_i_5_n_0 ),
        .I1(\m00_axis_tdata_r_reg[16] ),
        .O(\s00_axis_tuser[18]_2 ),
        .S(s00_axis_tuser[2]));
  MUXF7 \m00_axis_tdata_r_reg[20]_i_4 
       (.I0(\m00_axis_tdata_r[20]_i_5_n_0 ),
        .I1(\m00_axis_tdata_r_reg[20] ),
        .O(\s00_axis_tuser[18]_1 ),
        .S(s00_axis_tuser[2]));
  MUXF7 \m00_axis_tdata_r_reg[25]_i_4 
       (.I0(\m00_axis_tdata_r[25]_i_5_n_0 ),
        .I1(\m00_axis_tdata_r_reg[25] ),
        .O(\s00_axis_tuser[18]_0 ),
        .S(s00_axis_tuser[2]));
  MUXF7 \m00_axis_tdata_r_reg[27]_i_4 
       (.I0(\m00_axis_tdata_r[27]_i_5_n_0 ),
        .I1(\m00_axis_tdata_r_reg[27] ),
        .O(\s00_axis_tuser[18] ),
        .S(s00_axis_tuser[2]));
  MUXF7 \m00_axis_tdata_r_reg[4]_i_4 
       (.I0(\m00_axis_tdata_r[4]_i_5_n_0 ),
        .I1(\m00_axis_tdata_r_reg[4] ),
        .O(\s00_axis_tuser[18]_6 ),
        .S(s00_axis_tuser[2]));
  MUXF7 \m00_axis_tdata_r_reg[9]_i_4 
       (.I0(\m00_axis_tdata_r[9]_i_5_n_0 ),
        .I1(\m00_axis_tdata_r_reg[9] ),
        .O(\s00_axis_tuser[18]_5 ),
        .S(s00_axis_tuser[2]));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p2_d16" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "61472" *) 
  (* RTL_RAM_NAME = "ram" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "2047" *) 
  (* bram_slice_begin = "0" *) 
  (* bram_slice_end = "17" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "2047" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "17" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(18),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(18),
    .WRITE_WIDTH_B(0)) 
    ram_reg_0
       (.ADDRARDADDR({1'b1,ADDRARDADDR,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CASCADEINA(1'b1),
        .CASCADEINB(1'b0),
        .CASCADEOUTA(NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED),
        .CASCADEOUTB(NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED),
        .CLKARDCLK(m00_axis_aclk),
        .CLKBWRCLK(1'b0),
        .DBITERR(NLW_ram_reg_0_DBITERR_UNCONNECTED),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,Q[15:0]}),
        .DIBDI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0,Q[17:16]}),
        .DIPBDIP({1'b1,1'b1,1'b1,1'b1}),
        .DOADO({NLW_ram_reg_0_DOADO_UNCONNECTED[31:16],DOUTB_w[15:0]}),
        .DOBDO(NLW_ram_reg_0_DOBDO_UNCONNECTED[31:0]),
        .DOPADOP({NLW_ram_reg_0_DOPADOP_UNCONNECTED[3:2],DOUTB_w[17:16]}),
        .DOPBDOP(NLW_ram_reg_0_DOPBDOP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_ram_reg_0_ECCPARITY_UNCONNECTED[7:0]),
        .ENARDEN(en),
        .ENBWREN(1'b0),
        .INJECTDBITERR(NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED),
        .INJECTSBITERR(NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED),
        .RDADDRECC(NLW_ram_reg_0_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_ram_reg_0_SBITERR_UNCONNECTED),
        .WEA({ram_reg_0_i_1__10_n_0,ram_reg_0_i_1__10_n_0,ram_reg_0_i_1__10_n_0,ram_reg_0_i_1__10_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
  LUT3 #(
    .INIT(8'h80)) 
    ram_reg_0_i_1__10
       (.I0(en),
        .I1(WE_r),
        .I2(square_r),
        .O(ram_reg_0_i_1__10_n_0));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d14" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "61472" *) 
  (* RTL_RAM_NAME = "ram" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "2047" *) 
  (* bram_slice_begin = "18" *) 
  (* bram_slice_end = "31" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "2047" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "18" *) 
  (* ram_slice_end = "31" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(18),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(18),
    .WRITE_WIDTH_B(0)) 
    ram_reg_1
       (.ADDRARDADDR({1'b1,addr,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CASCADEINA(1'b1),
        .CASCADEINB(1'b0),
        .CASCADEOUTA(NLW_ram_reg_1_CASCADEOUTA_UNCONNECTED),
        .CASCADEOUTB(NLW_ram_reg_1_CASCADEOUTB_UNCONNECTED),
        .CLKARDCLK(m00_axis_aclk),
        .CLKBWRCLK(1'b0),
        .DBITERR(NLW_ram_reg_1_DBITERR_UNCONNECTED),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,Q[31:18]}),
        .DIBDI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0,1'b0,1'b0}),
        .DIPBDIP({1'b1,1'b1,1'b1,1'b1}),
        .DOADO({NLW_ram_reg_1_DOADO_UNCONNECTED[31:14],DOUTB_w[31:18]}),
        .DOBDO(NLW_ram_reg_1_DOBDO_UNCONNECTED[31:0]),
        .DOPADOP(NLW_ram_reg_1_DOPADOP_UNCONNECTED[3:0]),
        .DOPBDOP(NLW_ram_reg_1_DOPBDOP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_ram_reg_1_ECCPARITY_UNCONNECTED[7:0]),
        .ENARDEN(en),
        .ENBWREN(1'b0),
        .INJECTDBITERR(NLW_ram_reg_1_INJECTDBITERR_UNCONNECTED),
        .INJECTSBITERR(NLW_ram_reg_1_INJECTSBITERR_UNCONNECTED),
        .RDADDRECC(NLW_ram_reg_1_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_ram_reg_1_SBITERR_UNCONNECTED),
        .WEA({ram_reg_0_i_1__10_n_0,ram_reg_0_i_1__10_n_0,ram_reg_0_i_1__10_n_0,ram_reg_0_i_1__10_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
endmodule

(* ORIG_REF_NAME = "rams_sp_rf_rst" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_11
   (dout0_out,
    m00_axis_aclk,
    en,
    ADDRARDADDR,
    Q,
    addr,
    WE_r,
    square_r);
  output [31:0]dout0_out;
  input m00_axis_aclk;
  input en;
  input [10:0]ADDRARDADDR;
  input [31:0]Q;
  input [10:0]addr;
  input WE_r;
  input square_r;

  wire [10:0]ADDRARDADDR;
  wire [31:0]Q;
  wire WE_r;
  wire [10:0]addr;
  wire [31:0]dout0_out;
  wire en;
  wire m00_axis_aclk;
  wire ram_reg_0_i_1__11_n_0;
  wire square_r;
  wire NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED;
  wire NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED;
  wire NLW_ram_reg_0_DBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_SBITERR_UNCONNECTED;
  wire [31:16]NLW_ram_reg_0_DOADO_UNCONNECTED;
  wire [31:0]NLW_ram_reg_0_DOBDO_UNCONNECTED;
  wire [3:2]NLW_ram_reg_0_DOPADOP_UNCONNECTED;
  wire [3:0]NLW_ram_reg_0_DOPBDOP_UNCONNECTED;
  wire [7:0]NLW_ram_reg_0_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_ram_reg_0_RDADDRECC_UNCONNECTED;
  wire NLW_ram_reg_1_CASCADEOUTA_UNCONNECTED;
  wire NLW_ram_reg_1_CASCADEOUTB_UNCONNECTED;
  wire NLW_ram_reg_1_DBITERR_UNCONNECTED;
  wire NLW_ram_reg_1_INJECTDBITERR_UNCONNECTED;
  wire NLW_ram_reg_1_INJECTSBITERR_UNCONNECTED;
  wire NLW_ram_reg_1_SBITERR_UNCONNECTED;
  wire [31:14]NLW_ram_reg_1_DOADO_UNCONNECTED;
  wire [31:0]NLW_ram_reg_1_DOBDO_UNCONNECTED;
  wire [3:0]NLW_ram_reg_1_DOPADOP_UNCONNECTED;
  wire [3:0]NLW_ram_reg_1_DOPBDOP_UNCONNECTED;
  wire [7:0]NLW_ram_reg_1_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_ram_reg_1_RDADDRECC_UNCONNECTED;

  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p2_d16" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "61472" *) 
  (* RTL_RAM_NAME = "ram" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "2047" *) 
  (* bram_slice_begin = "0" *) 
  (* bram_slice_end = "17" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "2047" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "17" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(18),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(18),
    .WRITE_WIDTH_B(0)) 
    ram_reg_0
       (.ADDRARDADDR({1'b1,ADDRARDADDR,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CASCADEINA(1'b1),
        .CASCADEINB(1'b0),
        .CASCADEOUTA(NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED),
        .CASCADEOUTB(NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED),
        .CLKARDCLK(m00_axis_aclk),
        .CLKBWRCLK(1'b0),
        .DBITERR(NLW_ram_reg_0_DBITERR_UNCONNECTED),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,Q[15:0]}),
        .DIBDI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0,Q[17:16]}),
        .DIPBDIP({1'b1,1'b1,1'b1,1'b1}),
        .DOADO({NLW_ram_reg_0_DOADO_UNCONNECTED[31:16],dout0_out[15:0]}),
        .DOBDO(NLW_ram_reg_0_DOBDO_UNCONNECTED[31:0]),
        .DOPADOP({NLW_ram_reg_0_DOPADOP_UNCONNECTED[3:2],dout0_out[17:16]}),
        .DOPBDOP(NLW_ram_reg_0_DOPBDOP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_ram_reg_0_ECCPARITY_UNCONNECTED[7:0]),
        .ENARDEN(en),
        .ENBWREN(1'b0),
        .INJECTDBITERR(NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED),
        .INJECTSBITERR(NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED),
        .RDADDRECC(NLW_ram_reg_0_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_ram_reg_0_SBITERR_UNCONNECTED),
        .WEA({ram_reg_0_i_1__11_n_0,ram_reg_0_i_1__11_n_0,ram_reg_0_i_1__11_n_0,ram_reg_0_i_1__11_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
  LUT3 #(
    .INIT(8'h80)) 
    ram_reg_0_i_1__11
       (.I0(en),
        .I1(WE_r),
        .I2(square_r),
        .O(ram_reg_0_i_1__11_n_0));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d14" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "61472" *) 
  (* RTL_RAM_NAME = "ram" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "2047" *) 
  (* bram_slice_begin = "18" *) 
  (* bram_slice_end = "31" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "2047" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "18" *) 
  (* ram_slice_end = "31" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(18),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(18),
    .WRITE_WIDTH_B(0)) 
    ram_reg_1
       (.ADDRARDADDR({1'b1,addr,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CASCADEINA(1'b1),
        .CASCADEINB(1'b0),
        .CASCADEOUTA(NLW_ram_reg_1_CASCADEOUTA_UNCONNECTED),
        .CASCADEOUTB(NLW_ram_reg_1_CASCADEOUTB_UNCONNECTED),
        .CLKARDCLK(m00_axis_aclk),
        .CLKBWRCLK(1'b0),
        .DBITERR(NLW_ram_reg_1_DBITERR_UNCONNECTED),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,Q[31:18]}),
        .DIBDI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0,1'b0,1'b0}),
        .DIPBDIP({1'b1,1'b1,1'b1,1'b1}),
        .DOADO({NLW_ram_reg_1_DOADO_UNCONNECTED[31:14],dout0_out[31:18]}),
        .DOBDO(NLW_ram_reg_1_DOBDO_UNCONNECTED[31:0]),
        .DOPADOP(NLW_ram_reg_1_DOPADOP_UNCONNECTED[3:0]),
        .DOPBDOP(NLW_ram_reg_1_DOPBDOP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_ram_reg_1_ECCPARITY_UNCONNECTED[7:0]),
        .ENARDEN(en),
        .ENBWREN(1'b0),
        .INJECTDBITERR(NLW_ram_reg_1_INJECTDBITERR_UNCONNECTED),
        .INJECTSBITERR(NLW_ram_reg_1_INJECTSBITERR_UNCONNECTED),
        .RDADDRECC(NLW_ram_reg_1_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_ram_reg_1_SBITERR_UNCONNECTED),
        .WEA({ram_reg_0_i_1__11_n_0,ram_reg_0_i_1__11_n_0,ram_reg_0_i_1__11_n_0,ram_reg_0_i_1__11_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
endmodule

(* ORIG_REF_NAME = "rams_sp_rf_rst" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_12
   (dout0_out,
    m00_axis_aclk,
    en,
    ADDRARDADDR,
    Q,
    addr,
    WE_r,
    square_r);
  output [31:0]dout0_out;
  input m00_axis_aclk;
  input en;
  input [10:0]ADDRARDADDR;
  input [31:0]Q;
  input [10:0]addr;
  input WE_r;
  input square_r;

  wire [10:0]ADDRARDADDR;
  wire [31:0]Q;
  wire WE_r;
  wire [10:0]addr;
  wire [31:0]dout0_out;
  wire en;
  wire m00_axis_aclk;
  wire ram_reg_0_i_1__12_n_0;
  wire square_r;
  wire NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED;
  wire NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED;
  wire NLW_ram_reg_0_DBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_SBITERR_UNCONNECTED;
  wire [31:16]NLW_ram_reg_0_DOADO_UNCONNECTED;
  wire [31:0]NLW_ram_reg_0_DOBDO_UNCONNECTED;
  wire [3:2]NLW_ram_reg_0_DOPADOP_UNCONNECTED;
  wire [3:0]NLW_ram_reg_0_DOPBDOP_UNCONNECTED;
  wire [7:0]NLW_ram_reg_0_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_ram_reg_0_RDADDRECC_UNCONNECTED;
  wire NLW_ram_reg_1_CASCADEOUTA_UNCONNECTED;
  wire NLW_ram_reg_1_CASCADEOUTB_UNCONNECTED;
  wire NLW_ram_reg_1_DBITERR_UNCONNECTED;
  wire NLW_ram_reg_1_INJECTDBITERR_UNCONNECTED;
  wire NLW_ram_reg_1_INJECTSBITERR_UNCONNECTED;
  wire NLW_ram_reg_1_SBITERR_UNCONNECTED;
  wire [31:14]NLW_ram_reg_1_DOADO_UNCONNECTED;
  wire [31:0]NLW_ram_reg_1_DOBDO_UNCONNECTED;
  wire [3:0]NLW_ram_reg_1_DOPADOP_UNCONNECTED;
  wire [3:0]NLW_ram_reg_1_DOPBDOP_UNCONNECTED;
  wire [7:0]NLW_ram_reg_1_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_ram_reg_1_RDADDRECC_UNCONNECTED;

  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p2_d16" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "61472" *) 
  (* RTL_RAM_NAME = "ram" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "2047" *) 
  (* bram_slice_begin = "0" *) 
  (* bram_slice_end = "17" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "2047" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "17" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(18),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(18),
    .WRITE_WIDTH_B(0)) 
    ram_reg_0
       (.ADDRARDADDR({1'b1,ADDRARDADDR,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CASCADEINA(1'b1),
        .CASCADEINB(1'b0),
        .CASCADEOUTA(NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED),
        .CASCADEOUTB(NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED),
        .CLKARDCLK(m00_axis_aclk),
        .CLKBWRCLK(1'b0),
        .DBITERR(NLW_ram_reg_0_DBITERR_UNCONNECTED),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,Q[15:0]}),
        .DIBDI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0,Q[17:16]}),
        .DIPBDIP({1'b1,1'b1,1'b1,1'b1}),
        .DOADO({NLW_ram_reg_0_DOADO_UNCONNECTED[31:16],dout0_out[15:0]}),
        .DOBDO(NLW_ram_reg_0_DOBDO_UNCONNECTED[31:0]),
        .DOPADOP({NLW_ram_reg_0_DOPADOP_UNCONNECTED[3:2],dout0_out[17:16]}),
        .DOPBDOP(NLW_ram_reg_0_DOPBDOP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_ram_reg_0_ECCPARITY_UNCONNECTED[7:0]),
        .ENARDEN(en),
        .ENBWREN(1'b0),
        .INJECTDBITERR(NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED),
        .INJECTSBITERR(NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED),
        .RDADDRECC(NLW_ram_reg_0_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_ram_reg_0_SBITERR_UNCONNECTED),
        .WEA({ram_reg_0_i_1__12_n_0,ram_reg_0_i_1__12_n_0,ram_reg_0_i_1__12_n_0,ram_reg_0_i_1__12_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
  LUT3 #(
    .INIT(8'h80)) 
    ram_reg_0_i_1__12
       (.I0(en),
        .I1(WE_r),
        .I2(square_r),
        .O(ram_reg_0_i_1__12_n_0));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d14" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "61472" *) 
  (* RTL_RAM_NAME = "ram" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "2047" *) 
  (* bram_slice_begin = "18" *) 
  (* bram_slice_end = "31" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "2047" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "18" *) 
  (* ram_slice_end = "31" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(18),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(18),
    .WRITE_WIDTH_B(0)) 
    ram_reg_1
       (.ADDRARDADDR({1'b1,addr,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CASCADEINA(1'b1),
        .CASCADEINB(1'b0),
        .CASCADEOUTA(NLW_ram_reg_1_CASCADEOUTA_UNCONNECTED),
        .CASCADEOUTB(NLW_ram_reg_1_CASCADEOUTB_UNCONNECTED),
        .CLKARDCLK(m00_axis_aclk),
        .CLKBWRCLK(1'b0),
        .DBITERR(NLW_ram_reg_1_DBITERR_UNCONNECTED),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,Q[31:18]}),
        .DIBDI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0,1'b0,1'b0}),
        .DIPBDIP({1'b1,1'b1,1'b1,1'b1}),
        .DOADO({NLW_ram_reg_1_DOADO_UNCONNECTED[31:14],dout0_out[31:18]}),
        .DOBDO(NLW_ram_reg_1_DOBDO_UNCONNECTED[31:0]),
        .DOPADOP(NLW_ram_reg_1_DOPADOP_UNCONNECTED[3:0]),
        .DOPBDOP(NLW_ram_reg_1_DOPBDOP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_ram_reg_1_ECCPARITY_UNCONNECTED[7:0]),
        .ENARDEN(en),
        .ENBWREN(1'b0),
        .INJECTDBITERR(NLW_ram_reg_1_INJECTDBITERR_UNCONNECTED),
        .INJECTSBITERR(NLW_ram_reg_1_INJECTSBITERR_UNCONNECTED),
        .RDADDRECC(NLW_ram_reg_1_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_ram_reg_1_SBITERR_UNCONNECTED),
        .WEA({ram_reg_0_i_1__12_n_0,ram_reg_0_i_1__12_n_0,ram_reg_0_i_1__12_n_0,ram_reg_0_i_1__12_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
endmodule

(* ORIG_REF_NAME = "rams_sp_rf_rst" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_13
   (dout0_out,
    m00_axis_aclk,
    en,
    ADDRARDADDR,
    Q,
    addr,
    WE_r,
    square_r);
  output [31:0]dout0_out;
  input m00_axis_aclk;
  input en;
  input [10:0]ADDRARDADDR;
  input [31:0]Q;
  input [10:0]addr;
  input WE_r;
  input square_r;

  wire [10:0]ADDRARDADDR;
  wire [31:0]Q;
  wire WE_r;
  wire [10:0]addr;
  wire [31:0]dout0_out;
  wire en;
  wire m00_axis_aclk;
  wire ram_reg_0_i_1__13_n_0;
  wire square_r;
  wire NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED;
  wire NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED;
  wire NLW_ram_reg_0_DBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_SBITERR_UNCONNECTED;
  wire [31:16]NLW_ram_reg_0_DOADO_UNCONNECTED;
  wire [31:0]NLW_ram_reg_0_DOBDO_UNCONNECTED;
  wire [3:2]NLW_ram_reg_0_DOPADOP_UNCONNECTED;
  wire [3:0]NLW_ram_reg_0_DOPBDOP_UNCONNECTED;
  wire [7:0]NLW_ram_reg_0_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_ram_reg_0_RDADDRECC_UNCONNECTED;
  wire NLW_ram_reg_1_CASCADEOUTA_UNCONNECTED;
  wire NLW_ram_reg_1_CASCADEOUTB_UNCONNECTED;
  wire NLW_ram_reg_1_DBITERR_UNCONNECTED;
  wire NLW_ram_reg_1_INJECTDBITERR_UNCONNECTED;
  wire NLW_ram_reg_1_INJECTSBITERR_UNCONNECTED;
  wire NLW_ram_reg_1_SBITERR_UNCONNECTED;
  wire [31:14]NLW_ram_reg_1_DOADO_UNCONNECTED;
  wire [31:0]NLW_ram_reg_1_DOBDO_UNCONNECTED;
  wire [3:0]NLW_ram_reg_1_DOPADOP_UNCONNECTED;
  wire [3:0]NLW_ram_reg_1_DOPBDOP_UNCONNECTED;
  wire [7:0]NLW_ram_reg_1_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_ram_reg_1_RDADDRECC_UNCONNECTED;

  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p2_d16" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "61472" *) 
  (* RTL_RAM_NAME = "ram" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "2047" *) 
  (* bram_slice_begin = "0" *) 
  (* bram_slice_end = "17" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "2047" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "17" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(18),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(18),
    .WRITE_WIDTH_B(0)) 
    ram_reg_0
       (.ADDRARDADDR({1'b1,ADDRARDADDR,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CASCADEINA(1'b1),
        .CASCADEINB(1'b0),
        .CASCADEOUTA(NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED),
        .CASCADEOUTB(NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED),
        .CLKARDCLK(m00_axis_aclk),
        .CLKBWRCLK(1'b0),
        .DBITERR(NLW_ram_reg_0_DBITERR_UNCONNECTED),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,Q[15:0]}),
        .DIBDI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0,Q[17:16]}),
        .DIPBDIP({1'b1,1'b1,1'b1,1'b1}),
        .DOADO({NLW_ram_reg_0_DOADO_UNCONNECTED[31:16],dout0_out[15:0]}),
        .DOBDO(NLW_ram_reg_0_DOBDO_UNCONNECTED[31:0]),
        .DOPADOP({NLW_ram_reg_0_DOPADOP_UNCONNECTED[3:2],dout0_out[17:16]}),
        .DOPBDOP(NLW_ram_reg_0_DOPBDOP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_ram_reg_0_ECCPARITY_UNCONNECTED[7:0]),
        .ENARDEN(en),
        .ENBWREN(1'b0),
        .INJECTDBITERR(NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED),
        .INJECTSBITERR(NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED),
        .RDADDRECC(NLW_ram_reg_0_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_ram_reg_0_SBITERR_UNCONNECTED),
        .WEA({ram_reg_0_i_1__13_n_0,ram_reg_0_i_1__13_n_0,ram_reg_0_i_1__13_n_0,ram_reg_0_i_1__13_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
  LUT3 #(
    .INIT(8'h80)) 
    ram_reg_0_i_1__13
       (.I0(en),
        .I1(WE_r),
        .I2(square_r),
        .O(ram_reg_0_i_1__13_n_0));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d14" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "61472" *) 
  (* RTL_RAM_NAME = "ram" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "2047" *) 
  (* bram_slice_begin = "18" *) 
  (* bram_slice_end = "31" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "2047" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "18" *) 
  (* ram_slice_end = "31" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(18),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(18),
    .WRITE_WIDTH_B(0)) 
    ram_reg_1
       (.ADDRARDADDR({1'b1,addr,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CASCADEINA(1'b1),
        .CASCADEINB(1'b0),
        .CASCADEOUTA(NLW_ram_reg_1_CASCADEOUTA_UNCONNECTED),
        .CASCADEOUTB(NLW_ram_reg_1_CASCADEOUTB_UNCONNECTED),
        .CLKARDCLK(m00_axis_aclk),
        .CLKBWRCLK(1'b0),
        .DBITERR(NLW_ram_reg_1_DBITERR_UNCONNECTED),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,Q[31:18]}),
        .DIBDI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0,1'b0,1'b0}),
        .DIPBDIP({1'b1,1'b1,1'b1,1'b1}),
        .DOADO({NLW_ram_reg_1_DOADO_UNCONNECTED[31:14],dout0_out[31:18]}),
        .DOBDO(NLW_ram_reg_1_DOBDO_UNCONNECTED[31:0]),
        .DOPADOP(NLW_ram_reg_1_DOPADOP_UNCONNECTED[3:0]),
        .DOPBDOP(NLW_ram_reg_1_DOPBDOP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_ram_reg_1_ECCPARITY_UNCONNECTED[7:0]),
        .ENARDEN(en),
        .ENBWREN(1'b0),
        .INJECTDBITERR(NLW_ram_reg_1_INJECTDBITERR_UNCONNECTED),
        .INJECTSBITERR(NLW_ram_reg_1_INJECTSBITERR_UNCONNECTED),
        .RDADDRECC(NLW_ram_reg_1_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_ram_reg_1_SBITERR_UNCONNECTED),
        .WEA({ram_reg_0_i_1__13_n_0,ram_reg_0_i_1__13_n_0,ram_reg_0_i_1__13_n_0,ram_reg_0_i_1__13_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
endmodule

(* ORIG_REF_NAME = "rams_sp_rf_rst" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_14
   (D,
    ram_reg_1_0,
    ram_reg_1_1,
    ram_reg_1_2,
    ram_reg_1_3,
    ram_reg_0_0,
    ram_reg_0_1,
    ram_reg_0_2,
    ram_reg_0_3,
    ram_reg_0_4,
    ram_reg_0_5,
    s00_axis_tuser,
    \m00_axis_tdata_r_reg[30] ,
    \m00_axis_tdata_r_reg[30]_0 ,
    \m00_axis_tdata_r_reg[29] ,
    \m00_axis_tdata_r_reg[29]_0 ,
    \m00_axis_tdata_r_reg[28] ,
    \m00_axis_tdata_r_reg[28]_0 ,
    \m00_axis_tdata_r_reg[26] ,
    \m00_axis_tdata_r_reg[26]_0 ,
    \m00_axis_tdata_r_reg[24] ,
    \m00_axis_tdata_r_reg[24]_0 ,
    \m00_axis_tdata_r_reg[23] ,
    \m00_axis_tdata_r_reg[23]_0 ,
    \m00_axis_tdata_r_reg[22] ,
    \m00_axis_tdata_r_reg[22]_0 ,
    \m00_axis_tdata_r_reg[21] ,
    \m00_axis_tdata_r_reg[21]_0 ,
    \m00_axis_tdata_r_reg[19] ,
    \m00_axis_tdata_r_reg[19]_0 ,
    \m00_axis_tdata_r_reg[18] ,
    \m00_axis_tdata_r_reg[18]_0 ,
    \m00_axis_tdata_r_reg[17] ,
    \m00_axis_tdata_r_reg[17]_0 ,
    \m00_axis_tdata_r_reg[14] ,
    \m00_axis_tdata_r_reg[14]_0 ,
    \m00_axis_tdata_r_reg[13] ,
    \m00_axis_tdata_r_reg[13]_0 ,
    \m00_axis_tdata_r_reg[12] ,
    \m00_axis_tdata_r_reg[12]_0 ,
    \m00_axis_tdata_r_reg[10] ,
    \m00_axis_tdata_r_reg[10]_0 ,
    \m00_axis_tdata_r_reg[8] ,
    \m00_axis_tdata_r_reg[8]_0 ,
    \m00_axis_tdata_r_reg[7] ,
    \m00_axis_tdata_r_reg[7]_0 ,
    \m00_axis_tdata_r_reg[6] ,
    \m00_axis_tdata_r_reg[6]_0 ,
    \m00_axis_tdata_r_reg[5] ,
    \m00_axis_tdata_r_reg[5]_0 ,
    \m00_axis_tdata_r_reg[3] ,
    \m00_axis_tdata_r_reg[3]_0 ,
    \m00_axis_tdata_r_reg[2] ,
    \m00_axis_tdata_r_reg[2]_0 ,
    \m00_axis_tdata_r_reg[1] ,
    \m00_axis_tdata_r_reg[1]_0 ,
    dout0_out,
    \m00_axis_tdata_r_reg[31] ,
    \m00_axis_tdata_r_reg[31]_0 ,
    m00_axis_aclk,
    en,
    ADDRARDADDR,
    Q,
    addr,
    WE_r,
    square_r);
  output [21:0]D;
  output ram_reg_1_0;
  output ram_reg_1_1;
  output ram_reg_1_2;
  output ram_reg_1_3;
  output ram_reg_0_0;
  output ram_reg_0_1;
  output ram_reg_0_2;
  output ram_reg_0_3;
  output ram_reg_0_4;
  output ram_reg_0_5;
  input [3:0]s00_axis_tuser;
  input \m00_axis_tdata_r_reg[30] ;
  input \m00_axis_tdata_r_reg[30]_0 ;
  input \m00_axis_tdata_r_reg[29] ;
  input \m00_axis_tdata_r_reg[29]_0 ;
  input \m00_axis_tdata_r_reg[28] ;
  input \m00_axis_tdata_r_reg[28]_0 ;
  input \m00_axis_tdata_r_reg[26] ;
  input \m00_axis_tdata_r_reg[26]_0 ;
  input \m00_axis_tdata_r_reg[24] ;
  input \m00_axis_tdata_r_reg[24]_0 ;
  input \m00_axis_tdata_r_reg[23] ;
  input \m00_axis_tdata_r_reg[23]_0 ;
  input \m00_axis_tdata_r_reg[22] ;
  input \m00_axis_tdata_r_reg[22]_0 ;
  input \m00_axis_tdata_r_reg[21] ;
  input \m00_axis_tdata_r_reg[21]_0 ;
  input \m00_axis_tdata_r_reg[19] ;
  input \m00_axis_tdata_r_reg[19]_0 ;
  input \m00_axis_tdata_r_reg[18] ;
  input \m00_axis_tdata_r_reg[18]_0 ;
  input \m00_axis_tdata_r_reg[17] ;
  input \m00_axis_tdata_r_reg[17]_0 ;
  input \m00_axis_tdata_r_reg[14] ;
  input \m00_axis_tdata_r_reg[14]_0 ;
  input \m00_axis_tdata_r_reg[13] ;
  input \m00_axis_tdata_r_reg[13]_0 ;
  input \m00_axis_tdata_r_reg[12] ;
  input \m00_axis_tdata_r_reg[12]_0 ;
  input \m00_axis_tdata_r_reg[10] ;
  input \m00_axis_tdata_r_reg[10]_0 ;
  input \m00_axis_tdata_r_reg[8] ;
  input \m00_axis_tdata_r_reg[8]_0 ;
  input \m00_axis_tdata_r_reg[7] ;
  input \m00_axis_tdata_r_reg[7]_0 ;
  input \m00_axis_tdata_r_reg[6] ;
  input \m00_axis_tdata_r_reg[6]_0 ;
  input \m00_axis_tdata_r_reg[5] ;
  input \m00_axis_tdata_r_reg[5]_0 ;
  input \m00_axis_tdata_r_reg[3] ;
  input \m00_axis_tdata_r_reg[3]_0 ;
  input \m00_axis_tdata_r_reg[2] ;
  input \m00_axis_tdata_r_reg[2]_0 ;
  input \m00_axis_tdata_r_reg[1] ;
  input \m00_axis_tdata_r_reg[1]_0 ;
  input [31:0]dout0_out;
  input [31:0]\m00_axis_tdata_r_reg[31] ;
  input [31:0]\m00_axis_tdata_r_reg[31]_0 ;
  input m00_axis_aclk;
  input en;
  input [10:0]ADDRARDADDR;
  input [31:0]Q;
  input [10:0]addr;
  input WE_r;
  input square_r;

  wire [10:0]ADDRARDADDR;
  wire [21:0]D;
  wire [31:0]DOUTF_w;
  wire [31:0]Q;
  wire WE_r;
  wire [10:0]addr;
  wire [31:0]dout0_out;
  wire en;
  wire m00_axis_aclk;
  wire \m00_axis_tdata_r[10]_i_2_n_0 ;
  wire \m00_axis_tdata_r[12]_i_2_n_0 ;
  wire \m00_axis_tdata_r[13]_i_2_n_0 ;
  wire \m00_axis_tdata_r[14]_i_2_n_0 ;
  wire \m00_axis_tdata_r[17]_i_2_n_0 ;
  wire \m00_axis_tdata_r[18]_i_2_n_0 ;
  wire \m00_axis_tdata_r[19]_i_2_n_0 ;
  wire \m00_axis_tdata_r[1]_i_2_n_0 ;
  wire \m00_axis_tdata_r[21]_i_2_n_0 ;
  wire \m00_axis_tdata_r[22]_i_2_n_0 ;
  wire \m00_axis_tdata_r[23]_i_2_n_0 ;
  wire \m00_axis_tdata_r[24]_i_2_n_0 ;
  wire \m00_axis_tdata_r[26]_i_2_n_0 ;
  wire \m00_axis_tdata_r[28]_i_2_n_0 ;
  wire \m00_axis_tdata_r[29]_i_2_n_0 ;
  wire \m00_axis_tdata_r[2]_i_2_n_0 ;
  wire \m00_axis_tdata_r[30]_i_2_n_0 ;
  wire \m00_axis_tdata_r[3]_i_2_n_0 ;
  wire \m00_axis_tdata_r[5]_i_2_n_0 ;
  wire \m00_axis_tdata_r[6]_i_2_n_0 ;
  wire \m00_axis_tdata_r[7]_i_2_n_0 ;
  wire \m00_axis_tdata_r[8]_i_2_n_0 ;
  wire \m00_axis_tdata_r_reg[10] ;
  wire \m00_axis_tdata_r_reg[10]_0 ;
  wire \m00_axis_tdata_r_reg[12] ;
  wire \m00_axis_tdata_r_reg[12]_0 ;
  wire \m00_axis_tdata_r_reg[13] ;
  wire \m00_axis_tdata_r_reg[13]_0 ;
  wire \m00_axis_tdata_r_reg[14] ;
  wire \m00_axis_tdata_r_reg[14]_0 ;
  wire \m00_axis_tdata_r_reg[17] ;
  wire \m00_axis_tdata_r_reg[17]_0 ;
  wire \m00_axis_tdata_r_reg[18] ;
  wire \m00_axis_tdata_r_reg[18]_0 ;
  wire \m00_axis_tdata_r_reg[19] ;
  wire \m00_axis_tdata_r_reg[19]_0 ;
  wire \m00_axis_tdata_r_reg[1] ;
  wire \m00_axis_tdata_r_reg[1]_0 ;
  wire \m00_axis_tdata_r_reg[21] ;
  wire \m00_axis_tdata_r_reg[21]_0 ;
  wire \m00_axis_tdata_r_reg[22] ;
  wire \m00_axis_tdata_r_reg[22]_0 ;
  wire \m00_axis_tdata_r_reg[23] ;
  wire \m00_axis_tdata_r_reg[23]_0 ;
  wire \m00_axis_tdata_r_reg[24] ;
  wire \m00_axis_tdata_r_reg[24]_0 ;
  wire \m00_axis_tdata_r_reg[26] ;
  wire \m00_axis_tdata_r_reg[26]_0 ;
  wire \m00_axis_tdata_r_reg[28] ;
  wire \m00_axis_tdata_r_reg[28]_0 ;
  wire \m00_axis_tdata_r_reg[29] ;
  wire \m00_axis_tdata_r_reg[29]_0 ;
  wire \m00_axis_tdata_r_reg[2] ;
  wire \m00_axis_tdata_r_reg[2]_0 ;
  wire \m00_axis_tdata_r_reg[30] ;
  wire \m00_axis_tdata_r_reg[30]_0 ;
  wire [31:0]\m00_axis_tdata_r_reg[31] ;
  wire [31:0]\m00_axis_tdata_r_reg[31]_0 ;
  wire \m00_axis_tdata_r_reg[3] ;
  wire \m00_axis_tdata_r_reg[3]_0 ;
  wire \m00_axis_tdata_r_reg[5] ;
  wire \m00_axis_tdata_r_reg[5]_0 ;
  wire \m00_axis_tdata_r_reg[6] ;
  wire \m00_axis_tdata_r_reg[6]_0 ;
  wire \m00_axis_tdata_r_reg[7] ;
  wire \m00_axis_tdata_r_reg[7]_0 ;
  wire \m00_axis_tdata_r_reg[8] ;
  wire \m00_axis_tdata_r_reg[8]_0 ;
  wire ram_reg_0_0;
  wire ram_reg_0_1;
  wire ram_reg_0_2;
  wire ram_reg_0_3;
  wire ram_reg_0_4;
  wire ram_reg_0_5;
  wire ram_reg_0_i_1__14_n_0;
  wire ram_reg_1_0;
  wire ram_reg_1_1;
  wire ram_reg_1_2;
  wire ram_reg_1_3;
  wire [3:0]s00_axis_tuser;
  wire square_r;
  wire NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED;
  wire NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED;
  wire NLW_ram_reg_0_DBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_SBITERR_UNCONNECTED;
  wire [31:16]NLW_ram_reg_0_DOADO_UNCONNECTED;
  wire [31:0]NLW_ram_reg_0_DOBDO_UNCONNECTED;
  wire [3:2]NLW_ram_reg_0_DOPADOP_UNCONNECTED;
  wire [3:0]NLW_ram_reg_0_DOPBDOP_UNCONNECTED;
  wire [7:0]NLW_ram_reg_0_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_ram_reg_0_RDADDRECC_UNCONNECTED;
  wire NLW_ram_reg_1_CASCADEOUTA_UNCONNECTED;
  wire NLW_ram_reg_1_CASCADEOUTB_UNCONNECTED;
  wire NLW_ram_reg_1_DBITERR_UNCONNECTED;
  wire NLW_ram_reg_1_INJECTDBITERR_UNCONNECTED;
  wire NLW_ram_reg_1_INJECTSBITERR_UNCONNECTED;
  wire NLW_ram_reg_1_SBITERR_UNCONNECTED;
  wire [31:14]NLW_ram_reg_1_DOADO_UNCONNECTED;
  wire [31:0]NLW_ram_reg_1_DOBDO_UNCONNECTED;
  wire [3:0]NLW_ram_reg_1_DOPADOP_UNCONNECTED;
  wire [3:0]NLW_ram_reg_1_DOPBDOP_UNCONNECTED;
  wire [7:0]NLW_ram_reg_1_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_ram_reg_1_RDADDRECC_UNCONNECTED;

  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata_r[0]_i_6 
       (.I0(DOUTF_w[0]),
        .I1(dout0_out[0]),
        .I2(s00_axis_tuser[1]),
        .I3(\m00_axis_tdata_r_reg[31] [0]),
        .I4(s00_axis_tuser[0]),
        .I5(\m00_axis_tdata_r_reg[31]_0 [0]),
        .O(ram_reg_0_5));
  LUT5 #(
    .INIT(32'hDFD58A80)) 
    \m00_axis_tdata_r[10]_i_1 
       (.I0(s00_axis_tuser[3]),
        .I1(\m00_axis_tdata_r[10]_i_2_n_0 ),
        .I2(s00_axis_tuser[2]),
        .I3(\m00_axis_tdata_r_reg[10] ),
        .I4(\m00_axis_tdata_r_reg[10]_0 ),
        .O(D[7]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata_r[10]_i_2 
       (.I0(DOUTF_w[10]),
        .I1(dout0_out[10]),
        .I2(s00_axis_tuser[1]),
        .I3(\m00_axis_tdata_r_reg[31] [10]),
        .I4(s00_axis_tuser[0]),
        .I5(\m00_axis_tdata_r_reg[31]_0 [10]),
        .O(\m00_axis_tdata_r[10]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata_r[11]_i_6 
       (.I0(DOUTF_w[11]),
        .I1(dout0_out[11]),
        .I2(s00_axis_tuser[1]),
        .I3(\m00_axis_tdata_r_reg[31] [11]),
        .I4(s00_axis_tuser[0]),
        .I5(\m00_axis_tdata_r_reg[31]_0 [11]),
        .O(ram_reg_0_2));
  LUT5 #(
    .INIT(32'hDFD58A80)) 
    \m00_axis_tdata_r[12]_i_1 
       (.I0(s00_axis_tuser[3]),
        .I1(\m00_axis_tdata_r[12]_i_2_n_0 ),
        .I2(s00_axis_tuser[2]),
        .I3(\m00_axis_tdata_r_reg[12] ),
        .I4(\m00_axis_tdata_r_reg[12]_0 ),
        .O(D[8]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata_r[12]_i_2 
       (.I0(DOUTF_w[12]),
        .I1(dout0_out[12]),
        .I2(s00_axis_tuser[1]),
        .I3(\m00_axis_tdata_r_reg[31] [12]),
        .I4(s00_axis_tuser[0]),
        .I5(\m00_axis_tdata_r_reg[31]_0 [12]),
        .O(\m00_axis_tdata_r[12]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hDFD58A80)) 
    \m00_axis_tdata_r[13]_i_1 
       (.I0(s00_axis_tuser[3]),
        .I1(\m00_axis_tdata_r[13]_i_2_n_0 ),
        .I2(s00_axis_tuser[2]),
        .I3(\m00_axis_tdata_r_reg[13] ),
        .I4(\m00_axis_tdata_r_reg[13]_0 ),
        .O(D[9]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata_r[13]_i_2 
       (.I0(DOUTF_w[13]),
        .I1(dout0_out[13]),
        .I2(s00_axis_tuser[1]),
        .I3(\m00_axis_tdata_r_reg[31] [13]),
        .I4(s00_axis_tuser[0]),
        .I5(\m00_axis_tdata_r_reg[31]_0 [13]),
        .O(\m00_axis_tdata_r[13]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hDFD58A80)) 
    \m00_axis_tdata_r[14]_i_1 
       (.I0(s00_axis_tuser[3]),
        .I1(\m00_axis_tdata_r[14]_i_2_n_0 ),
        .I2(s00_axis_tuser[2]),
        .I3(\m00_axis_tdata_r_reg[14] ),
        .I4(\m00_axis_tdata_r_reg[14]_0 ),
        .O(D[10]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata_r[14]_i_2 
       (.I0(DOUTF_w[14]),
        .I1(dout0_out[14]),
        .I2(s00_axis_tuser[1]),
        .I3(\m00_axis_tdata_r_reg[31] [14]),
        .I4(s00_axis_tuser[0]),
        .I5(\m00_axis_tdata_r_reg[31]_0 [14]),
        .O(\m00_axis_tdata_r[14]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata_r[15]_i_6 
       (.I0(DOUTF_w[15]),
        .I1(dout0_out[15]),
        .I2(s00_axis_tuser[1]),
        .I3(\m00_axis_tdata_r_reg[31] [15]),
        .I4(s00_axis_tuser[0]),
        .I5(\m00_axis_tdata_r_reg[31]_0 [15]),
        .O(ram_reg_0_1));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata_r[16]_i_6 
       (.I0(DOUTF_w[16]),
        .I1(dout0_out[16]),
        .I2(s00_axis_tuser[1]),
        .I3(\m00_axis_tdata_r_reg[31] [16]),
        .I4(s00_axis_tuser[0]),
        .I5(\m00_axis_tdata_r_reg[31]_0 [16]),
        .O(ram_reg_0_0));
  LUT5 #(
    .INIT(32'hDFD58A80)) 
    \m00_axis_tdata_r[17]_i_1 
       (.I0(s00_axis_tuser[3]),
        .I1(\m00_axis_tdata_r[17]_i_2_n_0 ),
        .I2(s00_axis_tuser[2]),
        .I3(\m00_axis_tdata_r_reg[17] ),
        .I4(\m00_axis_tdata_r_reg[17]_0 ),
        .O(D[11]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata_r[17]_i_2 
       (.I0(DOUTF_w[17]),
        .I1(dout0_out[17]),
        .I2(s00_axis_tuser[1]),
        .I3(\m00_axis_tdata_r_reg[31] [17]),
        .I4(s00_axis_tuser[0]),
        .I5(\m00_axis_tdata_r_reg[31]_0 [17]),
        .O(\m00_axis_tdata_r[17]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hDFD58A80)) 
    \m00_axis_tdata_r[18]_i_1 
       (.I0(s00_axis_tuser[3]),
        .I1(\m00_axis_tdata_r[18]_i_2_n_0 ),
        .I2(s00_axis_tuser[2]),
        .I3(\m00_axis_tdata_r_reg[18] ),
        .I4(\m00_axis_tdata_r_reg[18]_0 ),
        .O(D[12]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata_r[18]_i_2 
       (.I0(DOUTF_w[18]),
        .I1(dout0_out[18]),
        .I2(s00_axis_tuser[1]),
        .I3(\m00_axis_tdata_r_reg[31] [18]),
        .I4(s00_axis_tuser[0]),
        .I5(\m00_axis_tdata_r_reg[31]_0 [18]),
        .O(\m00_axis_tdata_r[18]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hDFD58A80)) 
    \m00_axis_tdata_r[19]_i_1 
       (.I0(s00_axis_tuser[3]),
        .I1(\m00_axis_tdata_r[19]_i_2_n_0 ),
        .I2(s00_axis_tuser[2]),
        .I3(\m00_axis_tdata_r_reg[19] ),
        .I4(\m00_axis_tdata_r_reg[19]_0 ),
        .O(D[13]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata_r[19]_i_2 
       (.I0(DOUTF_w[19]),
        .I1(dout0_out[19]),
        .I2(s00_axis_tuser[1]),
        .I3(\m00_axis_tdata_r_reg[31] [19]),
        .I4(s00_axis_tuser[0]),
        .I5(\m00_axis_tdata_r_reg[31]_0 [19]),
        .O(\m00_axis_tdata_r[19]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hDFD58A80)) 
    \m00_axis_tdata_r[1]_i_1 
       (.I0(s00_axis_tuser[3]),
        .I1(\m00_axis_tdata_r[1]_i_2_n_0 ),
        .I2(s00_axis_tuser[2]),
        .I3(\m00_axis_tdata_r_reg[1] ),
        .I4(\m00_axis_tdata_r_reg[1]_0 ),
        .O(D[0]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata_r[1]_i_2 
       (.I0(DOUTF_w[1]),
        .I1(dout0_out[1]),
        .I2(s00_axis_tuser[1]),
        .I3(\m00_axis_tdata_r_reg[31] [1]),
        .I4(s00_axis_tuser[0]),
        .I5(\m00_axis_tdata_r_reg[31]_0 [1]),
        .O(\m00_axis_tdata_r[1]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata_r[20]_i_6 
       (.I0(DOUTF_w[20]),
        .I1(dout0_out[20]),
        .I2(s00_axis_tuser[1]),
        .I3(\m00_axis_tdata_r_reg[31] [20]),
        .I4(s00_axis_tuser[0]),
        .I5(\m00_axis_tdata_r_reg[31]_0 [20]),
        .O(ram_reg_1_3));
  LUT5 #(
    .INIT(32'hDFD58A80)) 
    \m00_axis_tdata_r[21]_i_1 
       (.I0(s00_axis_tuser[3]),
        .I1(\m00_axis_tdata_r[21]_i_2_n_0 ),
        .I2(s00_axis_tuser[2]),
        .I3(\m00_axis_tdata_r_reg[21] ),
        .I4(\m00_axis_tdata_r_reg[21]_0 ),
        .O(D[14]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata_r[21]_i_2 
       (.I0(DOUTF_w[21]),
        .I1(dout0_out[21]),
        .I2(s00_axis_tuser[1]),
        .I3(\m00_axis_tdata_r_reg[31] [21]),
        .I4(s00_axis_tuser[0]),
        .I5(\m00_axis_tdata_r_reg[31]_0 [21]),
        .O(\m00_axis_tdata_r[21]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hDFD58A80)) 
    \m00_axis_tdata_r[22]_i_1 
       (.I0(s00_axis_tuser[3]),
        .I1(\m00_axis_tdata_r[22]_i_2_n_0 ),
        .I2(s00_axis_tuser[2]),
        .I3(\m00_axis_tdata_r_reg[22] ),
        .I4(\m00_axis_tdata_r_reg[22]_0 ),
        .O(D[15]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata_r[22]_i_2 
       (.I0(DOUTF_w[22]),
        .I1(dout0_out[22]),
        .I2(s00_axis_tuser[1]),
        .I3(\m00_axis_tdata_r_reg[31] [22]),
        .I4(s00_axis_tuser[0]),
        .I5(\m00_axis_tdata_r_reg[31]_0 [22]),
        .O(\m00_axis_tdata_r[22]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hDFD58A80)) 
    \m00_axis_tdata_r[23]_i_1 
       (.I0(s00_axis_tuser[3]),
        .I1(\m00_axis_tdata_r[23]_i_2_n_0 ),
        .I2(s00_axis_tuser[2]),
        .I3(\m00_axis_tdata_r_reg[23] ),
        .I4(\m00_axis_tdata_r_reg[23]_0 ),
        .O(D[16]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata_r[23]_i_2 
       (.I0(DOUTF_w[23]),
        .I1(dout0_out[23]),
        .I2(s00_axis_tuser[1]),
        .I3(\m00_axis_tdata_r_reg[31] [23]),
        .I4(s00_axis_tuser[0]),
        .I5(\m00_axis_tdata_r_reg[31]_0 [23]),
        .O(\m00_axis_tdata_r[23]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hDFD58A80)) 
    \m00_axis_tdata_r[24]_i_1 
       (.I0(s00_axis_tuser[3]),
        .I1(\m00_axis_tdata_r[24]_i_2_n_0 ),
        .I2(s00_axis_tuser[2]),
        .I3(\m00_axis_tdata_r_reg[24] ),
        .I4(\m00_axis_tdata_r_reg[24]_0 ),
        .O(D[17]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata_r[24]_i_2 
       (.I0(DOUTF_w[24]),
        .I1(dout0_out[24]),
        .I2(s00_axis_tuser[1]),
        .I3(\m00_axis_tdata_r_reg[31] [24]),
        .I4(s00_axis_tuser[0]),
        .I5(\m00_axis_tdata_r_reg[31]_0 [24]),
        .O(\m00_axis_tdata_r[24]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata_r[25]_i_6 
       (.I0(DOUTF_w[25]),
        .I1(dout0_out[25]),
        .I2(s00_axis_tuser[1]),
        .I3(\m00_axis_tdata_r_reg[31] [25]),
        .I4(s00_axis_tuser[0]),
        .I5(\m00_axis_tdata_r_reg[31]_0 [25]),
        .O(ram_reg_1_2));
  LUT5 #(
    .INIT(32'hDFD58A80)) 
    \m00_axis_tdata_r[26]_i_1 
       (.I0(s00_axis_tuser[3]),
        .I1(\m00_axis_tdata_r[26]_i_2_n_0 ),
        .I2(s00_axis_tuser[2]),
        .I3(\m00_axis_tdata_r_reg[26] ),
        .I4(\m00_axis_tdata_r_reg[26]_0 ),
        .O(D[18]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata_r[26]_i_2 
       (.I0(DOUTF_w[26]),
        .I1(dout0_out[26]),
        .I2(s00_axis_tuser[1]),
        .I3(\m00_axis_tdata_r_reg[31] [26]),
        .I4(s00_axis_tuser[0]),
        .I5(\m00_axis_tdata_r_reg[31]_0 [26]),
        .O(\m00_axis_tdata_r[26]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata_r[27]_i_6 
       (.I0(DOUTF_w[27]),
        .I1(dout0_out[27]),
        .I2(s00_axis_tuser[1]),
        .I3(\m00_axis_tdata_r_reg[31] [27]),
        .I4(s00_axis_tuser[0]),
        .I5(\m00_axis_tdata_r_reg[31]_0 [27]),
        .O(ram_reg_1_1));
  LUT5 #(
    .INIT(32'hDFD58A80)) 
    \m00_axis_tdata_r[28]_i_1 
       (.I0(s00_axis_tuser[3]),
        .I1(\m00_axis_tdata_r[28]_i_2_n_0 ),
        .I2(s00_axis_tuser[2]),
        .I3(\m00_axis_tdata_r_reg[28] ),
        .I4(\m00_axis_tdata_r_reg[28]_0 ),
        .O(D[19]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata_r[28]_i_2 
       (.I0(DOUTF_w[28]),
        .I1(dout0_out[28]),
        .I2(s00_axis_tuser[1]),
        .I3(\m00_axis_tdata_r_reg[31] [28]),
        .I4(s00_axis_tuser[0]),
        .I5(\m00_axis_tdata_r_reg[31]_0 [28]),
        .O(\m00_axis_tdata_r[28]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hDFD58A80)) 
    \m00_axis_tdata_r[29]_i_1 
       (.I0(s00_axis_tuser[3]),
        .I1(\m00_axis_tdata_r[29]_i_2_n_0 ),
        .I2(s00_axis_tuser[2]),
        .I3(\m00_axis_tdata_r_reg[29] ),
        .I4(\m00_axis_tdata_r_reg[29]_0 ),
        .O(D[20]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata_r[29]_i_2 
       (.I0(DOUTF_w[29]),
        .I1(dout0_out[29]),
        .I2(s00_axis_tuser[1]),
        .I3(\m00_axis_tdata_r_reg[31] [29]),
        .I4(s00_axis_tuser[0]),
        .I5(\m00_axis_tdata_r_reg[31]_0 [29]),
        .O(\m00_axis_tdata_r[29]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hDFD58A80)) 
    \m00_axis_tdata_r[2]_i_1 
       (.I0(s00_axis_tuser[3]),
        .I1(\m00_axis_tdata_r[2]_i_2_n_0 ),
        .I2(s00_axis_tuser[2]),
        .I3(\m00_axis_tdata_r_reg[2] ),
        .I4(\m00_axis_tdata_r_reg[2]_0 ),
        .O(D[1]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata_r[2]_i_2 
       (.I0(DOUTF_w[2]),
        .I1(dout0_out[2]),
        .I2(s00_axis_tuser[1]),
        .I3(\m00_axis_tdata_r_reg[31] [2]),
        .I4(s00_axis_tuser[0]),
        .I5(\m00_axis_tdata_r_reg[31]_0 [2]),
        .O(\m00_axis_tdata_r[2]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hDFD58A80)) 
    \m00_axis_tdata_r[30]_i_1 
       (.I0(s00_axis_tuser[3]),
        .I1(\m00_axis_tdata_r[30]_i_2_n_0 ),
        .I2(s00_axis_tuser[2]),
        .I3(\m00_axis_tdata_r_reg[30] ),
        .I4(\m00_axis_tdata_r_reg[30]_0 ),
        .O(D[21]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata_r[30]_i_2 
       (.I0(DOUTF_w[30]),
        .I1(dout0_out[30]),
        .I2(s00_axis_tuser[1]),
        .I3(\m00_axis_tdata_r_reg[31] [30]),
        .I4(s00_axis_tuser[0]),
        .I5(\m00_axis_tdata_r_reg[31]_0 [30]),
        .O(\m00_axis_tdata_r[30]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata_r[31]_i_7 
       (.I0(DOUTF_w[31]),
        .I1(dout0_out[31]),
        .I2(s00_axis_tuser[1]),
        .I3(\m00_axis_tdata_r_reg[31] [31]),
        .I4(s00_axis_tuser[0]),
        .I5(\m00_axis_tdata_r_reg[31]_0 [31]),
        .O(ram_reg_1_0));
  LUT5 #(
    .INIT(32'hDFD58A80)) 
    \m00_axis_tdata_r[3]_i_1 
       (.I0(s00_axis_tuser[3]),
        .I1(\m00_axis_tdata_r[3]_i_2_n_0 ),
        .I2(s00_axis_tuser[2]),
        .I3(\m00_axis_tdata_r_reg[3] ),
        .I4(\m00_axis_tdata_r_reg[3]_0 ),
        .O(D[2]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata_r[3]_i_2 
       (.I0(DOUTF_w[3]),
        .I1(dout0_out[3]),
        .I2(s00_axis_tuser[1]),
        .I3(\m00_axis_tdata_r_reg[31] [3]),
        .I4(s00_axis_tuser[0]),
        .I5(\m00_axis_tdata_r_reg[31]_0 [3]),
        .O(\m00_axis_tdata_r[3]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata_r[4]_i_6 
       (.I0(DOUTF_w[4]),
        .I1(dout0_out[4]),
        .I2(s00_axis_tuser[1]),
        .I3(\m00_axis_tdata_r_reg[31] [4]),
        .I4(s00_axis_tuser[0]),
        .I5(\m00_axis_tdata_r_reg[31]_0 [4]),
        .O(ram_reg_0_4));
  LUT5 #(
    .INIT(32'hDFD58A80)) 
    \m00_axis_tdata_r[5]_i_1 
       (.I0(s00_axis_tuser[3]),
        .I1(\m00_axis_tdata_r[5]_i_2_n_0 ),
        .I2(s00_axis_tuser[2]),
        .I3(\m00_axis_tdata_r_reg[5] ),
        .I4(\m00_axis_tdata_r_reg[5]_0 ),
        .O(D[3]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata_r[5]_i_2 
       (.I0(DOUTF_w[5]),
        .I1(dout0_out[5]),
        .I2(s00_axis_tuser[1]),
        .I3(\m00_axis_tdata_r_reg[31] [5]),
        .I4(s00_axis_tuser[0]),
        .I5(\m00_axis_tdata_r_reg[31]_0 [5]),
        .O(\m00_axis_tdata_r[5]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hDFD58A80)) 
    \m00_axis_tdata_r[6]_i_1 
       (.I0(s00_axis_tuser[3]),
        .I1(\m00_axis_tdata_r[6]_i_2_n_0 ),
        .I2(s00_axis_tuser[2]),
        .I3(\m00_axis_tdata_r_reg[6] ),
        .I4(\m00_axis_tdata_r_reg[6]_0 ),
        .O(D[4]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata_r[6]_i_2 
       (.I0(DOUTF_w[6]),
        .I1(dout0_out[6]),
        .I2(s00_axis_tuser[1]),
        .I3(\m00_axis_tdata_r_reg[31] [6]),
        .I4(s00_axis_tuser[0]),
        .I5(\m00_axis_tdata_r_reg[31]_0 [6]),
        .O(\m00_axis_tdata_r[6]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hDFD58A80)) 
    \m00_axis_tdata_r[7]_i_1 
       (.I0(s00_axis_tuser[3]),
        .I1(\m00_axis_tdata_r[7]_i_2_n_0 ),
        .I2(s00_axis_tuser[2]),
        .I3(\m00_axis_tdata_r_reg[7] ),
        .I4(\m00_axis_tdata_r_reg[7]_0 ),
        .O(D[5]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata_r[7]_i_2 
       (.I0(DOUTF_w[7]),
        .I1(dout0_out[7]),
        .I2(s00_axis_tuser[1]),
        .I3(\m00_axis_tdata_r_reg[31] [7]),
        .I4(s00_axis_tuser[0]),
        .I5(\m00_axis_tdata_r_reg[31]_0 [7]),
        .O(\m00_axis_tdata_r[7]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hDFD58A80)) 
    \m00_axis_tdata_r[8]_i_1 
       (.I0(s00_axis_tuser[3]),
        .I1(\m00_axis_tdata_r[8]_i_2_n_0 ),
        .I2(s00_axis_tuser[2]),
        .I3(\m00_axis_tdata_r_reg[8] ),
        .I4(\m00_axis_tdata_r_reg[8]_0 ),
        .O(D[6]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata_r[8]_i_2 
       (.I0(DOUTF_w[8]),
        .I1(dout0_out[8]),
        .I2(s00_axis_tuser[1]),
        .I3(\m00_axis_tdata_r_reg[31] [8]),
        .I4(s00_axis_tuser[0]),
        .I5(\m00_axis_tdata_r_reg[31]_0 [8]),
        .O(\m00_axis_tdata_r[8]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata_r[9]_i_6 
       (.I0(DOUTF_w[9]),
        .I1(dout0_out[9]),
        .I2(s00_axis_tuser[1]),
        .I3(\m00_axis_tdata_r_reg[31] [9]),
        .I4(s00_axis_tuser[0]),
        .I5(\m00_axis_tdata_r_reg[31]_0 [9]),
        .O(ram_reg_0_3));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p2_d16" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "61472" *) 
  (* RTL_RAM_NAME = "ram" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "2047" *) 
  (* bram_slice_begin = "0" *) 
  (* bram_slice_end = "17" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "2047" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "17" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(18),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(18),
    .WRITE_WIDTH_B(0)) 
    ram_reg_0
       (.ADDRARDADDR({1'b1,ADDRARDADDR,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CASCADEINA(1'b1),
        .CASCADEINB(1'b0),
        .CASCADEOUTA(NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED),
        .CASCADEOUTB(NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED),
        .CLKARDCLK(m00_axis_aclk),
        .CLKBWRCLK(1'b0),
        .DBITERR(NLW_ram_reg_0_DBITERR_UNCONNECTED),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,Q[15:0]}),
        .DIBDI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0,Q[17:16]}),
        .DIPBDIP({1'b1,1'b1,1'b1,1'b1}),
        .DOADO({NLW_ram_reg_0_DOADO_UNCONNECTED[31:16],DOUTF_w[15:0]}),
        .DOBDO(NLW_ram_reg_0_DOBDO_UNCONNECTED[31:0]),
        .DOPADOP({NLW_ram_reg_0_DOPADOP_UNCONNECTED[3:2],DOUTF_w[17:16]}),
        .DOPBDOP(NLW_ram_reg_0_DOPBDOP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_ram_reg_0_ECCPARITY_UNCONNECTED[7:0]),
        .ENARDEN(en),
        .ENBWREN(1'b0),
        .INJECTDBITERR(NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED),
        .INJECTSBITERR(NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED),
        .RDADDRECC(NLW_ram_reg_0_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_ram_reg_0_SBITERR_UNCONNECTED),
        .WEA({ram_reg_0_i_1__14_n_0,ram_reg_0_i_1__14_n_0,ram_reg_0_i_1__14_n_0,ram_reg_0_i_1__14_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
  LUT3 #(
    .INIT(8'h80)) 
    ram_reg_0_i_1__14
       (.I0(en),
        .I1(WE_r),
        .I2(square_r),
        .O(ram_reg_0_i_1__14_n_0));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d14" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "61472" *) 
  (* RTL_RAM_NAME = "ram" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "2047" *) 
  (* bram_slice_begin = "18" *) 
  (* bram_slice_end = "31" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "2047" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "18" *) 
  (* ram_slice_end = "31" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(18),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(18),
    .WRITE_WIDTH_B(0)) 
    ram_reg_1
       (.ADDRARDADDR({1'b1,addr,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CASCADEINA(1'b1),
        .CASCADEINB(1'b0),
        .CASCADEOUTA(NLW_ram_reg_1_CASCADEOUTA_UNCONNECTED),
        .CASCADEOUTB(NLW_ram_reg_1_CASCADEOUTB_UNCONNECTED),
        .CLKARDCLK(m00_axis_aclk),
        .CLKBWRCLK(1'b0),
        .DBITERR(NLW_ram_reg_1_DBITERR_UNCONNECTED),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,Q[31:18]}),
        .DIBDI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0,1'b0,1'b0}),
        .DIPBDIP({1'b1,1'b1,1'b1,1'b1}),
        .DOADO({NLW_ram_reg_1_DOADO_UNCONNECTED[31:14],DOUTF_w[31:18]}),
        .DOBDO(NLW_ram_reg_1_DOBDO_UNCONNECTED[31:0]),
        .DOPADOP(NLW_ram_reg_1_DOPADOP_UNCONNECTED[3:0]),
        .DOPBDOP(NLW_ram_reg_1_DOPBDOP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_ram_reg_1_ECCPARITY_UNCONNECTED[7:0]),
        .ENARDEN(en),
        .ENBWREN(1'b0),
        .INJECTDBITERR(NLW_ram_reg_1_INJECTDBITERR_UNCONNECTED),
        .INJECTSBITERR(NLW_ram_reg_1_INJECTSBITERR_UNCONNECTED),
        .RDADDRECC(NLW_ram_reg_1_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_ram_reg_1_SBITERR_UNCONNECTED),
        .WEA({ram_reg_0_i_1__14_n_0,ram_reg_0_i_1__14_n_0,ram_reg_0_i_1__14_n_0,ram_reg_0_i_1__14_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
endmodule

(* ORIG_REF_NAME = "rams_sp_rf_rst" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_2
   (D,
    \s00_axis_tuser[18] ,
    \s00_axis_tuser[18]_0 ,
    \s00_axis_tuser[18]_1 ,
    ram_reg_1_0,
    \s00_axis_tuser[18]_2 ,
    ram_reg_1_1,
    \s00_axis_tuser[18]_3 ,
    \s00_axis_tuser[18]_4 ,
    \s00_axis_tuser[18]_5 ,
    \s00_axis_tuser[18]_6 ,
    ram_reg_1_2,
    \s00_axis_tuser[18]_7 ,
    \s00_axis_tuser[18]_8 ,
    \s00_axis_tuser[18]_9 ,
    ram_reg_0_0,
    ram_reg_0_1,
    \s00_axis_tuser[18]_10 ,
    \s00_axis_tuser[18]_11 ,
    \s00_axis_tuser[18]_12 ,
    ram_reg_0_2,
    \s00_axis_tuser[18]_13 ,
    ram_reg_0_3,
    \s00_axis_tuser[18]_14 ,
    \s00_axis_tuser[18]_15 ,
    \s00_axis_tuser[18]_16 ,
    \s00_axis_tuser[18]_17 ,
    ram_reg_0_4,
    \s00_axis_tuser[18]_18 ,
    \s00_axis_tuser[18]_19 ,
    \s00_axis_tuser[18]_20 ,
    ram_reg_0_5,
    \m00_axis_tdata_r_reg[31] ,
    s00_axis_tuser,
    \m00_axis_tdata_r_reg[31]_0 ,
    \m00_axis_tdata_r_reg[31]_1 ,
    dout0_out,
    \m00_axis_tdata_r_reg[31]_2 ,
    \m00_axis_tdata_r_reg[31]_3 ,
    \m00_axis_tdata_r_reg[30] ,
    \m00_axis_tdata_r_reg[29] ,
    \m00_axis_tdata_r_reg[28] ,
    \m00_axis_tdata_r_reg[26] ,
    \m00_axis_tdata_r_reg[24] ,
    \m00_axis_tdata_r_reg[23] ,
    \m00_axis_tdata_r_reg[22] ,
    \m00_axis_tdata_r_reg[21] ,
    \m00_axis_tdata_r_reg[19] ,
    \m00_axis_tdata_r_reg[18] ,
    \m00_axis_tdata_r_reg[17] ,
    \m00_axis_tdata_r_reg[14] ,
    \m00_axis_tdata_r_reg[13] ,
    \m00_axis_tdata_r_reg[12] ,
    \m00_axis_tdata_r_reg[10] ,
    \m00_axis_tdata_r_reg[8] ,
    \m00_axis_tdata_r_reg[7] ,
    \m00_axis_tdata_r_reg[6] ,
    \m00_axis_tdata_r_reg[5] ,
    \m00_axis_tdata_r_reg[3] ,
    \m00_axis_tdata_r_reg[2] ,
    \m00_axis_tdata_r_reg[1] ,
    m00_axis_aclk,
    en,
    ADDRARDADDR,
    Q,
    addr,
    WE_r,
    square_r);
  output [0:0]D;
  output \s00_axis_tuser[18] ;
  output \s00_axis_tuser[18]_0 ;
  output \s00_axis_tuser[18]_1 ;
  output ram_reg_1_0;
  output \s00_axis_tuser[18]_2 ;
  output ram_reg_1_1;
  output \s00_axis_tuser[18]_3 ;
  output \s00_axis_tuser[18]_4 ;
  output \s00_axis_tuser[18]_5 ;
  output \s00_axis_tuser[18]_6 ;
  output ram_reg_1_2;
  output \s00_axis_tuser[18]_7 ;
  output \s00_axis_tuser[18]_8 ;
  output \s00_axis_tuser[18]_9 ;
  output ram_reg_0_0;
  output ram_reg_0_1;
  output \s00_axis_tuser[18]_10 ;
  output \s00_axis_tuser[18]_11 ;
  output \s00_axis_tuser[18]_12 ;
  output ram_reg_0_2;
  output \s00_axis_tuser[18]_13 ;
  output ram_reg_0_3;
  output \s00_axis_tuser[18]_14 ;
  output \s00_axis_tuser[18]_15 ;
  output \s00_axis_tuser[18]_16 ;
  output \s00_axis_tuser[18]_17 ;
  output ram_reg_0_4;
  output \s00_axis_tuser[18]_18 ;
  output \s00_axis_tuser[18]_19 ;
  output \s00_axis_tuser[18]_20 ;
  output ram_reg_0_5;
  input \m00_axis_tdata_r_reg[31] ;
  input [3:0]s00_axis_tuser;
  input \m00_axis_tdata_r_reg[31]_0 ;
  input \m00_axis_tdata_r_reg[31]_1 ;
  input [31:0]dout0_out;
  input [31:0]\m00_axis_tdata_r_reg[31]_2 ;
  input [31:0]\m00_axis_tdata_r_reg[31]_3 ;
  input \m00_axis_tdata_r_reg[30] ;
  input \m00_axis_tdata_r_reg[29] ;
  input \m00_axis_tdata_r_reg[28] ;
  input \m00_axis_tdata_r_reg[26] ;
  input \m00_axis_tdata_r_reg[24] ;
  input \m00_axis_tdata_r_reg[23] ;
  input \m00_axis_tdata_r_reg[22] ;
  input \m00_axis_tdata_r_reg[21] ;
  input \m00_axis_tdata_r_reg[19] ;
  input \m00_axis_tdata_r_reg[18] ;
  input \m00_axis_tdata_r_reg[17] ;
  input \m00_axis_tdata_r_reg[14] ;
  input \m00_axis_tdata_r_reg[13] ;
  input \m00_axis_tdata_r_reg[12] ;
  input \m00_axis_tdata_r_reg[10] ;
  input \m00_axis_tdata_r_reg[8] ;
  input \m00_axis_tdata_r_reg[7] ;
  input \m00_axis_tdata_r_reg[6] ;
  input \m00_axis_tdata_r_reg[5] ;
  input \m00_axis_tdata_r_reg[3] ;
  input \m00_axis_tdata_r_reg[2] ;
  input \m00_axis_tdata_r_reg[1] ;
  input m00_axis_aclk;
  input en;
  input [10:0]ADDRARDADDR;
  input [31:0]Q;
  input [10:0]addr;
  input WE_r;
  input square_r;

  wire [10:0]ADDRARDADDR;
  wire [0:0]D;
  wire [31:0]DOUT3_w;
  wire [31:0]Q;
  wire WE_r;
  wire [10:0]addr;
  wire [31:0]dout0_out;
  wire en;
  wire m00_axis_aclk;
  wire \m00_axis_tdata_r[10]_i_5_n_0 ;
  wire \m00_axis_tdata_r[12]_i_5_n_0 ;
  wire \m00_axis_tdata_r[13]_i_5_n_0 ;
  wire \m00_axis_tdata_r[14]_i_5_n_0 ;
  wire \m00_axis_tdata_r[17]_i_5_n_0 ;
  wire \m00_axis_tdata_r[18]_i_5_n_0 ;
  wire \m00_axis_tdata_r[19]_i_5_n_0 ;
  wire \m00_axis_tdata_r[1]_i_5_n_0 ;
  wire \m00_axis_tdata_r[21]_i_5_n_0 ;
  wire \m00_axis_tdata_r[22]_i_5_n_0 ;
  wire \m00_axis_tdata_r[23]_i_5_n_0 ;
  wire \m00_axis_tdata_r[24]_i_5_n_0 ;
  wire \m00_axis_tdata_r[26]_i_5_n_0 ;
  wire \m00_axis_tdata_r[28]_i_5_n_0 ;
  wire \m00_axis_tdata_r[29]_i_5_n_0 ;
  wire \m00_axis_tdata_r[2]_i_5_n_0 ;
  wire \m00_axis_tdata_r[30]_i_5_n_0 ;
  wire \m00_axis_tdata_r[31]_i_5_n_0 ;
  wire \m00_axis_tdata_r[3]_i_5_n_0 ;
  wire \m00_axis_tdata_r[5]_i_5_n_0 ;
  wire \m00_axis_tdata_r[6]_i_5_n_0 ;
  wire \m00_axis_tdata_r[7]_i_5_n_0 ;
  wire \m00_axis_tdata_r[8]_i_5_n_0 ;
  wire \m00_axis_tdata_r_reg[10] ;
  wire \m00_axis_tdata_r_reg[12] ;
  wire \m00_axis_tdata_r_reg[13] ;
  wire \m00_axis_tdata_r_reg[14] ;
  wire \m00_axis_tdata_r_reg[17] ;
  wire \m00_axis_tdata_r_reg[18] ;
  wire \m00_axis_tdata_r_reg[19] ;
  wire \m00_axis_tdata_r_reg[1] ;
  wire \m00_axis_tdata_r_reg[21] ;
  wire \m00_axis_tdata_r_reg[22] ;
  wire \m00_axis_tdata_r_reg[23] ;
  wire \m00_axis_tdata_r_reg[24] ;
  wire \m00_axis_tdata_r_reg[26] ;
  wire \m00_axis_tdata_r_reg[28] ;
  wire \m00_axis_tdata_r_reg[29] ;
  wire \m00_axis_tdata_r_reg[2] ;
  wire \m00_axis_tdata_r_reg[30] ;
  wire \m00_axis_tdata_r_reg[31] ;
  wire \m00_axis_tdata_r_reg[31]_0 ;
  wire \m00_axis_tdata_r_reg[31]_1 ;
  wire [31:0]\m00_axis_tdata_r_reg[31]_2 ;
  wire [31:0]\m00_axis_tdata_r_reg[31]_3 ;
  wire \m00_axis_tdata_r_reg[3] ;
  wire \m00_axis_tdata_r_reg[5] ;
  wire \m00_axis_tdata_r_reg[6] ;
  wire \m00_axis_tdata_r_reg[7] ;
  wire \m00_axis_tdata_r_reg[8] ;
  wire ram_reg_0_0;
  wire ram_reg_0_1;
  wire ram_reg_0_2;
  wire ram_reg_0_3;
  wire ram_reg_0_4;
  wire ram_reg_0_5;
  wire ram_reg_0_i_1__2_n_0;
  wire ram_reg_1_0;
  wire ram_reg_1_1;
  wire ram_reg_1_2;
  wire [3:0]s00_axis_tuser;
  wire \s00_axis_tuser[18] ;
  wire \s00_axis_tuser[18]_0 ;
  wire \s00_axis_tuser[18]_1 ;
  wire \s00_axis_tuser[18]_10 ;
  wire \s00_axis_tuser[18]_11 ;
  wire \s00_axis_tuser[18]_12 ;
  wire \s00_axis_tuser[18]_13 ;
  wire \s00_axis_tuser[18]_14 ;
  wire \s00_axis_tuser[18]_15 ;
  wire \s00_axis_tuser[18]_16 ;
  wire \s00_axis_tuser[18]_17 ;
  wire \s00_axis_tuser[18]_18 ;
  wire \s00_axis_tuser[18]_19 ;
  wire \s00_axis_tuser[18]_2 ;
  wire \s00_axis_tuser[18]_20 ;
  wire \s00_axis_tuser[18]_3 ;
  wire \s00_axis_tuser[18]_4 ;
  wire \s00_axis_tuser[18]_5 ;
  wire \s00_axis_tuser[18]_6 ;
  wire \s00_axis_tuser[18]_7 ;
  wire \s00_axis_tuser[18]_8 ;
  wire \s00_axis_tuser[18]_9 ;
  wire square_r;
  wire NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED;
  wire NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED;
  wire NLW_ram_reg_0_DBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_SBITERR_UNCONNECTED;
  wire [31:16]NLW_ram_reg_0_DOADO_UNCONNECTED;
  wire [31:0]NLW_ram_reg_0_DOBDO_UNCONNECTED;
  wire [3:2]NLW_ram_reg_0_DOPADOP_UNCONNECTED;
  wire [3:0]NLW_ram_reg_0_DOPBDOP_UNCONNECTED;
  wire [7:0]NLW_ram_reg_0_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_ram_reg_0_RDADDRECC_UNCONNECTED;
  wire NLW_ram_reg_1_CASCADEOUTA_UNCONNECTED;
  wire NLW_ram_reg_1_CASCADEOUTB_UNCONNECTED;
  wire NLW_ram_reg_1_DBITERR_UNCONNECTED;
  wire NLW_ram_reg_1_INJECTDBITERR_UNCONNECTED;
  wire NLW_ram_reg_1_INJECTSBITERR_UNCONNECTED;
  wire NLW_ram_reg_1_SBITERR_UNCONNECTED;
  wire [31:14]NLW_ram_reg_1_DOADO_UNCONNECTED;
  wire [31:0]NLW_ram_reg_1_DOBDO_UNCONNECTED;
  wire [3:0]NLW_ram_reg_1_DOPADOP_UNCONNECTED;
  wire [3:0]NLW_ram_reg_1_DOPBDOP_UNCONNECTED;
  wire [7:0]NLW_ram_reg_1_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_ram_reg_1_RDADDRECC_UNCONNECTED;

  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata_r[0]_i_3 
       (.I0(DOUT3_w[0]),
        .I1(dout0_out[0]),
        .I2(s00_axis_tuser[1]),
        .I3(\m00_axis_tdata_r_reg[31]_2 [0]),
        .I4(s00_axis_tuser[0]),
        .I5(\m00_axis_tdata_r_reg[31]_3 [0]),
        .O(ram_reg_0_5));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata_r[10]_i_5 
       (.I0(DOUT3_w[10]),
        .I1(dout0_out[10]),
        .I2(s00_axis_tuser[1]),
        .I3(\m00_axis_tdata_r_reg[31]_2 [10]),
        .I4(s00_axis_tuser[0]),
        .I5(\m00_axis_tdata_r_reg[31]_3 [10]),
        .O(\m00_axis_tdata_r[10]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata_r[11]_i_3 
       (.I0(DOUT3_w[11]),
        .I1(dout0_out[11]),
        .I2(s00_axis_tuser[1]),
        .I3(\m00_axis_tdata_r_reg[31]_2 [11]),
        .I4(s00_axis_tuser[0]),
        .I5(\m00_axis_tdata_r_reg[31]_3 [11]),
        .O(ram_reg_0_2));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata_r[12]_i_5 
       (.I0(DOUT3_w[12]),
        .I1(dout0_out[12]),
        .I2(s00_axis_tuser[1]),
        .I3(\m00_axis_tdata_r_reg[31]_2 [12]),
        .I4(s00_axis_tuser[0]),
        .I5(\m00_axis_tdata_r_reg[31]_3 [12]),
        .O(\m00_axis_tdata_r[12]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata_r[13]_i_5 
       (.I0(DOUT3_w[13]),
        .I1(dout0_out[13]),
        .I2(s00_axis_tuser[1]),
        .I3(\m00_axis_tdata_r_reg[31]_2 [13]),
        .I4(s00_axis_tuser[0]),
        .I5(\m00_axis_tdata_r_reg[31]_3 [13]),
        .O(\m00_axis_tdata_r[13]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata_r[14]_i_5 
       (.I0(DOUT3_w[14]),
        .I1(dout0_out[14]),
        .I2(s00_axis_tuser[1]),
        .I3(\m00_axis_tdata_r_reg[31]_2 [14]),
        .I4(s00_axis_tuser[0]),
        .I5(\m00_axis_tdata_r_reg[31]_3 [14]),
        .O(\m00_axis_tdata_r[14]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata_r[15]_i_3 
       (.I0(DOUT3_w[15]),
        .I1(dout0_out[15]),
        .I2(s00_axis_tuser[1]),
        .I3(\m00_axis_tdata_r_reg[31]_2 [15]),
        .I4(s00_axis_tuser[0]),
        .I5(\m00_axis_tdata_r_reg[31]_3 [15]),
        .O(ram_reg_0_1));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata_r[16]_i_3 
       (.I0(DOUT3_w[16]),
        .I1(dout0_out[16]),
        .I2(s00_axis_tuser[1]),
        .I3(\m00_axis_tdata_r_reg[31]_2 [16]),
        .I4(s00_axis_tuser[0]),
        .I5(\m00_axis_tdata_r_reg[31]_3 [16]),
        .O(ram_reg_0_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata_r[17]_i_5 
       (.I0(DOUT3_w[17]),
        .I1(dout0_out[17]),
        .I2(s00_axis_tuser[1]),
        .I3(\m00_axis_tdata_r_reg[31]_2 [17]),
        .I4(s00_axis_tuser[0]),
        .I5(\m00_axis_tdata_r_reg[31]_3 [17]),
        .O(\m00_axis_tdata_r[17]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata_r[18]_i_5 
       (.I0(DOUT3_w[18]),
        .I1(dout0_out[18]),
        .I2(s00_axis_tuser[1]),
        .I3(\m00_axis_tdata_r_reg[31]_2 [18]),
        .I4(s00_axis_tuser[0]),
        .I5(\m00_axis_tdata_r_reg[31]_3 [18]),
        .O(\m00_axis_tdata_r[18]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata_r[19]_i_5 
       (.I0(DOUT3_w[19]),
        .I1(dout0_out[19]),
        .I2(s00_axis_tuser[1]),
        .I3(\m00_axis_tdata_r_reg[31]_2 [19]),
        .I4(s00_axis_tuser[0]),
        .I5(\m00_axis_tdata_r_reg[31]_3 [19]),
        .O(\m00_axis_tdata_r[19]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata_r[1]_i_5 
       (.I0(DOUT3_w[1]),
        .I1(dout0_out[1]),
        .I2(s00_axis_tuser[1]),
        .I3(\m00_axis_tdata_r_reg[31]_2 [1]),
        .I4(s00_axis_tuser[0]),
        .I5(\m00_axis_tdata_r_reg[31]_3 [1]),
        .O(\m00_axis_tdata_r[1]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata_r[20]_i_3 
       (.I0(DOUT3_w[20]),
        .I1(dout0_out[20]),
        .I2(s00_axis_tuser[1]),
        .I3(\m00_axis_tdata_r_reg[31]_2 [20]),
        .I4(s00_axis_tuser[0]),
        .I5(\m00_axis_tdata_r_reg[31]_3 [20]),
        .O(ram_reg_1_2));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata_r[21]_i_5 
       (.I0(DOUT3_w[21]),
        .I1(dout0_out[21]),
        .I2(s00_axis_tuser[1]),
        .I3(\m00_axis_tdata_r_reg[31]_2 [21]),
        .I4(s00_axis_tuser[0]),
        .I5(\m00_axis_tdata_r_reg[31]_3 [21]),
        .O(\m00_axis_tdata_r[21]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata_r[22]_i_5 
       (.I0(DOUT3_w[22]),
        .I1(dout0_out[22]),
        .I2(s00_axis_tuser[1]),
        .I3(\m00_axis_tdata_r_reg[31]_2 [22]),
        .I4(s00_axis_tuser[0]),
        .I5(\m00_axis_tdata_r_reg[31]_3 [22]),
        .O(\m00_axis_tdata_r[22]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata_r[23]_i_5 
       (.I0(DOUT3_w[23]),
        .I1(dout0_out[23]),
        .I2(s00_axis_tuser[1]),
        .I3(\m00_axis_tdata_r_reg[31]_2 [23]),
        .I4(s00_axis_tuser[0]),
        .I5(\m00_axis_tdata_r_reg[31]_3 [23]),
        .O(\m00_axis_tdata_r[23]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata_r[24]_i_5 
       (.I0(DOUT3_w[24]),
        .I1(dout0_out[24]),
        .I2(s00_axis_tuser[1]),
        .I3(\m00_axis_tdata_r_reg[31]_2 [24]),
        .I4(s00_axis_tuser[0]),
        .I5(\m00_axis_tdata_r_reg[31]_3 [24]),
        .O(\m00_axis_tdata_r[24]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata_r[25]_i_3 
       (.I0(DOUT3_w[25]),
        .I1(dout0_out[25]),
        .I2(s00_axis_tuser[1]),
        .I3(\m00_axis_tdata_r_reg[31]_2 [25]),
        .I4(s00_axis_tuser[0]),
        .I5(\m00_axis_tdata_r_reg[31]_3 [25]),
        .O(ram_reg_1_1));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata_r[26]_i_5 
       (.I0(DOUT3_w[26]),
        .I1(dout0_out[26]),
        .I2(s00_axis_tuser[1]),
        .I3(\m00_axis_tdata_r_reg[31]_2 [26]),
        .I4(s00_axis_tuser[0]),
        .I5(\m00_axis_tdata_r_reg[31]_3 [26]),
        .O(\m00_axis_tdata_r[26]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata_r[27]_i_3 
       (.I0(DOUT3_w[27]),
        .I1(dout0_out[27]),
        .I2(s00_axis_tuser[1]),
        .I3(\m00_axis_tdata_r_reg[31]_2 [27]),
        .I4(s00_axis_tuser[0]),
        .I5(\m00_axis_tdata_r_reg[31]_3 [27]),
        .O(ram_reg_1_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata_r[28]_i_5 
       (.I0(DOUT3_w[28]),
        .I1(dout0_out[28]),
        .I2(s00_axis_tuser[1]),
        .I3(\m00_axis_tdata_r_reg[31]_2 [28]),
        .I4(s00_axis_tuser[0]),
        .I5(\m00_axis_tdata_r_reg[31]_3 [28]),
        .O(\m00_axis_tdata_r[28]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata_r[29]_i_5 
       (.I0(DOUT3_w[29]),
        .I1(dout0_out[29]),
        .I2(s00_axis_tuser[1]),
        .I3(\m00_axis_tdata_r_reg[31]_2 [29]),
        .I4(s00_axis_tuser[0]),
        .I5(\m00_axis_tdata_r_reg[31]_3 [29]),
        .O(\m00_axis_tdata_r[29]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata_r[2]_i_5 
       (.I0(DOUT3_w[2]),
        .I1(dout0_out[2]),
        .I2(s00_axis_tuser[1]),
        .I3(\m00_axis_tdata_r_reg[31]_2 [2]),
        .I4(s00_axis_tuser[0]),
        .I5(\m00_axis_tdata_r_reg[31]_3 [2]),
        .O(\m00_axis_tdata_r[2]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata_r[30]_i_5 
       (.I0(DOUT3_w[30]),
        .I1(dout0_out[30]),
        .I2(s00_axis_tuser[1]),
        .I3(\m00_axis_tdata_r_reg[31]_2 [30]),
        .I4(s00_axis_tuser[0]),
        .I5(\m00_axis_tdata_r_reg[31]_3 [30]),
        .O(\m00_axis_tdata_r[30]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hFFCA0FCAF0CA00CA)) 
    \m00_axis_tdata_r[31]_i_2 
       (.I0(\m00_axis_tdata_r[31]_i_5_n_0 ),
        .I1(\m00_axis_tdata_r_reg[31] ),
        .I2(s00_axis_tuser[2]),
        .I3(s00_axis_tuser[3]),
        .I4(\m00_axis_tdata_r_reg[31]_0 ),
        .I5(\m00_axis_tdata_r_reg[31]_1 ),
        .O(D));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata_r[31]_i_5 
       (.I0(DOUT3_w[31]),
        .I1(dout0_out[31]),
        .I2(s00_axis_tuser[1]),
        .I3(\m00_axis_tdata_r_reg[31]_2 [31]),
        .I4(s00_axis_tuser[0]),
        .I5(\m00_axis_tdata_r_reg[31]_3 [31]),
        .O(\m00_axis_tdata_r[31]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata_r[3]_i_5 
       (.I0(DOUT3_w[3]),
        .I1(dout0_out[3]),
        .I2(s00_axis_tuser[1]),
        .I3(\m00_axis_tdata_r_reg[31]_2 [3]),
        .I4(s00_axis_tuser[0]),
        .I5(\m00_axis_tdata_r_reg[31]_3 [3]),
        .O(\m00_axis_tdata_r[3]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata_r[4]_i_3 
       (.I0(DOUT3_w[4]),
        .I1(dout0_out[4]),
        .I2(s00_axis_tuser[1]),
        .I3(\m00_axis_tdata_r_reg[31]_2 [4]),
        .I4(s00_axis_tuser[0]),
        .I5(\m00_axis_tdata_r_reg[31]_3 [4]),
        .O(ram_reg_0_4));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata_r[5]_i_5 
       (.I0(DOUT3_w[5]),
        .I1(dout0_out[5]),
        .I2(s00_axis_tuser[1]),
        .I3(\m00_axis_tdata_r_reg[31]_2 [5]),
        .I4(s00_axis_tuser[0]),
        .I5(\m00_axis_tdata_r_reg[31]_3 [5]),
        .O(\m00_axis_tdata_r[5]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata_r[6]_i_5 
       (.I0(DOUT3_w[6]),
        .I1(dout0_out[6]),
        .I2(s00_axis_tuser[1]),
        .I3(\m00_axis_tdata_r_reg[31]_2 [6]),
        .I4(s00_axis_tuser[0]),
        .I5(\m00_axis_tdata_r_reg[31]_3 [6]),
        .O(\m00_axis_tdata_r[6]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata_r[7]_i_5 
       (.I0(DOUT3_w[7]),
        .I1(dout0_out[7]),
        .I2(s00_axis_tuser[1]),
        .I3(\m00_axis_tdata_r_reg[31]_2 [7]),
        .I4(s00_axis_tuser[0]),
        .I5(\m00_axis_tdata_r_reg[31]_3 [7]),
        .O(\m00_axis_tdata_r[7]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata_r[8]_i_5 
       (.I0(DOUT3_w[8]),
        .I1(dout0_out[8]),
        .I2(s00_axis_tuser[1]),
        .I3(\m00_axis_tdata_r_reg[31]_2 [8]),
        .I4(s00_axis_tuser[0]),
        .I5(\m00_axis_tdata_r_reg[31]_3 [8]),
        .O(\m00_axis_tdata_r[8]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata_r[9]_i_3 
       (.I0(DOUT3_w[9]),
        .I1(dout0_out[9]),
        .I2(s00_axis_tuser[1]),
        .I3(\m00_axis_tdata_r_reg[31]_2 [9]),
        .I4(s00_axis_tuser[0]),
        .I5(\m00_axis_tdata_r_reg[31]_3 [9]),
        .O(ram_reg_0_3));
  MUXF7 \m00_axis_tdata_r_reg[10]_i_4 
       (.I0(\m00_axis_tdata_r[10]_i_5_n_0 ),
        .I1(\m00_axis_tdata_r_reg[10] ),
        .O(\s00_axis_tuser[18]_13 ),
        .S(s00_axis_tuser[2]));
  MUXF7 \m00_axis_tdata_r_reg[12]_i_4 
       (.I0(\m00_axis_tdata_r[12]_i_5_n_0 ),
        .I1(\m00_axis_tdata_r_reg[12] ),
        .O(\s00_axis_tuser[18]_12 ),
        .S(s00_axis_tuser[2]));
  MUXF7 \m00_axis_tdata_r_reg[13]_i_4 
       (.I0(\m00_axis_tdata_r[13]_i_5_n_0 ),
        .I1(\m00_axis_tdata_r_reg[13] ),
        .O(\s00_axis_tuser[18]_11 ),
        .S(s00_axis_tuser[2]));
  MUXF7 \m00_axis_tdata_r_reg[14]_i_4 
       (.I0(\m00_axis_tdata_r[14]_i_5_n_0 ),
        .I1(\m00_axis_tdata_r_reg[14] ),
        .O(\s00_axis_tuser[18]_10 ),
        .S(s00_axis_tuser[2]));
  MUXF7 \m00_axis_tdata_r_reg[17]_i_4 
       (.I0(\m00_axis_tdata_r[17]_i_5_n_0 ),
        .I1(\m00_axis_tdata_r_reg[17] ),
        .O(\s00_axis_tuser[18]_9 ),
        .S(s00_axis_tuser[2]));
  MUXF7 \m00_axis_tdata_r_reg[18]_i_4 
       (.I0(\m00_axis_tdata_r[18]_i_5_n_0 ),
        .I1(\m00_axis_tdata_r_reg[18] ),
        .O(\s00_axis_tuser[18]_8 ),
        .S(s00_axis_tuser[2]));
  MUXF7 \m00_axis_tdata_r_reg[19]_i_4 
       (.I0(\m00_axis_tdata_r[19]_i_5_n_0 ),
        .I1(\m00_axis_tdata_r_reg[19] ),
        .O(\s00_axis_tuser[18]_7 ),
        .S(s00_axis_tuser[2]));
  MUXF7 \m00_axis_tdata_r_reg[1]_i_4 
       (.I0(\m00_axis_tdata_r[1]_i_5_n_0 ),
        .I1(\m00_axis_tdata_r_reg[1] ),
        .O(\s00_axis_tuser[18]_20 ),
        .S(s00_axis_tuser[2]));
  MUXF7 \m00_axis_tdata_r_reg[21]_i_4 
       (.I0(\m00_axis_tdata_r[21]_i_5_n_0 ),
        .I1(\m00_axis_tdata_r_reg[21] ),
        .O(\s00_axis_tuser[18]_6 ),
        .S(s00_axis_tuser[2]));
  MUXF7 \m00_axis_tdata_r_reg[22]_i_4 
       (.I0(\m00_axis_tdata_r[22]_i_5_n_0 ),
        .I1(\m00_axis_tdata_r_reg[22] ),
        .O(\s00_axis_tuser[18]_5 ),
        .S(s00_axis_tuser[2]));
  MUXF7 \m00_axis_tdata_r_reg[23]_i_4 
       (.I0(\m00_axis_tdata_r[23]_i_5_n_0 ),
        .I1(\m00_axis_tdata_r_reg[23] ),
        .O(\s00_axis_tuser[18]_4 ),
        .S(s00_axis_tuser[2]));
  MUXF7 \m00_axis_tdata_r_reg[24]_i_4 
       (.I0(\m00_axis_tdata_r[24]_i_5_n_0 ),
        .I1(\m00_axis_tdata_r_reg[24] ),
        .O(\s00_axis_tuser[18]_3 ),
        .S(s00_axis_tuser[2]));
  MUXF7 \m00_axis_tdata_r_reg[26]_i_4 
       (.I0(\m00_axis_tdata_r[26]_i_5_n_0 ),
        .I1(\m00_axis_tdata_r_reg[26] ),
        .O(\s00_axis_tuser[18]_2 ),
        .S(s00_axis_tuser[2]));
  MUXF7 \m00_axis_tdata_r_reg[28]_i_4 
       (.I0(\m00_axis_tdata_r[28]_i_5_n_0 ),
        .I1(\m00_axis_tdata_r_reg[28] ),
        .O(\s00_axis_tuser[18]_1 ),
        .S(s00_axis_tuser[2]));
  MUXF7 \m00_axis_tdata_r_reg[29]_i_4 
       (.I0(\m00_axis_tdata_r[29]_i_5_n_0 ),
        .I1(\m00_axis_tdata_r_reg[29] ),
        .O(\s00_axis_tuser[18]_0 ),
        .S(s00_axis_tuser[2]));
  MUXF7 \m00_axis_tdata_r_reg[2]_i_4 
       (.I0(\m00_axis_tdata_r[2]_i_5_n_0 ),
        .I1(\m00_axis_tdata_r_reg[2] ),
        .O(\s00_axis_tuser[18]_19 ),
        .S(s00_axis_tuser[2]));
  MUXF7 \m00_axis_tdata_r_reg[30]_i_4 
       (.I0(\m00_axis_tdata_r[30]_i_5_n_0 ),
        .I1(\m00_axis_tdata_r_reg[30] ),
        .O(\s00_axis_tuser[18] ),
        .S(s00_axis_tuser[2]));
  MUXF7 \m00_axis_tdata_r_reg[3]_i_4 
       (.I0(\m00_axis_tdata_r[3]_i_5_n_0 ),
        .I1(\m00_axis_tdata_r_reg[3] ),
        .O(\s00_axis_tuser[18]_18 ),
        .S(s00_axis_tuser[2]));
  MUXF7 \m00_axis_tdata_r_reg[5]_i_4 
       (.I0(\m00_axis_tdata_r[5]_i_5_n_0 ),
        .I1(\m00_axis_tdata_r_reg[5] ),
        .O(\s00_axis_tuser[18]_17 ),
        .S(s00_axis_tuser[2]));
  MUXF7 \m00_axis_tdata_r_reg[6]_i_4 
       (.I0(\m00_axis_tdata_r[6]_i_5_n_0 ),
        .I1(\m00_axis_tdata_r_reg[6] ),
        .O(\s00_axis_tuser[18]_16 ),
        .S(s00_axis_tuser[2]));
  MUXF7 \m00_axis_tdata_r_reg[7]_i_4 
       (.I0(\m00_axis_tdata_r[7]_i_5_n_0 ),
        .I1(\m00_axis_tdata_r_reg[7] ),
        .O(\s00_axis_tuser[18]_15 ),
        .S(s00_axis_tuser[2]));
  MUXF7 \m00_axis_tdata_r_reg[8]_i_4 
       (.I0(\m00_axis_tdata_r[8]_i_5_n_0 ),
        .I1(\m00_axis_tdata_r_reg[8] ),
        .O(\s00_axis_tuser[18]_14 ),
        .S(s00_axis_tuser[2]));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p2_d16" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "61472" *) 
  (* RTL_RAM_NAME = "ram" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "2047" *) 
  (* bram_slice_begin = "0" *) 
  (* bram_slice_end = "17" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "2047" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "17" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(18),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(18),
    .WRITE_WIDTH_B(0)) 
    ram_reg_0
       (.ADDRARDADDR({1'b1,ADDRARDADDR,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CASCADEINA(1'b1),
        .CASCADEINB(1'b0),
        .CASCADEOUTA(NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED),
        .CASCADEOUTB(NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED),
        .CLKARDCLK(m00_axis_aclk),
        .CLKBWRCLK(1'b0),
        .DBITERR(NLW_ram_reg_0_DBITERR_UNCONNECTED),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,Q[15:0]}),
        .DIBDI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0,Q[17:16]}),
        .DIPBDIP({1'b1,1'b1,1'b1,1'b1}),
        .DOADO({NLW_ram_reg_0_DOADO_UNCONNECTED[31:16],DOUT3_w[15:0]}),
        .DOBDO(NLW_ram_reg_0_DOBDO_UNCONNECTED[31:0]),
        .DOPADOP({NLW_ram_reg_0_DOPADOP_UNCONNECTED[3:2],DOUT3_w[17:16]}),
        .DOPBDOP(NLW_ram_reg_0_DOPBDOP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_ram_reg_0_ECCPARITY_UNCONNECTED[7:0]),
        .ENARDEN(en),
        .ENBWREN(1'b0),
        .INJECTDBITERR(NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED),
        .INJECTSBITERR(NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED),
        .RDADDRECC(NLW_ram_reg_0_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_ram_reg_0_SBITERR_UNCONNECTED),
        .WEA({ram_reg_0_i_1__2_n_0,ram_reg_0_i_1__2_n_0,ram_reg_0_i_1__2_n_0,ram_reg_0_i_1__2_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
  LUT3 #(
    .INIT(8'h80)) 
    ram_reg_0_i_1__2
       (.I0(en),
        .I1(WE_r),
        .I2(square_r),
        .O(ram_reg_0_i_1__2_n_0));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d14" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "61472" *) 
  (* RTL_RAM_NAME = "ram" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "2047" *) 
  (* bram_slice_begin = "18" *) 
  (* bram_slice_end = "31" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "2047" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "18" *) 
  (* ram_slice_end = "31" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(18),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(18),
    .WRITE_WIDTH_B(0)) 
    ram_reg_1
       (.ADDRARDADDR({1'b1,addr,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CASCADEINA(1'b1),
        .CASCADEINB(1'b0),
        .CASCADEOUTA(NLW_ram_reg_1_CASCADEOUTA_UNCONNECTED),
        .CASCADEOUTB(NLW_ram_reg_1_CASCADEOUTB_UNCONNECTED),
        .CLKARDCLK(m00_axis_aclk),
        .CLKBWRCLK(1'b0),
        .DBITERR(NLW_ram_reg_1_DBITERR_UNCONNECTED),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,Q[31:18]}),
        .DIBDI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0,1'b0,1'b0}),
        .DIPBDIP({1'b1,1'b1,1'b1,1'b1}),
        .DOADO({NLW_ram_reg_1_DOADO_UNCONNECTED[31:14],DOUT3_w[31:18]}),
        .DOBDO(NLW_ram_reg_1_DOBDO_UNCONNECTED[31:0]),
        .DOPADOP(NLW_ram_reg_1_DOPADOP_UNCONNECTED[3:0]),
        .DOPBDOP(NLW_ram_reg_1_DOPBDOP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_ram_reg_1_ECCPARITY_UNCONNECTED[7:0]),
        .ENARDEN(en),
        .ENBWREN(1'b0),
        .INJECTDBITERR(NLW_ram_reg_1_INJECTDBITERR_UNCONNECTED),
        .INJECTSBITERR(NLW_ram_reg_1_INJECTSBITERR_UNCONNECTED),
        .RDADDRECC(NLW_ram_reg_1_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_ram_reg_1_SBITERR_UNCONNECTED),
        .WEA({ram_reg_0_i_1__2_n_0,ram_reg_0_i_1__2_n_0,ram_reg_0_i_1__2_n_0,ram_reg_0_i_1__2_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
endmodule

(* ORIG_REF_NAME = "rams_sp_rf_rst" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_3
   (dout0_out,
    m00_axis_aclk,
    en,
    ADDRARDADDR,
    Q,
    addr,
    WE_r,
    square_r);
  output [31:0]dout0_out;
  input m00_axis_aclk;
  input en;
  input [10:0]ADDRARDADDR;
  input [31:0]Q;
  input [10:0]addr;
  input WE_r;
  input square_r;

  wire [10:0]ADDRARDADDR;
  wire [31:0]Q;
  wire WE_r;
  wire [10:0]addr;
  wire [31:0]dout0_out;
  wire en;
  wire m00_axis_aclk;
  wire ram_reg_0_i_1__3_n_0;
  wire square_r;
  wire NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED;
  wire NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED;
  wire NLW_ram_reg_0_DBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_SBITERR_UNCONNECTED;
  wire [31:16]NLW_ram_reg_0_DOADO_UNCONNECTED;
  wire [31:0]NLW_ram_reg_0_DOBDO_UNCONNECTED;
  wire [3:2]NLW_ram_reg_0_DOPADOP_UNCONNECTED;
  wire [3:0]NLW_ram_reg_0_DOPBDOP_UNCONNECTED;
  wire [7:0]NLW_ram_reg_0_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_ram_reg_0_RDADDRECC_UNCONNECTED;
  wire NLW_ram_reg_1_CASCADEOUTA_UNCONNECTED;
  wire NLW_ram_reg_1_CASCADEOUTB_UNCONNECTED;
  wire NLW_ram_reg_1_DBITERR_UNCONNECTED;
  wire NLW_ram_reg_1_INJECTDBITERR_UNCONNECTED;
  wire NLW_ram_reg_1_INJECTSBITERR_UNCONNECTED;
  wire NLW_ram_reg_1_SBITERR_UNCONNECTED;
  wire [31:14]NLW_ram_reg_1_DOADO_UNCONNECTED;
  wire [31:0]NLW_ram_reg_1_DOBDO_UNCONNECTED;
  wire [3:0]NLW_ram_reg_1_DOPADOP_UNCONNECTED;
  wire [3:0]NLW_ram_reg_1_DOPBDOP_UNCONNECTED;
  wire [7:0]NLW_ram_reg_1_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_ram_reg_1_RDADDRECC_UNCONNECTED;

  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p2_d16" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "61472" *) 
  (* RTL_RAM_NAME = "ram" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "2047" *) 
  (* bram_slice_begin = "0" *) 
  (* bram_slice_end = "17" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "2047" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "17" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(18),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(18),
    .WRITE_WIDTH_B(0)) 
    ram_reg_0
       (.ADDRARDADDR({1'b1,ADDRARDADDR,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CASCADEINA(1'b1),
        .CASCADEINB(1'b0),
        .CASCADEOUTA(NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED),
        .CASCADEOUTB(NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED),
        .CLKARDCLK(m00_axis_aclk),
        .CLKBWRCLK(1'b0),
        .DBITERR(NLW_ram_reg_0_DBITERR_UNCONNECTED),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,Q[15:0]}),
        .DIBDI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0,Q[17:16]}),
        .DIPBDIP({1'b1,1'b1,1'b1,1'b1}),
        .DOADO({NLW_ram_reg_0_DOADO_UNCONNECTED[31:16],dout0_out[15:0]}),
        .DOBDO(NLW_ram_reg_0_DOBDO_UNCONNECTED[31:0]),
        .DOPADOP({NLW_ram_reg_0_DOPADOP_UNCONNECTED[3:2],dout0_out[17:16]}),
        .DOPBDOP(NLW_ram_reg_0_DOPBDOP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_ram_reg_0_ECCPARITY_UNCONNECTED[7:0]),
        .ENARDEN(en),
        .ENBWREN(1'b0),
        .INJECTDBITERR(NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED),
        .INJECTSBITERR(NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED),
        .RDADDRECC(NLW_ram_reg_0_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_ram_reg_0_SBITERR_UNCONNECTED),
        .WEA({ram_reg_0_i_1__3_n_0,ram_reg_0_i_1__3_n_0,ram_reg_0_i_1__3_n_0,ram_reg_0_i_1__3_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
  LUT3 #(
    .INIT(8'h80)) 
    ram_reg_0_i_1__3
       (.I0(en),
        .I1(WE_r),
        .I2(square_r),
        .O(ram_reg_0_i_1__3_n_0));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d14" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "61472" *) 
  (* RTL_RAM_NAME = "ram" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "2047" *) 
  (* bram_slice_begin = "18" *) 
  (* bram_slice_end = "31" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "2047" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "18" *) 
  (* ram_slice_end = "31" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(18),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(18),
    .WRITE_WIDTH_B(0)) 
    ram_reg_1
       (.ADDRARDADDR({1'b1,addr,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CASCADEINA(1'b1),
        .CASCADEINB(1'b0),
        .CASCADEOUTA(NLW_ram_reg_1_CASCADEOUTA_UNCONNECTED),
        .CASCADEOUTB(NLW_ram_reg_1_CASCADEOUTB_UNCONNECTED),
        .CLKARDCLK(m00_axis_aclk),
        .CLKBWRCLK(1'b0),
        .DBITERR(NLW_ram_reg_1_DBITERR_UNCONNECTED),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,Q[31:18]}),
        .DIBDI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0,1'b0,1'b0}),
        .DIPBDIP({1'b1,1'b1,1'b1,1'b1}),
        .DOADO({NLW_ram_reg_1_DOADO_UNCONNECTED[31:14],dout0_out[31:18]}),
        .DOBDO(NLW_ram_reg_1_DOBDO_UNCONNECTED[31:0]),
        .DOPADOP(NLW_ram_reg_1_DOPADOP_UNCONNECTED[3:0]),
        .DOPBDOP(NLW_ram_reg_1_DOPBDOP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_ram_reg_1_ECCPARITY_UNCONNECTED[7:0]),
        .ENARDEN(en),
        .ENBWREN(1'b0),
        .INJECTDBITERR(NLW_ram_reg_1_INJECTDBITERR_UNCONNECTED),
        .INJECTSBITERR(NLW_ram_reg_1_INJECTSBITERR_UNCONNECTED),
        .RDADDRECC(NLW_ram_reg_1_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_ram_reg_1_SBITERR_UNCONNECTED),
        .WEA({ram_reg_0_i_1__3_n_0,ram_reg_0_i_1__3_n_0,ram_reg_0_i_1__3_n_0,ram_reg_0_i_1__3_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
endmodule

(* ORIG_REF_NAME = "rams_sp_rf_rst" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_4
   (dout0_out,
    m00_axis_aclk,
    en,
    ADDRARDADDR,
    Q,
    addr,
    WE_r,
    square_r);
  output [31:0]dout0_out;
  input m00_axis_aclk;
  input en;
  input [10:0]ADDRARDADDR;
  input [31:0]Q;
  input [10:0]addr;
  input WE_r;
  input square_r;

  wire [10:0]ADDRARDADDR;
  wire [31:0]Q;
  wire WE_r;
  wire [10:0]addr;
  wire [31:0]dout0_out;
  wire en;
  wire m00_axis_aclk;
  wire ram_reg_0_i_1__4_n_0;
  wire square_r;
  wire NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED;
  wire NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED;
  wire NLW_ram_reg_0_DBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_SBITERR_UNCONNECTED;
  wire [31:16]NLW_ram_reg_0_DOADO_UNCONNECTED;
  wire [31:0]NLW_ram_reg_0_DOBDO_UNCONNECTED;
  wire [3:2]NLW_ram_reg_0_DOPADOP_UNCONNECTED;
  wire [3:0]NLW_ram_reg_0_DOPBDOP_UNCONNECTED;
  wire [7:0]NLW_ram_reg_0_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_ram_reg_0_RDADDRECC_UNCONNECTED;
  wire NLW_ram_reg_1_CASCADEOUTA_UNCONNECTED;
  wire NLW_ram_reg_1_CASCADEOUTB_UNCONNECTED;
  wire NLW_ram_reg_1_DBITERR_UNCONNECTED;
  wire NLW_ram_reg_1_INJECTDBITERR_UNCONNECTED;
  wire NLW_ram_reg_1_INJECTSBITERR_UNCONNECTED;
  wire NLW_ram_reg_1_SBITERR_UNCONNECTED;
  wire [31:14]NLW_ram_reg_1_DOADO_UNCONNECTED;
  wire [31:0]NLW_ram_reg_1_DOBDO_UNCONNECTED;
  wire [3:0]NLW_ram_reg_1_DOPADOP_UNCONNECTED;
  wire [3:0]NLW_ram_reg_1_DOPBDOP_UNCONNECTED;
  wire [7:0]NLW_ram_reg_1_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_ram_reg_1_RDADDRECC_UNCONNECTED;

  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p2_d16" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "61472" *) 
  (* RTL_RAM_NAME = "ram" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "2047" *) 
  (* bram_slice_begin = "0" *) 
  (* bram_slice_end = "17" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "2047" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "17" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(18),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(18),
    .WRITE_WIDTH_B(0)) 
    ram_reg_0
       (.ADDRARDADDR({1'b1,ADDRARDADDR,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CASCADEINA(1'b1),
        .CASCADEINB(1'b0),
        .CASCADEOUTA(NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED),
        .CASCADEOUTB(NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED),
        .CLKARDCLK(m00_axis_aclk),
        .CLKBWRCLK(1'b0),
        .DBITERR(NLW_ram_reg_0_DBITERR_UNCONNECTED),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,Q[15:0]}),
        .DIBDI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0,Q[17:16]}),
        .DIPBDIP({1'b1,1'b1,1'b1,1'b1}),
        .DOADO({NLW_ram_reg_0_DOADO_UNCONNECTED[31:16],dout0_out[15:0]}),
        .DOBDO(NLW_ram_reg_0_DOBDO_UNCONNECTED[31:0]),
        .DOPADOP({NLW_ram_reg_0_DOPADOP_UNCONNECTED[3:2],dout0_out[17:16]}),
        .DOPBDOP(NLW_ram_reg_0_DOPBDOP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_ram_reg_0_ECCPARITY_UNCONNECTED[7:0]),
        .ENARDEN(en),
        .ENBWREN(1'b0),
        .INJECTDBITERR(NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED),
        .INJECTSBITERR(NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED),
        .RDADDRECC(NLW_ram_reg_0_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_ram_reg_0_SBITERR_UNCONNECTED),
        .WEA({ram_reg_0_i_1__4_n_0,ram_reg_0_i_1__4_n_0,ram_reg_0_i_1__4_n_0,ram_reg_0_i_1__4_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
  LUT3 #(
    .INIT(8'h80)) 
    ram_reg_0_i_1__4
       (.I0(en),
        .I1(WE_r),
        .I2(square_r),
        .O(ram_reg_0_i_1__4_n_0));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d14" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "61472" *) 
  (* RTL_RAM_NAME = "ram" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "2047" *) 
  (* bram_slice_begin = "18" *) 
  (* bram_slice_end = "31" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "2047" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "18" *) 
  (* ram_slice_end = "31" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(18),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(18),
    .WRITE_WIDTH_B(0)) 
    ram_reg_1
       (.ADDRARDADDR({1'b1,addr,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CASCADEINA(1'b1),
        .CASCADEINB(1'b0),
        .CASCADEOUTA(NLW_ram_reg_1_CASCADEOUTA_UNCONNECTED),
        .CASCADEOUTB(NLW_ram_reg_1_CASCADEOUTB_UNCONNECTED),
        .CLKARDCLK(m00_axis_aclk),
        .CLKBWRCLK(1'b0),
        .DBITERR(NLW_ram_reg_1_DBITERR_UNCONNECTED),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,Q[31:18]}),
        .DIBDI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0,1'b0,1'b0}),
        .DIPBDIP({1'b1,1'b1,1'b1,1'b1}),
        .DOADO({NLW_ram_reg_1_DOADO_UNCONNECTED[31:14],dout0_out[31:18]}),
        .DOBDO(NLW_ram_reg_1_DOBDO_UNCONNECTED[31:0]),
        .DOPADOP(NLW_ram_reg_1_DOPADOP_UNCONNECTED[3:0]),
        .DOPBDOP(NLW_ram_reg_1_DOPBDOP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_ram_reg_1_ECCPARITY_UNCONNECTED[7:0]),
        .ENARDEN(en),
        .ENBWREN(1'b0),
        .INJECTDBITERR(NLW_ram_reg_1_INJECTDBITERR_UNCONNECTED),
        .INJECTSBITERR(NLW_ram_reg_1_INJECTSBITERR_UNCONNECTED),
        .RDADDRECC(NLW_ram_reg_1_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_ram_reg_1_SBITERR_UNCONNECTED),
        .WEA({ram_reg_0_i_1__4_n_0,ram_reg_0_i_1__4_n_0,ram_reg_0_i_1__4_n_0,ram_reg_0_i_1__4_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
endmodule

(* ORIG_REF_NAME = "rams_sp_rf_rst" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_5
   (dout0_out,
    m00_axis_aclk,
    en,
    ADDRARDADDR,
    Q,
    addr,
    WE_r,
    square_r);
  output [31:0]dout0_out;
  input m00_axis_aclk;
  input en;
  input [10:0]ADDRARDADDR;
  input [31:0]Q;
  input [10:0]addr;
  input WE_r;
  input square_r;

  wire [10:0]ADDRARDADDR;
  wire [31:0]Q;
  wire WE_r;
  wire [10:0]addr;
  wire [31:0]dout0_out;
  wire en;
  wire m00_axis_aclk;
  wire ram_reg_0_i_1__5_n_0;
  wire square_r;
  wire NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED;
  wire NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED;
  wire NLW_ram_reg_0_DBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_SBITERR_UNCONNECTED;
  wire [31:16]NLW_ram_reg_0_DOADO_UNCONNECTED;
  wire [31:0]NLW_ram_reg_0_DOBDO_UNCONNECTED;
  wire [3:2]NLW_ram_reg_0_DOPADOP_UNCONNECTED;
  wire [3:0]NLW_ram_reg_0_DOPBDOP_UNCONNECTED;
  wire [7:0]NLW_ram_reg_0_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_ram_reg_0_RDADDRECC_UNCONNECTED;
  wire NLW_ram_reg_1_CASCADEOUTA_UNCONNECTED;
  wire NLW_ram_reg_1_CASCADEOUTB_UNCONNECTED;
  wire NLW_ram_reg_1_DBITERR_UNCONNECTED;
  wire NLW_ram_reg_1_INJECTDBITERR_UNCONNECTED;
  wire NLW_ram_reg_1_INJECTSBITERR_UNCONNECTED;
  wire NLW_ram_reg_1_SBITERR_UNCONNECTED;
  wire [31:14]NLW_ram_reg_1_DOADO_UNCONNECTED;
  wire [31:0]NLW_ram_reg_1_DOBDO_UNCONNECTED;
  wire [3:0]NLW_ram_reg_1_DOPADOP_UNCONNECTED;
  wire [3:0]NLW_ram_reg_1_DOPBDOP_UNCONNECTED;
  wire [7:0]NLW_ram_reg_1_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_ram_reg_1_RDADDRECC_UNCONNECTED;

  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p2_d16" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "61472" *) 
  (* RTL_RAM_NAME = "ram" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "2047" *) 
  (* bram_slice_begin = "0" *) 
  (* bram_slice_end = "17" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "2047" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "17" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(18),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(18),
    .WRITE_WIDTH_B(0)) 
    ram_reg_0
       (.ADDRARDADDR({1'b1,ADDRARDADDR,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CASCADEINA(1'b1),
        .CASCADEINB(1'b0),
        .CASCADEOUTA(NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED),
        .CASCADEOUTB(NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED),
        .CLKARDCLK(m00_axis_aclk),
        .CLKBWRCLK(1'b0),
        .DBITERR(NLW_ram_reg_0_DBITERR_UNCONNECTED),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,Q[15:0]}),
        .DIBDI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0,Q[17:16]}),
        .DIPBDIP({1'b1,1'b1,1'b1,1'b1}),
        .DOADO({NLW_ram_reg_0_DOADO_UNCONNECTED[31:16],dout0_out[15:0]}),
        .DOBDO(NLW_ram_reg_0_DOBDO_UNCONNECTED[31:0]),
        .DOPADOP({NLW_ram_reg_0_DOPADOP_UNCONNECTED[3:2],dout0_out[17:16]}),
        .DOPBDOP(NLW_ram_reg_0_DOPBDOP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_ram_reg_0_ECCPARITY_UNCONNECTED[7:0]),
        .ENARDEN(en),
        .ENBWREN(1'b0),
        .INJECTDBITERR(NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED),
        .INJECTSBITERR(NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED),
        .RDADDRECC(NLW_ram_reg_0_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_ram_reg_0_SBITERR_UNCONNECTED),
        .WEA({ram_reg_0_i_1__5_n_0,ram_reg_0_i_1__5_n_0,ram_reg_0_i_1__5_n_0,ram_reg_0_i_1__5_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
  LUT3 #(
    .INIT(8'h80)) 
    ram_reg_0_i_1__5
       (.I0(en),
        .I1(WE_r),
        .I2(square_r),
        .O(ram_reg_0_i_1__5_n_0));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d14" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "61472" *) 
  (* RTL_RAM_NAME = "ram" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "2047" *) 
  (* bram_slice_begin = "18" *) 
  (* bram_slice_end = "31" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "2047" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "18" *) 
  (* ram_slice_end = "31" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(18),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(18),
    .WRITE_WIDTH_B(0)) 
    ram_reg_1
       (.ADDRARDADDR({1'b1,addr,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CASCADEINA(1'b1),
        .CASCADEINB(1'b0),
        .CASCADEOUTA(NLW_ram_reg_1_CASCADEOUTA_UNCONNECTED),
        .CASCADEOUTB(NLW_ram_reg_1_CASCADEOUTB_UNCONNECTED),
        .CLKARDCLK(m00_axis_aclk),
        .CLKBWRCLK(1'b0),
        .DBITERR(NLW_ram_reg_1_DBITERR_UNCONNECTED),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,Q[31:18]}),
        .DIBDI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0,1'b0,1'b0}),
        .DIPBDIP({1'b1,1'b1,1'b1,1'b1}),
        .DOADO({NLW_ram_reg_1_DOADO_UNCONNECTED[31:14],dout0_out[31:18]}),
        .DOBDO(NLW_ram_reg_1_DOBDO_UNCONNECTED[31:0]),
        .DOPADOP(NLW_ram_reg_1_DOPADOP_UNCONNECTED[3:0]),
        .DOPBDOP(NLW_ram_reg_1_DOPBDOP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_ram_reg_1_ECCPARITY_UNCONNECTED[7:0]),
        .ENARDEN(en),
        .ENBWREN(1'b0),
        .INJECTDBITERR(NLW_ram_reg_1_INJECTDBITERR_UNCONNECTED),
        .INJECTSBITERR(NLW_ram_reg_1_INJECTSBITERR_UNCONNECTED),
        .RDADDRECC(NLW_ram_reg_1_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_ram_reg_1_SBITERR_UNCONNECTED),
        .WEA({ram_reg_0_i_1__5_n_0,ram_reg_0_i_1__5_n_0,ram_reg_0_i_1__5_n_0,ram_reg_0_i_1__5_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
endmodule

(* ORIG_REF_NAME = "rams_sp_rf_rst" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_6
   (D,
    ram_reg_1_0,
    ram_reg_1_1,
    ram_reg_1_2,
    ram_reg_1_3,
    ram_reg_1_4,
    ram_reg_1_5,
    ram_reg_1_6,
    ram_reg_1_7,
    ram_reg_1_8,
    ram_reg_1_9,
    ram_reg_1_10,
    ram_reg_0_0,
    ram_reg_0_1,
    ram_reg_0_2,
    ram_reg_0_3,
    ram_reg_0_4,
    ram_reg_0_5,
    ram_reg_0_6,
    ram_reg_0_7,
    ram_reg_0_8,
    ram_reg_0_9,
    ram_reg_0_10,
    ram_reg_0_11,
    s00_axis_tuser,
    \m00_axis_tdata_r_reg[27] ,
    \m00_axis_tdata_r_reg[27]_0 ,
    \m00_axis_tdata_r_reg[25] ,
    \m00_axis_tdata_r_reg[25]_0 ,
    \m00_axis_tdata_r_reg[20] ,
    \m00_axis_tdata_r_reg[20]_0 ,
    \m00_axis_tdata_r_reg[16] ,
    \m00_axis_tdata_r_reg[16]_0 ,
    \m00_axis_tdata_r_reg[15] ,
    \m00_axis_tdata_r_reg[15]_0 ,
    \m00_axis_tdata_r_reg[11] ,
    \m00_axis_tdata_r_reg[11]_0 ,
    \m00_axis_tdata_r_reg[9] ,
    \m00_axis_tdata_r_reg[9]_0 ,
    \m00_axis_tdata_r_reg[4] ,
    \m00_axis_tdata_r_reg[4]_0 ,
    \m00_axis_tdata_r_reg[0] ,
    \m00_axis_tdata_r_reg[0]_0 ,
    dout0_out,
    \m00_axis_tdata_r_reg[31] ,
    \m00_axis_tdata_r_reg[31]_0 ,
    m00_axis_aclk,
    en,
    ADDRARDADDR,
    Q,
    addr,
    WE_r,
    square_r);
  output [8:0]D;
  output ram_reg_1_0;
  output ram_reg_1_1;
  output ram_reg_1_2;
  output ram_reg_1_3;
  output ram_reg_1_4;
  output ram_reg_1_5;
  output ram_reg_1_6;
  output ram_reg_1_7;
  output ram_reg_1_8;
  output ram_reg_1_9;
  output ram_reg_1_10;
  output ram_reg_0_0;
  output ram_reg_0_1;
  output ram_reg_0_2;
  output ram_reg_0_3;
  output ram_reg_0_4;
  output ram_reg_0_5;
  output ram_reg_0_6;
  output ram_reg_0_7;
  output ram_reg_0_8;
  output ram_reg_0_9;
  output ram_reg_0_10;
  output ram_reg_0_11;
  input [3:0]s00_axis_tuser;
  input \m00_axis_tdata_r_reg[27] ;
  input \m00_axis_tdata_r_reg[27]_0 ;
  input \m00_axis_tdata_r_reg[25] ;
  input \m00_axis_tdata_r_reg[25]_0 ;
  input \m00_axis_tdata_r_reg[20] ;
  input \m00_axis_tdata_r_reg[20]_0 ;
  input \m00_axis_tdata_r_reg[16] ;
  input \m00_axis_tdata_r_reg[16]_0 ;
  input \m00_axis_tdata_r_reg[15] ;
  input \m00_axis_tdata_r_reg[15]_0 ;
  input \m00_axis_tdata_r_reg[11] ;
  input \m00_axis_tdata_r_reg[11]_0 ;
  input \m00_axis_tdata_r_reg[9] ;
  input \m00_axis_tdata_r_reg[9]_0 ;
  input \m00_axis_tdata_r_reg[4] ;
  input \m00_axis_tdata_r_reg[4]_0 ;
  input \m00_axis_tdata_r_reg[0] ;
  input \m00_axis_tdata_r_reg[0]_0 ;
  input [31:0]dout0_out;
  input [31:0]\m00_axis_tdata_r_reg[31] ;
  input [31:0]\m00_axis_tdata_r_reg[31]_0 ;
  input m00_axis_aclk;
  input en;
  input [10:0]ADDRARDADDR;
  input [31:0]Q;
  input [10:0]addr;
  input WE_r;
  input square_r;

  wire [10:0]ADDRARDADDR;
  wire [8:0]D;
  wire [31:0]DOUT7_w;
  wire [31:0]Q;
  wire WE_r;
  wire [10:0]addr;
  wire [31:0]dout0_out;
  wire en;
  wire m00_axis_aclk;
  wire \m00_axis_tdata_r[0]_i_2_n_0 ;
  wire \m00_axis_tdata_r[11]_i_2_n_0 ;
  wire \m00_axis_tdata_r[15]_i_2_n_0 ;
  wire \m00_axis_tdata_r[16]_i_2_n_0 ;
  wire \m00_axis_tdata_r[20]_i_2_n_0 ;
  wire \m00_axis_tdata_r[25]_i_2_n_0 ;
  wire \m00_axis_tdata_r[27]_i_2_n_0 ;
  wire \m00_axis_tdata_r[4]_i_2_n_0 ;
  wire \m00_axis_tdata_r[9]_i_2_n_0 ;
  wire \m00_axis_tdata_r_reg[0] ;
  wire \m00_axis_tdata_r_reg[0]_0 ;
  wire \m00_axis_tdata_r_reg[11] ;
  wire \m00_axis_tdata_r_reg[11]_0 ;
  wire \m00_axis_tdata_r_reg[15] ;
  wire \m00_axis_tdata_r_reg[15]_0 ;
  wire \m00_axis_tdata_r_reg[16] ;
  wire \m00_axis_tdata_r_reg[16]_0 ;
  wire \m00_axis_tdata_r_reg[20] ;
  wire \m00_axis_tdata_r_reg[20]_0 ;
  wire \m00_axis_tdata_r_reg[25] ;
  wire \m00_axis_tdata_r_reg[25]_0 ;
  wire \m00_axis_tdata_r_reg[27] ;
  wire \m00_axis_tdata_r_reg[27]_0 ;
  wire [31:0]\m00_axis_tdata_r_reg[31] ;
  wire [31:0]\m00_axis_tdata_r_reg[31]_0 ;
  wire \m00_axis_tdata_r_reg[4] ;
  wire \m00_axis_tdata_r_reg[4]_0 ;
  wire \m00_axis_tdata_r_reg[9] ;
  wire \m00_axis_tdata_r_reg[9]_0 ;
  wire ram_reg_0_0;
  wire ram_reg_0_1;
  wire ram_reg_0_10;
  wire ram_reg_0_11;
  wire ram_reg_0_2;
  wire ram_reg_0_3;
  wire ram_reg_0_4;
  wire ram_reg_0_5;
  wire ram_reg_0_6;
  wire ram_reg_0_7;
  wire ram_reg_0_8;
  wire ram_reg_0_9;
  wire ram_reg_0_i_1__6_n_0;
  wire ram_reg_1_0;
  wire ram_reg_1_1;
  wire ram_reg_1_10;
  wire ram_reg_1_2;
  wire ram_reg_1_3;
  wire ram_reg_1_4;
  wire ram_reg_1_5;
  wire ram_reg_1_6;
  wire ram_reg_1_7;
  wire ram_reg_1_8;
  wire ram_reg_1_9;
  wire [3:0]s00_axis_tuser;
  wire square_r;
  wire NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED;
  wire NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED;
  wire NLW_ram_reg_0_DBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_SBITERR_UNCONNECTED;
  wire [31:16]NLW_ram_reg_0_DOADO_UNCONNECTED;
  wire [31:0]NLW_ram_reg_0_DOBDO_UNCONNECTED;
  wire [3:2]NLW_ram_reg_0_DOPADOP_UNCONNECTED;
  wire [3:0]NLW_ram_reg_0_DOPBDOP_UNCONNECTED;
  wire [7:0]NLW_ram_reg_0_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_ram_reg_0_RDADDRECC_UNCONNECTED;
  wire NLW_ram_reg_1_CASCADEOUTA_UNCONNECTED;
  wire NLW_ram_reg_1_CASCADEOUTB_UNCONNECTED;
  wire NLW_ram_reg_1_DBITERR_UNCONNECTED;
  wire NLW_ram_reg_1_INJECTDBITERR_UNCONNECTED;
  wire NLW_ram_reg_1_INJECTSBITERR_UNCONNECTED;
  wire NLW_ram_reg_1_SBITERR_UNCONNECTED;
  wire [31:14]NLW_ram_reg_1_DOADO_UNCONNECTED;
  wire [31:0]NLW_ram_reg_1_DOBDO_UNCONNECTED;
  wire [3:0]NLW_ram_reg_1_DOPADOP_UNCONNECTED;
  wire [3:0]NLW_ram_reg_1_DOPBDOP_UNCONNECTED;
  wire [7:0]NLW_ram_reg_1_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_ram_reg_1_RDADDRECC_UNCONNECTED;

  LUT5 #(
    .INIT(32'hEFEA4540)) 
    \m00_axis_tdata_r[0]_i_1 
       (.I0(s00_axis_tuser[3]),
        .I1(\m00_axis_tdata_r[0]_i_2_n_0 ),
        .I2(s00_axis_tuser[2]),
        .I3(\m00_axis_tdata_r_reg[0] ),
        .I4(\m00_axis_tdata_r_reg[0]_0 ),
        .O(D[0]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata_r[0]_i_2 
       (.I0(DOUT7_w[0]),
        .I1(dout0_out[0]),
        .I2(s00_axis_tuser[1]),
        .I3(\m00_axis_tdata_r_reg[31] [0]),
        .I4(s00_axis_tuser[0]),
        .I5(\m00_axis_tdata_r_reg[31]_0 [0]),
        .O(\m00_axis_tdata_r[0]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata_r[10]_i_6 
       (.I0(DOUT7_w[10]),
        .I1(dout0_out[10]),
        .I2(s00_axis_tuser[1]),
        .I3(\m00_axis_tdata_r_reg[31] [10]),
        .I4(s00_axis_tuser[0]),
        .I5(\m00_axis_tdata_r_reg[31]_0 [10]),
        .O(ram_reg_0_4));
  LUT5 #(
    .INIT(32'hEFEA4540)) 
    \m00_axis_tdata_r[11]_i_1 
       (.I0(s00_axis_tuser[3]),
        .I1(\m00_axis_tdata_r[11]_i_2_n_0 ),
        .I2(s00_axis_tuser[2]),
        .I3(\m00_axis_tdata_r_reg[11] ),
        .I4(\m00_axis_tdata_r_reg[11]_0 ),
        .O(D[3]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata_r[11]_i_2 
       (.I0(DOUT7_w[11]),
        .I1(dout0_out[11]),
        .I2(s00_axis_tuser[1]),
        .I3(\m00_axis_tdata_r_reg[31] [11]),
        .I4(s00_axis_tuser[0]),
        .I5(\m00_axis_tdata_r_reg[31]_0 [11]),
        .O(\m00_axis_tdata_r[11]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata_r[12]_i_6 
       (.I0(DOUT7_w[12]),
        .I1(dout0_out[12]),
        .I2(s00_axis_tuser[1]),
        .I3(\m00_axis_tdata_r_reg[31] [12]),
        .I4(s00_axis_tuser[0]),
        .I5(\m00_axis_tdata_r_reg[31]_0 [12]),
        .O(ram_reg_0_3));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata_r[13]_i_6 
       (.I0(DOUT7_w[13]),
        .I1(dout0_out[13]),
        .I2(s00_axis_tuser[1]),
        .I3(\m00_axis_tdata_r_reg[31] [13]),
        .I4(s00_axis_tuser[0]),
        .I5(\m00_axis_tdata_r_reg[31]_0 [13]),
        .O(ram_reg_0_2));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata_r[14]_i_6 
       (.I0(DOUT7_w[14]),
        .I1(dout0_out[14]),
        .I2(s00_axis_tuser[1]),
        .I3(\m00_axis_tdata_r_reg[31] [14]),
        .I4(s00_axis_tuser[0]),
        .I5(\m00_axis_tdata_r_reg[31]_0 [14]),
        .O(ram_reg_0_1));
  LUT5 #(
    .INIT(32'hEFEA4540)) 
    \m00_axis_tdata_r[15]_i_1 
       (.I0(s00_axis_tuser[3]),
        .I1(\m00_axis_tdata_r[15]_i_2_n_0 ),
        .I2(s00_axis_tuser[2]),
        .I3(\m00_axis_tdata_r_reg[15] ),
        .I4(\m00_axis_tdata_r_reg[15]_0 ),
        .O(D[4]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata_r[15]_i_2 
       (.I0(DOUT7_w[15]),
        .I1(dout0_out[15]),
        .I2(s00_axis_tuser[1]),
        .I3(\m00_axis_tdata_r_reg[31] [15]),
        .I4(s00_axis_tuser[0]),
        .I5(\m00_axis_tdata_r_reg[31]_0 [15]),
        .O(\m00_axis_tdata_r[15]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hEFEA4540)) 
    \m00_axis_tdata_r[16]_i_1 
       (.I0(s00_axis_tuser[3]),
        .I1(\m00_axis_tdata_r[16]_i_2_n_0 ),
        .I2(s00_axis_tuser[2]),
        .I3(\m00_axis_tdata_r_reg[16] ),
        .I4(\m00_axis_tdata_r_reg[16]_0 ),
        .O(D[5]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata_r[16]_i_2 
       (.I0(DOUT7_w[16]),
        .I1(dout0_out[16]),
        .I2(s00_axis_tuser[1]),
        .I3(\m00_axis_tdata_r_reg[31] [16]),
        .I4(s00_axis_tuser[0]),
        .I5(\m00_axis_tdata_r_reg[31]_0 [16]),
        .O(\m00_axis_tdata_r[16]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata_r[17]_i_6 
       (.I0(DOUT7_w[17]),
        .I1(dout0_out[17]),
        .I2(s00_axis_tuser[1]),
        .I3(\m00_axis_tdata_r_reg[31] [17]),
        .I4(s00_axis_tuser[0]),
        .I5(\m00_axis_tdata_r_reg[31]_0 [17]),
        .O(ram_reg_0_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata_r[18]_i_6 
       (.I0(DOUT7_w[18]),
        .I1(dout0_out[18]),
        .I2(s00_axis_tuser[1]),
        .I3(\m00_axis_tdata_r_reg[31] [18]),
        .I4(s00_axis_tuser[0]),
        .I5(\m00_axis_tdata_r_reg[31]_0 [18]),
        .O(ram_reg_1_10));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata_r[19]_i_6 
       (.I0(DOUT7_w[19]),
        .I1(dout0_out[19]),
        .I2(s00_axis_tuser[1]),
        .I3(\m00_axis_tdata_r_reg[31] [19]),
        .I4(s00_axis_tuser[0]),
        .I5(\m00_axis_tdata_r_reg[31]_0 [19]),
        .O(ram_reg_1_9));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata_r[1]_i_6 
       (.I0(DOUT7_w[1]),
        .I1(dout0_out[1]),
        .I2(s00_axis_tuser[1]),
        .I3(\m00_axis_tdata_r_reg[31] [1]),
        .I4(s00_axis_tuser[0]),
        .I5(\m00_axis_tdata_r_reg[31]_0 [1]),
        .O(ram_reg_0_11));
  LUT5 #(
    .INIT(32'hEFEA4540)) 
    \m00_axis_tdata_r[20]_i_1 
       (.I0(s00_axis_tuser[3]),
        .I1(\m00_axis_tdata_r[20]_i_2_n_0 ),
        .I2(s00_axis_tuser[2]),
        .I3(\m00_axis_tdata_r_reg[20] ),
        .I4(\m00_axis_tdata_r_reg[20]_0 ),
        .O(D[6]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata_r[20]_i_2 
       (.I0(DOUT7_w[20]),
        .I1(dout0_out[20]),
        .I2(s00_axis_tuser[1]),
        .I3(\m00_axis_tdata_r_reg[31] [20]),
        .I4(s00_axis_tuser[0]),
        .I5(\m00_axis_tdata_r_reg[31]_0 [20]),
        .O(\m00_axis_tdata_r[20]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata_r[21]_i_6 
       (.I0(DOUT7_w[21]),
        .I1(dout0_out[21]),
        .I2(s00_axis_tuser[1]),
        .I3(\m00_axis_tdata_r_reg[31] [21]),
        .I4(s00_axis_tuser[0]),
        .I5(\m00_axis_tdata_r_reg[31]_0 [21]),
        .O(ram_reg_1_8));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata_r[22]_i_6 
       (.I0(DOUT7_w[22]),
        .I1(dout0_out[22]),
        .I2(s00_axis_tuser[1]),
        .I3(\m00_axis_tdata_r_reg[31] [22]),
        .I4(s00_axis_tuser[0]),
        .I5(\m00_axis_tdata_r_reg[31]_0 [22]),
        .O(ram_reg_1_7));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata_r[23]_i_6 
       (.I0(DOUT7_w[23]),
        .I1(dout0_out[23]),
        .I2(s00_axis_tuser[1]),
        .I3(\m00_axis_tdata_r_reg[31] [23]),
        .I4(s00_axis_tuser[0]),
        .I5(\m00_axis_tdata_r_reg[31]_0 [23]),
        .O(ram_reg_1_6));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata_r[24]_i_6 
       (.I0(DOUT7_w[24]),
        .I1(dout0_out[24]),
        .I2(s00_axis_tuser[1]),
        .I3(\m00_axis_tdata_r_reg[31] [24]),
        .I4(s00_axis_tuser[0]),
        .I5(\m00_axis_tdata_r_reg[31]_0 [24]),
        .O(ram_reg_1_5));
  LUT5 #(
    .INIT(32'hEFEA4540)) 
    \m00_axis_tdata_r[25]_i_1 
       (.I0(s00_axis_tuser[3]),
        .I1(\m00_axis_tdata_r[25]_i_2_n_0 ),
        .I2(s00_axis_tuser[2]),
        .I3(\m00_axis_tdata_r_reg[25] ),
        .I4(\m00_axis_tdata_r_reg[25]_0 ),
        .O(D[7]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata_r[25]_i_2 
       (.I0(DOUT7_w[25]),
        .I1(dout0_out[25]),
        .I2(s00_axis_tuser[1]),
        .I3(\m00_axis_tdata_r_reg[31] [25]),
        .I4(s00_axis_tuser[0]),
        .I5(\m00_axis_tdata_r_reg[31]_0 [25]),
        .O(\m00_axis_tdata_r[25]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata_r[26]_i_6 
       (.I0(DOUT7_w[26]),
        .I1(dout0_out[26]),
        .I2(s00_axis_tuser[1]),
        .I3(\m00_axis_tdata_r_reg[31] [26]),
        .I4(s00_axis_tuser[0]),
        .I5(\m00_axis_tdata_r_reg[31]_0 [26]),
        .O(ram_reg_1_4));
  LUT5 #(
    .INIT(32'hEFEA4540)) 
    \m00_axis_tdata_r[27]_i_1 
       (.I0(s00_axis_tuser[3]),
        .I1(\m00_axis_tdata_r[27]_i_2_n_0 ),
        .I2(s00_axis_tuser[2]),
        .I3(\m00_axis_tdata_r_reg[27] ),
        .I4(\m00_axis_tdata_r_reg[27]_0 ),
        .O(D[8]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata_r[27]_i_2 
       (.I0(DOUT7_w[27]),
        .I1(dout0_out[27]),
        .I2(s00_axis_tuser[1]),
        .I3(\m00_axis_tdata_r_reg[31] [27]),
        .I4(s00_axis_tuser[0]),
        .I5(\m00_axis_tdata_r_reg[31]_0 [27]),
        .O(\m00_axis_tdata_r[27]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata_r[28]_i_6 
       (.I0(DOUT7_w[28]),
        .I1(dout0_out[28]),
        .I2(s00_axis_tuser[1]),
        .I3(\m00_axis_tdata_r_reg[31] [28]),
        .I4(s00_axis_tuser[0]),
        .I5(\m00_axis_tdata_r_reg[31]_0 [28]),
        .O(ram_reg_1_3));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata_r[29]_i_6 
       (.I0(DOUT7_w[29]),
        .I1(dout0_out[29]),
        .I2(s00_axis_tuser[1]),
        .I3(\m00_axis_tdata_r_reg[31] [29]),
        .I4(s00_axis_tuser[0]),
        .I5(\m00_axis_tdata_r_reg[31]_0 [29]),
        .O(ram_reg_1_2));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata_r[2]_i_6 
       (.I0(DOUT7_w[2]),
        .I1(dout0_out[2]),
        .I2(s00_axis_tuser[1]),
        .I3(\m00_axis_tdata_r_reg[31] [2]),
        .I4(s00_axis_tuser[0]),
        .I5(\m00_axis_tdata_r_reg[31]_0 [2]),
        .O(ram_reg_0_10));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata_r[30]_i_6 
       (.I0(DOUT7_w[30]),
        .I1(dout0_out[30]),
        .I2(s00_axis_tuser[1]),
        .I3(\m00_axis_tdata_r_reg[31] [30]),
        .I4(s00_axis_tuser[0]),
        .I5(\m00_axis_tdata_r_reg[31]_0 [30]),
        .O(ram_reg_1_1));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata_r[31]_i_6 
       (.I0(DOUT7_w[31]),
        .I1(dout0_out[31]),
        .I2(s00_axis_tuser[1]),
        .I3(\m00_axis_tdata_r_reg[31] [31]),
        .I4(s00_axis_tuser[0]),
        .I5(\m00_axis_tdata_r_reg[31]_0 [31]),
        .O(ram_reg_1_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata_r[3]_i_6 
       (.I0(DOUT7_w[3]),
        .I1(dout0_out[3]),
        .I2(s00_axis_tuser[1]),
        .I3(\m00_axis_tdata_r_reg[31] [3]),
        .I4(s00_axis_tuser[0]),
        .I5(\m00_axis_tdata_r_reg[31]_0 [3]),
        .O(ram_reg_0_9));
  LUT5 #(
    .INIT(32'hEFEA4540)) 
    \m00_axis_tdata_r[4]_i_1 
       (.I0(s00_axis_tuser[3]),
        .I1(\m00_axis_tdata_r[4]_i_2_n_0 ),
        .I2(s00_axis_tuser[2]),
        .I3(\m00_axis_tdata_r_reg[4] ),
        .I4(\m00_axis_tdata_r_reg[4]_0 ),
        .O(D[1]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata_r[4]_i_2 
       (.I0(DOUT7_w[4]),
        .I1(dout0_out[4]),
        .I2(s00_axis_tuser[1]),
        .I3(\m00_axis_tdata_r_reg[31] [4]),
        .I4(s00_axis_tuser[0]),
        .I5(\m00_axis_tdata_r_reg[31]_0 [4]),
        .O(\m00_axis_tdata_r[4]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata_r[5]_i_6 
       (.I0(DOUT7_w[5]),
        .I1(dout0_out[5]),
        .I2(s00_axis_tuser[1]),
        .I3(\m00_axis_tdata_r_reg[31] [5]),
        .I4(s00_axis_tuser[0]),
        .I5(\m00_axis_tdata_r_reg[31]_0 [5]),
        .O(ram_reg_0_8));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata_r[6]_i_6 
       (.I0(DOUT7_w[6]),
        .I1(dout0_out[6]),
        .I2(s00_axis_tuser[1]),
        .I3(\m00_axis_tdata_r_reg[31] [6]),
        .I4(s00_axis_tuser[0]),
        .I5(\m00_axis_tdata_r_reg[31]_0 [6]),
        .O(ram_reg_0_7));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata_r[7]_i_6 
       (.I0(DOUT7_w[7]),
        .I1(dout0_out[7]),
        .I2(s00_axis_tuser[1]),
        .I3(\m00_axis_tdata_r_reg[31] [7]),
        .I4(s00_axis_tuser[0]),
        .I5(\m00_axis_tdata_r_reg[31]_0 [7]),
        .O(ram_reg_0_6));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata_r[8]_i_6 
       (.I0(DOUT7_w[8]),
        .I1(dout0_out[8]),
        .I2(s00_axis_tuser[1]),
        .I3(\m00_axis_tdata_r_reg[31] [8]),
        .I4(s00_axis_tuser[0]),
        .I5(\m00_axis_tdata_r_reg[31]_0 [8]),
        .O(ram_reg_0_5));
  LUT5 #(
    .INIT(32'hEFEA4540)) 
    \m00_axis_tdata_r[9]_i_1 
       (.I0(s00_axis_tuser[3]),
        .I1(\m00_axis_tdata_r[9]_i_2_n_0 ),
        .I2(s00_axis_tuser[2]),
        .I3(\m00_axis_tdata_r_reg[9] ),
        .I4(\m00_axis_tdata_r_reg[9]_0 ),
        .O(D[2]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata_r[9]_i_2 
       (.I0(DOUT7_w[9]),
        .I1(dout0_out[9]),
        .I2(s00_axis_tuser[1]),
        .I3(\m00_axis_tdata_r_reg[31] [9]),
        .I4(s00_axis_tuser[0]),
        .I5(\m00_axis_tdata_r_reg[31]_0 [9]),
        .O(\m00_axis_tdata_r[9]_i_2_n_0 ));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p2_d16" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "61472" *) 
  (* RTL_RAM_NAME = "ram" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "2047" *) 
  (* bram_slice_begin = "0" *) 
  (* bram_slice_end = "17" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "2047" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "17" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(18),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(18),
    .WRITE_WIDTH_B(0)) 
    ram_reg_0
       (.ADDRARDADDR({1'b1,ADDRARDADDR,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CASCADEINA(1'b1),
        .CASCADEINB(1'b0),
        .CASCADEOUTA(NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED),
        .CASCADEOUTB(NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED),
        .CLKARDCLK(m00_axis_aclk),
        .CLKBWRCLK(1'b0),
        .DBITERR(NLW_ram_reg_0_DBITERR_UNCONNECTED),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,Q[15:0]}),
        .DIBDI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0,Q[17:16]}),
        .DIPBDIP({1'b1,1'b1,1'b1,1'b1}),
        .DOADO({NLW_ram_reg_0_DOADO_UNCONNECTED[31:16],DOUT7_w[15:0]}),
        .DOBDO(NLW_ram_reg_0_DOBDO_UNCONNECTED[31:0]),
        .DOPADOP({NLW_ram_reg_0_DOPADOP_UNCONNECTED[3:2],DOUT7_w[17:16]}),
        .DOPBDOP(NLW_ram_reg_0_DOPBDOP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_ram_reg_0_ECCPARITY_UNCONNECTED[7:0]),
        .ENARDEN(en),
        .ENBWREN(1'b0),
        .INJECTDBITERR(NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED),
        .INJECTSBITERR(NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED),
        .RDADDRECC(NLW_ram_reg_0_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_ram_reg_0_SBITERR_UNCONNECTED),
        .WEA({ram_reg_0_i_1__6_n_0,ram_reg_0_i_1__6_n_0,ram_reg_0_i_1__6_n_0,ram_reg_0_i_1__6_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
  LUT3 #(
    .INIT(8'h80)) 
    ram_reg_0_i_1__6
       (.I0(en),
        .I1(WE_r),
        .I2(square_r),
        .O(ram_reg_0_i_1__6_n_0));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d14" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "61472" *) 
  (* RTL_RAM_NAME = "ram" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "2047" *) 
  (* bram_slice_begin = "18" *) 
  (* bram_slice_end = "31" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "2047" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "18" *) 
  (* ram_slice_end = "31" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(18),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(18),
    .WRITE_WIDTH_B(0)) 
    ram_reg_1
       (.ADDRARDADDR({1'b1,addr,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CASCADEINA(1'b1),
        .CASCADEINB(1'b0),
        .CASCADEOUTA(NLW_ram_reg_1_CASCADEOUTA_UNCONNECTED),
        .CASCADEOUTB(NLW_ram_reg_1_CASCADEOUTB_UNCONNECTED),
        .CLKARDCLK(m00_axis_aclk),
        .CLKBWRCLK(1'b0),
        .DBITERR(NLW_ram_reg_1_DBITERR_UNCONNECTED),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,Q[31:18]}),
        .DIBDI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0,1'b0,1'b0}),
        .DIPBDIP({1'b1,1'b1,1'b1,1'b1}),
        .DOADO({NLW_ram_reg_1_DOADO_UNCONNECTED[31:14],DOUT7_w[31:18]}),
        .DOBDO(NLW_ram_reg_1_DOBDO_UNCONNECTED[31:0]),
        .DOPADOP(NLW_ram_reg_1_DOPADOP_UNCONNECTED[3:0]),
        .DOPBDOP(NLW_ram_reg_1_DOPBDOP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_ram_reg_1_ECCPARITY_UNCONNECTED[7:0]),
        .ENARDEN(en),
        .ENBWREN(1'b0),
        .INJECTDBITERR(NLW_ram_reg_1_INJECTDBITERR_UNCONNECTED),
        .INJECTSBITERR(NLW_ram_reg_1_INJECTSBITERR_UNCONNECTED),
        .RDADDRECC(NLW_ram_reg_1_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_ram_reg_1_SBITERR_UNCONNECTED),
        .WEA({ram_reg_0_i_1__6_n_0,ram_reg_0_i_1__6_n_0,ram_reg_0_i_1__6_n_0,ram_reg_0_i_1__6_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
endmodule

(* ORIG_REF_NAME = "rams_sp_rf_rst" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_7
   (dout0_out,
    m00_axis_aclk,
    Q,
    ADDRARDADDR,
    ram_reg_1_0,
    addr,
    WE_r,
    square_r);
  output [31:0]dout0_out;
  input m00_axis_aclk;
  input [0:0]Q;
  input [10:0]ADDRARDADDR;
  input [31:0]ram_reg_1_0;
  input [10:0]addr;
  input WE_r;
  input square_r;

  wire [10:0]ADDRARDADDR;
  wire [0:0]Q;
  wire WE_r;
  wire [10:0]addr;
  wire [31:0]dout0_out;
  wire m00_axis_aclk;
  wire ram_reg_0_i_1__7_n_0;
  wire [31:0]ram_reg_1_0;
  wire square_r;
  wire NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED;
  wire NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED;
  wire NLW_ram_reg_0_DBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_SBITERR_UNCONNECTED;
  wire [31:16]NLW_ram_reg_0_DOADO_UNCONNECTED;
  wire [31:0]NLW_ram_reg_0_DOBDO_UNCONNECTED;
  wire [3:2]NLW_ram_reg_0_DOPADOP_UNCONNECTED;
  wire [3:0]NLW_ram_reg_0_DOPBDOP_UNCONNECTED;
  wire [7:0]NLW_ram_reg_0_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_ram_reg_0_RDADDRECC_UNCONNECTED;
  wire NLW_ram_reg_1_CASCADEOUTA_UNCONNECTED;
  wire NLW_ram_reg_1_CASCADEOUTB_UNCONNECTED;
  wire NLW_ram_reg_1_DBITERR_UNCONNECTED;
  wire NLW_ram_reg_1_INJECTDBITERR_UNCONNECTED;
  wire NLW_ram_reg_1_INJECTSBITERR_UNCONNECTED;
  wire NLW_ram_reg_1_SBITERR_UNCONNECTED;
  wire [31:14]NLW_ram_reg_1_DOADO_UNCONNECTED;
  wire [31:0]NLW_ram_reg_1_DOBDO_UNCONNECTED;
  wire [3:0]NLW_ram_reg_1_DOPADOP_UNCONNECTED;
  wire [3:0]NLW_ram_reg_1_DOPBDOP_UNCONNECTED;
  wire [7:0]NLW_ram_reg_1_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_ram_reg_1_RDADDRECC_UNCONNECTED;

  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p2_d16" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "61472" *) 
  (* RTL_RAM_NAME = "ram" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "2047" *) 
  (* bram_slice_begin = "0" *) 
  (* bram_slice_end = "17" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "2047" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "17" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(18),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(18),
    .WRITE_WIDTH_B(0)) 
    ram_reg_0
       (.ADDRARDADDR({1'b1,ADDRARDADDR,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CASCADEINA(1'b1),
        .CASCADEINB(1'b0),
        .CASCADEOUTA(NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED),
        .CASCADEOUTB(NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED),
        .CLKARDCLK(m00_axis_aclk),
        .CLKBWRCLK(1'b0),
        .DBITERR(NLW_ram_reg_0_DBITERR_UNCONNECTED),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,ram_reg_1_0[15:0]}),
        .DIBDI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0,ram_reg_1_0[17:16]}),
        .DIPBDIP({1'b1,1'b1,1'b1,1'b1}),
        .DOADO({NLW_ram_reg_0_DOADO_UNCONNECTED[31:16],dout0_out[15:0]}),
        .DOBDO(NLW_ram_reg_0_DOBDO_UNCONNECTED[31:0]),
        .DOPADOP({NLW_ram_reg_0_DOPADOP_UNCONNECTED[3:2],dout0_out[17:16]}),
        .DOPBDOP(NLW_ram_reg_0_DOPBDOP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_ram_reg_0_ECCPARITY_UNCONNECTED[7:0]),
        .ENARDEN(Q),
        .ENBWREN(1'b0),
        .INJECTDBITERR(NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED),
        .INJECTSBITERR(NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED),
        .RDADDRECC(NLW_ram_reg_0_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_ram_reg_0_SBITERR_UNCONNECTED),
        .WEA({ram_reg_0_i_1__7_n_0,ram_reg_0_i_1__7_n_0,ram_reg_0_i_1__7_n_0,ram_reg_0_i_1__7_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
  LUT3 #(
    .INIT(8'h80)) 
    ram_reg_0_i_1__7
       (.I0(Q),
        .I1(WE_r),
        .I2(square_r),
        .O(ram_reg_0_i_1__7_n_0));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d14" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "61472" *) 
  (* RTL_RAM_NAME = "ram" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "2047" *) 
  (* bram_slice_begin = "18" *) 
  (* bram_slice_end = "31" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "2047" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "18" *) 
  (* ram_slice_end = "31" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(18),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(18),
    .WRITE_WIDTH_B(0)) 
    ram_reg_1
       (.ADDRARDADDR({1'b1,addr,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CASCADEINA(1'b1),
        .CASCADEINB(1'b0),
        .CASCADEOUTA(NLW_ram_reg_1_CASCADEOUTA_UNCONNECTED),
        .CASCADEOUTB(NLW_ram_reg_1_CASCADEOUTB_UNCONNECTED),
        .CLKARDCLK(m00_axis_aclk),
        .CLKBWRCLK(1'b0),
        .DBITERR(NLW_ram_reg_1_DBITERR_UNCONNECTED),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,ram_reg_1_0[31:18]}),
        .DIBDI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0,1'b0,1'b0}),
        .DIPBDIP({1'b1,1'b1,1'b1,1'b1}),
        .DOADO({NLW_ram_reg_1_DOADO_UNCONNECTED[31:14],dout0_out[31:18]}),
        .DOBDO(NLW_ram_reg_1_DOBDO_UNCONNECTED[31:0]),
        .DOPADOP(NLW_ram_reg_1_DOPADOP_UNCONNECTED[3:0]),
        .DOPBDOP(NLW_ram_reg_1_DOPBDOP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_ram_reg_1_ECCPARITY_UNCONNECTED[7:0]),
        .ENARDEN(Q),
        .ENBWREN(1'b0),
        .INJECTDBITERR(NLW_ram_reg_1_INJECTDBITERR_UNCONNECTED),
        .INJECTSBITERR(NLW_ram_reg_1_INJECTSBITERR_UNCONNECTED),
        .RDADDRECC(NLW_ram_reg_1_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_ram_reg_1_SBITERR_UNCONNECTED),
        .WEA({ram_reg_0_i_1__7_n_0,ram_reg_0_i_1__7_n_0,ram_reg_0_i_1__7_n_0,ram_reg_0_i_1__7_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
endmodule

(* ORIG_REF_NAME = "rams_sp_rf_rst" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_8
   (dout0_out,
    m00_axis_aclk,
    Q,
    ADDRARDADDR,
    ram_reg_1_0,
    addr,
    WE_r,
    square_r);
  output [31:0]dout0_out;
  input m00_axis_aclk;
  input [0:0]Q;
  input [10:0]ADDRARDADDR;
  input [31:0]ram_reg_1_0;
  input [10:0]addr;
  input WE_r;
  input square_r;

  wire [10:0]ADDRARDADDR;
  wire [0:0]Q;
  wire WE_r;
  wire [10:0]addr;
  wire [31:0]dout0_out;
  wire m00_axis_aclk;
  wire ram_reg_0_i_1__8_n_0;
  wire [31:0]ram_reg_1_0;
  wire square_r;
  wire NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED;
  wire NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED;
  wire NLW_ram_reg_0_DBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_SBITERR_UNCONNECTED;
  wire [31:16]NLW_ram_reg_0_DOADO_UNCONNECTED;
  wire [31:0]NLW_ram_reg_0_DOBDO_UNCONNECTED;
  wire [3:2]NLW_ram_reg_0_DOPADOP_UNCONNECTED;
  wire [3:0]NLW_ram_reg_0_DOPBDOP_UNCONNECTED;
  wire [7:0]NLW_ram_reg_0_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_ram_reg_0_RDADDRECC_UNCONNECTED;
  wire NLW_ram_reg_1_CASCADEOUTA_UNCONNECTED;
  wire NLW_ram_reg_1_CASCADEOUTB_UNCONNECTED;
  wire NLW_ram_reg_1_DBITERR_UNCONNECTED;
  wire NLW_ram_reg_1_INJECTDBITERR_UNCONNECTED;
  wire NLW_ram_reg_1_INJECTSBITERR_UNCONNECTED;
  wire NLW_ram_reg_1_SBITERR_UNCONNECTED;
  wire [31:14]NLW_ram_reg_1_DOADO_UNCONNECTED;
  wire [31:0]NLW_ram_reg_1_DOBDO_UNCONNECTED;
  wire [3:0]NLW_ram_reg_1_DOPADOP_UNCONNECTED;
  wire [3:0]NLW_ram_reg_1_DOPBDOP_UNCONNECTED;
  wire [7:0]NLW_ram_reg_1_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_ram_reg_1_RDADDRECC_UNCONNECTED;

  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p2_d16" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "61472" *) 
  (* RTL_RAM_NAME = "ram" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "2047" *) 
  (* bram_slice_begin = "0" *) 
  (* bram_slice_end = "17" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "2047" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "17" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(18),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(18),
    .WRITE_WIDTH_B(0)) 
    ram_reg_0
       (.ADDRARDADDR({1'b1,ADDRARDADDR,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CASCADEINA(1'b1),
        .CASCADEINB(1'b0),
        .CASCADEOUTA(NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED),
        .CASCADEOUTB(NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED),
        .CLKARDCLK(m00_axis_aclk),
        .CLKBWRCLK(1'b0),
        .DBITERR(NLW_ram_reg_0_DBITERR_UNCONNECTED),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,ram_reg_1_0[15:0]}),
        .DIBDI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0,ram_reg_1_0[17:16]}),
        .DIPBDIP({1'b1,1'b1,1'b1,1'b1}),
        .DOADO({NLW_ram_reg_0_DOADO_UNCONNECTED[31:16],dout0_out[15:0]}),
        .DOBDO(NLW_ram_reg_0_DOBDO_UNCONNECTED[31:0]),
        .DOPADOP({NLW_ram_reg_0_DOPADOP_UNCONNECTED[3:2],dout0_out[17:16]}),
        .DOPBDOP(NLW_ram_reg_0_DOPBDOP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_ram_reg_0_ECCPARITY_UNCONNECTED[7:0]),
        .ENARDEN(Q),
        .ENBWREN(1'b0),
        .INJECTDBITERR(NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED),
        .INJECTSBITERR(NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED),
        .RDADDRECC(NLW_ram_reg_0_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_ram_reg_0_SBITERR_UNCONNECTED),
        .WEA({ram_reg_0_i_1__8_n_0,ram_reg_0_i_1__8_n_0,ram_reg_0_i_1__8_n_0,ram_reg_0_i_1__8_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
  LUT3 #(
    .INIT(8'h80)) 
    ram_reg_0_i_1__8
       (.I0(Q),
        .I1(WE_r),
        .I2(square_r),
        .O(ram_reg_0_i_1__8_n_0));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d14" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "61472" *) 
  (* RTL_RAM_NAME = "ram" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "2047" *) 
  (* bram_slice_begin = "18" *) 
  (* bram_slice_end = "31" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "2047" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "18" *) 
  (* ram_slice_end = "31" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(18),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(18),
    .WRITE_WIDTH_B(0)) 
    ram_reg_1
       (.ADDRARDADDR({1'b1,addr,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CASCADEINA(1'b1),
        .CASCADEINB(1'b0),
        .CASCADEOUTA(NLW_ram_reg_1_CASCADEOUTA_UNCONNECTED),
        .CASCADEOUTB(NLW_ram_reg_1_CASCADEOUTB_UNCONNECTED),
        .CLKARDCLK(m00_axis_aclk),
        .CLKBWRCLK(1'b0),
        .DBITERR(NLW_ram_reg_1_DBITERR_UNCONNECTED),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,ram_reg_1_0[31:18]}),
        .DIBDI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0,1'b0,1'b0}),
        .DIPBDIP({1'b1,1'b1,1'b1,1'b1}),
        .DOADO({NLW_ram_reg_1_DOADO_UNCONNECTED[31:14],dout0_out[31:18]}),
        .DOBDO(NLW_ram_reg_1_DOBDO_UNCONNECTED[31:0]),
        .DOPADOP(NLW_ram_reg_1_DOPADOP_UNCONNECTED[3:0]),
        .DOPBDOP(NLW_ram_reg_1_DOPBDOP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_ram_reg_1_ECCPARITY_UNCONNECTED[7:0]),
        .ENARDEN(Q),
        .ENBWREN(1'b0),
        .INJECTDBITERR(NLW_ram_reg_1_INJECTDBITERR_UNCONNECTED),
        .INJECTSBITERR(NLW_ram_reg_1_INJECTSBITERR_UNCONNECTED),
        .RDADDRECC(NLW_ram_reg_1_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_ram_reg_1_SBITERR_UNCONNECTED),
        .WEA({ram_reg_0_i_1__8_n_0,ram_reg_0_i_1__8_n_0,ram_reg_0_i_1__8_n_0,ram_reg_0_i_1__8_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
endmodule

(* ORIG_REF_NAME = "rams_sp_rf_rst" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_9
   (dout0_out,
    m00_axis_aclk,
    en,
    ADDRARDADDR,
    Q,
    addr,
    WE_r,
    square_r);
  output [31:0]dout0_out;
  input m00_axis_aclk;
  input en;
  input [10:0]ADDRARDADDR;
  input [31:0]Q;
  input [10:0]addr;
  input WE_r;
  input square_r;

  wire [10:0]ADDRARDADDR;
  wire [31:0]Q;
  wire WE_r;
  wire [10:0]addr;
  wire [31:0]dout0_out;
  wire en;
  wire m00_axis_aclk;
  wire ram_reg_0_i_1__9_n_0;
  wire square_r;
  wire NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED;
  wire NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED;
  wire NLW_ram_reg_0_DBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_SBITERR_UNCONNECTED;
  wire [31:16]NLW_ram_reg_0_DOADO_UNCONNECTED;
  wire [31:0]NLW_ram_reg_0_DOBDO_UNCONNECTED;
  wire [3:2]NLW_ram_reg_0_DOPADOP_UNCONNECTED;
  wire [3:0]NLW_ram_reg_0_DOPBDOP_UNCONNECTED;
  wire [7:0]NLW_ram_reg_0_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_ram_reg_0_RDADDRECC_UNCONNECTED;
  wire NLW_ram_reg_1_CASCADEOUTA_UNCONNECTED;
  wire NLW_ram_reg_1_CASCADEOUTB_UNCONNECTED;
  wire NLW_ram_reg_1_DBITERR_UNCONNECTED;
  wire NLW_ram_reg_1_INJECTDBITERR_UNCONNECTED;
  wire NLW_ram_reg_1_INJECTSBITERR_UNCONNECTED;
  wire NLW_ram_reg_1_SBITERR_UNCONNECTED;
  wire [31:14]NLW_ram_reg_1_DOADO_UNCONNECTED;
  wire [31:0]NLW_ram_reg_1_DOBDO_UNCONNECTED;
  wire [3:0]NLW_ram_reg_1_DOPADOP_UNCONNECTED;
  wire [3:0]NLW_ram_reg_1_DOPBDOP_UNCONNECTED;
  wire [7:0]NLW_ram_reg_1_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_ram_reg_1_RDADDRECC_UNCONNECTED;

  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p2_d16" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "61472" *) 
  (* RTL_RAM_NAME = "ram" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "2047" *) 
  (* bram_slice_begin = "0" *) 
  (* bram_slice_end = "17" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "2047" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "17" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(18),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(18),
    .WRITE_WIDTH_B(0)) 
    ram_reg_0
       (.ADDRARDADDR({1'b1,ADDRARDADDR,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CASCADEINA(1'b1),
        .CASCADEINB(1'b0),
        .CASCADEOUTA(NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED),
        .CASCADEOUTB(NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED),
        .CLKARDCLK(m00_axis_aclk),
        .CLKBWRCLK(1'b0),
        .DBITERR(NLW_ram_reg_0_DBITERR_UNCONNECTED),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,Q[15:0]}),
        .DIBDI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0,Q[17:16]}),
        .DIPBDIP({1'b1,1'b1,1'b1,1'b1}),
        .DOADO({NLW_ram_reg_0_DOADO_UNCONNECTED[31:16],dout0_out[15:0]}),
        .DOBDO(NLW_ram_reg_0_DOBDO_UNCONNECTED[31:0]),
        .DOPADOP({NLW_ram_reg_0_DOPADOP_UNCONNECTED[3:2],dout0_out[17:16]}),
        .DOPBDOP(NLW_ram_reg_0_DOPBDOP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_ram_reg_0_ECCPARITY_UNCONNECTED[7:0]),
        .ENARDEN(en),
        .ENBWREN(1'b0),
        .INJECTDBITERR(NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED),
        .INJECTSBITERR(NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED),
        .RDADDRECC(NLW_ram_reg_0_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_ram_reg_0_SBITERR_UNCONNECTED),
        .WEA({ram_reg_0_i_1__9_n_0,ram_reg_0_i_1__9_n_0,ram_reg_0_i_1__9_n_0,ram_reg_0_i_1__9_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
  LUT3 #(
    .INIT(8'h80)) 
    ram_reg_0_i_1__9
       (.I0(en),
        .I1(WE_r),
        .I2(square_r),
        .O(ram_reg_0_i_1__9_n_0));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d14" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "61472" *) 
  (* RTL_RAM_NAME = "ram" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "2047" *) 
  (* bram_slice_begin = "18" *) 
  (* bram_slice_end = "31" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "2047" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "18" *) 
  (* ram_slice_end = "31" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(18),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(18),
    .WRITE_WIDTH_B(0)) 
    ram_reg_1
       (.ADDRARDADDR({1'b1,addr,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CASCADEINA(1'b1),
        .CASCADEINB(1'b0),
        .CASCADEOUTA(NLW_ram_reg_1_CASCADEOUTA_UNCONNECTED),
        .CASCADEOUTB(NLW_ram_reg_1_CASCADEOUTB_UNCONNECTED),
        .CLKARDCLK(m00_axis_aclk),
        .CLKBWRCLK(1'b0),
        .DBITERR(NLW_ram_reg_1_DBITERR_UNCONNECTED),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,Q[31:18]}),
        .DIBDI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0,1'b0,1'b0}),
        .DIPBDIP({1'b1,1'b1,1'b1,1'b1}),
        .DOADO({NLW_ram_reg_1_DOADO_UNCONNECTED[31:14],dout0_out[31:18]}),
        .DOBDO(NLW_ram_reg_1_DOBDO_UNCONNECTED[31:0]),
        .DOPADOP(NLW_ram_reg_1_DOPADOP_UNCONNECTED[3:0]),
        .DOPBDOP(NLW_ram_reg_1_DOPBDOP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_ram_reg_1_ECCPARITY_UNCONNECTED[7:0]),
        .ENARDEN(en),
        .ENBWREN(1'b0),
        .INJECTDBITERR(NLW_ram_reg_1_INJECTDBITERR_UNCONNECTED),
        .INJECTSBITERR(NLW_ram_reg_1_INJECTSBITERR_UNCONNECTED),
        .RDADDRECC(NLW_ram_reg_1_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_ram_reg_1_SBITERR_UNCONNECTED),
        .WEA({ram_reg_0_i_1__9_n_0,ram_reg_0_i_1__9_n_0,ram_reg_0_i_1__9_n_0,ram_reg_0_i_1__9_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
