// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (lin64) Build 2552052 Fri May 24 14:47:09 MDT 2019
// Date        : Mon Sep 27 23:50:29 2021
// Host        : alex-HP-Compaq-8200-Elite-CMT-PC running 64-bit Ubuntu 20.04.2 LTS
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_2_pixelReOrder_0_0_sim_netlist.v
// Design      : design_2_pixelReOrder_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7z020clg484-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_2_pixelReOrder_0_0,pixelReOrder_v1_0,{}" *) (* DowngradeIPIdentifiedWarnings = "yes" *) (* X_CORE_INFO = "pixelReOrder_v1_0,Vivado 2019.1" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (m00_axis_aclk,
    m00_axis_aresetn,
    m00_axis_tvalid,
    m00_axis_tdata,
    m00_axis_tstrb,
    m00_axis_tlast,
    m00_axis_tready,
    s00_axis_aclk,
    s00_axis_aresetn,
    s00_axis_tready,
    s00_axis_tdata,
    s00_axis_tuser,
    s00_axis_tstrb,
    s00_axis_tlast,
    s00_axis_tvalid);
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 M00_AXIS_CLK CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME M00_AXIS_CLK, ASSOCIATED_BUSIF M00_AXIS, ASSOCIATED_RESET m00_axis_aresetn, FREQ_HZ 200000000, PHASE 0.000, CLK_DOMAIN design_2_processing_system7_0_0_FCLK_CLK0, INSERT_VIP 0" *) input m00_axis_aclk;
  (* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 M00_AXIS_RST RST" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME M00_AXIS_RST, POLARITY ACTIVE_LOW, INSERT_VIP 0" *) input m00_axis_aresetn;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 M00_AXIS TVALID" *) output m00_axis_tvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 M00_AXIS TDATA" *) output [31:0]m00_axis_tdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 M00_AXIS TSTRB" *) output [3:0]m00_axis_tstrb;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 M00_AXIS TLAST" *) output m00_axis_tlast;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 M00_AXIS TREADY" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME M00_AXIS, WIZ_DATA_WIDTH 32, TDATA_NUM_BYTES 4, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 1, HAS_TKEEP 0, HAS_TLAST 1, FREQ_HZ 200000000, PHASE 0.000, CLK_DOMAIN design_2_processing_system7_0_0_FCLK_CLK0, LAYERED_METADATA undef, INSERT_VIP 0" *) input m00_axis_tready;
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 S00_AXIS_CLK CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME S00_AXIS_CLK, ASSOCIATED_BUSIF S00_AXIS, ASSOCIATED_RESET s00_axis_aresetn, FREQ_HZ 200000000, PHASE 0.000, CLK_DOMAIN design_2_processing_system7_0_0_FCLK_CLK0, INSERT_VIP 0" *) input s00_axis_aclk;
  (* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 S00_AXIS_RST RST" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME S00_AXIS_RST, POLARITY ACTIVE_LOW, INSERT_VIP 0" *) input s00_axis_aresetn;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 S00_AXIS TREADY" *) output s00_axis_tready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 S00_AXIS TDATA" *) input [31:0]s00_axis_tdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 S00_AXIS TUSER" *) input [47:0]s00_axis_tuser;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 S00_AXIS TSTRB" *) input [3:0]s00_axis_tstrb;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 S00_AXIS TLAST" *) input s00_axis_tlast;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 S00_AXIS TVALID" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME S00_AXIS, WIZ_DATA_WIDTH 32, TDATA_NUM_BYTES 4, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 48, HAS_TREADY 1, HAS_TSTRB 1, HAS_TKEEP 0, HAS_TLAST 1, FREQ_HZ 200000000, PHASE 0.000, CLK_DOMAIN design_2_processing_system7_0_0_FCLK_CLK0, LAYERED_METADATA undef, INSERT_VIP 0" *) input s00_axis_tvalid;

  wire \<const1> ;
  wire m00_axis_aclk;
  wire [31:0]m00_axis_tdata;
  wire m00_axis_tvalid;
  wire p_0_in;
  wire [31:0]s00_axis_tdata;
  wire [47:0]s00_axis_tuser;
  wire s00_axis_tvalid;

  assign m00_axis_tlast = \<const1> ;
  assign s00_axis_tready = \<const1> ;
  VCC VCC
       (.P(\<const1> ));
  LUT1 #(
    .INIT(2'h1)) 
    i_105
       (.I0(s00_axis_tvalid),
        .O(p_0_in));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_pixelReOrder_v1_0 inst
       (.m00_axis_aclk(m00_axis_aclk),
        .m00_axis_tdata(m00_axis_tdata),
        .m00_axis_tvalid(m00_axis_tvalid),
        .s00_axis_tdata(s00_axis_tdata),
        .s00_axis_tuser({s00_axis_tuser[31:16],s00_axis_tuser[10:0]}),
        .s00_axis_tvalid(s00_axis_tvalid));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_multiplexer
   (ram_reg_0,
    ram_reg_0_0,
    ram_reg_0_1,
    ram_reg_0_2,
    ram_reg_0_3,
    ram_reg_0_4,
    ram_reg_0_5,
    ram_reg_0_6,
    ram_reg_0_7,
    ram_reg_0_8,
    ram_reg_0_9,
    ram_reg_0_10,
    ram_reg_0_11,
    ram_reg_0_12,
    ram_reg_0_13,
    ram_reg_0_14,
    ram_reg_0_15,
    ram_reg_0_16,
    ram_reg_0_17,
    ram_reg_0_18,
    ram_reg_0_19,
    ram_reg_0_20,
    ram_reg_0_21,
    ram_reg_0_22,
    ram_reg_0_23,
    ram_reg_0_24,
    ram_reg_0_25,
    ram_reg_0_26,
    ram_reg_0_27,
    ram_reg_0_28,
    ram_reg_0_29,
    ram_reg_0_30,
    ram_reg_0_31,
    ram_reg_0_32,
    ram_reg_0_33,
    ram_reg_0_34,
    ram_reg_1,
    ram_reg_1_0,
    ram_reg_1_1,
    ram_reg_1_2,
    ram_reg_1_3,
    ram_reg_1_4,
    ram_reg_1_5,
    ram_reg_1_6,
    ram_reg_1_7,
    ram_reg_1_8,
    ram_reg_1_9,
    ram_reg_1_10,
    ram_reg_1_11,
    ram_reg_1_12,
    ram_reg_1_13,
    ram_reg_1_14,
    ram_reg_1_15,
    ram_reg_1_16,
    ram_reg_1_17,
    ram_reg_1_18,
    ram_reg_1_19,
    ram_reg_1_20,
    ram_reg_1_21,
    ram_reg_1_22,
    ram_reg_1_23,
    ram_reg_1_24,
    ram_reg_1_25,
    ram_reg_1_26,
    dout0_out,
    \m00_axis_tdata[31] ,
    \m00_axis_tdata[0] ,
    \m00_axis_tdata[31]_0 ,
    \m00_axis_tdata[0]_0 ,
    \m00_axis_tdata[31]_1 ,
    \m00_axis_tdata[31]_2 ,
    \m00_axis_tdata[31]_3 ,
    \m00_axis_tdata[31]_4 );
  output ram_reg_0;
  output ram_reg_0_0;
  output ram_reg_0_1;
  output ram_reg_0_2;
  output ram_reg_0_3;
  output ram_reg_0_4;
  output ram_reg_0_5;
  output ram_reg_0_6;
  output ram_reg_0_7;
  output ram_reg_0_8;
  output ram_reg_0_9;
  output ram_reg_0_10;
  output ram_reg_0_11;
  output ram_reg_0_12;
  output ram_reg_0_13;
  output ram_reg_0_14;
  output ram_reg_0_15;
  output ram_reg_0_16;
  output ram_reg_0_17;
  output ram_reg_0_18;
  output ram_reg_0_19;
  output ram_reg_0_20;
  output ram_reg_0_21;
  output ram_reg_0_22;
  output ram_reg_0_23;
  output ram_reg_0_24;
  output ram_reg_0_25;
  output ram_reg_0_26;
  output ram_reg_0_27;
  output ram_reg_0_28;
  output ram_reg_0_29;
  output ram_reg_0_30;
  output ram_reg_0_31;
  output ram_reg_0_32;
  output ram_reg_0_33;
  output ram_reg_0_34;
  output ram_reg_1;
  output ram_reg_1_0;
  output ram_reg_1_1;
  output ram_reg_1_2;
  output ram_reg_1_3;
  output ram_reg_1_4;
  output ram_reg_1_5;
  output ram_reg_1_6;
  output ram_reg_1_7;
  output ram_reg_1_8;
  output ram_reg_1_9;
  output ram_reg_1_10;
  output ram_reg_1_11;
  output ram_reg_1_12;
  output ram_reg_1_13;
  output ram_reg_1_14;
  output ram_reg_1_15;
  output ram_reg_1_16;
  output ram_reg_1_17;
  output ram_reg_1_18;
  output ram_reg_1_19;
  output ram_reg_1_20;
  output ram_reg_1_21;
  output ram_reg_1_22;
  output ram_reg_1_23;
  output ram_reg_1_24;
  output ram_reg_1_25;
  output ram_reg_1_26;
  input [31:0]dout0_out;
  input [31:0]\m00_axis_tdata[31] ;
  input \m00_axis_tdata[0] ;
  input [31:0]\m00_axis_tdata[31]_0 ;
  input \m00_axis_tdata[0]_0 ;
  input [31:0]\m00_axis_tdata[31]_1 ;
  input [31:0]\m00_axis_tdata[31]_2 ;
  input [31:0]\m00_axis_tdata[31]_3 ;
  input [31:0]\m00_axis_tdata[31]_4 ;

  wire [31:0]dout0_out;
  wire \m00_axis_tdata[0] ;
  wire \m00_axis_tdata[0]_0 ;
  wire [31:0]\m00_axis_tdata[31] ;
  wire [31:0]\m00_axis_tdata[31]_0 ;
  wire [31:0]\m00_axis_tdata[31]_1 ;
  wire [31:0]\m00_axis_tdata[31]_2 ;
  wire [31:0]\m00_axis_tdata[31]_3 ;
  wire [31:0]\m00_axis_tdata[31]_4 ;
  wire ram_reg_0;
  wire ram_reg_0_0;
  wire ram_reg_0_1;
  wire ram_reg_0_10;
  wire ram_reg_0_11;
  wire ram_reg_0_12;
  wire ram_reg_0_13;
  wire ram_reg_0_14;
  wire ram_reg_0_15;
  wire ram_reg_0_16;
  wire ram_reg_0_17;
  wire ram_reg_0_18;
  wire ram_reg_0_19;
  wire ram_reg_0_2;
  wire ram_reg_0_20;
  wire ram_reg_0_21;
  wire ram_reg_0_22;
  wire ram_reg_0_23;
  wire ram_reg_0_24;
  wire ram_reg_0_25;
  wire ram_reg_0_26;
  wire ram_reg_0_27;
  wire ram_reg_0_28;
  wire ram_reg_0_29;
  wire ram_reg_0_3;
  wire ram_reg_0_30;
  wire ram_reg_0_31;
  wire ram_reg_0_32;
  wire ram_reg_0_33;
  wire ram_reg_0_34;
  wire ram_reg_0_4;
  wire ram_reg_0_5;
  wire ram_reg_0_6;
  wire ram_reg_0_7;
  wire ram_reg_0_8;
  wire ram_reg_0_9;
  wire ram_reg_1;
  wire ram_reg_1_0;
  wire ram_reg_1_1;
  wire ram_reg_1_10;
  wire ram_reg_1_11;
  wire ram_reg_1_12;
  wire ram_reg_1_13;
  wire ram_reg_1_14;
  wire ram_reg_1_15;
  wire ram_reg_1_16;
  wire ram_reg_1_17;
  wire ram_reg_1_18;
  wire ram_reg_1_19;
  wire ram_reg_1_2;
  wire ram_reg_1_20;
  wire ram_reg_1_21;
  wire ram_reg_1_22;
  wire ram_reg_1_23;
  wire ram_reg_1_24;
  wire ram_reg_1_25;
  wire ram_reg_1_26;
  wire ram_reg_1_3;
  wire ram_reg_1_4;
  wire ram_reg_1_5;
  wire ram_reg_1_6;
  wire ram_reg_1_7;
  wire ram_reg_1_8;
  wire ram_reg_1_9;

  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata[0]_INST_0_i_1 
       (.I0(dout0_out[0]),
        .I1(\m00_axis_tdata[31] [0]),
        .I2(\m00_axis_tdata[0] ),
        .I3(\m00_axis_tdata[31]_0 [0]),
        .I4(\m00_axis_tdata[0]_0 ),
        .I5(\m00_axis_tdata[31]_1 [0]),
        .O(ram_reg_0));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \m00_axis_tdata[0]_INST_0_i_2 
       (.I0(\m00_axis_tdata[31]_2 [0]),
        .I1(\m00_axis_tdata[0] ),
        .I2(\m00_axis_tdata[31]_3 [0]),
        .I3(\m00_axis_tdata[0]_0 ),
        .I4(\m00_axis_tdata[31]_4 [0]),
        .O(ram_reg_0_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata[10]_INST_0_i_1 
       (.I0(dout0_out[10]),
        .I1(\m00_axis_tdata[31] [10]),
        .I2(\m00_axis_tdata[0] ),
        .I3(\m00_axis_tdata[31]_0 [10]),
        .I4(\m00_axis_tdata[0]_0 ),
        .I5(\m00_axis_tdata[31]_1 [10]),
        .O(ram_reg_0_19));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \m00_axis_tdata[10]_INST_0_i_2 
       (.I0(\m00_axis_tdata[31]_2 [10]),
        .I1(\m00_axis_tdata[0] ),
        .I2(\m00_axis_tdata[31]_3 [10]),
        .I3(\m00_axis_tdata[0]_0 ),
        .I4(\m00_axis_tdata[31]_4 [10]),
        .O(ram_reg_0_20));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata[11]_INST_0_i_1 
       (.I0(dout0_out[11]),
        .I1(\m00_axis_tdata[31] [11]),
        .I2(\m00_axis_tdata[0] ),
        .I3(\m00_axis_tdata[31]_0 [11]),
        .I4(\m00_axis_tdata[0]_0 ),
        .I5(\m00_axis_tdata[31]_1 [11]),
        .O(ram_reg_0_21));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \m00_axis_tdata[11]_INST_0_i_2 
       (.I0(\m00_axis_tdata[31]_2 [11]),
        .I1(\m00_axis_tdata[0] ),
        .I2(\m00_axis_tdata[31]_3 [11]),
        .I3(\m00_axis_tdata[0]_0 ),
        .I4(\m00_axis_tdata[31]_4 [11]),
        .O(ram_reg_0_22));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata[12]_INST_0_i_1 
       (.I0(dout0_out[12]),
        .I1(\m00_axis_tdata[31] [12]),
        .I2(\m00_axis_tdata[0] ),
        .I3(\m00_axis_tdata[31]_0 [12]),
        .I4(\m00_axis_tdata[0]_0 ),
        .I5(\m00_axis_tdata[31]_1 [12]),
        .O(ram_reg_0_23));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \m00_axis_tdata[12]_INST_0_i_2 
       (.I0(\m00_axis_tdata[31]_2 [12]),
        .I1(\m00_axis_tdata[0] ),
        .I2(\m00_axis_tdata[31]_3 [12]),
        .I3(\m00_axis_tdata[0]_0 ),
        .I4(\m00_axis_tdata[31]_4 [12]),
        .O(ram_reg_0_24));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata[13]_INST_0_i_1 
       (.I0(dout0_out[13]),
        .I1(\m00_axis_tdata[31] [13]),
        .I2(\m00_axis_tdata[0] ),
        .I3(\m00_axis_tdata[31]_0 [13]),
        .I4(\m00_axis_tdata[0]_0 ),
        .I5(\m00_axis_tdata[31]_1 [13]),
        .O(ram_reg_0_25));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \m00_axis_tdata[13]_INST_0_i_2 
       (.I0(\m00_axis_tdata[31]_2 [13]),
        .I1(\m00_axis_tdata[0] ),
        .I2(\m00_axis_tdata[31]_3 [13]),
        .I3(\m00_axis_tdata[0]_0 ),
        .I4(\m00_axis_tdata[31]_4 [13]),
        .O(ram_reg_0_26));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata[14]_INST_0_i_1 
       (.I0(dout0_out[14]),
        .I1(\m00_axis_tdata[31] [14]),
        .I2(\m00_axis_tdata[0] ),
        .I3(\m00_axis_tdata[31]_0 [14]),
        .I4(\m00_axis_tdata[0]_0 ),
        .I5(\m00_axis_tdata[31]_1 [14]),
        .O(ram_reg_0_27));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \m00_axis_tdata[14]_INST_0_i_2 
       (.I0(\m00_axis_tdata[31]_2 [14]),
        .I1(\m00_axis_tdata[0] ),
        .I2(\m00_axis_tdata[31]_3 [14]),
        .I3(\m00_axis_tdata[0]_0 ),
        .I4(\m00_axis_tdata[31]_4 [14]),
        .O(ram_reg_0_28));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata[15]_INST_0_i_1 
       (.I0(dout0_out[15]),
        .I1(\m00_axis_tdata[31] [15]),
        .I2(\m00_axis_tdata[0] ),
        .I3(\m00_axis_tdata[31]_0 [15]),
        .I4(\m00_axis_tdata[0]_0 ),
        .I5(\m00_axis_tdata[31]_1 [15]),
        .O(ram_reg_0_29));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \m00_axis_tdata[15]_INST_0_i_2 
       (.I0(\m00_axis_tdata[31]_2 [15]),
        .I1(\m00_axis_tdata[0] ),
        .I2(\m00_axis_tdata[31]_3 [15]),
        .I3(\m00_axis_tdata[0]_0 ),
        .I4(\m00_axis_tdata[31]_4 [15]),
        .O(ram_reg_0_30));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata[16]_INST_0_i_1 
       (.I0(dout0_out[16]),
        .I1(\m00_axis_tdata[31] [16]),
        .I2(\m00_axis_tdata[0] ),
        .I3(\m00_axis_tdata[31]_0 [16]),
        .I4(\m00_axis_tdata[0]_0 ),
        .I5(\m00_axis_tdata[31]_1 [16]),
        .O(ram_reg_0_31));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \m00_axis_tdata[16]_INST_0_i_2 
       (.I0(\m00_axis_tdata[31]_2 [16]),
        .I1(\m00_axis_tdata[0] ),
        .I2(\m00_axis_tdata[31]_3 [16]),
        .I3(\m00_axis_tdata[0]_0 ),
        .I4(\m00_axis_tdata[31]_4 [16]),
        .O(ram_reg_0_32));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata[17]_INST_0_i_1 
       (.I0(dout0_out[17]),
        .I1(\m00_axis_tdata[31] [17]),
        .I2(\m00_axis_tdata[0] ),
        .I3(\m00_axis_tdata[31]_0 [17]),
        .I4(\m00_axis_tdata[0]_0 ),
        .I5(\m00_axis_tdata[31]_1 [17]),
        .O(ram_reg_0_33));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \m00_axis_tdata[17]_INST_0_i_2 
       (.I0(\m00_axis_tdata[31]_2 [17]),
        .I1(\m00_axis_tdata[0] ),
        .I2(\m00_axis_tdata[31]_3 [17]),
        .I3(\m00_axis_tdata[0]_0 ),
        .I4(\m00_axis_tdata[31]_4 [17]),
        .O(ram_reg_0_34));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata[18]_INST_0_i_1 
       (.I0(dout0_out[18]),
        .I1(\m00_axis_tdata[31] [18]),
        .I2(\m00_axis_tdata[0] ),
        .I3(\m00_axis_tdata[31]_0 [18]),
        .I4(\m00_axis_tdata[0]_0 ),
        .I5(\m00_axis_tdata[31]_1 [18]),
        .O(ram_reg_1));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \m00_axis_tdata[18]_INST_0_i_2 
       (.I0(\m00_axis_tdata[31]_2 [18]),
        .I1(\m00_axis_tdata[0] ),
        .I2(\m00_axis_tdata[31]_3 [18]),
        .I3(\m00_axis_tdata[0]_0 ),
        .I4(\m00_axis_tdata[31]_4 [18]),
        .O(ram_reg_1_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata[19]_INST_0_i_1 
       (.I0(dout0_out[19]),
        .I1(\m00_axis_tdata[31] [19]),
        .I2(\m00_axis_tdata[0] ),
        .I3(\m00_axis_tdata[31]_0 [19]),
        .I4(\m00_axis_tdata[0]_0 ),
        .I5(\m00_axis_tdata[31]_1 [19]),
        .O(ram_reg_1_1));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \m00_axis_tdata[19]_INST_0_i_2 
       (.I0(\m00_axis_tdata[31]_2 [19]),
        .I1(\m00_axis_tdata[0] ),
        .I2(\m00_axis_tdata[31]_3 [19]),
        .I3(\m00_axis_tdata[0]_0 ),
        .I4(\m00_axis_tdata[31]_4 [19]),
        .O(ram_reg_1_2));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata[1]_INST_0_i_1 
       (.I0(dout0_out[1]),
        .I1(\m00_axis_tdata[31] [1]),
        .I2(\m00_axis_tdata[0] ),
        .I3(\m00_axis_tdata[31]_0 [1]),
        .I4(\m00_axis_tdata[0]_0 ),
        .I5(\m00_axis_tdata[31]_1 [1]),
        .O(ram_reg_0_1));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \m00_axis_tdata[1]_INST_0_i_2 
       (.I0(\m00_axis_tdata[31]_2 [1]),
        .I1(\m00_axis_tdata[0] ),
        .I2(\m00_axis_tdata[31]_3 [1]),
        .I3(\m00_axis_tdata[0]_0 ),
        .I4(\m00_axis_tdata[31]_4 [1]),
        .O(ram_reg_0_2));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata[20]_INST_0_i_1 
       (.I0(dout0_out[20]),
        .I1(\m00_axis_tdata[31] [20]),
        .I2(\m00_axis_tdata[0] ),
        .I3(\m00_axis_tdata[31]_0 [20]),
        .I4(\m00_axis_tdata[0]_0 ),
        .I5(\m00_axis_tdata[31]_1 [20]),
        .O(ram_reg_1_3));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \m00_axis_tdata[20]_INST_0_i_2 
       (.I0(\m00_axis_tdata[31]_2 [20]),
        .I1(\m00_axis_tdata[0] ),
        .I2(\m00_axis_tdata[31]_3 [20]),
        .I3(\m00_axis_tdata[0]_0 ),
        .I4(\m00_axis_tdata[31]_4 [20]),
        .O(ram_reg_1_4));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata[21]_INST_0_i_1 
       (.I0(dout0_out[21]),
        .I1(\m00_axis_tdata[31] [21]),
        .I2(\m00_axis_tdata[0] ),
        .I3(\m00_axis_tdata[31]_0 [21]),
        .I4(\m00_axis_tdata[0]_0 ),
        .I5(\m00_axis_tdata[31]_1 [21]),
        .O(ram_reg_1_5));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \m00_axis_tdata[21]_INST_0_i_2 
       (.I0(\m00_axis_tdata[31]_2 [21]),
        .I1(\m00_axis_tdata[0] ),
        .I2(\m00_axis_tdata[31]_3 [21]),
        .I3(\m00_axis_tdata[0]_0 ),
        .I4(\m00_axis_tdata[31]_4 [21]),
        .O(ram_reg_1_6));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata[22]_INST_0_i_1 
       (.I0(dout0_out[22]),
        .I1(\m00_axis_tdata[31] [22]),
        .I2(\m00_axis_tdata[0] ),
        .I3(\m00_axis_tdata[31]_0 [22]),
        .I4(\m00_axis_tdata[0]_0 ),
        .I5(\m00_axis_tdata[31]_1 [22]),
        .O(ram_reg_1_7));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \m00_axis_tdata[22]_INST_0_i_2 
       (.I0(\m00_axis_tdata[31]_2 [22]),
        .I1(\m00_axis_tdata[0] ),
        .I2(\m00_axis_tdata[31]_3 [22]),
        .I3(\m00_axis_tdata[0]_0 ),
        .I4(\m00_axis_tdata[31]_4 [22]),
        .O(ram_reg_1_8));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata[23]_INST_0_i_1 
       (.I0(dout0_out[23]),
        .I1(\m00_axis_tdata[31] [23]),
        .I2(\m00_axis_tdata[0] ),
        .I3(\m00_axis_tdata[31]_0 [23]),
        .I4(\m00_axis_tdata[0]_0 ),
        .I5(\m00_axis_tdata[31]_1 [23]),
        .O(ram_reg_1_9));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \m00_axis_tdata[23]_INST_0_i_2 
       (.I0(\m00_axis_tdata[31]_2 [23]),
        .I1(\m00_axis_tdata[0] ),
        .I2(\m00_axis_tdata[31]_3 [23]),
        .I3(\m00_axis_tdata[0]_0 ),
        .I4(\m00_axis_tdata[31]_4 [23]),
        .O(ram_reg_1_10));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata[24]_INST_0_i_1 
       (.I0(dout0_out[24]),
        .I1(\m00_axis_tdata[31] [24]),
        .I2(\m00_axis_tdata[0] ),
        .I3(\m00_axis_tdata[31]_0 [24]),
        .I4(\m00_axis_tdata[0]_0 ),
        .I5(\m00_axis_tdata[31]_1 [24]),
        .O(ram_reg_1_11));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \m00_axis_tdata[24]_INST_0_i_2 
       (.I0(\m00_axis_tdata[31]_2 [24]),
        .I1(\m00_axis_tdata[0] ),
        .I2(\m00_axis_tdata[31]_3 [24]),
        .I3(\m00_axis_tdata[0]_0 ),
        .I4(\m00_axis_tdata[31]_4 [24]),
        .O(ram_reg_1_12));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata[25]_INST_0_i_1 
       (.I0(dout0_out[25]),
        .I1(\m00_axis_tdata[31] [25]),
        .I2(\m00_axis_tdata[0] ),
        .I3(\m00_axis_tdata[31]_0 [25]),
        .I4(\m00_axis_tdata[0]_0 ),
        .I5(\m00_axis_tdata[31]_1 [25]),
        .O(ram_reg_1_13));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \m00_axis_tdata[25]_INST_0_i_2 
       (.I0(\m00_axis_tdata[31]_2 [25]),
        .I1(\m00_axis_tdata[0] ),
        .I2(\m00_axis_tdata[31]_3 [25]),
        .I3(\m00_axis_tdata[0]_0 ),
        .I4(\m00_axis_tdata[31]_4 [25]),
        .O(ram_reg_1_14));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata[26]_INST_0_i_1 
       (.I0(dout0_out[26]),
        .I1(\m00_axis_tdata[31] [26]),
        .I2(\m00_axis_tdata[0] ),
        .I3(\m00_axis_tdata[31]_0 [26]),
        .I4(\m00_axis_tdata[0]_0 ),
        .I5(\m00_axis_tdata[31]_1 [26]),
        .O(ram_reg_1_15));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \m00_axis_tdata[26]_INST_0_i_2 
       (.I0(\m00_axis_tdata[31]_2 [26]),
        .I1(\m00_axis_tdata[0] ),
        .I2(\m00_axis_tdata[31]_3 [26]),
        .I3(\m00_axis_tdata[0]_0 ),
        .I4(\m00_axis_tdata[31]_4 [26]),
        .O(ram_reg_1_16));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata[27]_INST_0_i_1 
       (.I0(dout0_out[27]),
        .I1(\m00_axis_tdata[31] [27]),
        .I2(\m00_axis_tdata[0] ),
        .I3(\m00_axis_tdata[31]_0 [27]),
        .I4(\m00_axis_tdata[0]_0 ),
        .I5(\m00_axis_tdata[31]_1 [27]),
        .O(ram_reg_1_17));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \m00_axis_tdata[27]_INST_0_i_2 
       (.I0(\m00_axis_tdata[31]_2 [27]),
        .I1(\m00_axis_tdata[0] ),
        .I2(\m00_axis_tdata[31]_3 [27]),
        .I3(\m00_axis_tdata[0]_0 ),
        .I4(\m00_axis_tdata[31]_4 [27]),
        .O(ram_reg_1_18));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata[28]_INST_0_i_1 
       (.I0(dout0_out[28]),
        .I1(\m00_axis_tdata[31] [28]),
        .I2(\m00_axis_tdata[0] ),
        .I3(\m00_axis_tdata[31]_0 [28]),
        .I4(\m00_axis_tdata[0]_0 ),
        .I5(\m00_axis_tdata[31]_1 [28]),
        .O(ram_reg_1_19));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \m00_axis_tdata[28]_INST_0_i_2 
       (.I0(\m00_axis_tdata[31]_2 [28]),
        .I1(\m00_axis_tdata[0] ),
        .I2(\m00_axis_tdata[31]_3 [28]),
        .I3(\m00_axis_tdata[0]_0 ),
        .I4(\m00_axis_tdata[31]_4 [28]),
        .O(ram_reg_1_20));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata[29]_INST_0_i_1 
       (.I0(dout0_out[29]),
        .I1(\m00_axis_tdata[31] [29]),
        .I2(\m00_axis_tdata[0] ),
        .I3(\m00_axis_tdata[31]_0 [29]),
        .I4(\m00_axis_tdata[0]_0 ),
        .I5(\m00_axis_tdata[31]_1 [29]),
        .O(ram_reg_1_21));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \m00_axis_tdata[29]_INST_0_i_2 
       (.I0(\m00_axis_tdata[31]_2 [29]),
        .I1(\m00_axis_tdata[0] ),
        .I2(\m00_axis_tdata[31]_3 [29]),
        .I3(\m00_axis_tdata[0]_0 ),
        .I4(\m00_axis_tdata[31]_4 [29]),
        .O(ram_reg_1_22));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata[2]_INST_0_i_1 
       (.I0(dout0_out[2]),
        .I1(\m00_axis_tdata[31] [2]),
        .I2(\m00_axis_tdata[0] ),
        .I3(\m00_axis_tdata[31]_0 [2]),
        .I4(\m00_axis_tdata[0]_0 ),
        .I5(\m00_axis_tdata[31]_1 [2]),
        .O(ram_reg_0_3));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \m00_axis_tdata[2]_INST_0_i_2 
       (.I0(\m00_axis_tdata[31]_2 [2]),
        .I1(\m00_axis_tdata[0] ),
        .I2(\m00_axis_tdata[31]_3 [2]),
        .I3(\m00_axis_tdata[0]_0 ),
        .I4(\m00_axis_tdata[31]_4 [2]),
        .O(ram_reg_0_4));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata[30]_INST_0_i_1 
       (.I0(dout0_out[30]),
        .I1(\m00_axis_tdata[31] [30]),
        .I2(\m00_axis_tdata[0] ),
        .I3(\m00_axis_tdata[31]_0 [30]),
        .I4(\m00_axis_tdata[0]_0 ),
        .I5(\m00_axis_tdata[31]_1 [30]),
        .O(ram_reg_1_23));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \m00_axis_tdata[30]_INST_0_i_2 
       (.I0(\m00_axis_tdata[31]_2 [30]),
        .I1(\m00_axis_tdata[0] ),
        .I2(\m00_axis_tdata[31]_3 [30]),
        .I3(\m00_axis_tdata[0]_0 ),
        .I4(\m00_axis_tdata[31]_4 [30]),
        .O(ram_reg_1_24));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata[31]_INST_0_i_1 
       (.I0(dout0_out[31]),
        .I1(\m00_axis_tdata[31] [31]),
        .I2(\m00_axis_tdata[0] ),
        .I3(\m00_axis_tdata[31]_0 [31]),
        .I4(\m00_axis_tdata[0]_0 ),
        .I5(\m00_axis_tdata[31]_1 [31]),
        .O(ram_reg_1_25));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \m00_axis_tdata[31]_INST_0_i_2 
       (.I0(\m00_axis_tdata[31]_2 [31]),
        .I1(\m00_axis_tdata[0] ),
        .I2(\m00_axis_tdata[31]_3 [31]),
        .I3(\m00_axis_tdata[0]_0 ),
        .I4(\m00_axis_tdata[31]_4 [31]),
        .O(ram_reg_1_26));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata[3]_INST_0_i_1 
       (.I0(dout0_out[3]),
        .I1(\m00_axis_tdata[31] [3]),
        .I2(\m00_axis_tdata[0] ),
        .I3(\m00_axis_tdata[31]_0 [3]),
        .I4(\m00_axis_tdata[0]_0 ),
        .I5(\m00_axis_tdata[31]_1 [3]),
        .O(ram_reg_0_5));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \m00_axis_tdata[3]_INST_0_i_2 
       (.I0(\m00_axis_tdata[31]_2 [3]),
        .I1(\m00_axis_tdata[0] ),
        .I2(\m00_axis_tdata[31]_3 [3]),
        .I3(\m00_axis_tdata[0]_0 ),
        .I4(\m00_axis_tdata[31]_4 [3]),
        .O(ram_reg_0_6));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata[4]_INST_0_i_1 
       (.I0(dout0_out[4]),
        .I1(\m00_axis_tdata[31] [4]),
        .I2(\m00_axis_tdata[0] ),
        .I3(\m00_axis_tdata[31]_0 [4]),
        .I4(\m00_axis_tdata[0]_0 ),
        .I5(\m00_axis_tdata[31]_1 [4]),
        .O(ram_reg_0_7));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \m00_axis_tdata[4]_INST_0_i_2 
       (.I0(\m00_axis_tdata[31]_2 [4]),
        .I1(\m00_axis_tdata[0] ),
        .I2(\m00_axis_tdata[31]_3 [4]),
        .I3(\m00_axis_tdata[0]_0 ),
        .I4(\m00_axis_tdata[31]_4 [4]),
        .O(ram_reg_0_8));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata[5]_INST_0_i_1 
       (.I0(dout0_out[5]),
        .I1(\m00_axis_tdata[31] [5]),
        .I2(\m00_axis_tdata[0] ),
        .I3(\m00_axis_tdata[31]_0 [5]),
        .I4(\m00_axis_tdata[0]_0 ),
        .I5(\m00_axis_tdata[31]_1 [5]),
        .O(ram_reg_0_9));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \m00_axis_tdata[5]_INST_0_i_2 
       (.I0(\m00_axis_tdata[31]_2 [5]),
        .I1(\m00_axis_tdata[0] ),
        .I2(\m00_axis_tdata[31]_3 [5]),
        .I3(\m00_axis_tdata[0]_0 ),
        .I4(\m00_axis_tdata[31]_4 [5]),
        .O(ram_reg_0_10));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata[6]_INST_0_i_1 
       (.I0(dout0_out[6]),
        .I1(\m00_axis_tdata[31] [6]),
        .I2(\m00_axis_tdata[0] ),
        .I3(\m00_axis_tdata[31]_0 [6]),
        .I4(\m00_axis_tdata[0]_0 ),
        .I5(\m00_axis_tdata[31]_1 [6]),
        .O(ram_reg_0_11));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \m00_axis_tdata[6]_INST_0_i_2 
       (.I0(\m00_axis_tdata[31]_2 [6]),
        .I1(\m00_axis_tdata[0] ),
        .I2(\m00_axis_tdata[31]_3 [6]),
        .I3(\m00_axis_tdata[0]_0 ),
        .I4(\m00_axis_tdata[31]_4 [6]),
        .O(ram_reg_0_12));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata[7]_INST_0_i_1 
       (.I0(dout0_out[7]),
        .I1(\m00_axis_tdata[31] [7]),
        .I2(\m00_axis_tdata[0] ),
        .I3(\m00_axis_tdata[31]_0 [7]),
        .I4(\m00_axis_tdata[0]_0 ),
        .I5(\m00_axis_tdata[31]_1 [7]),
        .O(ram_reg_0_13));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \m00_axis_tdata[7]_INST_0_i_2 
       (.I0(\m00_axis_tdata[31]_2 [7]),
        .I1(\m00_axis_tdata[0] ),
        .I2(\m00_axis_tdata[31]_3 [7]),
        .I3(\m00_axis_tdata[0]_0 ),
        .I4(\m00_axis_tdata[31]_4 [7]),
        .O(ram_reg_0_14));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata[8]_INST_0_i_1 
       (.I0(dout0_out[8]),
        .I1(\m00_axis_tdata[31] [8]),
        .I2(\m00_axis_tdata[0] ),
        .I3(\m00_axis_tdata[31]_0 [8]),
        .I4(\m00_axis_tdata[0]_0 ),
        .I5(\m00_axis_tdata[31]_1 [8]),
        .O(ram_reg_0_15));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \m00_axis_tdata[8]_INST_0_i_2 
       (.I0(\m00_axis_tdata[31]_2 [8]),
        .I1(\m00_axis_tdata[0] ),
        .I2(\m00_axis_tdata[31]_3 [8]),
        .I3(\m00_axis_tdata[0]_0 ),
        .I4(\m00_axis_tdata[31]_4 [8]),
        .O(ram_reg_0_16));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m00_axis_tdata[9]_INST_0_i_1 
       (.I0(dout0_out[9]),
        .I1(\m00_axis_tdata[31] [9]),
        .I2(\m00_axis_tdata[0] ),
        .I3(\m00_axis_tdata[31]_0 [9]),
        .I4(\m00_axis_tdata[0]_0 ),
        .I5(\m00_axis_tdata[31]_1 [9]),
        .O(ram_reg_0_17));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \m00_axis_tdata[9]_INST_0_i_2 
       (.I0(\m00_axis_tdata[31]_2 [9]),
        .I1(\m00_axis_tdata[0] ),
        .I2(\m00_axis_tdata[31]_3 [9]),
        .I3(\m00_axis_tdata[0]_0 ),
        .I4(\m00_axis_tdata[31]_4 [9]),
        .O(ram_reg_0_18));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_pixelReOrder_v1_0
   (m00_axis_tvalid,
    m00_axis_tdata,
    s00_axis_tvalid,
    m00_axis_aclk,
    s00_axis_tdata,
    s00_axis_tuser);
  output m00_axis_tvalid;
  output [31:0]m00_axis_tdata;
  input s00_axis_tvalid;
  input m00_axis_aclk;
  input [31:0]s00_axis_tdata;
  input [26:0]s00_axis_tuser;

  wire [31:0]DI0;
  wire [31:0]DOUT0_w;
  wire [31:0]DOUT1_w;
  wire [31:0]DOUT2_w;
  wire [31:0]DOUT3_w;
  wire [31:0]DOUT4_w;
  wire [31:0]DOUT5_w;
  wire [31:0]DOUT6_w;
  wire [6:0]EN_r;
  wire \EN_r[0]_i_2_n_0 ;
  wire \EN_r[1]_i_2_n_0 ;
  wire \EN_r[2]_i_1_n_0 ;
  wire \EN_r[3]_i_1_n_0 ;
  wire \EN_r[4]_i_1_n_0 ;
  wire \EN_r[5]_i_1_n_0 ;
  wire \EN_r[6]_i_1_n_0 ;
  wire \EN_r[6]_i_2_n_0 ;
  wire WE_r;
  wire [10:0]adr_wr;
  wire [15:0]cnt_inner_valid;
  wire cnt_inner_valid0_carry__0_n_0;
  wire cnt_inner_valid0_carry__0_n_1;
  wire cnt_inner_valid0_carry__0_n_2;
  wire cnt_inner_valid0_carry__0_n_3;
  wire cnt_inner_valid0_carry__1_n_0;
  wire cnt_inner_valid0_carry__1_n_1;
  wire cnt_inner_valid0_carry__1_n_2;
  wire cnt_inner_valid0_carry__1_n_3;
  wire cnt_inner_valid0_carry__2_n_2;
  wire cnt_inner_valid0_carry__2_n_3;
  wire cnt_inner_valid0_carry_n_0;
  wire cnt_inner_valid0_carry_n_1;
  wire cnt_inner_valid0_carry_n_2;
  wire cnt_inner_valid0_carry_n_3;
  wire \cnt_inner_valid[0]_i_2_n_0 ;
  wire \cnt_inner_valid[15]_i_2_n_0 ;
  wire [15:0]cnt_inner_valid_0;
  wire \ctrl_r[3]_i_1_n_0 ;
  wire \ctrl_r[3]_i_2_n_0 ;
  wire \ctrl_r[3]_i_3_n_0 ;
  wire \ctrl_r[3]_i_4_n_0 ;
  wire \ctrl_r_reg_n_0_[0] ;
  wire \ctrl_r_reg_n_0_[1] ;
  wire \ctrl_r_reg_n_0_[2] ;
  wire \ctrl_r_reg_n_0_[3] ;
  wire [15:1]data0;
  wire m00_axis_aclk;
  wire [31:0]m00_axis_tdata;
  wire m00_axis_tvalid;
  wire m00_axis_tvalid_r0;
  wire m00_axis_tvalid_r_i_2_n_0;
  wire m00_axis_tvalid_r_i_3_n_0;
  wire m00_axis_tvalid_r_i_4_n_0;
  wire multiplexer_inst_n_0;
  wire multiplexer_inst_n_1;
  wire multiplexer_inst_n_10;
  wire multiplexer_inst_n_11;
  wire multiplexer_inst_n_12;
  wire multiplexer_inst_n_13;
  wire multiplexer_inst_n_14;
  wire multiplexer_inst_n_15;
  wire multiplexer_inst_n_16;
  wire multiplexer_inst_n_17;
  wire multiplexer_inst_n_18;
  wire multiplexer_inst_n_19;
  wire multiplexer_inst_n_2;
  wire multiplexer_inst_n_20;
  wire multiplexer_inst_n_21;
  wire multiplexer_inst_n_22;
  wire multiplexer_inst_n_23;
  wire multiplexer_inst_n_24;
  wire multiplexer_inst_n_25;
  wire multiplexer_inst_n_26;
  wire multiplexer_inst_n_27;
  wire multiplexer_inst_n_28;
  wire multiplexer_inst_n_29;
  wire multiplexer_inst_n_3;
  wire multiplexer_inst_n_30;
  wire multiplexer_inst_n_31;
  wire multiplexer_inst_n_32;
  wire multiplexer_inst_n_33;
  wire multiplexer_inst_n_34;
  wire multiplexer_inst_n_35;
  wire multiplexer_inst_n_36;
  wire multiplexer_inst_n_37;
  wire multiplexer_inst_n_38;
  wire multiplexer_inst_n_39;
  wire multiplexer_inst_n_4;
  wire multiplexer_inst_n_40;
  wire multiplexer_inst_n_41;
  wire multiplexer_inst_n_42;
  wire multiplexer_inst_n_43;
  wire multiplexer_inst_n_44;
  wire multiplexer_inst_n_45;
  wire multiplexer_inst_n_46;
  wire multiplexer_inst_n_47;
  wire multiplexer_inst_n_48;
  wire multiplexer_inst_n_49;
  wire multiplexer_inst_n_5;
  wire multiplexer_inst_n_50;
  wire multiplexer_inst_n_51;
  wire multiplexer_inst_n_52;
  wire multiplexer_inst_n_53;
  wire multiplexer_inst_n_54;
  wire multiplexer_inst_n_55;
  wire multiplexer_inst_n_56;
  wire multiplexer_inst_n_57;
  wire multiplexer_inst_n_58;
  wire multiplexer_inst_n_59;
  wire multiplexer_inst_n_6;
  wire multiplexer_inst_n_60;
  wire multiplexer_inst_n_61;
  wire multiplexer_inst_n_62;
  wire multiplexer_inst_n_63;
  wire multiplexer_inst_n_7;
  wire multiplexer_inst_n_8;
  wire multiplexer_inst_n_9;
  wire [1:0]p_0_out;
  wire [31:0]s00_axis_tdata;
  wire [26:0]s00_axis_tuser;
  wire s00_axis_tvalid;
  wire square_r;
  wire square_r_i_1_n_0;
  wire [3:2]NLW_cnt_inner_valid0_carry__2_CO_UNCONNECTED;
  wire [3:3]NLW_cnt_inner_valid0_carry__2_O_UNCONNECTED;

  FDRE \DI0_reg[0] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_tvalid),
        .D(s00_axis_tdata[0]),
        .Q(DI0[0]),
        .R(1'b0));
  FDRE \DI0_reg[10] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_tvalid),
        .D(s00_axis_tdata[10]),
        .Q(DI0[10]),
        .R(1'b0));
  FDRE \DI0_reg[11] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_tvalid),
        .D(s00_axis_tdata[11]),
        .Q(DI0[11]),
        .R(1'b0));
  FDRE \DI0_reg[12] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_tvalid),
        .D(s00_axis_tdata[12]),
        .Q(DI0[12]),
        .R(1'b0));
  FDRE \DI0_reg[13] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_tvalid),
        .D(s00_axis_tdata[13]),
        .Q(DI0[13]),
        .R(1'b0));
  FDRE \DI0_reg[14] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_tvalid),
        .D(s00_axis_tdata[14]),
        .Q(DI0[14]),
        .R(1'b0));
  FDRE \DI0_reg[15] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_tvalid),
        .D(s00_axis_tdata[15]),
        .Q(DI0[15]),
        .R(1'b0));
  FDRE \DI0_reg[16] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_tvalid),
        .D(s00_axis_tdata[16]),
        .Q(DI0[16]),
        .R(1'b0));
  FDRE \DI0_reg[17] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_tvalid),
        .D(s00_axis_tdata[17]),
        .Q(DI0[17]),
        .R(1'b0));
  FDRE \DI0_reg[18] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_tvalid),
        .D(s00_axis_tdata[18]),
        .Q(DI0[18]),
        .R(1'b0));
  FDRE \DI0_reg[19] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_tvalid),
        .D(s00_axis_tdata[19]),
        .Q(DI0[19]),
        .R(1'b0));
  FDRE \DI0_reg[1] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_tvalid),
        .D(s00_axis_tdata[1]),
        .Q(DI0[1]),
        .R(1'b0));
  FDRE \DI0_reg[20] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_tvalid),
        .D(s00_axis_tdata[20]),
        .Q(DI0[20]),
        .R(1'b0));
  FDRE \DI0_reg[21] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_tvalid),
        .D(s00_axis_tdata[21]),
        .Q(DI0[21]),
        .R(1'b0));
  FDRE \DI0_reg[22] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_tvalid),
        .D(s00_axis_tdata[22]),
        .Q(DI0[22]),
        .R(1'b0));
  FDRE \DI0_reg[23] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_tvalid),
        .D(s00_axis_tdata[23]),
        .Q(DI0[23]),
        .R(1'b0));
  FDRE \DI0_reg[24] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_tvalid),
        .D(s00_axis_tdata[24]),
        .Q(DI0[24]),
        .R(1'b0));
  FDRE \DI0_reg[25] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_tvalid),
        .D(s00_axis_tdata[25]),
        .Q(DI0[25]),
        .R(1'b0));
  FDRE \DI0_reg[26] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_tvalid),
        .D(s00_axis_tdata[26]),
        .Q(DI0[26]),
        .R(1'b0));
  FDRE \DI0_reg[27] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_tvalid),
        .D(s00_axis_tdata[27]),
        .Q(DI0[27]),
        .R(1'b0));
  FDRE \DI0_reg[28] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_tvalid),
        .D(s00_axis_tdata[28]),
        .Q(DI0[28]),
        .R(1'b0));
  FDRE \DI0_reg[29] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_tvalid),
        .D(s00_axis_tdata[29]),
        .Q(DI0[29]),
        .R(1'b0));
  FDRE \DI0_reg[2] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_tvalid),
        .D(s00_axis_tdata[2]),
        .Q(DI0[2]),
        .R(1'b0));
  FDRE \DI0_reg[30] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_tvalid),
        .D(s00_axis_tdata[30]),
        .Q(DI0[30]),
        .R(1'b0));
  FDRE \DI0_reg[31] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_tvalid),
        .D(s00_axis_tdata[31]),
        .Q(DI0[31]),
        .R(1'b0));
  FDRE \DI0_reg[3] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_tvalid),
        .D(s00_axis_tdata[3]),
        .Q(DI0[3]),
        .R(1'b0));
  FDRE \DI0_reg[4] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_tvalid),
        .D(s00_axis_tdata[4]),
        .Q(DI0[4]),
        .R(1'b0));
  FDRE \DI0_reg[5] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_tvalid),
        .D(s00_axis_tdata[5]),
        .Q(DI0[5]),
        .R(1'b0));
  FDRE \DI0_reg[6] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_tvalid),
        .D(s00_axis_tdata[6]),
        .Q(DI0[6]),
        .R(1'b0));
  FDRE \DI0_reg[7] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_tvalid),
        .D(s00_axis_tdata[7]),
        .Q(DI0[7]),
        .R(1'b0));
  FDRE \DI0_reg[8] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_tvalid),
        .D(s00_axis_tdata[8]),
        .Q(DI0[8]),
        .R(1'b0));
  FDRE \DI0_reg[9] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_tvalid),
        .D(s00_axis_tdata[9]),
        .Q(DI0[9]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT5 #(
    .INIT(32'h00000001)) 
    \EN_r[0]_i_1 
       (.I0(cnt_inner_valid[7]),
        .I1(cnt_inner_valid[8]),
        .I2(cnt_inner_valid[9]),
        .I3(cnt_inner_valid[10]),
        .I4(\EN_r[0]_i_2_n_0 ),
        .O(p_0_out[0]));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT4 #(
    .INIT(16'hFFFE)) 
    \EN_r[0]_i_2 
       (.I0(cnt_inner_valid[5]),
        .I1(m00_axis_tvalid_r_i_2_n_0),
        .I2(cnt_inner_valid[6]),
        .I3(cnt_inner_valid[4]),
        .O(\EN_r[0]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000002)) 
    \EN_r[1]_i_1 
       (.I0(cnt_inner_valid[4]),
        .I1(cnt_inner_valid[10]),
        .I2(cnt_inner_valid[9]),
        .I3(cnt_inner_valid[8]),
        .I4(cnt_inner_valid[7]),
        .I5(\EN_r[1]_i_2_n_0 ),
        .O(p_0_out[1]));
  LUT3 #(
    .INIT(8'hFE)) 
    \EN_r[1]_i_2 
       (.I0(cnt_inner_valid[6]),
        .I1(m00_axis_tvalid_r_i_2_n_0),
        .I2(cnt_inner_valid[5]),
        .O(\EN_r[1]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT4 #(
    .INIT(16'h0004)) 
    \EN_r[2]_i_1 
       (.I0(cnt_inner_valid[4]),
        .I1(cnt_inner_valid[5]),
        .I2(cnt_inner_valid[6]),
        .I3(m00_axis_tvalid_r_i_2_n_0),
        .O(\EN_r[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT4 #(
    .INIT(16'h0008)) 
    \EN_r[3]_i_1 
       (.I0(cnt_inner_valid[5]),
        .I1(cnt_inner_valid[4]),
        .I2(cnt_inner_valid[6]),
        .I3(m00_axis_tvalid_r_i_2_n_0),
        .O(\EN_r[3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT4 #(
    .INIT(16'h0004)) 
    \EN_r[4]_i_1 
       (.I0(cnt_inner_valid[4]),
        .I1(cnt_inner_valid[6]),
        .I2(cnt_inner_valid[5]),
        .I3(m00_axis_tvalid_r_i_2_n_0),
        .O(\EN_r[4]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT4 #(
    .INIT(16'h0008)) 
    \EN_r[5]_i_1 
       (.I0(cnt_inner_valid[4]),
        .I1(cnt_inner_valid[6]),
        .I2(cnt_inner_valid[5]),
        .I3(m00_axis_tvalid_r_i_2_n_0),
        .O(\EN_r[5]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAAAAAAAAAAAAAA2)) 
    \EN_r[6]_i_1 
       (.I0(s00_axis_tvalid),
        .I1(m00_axis_tvalid_r_i_4_n_0),
        .I2(cnt_inner_valid[10]),
        .I3(cnt_inner_valid[9]),
        .I4(cnt_inner_valid[8]),
        .I5(cnt_inner_valid[7]),
        .O(\EN_r[6]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT4 #(
    .INIT(16'h0040)) 
    \EN_r[6]_i_2 
       (.I0(cnt_inner_valid[4]),
        .I1(cnt_inner_valid[5]),
        .I2(cnt_inner_valid[6]),
        .I3(m00_axis_tvalid_r_i_2_n_0),
        .O(\EN_r[6]_i_2_n_0 ));
  FDRE \EN_r_reg[0] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_tvalid),
        .D(p_0_out[0]),
        .Q(EN_r[0]),
        .R(1'b0));
  FDRE \EN_r_reg[1] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_tvalid),
        .D(p_0_out[1]),
        .Q(EN_r[1]),
        .R(1'b0));
  FDRE \EN_r_reg[2] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_tvalid),
        .D(\EN_r[2]_i_1_n_0 ),
        .Q(EN_r[2]),
        .R(\EN_r[6]_i_1_n_0 ));
  FDRE \EN_r_reg[3] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_tvalid),
        .D(\EN_r[3]_i_1_n_0 ),
        .Q(EN_r[3]),
        .R(\EN_r[6]_i_1_n_0 ));
  FDRE \EN_r_reg[4] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_tvalid),
        .D(\EN_r[4]_i_1_n_0 ),
        .Q(EN_r[4]),
        .R(\EN_r[6]_i_1_n_0 ));
  FDRE \EN_r_reg[5] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_tvalid),
        .D(\EN_r[5]_i_1_n_0 ),
        .Q(EN_r[5]),
        .R(\EN_r[6]_i_1_n_0 ));
  FDRE \EN_r_reg[6] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_tvalid),
        .D(\EN_r[6]_i_2_n_0 ),
        .Q(EN_r[6]),
        .R(\EN_r[6]_i_1_n_0 ));
  FDRE WE_r_reg
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(s00_axis_tvalid),
        .Q(WE_r),
        .R(1'b0));
  FDSE \adr_wr_reg[0] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(s00_axis_tuser[0]),
        .Q(adr_wr[0]),
        .S(\ctrl_r[3]_i_1_n_0 ));
  FDSE \adr_wr_reg[10] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(s00_axis_tuser[10]),
        .Q(adr_wr[10]),
        .S(\ctrl_r[3]_i_1_n_0 ));
  FDSE \adr_wr_reg[1] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(s00_axis_tuser[1]),
        .Q(adr_wr[1]),
        .S(\ctrl_r[3]_i_1_n_0 ));
  FDSE \adr_wr_reg[2] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(s00_axis_tuser[2]),
        .Q(adr_wr[2]),
        .S(\ctrl_r[3]_i_1_n_0 ));
  FDSE \adr_wr_reg[3] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(s00_axis_tuser[3]),
        .Q(adr_wr[3]),
        .S(\ctrl_r[3]_i_1_n_0 ));
  FDSE \adr_wr_reg[4] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(s00_axis_tuser[4]),
        .Q(adr_wr[4]),
        .S(\ctrl_r[3]_i_1_n_0 ));
  FDSE \adr_wr_reg[5] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(s00_axis_tuser[5]),
        .Q(adr_wr[5]),
        .S(\ctrl_r[3]_i_1_n_0 ));
  FDSE \adr_wr_reg[6] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(s00_axis_tuser[6]),
        .Q(adr_wr[6]),
        .S(\ctrl_r[3]_i_1_n_0 ));
  FDSE \adr_wr_reg[7] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(s00_axis_tuser[7]),
        .Q(adr_wr[7]),
        .S(\ctrl_r[3]_i_1_n_0 ));
  FDSE \adr_wr_reg[8] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(s00_axis_tuser[8]),
        .Q(adr_wr[8]),
        .S(\ctrl_r[3]_i_1_n_0 ));
  FDSE \adr_wr_reg[9] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(s00_axis_tuser[9]),
        .Q(adr_wr[9]),
        .S(\ctrl_r[3]_i_1_n_0 ));
  CARRY4 cnt_inner_valid0_carry
       (.CI(1'b0),
        .CO({cnt_inner_valid0_carry_n_0,cnt_inner_valid0_carry_n_1,cnt_inner_valid0_carry_n_2,cnt_inner_valid0_carry_n_3}),
        .CYINIT(cnt_inner_valid[0]),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(data0[4:1]),
        .S(cnt_inner_valid[4:1]));
  CARRY4 cnt_inner_valid0_carry__0
       (.CI(cnt_inner_valid0_carry_n_0),
        .CO({cnt_inner_valid0_carry__0_n_0,cnt_inner_valid0_carry__0_n_1,cnt_inner_valid0_carry__0_n_2,cnt_inner_valid0_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(data0[8:5]),
        .S(cnt_inner_valid[8:5]));
  CARRY4 cnt_inner_valid0_carry__1
       (.CI(cnt_inner_valid0_carry__0_n_0),
        .CO({cnt_inner_valid0_carry__1_n_0,cnt_inner_valid0_carry__1_n_1,cnt_inner_valid0_carry__1_n_2,cnt_inner_valid0_carry__1_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(data0[12:9]),
        .S(cnt_inner_valid[12:9]));
  CARRY4 cnt_inner_valid0_carry__2
       (.CI(cnt_inner_valid0_carry__1_n_0),
        .CO({NLW_cnt_inner_valid0_carry__2_CO_UNCONNECTED[3:2],cnt_inner_valid0_carry__2_n_2,cnt_inner_valid0_carry__2_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({NLW_cnt_inner_valid0_carry__2_O_UNCONNECTED[3],data0[15:13]}),
        .S({1'b0,cnt_inner_valid[15:13]}));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'h00FF00FE)) 
    \cnt_inner_valid[0]_i_1 
       (.I0(cnt_inner_valid[3]),
        .I1(cnt_inner_valid[2]),
        .I2(cnt_inner_valid[1]),
        .I3(cnt_inner_valid[0]),
        .I4(\cnt_inner_valid[0]_i_2_n_0 ),
        .O(cnt_inner_valid_0[0]));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT5 #(
    .INIT(32'hFFFF7FFF)) 
    \cnt_inner_valid[0]_i_2 
       (.I0(cnt_inner_valid[9]),
        .I1(cnt_inner_valid[10]),
        .I2(cnt_inner_valid[7]),
        .I3(cnt_inner_valid[8]),
        .I4(\EN_r[0]_i_2_n_0 ),
        .O(\cnt_inner_valid[0]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \cnt_inner_valid[10]_i_1 
       (.I0(\cnt_inner_valid[15]_i_2_n_0 ),
        .I1(data0[10]),
        .O(cnt_inner_valid_0[10]));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \cnt_inner_valid[11]_i_1 
       (.I0(\cnt_inner_valid[15]_i_2_n_0 ),
        .I1(data0[11]),
        .O(cnt_inner_valid_0[11]));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \cnt_inner_valid[12]_i_1 
       (.I0(\cnt_inner_valid[15]_i_2_n_0 ),
        .I1(data0[12]),
        .O(cnt_inner_valid_0[12]));
  LUT2 #(
    .INIT(4'h8)) 
    \cnt_inner_valid[13]_i_1 
       (.I0(\cnt_inner_valid[15]_i_2_n_0 ),
        .I1(data0[13]),
        .O(cnt_inner_valid_0[13]));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \cnt_inner_valid[14]_i_1 
       (.I0(\cnt_inner_valid[15]_i_2_n_0 ),
        .I1(data0[14]),
        .O(cnt_inner_valid_0[14]));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \cnt_inner_valid[15]_i_1 
       (.I0(\cnt_inner_valid[15]_i_2_n_0 ),
        .I1(data0[15]),
        .O(cnt_inner_valid_0[15]));
  LUT6 #(
    .INIT(64'hFFFFFFFFBFFFFFFF)) 
    \cnt_inner_valid[15]_i_2 
       (.I0(\EN_r[0]_i_2_n_0 ),
        .I1(cnt_inner_valid[8]),
        .I2(cnt_inner_valid[7]),
        .I3(cnt_inner_valid[10]),
        .I4(cnt_inner_valid[9]),
        .I5(m00_axis_tvalid_r_i_4_n_0),
        .O(\cnt_inner_valid[15]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \cnt_inner_valid[1]_i_1 
       (.I0(\cnt_inner_valid[15]_i_2_n_0 ),
        .I1(data0[1]),
        .O(cnt_inner_valid_0[1]));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \cnt_inner_valid[2]_i_1 
       (.I0(\cnt_inner_valid[15]_i_2_n_0 ),
        .I1(data0[2]),
        .O(cnt_inner_valid_0[2]));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \cnt_inner_valid[3]_i_1 
       (.I0(\cnt_inner_valid[15]_i_2_n_0 ),
        .I1(data0[3]),
        .O(cnt_inner_valid_0[3]));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \cnt_inner_valid[4]_i_1 
       (.I0(\cnt_inner_valid[15]_i_2_n_0 ),
        .I1(data0[4]),
        .O(cnt_inner_valid_0[4]));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \cnt_inner_valid[5]_i_1 
       (.I0(\cnt_inner_valid[15]_i_2_n_0 ),
        .I1(data0[5]),
        .O(cnt_inner_valid_0[5]));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \cnt_inner_valid[6]_i_1 
       (.I0(\cnt_inner_valid[15]_i_2_n_0 ),
        .I1(data0[6]),
        .O(cnt_inner_valid_0[6]));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \cnt_inner_valid[7]_i_1 
       (.I0(\cnt_inner_valid[15]_i_2_n_0 ),
        .I1(data0[7]),
        .O(cnt_inner_valid_0[7]));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \cnt_inner_valid[8]_i_1 
       (.I0(\cnt_inner_valid[15]_i_2_n_0 ),
        .I1(data0[8]),
        .O(cnt_inner_valid_0[8]));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \cnt_inner_valid[9]_i_1 
       (.I0(\cnt_inner_valid[15]_i_2_n_0 ),
        .I1(data0[9]),
        .O(cnt_inner_valid_0[9]));
  FDRE \cnt_inner_valid_reg[0] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(cnt_inner_valid_0[0]),
        .Q(cnt_inner_valid[0]),
        .R(\ctrl_r[3]_i_1_n_0 ));
  FDRE \cnt_inner_valid_reg[10] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(cnt_inner_valid_0[10]),
        .Q(cnt_inner_valid[10]),
        .R(\ctrl_r[3]_i_1_n_0 ));
  FDRE \cnt_inner_valid_reg[11] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(cnt_inner_valid_0[11]),
        .Q(cnt_inner_valid[11]),
        .R(\ctrl_r[3]_i_1_n_0 ));
  FDRE \cnt_inner_valid_reg[12] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(cnt_inner_valid_0[12]),
        .Q(cnt_inner_valid[12]),
        .R(\ctrl_r[3]_i_1_n_0 ));
  FDRE \cnt_inner_valid_reg[13] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(cnt_inner_valid_0[13]),
        .Q(cnt_inner_valid[13]),
        .R(\ctrl_r[3]_i_1_n_0 ));
  FDRE \cnt_inner_valid_reg[14] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(cnt_inner_valid_0[14]),
        .Q(cnt_inner_valid[14]),
        .R(\ctrl_r[3]_i_1_n_0 ));
  FDRE \cnt_inner_valid_reg[15] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(cnt_inner_valid_0[15]),
        .Q(cnt_inner_valid[15]),
        .R(\ctrl_r[3]_i_1_n_0 ));
  FDRE \cnt_inner_valid_reg[1] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(cnt_inner_valid_0[1]),
        .Q(cnt_inner_valid[1]),
        .R(\ctrl_r[3]_i_1_n_0 ));
  FDRE \cnt_inner_valid_reg[2] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(cnt_inner_valid_0[2]),
        .Q(cnt_inner_valid[2]),
        .R(\ctrl_r[3]_i_1_n_0 ));
  FDRE \cnt_inner_valid_reg[3] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(cnt_inner_valid_0[3]),
        .Q(cnt_inner_valid[3]),
        .R(\ctrl_r[3]_i_1_n_0 ));
  FDRE \cnt_inner_valid_reg[4] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(cnt_inner_valid_0[4]),
        .Q(cnt_inner_valid[4]),
        .R(\ctrl_r[3]_i_1_n_0 ));
  FDRE \cnt_inner_valid_reg[5] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(cnt_inner_valid_0[5]),
        .Q(cnt_inner_valid[5]),
        .R(\ctrl_r[3]_i_1_n_0 ));
  FDRE \cnt_inner_valid_reg[6] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(cnt_inner_valid_0[6]),
        .Q(cnt_inner_valid[6]),
        .R(\ctrl_r[3]_i_1_n_0 ));
  FDRE \cnt_inner_valid_reg[7] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(cnt_inner_valid_0[7]),
        .Q(cnt_inner_valid[7]),
        .R(\ctrl_r[3]_i_1_n_0 ));
  FDRE \cnt_inner_valid_reg[8] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(cnt_inner_valid_0[8]),
        .Q(cnt_inner_valid[8]),
        .R(\ctrl_r[3]_i_1_n_0 ));
  FDRE \cnt_inner_valid_reg[9] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(cnt_inner_valid_0[9]),
        .Q(cnt_inner_valid[9]),
        .R(\ctrl_r[3]_i_1_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \ctrl_r[3]_i_1 
       (.I0(s00_axis_tvalid),
        .O(\ctrl_r[3]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0008800800000000)) 
    \ctrl_r[3]_i_2 
       (.I0(\ctrl_r[3]_i_3_n_0 ),
        .I1(\ctrl_r[3]_i_4_n_0 ),
        .I2(s00_axis_tuser[16]),
        .I3(s00_axis_tuser[17]),
        .I4(s00_axis_tuser[18]),
        .I5(s00_axis_tuser[15]),
        .O(\ctrl_r[3]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'h0001)) 
    \ctrl_r[3]_i_3 
       (.I0(s00_axis_tuser[26]),
        .I1(s00_axis_tuser[25]),
        .I2(s00_axis_tuser[24]),
        .I3(s00_axis_tuser[23]),
        .O(\ctrl_r[3]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'h0001)) 
    \ctrl_r[3]_i_4 
       (.I0(s00_axis_tuser[22]),
        .I1(s00_axis_tuser[21]),
        .I2(s00_axis_tuser[20]),
        .I3(s00_axis_tuser[19]),
        .O(\ctrl_r[3]_i_4_n_0 ));
  FDRE \ctrl_r_reg[0] 
       (.C(m00_axis_aclk),
        .CE(\ctrl_r[3]_i_2_n_0 ),
        .D(s00_axis_tuser[11]),
        .Q(\ctrl_r_reg_n_0_[0] ),
        .R(\ctrl_r[3]_i_1_n_0 ));
  FDRE \ctrl_r_reg[1] 
       (.C(m00_axis_aclk),
        .CE(\ctrl_r[3]_i_2_n_0 ),
        .D(s00_axis_tuser[12]),
        .Q(\ctrl_r_reg_n_0_[1] ),
        .R(\ctrl_r[3]_i_1_n_0 ));
  FDRE \ctrl_r_reg[2] 
       (.C(m00_axis_aclk),
        .CE(\ctrl_r[3]_i_2_n_0 ),
        .D(s00_axis_tuser[13]),
        .Q(\ctrl_r_reg_n_0_[2] ),
        .R(\ctrl_r[3]_i_1_n_0 ));
  FDRE \ctrl_r_reg[3] 
       (.C(m00_axis_aclk),
        .CE(\ctrl_r[3]_i_2_n_0 ),
        .D(s00_axis_tuser[14]),
        .Q(\ctrl_r_reg_n_0_[3] ),
        .R(\ctrl_r[3]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0001000001000000)) 
    m00_axis_tvalid_r_i_1
       (.I0(cnt_inner_valid[5]),
        .I1(m00_axis_tvalid_r_i_2_n_0),
        .I2(cnt_inner_valid[6]),
        .I3(cnt_inner_valid[4]),
        .I4(m00_axis_tvalid_r_i_3_n_0),
        .I5(m00_axis_tvalid_r_i_4_n_0),
        .O(m00_axis_tvalid_r0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    m00_axis_tvalid_r_i_2
       (.I0(cnt_inner_valid[11]),
        .I1(cnt_inner_valid[14]),
        .I2(cnt_inner_valid[15]),
        .I3(cnt_inner_valid[13]),
        .I4(cnt_inner_valid[12]),
        .O(m00_axis_tvalid_r_i_2_n_0));
  LUT4 #(
    .INIT(16'h0001)) 
    m00_axis_tvalid_r_i_3
       (.I0(cnt_inner_valid[10]),
        .I1(cnt_inner_valid[9]),
        .I2(cnt_inner_valid[8]),
        .I3(cnt_inner_valid[7]),
        .O(m00_axis_tvalid_r_i_3_n_0));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT4 #(
    .INIT(16'hFFFE)) 
    m00_axis_tvalid_r_i_4
       (.I0(cnt_inner_valid[1]),
        .I1(cnt_inner_valid[2]),
        .I2(cnt_inner_valid[3]),
        .I3(cnt_inner_valid[0]),
        .O(m00_axis_tvalid_r_i_4_n_0));
  FDRE m00_axis_tvalid_r_reg
       (.C(m00_axis_aclk),
        .CE(s00_axis_tvalid),
        .D(m00_axis_tvalid_r0),
        .Q(m00_axis_tvalid),
        .R(1'b0));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_multiplexer multiplexer_inst
       (.dout0_out(DOUT3_w),
        .\m00_axis_tdata[0] (\ctrl_r_reg_n_0_[1] ),
        .\m00_axis_tdata[0]_0 (\ctrl_r_reg_n_0_[0] ),
        .\m00_axis_tdata[31] (DOUT2_w),
        .\m00_axis_tdata[31]_0 (DOUT1_w),
        .\m00_axis_tdata[31]_1 (DOUT0_w),
        .\m00_axis_tdata[31]_2 (DOUT6_w),
        .\m00_axis_tdata[31]_3 (DOUT5_w),
        .\m00_axis_tdata[31]_4 (DOUT4_w),
        .ram_reg_0(multiplexer_inst_n_0),
        .ram_reg_0_0(multiplexer_inst_n_1),
        .ram_reg_0_1(multiplexer_inst_n_2),
        .ram_reg_0_10(multiplexer_inst_n_11),
        .ram_reg_0_11(multiplexer_inst_n_12),
        .ram_reg_0_12(multiplexer_inst_n_13),
        .ram_reg_0_13(multiplexer_inst_n_14),
        .ram_reg_0_14(multiplexer_inst_n_15),
        .ram_reg_0_15(multiplexer_inst_n_16),
        .ram_reg_0_16(multiplexer_inst_n_17),
        .ram_reg_0_17(multiplexer_inst_n_18),
        .ram_reg_0_18(multiplexer_inst_n_19),
        .ram_reg_0_19(multiplexer_inst_n_20),
        .ram_reg_0_2(multiplexer_inst_n_3),
        .ram_reg_0_20(multiplexer_inst_n_21),
        .ram_reg_0_21(multiplexer_inst_n_22),
        .ram_reg_0_22(multiplexer_inst_n_23),
        .ram_reg_0_23(multiplexer_inst_n_24),
        .ram_reg_0_24(multiplexer_inst_n_25),
        .ram_reg_0_25(multiplexer_inst_n_26),
        .ram_reg_0_26(multiplexer_inst_n_27),
        .ram_reg_0_27(multiplexer_inst_n_28),
        .ram_reg_0_28(multiplexer_inst_n_29),
        .ram_reg_0_29(multiplexer_inst_n_30),
        .ram_reg_0_3(multiplexer_inst_n_4),
        .ram_reg_0_30(multiplexer_inst_n_31),
        .ram_reg_0_31(multiplexer_inst_n_32),
        .ram_reg_0_32(multiplexer_inst_n_33),
        .ram_reg_0_33(multiplexer_inst_n_34),
        .ram_reg_0_34(multiplexer_inst_n_35),
        .ram_reg_0_4(multiplexer_inst_n_5),
        .ram_reg_0_5(multiplexer_inst_n_6),
        .ram_reg_0_6(multiplexer_inst_n_7),
        .ram_reg_0_7(multiplexer_inst_n_8),
        .ram_reg_0_8(multiplexer_inst_n_9),
        .ram_reg_0_9(multiplexer_inst_n_10),
        .ram_reg_1(multiplexer_inst_n_36),
        .ram_reg_1_0(multiplexer_inst_n_37),
        .ram_reg_1_1(multiplexer_inst_n_38),
        .ram_reg_1_10(multiplexer_inst_n_47),
        .ram_reg_1_11(multiplexer_inst_n_48),
        .ram_reg_1_12(multiplexer_inst_n_49),
        .ram_reg_1_13(multiplexer_inst_n_50),
        .ram_reg_1_14(multiplexer_inst_n_51),
        .ram_reg_1_15(multiplexer_inst_n_52),
        .ram_reg_1_16(multiplexer_inst_n_53),
        .ram_reg_1_17(multiplexer_inst_n_54),
        .ram_reg_1_18(multiplexer_inst_n_55),
        .ram_reg_1_19(multiplexer_inst_n_56),
        .ram_reg_1_2(multiplexer_inst_n_39),
        .ram_reg_1_20(multiplexer_inst_n_57),
        .ram_reg_1_21(multiplexer_inst_n_58),
        .ram_reg_1_22(multiplexer_inst_n_59),
        .ram_reg_1_23(multiplexer_inst_n_60),
        .ram_reg_1_24(multiplexer_inst_n_61),
        .ram_reg_1_25(multiplexer_inst_n_62),
        .ram_reg_1_26(multiplexer_inst_n_63),
        .ram_reg_1_3(multiplexer_inst_n_40),
        .ram_reg_1_4(multiplexer_inst_n_41),
        .ram_reg_1_5(multiplexer_inst_n_42),
        .ram_reg_1_6(multiplexer_inst_n_43),
        .ram_reg_1_7(multiplexer_inst_n_44),
        .ram_reg_1_8(multiplexer_inst_n_45),
        .ram_reg_1_9(multiplexer_inst_n_46));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst rams_sp_rf_rst_inst0
       (.Q(EN_r[0]),
        .WE_r(WE_r),
        .addr(adr_wr),
        .di(DI0),
        .dout0_out(DOUT0_w),
        .m00_axis_aclk(m00_axis_aclk),
        .square_r(square_r));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_0 rams_sp_rf_rst_inst1
       (.Q(EN_r[1]),
        .WE_r(WE_r),
        .addr(adr_wr),
        .di(DI0),
        .dout0_out(DOUT1_w),
        .m00_axis_aclk(m00_axis_aclk),
        .square_r(square_r));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_1 rams_sp_rf_rst_inst2
       (.WE_r(WE_r),
        .addr(adr_wr),
        .di(DI0),
        .dout0_out(DOUT2_w),
        .en(EN_r[2]),
        .m00_axis_aclk(m00_axis_aclk),
        .square_r(square_r));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_2 rams_sp_rf_rst_inst3
       (.WE_r(WE_r),
        .addr(adr_wr),
        .di(DI0),
        .dout0_out(DOUT3_w),
        .en(EN_r[3]),
        .m00_axis_aclk(m00_axis_aclk),
        .m00_axis_tdata(m00_axis_tdata),
        .\m00_axis_tdata[0]_0 (\ctrl_r_reg_n_0_[2] ),
        .\m00_axis_tdata[0]_1 (multiplexer_inst_n_1),
        .\m00_axis_tdata[0]_2 (\ctrl_r_reg_n_0_[3] ),
        .\m00_axis_tdata[10]_0 (multiplexer_inst_n_21),
        .\m00_axis_tdata[11]_0 (multiplexer_inst_n_23),
        .\m00_axis_tdata[12]_0 (multiplexer_inst_n_25),
        .\m00_axis_tdata[13]_0 (multiplexer_inst_n_27),
        .\m00_axis_tdata[14]_0 (multiplexer_inst_n_29),
        .\m00_axis_tdata[15]_0 (multiplexer_inst_n_31),
        .\m00_axis_tdata[16]_0 (multiplexer_inst_n_33),
        .\m00_axis_tdata[17]_0 (multiplexer_inst_n_35),
        .\m00_axis_tdata[18]_0 (multiplexer_inst_n_37),
        .\m00_axis_tdata[19]_0 (multiplexer_inst_n_39),
        .\m00_axis_tdata[1]_0 (multiplexer_inst_n_3),
        .\m00_axis_tdata[20]_0 (multiplexer_inst_n_41),
        .\m00_axis_tdata[21]_0 (multiplexer_inst_n_43),
        .\m00_axis_tdata[22]_0 (multiplexer_inst_n_45),
        .\m00_axis_tdata[23]_0 (multiplexer_inst_n_47),
        .\m00_axis_tdata[24]_0 (multiplexer_inst_n_49),
        .\m00_axis_tdata[25]_0 (multiplexer_inst_n_51),
        .\m00_axis_tdata[26]_0 (multiplexer_inst_n_53),
        .\m00_axis_tdata[27]_0 (multiplexer_inst_n_55),
        .\m00_axis_tdata[28]_0 (multiplexer_inst_n_57),
        .\m00_axis_tdata[29]_0 (multiplexer_inst_n_59),
        .\m00_axis_tdata[2]_0 (multiplexer_inst_n_5),
        .\m00_axis_tdata[30]_0 (multiplexer_inst_n_61),
        .\m00_axis_tdata[31]_0 (multiplexer_inst_n_63),
        .\m00_axis_tdata[3]_0 (multiplexer_inst_n_7),
        .\m00_axis_tdata[4]_0 (multiplexer_inst_n_9),
        .\m00_axis_tdata[5]_0 (multiplexer_inst_n_11),
        .\m00_axis_tdata[6]_0 (multiplexer_inst_n_13),
        .\m00_axis_tdata[7]_0 (multiplexer_inst_n_15),
        .\m00_axis_tdata[8]_0 (multiplexer_inst_n_17),
        .\m00_axis_tdata[9]_0 (multiplexer_inst_n_19),
        .m00_axis_tdata_0_sp_1(multiplexer_inst_n_0),
        .m00_axis_tdata_10_sp_1(multiplexer_inst_n_20),
        .m00_axis_tdata_11_sp_1(multiplexer_inst_n_22),
        .m00_axis_tdata_12_sp_1(multiplexer_inst_n_24),
        .m00_axis_tdata_13_sp_1(multiplexer_inst_n_26),
        .m00_axis_tdata_14_sp_1(multiplexer_inst_n_28),
        .m00_axis_tdata_15_sp_1(multiplexer_inst_n_30),
        .m00_axis_tdata_16_sp_1(multiplexer_inst_n_32),
        .m00_axis_tdata_17_sp_1(multiplexer_inst_n_34),
        .m00_axis_tdata_18_sp_1(multiplexer_inst_n_36),
        .m00_axis_tdata_19_sp_1(multiplexer_inst_n_38),
        .m00_axis_tdata_1_sp_1(multiplexer_inst_n_2),
        .m00_axis_tdata_20_sp_1(multiplexer_inst_n_40),
        .m00_axis_tdata_21_sp_1(multiplexer_inst_n_42),
        .m00_axis_tdata_22_sp_1(multiplexer_inst_n_44),
        .m00_axis_tdata_23_sp_1(multiplexer_inst_n_46),
        .m00_axis_tdata_24_sp_1(multiplexer_inst_n_48),
        .m00_axis_tdata_25_sp_1(multiplexer_inst_n_50),
        .m00_axis_tdata_26_sp_1(multiplexer_inst_n_52),
        .m00_axis_tdata_27_sp_1(multiplexer_inst_n_54),
        .m00_axis_tdata_28_sp_1(multiplexer_inst_n_56),
        .m00_axis_tdata_29_sp_1(multiplexer_inst_n_58),
        .m00_axis_tdata_2_sp_1(multiplexer_inst_n_4),
        .m00_axis_tdata_30_sp_1(multiplexer_inst_n_60),
        .m00_axis_tdata_31_sp_1(multiplexer_inst_n_62),
        .m00_axis_tdata_3_sp_1(multiplexer_inst_n_6),
        .m00_axis_tdata_4_sp_1(multiplexer_inst_n_8),
        .m00_axis_tdata_5_sp_1(multiplexer_inst_n_10),
        .m00_axis_tdata_6_sp_1(multiplexer_inst_n_12),
        .m00_axis_tdata_7_sp_1(multiplexer_inst_n_14),
        .m00_axis_tdata_8_sp_1(multiplexer_inst_n_16),
        .m00_axis_tdata_9_sp_1(multiplexer_inst_n_18),
        .square_r(square_r));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_3 rams_sp_rf_rst_inst4
       (.WE_r(WE_r),
        .addr(adr_wr),
        .di(DI0),
        .dout0_out(DOUT4_w),
        .en(EN_r[4]),
        .m00_axis_aclk(m00_axis_aclk),
        .square_r(square_r));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_4 rams_sp_rf_rst_inst5
       (.WE_r(WE_r),
        .addr(adr_wr),
        .di(DI0),
        .dout0_out(DOUT5_w),
        .en(EN_r[5]),
        .m00_axis_aclk(m00_axis_aclk),
        .square_r(square_r));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_5 rams_sp_rf_rst_inst6
       (.WE_r(WE_r),
        .addr(adr_wr),
        .di(DI0),
        .dout0_out(DOUT6_w),
        .en(EN_r[6]),
        .m00_axis_aclk(m00_axis_aclk),
        .square_r(square_r));
  LUT3 #(
    .INIT(8'h5C)) 
    square_r_i_1
       (.I0(s00_axis_tuser[15]),
        .I1(square_r),
        .I2(s00_axis_tvalid),
        .O(square_r_i_1_n_0));
  FDRE square_r_reg
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(square_r_i_1_n_0),
        .Q(square_r),
        .R(1'b0));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst
   (dout0_out,
    m00_axis_aclk,
    Q,
    addr,
    di,
    WE_r,
    square_r);
  output [31:0]dout0_out;
  input m00_axis_aclk;
  input [0:0]Q;
  input [10:0]addr;
  input [31:0]di;
  input WE_r;
  input square_r;

  wire [0:0]Q;
  wire WE_r;
  wire [10:0]addr;
  wire [31:0]di;
  wire [31:0]dout0_out;
  wire m00_axis_aclk;
  wire ram_reg_0_i_1_n_0;
  wire square_r;
  wire NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED;
  wire NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED;
  wire NLW_ram_reg_0_DBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_SBITERR_UNCONNECTED;
  wire [31:16]NLW_ram_reg_0_DOADO_UNCONNECTED;
  wire [31:0]NLW_ram_reg_0_DOBDO_UNCONNECTED;
  wire [3:2]NLW_ram_reg_0_DOPADOP_UNCONNECTED;
  wire [3:0]NLW_ram_reg_0_DOPBDOP_UNCONNECTED;
  wire [7:0]NLW_ram_reg_0_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_ram_reg_0_RDADDRECC_UNCONNECTED;
  wire NLW_ram_reg_1_CASCADEOUTA_UNCONNECTED;
  wire NLW_ram_reg_1_CASCADEOUTB_UNCONNECTED;
  wire NLW_ram_reg_1_DBITERR_UNCONNECTED;
  wire NLW_ram_reg_1_INJECTDBITERR_UNCONNECTED;
  wire NLW_ram_reg_1_INJECTSBITERR_UNCONNECTED;
  wire NLW_ram_reg_1_SBITERR_UNCONNECTED;
  wire [31:14]NLW_ram_reg_1_DOADO_UNCONNECTED;
  wire [31:0]NLW_ram_reg_1_DOBDO_UNCONNECTED;
  wire [3:0]NLW_ram_reg_1_DOPADOP_UNCONNECTED;
  wire [3:0]NLW_ram_reg_1_DOPBDOP_UNCONNECTED;
  wire [7:0]NLW_ram_reg_1_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_ram_reg_1_RDADDRECC_UNCONNECTED;

  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p2_d16" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "61472" *) 
  (* RTL_RAM_NAME = "ram" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "2047" *) 
  (* bram_slice_begin = "0" *) 
  (* bram_slice_end = "17" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "2047" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "17" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(18),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(18),
    .WRITE_WIDTH_B(0)) 
    ram_reg_0
       (.ADDRARDADDR({1'b1,addr,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CASCADEINA(1'b1),
        .CASCADEINB(1'b0),
        .CASCADEOUTA(NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED),
        .CASCADEOUTB(NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED),
        .CLKARDCLK(m00_axis_aclk),
        .CLKBWRCLK(1'b0),
        .DBITERR(NLW_ram_reg_0_DBITERR_UNCONNECTED),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,di[15:0]}),
        .DIBDI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0,di[17:16]}),
        .DIPBDIP({1'b1,1'b1,1'b1,1'b1}),
        .DOADO({NLW_ram_reg_0_DOADO_UNCONNECTED[31:16],dout0_out[15:0]}),
        .DOBDO(NLW_ram_reg_0_DOBDO_UNCONNECTED[31:0]),
        .DOPADOP({NLW_ram_reg_0_DOPADOP_UNCONNECTED[3:2],dout0_out[17:16]}),
        .DOPBDOP(NLW_ram_reg_0_DOPBDOP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_ram_reg_0_ECCPARITY_UNCONNECTED[7:0]),
        .ENARDEN(Q),
        .ENBWREN(1'b0),
        .INJECTDBITERR(NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED),
        .INJECTSBITERR(NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED),
        .RDADDRECC(NLW_ram_reg_0_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_ram_reg_0_SBITERR_UNCONNECTED),
        .WEA({ram_reg_0_i_1_n_0,ram_reg_0_i_1_n_0,ram_reg_0_i_1_n_0,ram_reg_0_i_1_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
  LUT3 #(
    .INIT(8'h80)) 
    ram_reg_0_i_1
       (.I0(Q),
        .I1(WE_r),
        .I2(square_r),
        .O(ram_reg_0_i_1_n_0));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d14" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "61472" *) 
  (* RTL_RAM_NAME = "ram" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "2047" *) 
  (* bram_slice_begin = "18" *) 
  (* bram_slice_end = "31" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "2047" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "18" *) 
  (* ram_slice_end = "31" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(18),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(18),
    .WRITE_WIDTH_B(0)) 
    ram_reg_1
       (.ADDRARDADDR({1'b1,addr,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CASCADEINA(1'b1),
        .CASCADEINB(1'b0),
        .CASCADEOUTA(NLW_ram_reg_1_CASCADEOUTA_UNCONNECTED),
        .CASCADEOUTB(NLW_ram_reg_1_CASCADEOUTB_UNCONNECTED),
        .CLKARDCLK(m00_axis_aclk),
        .CLKBWRCLK(1'b0),
        .DBITERR(NLW_ram_reg_1_DBITERR_UNCONNECTED),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,di[31:18]}),
        .DIBDI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0,1'b0,1'b0}),
        .DIPBDIP({1'b1,1'b1,1'b1,1'b1}),
        .DOADO({NLW_ram_reg_1_DOADO_UNCONNECTED[31:14],dout0_out[31:18]}),
        .DOBDO(NLW_ram_reg_1_DOBDO_UNCONNECTED[31:0]),
        .DOPADOP(NLW_ram_reg_1_DOPADOP_UNCONNECTED[3:0]),
        .DOPBDOP(NLW_ram_reg_1_DOPBDOP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_ram_reg_1_ECCPARITY_UNCONNECTED[7:0]),
        .ENARDEN(Q),
        .ENBWREN(1'b0),
        .INJECTDBITERR(NLW_ram_reg_1_INJECTDBITERR_UNCONNECTED),
        .INJECTSBITERR(NLW_ram_reg_1_INJECTSBITERR_UNCONNECTED),
        .RDADDRECC(NLW_ram_reg_1_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_ram_reg_1_SBITERR_UNCONNECTED),
        .WEA({ram_reg_0_i_1_n_0,ram_reg_0_i_1_n_0,ram_reg_0_i_1_n_0,ram_reg_0_i_1_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
endmodule

(* ORIG_REF_NAME = "rams_sp_rf_rst" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_0
   (dout0_out,
    m00_axis_aclk,
    Q,
    addr,
    di,
    WE_r,
    square_r);
  output [31:0]dout0_out;
  input m00_axis_aclk;
  input [0:0]Q;
  input [10:0]addr;
  input [31:0]di;
  input WE_r;
  input square_r;

  wire [0:0]Q;
  wire WE_r;
  wire [10:0]addr;
  wire [31:0]di;
  wire [31:0]dout0_out;
  wire m00_axis_aclk;
  wire ram_reg_0_i_1__0_n_0;
  wire square_r;
  wire NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED;
  wire NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED;
  wire NLW_ram_reg_0_DBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_SBITERR_UNCONNECTED;
  wire [31:16]NLW_ram_reg_0_DOADO_UNCONNECTED;
  wire [31:0]NLW_ram_reg_0_DOBDO_UNCONNECTED;
  wire [3:2]NLW_ram_reg_0_DOPADOP_UNCONNECTED;
  wire [3:0]NLW_ram_reg_0_DOPBDOP_UNCONNECTED;
  wire [7:0]NLW_ram_reg_0_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_ram_reg_0_RDADDRECC_UNCONNECTED;
  wire NLW_ram_reg_1_CASCADEOUTA_UNCONNECTED;
  wire NLW_ram_reg_1_CASCADEOUTB_UNCONNECTED;
  wire NLW_ram_reg_1_DBITERR_UNCONNECTED;
  wire NLW_ram_reg_1_INJECTDBITERR_UNCONNECTED;
  wire NLW_ram_reg_1_INJECTSBITERR_UNCONNECTED;
  wire NLW_ram_reg_1_SBITERR_UNCONNECTED;
  wire [31:14]NLW_ram_reg_1_DOADO_UNCONNECTED;
  wire [31:0]NLW_ram_reg_1_DOBDO_UNCONNECTED;
  wire [3:0]NLW_ram_reg_1_DOPADOP_UNCONNECTED;
  wire [3:0]NLW_ram_reg_1_DOPBDOP_UNCONNECTED;
  wire [7:0]NLW_ram_reg_1_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_ram_reg_1_RDADDRECC_UNCONNECTED;

  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p2_d16" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "61472" *) 
  (* RTL_RAM_NAME = "ram" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "2047" *) 
  (* bram_slice_begin = "0" *) 
  (* bram_slice_end = "17" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "2047" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "17" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(18),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(18),
    .WRITE_WIDTH_B(0)) 
    ram_reg_0
       (.ADDRARDADDR({1'b1,addr,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CASCADEINA(1'b1),
        .CASCADEINB(1'b0),
        .CASCADEOUTA(NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED),
        .CASCADEOUTB(NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED),
        .CLKARDCLK(m00_axis_aclk),
        .CLKBWRCLK(1'b0),
        .DBITERR(NLW_ram_reg_0_DBITERR_UNCONNECTED),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,di[15:0]}),
        .DIBDI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0,di[17:16]}),
        .DIPBDIP({1'b1,1'b1,1'b1,1'b1}),
        .DOADO({NLW_ram_reg_0_DOADO_UNCONNECTED[31:16],dout0_out[15:0]}),
        .DOBDO(NLW_ram_reg_0_DOBDO_UNCONNECTED[31:0]),
        .DOPADOP({NLW_ram_reg_0_DOPADOP_UNCONNECTED[3:2],dout0_out[17:16]}),
        .DOPBDOP(NLW_ram_reg_0_DOPBDOP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_ram_reg_0_ECCPARITY_UNCONNECTED[7:0]),
        .ENARDEN(Q),
        .ENBWREN(1'b0),
        .INJECTDBITERR(NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED),
        .INJECTSBITERR(NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED),
        .RDADDRECC(NLW_ram_reg_0_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_ram_reg_0_SBITERR_UNCONNECTED),
        .WEA({ram_reg_0_i_1__0_n_0,ram_reg_0_i_1__0_n_0,ram_reg_0_i_1__0_n_0,ram_reg_0_i_1__0_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
  LUT3 #(
    .INIT(8'h80)) 
    ram_reg_0_i_1__0
       (.I0(Q),
        .I1(WE_r),
        .I2(square_r),
        .O(ram_reg_0_i_1__0_n_0));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d14" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "61472" *) 
  (* RTL_RAM_NAME = "ram" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "2047" *) 
  (* bram_slice_begin = "18" *) 
  (* bram_slice_end = "31" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "2047" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "18" *) 
  (* ram_slice_end = "31" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(18),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(18),
    .WRITE_WIDTH_B(0)) 
    ram_reg_1
       (.ADDRARDADDR({1'b1,addr,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CASCADEINA(1'b1),
        .CASCADEINB(1'b0),
        .CASCADEOUTA(NLW_ram_reg_1_CASCADEOUTA_UNCONNECTED),
        .CASCADEOUTB(NLW_ram_reg_1_CASCADEOUTB_UNCONNECTED),
        .CLKARDCLK(m00_axis_aclk),
        .CLKBWRCLK(1'b0),
        .DBITERR(NLW_ram_reg_1_DBITERR_UNCONNECTED),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,di[31:18]}),
        .DIBDI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0,1'b0,1'b0}),
        .DIPBDIP({1'b1,1'b1,1'b1,1'b1}),
        .DOADO({NLW_ram_reg_1_DOADO_UNCONNECTED[31:14],dout0_out[31:18]}),
        .DOBDO(NLW_ram_reg_1_DOBDO_UNCONNECTED[31:0]),
        .DOPADOP(NLW_ram_reg_1_DOPADOP_UNCONNECTED[3:0]),
        .DOPBDOP(NLW_ram_reg_1_DOPBDOP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_ram_reg_1_ECCPARITY_UNCONNECTED[7:0]),
        .ENARDEN(Q),
        .ENBWREN(1'b0),
        .INJECTDBITERR(NLW_ram_reg_1_INJECTDBITERR_UNCONNECTED),
        .INJECTSBITERR(NLW_ram_reg_1_INJECTSBITERR_UNCONNECTED),
        .RDADDRECC(NLW_ram_reg_1_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_ram_reg_1_SBITERR_UNCONNECTED),
        .WEA({ram_reg_0_i_1__0_n_0,ram_reg_0_i_1__0_n_0,ram_reg_0_i_1__0_n_0,ram_reg_0_i_1__0_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
endmodule

(* ORIG_REF_NAME = "rams_sp_rf_rst" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_1
   (dout0_out,
    m00_axis_aclk,
    en,
    addr,
    di,
    WE_r,
    square_r);
  output [31:0]dout0_out;
  input m00_axis_aclk;
  input en;
  input [10:0]addr;
  input [31:0]di;
  input WE_r;
  input square_r;

  wire WE_r;
  wire [10:0]addr;
  wire [31:0]di;
  wire [31:0]dout0_out;
  wire en;
  wire m00_axis_aclk;
  wire ram_reg_0_i_1__1_n_0;
  wire square_r;
  wire NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED;
  wire NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED;
  wire NLW_ram_reg_0_DBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_SBITERR_UNCONNECTED;
  wire [31:16]NLW_ram_reg_0_DOADO_UNCONNECTED;
  wire [31:0]NLW_ram_reg_0_DOBDO_UNCONNECTED;
  wire [3:2]NLW_ram_reg_0_DOPADOP_UNCONNECTED;
  wire [3:0]NLW_ram_reg_0_DOPBDOP_UNCONNECTED;
  wire [7:0]NLW_ram_reg_0_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_ram_reg_0_RDADDRECC_UNCONNECTED;
  wire NLW_ram_reg_1_CASCADEOUTA_UNCONNECTED;
  wire NLW_ram_reg_1_CASCADEOUTB_UNCONNECTED;
  wire NLW_ram_reg_1_DBITERR_UNCONNECTED;
  wire NLW_ram_reg_1_INJECTDBITERR_UNCONNECTED;
  wire NLW_ram_reg_1_INJECTSBITERR_UNCONNECTED;
  wire NLW_ram_reg_1_SBITERR_UNCONNECTED;
  wire [31:14]NLW_ram_reg_1_DOADO_UNCONNECTED;
  wire [31:0]NLW_ram_reg_1_DOBDO_UNCONNECTED;
  wire [3:0]NLW_ram_reg_1_DOPADOP_UNCONNECTED;
  wire [3:0]NLW_ram_reg_1_DOPBDOP_UNCONNECTED;
  wire [7:0]NLW_ram_reg_1_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_ram_reg_1_RDADDRECC_UNCONNECTED;

  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p2_d16" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "61472" *) 
  (* RTL_RAM_NAME = "ram" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "2047" *) 
  (* bram_slice_begin = "0" *) 
  (* bram_slice_end = "17" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "2047" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "17" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(18),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(18),
    .WRITE_WIDTH_B(0)) 
    ram_reg_0
       (.ADDRARDADDR({1'b1,addr,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CASCADEINA(1'b1),
        .CASCADEINB(1'b0),
        .CASCADEOUTA(NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED),
        .CASCADEOUTB(NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED),
        .CLKARDCLK(m00_axis_aclk),
        .CLKBWRCLK(1'b0),
        .DBITERR(NLW_ram_reg_0_DBITERR_UNCONNECTED),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,di[15:0]}),
        .DIBDI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0,di[17:16]}),
        .DIPBDIP({1'b1,1'b1,1'b1,1'b1}),
        .DOADO({NLW_ram_reg_0_DOADO_UNCONNECTED[31:16],dout0_out[15:0]}),
        .DOBDO(NLW_ram_reg_0_DOBDO_UNCONNECTED[31:0]),
        .DOPADOP({NLW_ram_reg_0_DOPADOP_UNCONNECTED[3:2],dout0_out[17:16]}),
        .DOPBDOP(NLW_ram_reg_0_DOPBDOP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_ram_reg_0_ECCPARITY_UNCONNECTED[7:0]),
        .ENARDEN(en),
        .ENBWREN(1'b0),
        .INJECTDBITERR(NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED),
        .INJECTSBITERR(NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED),
        .RDADDRECC(NLW_ram_reg_0_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_ram_reg_0_SBITERR_UNCONNECTED),
        .WEA({ram_reg_0_i_1__1_n_0,ram_reg_0_i_1__1_n_0,ram_reg_0_i_1__1_n_0,ram_reg_0_i_1__1_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
  LUT3 #(
    .INIT(8'h80)) 
    ram_reg_0_i_1__1
       (.I0(en),
        .I1(WE_r),
        .I2(square_r),
        .O(ram_reg_0_i_1__1_n_0));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d14" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "61472" *) 
  (* RTL_RAM_NAME = "ram" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "2047" *) 
  (* bram_slice_begin = "18" *) 
  (* bram_slice_end = "31" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "2047" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "18" *) 
  (* ram_slice_end = "31" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(18),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(18),
    .WRITE_WIDTH_B(0)) 
    ram_reg_1
       (.ADDRARDADDR({1'b1,addr,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CASCADEINA(1'b1),
        .CASCADEINB(1'b0),
        .CASCADEOUTA(NLW_ram_reg_1_CASCADEOUTA_UNCONNECTED),
        .CASCADEOUTB(NLW_ram_reg_1_CASCADEOUTB_UNCONNECTED),
        .CLKARDCLK(m00_axis_aclk),
        .CLKBWRCLK(1'b0),
        .DBITERR(NLW_ram_reg_1_DBITERR_UNCONNECTED),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,di[31:18]}),
        .DIBDI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0,1'b0,1'b0}),
        .DIPBDIP({1'b1,1'b1,1'b1,1'b1}),
        .DOADO({NLW_ram_reg_1_DOADO_UNCONNECTED[31:14],dout0_out[31:18]}),
        .DOBDO(NLW_ram_reg_1_DOBDO_UNCONNECTED[31:0]),
        .DOPADOP(NLW_ram_reg_1_DOPADOP_UNCONNECTED[3:0]),
        .DOPBDOP(NLW_ram_reg_1_DOPBDOP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_ram_reg_1_ECCPARITY_UNCONNECTED[7:0]),
        .ENARDEN(en),
        .ENBWREN(1'b0),
        .INJECTDBITERR(NLW_ram_reg_1_INJECTDBITERR_UNCONNECTED),
        .INJECTSBITERR(NLW_ram_reg_1_INJECTSBITERR_UNCONNECTED),
        .RDADDRECC(NLW_ram_reg_1_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_ram_reg_1_SBITERR_UNCONNECTED),
        .WEA({ram_reg_0_i_1__1_n_0,ram_reg_0_i_1__1_n_0,ram_reg_0_i_1__1_n_0,ram_reg_0_i_1__1_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
endmodule

(* ORIG_REF_NAME = "rams_sp_rf_rst" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_2
   (m00_axis_tdata,
    dout0_out,
    m00_axis_tdata_0_sp_1,
    \m00_axis_tdata[0]_0 ,
    \m00_axis_tdata[0]_1 ,
    \m00_axis_tdata[0]_2 ,
    m00_axis_tdata_1_sp_1,
    \m00_axis_tdata[1]_0 ,
    m00_axis_tdata_2_sp_1,
    \m00_axis_tdata[2]_0 ,
    m00_axis_tdata_3_sp_1,
    \m00_axis_tdata[3]_0 ,
    m00_axis_tdata_4_sp_1,
    \m00_axis_tdata[4]_0 ,
    m00_axis_tdata_5_sp_1,
    \m00_axis_tdata[5]_0 ,
    m00_axis_tdata_6_sp_1,
    \m00_axis_tdata[6]_0 ,
    m00_axis_tdata_7_sp_1,
    \m00_axis_tdata[7]_0 ,
    m00_axis_tdata_8_sp_1,
    \m00_axis_tdata[8]_0 ,
    m00_axis_tdata_9_sp_1,
    \m00_axis_tdata[9]_0 ,
    m00_axis_tdata_10_sp_1,
    \m00_axis_tdata[10]_0 ,
    m00_axis_tdata_11_sp_1,
    \m00_axis_tdata[11]_0 ,
    m00_axis_tdata_12_sp_1,
    \m00_axis_tdata[12]_0 ,
    m00_axis_tdata_13_sp_1,
    \m00_axis_tdata[13]_0 ,
    m00_axis_tdata_14_sp_1,
    \m00_axis_tdata[14]_0 ,
    m00_axis_tdata_15_sp_1,
    \m00_axis_tdata[15]_0 ,
    m00_axis_tdata_16_sp_1,
    \m00_axis_tdata[16]_0 ,
    m00_axis_tdata_17_sp_1,
    \m00_axis_tdata[17]_0 ,
    m00_axis_tdata_18_sp_1,
    \m00_axis_tdata[18]_0 ,
    m00_axis_tdata_19_sp_1,
    \m00_axis_tdata[19]_0 ,
    m00_axis_tdata_20_sp_1,
    \m00_axis_tdata[20]_0 ,
    m00_axis_tdata_21_sp_1,
    \m00_axis_tdata[21]_0 ,
    m00_axis_tdata_22_sp_1,
    \m00_axis_tdata[22]_0 ,
    m00_axis_tdata_23_sp_1,
    \m00_axis_tdata[23]_0 ,
    m00_axis_tdata_24_sp_1,
    \m00_axis_tdata[24]_0 ,
    m00_axis_tdata_25_sp_1,
    \m00_axis_tdata[25]_0 ,
    m00_axis_tdata_26_sp_1,
    \m00_axis_tdata[26]_0 ,
    m00_axis_tdata_27_sp_1,
    \m00_axis_tdata[27]_0 ,
    m00_axis_tdata_28_sp_1,
    \m00_axis_tdata[28]_0 ,
    m00_axis_tdata_29_sp_1,
    \m00_axis_tdata[29]_0 ,
    m00_axis_tdata_30_sp_1,
    \m00_axis_tdata[30]_0 ,
    m00_axis_tdata_31_sp_1,
    \m00_axis_tdata[31]_0 ,
    m00_axis_aclk,
    en,
    addr,
    di,
    WE_r,
    square_r);
  output [31:0]m00_axis_tdata;
  output [31:0]dout0_out;
  input m00_axis_tdata_0_sp_1;
  input \m00_axis_tdata[0]_0 ;
  input \m00_axis_tdata[0]_1 ;
  input \m00_axis_tdata[0]_2 ;
  input m00_axis_tdata_1_sp_1;
  input \m00_axis_tdata[1]_0 ;
  input m00_axis_tdata_2_sp_1;
  input \m00_axis_tdata[2]_0 ;
  input m00_axis_tdata_3_sp_1;
  input \m00_axis_tdata[3]_0 ;
  input m00_axis_tdata_4_sp_1;
  input \m00_axis_tdata[4]_0 ;
  input m00_axis_tdata_5_sp_1;
  input \m00_axis_tdata[5]_0 ;
  input m00_axis_tdata_6_sp_1;
  input \m00_axis_tdata[6]_0 ;
  input m00_axis_tdata_7_sp_1;
  input \m00_axis_tdata[7]_0 ;
  input m00_axis_tdata_8_sp_1;
  input \m00_axis_tdata[8]_0 ;
  input m00_axis_tdata_9_sp_1;
  input \m00_axis_tdata[9]_0 ;
  input m00_axis_tdata_10_sp_1;
  input \m00_axis_tdata[10]_0 ;
  input m00_axis_tdata_11_sp_1;
  input \m00_axis_tdata[11]_0 ;
  input m00_axis_tdata_12_sp_1;
  input \m00_axis_tdata[12]_0 ;
  input m00_axis_tdata_13_sp_1;
  input \m00_axis_tdata[13]_0 ;
  input m00_axis_tdata_14_sp_1;
  input \m00_axis_tdata[14]_0 ;
  input m00_axis_tdata_15_sp_1;
  input \m00_axis_tdata[15]_0 ;
  input m00_axis_tdata_16_sp_1;
  input \m00_axis_tdata[16]_0 ;
  input m00_axis_tdata_17_sp_1;
  input \m00_axis_tdata[17]_0 ;
  input m00_axis_tdata_18_sp_1;
  input \m00_axis_tdata[18]_0 ;
  input m00_axis_tdata_19_sp_1;
  input \m00_axis_tdata[19]_0 ;
  input m00_axis_tdata_20_sp_1;
  input \m00_axis_tdata[20]_0 ;
  input m00_axis_tdata_21_sp_1;
  input \m00_axis_tdata[21]_0 ;
  input m00_axis_tdata_22_sp_1;
  input \m00_axis_tdata[22]_0 ;
  input m00_axis_tdata_23_sp_1;
  input \m00_axis_tdata[23]_0 ;
  input m00_axis_tdata_24_sp_1;
  input \m00_axis_tdata[24]_0 ;
  input m00_axis_tdata_25_sp_1;
  input \m00_axis_tdata[25]_0 ;
  input m00_axis_tdata_26_sp_1;
  input \m00_axis_tdata[26]_0 ;
  input m00_axis_tdata_27_sp_1;
  input \m00_axis_tdata[27]_0 ;
  input m00_axis_tdata_28_sp_1;
  input \m00_axis_tdata[28]_0 ;
  input m00_axis_tdata_29_sp_1;
  input \m00_axis_tdata[29]_0 ;
  input m00_axis_tdata_30_sp_1;
  input \m00_axis_tdata[30]_0 ;
  input m00_axis_tdata_31_sp_1;
  input \m00_axis_tdata[31]_0 ;
  input m00_axis_aclk;
  input en;
  input [10:0]addr;
  input [31:0]di;
  input WE_r;
  input square_r;

  wire WE_r;
  wire [10:0]addr;
  wire [31:0]di;
  wire [31:0]dout0_out;
  wire en;
  wire m00_axis_aclk;
  wire [31:0]m00_axis_tdata;
  wire \m00_axis_tdata[0]_0 ;
  wire \m00_axis_tdata[0]_1 ;
  wire \m00_axis_tdata[0]_2 ;
  wire \m00_axis_tdata[10]_0 ;
  wire \m00_axis_tdata[11]_0 ;
  wire \m00_axis_tdata[12]_0 ;
  wire \m00_axis_tdata[13]_0 ;
  wire \m00_axis_tdata[14]_0 ;
  wire \m00_axis_tdata[15]_0 ;
  wire \m00_axis_tdata[16]_0 ;
  wire \m00_axis_tdata[17]_0 ;
  wire \m00_axis_tdata[18]_0 ;
  wire \m00_axis_tdata[19]_0 ;
  wire \m00_axis_tdata[1]_0 ;
  wire \m00_axis_tdata[20]_0 ;
  wire \m00_axis_tdata[21]_0 ;
  wire \m00_axis_tdata[22]_0 ;
  wire \m00_axis_tdata[23]_0 ;
  wire \m00_axis_tdata[24]_0 ;
  wire \m00_axis_tdata[25]_0 ;
  wire \m00_axis_tdata[26]_0 ;
  wire \m00_axis_tdata[27]_0 ;
  wire \m00_axis_tdata[28]_0 ;
  wire \m00_axis_tdata[29]_0 ;
  wire \m00_axis_tdata[2]_0 ;
  wire \m00_axis_tdata[30]_0 ;
  wire \m00_axis_tdata[31]_0 ;
  wire \m00_axis_tdata[3]_0 ;
  wire \m00_axis_tdata[4]_0 ;
  wire \m00_axis_tdata[5]_0 ;
  wire \m00_axis_tdata[6]_0 ;
  wire \m00_axis_tdata[7]_0 ;
  wire \m00_axis_tdata[8]_0 ;
  wire \m00_axis_tdata[9]_0 ;
  wire m00_axis_tdata_0_sn_1;
  wire m00_axis_tdata_10_sn_1;
  wire m00_axis_tdata_11_sn_1;
  wire m00_axis_tdata_12_sn_1;
  wire m00_axis_tdata_13_sn_1;
  wire m00_axis_tdata_14_sn_1;
  wire m00_axis_tdata_15_sn_1;
  wire m00_axis_tdata_16_sn_1;
  wire m00_axis_tdata_17_sn_1;
  wire m00_axis_tdata_18_sn_1;
  wire m00_axis_tdata_19_sn_1;
  wire m00_axis_tdata_1_sn_1;
  wire m00_axis_tdata_20_sn_1;
  wire m00_axis_tdata_21_sn_1;
  wire m00_axis_tdata_22_sn_1;
  wire m00_axis_tdata_23_sn_1;
  wire m00_axis_tdata_24_sn_1;
  wire m00_axis_tdata_25_sn_1;
  wire m00_axis_tdata_26_sn_1;
  wire m00_axis_tdata_27_sn_1;
  wire m00_axis_tdata_28_sn_1;
  wire m00_axis_tdata_29_sn_1;
  wire m00_axis_tdata_2_sn_1;
  wire m00_axis_tdata_30_sn_1;
  wire m00_axis_tdata_31_sn_1;
  wire m00_axis_tdata_3_sn_1;
  wire m00_axis_tdata_4_sn_1;
  wire m00_axis_tdata_5_sn_1;
  wire m00_axis_tdata_6_sn_1;
  wire m00_axis_tdata_7_sn_1;
  wire m00_axis_tdata_8_sn_1;
  wire m00_axis_tdata_9_sn_1;
  wire ram_reg_0_i_1__2_n_0;
  wire square_r;
  wire NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED;
  wire NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED;
  wire NLW_ram_reg_0_DBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_SBITERR_UNCONNECTED;
  wire [31:16]NLW_ram_reg_0_DOADO_UNCONNECTED;
  wire [31:0]NLW_ram_reg_0_DOBDO_UNCONNECTED;
  wire [3:2]NLW_ram_reg_0_DOPADOP_UNCONNECTED;
  wire [3:0]NLW_ram_reg_0_DOPBDOP_UNCONNECTED;
  wire [7:0]NLW_ram_reg_0_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_ram_reg_0_RDADDRECC_UNCONNECTED;
  wire NLW_ram_reg_1_CASCADEOUTA_UNCONNECTED;
  wire NLW_ram_reg_1_CASCADEOUTB_UNCONNECTED;
  wire NLW_ram_reg_1_DBITERR_UNCONNECTED;
  wire NLW_ram_reg_1_INJECTDBITERR_UNCONNECTED;
  wire NLW_ram_reg_1_INJECTSBITERR_UNCONNECTED;
  wire NLW_ram_reg_1_SBITERR_UNCONNECTED;
  wire [31:14]NLW_ram_reg_1_DOADO_UNCONNECTED;
  wire [31:0]NLW_ram_reg_1_DOBDO_UNCONNECTED;
  wire [3:0]NLW_ram_reg_1_DOPADOP_UNCONNECTED;
  wire [3:0]NLW_ram_reg_1_DOPBDOP_UNCONNECTED;
  wire [7:0]NLW_ram_reg_1_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_ram_reg_1_RDADDRECC_UNCONNECTED;

  assign m00_axis_tdata_0_sn_1 = m00_axis_tdata_0_sp_1;
  assign m00_axis_tdata_10_sn_1 = m00_axis_tdata_10_sp_1;
  assign m00_axis_tdata_11_sn_1 = m00_axis_tdata_11_sp_1;
  assign m00_axis_tdata_12_sn_1 = m00_axis_tdata_12_sp_1;
  assign m00_axis_tdata_13_sn_1 = m00_axis_tdata_13_sp_1;
  assign m00_axis_tdata_14_sn_1 = m00_axis_tdata_14_sp_1;
  assign m00_axis_tdata_15_sn_1 = m00_axis_tdata_15_sp_1;
  assign m00_axis_tdata_16_sn_1 = m00_axis_tdata_16_sp_1;
  assign m00_axis_tdata_17_sn_1 = m00_axis_tdata_17_sp_1;
  assign m00_axis_tdata_18_sn_1 = m00_axis_tdata_18_sp_1;
  assign m00_axis_tdata_19_sn_1 = m00_axis_tdata_19_sp_1;
  assign m00_axis_tdata_1_sn_1 = m00_axis_tdata_1_sp_1;
  assign m00_axis_tdata_20_sn_1 = m00_axis_tdata_20_sp_1;
  assign m00_axis_tdata_21_sn_1 = m00_axis_tdata_21_sp_1;
  assign m00_axis_tdata_22_sn_1 = m00_axis_tdata_22_sp_1;
  assign m00_axis_tdata_23_sn_1 = m00_axis_tdata_23_sp_1;
  assign m00_axis_tdata_24_sn_1 = m00_axis_tdata_24_sp_1;
  assign m00_axis_tdata_25_sn_1 = m00_axis_tdata_25_sp_1;
  assign m00_axis_tdata_26_sn_1 = m00_axis_tdata_26_sp_1;
  assign m00_axis_tdata_27_sn_1 = m00_axis_tdata_27_sp_1;
  assign m00_axis_tdata_28_sn_1 = m00_axis_tdata_28_sp_1;
  assign m00_axis_tdata_29_sn_1 = m00_axis_tdata_29_sp_1;
  assign m00_axis_tdata_2_sn_1 = m00_axis_tdata_2_sp_1;
  assign m00_axis_tdata_30_sn_1 = m00_axis_tdata_30_sp_1;
  assign m00_axis_tdata_31_sn_1 = m00_axis_tdata_31_sp_1;
  assign m00_axis_tdata_3_sn_1 = m00_axis_tdata_3_sp_1;
  assign m00_axis_tdata_4_sn_1 = m00_axis_tdata_4_sp_1;
  assign m00_axis_tdata_5_sn_1 = m00_axis_tdata_5_sp_1;
  assign m00_axis_tdata_6_sn_1 = m00_axis_tdata_6_sp_1;
  assign m00_axis_tdata_7_sn_1 = m00_axis_tdata_7_sp_1;
  assign m00_axis_tdata_8_sn_1 = m00_axis_tdata_8_sp_1;
  assign m00_axis_tdata_9_sn_1 = m00_axis_tdata_9_sp_1;
  LUT4 #(
    .INIT(16'h00E2)) 
    \m00_axis_tdata[0]_INST_0 
       (.I0(m00_axis_tdata_0_sn_1),
        .I1(\m00_axis_tdata[0]_0 ),
        .I2(\m00_axis_tdata[0]_1 ),
        .I3(\m00_axis_tdata[0]_2 ),
        .O(m00_axis_tdata[0]));
  LUT4 #(
    .INIT(16'h00E2)) 
    \m00_axis_tdata[10]_INST_0 
       (.I0(m00_axis_tdata_10_sn_1),
        .I1(\m00_axis_tdata[0]_0 ),
        .I2(\m00_axis_tdata[10]_0 ),
        .I3(\m00_axis_tdata[0]_2 ),
        .O(m00_axis_tdata[10]));
  LUT4 #(
    .INIT(16'h00E2)) 
    \m00_axis_tdata[11]_INST_0 
       (.I0(m00_axis_tdata_11_sn_1),
        .I1(\m00_axis_tdata[0]_0 ),
        .I2(\m00_axis_tdata[11]_0 ),
        .I3(\m00_axis_tdata[0]_2 ),
        .O(m00_axis_tdata[11]));
  LUT4 #(
    .INIT(16'h00E2)) 
    \m00_axis_tdata[12]_INST_0 
       (.I0(m00_axis_tdata_12_sn_1),
        .I1(\m00_axis_tdata[0]_0 ),
        .I2(\m00_axis_tdata[12]_0 ),
        .I3(\m00_axis_tdata[0]_2 ),
        .O(m00_axis_tdata[12]));
  LUT4 #(
    .INIT(16'h00E2)) 
    \m00_axis_tdata[13]_INST_0 
       (.I0(m00_axis_tdata_13_sn_1),
        .I1(\m00_axis_tdata[0]_0 ),
        .I2(\m00_axis_tdata[13]_0 ),
        .I3(\m00_axis_tdata[0]_2 ),
        .O(m00_axis_tdata[13]));
  LUT4 #(
    .INIT(16'h00E2)) 
    \m00_axis_tdata[14]_INST_0 
       (.I0(m00_axis_tdata_14_sn_1),
        .I1(\m00_axis_tdata[0]_0 ),
        .I2(\m00_axis_tdata[14]_0 ),
        .I3(\m00_axis_tdata[0]_2 ),
        .O(m00_axis_tdata[14]));
  LUT4 #(
    .INIT(16'h00E2)) 
    \m00_axis_tdata[15]_INST_0 
       (.I0(m00_axis_tdata_15_sn_1),
        .I1(\m00_axis_tdata[0]_0 ),
        .I2(\m00_axis_tdata[15]_0 ),
        .I3(\m00_axis_tdata[0]_2 ),
        .O(m00_axis_tdata[15]));
  LUT4 #(
    .INIT(16'h00E2)) 
    \m00_axis_tdata[16]_INST_0 
       (.I0(m00_axis_tdata_16_sn_1),
        .I1(\m00_axis_tdata[0]_0 ),
        .I2(\m00_axis_tdata[16]_0 ),
        .I3(\m00_axis_tdata[0]_2 ),
        .O(m00_axis_tdata[16]));
  LUT4 #(
    .INIT(16'h00E2)) 
    \m00_axis_tdata[17]_INST_0 
       (.I0(m00_axis_tdata_17_sn_1),
        .I1(\m00_axis_tdata[0]_0 ),
        .I2(\m00_axis_tdata[17]_0 ),
        .I3(\m00_axis_tdata[0]_2 ),
        .O(m00_axis_tdata[17]));
  LUT4 #(
    .INIT(16'h00E2)) 
    \m00_axis_tdata[18]_INST_0 
       (.I0(m00_axis_tdata_18_sn_1),
        .I1(\m00_axis_tdata[0]_0 ),
        .I2(\m00_axis_tdata[18]_0 ),
        .I3(\m00_axis_tdata[0]_2 ),
        .O(m00_axis_tdata[18]));
  LUT4 #(
    .INIT(16'h00E2)) 
    \m00_axis_tdata[19]_INST_0 
       (.I0(m00_axis_tdata_19_sn_1),
        .I1(\m00_axis_tdata[0]_0 ),
        .I2(\m00_axis_tdata[19]_0 ),
        .I3(\m00_axis_tdata[0]_2 ),
        .O(m00_axis_tdata[19]));
  LUT4 #(
    .INIT(16'h00E2)) 
    \m00_axis_tdata[1]_INST_0 
       (.I0(m00_axis_tdata_1_sn_1),
        .I1(\m00_axis_tdata[0]_0 ),
        .I2(\m00_axis_tdata[1]_0 ),
        .I3(\m00_axis_tdata[0]_2 ),
        .O(m00_axis_tdata[1]));
  LUT4 #(
    .INIT(16'h00E2)) 
    \m00_axis_tdata[20]_INST_0 
       (.I0(m00_axis_tdata_20_sn_1),
        .I1(\m00_axis_tdata[0]_0 ),
        .I2(\m00_axis_tdata[20]_0 ),
        .I3(\m00_axis_tdata[0]_2 ),
        .O(m00_axis_tdata[20]));
  LUT4 #(
    .INIT(16'h00E2)) 
    \m00_axis_tdata[21]_INST_0 
       (.I0(m00_axis_tdata_21_sn_1),
        .I1(\m00_axis_tdata[0]_0 ),
        .I2(\m00_axis_tdata[21]_0 ),
        .I3(\m00_axis_tdata[0]_2 ),
        .O(m00_axis_tdata[21]));
  LUT4 #(
    .INIT(16'h00E2)) 
    \m00_axis_tdata[22]_INST_0 
       (.I0(m00_axis_tdata_22_sn_1),
        .I1(\m00_axis_tdata[0]_0 ),
        .I2(\m00_axis_tdata[22]_0 ),
        .I3(\m00_axis_tdata[0]_2 ),
        .O(m00_axis_tdata[22]));
  LUT4 #(
    .INIT(16'h00E2)) 
    \m00_axis_tdata[23]_INST_0 
       (.I0(m00_axis_tdata_23_sn_1),
        .I1(\m00_axis_tdata[0]_0 ),
        .I2(\m00_axis_tdata[23]_0 ),
        .I3(\m00_axis_tdata[0]_2 ),
        .O(m00_axis_tdata[23]));
  LUT4 #(
    .INIT(16'h00E2)) 
    \m00_axis_tdata[24]_INST_0 
       (.I0(m00_axis_tdata_24_sn_1),
        .I1(\m00_axis_tdata[0]_0 ),
        .I2(\m00_axis_tdata[24]_0 ),
        .I3(\m00_axis_tdata[0]_2 ),
        .O(m00_axis_tdata[24]));
  LUT4 #(
    .INIT(16'h00E2)) 
    \m00_axis_tdata[25]_INST_0 
       (.I0(m00_axis_tdata_25_sn_1),
        .I1(\m00_axis_tdata[0]_0 ),
        .I2(\m00_axis_tdata[25]_0 ),
        .I3(\m00_axis_tdata[0]_2 ),
        .O(m00_axis_tdata[25]));
  LUT4 #(
    .INIT(16'h00E2)) 
    \m00_axis_tdata[26]_INST_0 
       (.I0(m00_axis_tdata_26_sn_1),
        .I1(\m00_axis_tdata[0]_0 ),
        .I2(\m00_axis_tdata[26]_0 ),
        .I3(\m00_axis_tdata[0]_2 ),
        .O(m00_axis_tdata[26]));
  LUT4 #(
    .INIT(16'h00E2)) 
    \m00_axis_tdata[27]_INST_0 
       (.I0(m00_axis_tdata_27_sn_1),
        .I1(\m00_axis_tdata[0]_0 ),
        .I2(\m00_axis_tdata[27]_0 ),
        .I3(\m00_axis_tdata[0]_2 ),
        .O(m00_axis_tdata[27]));
  LUT4 #(
    .INIT(16'h00E2)) 
    \m00_axis_tdata[28]_INST_0 
       (.I0(m00_axis_tdata_28_sn_1),
        .I1(\m00_axis_tdata[0]_0 ),
        .I2(\m00_axis_tdata[28]_0 ),
        .I3(\m00_axis_tdata[0]_2 ),
        .O(m00_axis_tdata[28]));
  LUT4 #(
    .INIT(16'h00E2)) 
    \m00_axis_tdata[29]_INST_0 
       (.I0(m00_axis_tdata_29_sn_1),
        .I1(\m00_axis_tdata[0]_0 ),
        .I2(\m00_axis_tdata[29]_0 ),
        .I3(\m00_axis_tdata[0]_2 ),
        .O(m00_axis_tdata[29]));
  LUT4 #(
    .INIT(16'h00E2)) 
    \m00_axis_tdata[2]_INST_0 
       (.I0(m00_axis_tdata_2_sn_1),
        .I1(\m00_axis_tdata[0]_0 ),
        .I2(\m00_axis_tdata[2]_0 ),
        .I3(\m00_axis_tdata[0]_2 ),
        .O(m00_axis_tdata[2]));
  LUT4 #(
    .INIT(16'h00E2)) 
    \m00_axis_tdata[30]_INST_0 
       (.I0(m00_axis_tdata_30_sn_1),
        .I1(\m00_axis_tdata[0]_0 ),
        .I2(\m00_axis_tdata[30]_0 ),
        .I3(\m00_axis_tdata[0]_2 ),
        .O(m00_axis_tdata[30]));
  LUT4 #(
    .INIT(16'h00E2)) 
    \m00_axis_tdata[31]_INST_0 
       (.I0(m00_axis_tdata_31_sn_1),
        .I1(\m00_axis_tdata[0]_0 ),
        .I2(\m00_axis_tdata[31]_0 ),
        .I3(\m00_axis_tdata[0]_2 ),
        .O(m00_axis_tdata[31]));
  LUT4 #(
    .INIT(16'h00E2)) 
    \m00_axis_tdata[3]_INST_0 
       (.I0(m00_axis_tdata_3_sn_1),
        .I1(\m00_axis_tdata[0]_0 ),
        .I2(\m00_axis_tdata[3]_0 ),
        .I3(\m00_axis_tdata[0]_2 ),
        .O(m00_axis_tdata[3]));
  LUT4 #(
    .INIT(16'h00E2)) 
    \m00_axis_tdata[4]_INST_0 
       (.I0(m00_axis_tdata_4_sn_1),
        .I1(\m00_axis_tdata[0]_0 ),
        .I2(\m00_axis_tdata[4]_0 ),
        .I3(\m00_axis_tdata[0]_2 ),
        .O(m00_axis_tdata[4]));
  LUT4 #(
    .INIT(16'h00E2)) 
    \m00_axis_tdata[5]_INST_0 
       (.I0(m00_axis_tdata_5_sn_1),
        .I1(\m00_axis_tdata[0]_0 ),
        .I2(\m00_axis_tdata[5]_0 ),
        .I3(\m00_axis_tdata[0]_2 ),
        .O(m00_axis_tdata[5]));
  LUT4 #(
    .INIT(16'h00E2)) 
    \m00_axis_tdata[6]_INST_0 
       (.I0(m00_axis_tdata_6_sn_1),
        .I1(\m00_axis_tdata[0]_0 ),
        .I2(\m00_axis_tdata[6]_0 ),
        .I3(\m00_axis_tdata[0]_2 ),
        .O(m00_axis_tdata[6]));
  LUT4 #(
    .INIT(16'h00E2)) 
    \m00_axis_tdata[7]_INST_0 
       (.I0(m00_axis_tdata_7_sn_1),
        .I1(\m00_axis_tdata[0]_0 ),
        .I2(\m00_axis_tdata[7]_0 ),
        .I3(\m00_axis_tdata[0]_2 ),
        .O(m00_axis_tdata[7]));
  LUT4 #(
    .INIT(16'h00E2)) 
    \m00_axis_tdata[8]_INST_0 
       (.I0(m00_axis_tdata_8_sn_1),
        .I1(\m00_axis_tdata[0]_0 ),
        .I2(\m00_axis_tdata[8]_0 ),
        .I3(\m00_axis_tdata[0]_2 ),
        .O(m00_axis_tdata[8]));
  LUT4 #(
    .INIT(16'h00E2)) 
    \m00_axis_tdata[9]_INST_0 
       (.I0(m00_axis_tdata_9_sn_1),
        .I1(\m00_axis_tdata[0]_0 ),
        .I2(\m00_axis_tdata[9]_0 ),
        .I3(\m00_axis_tdata[0]_2 ),
        .O(m00_axis_tdata[9]));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p2_d16" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "61472" *) 
  (* RTL_RAM_NAME = "ram" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "2047" *) 
  (* bram_slice_begin = "0" *) 
  (* bram_slice_end = "17" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "2047" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "17" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(18),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(18),
    .WRITE_WIDTH_B(0)) 
    ram_reg_0
       (.ADDRARDADDR({1'b1,addr,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CASCADEINA(1'b1),
        .CASCADEINB(1'b0),
        .CASCADEOUTA(NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED),
        .CASCADEOUTB(NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED),
        .CLKARDCLK(m00_axis_aclk),
        .CLKBWRCLK(1'b0),
        .DBITERR(NLW_ram_reg_0_DBITERR_UNCONNECTED),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,di[15:0]}),
        .DIBDI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0,di[17:16]}),
        .DIPBDIP({1'b1,1'b1,1'b1,1'b1}),
        .DOADO({NLW_ram_reg_0_DOADO_UNCONNECTED[31:16],dout0_out[15:0]}),
        .DOBDO(NLW_ram_reg_0_DOBDO_UNCONNECTED[31:0]),
        .DOPADOP({NLW_ram_reg_0_DOPADOP_UNCONNECTED[3:2],dout0_out[17:16]}),
        .DOPBDOP(NLW_ram_reg_0_DOPBDOP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_ram_reg_0_ECCPARITY_UNCONNECTED[7:0]),
        .ENARDEN(en),
        .ENBWREN(1'b0),
        .INJECTDBITERR(NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED),
        .INJECTSBITERR(NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED),
        .RDADDRECC(NLW_ram_reg_0_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_ram_reg_0_SBITERR_UNCONNECTED),
        .WEA({ram_reg_0_i_1__2_n_0,ram_reg_0_i_1__2_n_0,ram_reg_0_i_1__2_n_0,ram_reg_0_i_1__2_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
  LUT3 #(
    .INIT(8'h80)) 
    ram_reg_0_i_1__2
       (.I0(en),
        .I1(WE_r),
        .I2(square_r),
        .O(ram_reg_0_i_1__2_n_0));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d14" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "61472" *) 
  (* RTL_RAM_NAME = "ram" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "2047" *) 
  (* bram_slice_begin = "18" *) 
  (* bram_slice_end = "31" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "2047" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "18" *) 
  (* ram_slice_end = "31" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(18),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(18),
    .WRITE_WIDTH_B(0)) 
    ram_reg_1
       (.ADDRARDADDR({1'b1,addr,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CASCADEINA(1'b1),
        .CASCADEINB(1'b0),
        .CASCADEOUTA(NLW_ram_reg_1_CASCADEOUTA_UNCONNECTED),
        .CASCADEOUTB(NLW_ram_reg_1_CASCADEOUTB_UNCONNECTED),
        .CLKARDCLK(m00_axis_aclk),
        .CLKBWRCLK(1'b0),
        .DBITERR(NLW_ram_reg_1_DBITERR_UNCONNECTED),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,di[31:18]}),
        .DIBDI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0,1'b0,1'b0}),
        .DIPBDIP({1'b1,1'b1,1'b1,1'b1}),
        .DOADO({NLW_ram_reg_1_DOADO_UNCONNECTED[31:14],dout0_out[31:18]}),
        .DOBDO(NLW_ram_reg_1_DOBDO_UNCONNECTED[31:0]),
        .DOPADOP(NLW_ram_reg_1_DOPADOP_UNCONNECTED[3:0]),
        .DOPBDOP(NLW_ram_reg_1_DOPBDOP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_ram_reg_1_ECCPARITY_UNCONNECTED[7:0]),
        .ENARDEN(en),
        .ENBWREN(1'b0),
        .INJECTDBITERR(NLW_ram_reg_1_INJECTDBITERR_UNCONNECTED),
        .INJECTSBITERR(NLW_ram_reg_1_INJECTSBITERR_UNCONNECTED),
        .RDADDRECC(NLW_ram_reg_1_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_ram_reg_1_SBITERR_UNCONNECTED),
        .WEA({ram_reg_0_i_1__2_n_0,ram_reg_0_i_1__2_n_0,ram_reg_0_i_1__2_n_0,ram_reg_0_i_1__2_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
endmodule

(* ORIG_REF_NAME = "rams_sp_rf_rst" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_3
   (dout0_out,
    m00_axis_aclk,
    en,
    addr,
    di,
    WE_r,
    square_r);
  output [31:0]dout0_out;
  input m00_axis_aclk;
  input en;
  input [10:0]addr;
  input [31:0]di;
  input WE_r;
  input square_r;

  wire WE_r;
  wire [10:0]addr;
  wire [31:0]di;
  wire [31:0]dout0_out;
  wire en;
  wire m00_axis_aclk;
  wire ram_reg_0_i_1__3_n_0;
  wire square_r;
  wire NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED;
  wire NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED;
  wire NLW_ram_reg_0_DBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_SBITERR_UNCONNECTED;
  wire [31:16]NLW_ram_reg_0_DOADO_UNCONNECTED;
  wire [31:0]NLW_ram_reg_0_DOBDO_UNCONNECTED;
  wire [3:2]NLW_ram_reg_0_DOPADOP_UNCONNECTED;
  wire [3:0]NLW_ram_reg_0_DOPBDOP_UNCONNECTED;
  wire [7:0]NLW_ram_reg_0_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_ram_reg_0_RDADDRECC_UNCONNECTED;
  wire NLW_ram_reg_1_CASCADEOUTA_UNCONNECTED;
  wire NLW_ram_reg_1_CASCADEOUTB_UNCONNECTED;
  wire NLW_ram_reg_1_DBITERR_UNCONNECTED;
  wire NLW_ram_reg_1_INJECTDBITERR_UNCONNECTED;
  wire NLW_ram_reg_1_INJECTSBITERR_UNCONNECTED;
  wire NLW_ram_reg_1_SBITERR_UNCONNECTED;
  wire [31:14]NLW_ram_reg_1_DOADO_UNCONNECTED;
  wire [31:0]NLW_ram_reg_1_DOBDO_UNCONNECTED;
  wire [3:0]NLW_ram_reg_1_DOPADOP_UNCONNECTED;
  wire [3:0]NLW_ram_reg_1_DOPBDOP_UNCONNECTED;
  wire [7:0]NLW_ram_reg_1_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_ram_reg_1_RDADDRECC_UNCONNECTED;

  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p2_d16" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "61472" *) 
  (* RTL_RAM_NAME = "ram" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "2047" *) 
  (* bram_slice_begin = "0" *) 
  (* bram_slice_end = "17" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "2047" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "17" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(18),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(18),
    .WRITE_WIDTH_B(0)) 
    ram_reg_0
       (.ADDRARDADDR({1'b1,addr,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CASCADEINA(1'b1),
        .CASCADEINB(1'b0),
        .CASCADEOUTA(NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED),
        .CASCADEOUTB(NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED),
        .CLKARDCLK(m00_axis_aclk),
        .CLKBWRCLK(1'b0),
        .DBITERR(NLW_ram_reg_0_DBITERR_UNCONNECTED),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,di[15:0]}),
        .DIBDI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0,di[17:16]}),
        .DIPBDIP({1'b1,1'b1,1'b1,1'b1}),
        .DOADO({NLW_ram_reg_0_DOADO_UNCONNECTED[31:16],dout0_out[15:0]}),
        .DOBDO(NLW_ram_reg_0_DOBDO_UNCONNECTED[31:0]),
        .DOPADOP({NLW_ram_reg_0_DOPADOP_UNCONNECTED[3:2],dout0_out[17:16]}),
        .DOPBDOP(NLW_ram_reg_0_DOPBDOP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_ram_reg_0_ECCPARITY_UNCONNECTED[7:0]),
        .ENARDEN(en),
        .ENBWREN(1'b0),
        .INJECTDBITERR(NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED),
        .INJECTSBITERR(NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED),
        .RDADDRECC(NLW_ram_reg_0_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_ram_reg_0_SBITERR_UNCONNECTED),
        .WEA({ram_reg_0_i_1__3_n_0,ram_reg_0_i_1__3_n_0,ram_reg_0_i_1__3_n_0,ram_reg_0_i_1__3_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
  LUT3 #(
    .INIT(8'h80)) 
    ram_reg_0_i_1__3
       (.I0(en),
        .I1(WE_r),
        .I2(square_r),
        .O(ram_reg_0_i_1__3_n_0));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d14" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "61472" *) 
  (* RTL_RAM_NAME = "ram" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "2047" *) 
  (* bram_slice_begin = "18" *) 
  (* bram_slice_end = "31" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "2047" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "18" *) 
  (* ram_slice_end = "31" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(18),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(18),
    .WRITE_WIDTH_B(0)) 
    ram_reg_1
       (.ADDRARDADDR({1'b1,addr,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CASCADEINA(1'b1),
        .CASCADEINB(1'b0),
        .CASCADEOUTA(NLW_ram_reg_1_CASCADEOUTA_UNCONNECTED),
        .CASCADEOUTB(NLW_ram_reg_1_CASCADEOUTB_UNCONNECTED),
        .CLKARDCLK(m00_axis_aclk),
        .CLKBWRCLK(1'b0),
        .DBITERR(NLW_ram_reg_1_DBITERR_UNCONNECTED),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,di[31:18]}),
        .DIBDI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0,1'b0,1'b0}),
        .DIPBDIP({1'b1,1'b1,1'b1,1'b1}),
        .DOADO({NLW_ram_reg_1_DOADO_UNCONNECTED[31:14],dout0_out[31:18]}),
        .DOBDO(NLW_ram_reg_1_DOBDO_UNCONNECTED[31:0]),
        .DOPADOP(NLW_ram_reg_1_DOPADOP_UNCONNECTED[3:0]),
        .DOPBDOP(NLW_ram_reg_1_DOPBDOP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_ram_reg_1_ECCPARITY_UNCONNECTED[7:0]),
        .ENARDEN(en),
        .ENBWREN(1'b0),
        .INJECTDBITERR(NLW_ram_reg_1_INJECTDBITERR_UNCONNECTED),
        .INJECTSBITERR(NLW_ram_reg_1_INJECTSBITERR_UNCONNECTED),
        .RDADDRECC(NLW_ram_reg_1_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_ram_reg_1_SBITERR_UNCONNECTED),
        .WEA({ram_reg_0_i_1__3_n_0,ram_reg_0_i_1__3_n_0,ram_reg_0_i_1__3_n_0,ram_reg_0_i_1__3_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
endmodule

(* ORIG_REF_NAME = "rams_sp_rf_rst" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_4
   (dout0_out,
    m00_axis_aclk,
    en,
    addr,
    di,
    WE_r,
    square_r);
  output [31:0]dout0_out;
  input m00_axis_aclk;
  input en;
  input [10:0]addr;
  input [31:0]di;
  input WE_r;
  input square_r;

  wire WE_r;
  wire [10:0]addr;
  wire [31:0]di;
  wire [31:0]dout0_out;
  wire en;
  wire m00_axis_aclk;
  wire ram_reg_0_i_1__4_n_0;
  wire square_r;
  wire NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED;
  wire NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED;
  wire NLW_ram_reg_0_DBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_SBITERR_UNCONNECTED;
  wire [31:16]NLW_ram_reg_0_DOADO_UNCONNECTED;
  wire [31:0]NLW_ram_reg_0_DOBDO_UNCONNECTED;
  wire [3:2]NLW_ram_reg_0_DOPADOP_UNCONNECTED;
  wire [3:0]NLW_ram_reg_0_DOPBDOP_UNCONNECTED;
  wire [7:0]NLW_ram_reg_0_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_ram_reg_0_RDADDRECC_UNCONNECTED;
  wire NLW_ram_reg_1_CASCADEOUTA_UNCONNECTED;
  wire NLW_ram_reg_1_CASCADEOUTB_UNCONNECTED;
  wire NLW_ram_reg_1_DBITERR_UNCONNECTED;
  wire NLW_ram_reg_1_INJECTDBITERR_UNCONNECTED;
  wire NLW_ram_reg_1_INJECTSBITERR_UNCONNECTED;
  wire NLW_ram_reg_1_SBITERR_UNCONNECTED;
  wire [31:14]NLW_ram_reg_1_DOADO_UNCONNECTED;
  wire [31:0]NLW_ram_reg_1_DOBDO_UNCONNECTED;
  wire [3:0]NLW_ram_reg_1_DOPADOP_UNCONNECTED;
  wire [3:0]NLW_ram_reg_1_DOPBDOP_UNCONNECTED;
  wire [7:0]NLW_ram_reg_1_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_ram_reg_1_RDADDRECC_UNCONNECTED;

  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p2_d16" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "61472" *) 
  (* RTL_RAM_NAME = "ram" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "2047" *) 
  (* bram_slice_begin = "0" *) 
  (* bram_slice_end = "17" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "2047" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "17" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(18),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(18),
    .WRITE_WIDTH_B(0)) 
    ram_reg_0
       (.ADDRARDADDR({1'b1,addr,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CASCADEINA(1'b1),
        .CASCADEINB(1'b0),
        .CASCADEOUTA(NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED),
        .CASCADEOUTB(NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED),
        .CLKARDCLK(m00_axis_aclk),
        .CLKBWRCLK(1'b0),
        .DBITERR(NLW_ram_reg_0_DBITERR_UNCONNECTED),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,di[15:0]}),
        .DIBDI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0,di[17:16]}),
        .DIPBDIP({1'b1,1'b1,1'b1,1'b1}),
        .DOADO({NLW_ram_reg_0_DOADO_UNCONNECTED[31:16],dout0_out[15:0]}),
        .DOBDO(NLW_ram_reg_0_DOBDO_UNCONNECTED[31:0]),
        .DOPADOP({NLW_ram_reg_0_DOPADOP_UNCONNECTED[3:2],dout0_out[17:16]}),
        .DOPBDOP(NLW_ram_reg_0_DOPBDOP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_ram_reg_0_ECCPARITY_UNCONNECTED[7:0]),
        .ENARDEN(en),
        .ENBWREN(1'b0),
        .INJECTDBITERR(NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED),
        .INJECTSBITERR(NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED),
        .RDADDRECC(NLW_ram_reg_0_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_ram_reg_0_SBITERR_UNCONNECTED),
        .WEA({ram_reg_0_i_1__4_n_0,ram_reg_0_i_1__4_n_0,ram_reg_0_i_1__4_n_0,ram_reg_0_i_1__4_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
  LUT3 #(
    .INIT(8'h80)) 
    ram_reg_0_i_1__4
       (.I0(en),
        .I1(WE_r),
        .I2(square_r),
        .O(ram_reg_0_i_1__4_n_0));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d14" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "61472" *) 
  (* RTL_RAM_NAME = "ram" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "2047" *) 
  (* bram_slice_begin = "18" *) 
  (* bram_slice_end = "31" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "2047" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "18" *) 
  (* ram_slice_end = "31" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(18),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(18),
    .WRITE_WIDTH_B(0)) 
    ram_reg_1
       (.ADDRARDADDR({1'b1,addr,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CASCADEINA(1'b1),
        .CASCADEINB(1'b0),
        .CASCADEOUTA(NLW_ram_reg_1_CASCADEOUTA_UNCONNECTED),
        .CASCADEOUTB(NLW_ram_reg_1_CASCADEOUTB_UNCONNECTED),
        .CLKARDCLK(m00_axis_aclk),
        .CLKBWRCLK(1'b0),
        .DBITERR(NLW_ram_reg_1_DBITERR_UNCONNECTED),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,di[31:18]}),
        .DIBDI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0,1'b0,1'b0}),
        .DIPBDIP({1'b1,1'b1,1'b1,1'b1}),
        .DOADO({NLW_ram_reg_1_DOADO_UNCONNECTED[31:14],dout0_out[31:18]}),
        .DOBDO(NLW_ram_reg_1_DOBDO_UNCONNECTED[31:0]),
        .DOPADOP(NLW_ram_reg_1_DOPADOP_UNCONNECTED[3:0]),
        .DOPBDOP(NLW_ram_reg_1_DOPBDOP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_ram_reg_1_ECCPARITY_UNCONNECTED[7:0]),
        .ENARDEN(en),
        .ENBWREN(1'b0),
        .INJECTDBITERR(NLW_ram_reg_1_INJECTDBITERR_UNCONNECTED),
        .INJECTSBITERR(NLW_ram_reg_1_INJECTSBITERR_UNCONNECTED),
        .RDADDRECC(NLW_ram_reg_1_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_ram_reg_1_SBITERR_UNCONNECTED),
        .WEA({ram_reg_0_i_1__4_n_0,ram_reg_0_i_1__4_n_0,ram_reg_0_i_1__4_n_0,ram_reg_0_i_1__4_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
endmodule

(* ORIG_REF_NAME = "rams_sp_rf_rst" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_5
   (dout0_out,
    m00_axis_aclk,
    en,
    addr,
    di,
    WE_r,
    square_r);
  output [31:0]dout0_out;
  input m00_axis_aclk;
  input en;
  input [10:0]addr;
  input [31:0]di;
  input WE_r;
  input square_r;

  wire WE_r;
  wire [10:0]addr;
  wire [31:0]di;
  wire [31:0]dout0_out;
  wire en;
  wire m00_axis_aclk;
  wire ram_reg_0_i_1__5_n_0;
  wire square_r;
  wire NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED;
  wire NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED;
  wire NLW_ram_reg_0_DBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_SBITERR_UNCONNECTED;
  wire [31:16]NLW_ram_reg_0_DOADO_UNCONNECTED;
  wire [31:0]NLW_ram_reg_0_DOBDO_UNCONNECTED;
  wire [3:2]NLW_ram_reg_0_DOPADOP_UNCONNECTED;
  wire [3:0]NLW_ram_reg_0_DOPBDOP_UNCONNECTED;
  wire [7:0]NLW_ram_reg_0_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_ram_reg_0_RDADDRECC_UNCONNECTED;
  wire NLW_ram_reg_1_CASCADEOUTA_UNCONNECTED;
  wire NLW_ram_reg_1_CASCADEOUTB_UNCONNECTED;
  wire NLW_ram_reg_1_DBITERR_UNCONNECTED;
  wire NLW_ram_reg_1_INJECTDBITERR_UNCONNECTED;
  wire NLW_ram_reg_1_INJECTSBITERR_UNCONNECTED;
  wire NLW_ram_reg_1_SBITERR_UNCONNECTED;
  wire [31:14]NLW_ram_reg_1_DOADO_UNCONNECTED;
  wire [31:0]NLW_ram_reg_1_DOBDO_UNCONNECTED;
  wire [3:0]NLW_ram_reg_1_DOPADOP_UNCONNECTED;
  wire [3:0]NLW_ram_reg_1_DOPBDOP_UNCONNECTED;
  wire [7:0]NLW_ram_reg_1_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_ram_reg_1_RDADDRECC_UNCONNECTED;

  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p2_d16" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "61472" *) 
  (* RTL_RAM_NAME = "ram" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "2047" *) 
  (* bram_slice_begin = "0" *) 
  (* bram_slice_end = "17" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "2047" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "17" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(18),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(18),
    .WRITE_WIDTH_B(0)) 
    ram_reg_0
       (.ADDRARDADDR({1'b1,addr,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CASCADEINA(1'b1),
        .CASCADEINB(1'b0),
        .CASCADEOUTA(NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED),
        .CASCADEOUTB(NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED),
        .CLKARDCLK(m00_axis_aclk),
        .CLKBWRCLK(1'b0),
        .DBITERR(NLW_ram_reg_0_DBITERR_UNCONNECTED),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,di[15:0]}),
        .DIBDI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0,di[17:16]}),
        .DIPBDIP({1'b1,1'b1,1'b1,1'b1}),
        .DOADO({NLW_ram_reg_0_DOADO_UNCONNECTED[31:16],dout0_out[15:0]}),
        .DOBDO(NLW_ram_reg_0_DOBDO_UNCONNECTED[31:0]),
        .DOPADOP({NLW_ram_reg_0_DOPADOP_UNCONNECTED[3:2],dout0_out[17:16]}),
        .DOPBDOP(NLW_ram_reg_0_DOPBDOP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_ram_reg_0_ECCPARITY_UNCONNECTED[7:0]),
        .ENARDEN(en),
        .ENBWREN(1'b0),
        .INJECTDBITERR(NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED),
        .INJECTSBITERR(NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED),
        .RDADDRECC(NLW_ram_reg_0_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_ram_reg_0_SBITERR_UNCONNECTED),
        .WEA({ram_reg_0_i_1__5_n_0,ram_reg_0_i_1__5_n_0,ram_reg_0_i_1__5_n_0,ram_reg_0_i_1__5_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
  LUT3 #(
    .INIT(8'h80)) 
    ram_reg_0_i_1__5
       (.I0(en),
        .I1(WE_r),
        .I2(square_r),
        .O(ram_reg_0_i_1__5_n_0));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d14" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "61472" *) 
  (* RTL_RAM_NAME = "ram" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "2047" *) 
  (* bram_slice_begin = "18" *) 
  (* bram_slice_end = "31" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "2047" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "18" *) 
  (* ram_slice_end = "31" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(18),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(18),
    .WRITE_WIDTH_B(0)) 
    ram_reg_1
       (.ADDRARDADDR({1'b1,addr,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CASCADEINA(1'b1),
        .CASCADEINB(1'b0),
        .CASCADEOUTA(NLW_ram_reg_1_CASCADEOUTA_UNCONNECTED),
        .CASCADEOUTB(NLW_ram_reg_1_CASCADEOUTB_UNCONNECTED),
        .CLKARDCLK(m00_axis_aclk),
        .CLKBWRCLK(1'b0),
        .DBITERR(NLW_ram_reg_1_DBITERR_UNCONNECTED),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,di[31:18]}),
        .DIBDI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0,1'b0,1'b0}),
        .DIPBDIP({1'b1,1'b1,1'b1,1'b1}),
        .DOADO({NLW_ram_reg_1_DOADO_UNCONNECTED[31:14],dout0_out[31:18]}),
        .DOBDO(NLW_ram_reg_1_DOBDO_UNCONNECTED[31:0]),
        .DOPADOP(NLW_ram_reg_1_DOPADOP_UNCONNECTED[3:0]),
        .DOPBDOP(NLW_ram_reg_1_DOPBDOP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_ram_reg_1_ECCPARITY_UNCONNECTED[7:0]),
        .ENARDEN(en),
        .ENBWREN(1'b0),
        .INJECTDBITERR(NLW_ram_reg_1_INJECTDBITERR_UNCONNECTED),
        .INJECTSBITERR(NLW_ram_reg_1_INJECTSBITERR_UNCONNECTED),
        .RDADDRECC(NLW_ram_reg_1_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_ram_reg_1_SBITERR_UNCONNECTED),
        .WEA({ram_reg_0_i_1__5_n_0,ram_reg_0_i_1__5_n_0,ram_reg_0_i_1__5_n_0,ram_reg_0_i_1__5_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
