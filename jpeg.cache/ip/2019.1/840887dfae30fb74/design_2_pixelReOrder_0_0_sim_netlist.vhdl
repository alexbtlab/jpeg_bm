-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1 (lin64) Build 2552052 Fri May 24 14:47:09 MDT 2019
-- Date        : Sat Sep 25 12:58:38 2021
-- Host        : alex-HP-Compaq-8200-Elite-CMT-PC running 64-bit Ubuntu 20.04.2 LTS
-- Command     : write_vhdl -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_2_pixelReOrder_0_0_sim_netlist.vhdl
-- Design      : design_2_pixelReOrder_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7z020clg484-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_pixelReOrder_v1_0 is
  port (
    m00_axis_tvalid : out STD_LOGIC;
    m00_axis_aclk : in STD_LOGIC;
    s00_axis_tvalid : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_pixelReOrder_v1_0;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_pixelReOrder_v1_0 is
  signal cnt_inner_valid : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal \cnt_inner_valid0_carry__0_n_0\ : STD_LOGIC;
  signal \cnt_inner_valid0_carry__0_n_1\ : STD_LOGIC;
  signal \cnt_inner_valid0_carry__0_n_2\ : STD_LOGIC;
  signal \cnt_inner_valid0_carry__0_n_3\ : STD_LOGIC;
  signal \cnt_inner_valid0_carry__1_n_0\ : STD_LOGIC;
  signal \cnt_inner_valid0_carry__1_n_1\ : STD_LOGIC;
  signal \cnt_inner_valid0_carry__1_n_2\ : STD_LOGIC;
  signal \cnt_inner_valid0_carry__1_n_3\ : STD_LOGIC;
  signal \cnt_inner_valid0_carry__2_n_2\ : STD_LOGIC;
  signal \cnt_inner_valid0_carry__2_n_3\ : STD_LOGIC;
  signal cnt_inner_valid0_carry_n_0 : STD_LOGIC;
  signal cnt_inner_valid0_carry_n_1 : STD_LOGIC;
  signal cnt_inner_valid0_carry_n_2 : STD_LOGIC;
  signal cnt_inner_valid0_carry_n_3 : STD_LOGIC;
  signal \cnt_inner_valid[0]_i_2_n_0\ : STD_LOGIC;
  signal \cnt_inner_valid[0]_i_3_n_0\ : STD_LOGIC;
  signal \cnt_inner_valid[15]_i_1_n_0\ : STD_LOGIC;
  signal \cnt_inner_valid[15]_i_3_n_0\ : STD_LOGIC;
  signal \cnt_inner_valid[15]_i_4_n_0\ : STD_LOGIC;
  signal \cnt_inner_valid[15]_i_5_n_0\ : STD_LOGIC;
  signal \cnt_inner_valid_reg_n_0_[0]\ : STD_LOGIC;
  signal \cnt_inner_valid_reg_n_0_[10]\ : STD_LOGIC;
  signal \cnt_inner_valid_reg_n_0_[11]\ : STD_LOGIC;
  signal \cnt_inner_valid_reg_n_0_[12]\ : STD_LOGIC;
  signal \cnt_inner_valid_reg_n_0_[13]\ : STD_LOGIC;
  signal \cnt_inner_valid_reg_n_0_[14]\ : STD_LOGIC;
  signal \cnt_inner_valid_reg_n_0_[15]\ : STD_LOGIC;
  signal \cnt_inner_valid_reg_n_0_[1]\ : STD_LOGIC;
  signal \cnt_inner_valid_reg_n_0_[2]\ : STD_LOGIC;
  signal \cnt_inner_valid_reg_n_0_[3]\ : STD_LOGIC;
  signal \cnt_inner_valid_reg_n_0_[4]\ : STD_LOGIC;
  signal \cnt_inner_valid_reg_n_0_[5]\ : STD_LOGIC;
  signal \cnt_inner_valid_reg_n_0_[6]\ : STD_LOGIC;
  signal \cnt_inner_valid_reg_n_0_[7]\ : STD_LOGIC;
  signal \cnt_inner_valid_reg_n_0_[8]\ : STD_LOGIC;
  signal \cnt_inner_valid_reg_n_0_[9]\ : STD_LOGIC;
  signal data0 : STD_LOGIC_VECTOR ( 15 downto 1 );
  signal \^m00_axis_tvalid\ : STD_LOGIC;
  signal m00_axis_tvalid_r_i_1_n_0 : STD_LOGIC;
  signal m00_axis_tvalid_r_i_2_n_0 : STD_LOGIC;
  signal m00_axis_tvalid_r_i_3_n_0 : STD_LOGIC;
  signal m00_axis_tvalid_r_i_4_n_0 : STD_LOGIC;
  signal m00_axis_tvalid_r_i_5_n_0 : STD_LOGIC;
  signal \NLW_cnt_inner_valid0_carry__2_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_cnt_inner_valid0_carry__2_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \cnt_inner_valid[0]_i_3\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of m00_axis_tvalid_r_i_5 : label is "soft_lutpair0";
begin
  m00_axis_tvalid <= \^m00_axis_tvalid\;
cnt_inner_valid0_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => cnt_inner_valid0_carry_n_0,
      CO(2) => cnt_inner_valid0_carry_n_1,
      CO(1) => cnt_inner_valid0_carry_n_2,
      CO(0) => cnt_inner_valid0_carry_n_3,
      CYINIT => \cnt_inner_valid_reg_n_0_[0]\,
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => data0(4 downto 1),
      S(3) => \cnt_inner_valid_reg_n_0_[4]\,
      S(2) => \cnt_inner_valid_reg_n_0_[3]\,
      S(1) => \cnt_inner_valid_reg_n_0_[2]\,
      S(0) => \cnt_inner_valid_reg_n_0_[1]\
    );
\cnt_inner_valid0_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => cnt_inner_valid0_carry_n_0,
      CO(3) => \cnt_inner_valid0_carry__0_n_0\,
      CO(2) => \cnt_inner_valid0_carry__0_n_1\,
      CO(1) => \cnt_inner_valid0_carry__0_n_2\,
      CO(0) => \cnt_inner_valid0_carry__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => data0(8 downto 5),
      S(3) => \cnt_inner_valid_reg_n_0_[8]\,
      S(2) => \cnt_inner_valid_reg_n_0_[7]\,
      S(1) => \cnt_inner_valid_reg_n_0_[6]\,
      S(0) => \cnt_inner_valid_reg_n_0_[5]\
    );
\cnt_inner_valid0_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \cnt_inner_valid0_carry__0_n_0\,
      CO(3) => \cnt_inner_valid0_carry__1_n_0\,
      CO(2) => \cnt_inner_valid0_carry__1_n_1\,
      CO(1) => \cnt_inner_valid0_carry__1_n_2\,
      CO(0) => \cnt_inner_valid0_carry__1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => data0(12 downto 9),
      S(3) => \cnt_inner_valid_reg_n_0_[12]\,
      S(2) => \cnt_inner_valid_reg_n_0_[11]\,
      S(1) => \cnt_inner_valid_reg_n_0_[10]\,
      S(0) => \cnt_inner_valid_reg_n_0_[9]\
    );
\cnt_inner_valid0_carry__2\: unisim.vcomponents.CARRY4
     port map (
      CI => \cnt_inner_valid0_carry__1_n_0\,
      CO(3 downto 2) => \NLW_cnt_inner_valid0_carry__2_CO_UNCONNECTED\(3 downto 2),
      CO(1) => \cnt_inner_valid0_carry__2_n_2\,
      CO(0) => \cnt_inner_valid0_carry__2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \NLW_cnt_inner_valid0_carry__2_O_UNCONNECTED\(3),
      O(2 downto 0) => data0(15 downto 13),
      S(3) => '0',
      S(2) => \cnt_inner_valid_reg_n_0_[15]\,
      S(1) => \cnt_inner_valid_reg_n_0_[14]\,
      S(0) => \cnt_inner_valid_reg_n_0_[13]\
    );
\cnt_inner_valid[0]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \cnt_inner_valid_reg_n_0_[0]\,
      I1 => \cnt_inner_valid[0]_i_2_n_0\,
      O => cnt_inner_valid(0)
    );
\cnt_inner_valid[0]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2A00000000000000"
    )
        port map (
      I0 => \cnt_inner_valid[0]_i_3_n_0\,
      I1 => \cnt_inner_valid_reg_n_0_[6]\,
      I2 => \cnt_inner_valid_reg_n_0_[7]\,
      I3 => \cnt_inner_valid[15]_i_4_n_0\,
      I4 => \cnt_inner_valid[15]_i_3_n_0\,
      I5 => m00_axis_tvalid_r_i_4_n_0,
      O => \cnt_inner_valid[0]_i_2_n_0\
    );
\cnt_inner_valid[0]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \cnt_inner_valid_reg_n_0_[2]\,
      I1 => \cnt_inner_valid_reg_n_0_[1]\,
      O => \cnt_inner_valid[0]_i_3_n_0\
    );
\cnt_inner_valid[10]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFF7F00000000"
    )
        port map (
      I0 => m00_axis_tvalid_r_i_4_n_0,
      I1 => \cnt_inner_valid[15]_i_3_n_0\,
      I2 => \cnt_inner_valid[15]_i_4_n_0\,
      I3 => \cnt_inner_valid[15]_i_5_n_0\,
      I4 => \cnt_inner_valid_reg_n_0_[0]\,
      I5 => data0(10),
      O => cnt_inner_valid(10)
    );
\cnt_inner_valid[11]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFF7F00000000"
    )
        port map (
      I0 => m00_axis_tvalid_r_i_4_n_0,
      I1 => \cnt_inner_valid[15]_i_3_n_0\,
      I2 => \cnt_inner_valid[15]_i_4_n_0\,
      I3 => \cnt_inner_valid[15]_i_5_n_0\,
      I4 => \cnt_inner_valid_reg_n_0_[0]\,
      I5 => data0(11),
      O => cnt_inner_valid(11)
    );
\cnt_inner_valid[12]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFF7F00000000"
    )
        port map (
      I0 => m00_axis_tvalid_r_i_4_n_0,
      I1 => \cnt_inner_valid[15]_i_3_n_0\,
      I2 => \cnt_inner_valid[15]_i_4_n_0\,
      I3 => \cnt_inner_valid[15]_i_5_n_0\,
      I4 => \cnt_inner_valid_reg_n_0_[0]\,
      I5 => data0(12),
      O => cnt_inner_valid(12)
    );
\cnt_inner_valid[13]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFF7F00000000"
    )
        port map (
      I0 => m00_axis_tvalid_r_i_4_n_0,
      I1 => \cnt_inner_valid[15]_i_3_n_0\,
      I2 => \cnt_inner_valid[15]_i_4_n_0\,
      I3 => \cnt_inner_valid[15]_i_5_n_0\,
      I4 => \cnt_inner_valid_reg_n_0_[0]\,
      I5 => data0(13),
      O => cnt_inner_valid(13)
    );
\cnt_inner_valid[14]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFF7F00000000"
    )
        port map (
      I0 => m00_axis_tvalid_r_i_4_n_0,
      I1 => \cnt_inner_valid[15]_i_3_n_0\,
      I2 => \cnt_inner_valid[15]_i_4_n_0\,
      I3 => \cnt_inner_valid[15]_i_5_n_0\,
      I4 => \cnt_inner_valid_reg_n_0_[0]\,
      I5 => data0(14),
      O => cnt_inner_valid(14)
    );
\cnt_inner_valid[15]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => s00_axis_tvalid,
      O => \cnt_inner_valid[15]_i_1_n_0\
    );
\cnt_inner_valid[15]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFF7F00000000"
    )
        port map (
      I0 => m00_axis_tvalid_r_i_4_n_0,
      I1 => \cnt_inner_valid[15]_i_3_n_0\,
      I2 => \cnt_inner_valid[15]_i_4_n_0\,
      I3 => \cnt_inner_valid[15]_i_5_n_0\,
      I4 => \cnt_inner_valid_reg_n_0_[0]\,
      I5 => data0(15),
      O => cnt_inner_valid(15)
    );
\cnt_inner_valid[15]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2000"
    )
        port map (
      I0 => \cnt_inner_valid_reg_n_0_[8]\,
      I1 => \cnt_inner_valid_reg_n_0_[4]\,
      I2 => \cnt_inner_valid_reg_n_0_[10]\,
      I3 => \cnt_inner_valid_reg_n_0_[7]\,
      O => \cnt_inner_valid[15]_i_3_n_0\
    );
\cnt_inner_valid[15]_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0010"
    )
        port map (
      I0 => \cnt_inner_valid_reg_n_0_[3]\,
      I1 => \cnt_inner_valid_reg_n_0_[15]\,
      I2 => \cnt_inner_valid_reg_n_0_[9]\,
      I3 => \cnt_inner_valid_reg_n_0_[5]\,
      O => \cnt_inner_valid[15]_i_4_n_0\
    );
\cnt_inner_valid[15]_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FEEE"
    )
        port map (
      I0 => \cnt_inner_valid_reg_n_0_[1]\,
      I1 => \cnt_inner_valid_reg_n_0_[2]\,
      I2 => \cnt_inner_valid_reg_n_0_[6]\,
      I3 => \cnt_inner_valid_reg_n_0_[7]\,
      O => \cnt_inner_valid[15]_i_5_n_0\
    );
\cnt_inner_valid[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFF7F00000000"
    )
        port map (
      I0 => m00_axis_tvalid_r_i_4_n_0,
      I1 => \cnt_inner_valid[15]_i_3_n_0\,
      I2 => \cnt_inner_valid[15]_i_4_n_0\,
      I3 => \cnt_inner_valid[15]_i_5_n_0\,
      I4 => \cnt_inner_valid_reg_n_0_[0]\,
      I5 => data0(1),
      O => cnt_inner_valid(1)
    );
\cnt_inner_valid[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFF7F00000000"
    )
        port map (
      I0 => m00_axis_tvalid_r_i_4_n_0,
      I1 => \cnt_inner_valid[15]_i_3_n_0\,
      I2 => \cnt_inner_valid[15]_i_4_n_0\,
      I3 => \cnt_inner_valid[15]_i_5_n_0\,
      I4 => \cnt_inner_valid_reg_n_0_[0]\,
      I5 => data0(2),
      O => cnt_inner_valid(2)
    );
\cnt_inner_valid[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFF7F00000000"
    )
        port map (
      I0 => m00_axis_tvalid_r_i_4_n_0,
      I1 => \cnt_inner_valid[15]_i_3_n_0\,
      I2 => \cnt_inner_valid[15]_i_4_n_0\,
      I3 => \cnt_inner_valid[15]_i_5_n_0\,
      I4 => \cnt_inner_valid_reg_n_0_[0]\,
      I5 => data0(3),
      O => cnt_inner_valid(3)
    );
\cnt_inner_valid[4]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFF7F00000000"
    )
        port map (
      I0 => m00_axis_tvalid_r_i_4_n_0,
      I1 => \cnt_inner_valid[15]_i_3_n_0\,
      I2 => \cnt_inner_valid[15]_i_4_n_0\,
      I3 => \cnt_inner_valid[15]_i_5_n_0\,
      I4 => \cnt_inner_valid_reg_n_0_[0]\,
      I5 => data0(4),
      O => cnt_inner_valid(4)
    );
\cnt_inner_valid[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFF7F00000000"
    )
        port map (
      I0 => m00_axis_tvalid_r_i_4_n_0,
      I1 => \cnt_inner_valid[15]_i_3_n_0\,
      I2 => \cnt_inner_valid[15]_i_4_n_0\,
      I3 => \cnt_inner_valid[15]_i_5_n_0\,
      I4 => \cnt_inner_valid_reg_n_0_[0]\,
      I5 => data0(5),
      O => cnt_inner_valid(5)
    );
\cnt_inner_valid[6]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFF7F00000000"
    )
        port map (
      I0 => m00_axis_tvalid_r_i_4_n_0,
      I1 => \cnt_inner_valid[15]_i_3_n_0\,
      I2 => \cnt_inner_valid[15]_i_4_n_0\,
      I3 => \cnt_inner_valid[15]_i_5_n_0\,
      I4 => \cnt_inner_valid_reg_n_0_[0]\,
      I5 => data0(6),
      O => cnt_inner_valid(6)
    );
\cnt_inner_valid[7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFF7F00000000"
    )
        port map (
      I0 => m00_axis_tvalid_r_i_4_n_0,
      I1 => \cnt_inner_valid[15]_i_3_n_0\,
      I2 => \cnt_inner_valid[15]_i_4_n_0\,
      I3 => \cnt_inner_valid[15]_i_5_n_0\,
      I4 => \cnt_inner_valid_reg_n_0_[0]\,
      I5 => data0(7),
      O => cnt_inner_valid(7)
    );
\cnt_inner_valid[8]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFF7F00000000"
    )
        port map (
      I0 => m00_axis_tvalid_r_i_4_n_0,
      I1 => \cnt_inner_valid[15]_i_3_n_0\,
      I2 => \cnt_inner_valid[15]_i_4_n_0\,
      I3 => \cnt_inner_valid[15]_i_5_n_0\,
      I4 => \cnt_inner_valid_reg_n_0_[0]\,
      I5 => data0(8),
      O => cnt_inner_valid(8)
    );
\cnt_inner_valid[9]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFF7F00000000"
    )
        port map (
      I0 => m00_axis_tvalid_r_i_4_n_0,
      I1 => \cnt_inner_valid[15]_i_3_n_0\,
      I2 => \cnt_inner_valid[15]_i_4_n_0\,
      I3 => \cnt_inner_valid[15]_i_5_n_0\,
      I4 => \cnt_inner_valid_reg_n_0_[0]\,
      I5 => data0(9),
      O => cnt_inner_valid(9)
    );
\cnt_inner_valid_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => cnt_inner_valid(0),
      Q => \cnt_inner_valid_reg_n_0_[0]\,
      R => \cnt_inner_valid[15]_i_1_n_0\
    );
\cnt_inner_valid_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => cnt_inner_valid(10),
      Q => \cnt_inner_valid_reg_n_0_[10]\,
      R => \cnt_inner_valid[15]_i_1_n_0\
    );
\cnt_inner_valid_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => cnt_inner_valid(11),
      Q => \cnt_inner_valid_reg_n_0_[11]\,
      R => \cnt_inner_valid[15]_i_1_n_0\
    );
\cnt_inner_valid_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => cnt_inner_valid(12),
      Q => \cnt_inner_valid_reg_n_0_[12]\,
      R => \cnt_inner_valid[15]_i_1_n_0\
    );
\cnt_inner_valid_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => cnt_inner_valid(13),
      Q => \cnt_inner_valid_reg_n_0_[13]\,
      R => \cnt_inner_valid[15]_i_1_n_0\
    );
\cnt_inner_valid_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => cnt_inner_valid(14),
      Q => \cnt_inner_valid_reg_n_0_[14]\,
      R => \cnt_inner_valid[15]_i_1_n_0\
    );
\cnt_inner_valid_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => cnt_inner_valid(15),
      Q => \cnt_inner_valid_reg_n_0_[15]\,
      R => \cnt_inner_valid[15]_i_1_n_0\
    );
\cnt_inner_valid_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => cnt_inner_valid(1),
      Q => \cnt_inner_valid_reg_n_0_[1]\,
      R => \cnt_inner_valid[15]_i_1_n_0\
    );
\cnt_inner_valid_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => cnt_inner_valid(2),
      Q => \cnt_inner_valid_reg_n_0_[2]\,
      R => \cnt_inner_valid[15]_i_1_n_0\
    );
\cnt_inner_valid_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => cnt_inner_valid(3),
      Q => \cnt_inner_valid_reg_n_0_[3]\,
      R => \cnt_inner_valid[15]_i_1_n_0\
    );
\cnt_inner_valid_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => cnt_inner_valid(4),
      Q => \cnt_inner_valid_reg_n_0_[4]\,
      R => \cnt_inner_valid[15]_i_1_n_0\
    );
\cnt_inner_valid_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => cnt_inner_valid(5),
      Q => \cnt_inner_valid_reg_n_0_[5]\,
      R => \cnt_inner_valid[15]_i_1_n_0\
    );
\cnt_inner_valid_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => cnt_inner_valid(6),
      Q => \cnt_inner_valid_reg_n_0_[6]\,
      R => \cnt_inner_valid[15]_i_1_n_0\
    );
\cnt_inner_valid_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => cnt_inner_valid(7),
      Q => \cnt_inner_valid_reg_n_0_[7]\,
      R => \cnt_inner_valid[15]_i_1_n_0\
    );
\cnt_inner_valid_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => cnt_inner_valid(8),
      Q => \cnt_inner_valid_reg_n_0_[8]\,
      R => \cnt_inner_valid[15]_i_1_n_0\
    );
\cnt_inner_valid_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => cnt_inner_valid(9),
      Q => \cnt_inner_valid_reg_n_0_[9]\,
      R => \cnt_inner_valid[15]_i_1_n_0\
    );
m00_axis_tvalid_r_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0010FFFF00100000"
    )
        port map (
      I0 => m00_axis_tvalid_r_i_2_n_0,
      I1 => m00_axis_tvalid_r_i_3_n_0,
      I2 => m00_axis_tvalid_r_i_4_n_0,
      I3 => m00_axis_tvalid_r_i_5_n_0,
      I4 => s00_axis_tvalid,
      I5 => \^m00_axis_tvalid\,
      O => m00_axis_tvalid_r_i_1_n_0
    );
m00_axis_tvalid_r_i_2: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FE"
    )
        port map (
      I0 => \cnt_inner_valid_reg_n_0_[8]\,
      I1 => \cnt_inner_valid_reg_n_0_[6]\,
      I2 => \cnt_inner_valid_reg_n_0_[15]\,
      O => m00_axis_tvalid_r_i_2_n_0
    );
m00_axis_tvalid_r_i_3: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => \cnt_inner_valid_reg_n_0_[9]\,
      I1 => \cnt_inner_valid_reg_n_0_[7]\,
      I2 => \cnt_inner_valid_reg_n_0_[10]\,
      I3 => \cnt_inner_valid_reg_n_0_[5]\,
      O => m00_axis_tvalid_r_i_3_n_0
    );
m00_axis_tvalid_r_i_4: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0001"
    )
        port map (
      I0 => \cnt_inner_valid_reg_n_0_[13]\,
      I1 => \cnt_inner_valid_reg_n_0_[14]\,
      I2 => \cnt_inner_valid_reg_n_0_[12]\,
      I3 => \cnt_inner_valid_reg_n_0_[11]\,
      O => m00_axis_tvalid_r_i_4_n_0
    );
m00_axis_tvalid_r_i_5: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAAAAA9"
    )
        port map (
      I0 => \cnt_inner_valid_reg_n_0_[4]\,
      I1 => \cnt_inner_valid_reg_n_0_[0]\,
      I2 => \cnt_inner_valid_reg_n_0_[3]\,
      I3 => \cnt_inner_valid_reg_n_0_[2]\,
      I4 => \cnt_inner_valid_reg_n_0_[1]\,
      O => m00_axis_tvalid_r_i_5_n_0
    );
m00_axis_tvalid_r_reg: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => m00_axis_tvalid_r_i_1_n_0,
      Q => \^m00_axis_tvalid\,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  port (
    m00_axis_aclk : in STD_LOGIC;
    m00_axis_aresetn : in STD_LOGIC;
    m00_axis_tvalid : out STD_LOGIC;
    m00_axis_tdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    m00_axis_tstrb : out STD_LOGIC_VECTOR ( 3 downto 0 );
    m00_axis_tlast : out STD_LOGIC;
    m00_axis_tready : in STD_LOGIC;
    s00_axis_aclk : in STD_LOGIC;
    s00_axis_aresetn : in STD_LOGIC;
    s00_axis_tready : out STD_LOGIC;
    s00_axis_tdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s00_axis_tuser : in STD_LOGIC_VECTOR ( 47 downto 0 );
    s00_axis_tstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s00_axis_tlast : in STD_LOGIC;
    s00_axis_tvalid : in STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "design_2_pixelReOrder_0_0,pixelReOrder_v1_0,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "yes";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "pixelReOrder_v1_0,Vivado 2019.1";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  signal \<const1>\ : STD_LOGIC;
  attribute X_INTERFACE_INFO : string;
  attribute X_INTERFACE_INFO of m00_axis_aclk : signal is "xilinx.com:signal:clock:1.0 M00_AXIS_CLK CLK";
  attribute X_INTERFACE_PARAMETER : string;
  attribute X_INTERFACE_PARAMETER of m00_axis_aclk : signal is "XIL_INTERFACENAME M00_AXIS_CLK, ASSOCIATED_BUSIF M00_AXIS, ASSOCIATED_RESET m00_axis_aresetn, FREQ_HZ 200000000, PHASE 0.000, CLK_DOMAIN design_2_processing_system7_0_0_FCLK_CLK0, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of m00_axis_aresetn : signal is "xilinx.com:signal:reset:1.0 M00_AXIS_RST RST";
  attribute X_INTERFACE_PARAMETER of m00_axis_aresetn : signal is "XIL_INTERFACENAME M00_AXIS_RST, POLARITY ACTIVE_LOW, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of m00_axis_tlast : signal is "xilinx.com:interface:axis:1.0 M00_AXIS TLAST";
  attribute X_INTERFACE_INFO of m00_axis_tready : signal is "xilinx.com:interface:axis:1.0 M00_AXIS TREADY";
  attribute X_INTERFACE_PARAMETER of m00_axis_tready : signal is "XIL_INTERFACENAME M00_AXIS, WIZ_DATA_WIDTH 32, TDATA_NUM_BYTES 4, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 1, HAS_TKEEP 0, HAS_TLAST 1, FREQ_HZ 200000000, PHASE 0.000, CLK_DOMAIN design_2_processing_system7_0_0_FCLK_CLK0, LAYERED_METADATA undef, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of m00_axis_tvalid : signal is "xilinx.com:interface:axis:1.0 M00_AXIS TVALID";
  attribute X_INTERFACE_INFO of s00_axis_aclk : signal is "xilinx.com:signal:clock:1.0 S00_AXIS_CLK CLK";
  attribute X_INTERFACE_PARAMETER of s00_axis_aclk : signal is "XIL_INTERFACENAME S00_AXIS_CLK, ASSOCIATED_BUSIF S00_AXIS, ASSOCIATED_RESET s00_axis_aresetn, FREQ_HZ 200000000, PHASE 0.000, CLK_DOMAIN design_2_processing_system7_0_0_FCLK_CLK0, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of s00_axis_aresetn : signal is "xilinx.com:signal:reset:1.0 S00_AXIS_RST RST";
  attribute X_INTERFACE_PARAMETER of s00_axis_aresetn : signal is "XIL_INTERFACENAME S00_AXIS_RST, POLARITY ACTIVE_LOW, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of s00_axis_tlast : signal is "xilinx.com:interface:axis:1.0 S00_AXIS TLAST";
  attribute X_INTERFACE_INFO of s00_axis_tready : signal is "xilinx.com:interface:axis:1.0 S00_AXIS TREADY";
  attribute X_INTERFACE_INFO of s00_axis_tvalid : signal is "xilinx.com:interface:axis:1.0 S00_AXIS TVALID";
  attribute X_INTERFACE_PARAMETER of s00_axis_tvalid : signal is "XIL_INTERFACENAME S00_AXIS, WIZ_DATA_WIDTH 32, TDATA_NUM_BYTES 4, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 48, HAS_TREADY 1, HAS_TSTRB 1, HAS_TKEEP 0, HAS_TLAST 1, FREQ_HZ 200000000, PHASE 0.000, CLK_DOMAIN design_2_processing_system7_0_0_FCLK_CLK0, LAYERED_METADATA undef, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of m00_axis_tdata : signal is "xilinx.com:interface:axis:1.0 M00_AXIS TDATA";
  attribute X_INTERFACE_INFO of m00_axis_tstrb : signal is "xilinx.com:interface:axis:1.0 M00_AXIS TSTRB";
  attribute X_INTERFACE_INFO of s00_axis_tdata : signal is "xilinx.com:interface:axis:1.0 S00_AXIS TDATA";
  attribute X_INTERFACE_INFO of s00_axis_tstrb : signal is "xilinx.com:interface:axis:1.0 S00_AXIS TSTRB";
  attribute X_INTERFACE_INFO of s00_axis_tuser : signal is "xilinx.com:interface:axis:1.0 S00_AXIS TUSER";
begin
  s00_axis_tready <= \<const1>\;
  m00_axis_tlast <= 'Z';
  m00_axis_tdata(0) <= 'Z';
  m00_axis_tdata(1) <= 'Z';
  m00_axis_tdata(2) <= 'Z';
  m00_axis_tdata(3) <= 'Z';
  m00_axis_tdata(4) <= 'Z';
  m00_axis_tdata(5) <= 'Z';
  m00_axis_tdata(6) <= 'Z';
  m00_axis_tdata(7) <= 'Z';
  m00_axis_tdata(8) <= 'Z';
  m00_axis_tdata(9) <= 'Z';
  m00_axis_tdata(10) <= 'Z';
  m00_axis_tdata(11) <= 'Z';
  m00_axis_tdata(12) <= 'Z';
  m00_axis_tdata(13) <= 'Z';
  m00_axis_tdata(14) <= 'Z';
  m00_axis_tdata(15) <= 'Z';
  m00_axis_tdata(16) <= 'Z';
  m00_axis_tdata(17) <= 'Z';
  m00_axis_tdata(18) <= 'Z';
  m00_axis_tdata(19) <= 'Z';
  m00_axis_tdata(20) <= 'Z';
  m00_axis_tdata(21) <= 'Z';
  m00_axis_tdata(22) <= 'Z';
  m00_axis_tdata(23) <= 'Z';
  m00_axis_tdata(24) <= 'Z';
  m00_axis_tdata(25) <= 'Z';
  m00_axis_tdata(26) <= 'Z';
  m00_axis_tdata(27) <= 'Z';
  m00_axis_tdata(28) <= 'Z';
  m00_axis_tdata(29) <= 'Z';
  m00_axis_tdata(30) <= 'Z';
  m00_axis_tdata(31) <= 'Z';
  m00_axis_tstrb(0) <= 'Z';
  m00_axis_tstrb(1) <= 'Z';
  m00_axis_tstrb(2) <= 'Z';
  m00_axis_tstrb(3) <= 'Z';
VCC: unisim.vcomponents.VCC
     port map (
      P => \<const1>\
    );
inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_pixelReOrder_v1_0
     port map (
      m00_axis_aclk => m00_axis_aclk,
      m00_axis_tvalid => m00_axis_tvalid,
      s00_axis_tvalid => s00_axis_tvalid
    );
end STRUCTURE;
