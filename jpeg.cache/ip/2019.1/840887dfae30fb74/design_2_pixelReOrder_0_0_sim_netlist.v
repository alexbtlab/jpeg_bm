// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (lin64) Build 2552052 Fri May 24 14:47:09 MDT 2019
// Date        : Sat Sep 25 12:58:37 2021
// Host        : alex-HP-Compaq-8200-Elite-CMT-PC running 64-bit Ubuntu 20.04.2 LTS
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_2_pixelReOrder_0_0_sim_netlist.v
// Design      : design_2_pixelReOrder_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7z020clg484-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_2_pixelReOrder_0_0,pixelReOrder_v1_0,{}" *) (* DowngradeIPIdentifiedWarnings = "yes" *) (* X_CORE_INFO = "pixelReOrder_v1_0,Vivado 2019.1" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (m00_axis_aclk,
    m00_axis_aresetn,
    m00_axis_tvalid,
    m00_axis_tdata,
    m00_axis_tstrb,
    m00_axis_tlast,
    m00_axis_tready,
    s00_axis_aclk,
    s00_axis_aresetn,
    s00_axis_tready,
    s00_axis_tdata,
    s00_axis_tuser,
    s00_axis_tstrb,
    s00_axis_tlast,
    s00_axis_tvalid);
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 M00_AXIS_CLK CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME M00_AXIS_CLK, ASSOCIATED_BUSIF M00_AXIS, ASSOCIATED_RESET m00_axis_aresetn, FREQ_HZ 200000000, PHASE 0.000, CLK_DOMAIN design_2_processing_system7_0_0_FCLK_CLK0, INSERT_VIP 0" *) input m00_axis_aclk;
  (* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 M00_AXIS_RST RST" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME M00_AXIS_RST, POLARITY ACTIVE_LOW, INSERT_VIP 0" *) input m00_axis_aresetn;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 M00_AXIS TVALID" *) output m00_axis_tvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 M00_AXIS TDATA" *) output [31:0]m00_axis_tdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 M00_AXIS TSTRB" *) output [3:0]m00_axis_tstrb;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 M00_AXIS TLAST" *) output m00_axis_tlast;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 M00_AXIS TREADY" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME M00_AXIS, WIZ_DATA_WIDTH 32, TDATA_NUM_BYTES 4, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 1, HAS_TKEEP 0, HAS_TLAST 1, FREQ_HZ 200000000, PHASE 0.000, CLK_DOMAIN design_2_processing_system7_0_0_FCLK_CLK0, LAYERED_METADATA undef, INSERT_VIP 0" *) input m00_axis_tready;
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 S00_AXIS_CLK CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME S00_AXIS_CLK, ASSOCIATED_BUSIF S00_AXIS, ASSOCIATED_RESET s00_axis_aresetn, FREQ_HZ 200000000, PHASE 0.000, CLK_DOMAIN design_2_processing_system7_0_0_FCLK_CLK0, INSERT_VIP 0" *) input s00_axis_aclk;
  (* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 S00_AXIS_RST RST" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME S00_AXIS_RST, POLARITY ACTIVE_LOW, INSERT_VIP 0" *) input s00_axis_aresetn;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 S00_AXIS TREADY" *) output s00_axis_tready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 S00_AXIS TDATA" *) input [31:0]s00_axis_tdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 S00_AXIS TUSER" *) input [47:0]s00_axis_tuser;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 S00_AXIS TSTRB" *) input [3:0]s00_axis_tstrb;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 S00_AXIS TLAST" *) input s00_axis_tlast;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 S00_AXIS TVALID" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME S00_AXIS, WIZ_DATA_WIDTH 32, TDATA_NUM_BYTES 4, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 48, HAS_TREADY 1, HAS_TSTRB 1, HAS_TKEEP 0, HAS_TLAST 1, FREQ_HZ 200000000, PHASE 0.000, CLK_DOMAIN design_2_processing_system7_0_0_FCLK_CLK0, LAYERED_METADATA undef, INSERT_VIP 0" *) input s00_axis_tvalid;

  wire \<const1> ;
  wire m00_axis_aclk;
  wire m00_axis_tvalid;
  wire s00_axis_tvalid;

  assign s00_axis_tready = \<const1> ;
  VCC VCC
       (.P(\<const1> ));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_pixelReOrder_v1_0 inst
       (.m00_axis_aclk(m00_axis_aclk),
        .m00_axis_tvalid(m00_axis_tvalid),
        .s00_axis_tvalid(s00_axis_tvalid));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_pixelReOrder_v1_0
   (m00_axis_tvalid,
    m00_axis_aclk,
    s00_axis_tvalid);
  output m00_axis_tvalid;
  input m00_axis_aclk;
  input s00_axis_tvalid;

  wire [15:0]cnt_inner_valid;
  wire cnt_inner_valid0_carry__0_n_0;
  wire cnt_inner_valid0_carry__0_n_1;
  wire cnt_inner_valid0_carry__0_n_2;
  wire cnt_inner_valid0_carry__0_n_3;
  wire cnt_inner_valid0_carry__1_n_0;
  wire cnt_inner_valid0_carry__1_n_1;
  wire cnt_inner_valid0_carry__1_n_2;
  wire cnt_inner_valid0_carry__1_n_3;
  wire cnt_inner_valid0_carry__2_n_2;
  wire cnt_inner_valid0_carry__2_n_3;
  wire cnt_inner_valid0_carry_n_0;
  wire cnt_inner_valid0_carry_n_1;
  wire cnt_inner_valid0_carry_n_2;
  wire cnt_inner_valid0_carry_n_3;
  wire \cnt_inner_valid[0]_i_2_n_0 ;
  wire \cnt_inner_valid[0]_i_3_n_0 ;
  wire \cnt_inner_valid[15]_i_1_n_0 ;
  wire \cnt_inner_valid[15]_i_3_n_0 ;
  wire \cnt_inner_valid[15]_i_4_n_0 ;
  wire \cnt_inner_valid[15]_i_5_n_0 ;
  wire \cnt_inner_valid_reg_n_0_[0] ;
  wire \cnt_inner_valid_reg_n_0_[10] ;
  wire \cnt_inner_valid_reg_n_0_[11] ;
  wire \cnt_inner_valid_reg_n_0_[12] ;
  wire \cnt_inner_valid_reg_n_0_[13] ;
  wire \cnt_inner_valid_reg_n_0_[14] ;
  wire \cnt_inner_valid_reg_n_0_[15] ;
  wire \cnt_inner_valid_reg_n_0_[1] ;
  wire \cnt_inner_valid_reg_n_0_[2] ;
  wire \cnt_inner_valid_reg_n_0_[3] ;
  wire \cnt_inner_valid_reg_n_0_[4] ;
  wire \cnt_inner_valid_reg_n_0_[5] ;
  wire \cnt_inner_valid_reg_n_0_[6] ;
  wire \cnt_inner_valid_reg_n_0_[7] ;
  wire \cnt_inner_valid_reg_n_0_[8] ;
  wire \cnt_inner_valid_reg_n_0_[9] ;
  wire [15:1]data0;
  wire m00_axis_aclk;
  wire m00_axis_tvalid;
  wire m00_axis_tvalid_r_i_1_n_0;
  wire m00_axis_tvalid_r_i_2_n_0;
  wire m00_axis_tvalid_r_i_3_n_0;
  wire m00_axis_tvalid_r_i_4_n_0;
  wire m00_axis_tvalid_r_i_5_n_0;
  wire s00_axis_tvalid;
  wire [3:2]NLW_cnt_inner_valid0_carry__2_CO_UNCONNECTED;
  wire [3:3]NLW_cnt_inner_valid0_carry__2_O_UNCONNECTED;

  CARRY4 cnt_inner_valid0_carry
       (.CI(1'b0),
        .CO({cnt_inner_valid0_carry_n_0,cnt_inner_valid0_carry_n_1,cnt_inner_valid0_carry_n_2,cnt_inner_valid0_carry_n_3}),
        .CYINIT(\cnt_inner_valid_reg_n_0_[0] ),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(data0[4:1]),
        .S({\cnt_inner_valid_reg_n_0_[4] ,\cnt_inner_valid_reg_n_0_[3] ,\cnt_inner_valid_reg_n_0_[2] ,\cnt_inner_valid_reg_n_0_[1] }));
  CARRY4 cnt_inner_valid0_carry__0
       (.CI(cnt_inner_valid0_carry_n_0),
        .CO({cnt_inner_valid0_carry__0_n_0,cnt_inner_valid0_carry__0_n_1,cnt_inner_valid0_carry__0_n_2,cnt_inner_valid0_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(data0[8:5]),
        .S({\cnt_inner_valid_reg_n_0_[8] ,\cnt_inner_valid_reg_n_0_[7] ,\cnt_inner_valid_reg_n_0_[6] ,\cnt_inner_valid_reg_n_0_[5] }));
  CARRY4 cnt_inner_valid0_carry__1
       (.CI(cnt_inner_valid0_carry__0_n_0),
        .CO({cnt_inner_valid0_carry__1_n_0,cnt_inner_valid0_carry__1_n_1,cnt_inner_valid0_carry__1_n_2,cnt_inner_valid0_carry__1_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(data0[12:9]),
        .S({\cnt_inner_valid_reg_n_0_[12] ,\cnt_inner_valid_reg_n_0_[11] ,\cnt_inner_valid_reg_n_0_[10] ,\cnt_inner_valid_reg_n_0_[9] }));
  CARRY4 cnt_inner_valid0_carry__2
       (.CI(cnt_inner_valid0_carry__1_n_0),
        .CO({NLW_cnt_inner_valid0_carry__2_CO_UNCONNECTED[3:2],cnt_inner_valid0_carry__2_n_2,cnt_inner_valid0_carry__2_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({NLW_cnt_inner_valid0_carry__2_O_UNCONNECTED[3],data0[15:13]}),
        .S({1'b0,\cnt_inner_valid_reg_n_0_[15] ,\cnt_inner_valid_reg_n_0_[14] ,\cnt_inner_valid_reg_n_0_[13] }));
  LUT2 #(
    .INIT(4'h1)) 
    \cnt_inner_valid[0]_i_1 
       (.I0(\cnt_inner_valid_reg_n_0_[0] ),
        .I1(\cnt_inner_valid[0]_i_2_n_0 ),
        .O(cnt_inner_valid[0]));
  LUT6 #(
    .INIT(64'h2A00000000000000)) 
    \cnt_inner_valid[0]_i_2 
       (.I0(\cnt_inner_valid[0]_i_3_n_0 ),
        .I1(\cnt_inner_valid_reg_n_0_[6] ),
        .I2(\cnt_inner_valid_reg_n_0_[7] ),
        .I3(\cnt_inner_valid[15]_i_4_n_0 ),
        .I4(\cnt_inner_valid[15]_i_3_n_0 ),
        .I5(m00_axis_tvalid_r_i_4_n_0),
        .O(\cnt_inner_valid[0]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT2 #(
    .INIT(4'h1)) 
    \cnt_inner_valid[0]_i_3 
       (.I0(\cnt_inner_valid_reg_n_0_[2] ),
        .I1(\cnt_inner_valid_reg_n_0_[1] ),
        .O(\cnt_inner_valid[0]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFF7F00000000)) 
    \cnt_inner_valid[10]_i_1 
       (.I0(m00_axis_tvalid_r_i_4_n_0),
        .I1(\cnt_inner_valid[15]_i_3_n_0 ),
        .I2(\cnt_inner_valid[15]_i_4_n_0 ),
        .I3(\cnt_inner_valid[15]_i_5_n_0 ),
        .I4(\cnt_inner_valid_reg_n_0_[0] ),
        .I5(data0[10]),
        .O(cnt_inner_valid[10]));
  LUT6 #(
    .INIT(64'hFFFFFF7F00000000)) 
    \cnt_inner_valid[11]_i_1 
       (.I0(m00_axis_tvalid_r_i_4_n_0),
        .I1(\cnt_inner_valid[15]_i_3_n_0 ),
        .I2(\cnt_inner_valid[15]_i_4_n_0 ),
        .I3(\cnt_inner_valid[15]_i_5_n_0 ),
        .I4(\cnt_inner_valid_reg_n_0_[0] ),
        .I5(data0[11]),
        .O(cnt_inner_valid[11]));
  LUT6 #(
    .INIT(64'hFFFFFF7F00000000)) 
    \cnt_inner_valid[12]_i_1 
       (.I0(m00_axis_tvalid_r_i_4_n_0),
        .I1(\cnt_inner_valid[15]_i_3_n_0 ),
        .I2(\cnt_inner_valid[15]_i_4_n_0 ),
        .I3(\cnt_inner_valid[15]_i_5_n_0 ),
        .I4(\cnt_inner_valid_reg_n_0_[0] ),
        .I5(data0[12]),
        .O(cnt_inner_valid[12]));
  LUT6 #(
    .INIT(64'hFFFFFF7F00000000)) 
    \cnt_inner_valid[13]_i_1 
       (.I0(m00_axis_tvalid_r_i_4_n_0),
        .I1(\cnt_inner_valid[15]_i_3_n_0 ),
        .I2(\cnt_inner_valid[15]_i_4_n_0 ),
        .I3(\cnt_inner_valid[15]_i_5_n_0 ),
        .I4(\cnt_inner_valid_reg_n_0_[0] ),
        .I5(data0[13]),
        .O(cnt_inner_valid[13]));
  LUT6 #(
    .INIT(64'hFFFFFF7F00000000)) 
    \cnt_inner_valid[14]_i_1 
       (.I0(m00_axis_tvalid_r_i_4_n_0),
        .I1(\cnt_inner_valid[15]_i_3_n_0 ),
        .I2(\cnt_inner_valid[15]_i_4_n_0 ),
        .I3(\cnt_inner_valid[15]_i_5_n_0 ),
        .I4(\cnt_inner_valid_reg_n_0_[0] ),
        .I5(data0[14]),
        .O(cnt_inner_valid[14]));
  LUT1 #(
    .INIT(2'h1)) 
    \cnt_inner_valid[15]_i_1 
       (.I0(s00_axis_tvalid),
        .O(\cnt_inner_valid[15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFF7F00000000)) 
    \cnt_inner_valid[15]_i_2 
       (.I0(m00_axis_tvalid_r_i_4_n_0),
        .I1(\cnt_inner_valid[15]_i_3_n_0 ),
        .I2(\cnt_inner_valid[15]_i_4_n_0 ),
        .I3(\cnt_inner_valid[15]_i_5_n_0 ),
        .I4(\cnt_inner_valid_reg_n_0_[0] ),
        .I5(data0[15]),
        .O(cnt_inner_valid[15]));
  LUT4 #(
    .INIT(16'h2000)) 
    \cnt_inner_valid[15]_i_3 
       (.I0(\cnt_inner_valid_reg_n_0_[8] ),
        .I1(\cnt_inner_valid_reg_n_0_[4] ),
        .I2(\cnt_inner_valid_reg_n_0_[10] ),
        .I3(\cnt_inner_valid_reg_n_0_[7] ),
        .O(\cnt_inner_valid[15]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'h0010)) 
    \cnt_inner_valid[15]_i_4 
       (.I0(\cnt_inner_valid_reg_n_0_[3] ),
        .I1(\cnt_inner_valid_reg_n_0_[15] ),
        .I2(\cnt_inner_valid_reg_n_0_[9] ),
        .I3(\cnt_inner_valid_reg_n_0_[5] ),
        .O(\cnt_inner_valid[15]_i_4_n_0 ));
  LUT4 #(
    .INIT(16'hFEEE)) 
    \cnt_inner_valid[15]_i_5 
       (.I0(\cnt_inner_valid_reg_n_0_[1] ),
        .I1(\cnt_inner_valid_reg_n_0_[2] ),
        .I2(\cnt_inner_valid_reg_n_0_[6] ),
        .I3(\cnt_inner_valid_reg_n_0_[7] ),
        .O(\cnt_inner_valid[15]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFF7F00000000)) 
    \cnt_inner_valid[1]_i_1 
       (.I0(m00_axis_tvalid_r_i_4_n_0),
        .I1(\cnt_inner_valid[15]_i_3_n_0 ),
        .I2(\cnt_inner_valid[15]_i_4_n_0 ),
        .I3(\cnt_inner_valid[15]_i_5_n_0 ),
        .I4(\cnt_inner_valid_reg_n_0_[0] ),
        .I5(data0[1]),
        .O(cnt_inner_valid[1]));
  LUT6 #(
    .INIT(64'hFFFFFF7F00000000)) 
    \cnt_inner_valid[2]_i_1 
       (.I0(m00_axis_tvalid_r_i_4_n_0),
        .I1(\cnt_inner_valid[15]_i_3_n_0 ),
        .I2(\cnt_inner_valid[15]_i_4_n_0 ),
        .I3(\cnt_inner_valid[15]_i_5_n_0 ),
        .I4(\cnt_inner_valid_reg_n_0_[0] ),
        .I5(data0[2]),
        .O(cnt_inner_valid[2]));
  LUT6 #(
    .INIT(64'hFFFFFF7F00000000)) 
    \cnt_inner_valid[3]_i_1 
       (.I0(m00_axis_tvalid_r_i_4_n_0),
        .I1(\cnt_inner_valid[15]_i_3_n_0 ),
        .I2(\cnt_inner_valid[15]_i_4_n_0 ),
        .I3(\cnt_inner_valid[15]_i_5_n_0 ),
        .I4(\cnt_inner_valid_reg_n_0_[0] ),
        .I5(data0[3]),
        .O(cnt_inner_valid[3]));
  LUT6 #(
    .INIT(64'hFFFFFF7F00000000)) 
    \cnt_inner_valid[4]_i_1 
       (.I0(m00_axis_tvalid_r_i_4_n_0),
        .I1(\cnt_inner_valid[15]_i_3_n_0 ),
        .I2(\cnt_inner_valid[15]_i_4_n_0 ),
        .I3(\cnt_inner_valid[15]_i_5_n_0 ),
        .I4(\cnt_inner_valid_reg_n_0_[0] ),
        .I5(data0[4]),
        .O(cnt_inner_valid[4]));
  LUT6 #(
    .INIT(64'hFFFFFF7F00000000)) 
    \cnt_inner_valid[5]_i_1 
       (.I0(m00_axis_tvalid_r_i_4_n_0),
        .I1(\cnt_inner_valid[15]_i_3_n_0 ),
        .I2(\cnt_inner_valid[15]_i_4_n_0 ),
        .I3(\cnt_inner_valid[15]_i_5_n_0 ),
        .I4(\cnt_inner_valid_reg_n_0_[0] ),
        .I5(data0[5]),
        .O(cnt_inner_valid[5]));
  LUT6 #(
    .INIT(64'hFFFFFF7F00000000)) 
    \cnt_inner_valid[6]_i_1 
       (.I0(m00_axis_tvalid_r_i_4_n_0),
        .I1(\cnt_inner_valid[15]_i_3_n_0 ),
        .I2(\cnt_inner_valid[15]_i_4_n_0 ),
        .I3(\cnt_inner_valid[15]_i_5_n_0 ),
        .I4(\cnt_inner_valid_reg_n_0_[0] ),
        .I5(data0[6]),
        .O(cnt_inner_valid[6]));
  LUT6 #(
    .INIT(64'hFFFFFF7F00000000)) 
    \cnt_inner_valid[7]_i_1 
       (.I0(m00_axis_tvalid_r_i_4_n_0),
        .I1(\cnt_inner_valid[15]_i_3_n_0 ),
        .I2(\cnt_inner_valid[15]_i_4_n_0 ),
        .I3(\cnt_inner_valid[15]_i_5_n_0 ),
        .I4(\cnt_inner_valid_reg_n_0_[0] ),
        .I5(data0[7]),
        .O(cnt_inner_valid[7]));
  LUT6 #(
    .INIT(64'hFFFFFF7F00000000)) 
    \cnt_inner_valid[8]_i_1 
       (.I0(m00_axis_tvalid_r_i_4_n_0),
        .I1(\cnt_inner_valid[15]_i_3_n_0 ),
        .I2(\cnt_inner_valid[15]_i_4_n_0 ),
        .I3(\cnt_inner_valid[15]_i_5_n_0 ),
        .I4(\cnt_inner_valid_reg_n_0_[0] ),
        .I5(data0[8]),
        .O(cnt_inner_valid[8]));
  LUT6 #(
    .INIT(64'hFFFFFF7F00000000)) 
    \cnt_inner_valid[9]_i_1 
       (.I0(m00_axis_tvalid_r_i_4_n_0),
        .I1(\cnt_inner_valid[15]_i_3_n_0 ),
        .I2(\cnt_inner_valid[15]_i_4_n_0 ),
        .I3(\cnt_inner_valid[15]_i_5_n_0 ),
        .I4(\cnt_inner_valid_reg_n_0_[0] ),
        .I5(data0[9]),
        .O(cnt_inner_valid[9]));
  FDRE \cnt_inner_valid_reg[0] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(cnt_inner_valid[0]),
        .Q(\cnt_inner_valid_reg_n_0_[0] ),
        .R(\cnt_inner_valid[15]_i_1_n_0 ));
  FDRE \cnt_inner_valid_reg[10] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(cnt_inner_valid[10]),
        .Q(\cnt_inner_valid_reg_n_0_[10] ),
        .R(\cnt_inner_valid[15]_i_1_n_0 ));
  FDRE \cnt_inner_valid_reg[11] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(cnt_inner_valid[11]),
        .Q(\cnt_inner_valid_reg_n_0_[11] ),
        .R(\cnt_inner_valid[15]_i_1_n_0 ));
  FDRE \cnt_inner_valid_reg[12] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(cnt_inner_valid[12]),
        .Q(\cnt_inner_valid_reg_n_0_[12] ),
        .R(\cnt_inner_valid[15]_i_1_n_0 ));
  FDRE \cnt_inner_valid_reg[13] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(cnt_inner_valid[13]),
        .Q(\cnt_inner_valid_reg_n_0_[13] ),
        .R(\cnt_inner_valid[15]_i_1_n_0 ));
  FDRE \cnt_inner_valid_reg[14] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(cnt_inner_valid[14]),
        .Q(\cnt_inner_valid_reg_n_0_[14] ),
        .R(\cnt_inner_valid[15]_i_1_n_0 ));
  FDRE \cnt_inner_valid_reg[15] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(cnt_inner_valid[15]),
        .Q(\cnt_inner_valid_reg_n_0_[15] ),
        .R(\cnt_inner_valid[15]_i_1_n_0 ));
  FDRE \cnt_inner_valid_reg[1] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(cnt_inner_valid[1]),
        .Q(\cnt_inner_valid_reg_n_0_[1] ),
        .R(\cnt_inner_valid[15]_i_1_n_0 ));
  FDRE \cnt_inner_valid_reg[2] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(cnt_inner_valid[2]),
        .Q(\cnt_inner_valid_reg_n_0_[2] ),
        .R(\cnt_inner_valid[15]_i_1_n_0 ));
  FDRE \cnt_inner_valid_reg[3] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(cnt_inner_valid[3]),
        .Q(\cnt_inner_valid_reg_n_0_[3] ),
        .R(\cnt_inner_valid[15]_i_1_n_0 ));
  FDRE \cnt_inner_valid_reg[4] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(cnt_inner_valid[4]),
        .Q(\cnt_inner_valid_reg_n_0_[4] ),
        .R(\cnt_inner_valid[15]_i_1_n_0 ));
  FDRE \cnt_inner_valid_reg[5] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(cnt_inner_valid[5]),
        .Q(\cnt_inner_valid_reg_n_0_[5] ),
        .R(\cnt_inner_valid[15]_i_1_n_0 ));
  FDRE \cnt_inner_valid_reg[6] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(cnt_inner_valid[6]),
        .Q(\cnt_inner_valid_reg_n_0_[6] ),
        .R(\cnt_inner_valid[15]_i_1_n_0 ));
  FDRE \cnt_inner_valid_reg[7] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(cnt_inner_valid[7]),
        .Q(\cnt_inner_valid_reg_n_0_[7] ),
        .R(\cnt_inner_valid[15]_i_1_n_0 ));
  FDRE \cnt_inner_valid_reg[8] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(cnt_inner_valid[8]),
        .Q(\cnt_inner_valid_reg_n_0_[8] ),
        .R(\cnt_inner_valid[15]_i_1_n_0 ));
  FDRE \cnt_inner_valid_reg[9] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(cnt_inner_valid[9]),
        .Q(\cnt_inner_valid_reg_n_0_[9] ),
        .R(\cnt_inner_valid[15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0010FFFF00100000)) 
    m00_axis_tvalid_r_i_1
       (.I0(m00_axis_tvalid_r_i_2_n_0),
        .I1(m00_axis_tvalid_r_i_3_n_0),
        .I2(m00_axis_tvalid_r_i_4_n_0),
        .I3(m00_axis_tvalid_r_i_5_n_0),
        .I4(s00_axis_tvalid),
        .I5(m00_axis_tvalid),
        .O(m00_axis_tvalid_r_i_1_n_0));
  LUT3 #(
    .INIT(8'hFE)) 
    m00_axis_tvalid_r_i_2
       (.I0(\cnt_inner_valid_reg_n_0_[8] ),
        .I1(\cnt_inner_valid_reg_n_0_[6] ),
        .I2(\cnt_inner_valid_reg_n_0_[15] ),
        .O(m00_axis_tvalid_r_i_2_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    m00_axis_tvalid_r_i_3
       (.I0(\cnt_inner_valid_reg_n_0_[9] ),
        .I1(\cnt_inner_valid_reg_n_0_[7] ),
        .I2(\cnt_inner_valid_reg_n_0_[10] ),
        .I3(\cnt_inner_valid_reg_n_0_[5] ),
        .O(m00_axis_tvalid_r_i_3_n_0));
  LUT4 #(
    .INIT(16'h0001)) 
    m00_axis_tvalid_r_i_4
       (.I0(\cnt_inner_valid_reg_n_0_[13] ),
        .I1(\cnt_inner_valid_reg_n_0_[14] ),
        .I2(\cnt_inner_valid_reg_n_0_[12] ),
        .I3(\cnt_inner_valid_reg_n_0_[11] ),
        .O(m00_axis_tvalid_r_i_4_n_0));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'hAAAAAAA9)) 
    m00_axis_tvalid_r_i_5
       (.I0(\cnt_inner_valid_reg_n_0_[4] ),
        .I1(\cnt_inner_valid_reg_n_0_[0] ),
        .I2(\cnt_inner_valid_reg_n_0_[3] ),
        .I3(\cnt_inner_valid_reg_n_0_[2] ),
        .I4(\cnt_inner_valid_reg_n_0_[1] ),
        .O(m00_axis_tvalid_r_i_5_n_0));
  FDRE m00_axis_tvalid_r_reg
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(m00_axis_tvalid_r_i_1_n_0),
        .Q(m00_axis_tvalid),
        .R(1'b0));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
