-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1 (lin64) Build 2552052 Fri May 24 14:47:09 MDT 2019
-- Date        : Sat Sep 25 00:45:34 2021
-- Host        : alex-HP-Compaq-8200-Elite-CMT-PC running 64-bit Ubuntu 20.04.2 LTS
-- Command     : write_vhdl -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_2_pixelReOrder_0_0_sim_netlist.vhdl
-- Design      : design_2_pixelReOrder_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7z020clg484-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst is
  port (
    m00_axis_tdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    m00_axis_tdata_31_sp_1 : in STD_LOGIC;
    s00_axis_tuser : in STD_LOGIC_VECTOR ( 0 to 0 );
    m00_axis_aclk : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 0 to 0 );
    addr : in STD_LOGIC_VECTOR ( 10 downto 0 );
    ram_reg_1_0 : in STD_LOGIC_VECTOR ( 31 downto 0 );
    WE_r : in STD_LOGIC;
    square_r : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst is
  signal DOUT0 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal m00_axis_tdata_31_sn_1 : STD_LOGIC;
  signal ram_reg_0_i_1_n_0 : STD_LOGIC;
  signal NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_DBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_SBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_DOADO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 16 );
  signal NLW_ram_reg_0_DOBDO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_ram_reg_0_DOPADOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal NLW_ram_reg_0_DOPBDOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_ram_reg_0_ECCPARITY_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal NLW_ram_reg_0_RDADDRECC_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_ram_reg_1_CASCADEOUTA_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_1_CASCADEOUTB_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_1_DBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_1_INJECTDBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_1_INJECTSBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_1_SBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_1_DOADO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 14 );
  signal NLW_ram_reg_1_DOBDO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_ram_reg_1_DOPADOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_ram_reg_1_DOPBDOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_ram_reg_1_ECCPARITY_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal NLW_ram_reg_1_RDADDRECC_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \m00_axis_tdata[0]_INST_1\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \m00_axis_tdata[10]_INST_1\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \m00_axis_tdata[11]_INST_1\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \m00_axis_tdata[12]_INST_1\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \m00_axis_tdata[13]_INST_1\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \m00_axis_tdata[14]_INST_1\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \m00_axis_tdata[15]_INST_1\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \m00_axis_tdata[16]_INST_1\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \m00_axis_tdata[17]_INST_1\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \m00_axis_tdata[18]_INST_1\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \m00_axis_tdata[19]_INST_1\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \m00_axis_tdata[1]_INST_1\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \m00_axis_tdata[20]_INST_1\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \m00_axis_tdata[21]_INST_1\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \m00_axis_tdata[22]_INST_1\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \m00_axis_tdata[23]_INST_1\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \m00_axis_tdata[24]_INST_1\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \m00_axis_tdata[25]_INST_1\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \m00_axis_tdata[26]_INST_1\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \m00_axis_tdata[27]_INST_1\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \m00_axis_tdata[28]_INST_1\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \m00_axis_tdata[29]_INST_1\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \m00_axis_tdata[2]_INST_1\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \m00_axis_tdata[30]_INST_1\ : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of \m00_axis_tdata[31]_INST_1\ : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of \m00_axis_tdata[3]_INST_1\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \m00_axis_tdata[4]_INST_1\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \m00_axis_tdata[5]_INST_1\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \m00_axis_tdata[6]_INST_1\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \m00_axis_tdata[7]_INST_1\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \m00_axis_tdata[8]_INST_1\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \m00_axis_tdata[9]_INST_1\ : label is "soft_lutpair4";
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ : string;
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of ram_reg_0 : label is "p2_d16";
  attribute METHODOLOGY_DRC_VIOS : string;
  attribute METHODOLOGY_DRC_VIOS of ram_reg_0 : label is "{SYNTH-6 {cell *THIS*}}";
  attribute RTL_RAM_BITS : integer;
  attribute RTL_RAM_BITS of ram_reg_0 : label is 61472;
  attribute RTL_RAM_NAME : string;
  attribute RTL_RAM_NAME of ram_reg_0 : label is "ram";
  attribute bram_addr_begin : integer;
  attribute bram_addr_begin of ram_reg_0 : label is 0;
  attribute bram_addr_end : integer;
  attribute bram_addr_end of ram_reg_0 : label is 2047;
  attribute bram_slice_begin : integer;
  attribute bram_slice_begin of ram_reg_0 : label is 0;
  attribute bram_slice_end : integer;
  attribute bram_slice_end of ram_reg_0 : label is 17;
  attribute ram_addr_begin : integer;
  attribute ram_addr_begin of ram_reg_0 : label is 0;
  attribute ram_addr_end : integer;
  attribute ram_addr_end of ram_reg_0 : label is 2047;
  attribute ram_offset : integer;
  attribute ram_offset of ram_reg_0 : label is 0;
  attribute ram_slice_begin : integer;
  attribute ram_slice_begin of ram_reg_0 : label is 0;
  attribute ram_slice_end : integer;
  attribute ram_slice_end of ram_reg_0 : label is 17;
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of ram_reg_1 : label is "p0_d14";
  attribute METHODOLOGY_DRC_VIOS of ram_reg_1 : label is "{SYNTH-6 {cell *THIS*}}";
  attribute RTL_RAM_BITS of ram_reg_1 : label is 61472;
  attribute RTL_RAM_NAME of ram_reg_1 : label is "ram";
  attribute bram_addr_begin of ram_reg_1 : label is 0;
  attribute bram_addr_end of ram_reg_1 : label is 2047;
  attribute bram_slice_begin of ram_reg_1 : label is 18;
  attribute bram_slice_end of ram_reg_1 : label is 31;
  attribute ram_addr_begin of ram_reg_1 : label is 0;
  attribute ram_addr_end of ram_reg_1 : label is 2047;
  attribute ram_offset of ram_reg_1 : label is 0;
  attribute ram_slice_begin of ram_reg_1 : label is 18;
  attribute ram_slice_end of ram_reg_1 : label is 31;
begin
  m00_axis_tdata_31_sn_1 <= m00_axis_tdata_31_sp_1;
\m00_axis_tdata[0]_INST_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => m00_axis_tdata_31_sn_1,
      I1 => s00_axis_tuser(0),
      I2 => DOUT0(0),
      O => m00_axis_tdata(0)
    );
\m00_axis_tdata[10]_INST_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => m00_axis_tdata_31_sn_1,
      I1 => s00_axis_tuser(0),
      I2 => DOUT0(10),
      O => m00_axis_tdata(10)
    );
\m00_axis_tdata[11]_INST_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => m00_axis_tdata_31_sn_1,
      I1 => s00_axis_tuser(0),
      I2 => DOUT0(11),
      O => m00_axis_tdata(11)
    );
\m00_axis_tdata[12]_INST_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => m00_axis_tdata_31_sn_1,
      I1 => s00_axis_tuser(0),
      I2 => DOUT0(12),
      O => m00_axis_tdata(12)
    );
\m00_axis_tdata[13]_INST_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => m00_axis_tdata_31_sn_1,
      I1 => s00_axis_tuser(0),
      I2 => DOUT0(13),
      O => m00_axis_tdata(13)
    );
\m00_axis_tdata[14]_INST_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => m00_axis_tdata_31_sn_1,
      I1 => s00_axis_tuser(0),
      I2 => DOUT0(14),
      O => m00_axis_tdata(14)
    );
\m00_axis_tdata[15]_INST_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => m00_axis_tdata_31_sn_1,
      I1 => s00_axis_tuser(0),
      I2 => DOUT0(15),
      O => m00_axis_tdata(15)
    );
\m00_axis_tdata[16]_INST_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => m00_axis_tdata_31_sn_1,
      I1 => s00_axis_tuser(0),
      I2 => DOUT0(16),
      O => m00_axis_tdata(16)
    );
\m00_axis_tdata[17]_INST_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => m00_axis_tdata_31_sn_1,
      I1 => s00_axis_tuser(0),
      I2 => DOUT0(17),
      O => m00_axis_tdata(17)
    );
\m00_axis_tdata[18]_INST_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => m00_axis_tdata_31_sn_1,
      I1 => s00_axis_tuser(0),
      I2 => DOUT0(18),
      O => m00_axis_tdata(18)
    );
\m00_axis_tdata[19]_INST_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => m00_axis_tdata_31_sn_1,
      I1 => s00_axis_tuser(0),
      I2 => DOUT0(19),
      O => m00_axis_tdata(19)
    );
\m00_axis_tdata[1]_INST_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => m00_axis_tdata_31_sn_1,
      I1 => s00_axis_tuser(0),
      I2 => DOUT0(1),
      O => m00_axis_tdata(1)
    );
\m00_axis_tdata[20]_INST_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => m00_axis_tdata_31_sn_1,
      I1 => s00_axis_tuser(0),
      I2 => DOUT0(20),
      O => m00_axis_tdata(20)
    );
\m00_axis_tdata[21]_INST_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => m00_axis_tdata_31_sn_1,
      I1 => s00_axis_tuser(0),
      I2 => DOUT0(21),
      O => m00_axis_tdata(21)
    );
\m00_axis_tdata[22]_INST_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => m00_axis_tdata_31_sn_1,
      I1 => s00_axis_tuser(0),
      I2 => DOUT0(22),
      O => m00_axis_tdata(22)
    );
\m00_axis_tdata[23]_INST_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => m00_axis_tdata_31_sn_1,
      I1 => s00_axis_tuser(0),
      I2 => DOUT0(23),
      O => m00_axis_tdata(23)
    );
\m00_axis_tdata[24]_INST_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => m00_axis_tdata_31_sn_1,
      I1 => s00_axis_tuser(0),
      I2 => DOUT0(24),
      O => m00_axis_tdata(24)
    );
\m00_axis_tdata[25]_INST_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => m00_axis_tdata_31_sn_1,
      I1 => s00_axis_tuser(0),
      I2 => DOUT0(25),
      O => m00_axis_tdata(25)
    );
\m00_axis_tdata[26]_INST_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => m00_axis_tdata_31_sn_1,
      I1 => s00_axis_tuser(0),
      I2 => DOUT0(26),
      O => m00_axis_tdata(26)
    );
\m00_axis_tdata[27]_INST_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => m00_axis_tdata_31_sn_1,
      I1 => s00_axis_tuser(0),
      I2 => DOUT0(27),
      O => m00_axis_tdata(27)
    );
\m00_axis_tdata[28]_INST_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => m00_axis_tdata_31_sn_1,
      I1 => s00_axis_tuser(0),
      I2 => DOUT0(28),
      O => m00_axis_tdata(28)
    );
\m00_axis_tdata[29]_INST_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => m00_axis_tdata_31_sn_1,
      I1 => s00_axis_tuser(0),
      I2 => DOUT0(29),
      O => m00_axis_tdata(29)
    );
\m00_axis_tdata[2]_INST_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => m00_axis_tdata_31_sn_1,
      I1 => s00_axis_tuser(0),
      I2 => DOUT0(2),
      O => m00_axis_tdata(2)
    );
\m00_axis_tdata[30]_INST_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => m00_axis_tdata_31_sn_1,
      I1 => s00_axis_tuser(0),
      I2 => DOUT0(30),
      O => m00_axis_tdata(30)
    );
\m00_axis_tdata[31]_INST_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => m00_axis_tdata_31_sn_1,
      I1 => s00_axis_tuser(0),
      I2 => DOUT0(31),
      O => m00_axis_tdata(31)
    );
\m00_axis_tdata[3]_INST_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => m00_axis_tdata_31_sn_1,
      I1 => s00_axis_tuser(0),
      I2 => DOUT0(3),
      O => m00_axis_tdata(3)
    );
\m00_axis_tdata[4]_INST_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => m00_axis_tdata_31_sn_1,
      I1 => s00_axis_tuser(0),
      I2 => DOUT0(4),
      O => m00_axis_tdata(4)
    );
\m00_axis_tdata[5]_INST_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => m00_axis_tdata_31_sn_1,
      I1 => s00_axis_tuser(0),
      I2 => DOUT0(5),
      O => m00_axis_tdata(5)
    );
\m00_axis_tdata[6]_INST_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => m00_axis_tdata_31_sn_1,
      I1 => s00_axis_tuser(0),
      I2 => DOUT0(6),
      O => m00_axis_tdata(6)
    );
\m00_axis_tdata[7]_INST_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => m00_axis_tdata_31_sn_1,
      I1 => s00_axis_tuser(0),
      I2 => DOUT0(7),
      O => m00_axis_tdata(7)
    );
\m00_axis_tdata[8]_INST_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => m00_axis_tdata_31_sn_1,
      I1 => s00_axis_tuser(0),
      I2 => DOUT0(8),
      O => m00_axis_tdata(8)
    );
\m00_axis_tdata[9]_INST_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => m00_axis_tdata_31_sn_1,
      I1 => s00_axis_tuser(0),
      I2 => DOUT0(9),
      O => m00_axis_tdata(9)
    );
ram_reg_0: unisim.vcomponents.RAMB36E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      EN_ECC_READ => false,
      EN_ECC_WRITE => false,
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      RAM_EXTENSION_A => "NONE",
      RAM_EXTENSION_B => "NONE",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "PERFORMANCE",
      READ_WIDTH_A => 18,
      READ_WIDTH_B => 0,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "READ_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 18,
      WRITE_WIDTH_B => 0
    )
        port map (
      ADDRARDADDR(15) => '1',
      ADDRARDADDR(14 downto 4) => addr(10 downto 0),
      ADDRARDADDR(3 downto 0) => B"1111",
      ADDRBWRADDR(15 downto 0) => B"1111111111111111",
      CASCADEINA => '1',
      CASCADEINB => '0',
      CASCADEOUTA => NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED,
      CASCADEOUTB => NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED,
      CLKARDCLK => m00_axis_aclk,
      CLKBWRCLK => '0',
      DBITERR => NLW_ram_reg_0_DBITERR_UNCONNECTED,
      DIADI(31 downto 16) => B"0000000000000000",
      DIADI(15 downto 0) => ram_reg_1_0(15 downto 0),
      DIBDI(31 downto 0) => B"11111111111111111111111111111111",
      DIPADIP(3 downto 2) => B"00",
      DIPADIP(1 downto 0) => ram_reg_1_0(17 downto 16),
      DIPBDIP(3 downto 0) => B"1111",
      DOADO(31 downto 16) => NLW_ram_reg_0_DOADO_UNCONNECTED(31 downto 16),
      DOADO(15 downto 0) => DOUT0(15 downto 0),
      DOBDO(31 downto 0) => NLW_ram_reg_0_DOBDO_UNCONNECTED(31 downto 0),
      DOPADOP(3 downto 2) => NLW_ram_reg_0_DOPADOP_UNCONNECTED(3 downto 2),
      DOPADOP(1 downto 0) => DOUT0(17 downto 16),
      DOPBDOP(3 downto 0) => NLW_ram_reg_0_DOPBDOP_UNCONNECTED(3 downto 0),
      ECCPARITY(7 downto 0) => NLW_ram_reg_0_ECCPARITY_UNCONNECTED(7 downto 0),
      ENARDEN => Q(0),
      ENBWREN => '0',
      INJECTDBITERR => NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED,
      INJECTSBITERR => NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED,
      RDADDRECC(8 downto 0) => NLW_ram_reg_0_RDADDRECC_UNCONNECTED(8 downto 0),
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      SBITERR => NLW_ram_reg_0_SBITERR_UNCONNECTED,
      WEA(3) => ram_reg_0_i_1_n_0,
      WEA(2) => ram_reg_0_i_1_n_0,
      WEA(1) => ram_reg_0_i_1_n_0,
      WEA(0) => ram_reg_0_i_1_n_0,
      WEBWE(7 downto 0) => B"00000000"
    );
ram_reg_0_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => Q(0),
      I1 => WE_r,
      I2 => square_r,
      O => ram_reg_0_i_1_n_0
    );
ram_reg_1: unisim.vcomponents.RAMB36E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      EN_ECC_READ => false,
      EN_ECC_WRITE => false,
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      RAM_EXTENSION_A => "NONE",
      RAM_EXTENSION_B => "NONE",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "PERFORMANCE",
      READ_WIDTH_A => 18,
      READ_WIDTH_B => 0,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "READ_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 18,
      WRITE_WIDTH_B => 0
    )
        port map (
      ADDRARDADDR(15) => '1',
      ADDRARDADDR(14 downto 4) => addr(10 downto 0),
      ADDRARDADDR(3 downto 0) => B"1111",
      ADDRBWRADDR(15 downto 0) => B"1111111111111111",
      CASCADEINA => '1',
      CASCADEINB => '0',
      CASCADEOUTA => NLW_ram_reg_1_CASCADEOUTA_UNCONNECTED,
      CASCADEOUTB => NLW_ram_reg_1_CASCADEOUTB_UNCONNECTED,
      CLKARDCLK => m00_axis_aclk,
      CLKBWRCLK => '0',
      DBITERR => NLW_ram_reg_1_DBITERR_UNCONNECTED,
      DIADI(31 downto 14) => B"000000000000000000",
      DIADI(13 downto 0) => ram_reg_1_0(31 downto 18),
      DIBDI(31 downto 0) => B"11111111111111111111111111111111",
      DIPADIP(3 downto 0) => B"0000",
      DIPBDIP(3 downto 0) => B"1111",
      DOADO(31 downto 14) => NLW_ram_reg_1_DOADO_UNCONNECTED(31 downto 14),
      DOADO(13 downto 0) => DOUT0(31 downto 18),
      DOBDO(31 downto 0) => NLW_ram_reg_1_DOBDO_UNCONNECTED(31 downto 0),
      DOPADOP(3 downto 0) => NLW_ram_reg_1_DOPADOP_UNCONNECTED(3 downto 0),
      DOPBDOP(3 downto 0) => NLW_ram_reg_1_DOPBDOP_UNCONNECTED(3 downto 0),
      ECCPARITY(7 downto 0) => NLW_ram_reg_1_ECCPARITY_UNCONNECTED(7 downto 0),
      ENARDEN => Q(0),
      ENBWREN => '0',
      INJECTDBITERR => NLW_ram_reg_1_INJECTDBITERR_UNCONNECTED,
      INJECTSBITERR => NLW_ram_reg_1_INJECTSBITERR_UNCONNECTED,
      RDADDRECC(8 downto 0) => NLW_ram_reg_1_RDADDRECC_UNCONNECTED(8 downto 0),
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      SBITERR => NLW_ram_reg_1_SBITERR_UNCONNECTED,
      WEA(3) => ram_reg_0_i_1_n_0,
      WEA(2) => ram_reg_0_i_1_n_0,
      WEA(1) => ram_reg_0_i_1_n_0,
      WEA(0) => ram_reg_0_i_1_n_0,
      WEBWE(7 downto 0) => B"00000000"
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_0 is
  port (
    m00_axis_tdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    m00_axis_tdata_31_sp_1 : in STD_LOGIC;
    s00_axis_tuser : in STD_LOGIC_VECTOR ( 0 to 0 );
    m00_axis_aclk : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 0 to 0 );
    addr : in STD_LOGIC_VECTOR ( 10 downto 0 );
    ram_reg_1_0 : in STD_LOGIC_VECTOR ( 31 downto 0 );
    WE_r : in STD_LOGIC;
    square_r : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_0 : entity is "rams_sp_rf_rst";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_0;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_0 is
  signal DOUT1_w : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal m00_axis_tdata_31_sn_1 : STD_LOGIC;
  signal \ram_reg_0_i_1__0_n_0\ : STD_LOGIC;
  signal NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_DBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_SBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_DOADO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 16 );
  signal NLW_ram_reg_0_DOBDO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_ram_reg_0_DOPADOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal NLW_ram_reg_0_DOPBDOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_ram_reg_0_ECCPARITY_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal NLW_ram_reg_0_RDADDRECC_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_ram_reg_1_CASCADEOUTA_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_1_CASCADEOUTB_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_1_DBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_1_INJECTDBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_1_INJECTSBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_1_SBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_1_DOADO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 14 );
  signal NLW_ram_reg_1_DOBDO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_ram_reg_1_DOPADOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_ram_reg_1_DOPBDOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_ram_reg_1_ECCPARITY_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal NLW_ram_reg_1_RDADDRECC_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \m00_axis_tdata[0]_INST_0\ : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of \m00_axis_tdata[10]_INST_0\ : label is "soft_lutpair21";
  attribute SOFT_HLUTNM of \m00_axis_tdata[11]_INST_0\ : label is "soft_lutpair21";
  attribute SOFT_HLUTNM of \m00_axis_tdata[12]_INST_0\ : label is "soft_lutpair22";
  attribute SOFT_HLUTNM of \m00_axis_tdata[13]_INST_0\ : label is "soft_lutpair22";
  attribute SOFT_HLUTNM of \m00_axis_tdata[14]_INST_0\ : label is "soft_lutpair23";
  attribute SOFT_HLUTNM of \m00_axis_tdata[15]_INST_0\ : label is "soft_lutpair23";
  attribute SOFT_HLUTNM of \m00_axis_tdata[16]_INST_0\ : label is "soft_lutpair24";
  attribute SOFT_HLUTNM of \m00_axis_tdata[17]_INST_0\ : label is "soft_lutpair24";
  attribute SOFT_HLUTNM of \m00_axis_tdata[18]_INST_0\ : label is "soft_lutpair25";
  attribute SOFT_HLUTNM of \m00_axis_tdata[19]_INST_0\ : label is "soft_lutpair25";
  attribute SOFT_HLUTNM of \m00_axis_tdata[1]_INST_0\ : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of \m00_axis_tdata[20]_INST_0\ : label is "soft_lutpair26";
  attribute SOFT_HLUTNM of \m00_axis_tdata[21]_INST_0\ : label is "soft_lutpair26";
  attribute SOFT_HLUTNM of \m00_axis_tdata[22]_INST_0\ : label is "soft_lutpair27";
  attribute SOFT_HLUTNM of \m00_axis_tdata[23]_INST_0\ : label is "soft_lutpair27";
  attribute SOFT_HLUTNM of \m00_axis_tdata[24]_INST_0\ : label is "soft_lutpair28";
  attribute SOFT_HLUTNM of \m00_axis_tdata[25]_INST_0\ : label is "soft_lutpair28";
  attribute SOFT_HLUTNM of \m00_axis_tdata[26]_INST_0\ : label is "soft_lutpair29";
  attribute SOFT_HLUTNM of \m00_axis_tdata[27]_INST_0\ : label is "soft_lutpair29";
  attribute SOFT_HLUTNM of \m00_axis_tdata[28]_INST_0\ : label is "soft_lutpair30";
  attribute SOFT_HLUTNM of \m00_axis_tdata[29]_INST_0\ : label is "soft_lutpair30";
  attribute SOFT_HLUTNM of \m00_axis_tdata[2]_INST_0\ : label is "soft_lutpair17";
  attribute SOFT_HLUTNM of \m00_axis_tdata[30]_INST_0\ : label is "soft_lutpair31";
  attribute SOFT_HLUTNM of \m00_axis_tdata[31]_INST_0\ : label is "soft_lutpair31";
  attribute SOFT_HLUTNM of \m00_axis_tdata[3]_INST_0\ : label is "soft_lutpair17";
  attribute SOFT_HLUTNM of \m00_axis_tdata[4]_INST_0\ : label is "soft_lutpair18";
  attribute SOFT_HLUTNM of \m00_axis_tdata[5]_INST_0\ : label is "soft_lutpair18";
  attribute SOFT_HLUTNM of \m00_axis_tdata[6]_INST_0\ : label is "soft_lutpair19";
  attribute SOFT_HLUTNM of \m00_axis_tdata[7]_INST_0\ : label is "soft_lutpair19";
  attribute SOFT_HLUTNM of \m00_axis_tdata[8]_INST_0\ : label is "soft_lutpair20";
  attribute SOFT_HLUTNM of \m00_axis_tdata[9]_INST_0\ : label is "soft_lutpair20";
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ : string;
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of ram_reg_0 : label is "p2_d16";
  attribute METHODOLOGY_DRC_VIOS : string;
  attribute METHODOLOGY_DRC_VIOS of ram_reg_0 : label is "{SYNTH-6 {cell *THIS*}}";
  attribute RTL_RAM_BITS : integer;
  attribute RTL_RAM_BITS of ram_reg_0 : label is 61472;
  attribute RTL_RAM_NAME : string;
  attribute RTL_RAM_NAME of ram_reg_0 : label is "ram";
  attribute bram_addr_begin : integer;
  attribute bram_addr_begin of ram_reg_0 : label is 0;
  attribute bram_addr_end : integer;
  attribute bram_addr_end of ram_reg_0 : label is 2047;
  attribute bram_slice_begin : integer;
  attribute bram_slice_begin of ram_reg_0 : label is 0;
  attribute bram_slice_end : integer;
  attribute bram_slice_end of ram_reg_0 : label is 17;
  attribute ram_addr_begin : integer;
  attribute ram_addr_begin of ram_reg_0 : label is 0;
  attribute ram_addr_end : integer;
  attribute ram_addr_end of ram_reg_0 : label is 2047;
  attribute ram_offset : integer;
  attribute ram_offset of ram_reg_0 : label is 0;
  attribute ram_slice_begin : integer;
  attribute ram_slice_begin of ram_reg_0 : label is 0;
  attribute ram_slice_end : integer;
  attribute ram_slice_end of ram_reg_0 : label is 17;
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of ram_reg_1 : label is "p0_d14";
  attribute METHODOLOGY_DRC_VIOS of ram_reg_1 : label is "{SYNTH-6 {cell *THIS*}}";
  attribute RTL_RAM_BITS of ram_reg_1 : label is 61472;
  attribute RTL_RAM_NAME of ram_reg_1 : label is "ram";
  attribute bram_addr_begin of ram_reg_1 : label is 0;
  attribute bram_addr_end of ram_reg_1 : label is 2047;
  attribute bram_slice_begin of ram_reg_1 : label is 18;
  attribute bram_slice_end of ram_reg_1 : label is 31;
  attribute ram_addr_begin of ram_reg_1 : label is 0;
  attribute ram_addr_end of ram_reg_1 : label is 2047;
  attribute ram_offset of ram_reg_1 : label is 0;
  attribute ram_slice_begin of ram_reg_1 : label is 18;
  attribute ram_slice_end of ram_reg_1 : label is 31;
begin
  m00_axis_tdata_31_sn_1 <= m00_axis_tdata_31_sp_1;
\m00_axis_tdata[0]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => m00_axis_tdata_31_sn_1,
      I1 => s00_axis_tuser(0),
      I2 => DOUT1_w(0),
      O => m00_axis_tdata(0)
    );
\m00_axis_tdata[10]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => m00_axis_tdata_31_sn_1,
      I1 => s00_axis_tuser(0),
      I2 => DOUT1_w(10),
      O => m00_axis_tdata(10)
    );
\m00_axis_tdata[11]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => m00_axis_tdata_31_sn_1,
      I1 => s00_axis_tuser(0),
      I2 => DOUT1_w(11),
      O => m00_axis_tdata(11)
    );
\m00_axis_tdata[12]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => m00_axis_tdata_31_sn_1,
      I1 => s00_axis_tuser(0),
      I2 => DOUT1_w(12),
      O => m00_axis_tdata(12)
    );
\m00_axis_tdata[13]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => m00_axis_tdata_31_sn_1,
      I1 => s00_axis_tuser(0),
      I2 => DOUT1_w(13),
      O => m00_axis_tdata(13)
    );
\m00_axis_tdata[14]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => m00_axis_tdata_31_sn_1,
      I1 => s00_axis_tuser(0),
      I2 => DOUT1_w(14),
      O => m00_axis_tdata(14)
    );
\m00_axis_tdata[15]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => m00_axis_tdata_31_sn_1,
      I1 => s00_axis_tuser(0),
      I2 => DOUT1_w(15),
      O => m00_axis_tdata(15)
    );
\m00_axis_tdata[16]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => m00_axis_tdata_31_sn_1,
      I1 => s00_axis_tuser(0),
      I2 => DOUT1_w(16),
      O => m00_axis_tdata(16)
    );
\m00_axis_tdata[17]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => m00_axis_tdata_31_sn_1,
      I1 => s00_axis_tuser(0),
      I2 => DOUT1_w(17),
      O => m00_axis_tdata(17)
    );
\m00_axis_tdata[18]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => m00_axis_tdata_31_sn_1,
      I1 => s00_axis_tuser(0),
      I2 => DOUT1_w(18),
      O => m00_axis_tdata(18)
    );
\m00_axis_tdata[19]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => m00_axis_tdata_31_sn_1,
      I1 => s00_axis_tuser(0),
      I2 => DOUT1_w(19),
      O => m00_axis_tdata(19)
    );
\m00_axis_tdata[1]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => m00_axis_tdata_31_sn_1,
      I1 => s00_axis_tuser(0),
      I2 => DOUT1_w(1),
      O => m00_axis_tdata(1)
    );
\m00_axis_tdata[20]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => m00_axis_tdata_31_sn_1,
      I1 => s00_axis_tuser(0),
      I2 => DOUT1_w(20),
      O => m00_axis_tdata(20)
    );
\m00_axis_tdata[21]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => m00_axis_tdata_31_sn_1,
      I1 => s00_axis_tuser(0),
      I2 => DOUT1_w(21),
      O => m00_axis_tdata(21)
    );
\m00_axis_tdata[22]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => m00_axis_tdata_31_sn_1,
      I1 => s00_axis_tuser(0),
      I2 => DOUT1_w(22),
      O => m00_axis_tdata(22)
    );
\m00_axis_tdata[23]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => m00_axis_tdata_31_sn_1,
      I1 => s00_axis_tuser(0),
      I2 => DOUT1_w(23),
      O => m00_axis_tdata(23)
    );
\m00_axis_tdata[24]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => m00_axis_tdata_31_sn_1,
      I1 => s00_axis_tuser(0),
      I2 => DOUT1_w(24),
      O => m00_axis_tdata(24)
    );
\m00_axis_tdata[25]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => m00_axis_tdata_31_sn_1,
      I1 => s00_axis_tuser(0),
      I2 => DOUT1_w(25),
      O => m00_axis_tdata(25)
    );
\m00_axis_tdata[26]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => m00_axis_tdata_31_sn_1,
      I1 => s00_axis_tuser(0),
      I2 => DOUT1_w(26),
      O => m00_axis_tdata(26)
    );
\m00_axis_tdata[27]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => m00_axis_tdata_31_sn_1,
      I1 => s00_axis_tuser(0),
      I2 => DOUT1_w(27),
      O => m00_axis_tdata(27)
    );
\m00_axis_tdata[28]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => m00_axis_tdata_31_sn_1,
      I1 => s00_axis_tuser(0),
      I2 => DOUT1_w(28),
      O => m00_axis_tdata(28)
    );
\m00_axis_tdata[29]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => m00_axis_tdata_31_sn_1,
      I1 => s00_axis_tuser(0),
      I2 => DOUT1_w(29),
      O => m00_axis_tdata(29)
    );
\m00_axis_tdata[2]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => m00_axis_tdata_31_sn_1,
      I1 => s00_axis_tuser(0),
      I2 => DOUT1_w(2),
      O => m00_axis_tdata(2)
    );
\m00_axis_tdata[30]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => m00_axis_tdata_31_sn_1,
      I1 => s00_axis_tuser(0),
      I2 => DOUT1_w(30),
      O => m00_axis_tdata(30)
    );
\m00_axis_tdata[31]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => m00_axis_tdata_31_sn_1,
      I1 => s00_axis_tuser(0),
      I2 => DOUT1_w(31),
      O => m00_axis_tdata(31)
    );
\m00_axis_tdata[3]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => m00_axis_tdata_31_sn_1,
      I1 => s00_axis_tuser(0),
      I2 => DOUT1_w(3),
      O => m00_axis_tdata(3)
    );
\m00_axis_tdata[4]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => m00_axis_tdata_31_sn_1,
      I1 => s00_axis_tuser(0),
      I2 => DOUT1_w(4),
      O => m00_axis_tdata(4)
    );
\m00_axis_tdata[5]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => m00_axis_tdata_31_sn_1,
      I1 => s00_axis_tuser(0),
      I2 => DOUT1_w(5),
      O => m00_axis_tdata(5)
    );
\m00_axis_tdata[6]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => m00_axis_tdata_31_sn_1,
      I1 => s00_axis_tuser(0),
      I2 => DOUT1_w(6),
      O => m00_axis_tdata(6)
    );
\m00_axis_tdata[7]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => m00_axis_tdata_31_sn_1,
      I1 => s00_axis_tuser(0),
      I2 => DOUT1_w(7),
      O => m00_axis_tdata(7)
    );
\m00_axis_tdata[8]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => m00_axis_tdata_31_sn_1,
      I1 => s00_axis_tuser(0),
      I2 => DOUT1_w(8),
      O => m00_axis_tdata(8)
    );
\m00_axis_tdata[9]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => m00_axis_tdata_31_sn_1,
      I1 => s00_axis_tuser(0),
      I2 => DOUT1_w(9),
      O => m00_axis_tdata(9)
    );
ram_reg_0: unisim.vcomponents.RAMB36E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      EN_ECC_READ => false,
      EN_ECC_WRITE => false,
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      RAM_EXTENSION_A => "NONE",
      RAM_EXTENSION_B => "NONE",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "PERFORMANCE",
      READ_WIDTH_A => 18,
      READ_WIDTH_B => 0,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "READ_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 18,
      WRITE_WIDTH_B => 0
    )
        port map (
      ADDRARDADDR(15) => '1',
      ADDRARDADDR(14 downto 4) => addr(10 downto 0),
      ADDRARDADDR(3 downto 0) => B"1111",
      ADDRBWRADDR(15 downto 0) => B"1111111111111111",
      CASCADEINA => '1',
      CASCADEINB => '0',
      CASCADEOUTA => NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED,
      CASCADEOUTB => NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED,
      CLKARDCLK => m00_axis_aclk,
      CLKBWRCLK => '0',
      DBITERR => NLW_ram_reg_0_DBITERR_UNCONNECTED,
      DIADI(31 downto 16) => B"0000000000000000",
      DIADI(15 downto 0) => ram_reg_1_0(15 downto 0),
      DIBDI(31 downto 0) => B"11111111111111111111111111111111",
      DIPADIP(3 downto 2) => B"00",
      DIPADIP(1 downto 0) => ram_reg_1_0(17 downto 16),
      DIPBDIP(3 downto 0) => B"1111",
      DOADO(31 downto 16) => NLW_ram_reg_0_DOADO_UNCONNECTED(31 downto 16),
      DOADO(15 downto 0) => DOUT1_w(15 downto 0),
      DOBDO(31 downto 0) => NLW_ram_reg_0_DOBDO_UNCONNECTED(31 downto 0),
      DOPADOP(3 downto 2) => NLW_ram_reg_0_DOPADOP_UNCONNECTED(3 downto 2),
      DOPADOP(1 downto 0) => DOUT1_w(17 downto 16),
      DOPBDOP(3 downto 0) => NLW_ram_reg_0_DOPBDOP_UNCONNECTED(3 downto 0),
      ECCPARITY(7 downto 0) => NLW_ram_reg_0_ECCPARITY_UNCONNECTED(7 downto 0),
      ENARDEN => Q(0),
      ENBWREN => '0',
      INJECTDBITERR => NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED,
      INJECTSBITERR => NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED,
      RDADDRECC(8 downto 0) => NLW_ram_reg_0_RDADDRECC_UNCONNECTED(8 downto 0),
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      SBITERR => NLW_ram_reg_0_SBITERR_UNCONNECTED,
      WEA(3) => \ram_reg_0_i_1__0_n_0\,
      WEA(2) => \ram_reg_0_i_1__0_n_0\,
      WEA(1) => \ram_reg_0_i_1__0_n_0\,
      WEA(0) => \ram_reg_0_i_1__0_n_0\,
      WEBWE(7 downto 0) => B"00000000"
    );
\ram_reg_0_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => Q(0),
      I1 => WE_r,
      I2 => square_r,
      O => \ram_reg_0_i_1__0_n_0\
    );
ram_reg_1: unisim.vcomponents.RAMB36E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      EN_ECC_READ => false,
      EN_ECC_WRITE => false,
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      RAM_EXTENSION_A => "NONE",
      RAM_EXTENSION_B => "NONE",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "PERFORMANCE",
      READ_WIDTH_A => 18,
      READ_WIDTH_B => 0,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "READ_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 18,
      WRITE_WIDTH_B => 0
    )
        port map (
      ADDRARDADDR(15) => '1',
      ADDRARDADDR(14 downto 4) => addr(10 downto 0),
      ADDRARDADDR(3 downto 0) => B"1111",
      ADDRBWRADDR(15 downto 0) => B"1111111111111111",
      CASCADEINA => '1',
      CASCADEINB => '0',
      CASCADEOUTA => NLW_ram_reg_1_CASCADEOUTA_UNCONNECTED,
      CASCADEOUTB => NLW_ram_reg_1_CASCADEOUTB_UNCONNECTED,
      CLKARDCLK => m00_axis_aclk,
      CLKBWRCLK => '0',
      DBITERR => NLW_ram_reg_1_DBITERR_UNCONNECTED,
      DIADI(31 downto 14) => B"000000000000000000",
      DIADI(13 downto 0) => ram_reg_1_0(31 downto 18),
      DIBDI(31 downto 0) => B"11111111111111111111111111111111",
      DIPADIP(3 downto 0) => B"0000",
      DIPBDIP(3 downto 0) => B"1111",
      DOADO(31 downto 14) => NLW_ram_reg_1_DOADO_UNCONNECTED(31 downto 14),
      DOADO(13 downto 0) => DOUT1_w(31 downto 18),
      DOBDO(31 downto 0) => NLW_ram_reg_1_DOBDO_UNCONNECTED(31 downto 0),
      DOPADOP(3 downto 0) => NLW_ram_reg_1_DOPADOP_UNCONNECTED(3 downto 0),
      DOPBDOP(3 downto 0) => NLW_ram_reg_1_DOPBDOP_UNCONNECTED(3 downto 0),
      ECCPARITY(7 downto 0) => NLW_ram_reg_1_ECCPARITY_UNCONNECTED(7 downto 0),
      ENARDEN => Q(0),
      ENBWREN => '0',
      INJECTDBITERR => NLW_ram_reg_1_INJECTDBITERR_UNCONNECTED,
      INJECTSBITERR => NLW_ram_reg_1_INJECTSBITERR_UNCONNECTED,
      RDADDRECC(8 downto 0) => NLW_ram_reg_1_RDADDRECC_UNCONNECTED(8 downto 0),
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      SBITERR => NLW_ram_reg_1_SBITERR_UNCONNECTED,
      WEA(3) => \ram_reg_0_i_1__0_n_0\,
      WEA(2) => \ram_reg_0_i_1__0_n_0\,
      WEA(1) => \ram_reg_0_i_1__0_n_0\,
      WEA(0) => \ram_reg_0_i_1__0_n_0\,
      WEBWE(7 downto 0) => B"00000000"
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_pixelReOrder_v1_0 is
  port (
    m00_axis_tvalid : out STD_LOGIC;
    m00_axis_tdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s00_axis_tvalid : in STD_LOGIC;
    m00_axis_aclk : in STD_LOGIC;
    s00_axis_tuser : in STD_LOGIC_VECTOR ( 12 downto 0 );
    m00_axis_tdata_31_sp_1 : in STD_LOGIC;
    s00_axis_tdata : in STD_LOGIC_VECTOR ( 31 downto 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_pixelReOrder_v1_0;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_pixelReOrder_v1_0 is
  signal DI0 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal EN_r : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \EN_r[0]_i_2_n_0\ : STD_LOGIC;
  signal WE_r : STD_LOGIC;
  signal adr_wr : STD_LOGIC_VECTOR ( 10 downto 0 );
  signal cnt_inner_valid : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal \cnt_inner_valid0_carry__0_n_0\ : STD_LOGIC;
  signal \cnt_inner_valid0_carry__0_n_1\ : STD_LOGIC;
  signal \cnt_inner_valid0_carry__0_n_2\ : STD_LOGIC;
  signal \cnt_inner_valid0_carry__0_n_3\ : STD_LOGIC;
  signal \cnt_inner_valid0_carry__1_n_0\ : STD_LOGIC;
  signal \cnt_inner_valid0_carry__1_n_1\ : STD_LOGIC;
  signal \cnt_inner_valid0_carry__1_n_2\ : STD_LOGIC;
  signal \cnt_inner_valid0_carry__1_n_3\ : STD_LOGIC;
  signal \cnt_inner_valid0_carry__2_n_2\ : STD_LOGIC;
  signal \cnt_inner_valid0_carry__2_n_3\ : STD_LOGIC;
  signal cnt_inner_valid0_carry_n_0 : STD_LOGIC;
  signal cnt_inner_valid0_carry_n_1 : STD_LOGIC;
  signal cnt_inner_valid0_carry_n_2 : STD_LOGIC;
  signal cnt_inner_valid0_carry_n_3 : STD_LOGIC;
  signal \cnt_inner_valid[0]_i_2_n_0\ : STD_LOGIC;
  signal \cnt_inner_valid[15]_i_1_n_0\ : STD_LOGIC;
  signal \cnt_inner_valid[15]_i_3_n_0\ : STD_LOGIC;
  signal \cnt_inner_valid[15]_i_4_n_0\ : STD_LOGIC;
  signal \cnt_inner_valid[15]_i_5_n_0\ : STD_LOGIC;
  signal cnt_inner_valid_0 : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal data0 : STD_LOGIC_VECTOR ( 15 downto 1 );
  signal m00_axis_tdata_31_sn_1 : STD_LOGIC;
  signal m00_axis_tvalid_r0 : STD_LOGIC;
  signal m00_axis_tvalid_r_i_2_n_0 : STD_LOGIC;
  signal m00_axis_tvalid_r_i_3_n_0 : STD_LOGIC;
  signal m00_axis_tvalid_r_i_4_n_0 : STD_LOGIC;
  signal p_0_out : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal square_r : STD_LOGIC;
  signal square_r_i_1_n_0 : STD_LOGIC;
  signal \NLW_cnt_inner_valid0_carry__2_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_cnt_inner_valid0_carry__2_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \EN_r[0]_i_1\ : label is "soft_lutpair34";
  attribute SOFT_HLUTNM of \EN_r[0]_i_2\ : label is "soft_lutpair33";
  attribute SOFT_HLUTNM of \cnt_inner_valid[0]_i_1\ : label is "soft_lutpair32";
  attribute SOFT_HLUTNM of \cnt_inner_valid[15]_i_5\ : label is "soft_lutpair34";
  attribute SOFT_HLUTNM of m00_axis_tvalid_r_i_2 : label is "soft_lutpair33";
  attribute SOFT_HLUTNM of m00_axis_tvalid_r_i_3 : label is "soft_lutpair32";
begin
  m00_axis_tdata_31_sn_1 <= m00_axis_tdata_31_sp_1;
\DI0_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => s00_axis_tvalid,
      D => s00_axis_tdata(0),
      Q => DI0(0),
      R => '0'
    );
\DI0_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => s00_axis_tvalid,
      D => s00_axis_tdata(10),
      Q => DI0(10),
      R => '0'
    );
\DI0_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => s00_axis_tvalid,
      D => s00_axis_tdata(11),
      Q => DI0(11),
      R => '0'
    );
\DI0_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => s00_axis_tvalid,
      D => s00_axis_tdata(12),
      Q => DI0(12),
      R => '0'
    );
\DI0_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => s00_axis_tvalid,
      D => s00_axis_tdata(13),
      Q => DI0(13),
      R => '0'
    );
\DI0_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => s00_axis_tvalid,
      D => s00_axis_tdata(14),
      Q => DI0(14),
      R => '0'
    );
\DI0_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => s00_axis_tvalid,
      D => s00_axis_tdata(15),
      Q => DI0(15),
      R => '0'
    );
\DI0_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => s00_axis_tvalid,
      D => s00_axis_tdata(16),
      Q => DI0(16),
      R => '0'
    );
\DI0_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => s00_axis_tvalid,
      D => s00_axis_tdata(17),
      Q => DI0(17),
      R => '0'
    );
\DI0_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => s00_axis_tvalid,
      D => s00_axis_tdata(18),
      Q => DI0(18),
      R => '0'
    );
\DI0_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => s00_axis_tvalid,
      D => s00_axis_tdata(19),
      Q => DI0(19),
      R => '0'
    );
\DI0_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => s00_axis_tvalid,
      D => s00_axis_tdata(1),
      Q => DI0(1),
      R => '0'
    );
\DI0_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => s00_axis_tvalid,
      D => s00_axis_tdata(20),
      Q => DI0(20),
      R => '0'
    );
\DI0_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => s00_axis_tvalid,
      D => s00_axis_tdata(21),
      Q => DI0(21),
      R => '0'
    );
\DI0_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => s00_axis_tvalid,
      D => s00_axis_tdata(22),
      Q => DI0(22),
      R => '0'
    );
\DI0_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => s00_axis_tvalid,
      D => s00_axis_tdata(23),
      Q => DI0(23),
      R => '0'
    );
\DI0_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => s00_axis_tvalid,
      D => s00_axis_tdata(24),
      Q => DI0(24),
      R => '0'
    );
\DI0_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => s00_axis_tvalid,
      D => s00_axis_tdata(25),
      Q => DI0(25),
      R => '0'
    );
\DI0_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => s00_axis_tvalid,
      D => s00_axis_tdata(26),
      Q => DI0(26),
      R => '0'
    );
\DI0_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => s00_axis_tvalid,
      D => s00_axis_tdata(27),
      Q => DI0(27),
      R => '0'
    );
\DI0_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => s00_axis_tvalid,
      D => s00_axis_tdata(28),
      Q => DI0(28),
      R => '0'
    );
\DI0_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => s00_axis_tvalid,
      D => s00_axis_tdata(29),
      Q => DI0(29),
      R => '0'
    );
\DI0_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => s00_axis_tvalid,
      D => s00_axis_tdata(2),
      Q => DI0(2),
      R => '0'
    );
\DI0_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => s00_axis_tvalid,
      D => s00_axis_tdata(30),
      Q => DI0(30),
      R => '0'
    );
\DI0_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => s00_axis_tvalid,
      D => s00_axis_tdata(31),
      Q => DI0(31),
      R => '0'
    );
\DI0_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => s00_axis_tvalid,
      D => s00_axis_tdata(3),
      Q => DI0(3),
      R => '0'
    );
\DI0_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => s00_axis_tvalid,
      D => s00_axis_tdata(4),
      Q => DI0(4),
      R => '0'
    );
\DI0_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => s00_axis_tvalid,
      D => s00_axis_tdata(5),
      Q => DI0(5),
      R => '0'
    );
\DI0_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => s00_axis_tvalid,
      D => s00_axis_tdata(6),
      Q => DI0(6),
      R => '0'
    );
\DI0_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => s00_axis_tvalid,
      D => s00_axis_tdata(7),
      Q => DI0(7),
      R => '0'
    );
\DI0_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => s00_axis_tvalid,
      D => s00_axis_tdata(8),
      Q => DI0(8),
      R => '0'
    );
\DI0_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => s00_axis_tvalid,
      D => s00_axis_tdata(9),
      Q => DI0(9),
      R => '0'
    );
\EN_r[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000001"
    )
        port map (
      I0 => cnt_inner_valid(7),
      I1 => cnt_inner_valid(8),
      I2 => cnt_inner_valid(9),
      I3 => cnt_inner_valid(10),
      I4 => \EN_r[0]_i_2_n_0\,
      O => p_0_out(0)
    );
\EN_r[0]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => cnt_inner_valid(15),
      I1 => cnt_inner_valid(14),
      I2 => cnt_inner_valid(5),
      I3 => \cnt_inner_valid[15]_i_4_n_0\,
      I4 => cnt_inner_valid(4),
      O => \EN_r[0]_i_2_n_0\
    );
\EN_r[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000002"
    )
        port map (
      I0 => cnt_inner_valid(4),
      I1 => cnt_inner_valid(10),
      I2 => cnt_inner_valid(9),
      I3 => cnt_inner_valid(8),
      I4 => cnt_inner_valid(7),
      I5 => m00_axis_tvalid_r_i_2_n_0,
      O => p_0_out(1)
    );
\EN_r_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => s00_axis_tvalid,
      D => p_0_out(0),
      Q => EN_r(0),
      R => '0'
    );
\EN_r_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => s00_axis_tvalid,
      D => p_0_out(1),
      Q => EN_r(1),
      R => '0'
    );
WE_r_reg: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => s00_axis_tvalid,
      Q => WE_r,
      R => '0'
    );
\adr_wr_reg[0]\: unisim.vcomponents.FDSE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => s00_axis_tuser(0),
      Q => adr_wr(0),
      S => \cnt_inner_valid[15]_i_1_n_0\
    );
\adr_wr_reg[10]\: unisim.vcomponents.FDSE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => s00_axis_tuser(10),
      Q => adr_wr(10),
      S => \cnt_inner_valid[15]_i_1_n_0\
    );
\adr_wr_reg[1]\: unisim.vcomponents.FDSE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => s00_axis_tuser(1),
      Q => adr_wr(1),
      S => \cnt_inner_valid[15]_i_1_n_0\
    );
\adr_wr_reg[2]\: unisim.vcomponents.FDSE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => s00_axis_tuser(2),
      Q => adr_wr(2),
      S => \cnt_inner_valid[15]_i_1_n_0\
    );
\adr_wr_reg[3]\: unisim.vcomponents.FDSE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => s00_axis_tuser(3),
      Q => adr_wr(3),
      S => \cnt_inner_valid[15]_i_1_n_0\
    );
\adr_wr_reg[4]\: unisim.vcomponents.FDSE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => s00_axis_tuser(4),
      Q => adr_wr(4),
      S => \cnt_inner_valid[15]_i_1_n_0\
    );
\adr_wr_reg[5]\: unisim.vcomponents.FDSE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => s00_axis_tuser(5),
      Q => adr_wr(5),
      S => \cnt_inner_valid[15]_i_1_n_0\
    );
\adr_wr_reg[6]\: unisim.vcomponents.FDSE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => s00_axis_tuser(6),
      Q => adr_wr(6),
      S => \cnt_inner_valid[15]_i_1_n_0\
    );
\adr_wr_reg[7]\: unisim.vcomponents.FDSE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => s00_axis_tuser(7),
      Q => adr_wr(7),
      S => \cnt_inner_valid[15]_i_1_n_0\
    );
\adr_wr_reg[8]\: unisim.vcomponents.FDSE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => s00_axis_tuser(8),
      Q => adr_wr(8),
      S => \cnt_inner_valid[15]_i_1_n_0\
    );
\adr_wr_reg[9]\: unisim.vcomponents.FDSE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => s00_axis_tuser(9),
      Q => adr_wr(9),
      S => \cnt_inner_valid[15]_i_1_n_0\
    );
cnt_inner_valid0_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => cnt_inner_valid0_carry_n_0,
      CO(2) => cnt_inner_valid0_carry_n_1,
      CO(1) => cnt_inner_valid0_carry_n_2,
      CO(0) => cnt_inner_valid0_carry_n_3,
      CYINIT => cnt_inner_valid(0),
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => data0(4 downto 1),
      S(3 downto 0) => cnt_inner_valid(4 downto 1)
    );
\cnt_inner_valid0_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => cnt_inner_valid0_carry_n_0,
      CO(3) => \cnt_inner_valid0_carry__0_n_0\,
      CO(2) => \cnt_inner_valid0_carry__0_n_1\,
      CO(1) => \cnt_inner_valid0_carry__0_n_2\,
      CO(0) => \cnt_inner_valid0_carry__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => data0(8 downto 5),
      S(3 downto 0) => cnt_inner_valid(8 downto 5)
    );
\cnt_inner_valid0_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \cnt_inner_valid0_carry__0_n_0\,
      CO(3) => \cnt_inner_valid0_carry__1_n_0\,
      CO(2) => \cnt_inner_valid0_carry__1_n_1\,
      CO(1) => \cnt_inner_valid0_carry__1_n_2\,
      CO(0) => \cnt_inner_valid0_carry__1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => data0(12 downto 9),
      S(3 downto 0) => cnt_inner_valid(12 downto 9)
    );
\cnt_inner_valid0_carry__2\: unisim.vcomponents.CARRY4
     port map (
      CI => \cnt_inner_valid0_carry__1_n_0\,
      CO(3 downto 2) => \NLW_cnt_inner_valid0_carry__2_CO_UNCONNECTED\(3 downto 2),
      CO(1) => \cnt_inner_valid0_carry__2_n_2\,
      CO(0) => \cnt_inner_valid0_carry__2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \NLW_cnt_inner_valid0_carry__2_O_UNCONNECTED\(3),
      O(2 downto 0) => data0(15 downto 13),
      S(3) => '0',
      S(2 downto 0) => cnt_inner_valid(15 downto 13)
    );
\cnt_inner_valid[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00FF00FE"
    )
        port map (
      I0 => cnt_inner_valid(3),
      I1 => cnt_inner_valid(2),
      I2 => cnt_inner_valid(1),
      I3 => cnt_inner_valid(0),
      I4 => \cnt_inner_valid[0]_i_2_n_0\,
      O => cnt_inner_valid_0(0)
    );
\cnt_inner_valid[0]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => \cnt_inner_valid[15]_i_5_n_0\,
      I1 => cnt_inner_valid(4),
      I2 => \cnt_inner_valid[15]_i_4_n_0\,
      I3 => cnt_inner_valid(5),
      I4 => cnt_inner_valid(14),
      I5 => cnt_inner_valid(15),
      O => \cnt_inner_valid[0]_i_2_n_0\
    );
\cnt_inner_valid[10]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFE00000000"
    )
        port map (
      I0 => \cnt_inner_valid[15]_i_3_n_0\,
      I1 => \cnt_inner_valid[15]_i_4_n_0\,
      I2 => cnt_inner_valid(4),
      I3 => \cnt_inner_valid[15]_i_5_n_0\,
      I4 => m00_axis_tvalid_r_i_3_n_0,
      I5 => data0(10),
      O => cnt_inner_valid_0(10)
    );
\cnt_inner_valid[11]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFE00000000"
    )
        port map (
      I0 => \cnt_inner_valid[15]_i_3_n_0\,
      I1 => \cnt_inner_valid[15]_i_4_n_0\,
      I2 => cnt_inner_valid(4),
      I3 => \cnt_inner_valid[15]_i_5_n_0\,
      I4 => m00_axis_tvalid_r_i_3_n_0,
      I5 => data0(11),
      O => cnt_inner_valid_0(11)
    );
\cnt_inner_valid[12]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFE00000000"
    )
        port map (
      I0 => \cnt_inner_valid[15]_i_3_n_0\,
      I1 => \cnt_inner_valid[15]_i_4_n_0\,
      I2 => cnt_inner_valid(4),
      I3 => \cnt_inner_valid[15]_i_5_n_0\,
      I4 => m00_axis_tvalid_r_i_3_n_0,
      I5 => data0(12),
      O => cnt_inner_valid_0(12)
    );
\cnt_inner_valid[13]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFE00000000"
    )
        port map (
      I0 => \cnt_inner_valid[15]_i_3_n_0\,
      I1 => \cnt_inner_valid[15]_i_4_n_0\,
      I2 => cnt_inner_valid(4),
      I3 => \cnt_inner_valid[15]_i_5_n_0\,
      I4 => m00_axis_tvalid_r_i_3_n_0,
      I5 => data0(13),
      O => cnt_inner_valid_0(13)
    );
\cnt_inner_valid[14]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFE00000000"
    )
        port map (
      I0 => \cnt_inner_valid[15]_i_3_n_0\,
      I1 => \cnt_inner_valid[15]_i_4_n_0\,
      I2 => cnt_inner_valid(4),
      I3 => \cnt_inner_valid[15]_i_5_n_0\,
      I4 => m00_axis_tvalid_r_i_3_n_0,
      I5 => data0(14),
      O => cnt_inner_valid_0(14)
    );
\cnt_inner_valid[15]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => s00_axis_tvalid,
      O => \cnt_inner_valid[15]_i_1_n_0\
    );
\cnt_inner_valid[15]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFE00000000"
    )
        port map (
      I0 => \cnt_inner_valid[15]_i_3_n_0\,
      I1 => \cnt_inner_valid[15]_i_4_n_0\,
      I2 => cnt_inner_valid(4),
      I3 => \cnt_inner_valid[15]_i_5_n_0\,
      I4 => m00_axis_tvalid_r_i_3_n_0,
      I5 => data0(15),
      O => cnt_inner_valid_0(15)
    );
\cnt_inner_valid[15]_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FE"
    )
        port map (
      I0 => cnt_inner_valid(15),
      I1 => cnt_inner_valid(14),
      I2 => cnt_inner_valid(5),
      O => \cnt_inner_valid[15]_i_3_n_0\
    );
\cnt_inner_valid[15]_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => cnt_inner_valid(11),
      I1 => cnt_inner_valid(6),
      I2 => cnt_inner_valid(13),
      I3 => cnt_inner_valid(12),
      O => \cnt_inner_valid[15]_i_4_n_0\
    );
\cnt_inner_valid[15]_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7FFF"
    )
        port map (
      I0 => cnt_inner_valid(8),
      I1 => cnt_inner_valid(7),
      I2 => cnt_inner_valid(10),
      I3 => cnt_inner_valid(9),
      O => \cnt_inner_valid[15]_i_5_n_0\
    );
\cnt_inner_valid[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFE00000000"
    )
        port map (
      I0 => \cnt_inner_valid[15]_i_3_n_0\,
      I1 => \cnt_inner_valid[15]_i_4_n_0\,
      I2 => cnt_inner_valid(4),
      I3 => \cnt_inner_valid[15]_i_5_n_0\,
      I4 => m00_axis_tvalid_r_i_3_n_0,
      I5 => data0(1),
      O => cnt_inner_valid_0(1)
    );
\cnt_inner_valid[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFE00000000"
    )
        port map (
      I0 => \cnt_inner_valid[15]_i_3_n_0\,
      I1 => \cnt_inner_valid[15]_i_4_n_0\,
      I2 => cnt_inner_valid(4),
      I3 => \cnt_inner_valid[15]_i_5_n_0\,
      I4 => m00_axis_tvalid_r_i_3_n_0,
      I5 => data0(2),
      O => cnt_inner_valid_0(2)
    );
\cnt_inner_valid[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFE00000000"
    )
        port map (
      I0 => \cnt_inner_valid[15]_i_3_n_0\,
      I1 => \cnt_inner_valid[15]_i_4_n_0\,
      I2 => cnt_inner_valid(4),
      I3 => \cnt_inner_valid[15]_i_5_n_0\,
      I4 => m00_axis_tvalid_r_i_3_n_0,
      I5 => data0(3),
      O => cnt_inner_valid_0(3)
    );
\cnt_inner_valid[4]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFE00000000"
    )
        port map (
      I0 => \cnt_inner_valid[15]_i_3_n_0\,
      I1 => \cnt_inner_valid[15]_i_4_n_0\,
      I2 => cnt_inner_valid(4),
      I3 => \cnt_inner_valid[15]_i_5_n_0\,
      I4 => m00_axis_tvalid_r_i_3_n_0,
      I5 => data0(4),
      O => cnt_inner_valid_0(4)
    );
\cnt_inner_valid[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFE00000000"
    )
        port map (
      I0 => \cnt_inner_valid[15]_i_3_n_0\,
      I1 => \cnt_inner_valid[15]_i_4_n_0\,
      I2 => cnt_inner_valid(4),
      I3 => \cnt_inner_valid[15]_i_5_n_0\,
      I4 => m00_axis_tvalid_r_i_3_n_0,
      I5 => data0(5),
      O => cnt_inner_valid_0(5)
    );
\cnt_inner_valid[6]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFE00000000"
    )
        port map (
      I0 => \cnt_inner_valid[15]_i_3_n_0\,
      I1 => \cnt_inner_valid[15]_i_4_n_0\,
      I2 => cnt_inner_valid(4),
      I3 => \cnt_inner_valid[15]_i_5_n_0\,
      I4 => m00_axis_tvalid_r_i_3_n_0,
      I5 => data0(6),
      O => cnt_inner_valid_0(6)
    );
\cnt_inner_valid[7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFE00000000"
    )
        port map (
      I0 => \cnt_inner_valid[15]_i_3_n_0\,
      I1 => \cnt_inner_valid[15]_i_4_n_0\,
      I2 => cnt_inner_valid(4),
      I3 => \cnt_inner_valid[15]_i_5_n_0\,
      I4 => m00_axis_tvalid_r_i_3_n_0,
      I5 => data0(7),
      O => cnt_inner_valid_0(7)
    );
\cnt_inner_valid[8]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFE00000000"
    )
        port map (
      I0 => \cnt_inner_valid[15]_i_3_n_0\,
      I1 => \cnt_inner_valid[15]_i_4_n_0\,
      I2 => cnt_inner_valid(4),
      I3 => \cnt_inner_valid[15]_i_5_n_0\,
      I4 => m00_axis_tvalid_r_i_3_n_0,
      I5 => data0(8),
      O => cnt_inner_valid_0(8)
    );
\cnt_inner_valid[9]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFE00000000"
    )
        port map (
      I0 => \cnt_inner_valid[15]_i_3_n_0\,
      I1 => \cnt_inner_valid[15]_i_4_n_0\,
      I2 => cnt_inner_valid(4),
      I3 => \cnt_inner_valid[15]_i_5_n_0\,
      I4 => m00_axis_tvalid_r_i_3_n_0,
      I5 => data0(9),
      O => cnt_inner_valid_0(9)
    );
\cnt_inner_valid_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => cnt_inner_valid_0(0),
      Q => cnt_inner_valid(0),
      R => \cnt_inner_valid[15]_i_1_n_0\
    );
\cnt_inner_valid_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => cnt_inner_valid_0(10),
      Q => cnt_inner_valid(10),
      R => \cnt_inner_valid[15]_i_1_n_0\
    );
\cnt_inner_valid_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => cnt_inner_valid_0(11),
      Q => cnt_inner_valid(11),
      R => \cnt_inner_valid[15]_i_1_n_0\
    );
\cnt_inner_valid_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => cnt_inner_valid_0(12),
      Q => cnt_inner_valid(12),
      R => \cnt_inner_valid[15]_i_1_n_0\
    );
\cnt_inner_valid_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => cnt_inner_valid_0(13),
      Q => cnt_inner_valid(13),
      R => \cnt_inner_valid[15]_i_1_n_0\
    );
\cnt_inner_valid_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => cnt_inner_valid_0(14),
      Q => cnt_inner_valid(14),
      R => \cnt_inner_valid[15]_i_1_n_0\
    );
\cnt_inner_valid_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => cnt_inner_valid_0(15),
      Q => cnt_inner_valid(15),
      R => \cnt_inner_valid[15]_i_1_n_0\
    );
\cnt_inner_valid_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => cnt_inner_valid_0(1),
      Q => cnt_inner_valid(1),
      R => \cnt_inner_valid[15]_i_1_n_0\
    );
\cnt_inner_valid_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => cnt_inner_valid_0(2),
      Q => cnt_inner_valid(2),
      R => \cnt_inner_valid[15]_i_1_n_0\
    );
\cnt_inner_valid_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => cnt_inner_valid_0(3),
      Q => cnt_inner_valid(3),
      R => \cnt_inner_valid[15]_i_1_n_0\
    );
\cnt_inner_valid_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => cnt_inner_valid_0(4),
      Q => cnt_inner_valid(4),
      R => \cnt_inner_valid[15]_i_1_n_0\
    );
\cnt_inner_valid_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => cnt_inner_valid_0(5),
      Q => cnt_inner_valid(5),
      R => \cnt_inner_valid[15]_i_1_n_0\
    );
\cnt_inner_valid_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => cnt_inner_valid_0(6),
      Q => cnt_inner_valid(6),
      R => \cnt_inner_valid[15]_i_1_n_0\
    );
\cnt_inner_valid_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => cnt_inner_valid_0(7),
      Q => cnt_inner_valid(7),
      R => \cnt_inner_valid[15]_i_1_n_0\
    );
\cnt_inner_valid_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => cnt_inner_valid_0(8),
      Q => cnt_inner_valid(8),
      R => \cnt_inner_valid[15]_i_1_n_0\
    );
\cnt_inner_valid_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => cnt_inner_valid_0(9),
      Q => cnt_inner_valid(9),
      R => \cnt_inner_valid[15]_i_1_n_0\
    );
m00_axis_tvalid_r_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"1400"
    )
        port map (
      I0 => m00_axis_tvalid_r_i_2_n_0,
      I1 => cnt_inner_valid(4),
      I2 => m00_axis_tvalid_r_i_3_n_0,
      I3 => m00_axis_tvalid_r_i_4_n_0,
      O => m00_axis_tvalid_r0
    );
m00_axis_tvalid_r_i_2: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => \cnt_inner_valid[15]_i_4_n_0\,
      I1 => cnt_inner_valid(5),
      I2 => cnt_inner_valid(14),
      I3 => cnt_inner_valid(15),
      O => m00_axis_tvalid_r_i_2_n_0
    );
m00_axis_tvalid_r_i_3: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => cnt_inner_valid(1),
      I1 => cnt_inner_valid(2),
      I2 => cnt_inner_valid(3),
      I3 => cnt_inner_valid(0),
      O => m00_axis_tvalid_r_i_3_n_0
    );
m00_axis_tvalid_r_i_4: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0001"
    )
        port map (
      I0 => cnt_inner_valid(10),
      I1 => cnt_inner_valid(9),
      I2 => cnt_inner_valid(8),
      I3 => cnt_inner_valid(7),
      O => m00_axis_tvalid_r_i_4_n_0
    );
m00_axis_tvalid_r_reg: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => s00_axis_tvalid,
      D => m00_axis_tvalid_r0,
      Q => m00_axis_tvalid,
      R => '0'
    );
rams_sp_rf_rst_inst0: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst
     port map (
      Q(0) => EN_r(0),
      WE_r => WE_r,
      addr(10 downto 0) => adr_wr(10 downto 0),
      m00_axis_aclk => m00_axis_aclk,
      m00_axis_tdata(31 downto 0) => m00_axis_tdata(31 downto 0),
      m00_axis_tdata_31_sp_1 => m00_axis_tdata_31_sn_1,
      ram_reg_1_0(31 downto 0) => DI0(31 downto 0),
      s00_axis_tuser(0) => s00_axis_tuser(11),
      square_r => square_r
    );
rams_sp_rf_rst_inst1: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_0
     port map (
      Q(0) => EN_r(1),
      WE_r => WE_r,
      addr(10 downto 0) => adr_wr(10 downto 0),
      m00_axis_aclk => m00_axis_aclk,
      m00_axis_tdata(31 downto 0) => m00_axis_tdata(31 downto 0),
      m00_axis_tdata_31_sp_1 => m00_axis_tdata_31_sn_1,
      ram_reg_1_0(31 downto 0) => DI0(31 downto 0),
      s00_axis_tuser(0) => s00_axis_tuser(11),
      square_r => square_r
    );
square_r_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"74"
    )
        port map (
      I0 => s00_axis_tuser(12),
      I1 => s00_axis_tvalid,
      I2 => square_r,
      O => square_r_i_1_n_0
    );
square_r_reg: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => square_r_i_1_n_0,
      Q => square_r,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  port (
    m00_axis_aclk : in STD_LOGIC;
    m00_axis_aresetn : in STD_LOGIC;
    m00_axis_tvalid : out STD_LOGIC;
    m00_axis_tdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    m00_axis_tstrb : out STD_LOGIC_VECTOR ( 3 downto 0 );
    m00_axis_tlast : out STD_LOGIC;
    m00_axis_tready : in STD_LOGIC;
    s00_axis_aclk : in STD_LOGIC;
    s00_axis_aresetn : in STD_LOGIC;
    s00_axis_tready : out STD_LOGIC;
    s00_axis_tdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s00_axis_tuser : in STD_LOGIC_VECTOR ( 47 downto 0 );
    s00_axis_tstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s00_axis_tlast : in STD_LOGIC;
    s00_axis_tvalid : in STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "design_2_pixelReOrder_0_0,pixelReOrder_v1_0,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "yes";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "pixelReOrder_v1_0,Vivado 2019.1";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  signal \<const1>\ : STD_LOGIC;
  signal \m00_axis_tdata[31]_INST_0_i_1_n_0\ : STD_LOGIC;
  signal \m00_axis_tdata[31]_INST_0_i_2_n_0\ : STD_LOGIC;
  signal \m00_axis_tdata[31]_INST_0_i_3_n_0\ : STD_LOGIC;
  signal \m00_axis_tdata[31]_INST_0_i_4_n_0\ : STD_LOGIC;
  attribute X_INTERFACE_INFO : string;
  attribute X_INTERFACE_INFO of m00_axis_aclk : signal is "xilinx.com:signal:clock:1.0 M00_AXIS_CLK CLK";
  attribute X_INTERFACE_PARAMETER : string;
  attribute X_INTERFACE_PARAMETER of m00_axis_aclk : signal is "XIL_INTERFACENAME M00_AXIS_CLK, ASSOCIATED_BUSIF M00_AXIS, ASSOCIATED_RESET m00_axis_aresetn, FREQ_HZ 200000000, PHASE 0.000, CLK_DOMAIN design_2_processing_system7_0_0_FCLK_CLK0, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of m00_axis_aresetn : signal is "xilinx.com:signal:reset:1.0 M00_AXIS_RST RST";
  attribute X_INTERFACE_PARAMETER of m00_axis_aresetn : signal is "XIL_INTERFACENAME M00_AXIS_RST, POLARITY ACTIVE_LOW, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of m00_axis_tlast : signal is "xilinx.com:interface:axis:1.0 M00_AXIS TLAST";
  attribute X_INTERFACE_INFO of m00_axis_tready : signal is "xilinx.com:interface:axis:1.0 M00_AXIS TREADY";
  attribute X_INTERFACE_PARAMETER of m00_axis_tready : signal is "XIL_INTERFACENAME M00_AXIS, WIZ_DATA_WIDTH 32, TDATA_NUM_BYTES 4, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 1, HAS_TKEEP 0, HAS_TLAST 1, FREQ_HZ 200000000, PHASE 0.000, CLK_DOMAIN design_2_processing_system7_0_0_FCLK_CLK0, LAYERED_METADATA undef, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of m00_axis_tvalid : signal is "xilinx.com:interface:axis:1.0 M00_AXIS TVALID";
  attribute X_INTERFACE_INFO of s00_axis_aclk : signal is "xilinx.com:signal:clock:1.0 S00_AXIS_CLK CLK";
  attribute X_INTERFACE_PARAMETER of s00_axis_aclk : signal is "XIL_INTERFACENAME S00_AXIS_CLK, ASSOCIATED_BUSIF S00_AXIS, ASSOCIATED_RESET s00_axis_aresetn, FREQ_HZ 200000000, PHASE 0.000, CLK_DOMAIN design_2_processing_system7_0_0_FCLK_CLK0, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of s00_axis_aresetn : signal is "xilinx.com:signal:reset:1.0 S00_AXIS_RST RST";
  attribute X_INTERFACE_PARAMETER of s00_axis_aresetn : signal is "XIL_INTERFACENAME S00_AXIS_RST, POLARITY ACTIVE_LOW, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of s00_axis_tlast : signal is "xilinx.com:interface:axis:1.0 S00_AXIS TLAST";
  attribute X_INTERFACE_INFO of s00_axis_tready : signal is "xilinx.com:interface:axis:1.0 S00_AXIS TREADY";
  attribute X_INTERFACE_INFO of s00_axis_tvalid : signal is "xilinx.com:interface:axis:1.0 S00_AXIS TVALID";
  attribute X_INTERFACE_PARAMETER of s00_axis_tvalid : signal is "XIL_INTERFACENAME S00_AXIS, WIZ_DATA_WIDTH 32, TDATA_NUM_BYTES 4, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 48, HAS_TREADY 1, HAS_TSTRB 1, HAS_TKEEP 0, HAS_TLAST 1, FREQ_HZ 200000000, PHASE 0.000, CLK_DOMAIN design_2_processing_system7_0_0_FCLK_CLK0, LAYERED_METADATA undef, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of m00_axis_tdata : signal is "xilinx.com:interface:axis:1.0 M00_AXIS TDATA";
  attribute X_INTERFACE_INFO of m00_axis_tstrb : signal is "xilinx.com:interface:axis:1.0 M00_AXIS TSTRB";
  attribute X_INTERFACE_INFO of s00_axis_tdata : signal is "xilinx.com:interface:axis:1.0 S00_AXIS TDATA";
  attribute X_INTERFACE_INFO of s00_axis_tstrb : signal is "xilinx.com:interface:axis:1.0 S00_AXIS TSTRB";
  attribute X_INTERFACE_INFO of s00_axis_tuser : signal is "xilinx.com:interface:axis:1.0 S00_AXIS TUSER";
begin
  s00_axis_tready <= \<const1>\;
  m00_axis_tlast <= 'Z';
  m00_axis_tstrb(0) <= 'Z';
  m00_axis_tstrb(1) <= 'Z';
  m00_axis_tstrb(2) <= 'Z';
  m00_axis_tstrb(3) <= 'Z';
VCC: unisim.vcomponents.VCC
     port map (
      P => \<const1>\
    );
inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_pixelReOrder_v1_0
     port map (
      m00_axis_aclk => m00_axis_aclk,
      m00_axis_tdata(31 downto 0) => m00_axis_tdata(31 downto 0),
      m00_axis_tdata_31_sp_1 => \m00_axis_tdata[31]_INST_0_i_1_n_0\,
      m00_axis_tvalid => m00_axis_tvalid,
      s00_axis_tdata(31 downto 0) => s00_axis_tdata(31 downto 0),
      s00_axis_tuser(12) => s00_axis_tuser(20),
      s00_axis_tuser(11) => s00_axis_tuser(16),
      s00_axis_tuser(10 downto 0) => s00_axis_tuser(10 downto 0),
      s00_axis_tvalid => s00_axis_tvalid
    );
\m00_axis_tdata[31]_INST_0_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000080"
    )
        port map (
      I0 => \m00_axis_tdata[31]_INST_0_i_2_n_0\,
      I1 => \m00_axis_tdata[31]_INST_0_i_3_n_0\,
      I2 => \m00_axis_tdata[31]_INST_0_i_4_n_0\,
      I3 => s00_axis_tuser(18),
      I4 => s00_axis_tuser(17),
      I5 => s00_axis_tuser(31),
      O => \m00_axis_tdata[31]_INST_0_i_1_n_0\
    );
\m00_axis_tdata[31]_INST_0_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0001"
    )
        port map (
      I0 => s00_axis_tuser(30),
      I1 => s00_axis_tuser(29),
      I2 => s00_axis_tuser(28),
      I3 => s00_axis_tuser(27),
      O => \m00_axis_tdata[31]_INST_0_i_2_n_0\
    );
\m00_axis_tdata[31]_INST_0_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0001"
    )
        port map (
      I0 => s00_axis_tuser(26),
      I1 => s00_axis_tuser(25),
      I2 => s00_axis_tuser(24),
      I3 => s00_axis_tuser(23),
      O => \m00_axis_tdata[31]_INST_0_i_3_n_0\
    );
\m00_axis_tdata[31]_INST_0_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0100"
    )
        port map (
      I0 => s00_axis_tuser(22),
      I1 => s00_axis_tuser(21),
      I2 => s00_axis_tuser(19),
      I3 => s00_axis_tuser(20),
      O => \m00_axis_tdata[31]_INST_0_i_4_n_0\
    );
end STRUCTURE;
