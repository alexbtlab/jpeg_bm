// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (lin64) Build 2552052 Fri May 24 14:47:09 MDT 2019
// Date        : Fri Oct 15 21:04:42 2021
// Host        : alex-HP-Compaq-8200-Elite-CMT-PC running 64-bit Ubuntu 20.04.3 LTS
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ test_bd_pixelReOrder_0_0_sim_netlist.v
// Design      : test_bd_pixelReOrder_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7z020clg484-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_multiplexer
   (Q,
    ctrl_mux_out_2,
    D);
  output [23:0]Q;
  input [4:0]ctrl_mux_out_2;
  input [23:0]D;

  wire [23:0]D;
  wire [23:0]Q;
  wire [4:0]ctrl_mux_out_2;
  wire \y_reg[23]_i_2__0_n_0 ;

  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \y_reg[0] 
       (.CLR(1'b0),
        .D(D[0]),
        .G(\y_reg[23]_i_2__0_n_0 ),
        .GE(1'b1),
        .Q(Q[0]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \y_reg[10] 
       (.CLR(1'b0),
        .D(D[10]),
        .G(\y_reg[23]_i_2__0_n_0 ),
        .GE(1'b1),
        .Q(Q[10]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \y_reg[11] 
       (.CLR(1'b0),
        .D(D[11]),
        .G(\y_reg[23]_i_2__0_n_0 ),
        .GE(1'b1),
        .Q(Q[11]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \y_reg[12] 
       (.CLR(1'b0),
        .D(D[12]),
        .G(\y_reg[23]_i_2__0_n_0 ),
        .GE(1'b1),
        .Q(Q[12]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \y_reg[13] 
       (.CLR(1'b0),
        .D(D[13]),
        .G(\y_reg[23]_i_2__0_n_0 ),
        .GE(1'b1),
        .Q(Q[13]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \y_reg[14] 
       (.CLR(1'b0),
        .D(D[14]),
        .G(\y_reg[23]_i_2__0_n_0 ),
        .GE(1'b1),
        .Q(Q[14]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \y_reg[15] 
       (.CLR(1'b0),
        .D(D[15]),
        .G(\y_reg[23]_i_2__0_n_0 ),
        .GE(1'b1),
        .Q(Q[15]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \y_reg[16] 
       (.CLR(1'b0),
        .D(D[16]),
        .G(\y_reg[23]_i_2__0_n_0 ),
        .GE(1'b1),
        .Q(Q[16]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \y_reg[17] 
       (.CLR(1'b0),
        .D(D[17]),
        .G(\y_reg[23]_i_2__0_n_0 ),
        .GE(1'b1),
        .Q(Q[17]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \y_reg[18] 
       (.CLR(1'b0),
        .D(D[18]),
        .G(\y_reg[23]_i_2__0_n_0 ),
        .GE(1'b1),
        .Q(Q[18]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \y_reg[19] 
       (.CLR(1'b0),
        .D(D[19]),
        .G(\y_reg[23]_i_2__0_n_0 ),
        .GE(1'b1),
        .Q(Q[19]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \y_reg[1] 
       (.CLR(1'b0),
        .D(D[1]),
        .G(\y_reg[23]_i_2__0_n_0 ),
        .GE(1'b1),
        .Q(Q[1]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \y_reg[20] 
       (.CLR(1'b0),
        .D(D[20]),
        .G(\y_reg[23]_i_2__0_n_0 ),
        .GE(1'b1),
        .Q(Q[20]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \y_reg[21] 
       (.CLR(1'b0),
        .D(D[21]),
        .G(\y_reg[23]_i_2__0_n_0 ),
        .GE(1'b1),
        .Q(Q[21]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \y_reg[22] 
       (.CLR(1'b0),
        .D(D[22]),
        .G(\y_reg[23]_i_2__0_n_0 ),
        .GE(1'b1),
        .Q(Q[22]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \y_reg[23] 
       (.CLR(1'b0),
        .D(D[23]),
        .G(\y_reg[23]_i_2__0_n_0 ),
        .GE(1'b1),
        .Q(Q[23]));
  LUT5 #(
    .INIT(32'h55555557)) 
    \y_reg[23]_i_2__0 
       (.I0(ctrl_mux_out_2[4]),
        .I1(ctrl_mux_out_2[1]),
        .I2(ctrl_mux_out_2[0]),
        .I3(ctrl_mux_out_2[2]),
        .I4(ctrl_mux_out_2[3]),
        .O(\y_reg[23]_i_2__0_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \y_reg[2] 
       (.CLR(1'b0),
        .D(D[2]),
        .G(\y_reg[23]_i_2__0_n_0 ),
        .GE(1'b1),
        .Q(Q[2]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \y_reg[3] 
       (.CLR(1'b0),
        .D(D[3]),
        .G(\y_reg[23]_i_2__0_n_0 ),
        .GE(1'b1),
        .Q(Q[3]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \y_reg[4] 
       (.CLR(1'b0),
        .D(D[4]),
        .G(\y_reg[23]_i_2__0_n_0 ),
        .GE(1'b1),
        .Q(Q[4]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \y_reg[5] 
       (.CLR(1'b0),
        .D(D[5]),
        .G(\y_reg[23]_i_2__0_n_0 ),
        .GE(1'b1),
        .Q(Q[5]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \y_reg[6] 
       (.CLR(1'b0),
        .D(D[6]),
        .G(\y_reg[23]_i_2__0_n_0 ),
        .GE(1'b1),
        .Q(Q[6]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \y_reg[7] 
       (.CLR(1'b0),
        .D(D[7]),
        .G(\y_reg[23]_i_2__0_n_0 ),
        .GE(1'b1),
        .Q(Q[7]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \y_reg[8] 
       (.CLR(1'b0),
        .D(D[8]),
        .G(\y_reg[23]_i_2__0_n_0 ),
        .GE(1'b1),
        .Q(Q[8]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \y_reg[9] 
       (.CLR(1'b0),
        .D(D[9]),
        .G(\y_reg[23]_i_2__0_n_0 ),
        .GE(1'b1),
        .Q(Q[9]));
endmodule

(* ORIG_REF_NAME = "multiplexer" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_multiplexer_31
   (m00_axis_tdata,
    ctrl_mux_out_1,
    s00_axis_tuser,
    Q,
    D);
  output [23:0]m00_axis_tdata;
  input [4:0]ctrl_mux_out_1;
  input [0:0]s00_axis_tuser;
  input [23:0]Q;
  input [23:0]D;

  wire [23:0]D;
  wire [23:0]Q;
  wire [4:0]ctrl_mux_out_1;
  wire [23:0]m00_axis_tdata;
  wire [23:0]out_mux_1;
  wire [0:0]s00_axis_tuser;
  wire \y_reg[23]_i_2_n_0 ;

  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \m00_axis_tdata[0]_INST_0 
       (.I0(out_mux_1[0]),
        .I1(s00_axis_tuser),
        .I2(Q[0]),
        .O(m00_axis_tdata[0]));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \m00_axis_tdata[10]_INST_0 
       (.I0(out_mux_1[10]),
        .I1(s00_axis_tuser),
        .I2(Q[10]),
        .O(m00_axis_tdata[10]));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \m00_axis_tdata[11]_INST_0 
       (.I0(out_mux_1[11]),
        .I1(s00_axis_tuser),
        .I2(Q[11]),
        .O(m00_axis_tdata[11]));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \m00_axis_tdata[12]_INST_0 
       (.I0(out_mux_1[12]),
        .I1(s00_axis_tuser),
        .I2(Q[12]),
        .O(m00_axis_tdata[12]));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \m00_axis_tdata[13]_INST_0 
       (.I0(out_mux_1[13]),
        .I1(s00_axis_tuser),
        .I2(Q[13]),
        .O(m00_axis_tdata[13]));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \m00_axis_tdata[14]_INST_0 
       (.I0(out_mux_1[14]),
        .I1(s00_axis_tuser),
        .I2(Q[14]),
        .O(m00_axis_tdata[14]));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \m00_axis_tdata[15]_INST_0 
       (.I0(out_mux_1[15]),
        .I1(s00_axis_tuser),
        .I2(Q[15]),
        .O(m00_axis_tdata[15]));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \m00_axis_tdata[16]_INST_0 
       (.I0(out_mux_1[16]),
        .I1(s00_axis_tuser),
        .I2(Q[16]),
        .O(m00_axis_tdata[16]));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \m00_axis_tdata[17]_INST_0 
       (.I0(out_mux_1[17]),
        .I1(s00_axis_tuser),
        .I2(Q[17]),
        .O(m00_axis_tdata[17]));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \m00_axis_tdata[18]_INST_0 
       (.I0(out_mux_1[18]),
        .I1(s00_axis_tuser),
        .I2(Q[18]),
        .O(m00_axis_tdata[18]));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \m00_axis_tdata[19]_INST_0 
       (.I0(out_mux_1[19]),
        .I1(s00_axis_tuser),
        .I2(Q[19]),
        .O(m00_axis_tdata[19]));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \m00_axis_tdata[1]_INST_0 
       (.I0(out_mux_1[1]),
        .I1(s00_axis_tuser),
        .I2(Q[1]),
        .O(m00_axis_tdata[1]));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \m00_axis_tdata[20]_INST_0 
       (.I0(out_mux_1[20]),
        .I1(s00_axis_tuser),
        .I2(Q[20]),
        .O(m00_axis_tdata[20]));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \m00_axis_tdata[21]_INST_0 
       (.I0(out_mux_1[21]),
        .I1(s00_axis_tuser),
        .I2(Q[21]),
        .O(m00_axis_tdata[21]));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \m00_axis_tdata[22]_INST_0 
       (.I0(out_mux_1[22]),
        .I1(s00_axis_tuser),
        .I2(Q[22]),
        .O(m00_axis_tdata[22]));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \m00_axis_tdata[23]_INST_0 
       (.I0(out_mux_1[23]),
        .I1(s00_axis_tuser),
        .I2(Q[23]),
        .O(m00_axis_tdata[23]));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \m00_axis_tdata[2]_INST_0 
       (.I0(out_mux_1[2]),
        .I1(s00_axis_tuser),
        .I2(Q[2]),
        .O(m00_axis_tdata[2]));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \m00_axis_tdata[3]_INST_0 
       (.I0(out_mux_1[3]),
        .I1(s00_axis_tuser),
        .I2(Q[3]),
        .O(m00_axis_tdata[3]));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \m00_axis_tdata[4]_INST_0 
       (.I0(out_mux_1[4]),
        .I1(s00_axis_tuser),
        .I2(Q[4]),
        .O(m00_axis_tdata[4]));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \m00_axis_tdata[5]_INST_0 
       (.I0(out_mux_1[5]),
        .I1(s00_axis_tuser),
        .I2(Q[5]),
        .O(m00_axis_tdata[5]));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \m00_axis_tdata[6]_INST_0 
       (.I0(out_mux_1[6]),
        .I1(s00_axis_tuser),
        .I2(Q[6]),
        .O(m00_axis_tdata[6]));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \m00_axis_tdata[7]_INST_0 
       (.I0(out_mux_1[7]),
        .I1(s00_axis_tuser),
        .I2(Q[7]),
        .O(m00_axis_tdata[7]));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \m00_axis_tdata[8]_INST_0 
       (.I0(out_mux_1[8]),
        .I1(s00_axis_tuser),
        .I2(Q[8]),
        .O(m00_axis_tdata[8]));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \m00_axis_tdata[9]_INST_0 
       (.I0(out_mux_1[9]),
        .I1(s00_axis_tuser),
        .I2(Q[9]),
        .O(m00_axis_tdata[9]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \y_reg[0] 
       (.CLR(1'b0),
        .D(D[0]),
        .G(\y_reg[23]_i_2_n_0 ),
        .GE(1'b1),
        .Q(out_mux_1[0]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \y_reg[10] 
       (.CLR(1'b0),
        .D(D[10]),
        .G(\y_reg[23]_i_2_n_0 ),
        .GE(1'b1),
        .Q(out_mux_1[10]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \y_reg[11] 
       (.CLR(1'b0),
        .D(D[11]),
        .G(\y_reg[23]_i_2_n_0 ),
        .GE(1'b1),
        .Q(out_mux_1[11]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \y_reg[12] 
       (.CLR(1'b0),
        .D(D[12]),
        .G(\y_reg[23]_i_2_n_0 ),
        .GE(1'b1),
        .Q(out_mux_1[12]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \y_reg[13] 
       (.CLR(1'b0),
        .D(D[13]),
        .G(\y_reg[23]_i_2_n_0 ),
        .GE(1'b1),
        .Q(out_mux_1[13]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \y_reg[14] 
       (.CLR(1'b0),
        .D(D[14]),
        .G(\y_reg[23]_i_2_n_0 ),
        .GE(1'b1),
        .Q(out_mux_1[14]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \y_reg[15] 
       (.CLR(1'b0),
        .D(D[15]),
        .G(\y_reg[23]_i_2_n_0 ),
        .GE(1'b1),
        .Q(out_mux_1[15]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \y_reg[16] 
       (.CLR(1'b0),
        .D(D[16]),
        .G(\y_reg[23]_i_2_n_0 ),
        .GE(1'b1),
        .Q(out_mux_1[16]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \y_reg[17] 
       (.CLR(1'b0),
        .D(D[17]),
        .G(\y_reg[23]_i_2_n_0 ),
        .GE(1'b1),
        .Q(out_mux_1[17]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \y_reg[18] 
       (.CLR(1'b0),
        .D(D[18]),
        .G(\y_reg[23]_i_2_n_0 ),
        .GE(1'b1),
        .Q(out_mux_1[18]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \y_reg[19] 
       (.CLR(1'b0),
        .D(D[19]),
        .G(\y_reg[23]_i_2_n_0 ),
        .GE(1'b1),
        .Q(out_mux_1[19]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \y_reg[1] 
       (.CLR(1'b0),
        .D(D[1]),
        .G(\y_reg[23]_i_2_n_0 ),
        .GE(1'b1),
        .Q(out_mux_1[1]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \y_reg[20] 
       (.CLR(1'b0),
        .D(D[20]),
        .G(\y_reg[23]_i_2_n_0 ),
        .GE(1'b1),
        .Q(out_mux_1[20]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \y_reg[21] 
       (.CLR(1'b0),
        .D(D[21]),
        .G(\y_reg[23]_i_2_n_0 ),
        .GE(1'b1),
        .Q(out_mux_1[21]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \y_reg[22] 
       (.CLR(1'b0),
        .D(D[22]),
        .G(\y_reg[23]_i_2_n_0 ),
        .GE(1'b1),
        .Q(out_mux_1[22]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \y_reg[23] 
       (.CLR(1'b0),
        .D(D[23]),
        .G(\y_reg[23]_i_2_n_0 ),
        .GE(1'b1),
        .Q(out_mux_1[23]));
  LUT5 #(
    .INIT(32'h55555557)) 
    \y_reg[23]_i_2 
       (.I0(ctrl_mux_out_1[4]),
        .I1(ctrl_mux_out_1[1]),
        .I2(ctrl_mux_out_1[0]),
        .I3(ctrl_mux_out_1[2]),
        .I4(ctrl_mux_out_1[3]),
        .O(\y_reg[23]_i_2_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \y_reg[2] 
       (.CLR(1'b0),
        .D(D[2]),
        .G(\y_reg[23]_i_2_n_0 ),
        .GE(1'b1),
        .Q(out_mux_1[2]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \y_reg[3] 
       (.CLR(1'b0),
        .D(D[3]),
        .G(\y_reg[23]_i_2_n_0 ),
        .GE(1'b1),
        .Q(out_mux_1[3]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \y_reg[4] 
       (.CLR(1'b0),
        .D(D[4]),
        .G(\y_reg[23]_i_2_n_0 ),
        .GE(1'b1),
        .Q(out_mux_1[4]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \y_reg[5] 
       (.CLR(1'b0),
        .D(D[5]),
        .G(\y_reg[23]_i_2_n_0 ),
        .GE(1'b1),
        .Q(out_mux_1[5]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \y_reg[6] 
       (.CLR(1'b0),
        .D(D[6]),
        .G(\y_reg[23]_i_2_n_0 ),
        .GE(1'b1),
        .Q(out_mux_1[6]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \y_reg[7] 
       (.CLR(1'b0),
        .D(D[7]),
        .G(\y_reg[23]_i_2_n_0 ),
        .GE(1'b1),
        .Q(out_mux_1[7]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \y_reg[8] 
       (.CLR(1'b0),
        .D(D[8]),
        .G(\y_reg[23]_i_2_n_0 ),
        .GE(1'b1),
        .Q(out_mux_1[8]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \y_reg[9] 
       (.CLR(1'b0),
        .D(D[9]),
        .G(\y_reg[23]_i_2_n_0 ),
        .GE(1'b1),
        .Q(out_mux_1[9]));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_pixelReOrder_v1_0
   (m00_axis_tlast,
    m00_axis_tvalid,
    m00_axis_tdata,
    s00_axis_tuser,
    m00_axis_aclk,
    s00_axis_tdata,
    s00_axis_tvalid,
    s00_axis_aresetn);
  output m00_axis_tlast;
  output m00_axis_tvalid;
  output [23:0]m00_axis_tdata;
  input [14:0]s00_axis_tuser;
  input m00_axis_aclk;
  input [23:0]s00_axis_tdata;
  input s00_axis_tvalid;
  input s00_axis_aresetn;

  wire [10:0]ADDR2;
  wire \EN1[0]_i_1_n_0 ;
  wire \EN1[0]_i_2_n_0 ;
  wire \EN1[10]_i_1_n_0 ;
  wire \EN1[11]_i_1_n_0 ;
  wire \EN1[12]_i_1_n_0 ;
  wire \EN1[13]_i_1_n_0 ;
  wire \EN1[14]_i_1_n_0 ;
  wire \EN1[14]_i_2_n_0 ;
  wire \EN1[15]_i_1_n_0 ;
  wire \EN1[15]_i_2_n_0 ;
  wire \EN1[15]_i_3_n_0 ;
  wire \EN1[15]_i_4_n_0 ;
  wire \EN1[15]_i_5_n_0 ;
  wire \EN1[15]_i_6_n_0 ;
  wire \EN1[15]_i_7_n_0 ;
  wire \EN1[1]_i_1_n_0 ;
  wire \EN1[2]_i_1_n_0 ;
  wire \EN1[3]_i_1_n_0 ;
  wire \EN1[3]_i_2_n_0 ;
  wire \EN1[4]_i_1_n_0 ;
  wire \EN1[4]_i_2_n_0 ;
  wire \EN1[5]_i_1_n_0 ;
  wire \EN1[6]_i_1_n_0 ;
  wire \EN1[6]_i_2_n_0 ;
  wire \EN1[7]_i_1_n_0 ;
  wire \EN1[7]_i_2_n_0 ;
  wire \EN1[7]_i_3_n_0 ;
  wire \EN1[8]_i_1_n_0 ;
  wire \EN1[8]_i_2_n_0 ;
  wire \EN1[9]_i_1_n_0 ;
  wire \EN1_reg_n_0_[0] ;
  wire \EN1_reg_n_0_[10] ;
  wire \EN1_reg_n_0_[11] ;
  wire \EN1_reg_n_0_[12] ;
  wire \EN1_reg_n_0_[13] ;
  wire \EN1_reg_n_0_[14] ;
  wire \EN1_reg_n_0_[15] ;
  wire \EN1_reg_n_0_[1] ;
  wire \EN1_reg_n_0_[2] ;
  wire \EN1_reg_n_0_[3] ;
  wire \EN1_reg_n_0_[4] ;
  wire \EN1_reg_n_0_[5] ;
  wire \EN1_reg_n_0_[6] ;
  wire \EN1_reg_n_0_[7] ;
  wire \EN1_reg_n_0_[8] ;
  wire \EN1_reg_n_0_[9] ;
  wire \EN2[0]_i_1_n_0 ;
  wire \EN2[0]_i_2_n_0 ;
  wire \EN2[0]_i_3_n_0 ;
  wire \EN2[10]_i_1_n_0 ;
  wire \EN2[11]_i_1_n_0 ;
  wire \EN2[12]_i_1_n_0 ;
  wire \EN2[12]_i_2_n_0 ;
  wire \EN2[12]_i_3_n_0 ;
  wire \EN2[13]_i_1_n_0 ;
  wire \EN2[13]_i_2_n_0 ;
  wire \EN2[14]_i_1_n_0 ;
  wire \EN2[14]_i_2_n_0 ;
  wire \EN2[14]_i_3_n_0 ;
  wire \EN2[15]_i_10_n_0 ;
  wire \EN2[15]_i_1_n_0 ;
  wire \EN2[15]_i_2_n_0 ;
  wire \EN2[15]_i_3_n_0 ;
  wire \EN2[15]_i_4_n_0 ;
  wire \EN2[15]_i_5_n_0 ;
  wire \EN2[15]_i_6_n_0 ;
  wire \EN2[15]_i_7_n_0 ;
  wire \EN2[15]_i_8_n_0 ;
  wire \EN2[15]_i_9_n_0 ;
  wire \EN2[1]_i_1_n_0 ;
  wire \EN2[2]_i_1_n_0 ;
  wire \EN2[3]_i_1_n_0 ;
  wire \EN2[3]_i_2_n_0 ;
  wire \EN2[4]_i_1_n_0 ;
  wire \EN2[4]_i_2_n_0 ;
  wire \EN2[5]_i_1_n_0 ;
  wire \EN2[6]_i_1_n_0 ;
  wire \EN2[6]_i_2_n_0 ;
  wire \EN2[7]_i_1_n_0 ;
  wire \EN2[7]_i_2_n_0 ;
  wire \EN2[7]_i_3_n_0 ;
  wire \EN2[8]_i_1_n_0 ;
  wire \EN2[8]_i_2_n_0 ;
  wire \EN2[9]_i_1_n_0 ;
  wire \EN2_reg_n_0_[0] ;
  wire \EN2_reg_n_0_[10] ;
  wire \EN2_reg_n_0_[11] ;
  wire \EN2_reg_n_0_[12] ;
  wire \EN2_reg_n_0_[13] ;
  wire \EN2_reg_n_0_[14] ;
  wire \EN2_reg_n_0_[15] ;
  wire \EN2_reg_n_0_[1] ;
  wire \EN2_reg_n_0_[2] ;
  wire \EN2_reg_n_0_[3] ;
  wire \EN2_reg_n_0_[4] ;
  wire \EN2_reg_n_0_[5] ;
  wire \EN2_reg_n_0_[6] ;
  wire \EN2_reg_n_0_[7] ;
  wire \EN2_reg_n_0_[8] ;
  wire \EN2_reg_n_0_[9] ;
  wire [23:0]\_DOUT_1[0]_0 ;
  wire [23:0]\_DOUT_1[10]_20 ;
  wire [23:0]\_DOUT_1[12]_24 ;
  wire [23:0]\_DOUT_1[13]_26 ;
  wire [23:0]\_DOUT_1[14]_28 ;
  wire [23:0]\_DOUT_1[1]_2 ;
  wire [23:0]\_DOUT_1[2]_4 ;
  wire [23:0]\_DOUT_1[4]_8 ;
  wire [23:0]\_DOUT_1[5]_10 ;
  wire [23:0]\_DOUT_1[6]_12 ;
  wire [23:0]\_DOUT_1[8]_16 ;
  wire [23:0]\_DOUT_1[9]_18 ;
  wire [23:0]\_DOUT_2[0]_1 ;
  wire [23:0]\_DOUT_2[10]_21 ;
  wire [23:0]\_DOUT_2[12]_25 ;
  wire [23:0]\_DOUT_2[13]_27 ;
  wire [23:0]\_DOUT_2[14]_29 ;
  wire [23:0]\_DOUT_2[1]_3 ;
  wire [23:0]\_DOUT_2[2]_5 ;
  wire [23:0]\_DOUT_2[4]_9 ;
  wire [23:0]\_DOUT_2[5]_11 ;
  wire [23:0]\_DOUT_2[6]_13 ;
  wire [23:0]\_DOUT_2[8]_17 ;
  wire [23:0]\_DOUT_2[9]_19 ;
  wire \active_mem1_in_read_state[0]_i_1_n_0 ;
  wire \active_mem1_in_read_state[10]_i_1_n_0 ;
  wire \active_mem1_in_read_state[11]_i_1_n_0 ;
  wire \active_mem1_in_read_state[12]_i_1_n_0 ;
  wire \active_mem1_in_read_state[13]_i_1_n_0 ;
  wire \active_mem1_in_read_state[14]_i_1_n_0 ;
  wire \active_mem1_in_read_state[15]_i_1_n_0 ;
  wire \active_mem1_in_read_state[15]_i_2_n_0 ;
  wire \active_mem1_in_read_state[1]_i_1_n_0 ;
  wire \active_mem1_in_read_state[2]_i_1_n_0 ;
  wire \active_mem1_in_read_state[3]_i_1_n_0 ;
  wire \active_mem1_in_read_state[4]_i_1_n_0 ;
  wire \active_mem1_in_read_state[5]_i_1_n_0 ;
  wire \active_mem1_in_read_state[6]_i_1_n_0 ;
  wire \active_mem1_in_read_state[7]_i_1_n_0 ;
  wire \active_mem1_in_read_state[8]_i_1_n_0 ;
  wire \active_mem1_in_read_state[9]_i_1_n_0 ;
  wire \active_mem1_in_read_state_reg[12]_i_2_n_0 ;
  wire \active_mem1_in_read_state_reg[12]_i_2_n_1 ;
  wire \active_mem1_in_read_state_reg[12]_i_2_n_2 ;
  wire \active_mem1_in_read_state_reg[12]_i_2_n_3 ;
  wire \active_mem1_in_read_state_reg[12]_i_2_n_4 ;
  wire \active_mem1_in_read_state_reg[12]_i_2_n_5 ;
  wire \active_mem1_in_read_state_reg[12]_i_2_n_6 ;
  wire \active_mem1_in_read_state_reg[12]_i_2_n_7 ;
  wire \active_mem1_in_read_state_reg[15]_i_3_n_2 ;
  wire \active_mem1_in_read_state_reg[15]_i_3_n_3 ;
  wire \active_mem1_in_read_state_reg[15]_i_3_n_5 ;
  wire \active_mem1_in_read_state_reg[15]_i_3_n_6 ;
  wire \active_mem1_in_read_state_reg[15]_i_3_n_7 ;
  wire \active_mem1_in_read_state_reg[4]_i_2_n_0 ;
  wire \active_mem1_in_read_state_reg[4]_i_2_n_1 ;
  wire \active_mem1_in_read_state_reg[4]_i_2_n_2 ;
  wire \active_mem1_in_read_state_reg[4]_i_2_n_3 ;
  wire \active_mem1_in_read_state_reg[4]_i_2_n_4 ;
  wire \active_mem1_in_read_state_reg[4]_i_2_n_5 ;
  wire \active_mem1_in_read_state_reg[4]_i_2_n_6 ;
  wire \active_mem1_in_read_state_reg[4]_i_2_n_7 ;
  wire \active_mem1_in_read_state_reg[8]_i_2_n_0 ;
  wire \active_mem1_in_read_state_reg[8]_i_2_n_1 ;
  wire \active_mem1_in_read_state_reg[8]_i_2_n_2 ;
  wire \active_mem1_in_read_state_reg[8]_i_2_n_3 ;
  wire \active_mem1_in_read_state_reg[8]_i_2_n_4 ;
  wire \active_mem1_in_read_state_reg[8]_i_2_n_5 ;
  wire \active_mem1_in_read_state_reg[8]_i_2_n_6 ;
  wire \active_mem1_in_read_state_reg[8]_i_2_n_7 ;
  wire \active_mem1_in_read_state_reg_n_0_[0] ;
  wire \active_mem1_in_read_state_reg_n_0_[10] ;
  wire \active_mem1_in_read_state_reg_n_0_[11] ;
  wire \active_mem1_in_read_state_reg_n_0_[12] ;
  wire \active_mem1_in_read_state_reg_n_0_[13] ;
  wire \active_mem1_in_read_state_reg_n_0_[14] ;
  wire \active_mem1_in_read_state_reg_n_0_[15] ;
  wire \active_mem1_in_read_state_reg_n_0_[1] ;
  wire \active_mem1_in_read_state_reg_n_0_[2] ;
  wire \active_mem1_in_read_state_reg_n_0_[3] ;
  wire \active_mem1_in_read_state_reg_n_0_[4] ;
  wire \active_mem1_in_read_state_reg_n_0_[5] ;
  wire \active_mem1_in_read_state_reg_n_0_[6] ;
  wire \active_mem1_in_read_state_reg_n_0_[7] ;
  wire \active_mem1_in_read_state_reg_n_0_[8] ;
  wire \active_mem1_in_read_state_reg_n_0_[9] ;
  wire \active_mem2_in_read_state[0]_i_1_n_0 ;
  wire \active_mem2_in_read_state[0]_i_3_n_0 ;
  wire [15:0]active_mem2_in_read_state_reg;
  wire \active_mem2_in_read_state_reg[0]_i_2_n_0 ;
  wire \active_mem2_in_read_state_reg[0]_i_2_n_1 ;
  wire \active_mem2_in_read_state_reg[0]_i_2_n_2 ;
  wire \active_mem2_in_read_state_reg[0]_i_2_n_3 ;
  wire \active_mem2_in_read_state_reg[0]_i_2_n_4 ;
  wire \active_mem2_in_read_state_reg[0]_i_2_n_5 ;
  wire \active_mem2_in_read_state_reg[0]_i_2_n_6 ;
  wire \active_mem2_in_read_state_reg[0]_i_2_n_7 ;
  wire \active_mem2_in_read_state_reg[12]_i_1_n_1 ;
  wire \active_mem2_in_read_state_reg[12]_i_1_n_2 ;
  wire \active_mem2_in_read_state_reg[12]_i_1_n_3 ;
  wire \active_mem2_in_read_state_reg[12]_i_1_n_4 ;
  wire \active_mem2_in_read_state_reg[12]_i_1_n_5 ;
  wire \active_mem2_in_read_state_reg[12]_i_1_n_6 ;
  wire \active_mem2_in_read_state_reg[12]_i_1_n_7 ;
  wire \active_mem2_in_read_state_reg[4]_i_1_n_0 ;
  wire \active_mem2_in_read_state_reg[4]_i_1_n_1 ;
  wire \active_mem2_in_read_state_reg[4]_i_1_n_2 ;
  wire \active_mem2_in_read_state_reg[4]_i_1_n_3 ;
  wire \active_mem2_in_read_state_reg[4]_i_1_n_4 ;
  wire \active_mem2_in_read_state_reg[4]_i_1_n_5 ;
  wire \active_mem2_in_read_state_reg[4]_i_1_n_6 ;
  wire \active_mem2_in_read_state_reg[4]_i_1_n_7 ;
  wire \active_mem2_in_read_state_reg[8]_i_1_n_0 ;
  wire \active_mem2_in_read_state_reg[8]_i_1_n_1 ;
  wire \active_mem2_in_read_state_reg[8]_i_1_n_2 ;
  wire \active_mem2_in_read_state_reg[8]_i_1_n_3 ;
  wire \active_mem2_in_read_state_reg[8]_i_1_n_4 ;
  wire \active_mem2_in_read_state_reg[8]_i_1_n_5 ;
  wire \active_mem2_in_read_state_reg[8]_i_1_n_6 ;
  wire \active_mem2_in_read_state_reg[8]_i_1_n_7 ;
  wire \adr_read_reorder_data_mem1[0]_i_1_n_0 ;
  wire \adr_read_reorder_data_mem1[10]_i_1_n_0 ;
  wire \adr_read_reorder_data_mem1[10]_i_2_n_0 ;
  wire \adr_read_reorder_data_mem1[10]_i_3_n_0 ;
  wire \adr_read_reorder_data_mem1[10]_i_4_n_0 ;
  wire \adr_read_reorder_data_mem1[1]_i_1_n_0 ;
  wire \adr_read_reorder_data_mem1[2]_i_1_n_0 ;
  wire \adr_read_reorder_data_mem1[3]_i_1_n_0 ;
  wire \adr_read_reorder_data_mem1[3]_i_2_n_0 ;
  wire \adr_read_reorder_data_mem1[4]_i_1_n_0 ;
  wire \adr_read_reorder_data_mem1[4]_i_2_n_0 ;
  wire \adr_read_reorder_data_mem1[5]_i_1_n_0 ;
  wire \adr_read_reorder_data_mem1[5]_i_2_n_0 ;
  wire \adr_read_reorder_data_mem1[5]_i_3_n_0 ;
  wire \adr_read_reorder_data_mem1[6]_i_1_n_0 ;
  wire \adr_read_reorder_data_mem1[6]_i_2_n_0 ;
  wire \adr_read_reorder_data_mem1[6]_i_3_n_0 ;
  wire \adr_read_reorder_data_mem1[6]_i_4_n_0 ;
  wire \adr_read_reorder_data_mem1[7]_i_1_n_0 ;
  wire \adr_read_reorder_data_mem1[8]_i_1_n_0 ;
  wire \adr_read_reorder_data_mem1[9]_i_1_n_0 ;
  wire \adr_read_reorder_data_mem1_reg_n_0_[0] ;
  wire \adr_read_reorder_data_mem1_reg_n_0_[10] ;
  wire \adr_read_reorder_data_mem1_reg_n_0_[1] ;
  wire \adr_read_reorder_data_mem1_reg_n_0_[2] ;
  wire \adr_read_reorder_data_mem1_reg_n_0_[3] ;
  wire \adr_read_reorder_data_mem1_reg_n_0_[4] ;
  wire \adr_read_reorder_data_mem1_reg_n_0_[5] ;
  wire \adr_read_reorder_data_mem1_reg_n_0_[6] ;
  wire \adr_read_reorder_data_mem1_reg_n_0_[7] ;
  wire \adr_read_reorder_data_mem1_reg_n_0_[8] ;
  wire \adr_read_reorder_data_mem1_reg_n_0_[9] ;
  wire \adr_read_reorder_data_mem2[10]_i_1_n_0 ;
  wire \adr_read_reorder_data_mem2[10]_i_2_n_0 ;
  wire \adr_read_reorder_data_mem2[10]_i_4_n_0 ;
  wire \adr_read_reorder_data_mem2[1]_i_2_n_0 ;
  wire \adr_read_reorder_data_mem2[1]_i_3_n_0 ;
  wire \adr_read_reorder_data_mem2[2]_i_2_n_0 ;
  wire \adr_read_reorder_data_mem2[3]_i_2_n_0 ;
  wire \adr_read_reorder_data_mem2[4]_i_2_n_0 ;
  wire \adr_read_reorder_data_mem2[5]_i_2_n_0 ;
  wire \adr_read_reorder_data_mem2[5]_i_3_n_0 ;
  wire \adr_read_reorder_data_mem2[6]_i_2_n_0 ;
  wire \adr_read_reorder_data_mem2[6]_i_3_n_0 ;
  wire \adr_read_reorder_data_mem2[6]_i_4_n_0 ;
  wire \adr_read_reorder_data_mem2[6]_i_5_n_0 ;
  wire \adr_read_reorder_data_mem2_reg_n_0_[0] ;
  wire \adr_read_reorder_data_mem2_reg_n_0_[10] ;
  wire \adr_read_reorder_data_mem2_reg_n_0_[1] ;
  wire \adr_read_reorder_data_mem2_reg_n_0_[2] ;
  wire \adr_read_reorder_data_mem2_reg_n_0_[3] ;
  wire \adr_read_reorder_data_mem2_reg_n_0_[4] ;
  wire \adr_read_reorder_data_mem2_reg_n_0_[5] ;
  wire \adr_read_reorder_data_mem2_reg_n_0_[6] ;
  wire \adr_read_reorder_data_mem2_reg_n_0_[7] ;
  wire \adr_read_reorder_data_mem2_reg_n_0_[8] ;
  wire \adr_read_reorder_data_mem2_reg_n_0_[9] ;
  wire [15:0]cnt_inner_valid;
  wire cnt_inner_valid0_carry__0_n_0;
  wire cnt_inner_valid0_carry__0_n_1;
  wire cnt_inner_valid0_carry__0_n_2;
  wire cnt_inner_valid0_carry__0_n_3;
  wire cnt_inner_valid0_carry__0_n_4;
  wire cnt_inner_valid0_carry__0_n_5;
  wire cnt_inner_valid0_carry__0_n_6;
  wire cnt_inner_valid0_carry__0_n_7;
  wire cnt_inner_valid0_carry__1_n_0;
  wire cnt_inner_valid0_carry__1_n_1;
  wire cnt_inner_valid0_carry__1_n_2;
  wire cnt_inner_valid0_carry__1_n_3;
  wire cnt_inner_valid0_carry__1_n_4;
  wire cnt_inner_valid0_carry__1_n_5;
  wire cnt_inner_valid0_carry__1_n_6;
  wire cnt_inner_valid0_carry__1_n_7;
  wire cnt_inner_valid0_carry__2_n_2;
  wire cnt_inner_valid0_carry__2_n_3;
  wire cnt_inner_valid0_carry__2_n_5;
  wire cnt_inner_valid0_carry__2_n_6;
  wire cnt_inner_valid0_carry__2_n_7;
  wire cnt_inner_valid0_carry_n_0;
  wire cnt_inner_valid0_carry_n_1;
  wire cnt_inner_valid0_carry_n_2;
  wire cnt_inner_valid0_carry_n_3;
  wire cnt_inner_valid0_carry_n_4;
  wire cnt_inner_valid0_carry_n_5;
  wire cnt_inner_valid0_carry_n_6;
  wire cnt_inner_valid0_carry_n_7;
  wire \cnt_inner_valid[15]_i_1_n_0 ;
  wire \cnt_inner_valid[15]_i_3_n_0 ;
  wire \cnt_inner_valid[15]_i_4_n_0 ;
  wire \cnt_inner_valid[15]_i_5_n_0 ;
  wire \cnt_inner_valid_reg_n_0_[0] ;
  wire \cnt_inner_valid_reg_n_0_[10] ;
  wire \cnt_inner_valid_reg_n_0_[11] ;
  wire \cnt_inner_valid_reg_n_0_[12] ;
  wire \cnt_inner_valid_reg_n_0_[13] ;
  wire \cnt_inner_valid_reg_n_0_[14] ;
  wire \cnt_inner_valid_reg_n_0_[15] ;
  wire \cnt_inner_valid_reg_n_0_[1] ;
  wire \cnt_inner_valid_reg_n_0_[2] ;
  wire \cnt_inner_valid_reg_n_0_[3] ;
  wire \cnt_inner_valid_reg_n_0_[4] ;
  wire \cnt_inner_valid_reg_n_0_[5] ;
  wire \cnt_inner_valid_reg_n_0_[6] ;
  wire \cnt_inner_valid_reg_n_0_[7] ;
  wire \cnt_inner_valid_reg_n_0_[8] ;
  wire \cnt_inner_valid_reg_n_0_[9] ;
  wire \cnt_last[0]_i_3_n_0 ;
  wire \cnt_last[0]_i_4_n_0 ;
  wire [15:0]cnt_last_reg;
  wire \cnt_last_reg[0]_i_1_n_0 ;
  wire \cnt_last_reg[0]_i_1_n_1 ;
  wire \cnt_last_reg[0]_i_1_n_2 ;
  wire \cnt_last_reg[0]_i_1_n_3 ;
  wire \cnt_last_reg[0]_i_1_n_4 ;
  wire \cnt_last_reg[0]_i_1_n_5 ;
  wire \cnt_last_reg[0]_i_1_n_6 ;
  wire \cnt_last_reg[0]_i_1_n_7 ;
  wire \cnt_last_reg[12]_i_1_n_1 ;
  wire \cnt_last_reg[12]_i_1_n_2 ;
  wire \cnt_last_reg[12]_i_1_n_3 ;
  wire \cnt_last_reg[12]_i_1_n_4 ;
  wire \cnt_last_reg[12]_i_1_n_5 ;
  wire \cnt_last_reg[12]_i_1_n_6 ;
  wire \cnt_last_reg[12]_i_1_n_7 ;
  wire \cnt_last_reg[4]_i_1_n_0 ;
  wire \cnt_last_reg[4]_i_1_n_1 ;
  wire \cnt_last_reg[4]_i_1_n_2 ;
  wire \cnt_last_reg[4]_i_1_n_3 ;
  wire \cnt_last_reg[4]_i_1_n_4 ;
  wire \cnt_last_reg[4]_i_1_n_5 ;
  wire \cnt_last_reg[4]_i_1_n_6 ;
  wire \cnt_last_reg[4]_i_1_n_7 ;
  wire \cnt_last_reg[8]_i_1_n_0 ;
  wire \cnt_last_reg[8]_i_1_n_1 ;
  wire \cnt_last_reg[8]_i_1_n_2 ;
  wire \cnt_last_reg[8]_i_1_n_3 ;
  wire \cnt_last_reg[8]_i_1_n_4 ;
  wire \cnt_last_reg[8]_i_1_n_5 ;
  wire \cnt_last_reg[8]_i_1_n_6 ;
  wire \cnt_last_reg[8]_i_1_n_7 ;
  wire [4:0]ctrl_mux_out_1;
  wire \ctrl_mux_out_1[3]_i_1_n_0 ;
  wire \ctrl_mux_out_1[4]_i_1_n_0 ;
  wire [4:0]ctrl_mux_out_2;
  wire \ctrl_mux_out_2[4]_i_1_n_0 ;
  wire \genblk1[11].rams_sp_rf_rst_inst0_n_0 ;
  wire \genblk1[11].rams_sp_rf_rst_inst0_n_1 ;
  wire \genblk1[11].rams_sp_rf_rst_inst0_n_10 ;
  wire \genblk1[11].rams_sp_rf_rst_inst0_n_11 ;
  wire \genblk1[11].rams_sp_rf_rst_inst0_n_12 ;
  wire \genblk1[11].rams_sp_rf_rst_inst0_n_13 ;
  wire \genblk1[11].rams_sp_rf_rst_inst0_n_14 ;
  wire \genblk1[11].rams_sp_rf_rst_inst0_n_15 ;
  wire \genblk1[11].rams_sp_rf_rst_inst0_n_16 ;
  wire \genblk1[11].rams_sp_rf_rst_inst0_n_17 ;
  wire \genblk1[11].rams_sp_rf_rst_inst0_n_18 ;
  wire \genblk1[11].rams_sp_rf_rst_inst0_n_19 ;
  wire \genblk1[11].rams_sp_rf_rst_inst0_n_2 ;
  wire \genblk1[11].rams_sp_rf_rst_inst0_n_20 ;
  wire \genblk1[11].rams_sp_rf_rst_inst0_n_21 ;
  wire \genblk1[11].rams_sp_rf_rst_inst0_n_22 ;
  wire \genblk1[11].rams_sp_rf_rst_inst0_n_23 ;
  wire \genblk1[11].rams_sp_rf_rst_inst0_n_3 ;
  wire \genblk1[11].rams_sp_rf_rst_inst0_n_4 ;
  wire \genblk1[11].rams_sp_rf_rst_inst0_n_5 ;
  wire \genblk1[11].rams_sp_rf_rst_inst0_n_6 ;
  wire \genblk1[11].rams_sp_rf_rst_inst0_n_7 ;
  wire \genblk1[11].rams_sp_rf_rst_inst0_n_8 ;
  wire \genblk1[11].rams_sp_rf_rst_inst0_n_9 ;
  wire \genblk1[11].rams_sp_rf_rst_inst1_n_0 ;
  wire \genblk1[11].rams_sp_rf_rst_inst1_n_1 ;
  wire \genblk1[11].rams_sp_rf_rst_inst1_n_10 ;
  wire \genblk1[11].rams_sp_rf_rst_inst1_n_11 ;
  wire \genblk1[11].rams_sp_rf_rst_inst1_n_12 ;
  wire \genblk1[11].rams_sp_rf_rst_inst1_n_13 ;
  wire \genblk1[11].rams_sp_rf_rst_inst1_n_14 ;
  wire \genblk1[11].rams_sp_rf_rst_inst1_n_15 ;
  wire \genblk1[11].rams_sp_rf_rst_inst1_n_16 ;
  wire \genblk1[11].rams_sp_rf_rst_inst1_n_17 ;
  wire \genblk1[11].rams_sp_rf_rst_inst1_n_18 ;
  wire \genblk1[11].rams_sp_rf_rst_inst1_n_19 ;
  wire \genblk1[11].rams_sp_rf_rst_inst1_n_2 ;
  wire \genblk1[11].rams_sp_rf_rst_inst1_n_20 ;
  wire \genblk1[11].rams_sp_rf_rst_inst1_n_21 ;
  wire \genblk1[11].rams_sp_rf_rst_inst1_n_22 ;
  wire \genblk1[11].rams_sp_rf_rst_inst1_n_23 ;
  wire \genblk1[11].rams_sp_rf_rst_inst1_n_3 ;
  wire \genblk1[11].rams_sp_rf_rst_inst1_n_4 ;
  wire \genblk1[11].rams_sp_rf_rst_inst1_n_5 ;
  wire \genblk1[11].rams_sp_rf_rst_inst1_n_6 ;
  wire \genblk1[11].rams_sp_rf_rst_inst1_n_7 ;
  wire \genblk1[11].rams_sp_rf_rst_inst1_n_8 ;
  wire \genblk1[11].rams_sp_rf_rst_inst1_n_9 ;
  wire \genblk1[15].rams_sp_rf_rst_inst0_n_0 ;
  wire \genblk1[15].rams_sp_rf_rst_inst0_n_1 ;
  wire \genblk1[15].rams_sp_rf_rst_inst0_n_10 ;
  wire \genblk1[15].rams_sp_rf_rst_inst0_n_11 ;
  wire \genblk1[15].rams_sp_rf_rst_inst0_n_12 ;
  wire \genblk1[15].rams_sp_rf_rst_inst0_n_13 ;
  wire \genblk1[15].rams_sp_rf_rst_inst0_n_14 ;
  wire \genblk1[15].rams_sp_rf_rst_inst0_n_15 ;
  wire \genblk1[15].rams_sp_rf_rst_inst0_n_16 ;
  wire \genblk1[15].rams_sp_rf_rst_inst0_n_17 ;
  wire \genblk1[15].rams_sp_rf_rst_inst0_n_18 ;
  wire \genblk1[15].rams_sp_rf_rst_inst0_n_19 ;
  wire \genblk1[15].rams_sp_rf_rst_inst0_n_2 ;
  wire \genblk1[15].rams_sp_rf_rst_inst0_n_20 ;
  wire \genblk1[15].rams_sp_rf_rst_inst0_n_21 ;
  wire \genblk1[15].rams_sp_rf_rst_inst0_n_22 ;
  wire \genblk1[15].rams_sp_rf_rst_inst0_n_23 ;
  wire \genblk1[15].rams_sp_rf_rst_inst0_n_24 ;
  wire \genblk1[15].rams_sp_rf_rst_inst0_n_25 ;
  wire \genblk1[15].rams_sp_rf_rst_inst0_n_26 ;
  wire \genblk1[15].rams_sp_rf_rst_inst0_n_27 ;
  wire \genblk1[15].rams_sp_rf_rst_inst0_n_28 ;
  wire \genblk1[15].rams_sp_rf_rst_inst0_n_29 ;
  wire \genblk1[15].rams_sp_rf_rst_inst0_n_3 ;
  wire \genblk1[15].rams_sp_rf_rst_inst0_n_30 ;
  wire \genblk1[15].rams_sp_rf_rst_inst0_n_31 ;
  wire \genblk1[15].rams_sp_rf_rst_inst0_n_32 ;
  wire \genblk1[15].rams_sp_rf_rst_inst0_n_33 ;
  wire \genblk1[15].rams_sp_rf_rst_inst0_n_34 ;
  wire \genblk1[15].rams_sp_rf_rst_inst0_n_4 ;
  wire \genblk1[15].rams_sp_rf_rst_inst0_n_5 ;
  wire \genblk1[15].rams_sp_rf_rst_inst0_n_6 ;
  wire \genblk1[15].rams_sp_rf_rst_inst0_n_7 ;
  wire \genblk1[15].rams_sp_rf_rst_inst0_n_8 ;
  wire \genblk1[15].rams_sp_rf_rst_inst0_n_9 ;
  wire \genblk1[15].rams_sp_rf_rst_inst1_n_0 ;
  wire \genblk1[15].rams_sp_rf_rst_inst1_n_1 ;
  wire \genblk1[15].rams_sp_rf_rst_inst1_n_10 ;
  wire \genblk1[15].rams_sp_rf_rst_inst1_n_11 ;
  wire \genblk1[15].rams_sp_rf_rst_inst1_n_12 ;
  wire \genblk1[15].rams_sp_rf_rst_inst1_n_13 ;
  wire \genblk1[15].rams_sp_rf_rst_inst1_n_14 ;
  wire \genblk1[15].rams_sp_rf_rst_inst1_n_15 ;
  wire \genblk1[15].rams_sp_rf_rst_inst1_n_16 ;
  wire \genblk1[15].rams_sp_rf_rst_inst1_n_17 ;
  wire \genblk1[15].rams_sp_rf_rst_inst1_n_18 ;
  wire \genblk1[15].rams_sp_rf_rst_inst1_n_19 ;
  wire \genblk1[15].rams_sp_rf_rst_inst1_n_2 ;
  wire \genblk1[15].rams_sp_rf_rst_inst1_n_20 ;
  wire \genblk1[15].rams_sp_rf_rst_inst1_n_21 ;
  wire \genblk1[15].rams_sp_rf_rst_inst1_n_22 ;
  wire \genblk1[15].rams_sp_rf_rst_inst1_n_23 ;
  wire \genblk1[15].rams_sp_rf_rst_inst1_n_3 ;
  wire \genblk1[15].rams_sp_rf_rst_inst1_n_4 ;
  wire \genblk1[15].rams_sp_rf_rst_inst1_n_5 ;
  wire \genblk1[15].rams_sp_rf_rst_inst1_n_6 ;
  wire \genblk1[15].rams_sp_rf_rst_inst1_n_7 ;
  wire \genblk1[15].rams_sp_rf_rst_inst1_n_8 ;
  wire \genblk1[15].rams_sp_rf_rst_inst1_n_9 ;
  wire \genblk1[3].rams_sp_rf_rst_inst0_n_0 ;
  wire \genblk1[3].rams_sp_rf_rst_inst0_n_1 ;
  wire \genblk1[3].rams_sp_rf_rst_inst0_n_10 ;
  wire \genblk1[3].rams_sp_rf_rst_inst0_n_11 ;
  wire \genblk1[3].rams_sp_rf_rst_inst0_n_12 ;
  wire \genblk1[3].rams_sp_rf_rst_inst0_n_13 ;
  wire \genblk1[3].rams_sp_rf_rst_inst0_n_14 ;
  wire \genblk1[3].rams_sp_rf_rst_inst0_n_15 ;
  wire \genblk1[3].rams_sp_rf_rst_inst0_n_16 ;
  wire \genblk1[3].rams_sp_rf_rst_inst0_n_17 ;
  wire \genblk1[3].rams_sp_rf_rst_inst0_n_18 ;
  wire \genblk1[3].rams_sp_rf_rst_inst0_n_19 ;
  wire \genblk1[3].rams_sp_rf_rst_inst0_n_2 ;
  wire \genblk1[3].rams_sp_rf_rst_inst0_n_20 ;
  wire \genblk1[3].rams_sp_rf_rst_inst0_n_21 ;
  wire \genblk1[3].rams_sp_rf_rst_inst0_n_22 ;
  wire \genblk1[3].rams_sp_rf_rst_inst0_n_23 ;
  wire \genblk1[3].rams_sp_rf_rst_inst0_n_3 ;
  wire \genblk1[3].rams_sp_rf_rst_inst0_n_4 ;
  wire \genblk1[3].rams_sp_rf_rst_inst0_n_5 ;
  wire \genblk1[3].rams_sp_rf_rst_inst0_n_6 ;
  wire \genblk1[3].rams_sp_rf_rst_inst0_n_7 ;
  wire \genblk1[3].rams_sp_rf_rst_inst0_n_8 ;
  wire \genblk1[3].rams_sp_rf_rst_inst0_n_9 ;
  wire \genblk1[3].rams_sp_rf_rst_inst1_n_0 ;
  wire \genblk1[3].rams_sp_rf_rst_inst1_n_1 ;
  wire \genblk1[3].rams_sp_rf_rst_inst1_n_10 ;
  wire \genblk1[3].rams_sp_rf_rst_inst1_n_11 ;
  wire \genblk1[3].rams_sp_rf_rst_inst1_n_12 ;
  wire \genblk1[3].rams_sp_rf_rst_inst1_n_13 ;
  wire \genblk1[3].rams_sp_rf_rst_inst1_n_14 ;
  wire \genblk1[3].rams_sp_rf_rst_inst1_n_15 ;
  wire \genblk1[3].rams_sp_rf_rst_inst1_n_16 ;
  wire \genblk1[3].rams_sp_rf_rst_inst1_n_17 ;
  wire \genblk1[3].rams_sp_rf_rst_inst1_n_18 ;
  wire \genblk1[3].rams_sp_rf_rst_inst1_n_19 ;
  wire \genblk1[3].rams_sp_rf_rst_inst1_n_2 ;
  wire \genblk1[3].rams_sp_rf_rst_inst1_n_20 ;
  wire \genblk1[3].rams_sp_rf_rst_inst1_n_21 ;
  wire \genblk1[3].rams_sp_rf_rst_inst1_n_22 ;
  wire \genblk1[3].rams_sp_rf_rst_inst1_n_23 ;
  wire \genblk1[3].rams_sp_rf_rst_inst1_n_3 ;
  wire \genblk1[3].rams_sp_rf_rst_inst1_n_4 ;
  wire \genblk1[3].rams_sp_rf_rst_inst1_n_5 ;
  wire \genblk1[3].rams_sp_rf_rst_inst1_n_6 ;
  wire \genblk1[3].rams_sp_rf_rst_inst1_n_7 ;
  wire \genblk1[3].rams_sp_rf_rst_inst1_n_8 ;
  wire \genblk1[3].rams_sp_rf_rst_inst1_n_9 ;
  wire \genblk1[7].rams_sp_rf_rst_inst0_n_0 ;
  wire \genblk1[7].rams_sp_rf_rst_inst0_n_1 ;
  wire \genblk1[7].rams_sp_rf_rst_inst0_n_10 ;
  wire \genblk1[7].rams_sp_rf_rst_inst0_n_11 ;
  wire \genblk1[7].rams_sp_rf_rst_inst0_n_12 ;
  wire \genblk1[7].rams_sp_rf_rst_inst0_n_13 ;
  wire \genblk1[7].rams_sp_rf_rst_inst0_n_14 ;
  wire \genblk1[7].rams_sp_rf_rst_inst0_n_15 ;
  wire \genblk1[7].rams_sp_rf_rst_inst0_n_16 ;
  wire \genblk1[7].rams_sp_rf_rst_inst0_n_17 ;
  wire \genblk1[7].rams_sp_rf_rst_inst0_n_18 ;
  wire \genblk1[7].rams_sp_rf_rst_inst0_n_19 ;
  wire \genblk1[7].rams_sp_rf_rst_inst0_n_2 ;
  wire \genblk1[7].rams_sp_rf_rst_inst0_n_20 ;
  wire \genblk1[7].rams_sp_rf_rst_inst0_n_21 ;
  wire \genblk1[7].rams_sp_rf_rst_inst0_n_22 ;
  wire \genblk1[7].rams_sp_rf_rst_inst0_n_23 ;
  wire \genblk1[7].rams_sp_rf_rst_inst0_n_3 ;
  wire \genblk1[7].rams_sp_rf_rst_inst0_n_4 ;
  wire \genblk1[7].rams_sp_rf_rst_inst0_n_5 ;
  wire \genblk1[7].rams_sp_rf_rst_inst0_n_6 ;
  wire \genblk1[7].rams_sp_rf_rst_inst0_n_7 ;
  wire \genblk1[7].rams_sp_rf_rst_inst0_n_8 ;
  wire \genblk1[7].rams_sp_rf_rst_inst0_n_9 ;
  wire \genblk1[7].rams_sp_rf_rst_inst1_n_0 ;
  wire \genblk1[7].rams_sp_rf_rst_inst1_n_1 ;
  wire \genblk1[7].rams_sp_rf_rst_inst1_n_10 ;
  wire \genblk1[7].rams_sp_rf_rst_inst1_n_11 ;
  wire \genblk1[7].rams_sp_rf_rst_inst1_n_12 ;
  wire \genblk1[7].rams_sp_rf_rst_inst1_n_13 ;
  wire \genblk1[7].rams_sp_rf_rst_inst1_n_14 ;
  wire \genblk1[7].rams_sp_rf_rst_inst1_n_15 ;
  wire \genblk1[7].rams_sp_rf_rst_inst1_n_16 ;
  wire \genblk1[7].rams_sp_rf_rst_inst1_n_17 ;
  wire \genblk1[7].rams_sp_rf_rst_inst1_n_18 ;
  wire \genblk1[7].rams_sp_rf_rst_inst1_n_19 ;
  wire \genblk1[7].rams_sp_rf_rst_inst1_n_2 ;
  wire \genblk1[7].rams_sp_rf_rst_inst1_n_20 ;
  wire \genblk1[7].rams_sp_rf_rst_inst1_n_21 ;
  wire \genblk1[7].rams_sp_rf_rst_inst1_n_22 ;
  wire \genblk1[7].rams_sp_rf_rst_inst1_n_23 ;
  wire \genblk1[7].rams_sp_rf_rst_inst1_n_3 ;
  wire \genblk1[7].rams_sp_rf_rst_inst1_n_4 ;
  wire \genblk1[7].rams_sp_rf_rst_inst1_n_5 ;
  wire \genblk1[7].rams_sp_rf_rst_inst1_n_6 ;
  wire \genblk1[7].rams_sp_rf_rst_inst1_n_7 ;
  wire \genblk1[7].rams_sp_rf_rst_inst1_n_8 ;
  wire \genblk1[7].rams_sp_rf_rst_inst1_n_9 ;
  wire last;
  wire locked_EN1_i_1_n_0;
  wire locked_EN1_i_2_n_0;
  wire locked_EN1_reg_n_0;
  wire locked_EN2_i_1_n_0;
  wire locked_EN2_i_2_n_0;
  wire locked_EN2_reg_n_0;
  wire m00_axis_aclk;
  wire [23:0]m00_axis_tdata;
  wire m00_axis_tlast;
  wire m00_axis_tlast_INST_0_i_1_n_0;
  wire m00_axis_tlast_INST_0_i_2_n_0;
  wire m00_axis_tvalid;
  wire m00_axis_tvalid_r_i_1_n_0;
  wire m00_axis_tvalid_r_i_2_n_0;
  wire m00_axis_tvalid_r_i_3_n_0;
  wire m00_axis_tvalid_r_i_4_n_0;
  wire [23:0]out_mux_2;
  wire [10:0]p_1_in;
  wire s00_axis_aresetn;
  wire [23:0]s00_axis_tdata;
  wire [14:0]s00_axis_tuser;
  wire s00_axis_tvalid;
  wire [3:2]\NLW_active_mem1_in_read_state_reg[15]_i_3_CO_UNCONNECTED ;
  wire [3:3]\NLW_active_mem1_in_read_state_reg[15]_i_3_O_UNCONNECTED ;
  wire [3:3]\NLW_active_mem2_in_read_state_reg[12]_i_1_CO_UNCONNECTED ;
  wire [3:2]NLW_cnt_inner_valid0_carry__2_CO_UNCONNECTED;
  wire [3:3]NLW_cnt_inner_valid0_carry__2_O_UNCONNECTED;
  wire [3:3]\NLW_cnt_last_reg[12]_i_1_CO_UNCONNECTED ;

  LUT6 #(
    .INIT(64'h888888888888F888)) 
    \EN1[0]_i_1 
       (.I0(\EN1[0]_i_2_n_0 ),
        .I1(\EN1[6]_i_2_n_0 ),
        .I2(\EN1[3]_i_2_n_0 ),
        .I3(\EN2[0]_i_2_n_0 ),
        .I4(\cnt_inner_valid_reg_n_0_[5] ),
        .I5(\cnt_inner_valid_reg_n_0_[4] ),
        .O(\EN1[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT2 #(
    .INIT(4'h1)) 
    \EN1[0]_i_2 
       (.I0(\active_mem1_in_read_state_reg_n_0_[1] ),
        .I1(\active_mem1_in_read_state_reg_n_0_[2] ),
        .O(\EN1[0]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h20FF202020202020)) 
    \EN1[10]_i_1 
       (.I0(\active_mem1_in_read_state_reg_n_0_[1] ),
        .I1(\active_mem1_in_read_state_reg_n_0_[2] ),
        .I2(\EN1[14]_i_2_n_0 ),
        .I3(\cnt_inner_valid[15]_i_3_n_0 ),
        .I4(\EN2[14]_i_2_n_0 ),
        .I5(\EN1[15]_i_4_n_0 ),
        .O(\EN1[10]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h08FF080808080808)) 
    \EN1[11]_i_1 
       (.I0(\EN1[15]_i_3_n_0 ),
        .I1(\active_mem1_in_read_state_reg_n_0_[1] ),
        .I2(\active_mem1_in_read_state_reg_n_0_[2] ),
        .I3(\cnt_inner_valid[15]_i_3_n_0 ),
        .I4(\EN2[15]_i_4_n_0 ),
        .I5(\EN1[15]_i_4_n_0 ),
        .O(\EN1[11]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFF20202020202020)) 
    \EN1[12]_i_1 
       (.I0(\active_mem1_in_read_state_reg_n_0_[2] ),
        .I1(\active_mem1_in_read_state_reg_n_0_[1] ),
        .I2(\EN1[14]_i_2_n_0 ),
        .I3(\EN2[12]_i_2_n_0 ),
        .I4(\EN1[15]_i_4_n_0 ),
        .I5(\cnt_inner_valid_reg_n_0_[7] ),
        .O(\EN1[12]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFF08080808080808)) 
    \EN1[13]_i_1 
       (.I0(\EN1[15]_i_3_n_0 ),
        .I1(\active_mem1_in_read_state_reg_n_0_[2] ),
        .I2(\active_mem1_in_read_state_reg_n_0_[1] ),
        .I3(\EN2[15]_i_3_n_0 ),
        .I4(\EN2[13]_i_2_n_0 ),
        .I5(\EN1[15]_i_4_n_0 ),
        .O(\EN1[13]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFF80808080808080)) 
    \EN1[14]_i_1 
       (.I0(\active_mem1_in_read_state_reg_n_0_[1] ),
        .I1(\active_mem1_in_read_state_reg_n_0_[2] ),
        .I2(\EN1[14]_i_2_n_0 ),
        .I3(\EN2[15]_i_3_n_0 ),
        .I4(\EN2[14]_i_2_n_0 ),
        .I5(\EN1[15]_i_4_n_0 ),
        .O(\EN1[14]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair22" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \EN1[14]_i_2 
       (.I0(\active_mem1_in_read_state_reg_n_0_[0] ),
        .I1(\EN1[15]_i_5_n_0 ),
        .I2(\active_mem1_in_read_state_reg_n_0_[3] ),
        .O(\EN1[14]_i_2_n_0 ));
  LUT3 #(
    .INIT(8'h80)) 
    \EN1[15]_i_1 
       (.I0(locked_EN1_reg_n_0),
        .I1(s00_axis_tuser[11]),
        .I2(s00_axis_aresetn),
        .O(\EN1[15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFF80808080808080)) 
    \EN1[15]_i_2 
       (.I0(\active_mem1_in_read_state_reg_n_0_[1] ),
        .I1(\active_mem1_in_read_state_reg_n_0_[2] ),
        .I2(\EN1[15]_i_3_n_0 ),
        .I3(\EN2[15]_i_3_n_0 ),
        .I4(\EN2[15]_i_4_n_0 ),
        .I5(\EN1[15]_i_4_n_0 ),
        .O(\EN1[15]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair23" *) 
  LUT3 #(
    .INIT(8'h80)) 
    \EN1[15]_i_3 
       (.I0(\active_mem1_in_read_state_reg_n_0_[0] ),
        .I1(\EN1[15]_i_5_n_0 ),
        .I2(\active_mem1_in_read_state_reg_n_0_[3] ),
        .O(\EN1[15]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT5 #(
    .INIT(32'h00010101)) 
    \EN1[15]_i_4 
       (.I0(\cnt_inner_valid_reg_n_0_[10] ),
        .I1(\cnt_inner_valid_reg_n_0_[8] ),
        .I2(\cnt_inner_valid_reg_n_0_[9] ),
        .I3(s00_axis_tuser[11]),
        .I4(s00_axis_aresetn),
        .O(\EN1[15]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h0000000200000000)) 
    \EN1[15]_i_5 
       (.I0(\EN1[15]_i_6_n_0 ),
        .I1(\active_mem1_in_read_state_reg_n_0_[10] ),
        .I2(\active_mem1_in_read_state_reg_n_0_[9] ),
        .I3(\active_mem1_in_read_state_reg_n_0_[8] ),
        .I4(\active_mem1_in_read_state_reg_n_0_[7] ),
        .I5(\EN1[15]_i_7_n_0 ),
        .O(\EN1[15]_i_5_n_0 ));
  LUT4 #(
    .INIT(16'h0001)) 
    \EN1[15]_i_6 
       (.I0(\active_mem1_in_read_state_reg_n_0_[14] ),
        .I1(\active_mem1_in_read_state_reg_n_0_[13] ),
        .I2(\active_mem1_in_read_state_reg_n_0_[12] ),
        .I3(\active_mem1_in_read_state_reg_n_0_[11] ),
        .O(\EN1[15]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000040)) 
    \EN1[15]_i_7 
       (.I0(\active_mem1_in_read_state_reg_n_0_[15] ),
        .I1(s00_axis_tuser[11]),
        .I2(s00_axis_aresetn),
        .I3(\active_mem1_in_read_state_reg_n_0_[4] ),
        .I4(\active_mem1_in_read_state_reg_n_0_[6] ),
        .I5(\active_mem1_in_read_state_reg_n_0_[5] ),
        .O(\EN1[15]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hFF101010)) 
    \EN1[1]_i_1 
       (.I0(\active_mem1_in_read_state_reg_n_0_[1] ),
        .I1(\active_mem1_in_read_state_reg_n_0_[2] ),
        .I2(\EN1[7]_i_2_n_0 ),
        .I3(\EN2[13]_i_2_n_0 ),
        .I4(\EN1[3]_i_2_n_0 ),
        .O(\EN1[1]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFF202020)) 
    \EN1[2]_i_1 
       (.I0(\active_mem1_in_read_state_reg_n_0_[1] ),
        .I1(\active_mem1_in_read_state_reg_n_0_[2] ),
        .I2(\EN1[6]_i_2_n_0 ),
        .I3(\EN2[14]_i_2_n_0 ),
        .I4(\EN1[3]_i_2_n_0 ),
        .O(\EN1[2]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFF202020)) 
    \EN1[3]_i_1 
       (.I0(\active_mem1_in_read_state_reg_n_0_[1] ),
        .I1(\active_mem1_in_read_state_reg_n_0_[2] ),
        .I2(\EN1[7]_i_2_n_0 ),
        .I3(\EN2[15]_i_4_n_0 ),
        .I4(\EN1[3]_i_2_n_0 ),
        .O(\EN1[3]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000002)) 
    \EN1[3]_i_2 
       (.I0(\adr_read_reorder_data_mem2[10]_i_2_n_0 ),
        .I1(\cnt_inner_valid_reg_n_0_[10] ),
        .I2(\cnt_inner_valid_reg_n_0_[8] ),
        .I3(\cnt_inner_valid_reg_n_0_[9] ),
        .I4(\cnt_inner_valid_reg_n_0_[7] ),
        .I5(\cnt_inner_valid[15]_i_5_n_0 ),
        .O(\EN1[3]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hFF202020)) 
    \EN1[4]_i_1 
       (.I0(\active_mem1_in_read_state_reg_n_0_[2] ),
        .I1(\active_mem1_in_read_state_reg_n_0_[1] ),
        .I2(\EN1[6]_i_2_n_0 ),
        .I3(\EN2[12]_i_2_n_0 ),
        .I4(\EN1[4]_i_2_n_0 ),
        .O(\EN1[4]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000100010001)) 
    \EN1[4]_i_2 
       (.I0(\cnt_inner_valid_reg_n_0_[7] ),
        .I1(\cnt_inner_valid_reg_n_0_[9] ),
        .I2(\cnt_inner_valid_reg_n_0_[8] ),
        .I3(\cnt_inner_valid_reg_n_0_[10] ),
        .I4(s00_axis_tuser[11]),
        .I5(s00_axis_aresetn),
        .O(\EN1[4]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hFF202020)) 
    \EN1[5]_i_1 
       (.I0(\active_mem1_in_read_state_reg_n_0_[2] ),
        .I1(\active_mem1_in_read_state_reg_n_0_[1] ),
        .I2(\EN1[7]_i_2_n_0 ),
        .I3(\EN2[13]_i_2_n_0 ),
        .I4(\EN1[7]_i_3_n_0 ),
        .O(\EN1[5]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT5 #(
    .INIT(32'hFF808080)) 
    \EN1[6]_i_1 
       (.I0(\active_mem1_in_read_state_reg_n_0_[1] ),
        .I1(\active_mem1_in_read_state_reg_n_0_[2] ),
        .I2(\EN1[6]_i_2_n_0 ),
        .I3(\EN2[14]_i_2_n_0 ),
        .I4(\EN1[7]_i_3_n_0 ),
        .O(\EN1[6]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair22" *) 
  LUT3 #(
    .INIT(8'h04)) 
    \EN1[6]_i_2 
       (.I0(\active_mem1_in_read_state_reg_n_0_[0] ),
        .I1(\EN1[15]_i_5_n_0 ),
        .I2(\active_mem1_in_read_state_reg_n_0_[3] ),
        .O(\EN1[6]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hFF808080)) 
    \EN1[7]_i_1 
       (.I0(\active_mem1_in_read_state_reg_n_0_[1] ),
        .I1(\active_mem1_in_read_state_reg_n_0_[2] ),
        .I2(\EN1[7]_i_2_n_0 ),
        .I3(\EN2[15]_i_4_n_0 ),
        .I4(\EN1[7]_i_3_n_0 ),
        .O(\EN1[7]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair23" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \EN1[7]_i_2 
       (.I0(\active_mem1_in_read_state_reg_n_0_[0] ),
        .I1(\EN1[15]_i_5_n_0 ),
        .I2(\active_mem1_in_read_state_reg_n_0_[3] ),
        .O(\EN1[7]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0000000200000000)) 
    \EN1[7]_i_3 
       (.I0(\adr_read_reorder_data_mem2[10]_i_2_n_0 ),
        .I1(\cnt_inner_valid_reg_n_0_[10] ),
        .I2(\cnt_inner_valid_reg_n_0_[8] ),
        .I3(\cnt_inner_valid_reg_n_0_[9] ),
        .I4(\cnt_inner_valid_reg_n_0_[7] ),
        .I5(\EN2[15]_i_7_n_0 ),
        .O(\EN1[7]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h7777777700070000)) 
    \EN1[8]_i_1 
       (.I0(locked_EN1_reg_n_0),
        .I1(s00_axis_tuser[11]),
        .I2(\active_mem1_in_read_state_reg_n_0_[1] ),
        .I3(\active_mem1_in_read_state_reg_n_0_[2] ),
        .I4(\EN1[14]_i_2_n_0 ),
        .I5(\EN1[8]_i_2_n_0 ),
        .O(\EN1[8]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h00000044F0000000)) 
    \EN1[8]_i_2 
       (.I0(\cnt_inner_valid[15]_i_3_n_0 ),
        .I1(\EN1[15]_i_4_n_0 ),
        .I2(\EN1[7]_i_3_n_0 ),
        .I3(\cnt_inner_valid_reg_n_0_[5] ),
        .I4(\cnt_inner_valid_reg_n_0_[4] ),
        .I5(\EN2[0]_i_2_n_0 ),
        .O(\EN1[8]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h02FF020202020202)) 
    \EN1[9]_i_1 
       (.I0(\EN1[15]_i_3_n_0 ),
        .I1(\active_mem1_in_read_state_reg_n_0_[1] ),
        .I2(\active_mem1_in_read_state_reg_n_0_[2] ),
        .I3(\cnt_inner_valid[15]_i_3_n_0 ),
        .I4(\EN2[13]_i_2_n_0 ),
        .I5(\EN1[15]_i_4_n_0 ),
        .O(\EN1[9]_i_1_n_0 ));
  FDRE \EN1_reg[0] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_aresetn),
        .D(\EN1[0]_i_1_n_0 ),
        .Q(\EN1_reg_n_0_[0] ),
        .R(\EN1[15]_i_1_n_0 ));
  FDRE \EN1_reg[10] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_aresetn),
        .D(\EN1[10]_i_1_n_0 ),
        .Q(\EN1_reg_n_0_[10] ),
        .R(\EN1[15]_i_1_n_0 ));
  FDRE \EN1_reg[11] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_aresetn),
        .D(\EN1[11]_i_1_n_0 ),
        .Q(\EN1_reg_n_0_[11] ),
        .R(\EN1[15]_i_1_n_0 ));
  FDRE \EN1_reg[12] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_aresetn),
        .D(\EN1[12]_i_1_n_0 ),
        .Q(\EN1_reg_n_0_[12] ),
        .R(\EN1[15]_i_1_n_0 ));
  FDRE \EN1_reg[13] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_aresetn),
        .D(\EN1[13]_i_1_n_0 ),
        .Q(\EN1_reg_n_0_[13] ),
        .R(\EN1[15]_i_1_n_0 ));
  FDRE \EN1_reg[14] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_aresetn),
        .D(\EN1[14]_i_1_n_0 ),
        .Q(\EN1_reg_n_0_[14] ),
        .R(\EN1[15]_i_1_n_0 ));
  FDRE \EN1_reg[15] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_aresetn),
        .D(\EN1[15]_i_2_n_0 ),
        .Q(\EN1_reg_n_0_[15] ),
        .R(\EN1[15]_i_1_n_0 ));
  FDRE \EN1_reg[1] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_aresetn),
        .D(\EN1[1]_i_1_n_0 ),
        .Q(\EN1_reg_n_0_[1] ),
        .R(\EN1[15]_i_1_n_0 ));
  FDRE \EN1_reg[2] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_aresetn),
        .D(\EN1[2]_i_1_n_0 ),
        .Q(\EN1_reg_n_0_[2] ),
        .R(\EN1[15]_i_1_n_0 ));
  FDRE \EN1_reg[3] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_aresetn),
        .D(\EN1[3]_i_1_n_0 ),
        .Q(\EN1_reg_n_0_[3] ),
        .R(\EN1[15]_i_1_n_0 ));
  FDRE \EN1_reg[4] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_aresetn),
        .D(\EN1[4]_i_1_n_0 ),
        .Q(\EN1_reg_n_0_[4] ),
        .R(\EN1[15]_i_1_n_0 ));
  FDRE \EN1_reg[5] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_aresetn),
        .D(\EN1[5]_i_1_n_0 ),
        .Q(\EN1_reg_n_0_[5] ),
        .R(\EN1[15]_i_1_n_0 ));
  FDRE \EN1_reg[6] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_aresetn),
        .D(\EN1[6]_i_1_n_0 ),
        .Q(\EN1_reg_n_0_[6] ),
        .R(\EN1[15]_i_1_n_0 ));
  FDRE \EN1_reg[7] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_aresetn),
        .D(\EN1[7]_i_1_n_0 ),
        .Q(\EN1_reg_n_0_[7] ),
        .R(\EN1[15]_i_1_n_0 ));
  FDRE \EN1_reg[8] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_aresetn),
        .D(\EN1[8]_i_1_n_0 ),
        .Q(\EN1_reg_n_0_[8] ),
        .R(1'b0));
  FDRE \EN1_reg[9] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_aresetn),
        .D(\EN1[9]_i_1_n_0 ),
        .Q(\EN1_reg_n_0_[9] ),
        .R(\EN1[15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFF000800080008)) 
    \EN2[0]_i_1 
       (.I0(\EN2[3]_i_2_n_0 ),
        .I1(\EN2[0]_i_2_n_0 ),
        .I2(\cnt_inner_valid_reg_n_0_[5] ),
        .I3(\cnt_inner_valid_reg_n_0_[4] ),
        .I4(\EN2[0]_i_3_n_0 ),
        .I5(\EN2[6]_i_2_n_0 ),
        .O(\EN2[0]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h7FFF)) 
    \EN2[0]_i_2 
       (.I0(\cnt_inner_valid_reg_n_0_[1] ),
        .I1(\cnt_inner_valid_reg_n_0_[0] ),
        .I2(\cnt_inner_valid_reg_n_0_[3] ),
        .I3(\cnt_inner_valid_reg_n_0_[2] ),
        .O(\EN2[0]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT2 #(
    .INIT(4'h1)) 
    \EN2[0]_i_3 
       (.I0(active_mem2_in_read_state_reg[1]),
        .I1(active_mem2_in_read_state_reg[2]),
        .O(\EN2[0]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h4040FF4040404040)) 
    \EN2[10]_i_1 
       (.I0(\cnt_inner_valid[15]_i_3_n_0 ),
        .I1(\EN2[14]_i_2_n_0 ),
        .I2(\EN2[15]_i_5_n_0 ),
        .I3(active_mem2_in_read_state_reg[1]),
        .I4(active_mem2_in_read_state_reg[2]),
        .I5(\EN2[14]_i_3_n_0 ),
        .O(\EN2[10]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h40404040FF404040)) 
    \EN2[11]_i_1 
       (.I0(\cnt_inner_valid[15]_i_3_n_0 ),
        .I1(\EN2[15]_i_4_n_0 ),
        .I2(\EN2[15]_i_5_n_0 ),
        .I3(\EN2[15]_i_6_n_0 ),
        .I4(active_mem2_in_read_state_reg[1]),
        .I5(active_mem2_in_read_state_reg[2]),
        .O(\EN2[11]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h8080FF8080808080)) 
    \EN2[12]_i_1 
       (.I0(\EN2[12]_i_2_n_0 ),
        .I1(\EN2[15]_i_5_n_0 ),
        .I2(\cnt_inner_valid_reg_n_0_[7] ),
        .I3(active_mem2_in_read_state_reg[2]),
        .I4(active_mem2_in_read_state_reg[1]),
        .I5(\EN2[14]_i_3_n_0 ),
        .O(\EN2[12]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h00041000)) 
    \EN2[12]_i_2 
       (.I0(\EN2[12]_i_3_n_0 ),
        .I1(\cnt_inner_valid_reg_n_0_[6] ),
        .I2(\cnt_inner_valid_reg_n_0_[5] ),
        .I3(\cnt_inner_valid_reg_n_0_[4] ),
        .I4(\EN2[0]_i_2_n_0 ),
        .O(\EN2[12]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    \EN2[12]_i_3 
       (.I0(\cnt_inner_valid_reg_n_0_[11] ),
        .I1(\cnt_inner_valid_reg_n_0_[14] ),
        .I2(\cnt_inner_valid_reg_n_0_[15] ),
        .I3(\cnt_inner_valid_reg_n_0_[13] ),
        .I4(\cnt_inner_valid_reg_n_0_[12] ),
        .O(\EN2[12]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h80808080FF808080)) 
    \EN2[13]_i_1 
       (.I0(\EN2[15]_i_3_n_0 ),
        .I1(\EN2[13]_i_2_n_0 ),
        .I2(\EN2[15]_i_5_n_0 ),
        .I3(\EN2[15]_i_6_n_0 ),
        .I4(active_mem2_in_read_state_reg[2]),
        .I5(active_mem2_in_read_state_reg[1]),
        .O(\EN2[13]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h000000006AAAAAAA)) 
    \EN2[13]_i_2 
       (.I0(\cnt_inner_valid_reg_n_0_[4] ),
        .I1(\cnt_inner_valid_reg_n_0_[1] ),
        .I2(\cnt_inner_valid_reg_n_0_[0] ),
        .I3(\cnt_inner_valid_reg_n_0_[3] ),
        .I4(\cnt_inner_valid_reg_n_0_[2] ),
        .I5(\cnt_inner_valid_reg_n_0_[5] ),
        .O(\EN2[13]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFF80808080808080)) 
    \EN2[14]_i_1 
       (.I0(\EN2[15]_i_3_n_0 ),
        .I1(\EN2[14]_i_2_n_0 ),
        .I2(\EN2[15]_i_5_n_0 ),
        .I3(active_mem2_in_read_state_reg[1]),
        .I4(active_mem2_in_read_state_reg[2]),
        .I5(\EN2[14]_i_3_n_0 ),
        .O(\EN2[14]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h00007FFF80000000)) 
    \EN2[14]_i_2 
       (.I0(\cnt_inner_valid_reg_n_0_[1] ),
        .I1(\cnt_inner_valid_reg_n_0_[0] ),
        .I2(\cnt_inner_valid_reg_n_0_[3] ),
        .I3(\cnt_inner_valid_reg_n_0_[2] ),
        .I4(\cnt_inner_valid_reg_n_0_[4] ),
        .I5(\cnt_inner_valid_reg_n_0_[5] ),
        .O(\EN2[14]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair25" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \EN2[14]_i_3 
       (.I0(active_mem2_in_read_state_reg[0]),
        .I1(\EN2[15]_i_8_n_0 ),
        .I2(active_mem2_in_read_state_reg[3]),
        .O(\EN2[14]_i_3_n_0 ));
  LUT3 #(
    .INIT(8'h08)) 
    \EN2[15]_i_1 
       (.I0(locked_EN2_reg_n_0),
        .I1(s00_axis_aresetn),
        .I2(s00_axis_tuser[11]),
        .O(\EN2[15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000004)) 
    \EN2[15]_i_10 
       (.I0(active_mem2_in_read_state_reg[15]),
        .I1(s00_axis_aresetn),
        .I2(s00_axis_tuser[11]),
        .I3(active_mem2_in_read_state_reg[4]),
        .I4(active_mem2_in_read_state_reg[6]),
        .I5(active_mem2_in_read_state_reg[5]),
        .O(\EN2[15]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hFF80808080808080)) 
    \EN2[15]_i_2 
       (.I0(\EN2[15]_i_3_n_0 ),
        .I1(\EN2[15]_i_4_n_0 ),
        .I2(\EN2[15]_i_5_n_0 ),
        .I3(active_mem2_in_read_state_reg[1]),
        .I4(active_mem2_in_read_state_reg[2]),
        .I5(\EN2[15]_i_6_n_0 ),
        .O(\EN2[15]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair36" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \EN2[15]_i_3 
       (.I0(\EN2[15]_i_7_n_0 ),
        .I1(\cnt_inner_valid_reg_n_0_[7] ),
        .O(\EN2[15]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h6AAAAAAA00000000)) 
    \EN2[15]_i_4 
       (.I0(\cnt_inner_valid_reg_n_0_[4] ),
        .I1(\cnt_inner_valid_reg_n_0_[1] ),
        .I2(\cnt_inner_valid_reg_n_0_[0] ),
        .I3(\cnt_inner_valid_reg_n_0_[3] ),
        .I4(\cnt_inner_valid_reg_n_0_[2] ),
        .I5(\cnt_inner_valid_reg_n_0_[5] ),
        .O(\EN2[15]_i_4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT5 #(
    .INIT(32'h01010001)) 
    \EN2[15]_i_5 
       (.I0(\cnt_inner_valid_reg_n_0_[10] ),
        .I1(\cnt_inner_valid_reg_n_0_[8] ),
        .I2(\cnt_inner_valid_reg_n_0_[9] ),
        .I3(s00_axis_aresetn),
        .I4(s00_axis_tuser[11]),
        .O(\EN2[15]_i_5_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair27" *) 
  LUT3 #(
    .INIT(8'h80)) 
    \EN2[15]_i_6 
       (.I0(active_mem2_in_read_state_reg[0]),
        .I1(\EN2[15]_i_8_n_0 ),
        .I2(active_mem2_in_read_state_reg[3]),
        .O(\EN2[15]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000002)) 
    \EN2[15]_i_7 
       (.I0(\cnt_inner_valid_reg_n_0_[6] ),
        .I1(\cnt_inner_valid_reg_n_0_[12] ),
        .I2(\cnt_inner_valid_reg_n_0_[13] ),
        .I3(\cnt_inner_valid_reg_n_0_[15] ),
        .I4(\cnt_inner_valid_reg_n_0_[14] ),
        .I5(\cnt_inner_valid_reg_n_0_[11] ),
        .O(\EN2[15]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'h0000000200000000)) 
    \EN2[15]_i_8 
       (.I0(\EN2[15]_i_9_n_0 ),
        .I1(active_mem2_in_read_state_reg[10]),
        .I2(active_mem2_in_read_state_reg[9]),
        .I3(active_mem2_in_read_state_reg[8]),
        .I4(active_mem2_in_read_state_reg[7]),
        .I5(\EN2[15]_i_10_n_0 ),
        .O(\EN2[15]_i_8_n_0 ));
  LUT4 #(
    .INIT(16'h0001)) 
    \EN2[15]_i_9 
       (.I0(active_mem2_in_read_state_reg[14]),
        .I1(active_mem2_in_read_state_reg[13]),
        .I2(active_mem2_in_read_state_reg[12]),
        .I3(active_mem2_in_read_state_reg[11]),
        .O(\EN2[15]_i_9_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT5 #(
    .INIT(32'h888F8888)) 
    \EN2[1]_i_1 
       (.I0(\EN2[13]_i_2_n_0 ),
        .I1(\EN2[3]_i_2_n_0 ),
        .I2(active_mem2_in_read_state_reg[1]),
        .I3(active_mem2_in_read_state_reg[2]),
        .I4(\EN2[7]_i_3_n_0 ),
        .O(\EN2[1]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h88F88888)) 
    \EN2[2]_i_1 
       (.I0(\EN2[14]_i_2_n_0 ),
        .I1(\EN2[3]_i_2_n_0 ),
        .I2(active_mem2_in_read_state_reg[1]),
        .I3(active_mem2_in_read_state_reg[2]),
        .I4(\EN2[6]_i_2_n_0 ),
        .O(\EN2[2]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h88F88888)) 
    \EN2[3]_i_1 
       (.I0(\EN2[15]_i_4_n_0 ),
        .I1(\EN2[3]_i_2_n_0 ),
        .I2(active_mem2_in_read_state_reg[1]),
        .I3(active_mem2_in_read_state_reg[2]),
        .I4(\EN2[7]_i_3_n_0 ),
        .O(\EN2[3]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000002)) 
    \EN2[3]_i_2 
       (.I0(\adr_read_reorder_data_mem1[10]_i_2_n_0 ),
        .I1(\cnt_inner_valid_reg_n_0_[10] ),
        .I2(\cnt_inner_valid_reg_n_0_[8] ),
        .I3(\cnt_inner_valid_reg_n_0_[9] ),
        .I4(\cnt_inner_valid_reg_n_0_[7] ),
        .I5(\cnt_inner_valid[15]_i_5_n_0 ),
        .O(\EN2[3]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h88F88888)) 
    \EN2[4]_i_1 
       (.I0(\EN2[12]_i_2_n_0 ),
        .I1(\EN2[4]_i_2_n_0 ),
        .I2(active_mem2_in_read_state_reg[2]),
        .I3(active_mem2_in_read_state_reg[1]),
        .I4(\EN2[6]_i_2_n_0 ),
        .O(\EN2[4]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0001000100000001)) 
    \EN2[4]_i_2 
       (.I0(\cnt_inner_valid_reg_n_0_[7] ),
        .I1(\cnt_inner_valid_reg_n_0_[9] ),
        .I2(\cnt_inner_valid_reg_n_0_[8] ),
        .I3(\cnt_inner_valid_reg_n_0_[10] ),
        .I4(s00_axis_aresetn),
        .I5(s00_axis_tuser[11]),
        .O(\EN2[4]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h88F88888)) 
    \EN2[5]_i_1 
       (.I0(\EN2[13]_i_2_n_0 ),
        .I1(\EN2[7]_i_2_n_0 ),
        .I2(active_mem2_in_read_state_reg[2]),
        .I3(active_mem2_in_read_state_reg[1]),
        .I4(\EN2[7]_i_3_n_0 ),
        .O(\EN2[5]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hF8888888)) 
    \EN2[6]_i_1 
       (.I0(\EN2[14]_i_2_n_0 ),
        .I1(\EN2[7]_i_2_n_0 ),
        .I2(active_mem2_in_read_state_reg[1]),
        .I3(active_mem2_in_read_state_reg[2]),
        .I4(\EN2[6]_i_2_n_0 ),
        .O(\EN2[6]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair25" *) 
  LUT3 #(
    .INIT(8'h04)) 
    \EN2[6]_i_2 
       (.I0(active_mem2_in_read_state_reg[0]),
        .I1(\EN2[15]_i_8_n_0 ),
        .I2(active_mem2_in_read_state_reg[3]),
        .O(\EN2[6]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hF8888888)) 
    \EN2[7]_i_1 
       (.I0(\EN2[15]_i_4_n_0 ),
        .I1(\EN2[7]_i_2_n_0 ),
        .I2(active_mem2_in_read_state_reg[1]),
        .I3(active_mem2_in_read_state_reg[2]),
        .I4(\EN2[7]_i_3_n_0 ),
        .O(\EN2[7]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000200000000)) 
    \EN2[7]_i_2 
       (.I0(\adr_read_reorder_data_mem1[10]_i_2_n_0 ),
        .I1(\cnt_inner_valid_reg_n_0_[10] ),
        .I2(\cnt_inner_valid_reg_n_0_[8] ),
        .I3(\cnt_inner_valid_reg_n_0_[9] ),
        .I4(\cnt_inner_valid_reg_n_0_[7] ),
        .I5(\EN2[15]_i_7_n_0 ),
        .O(\EN2[7]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair27" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \EN2[7]_i_3 
       (.I0(active_mem2_in_read_state_reg[0]),
        .I1(\EN2[15]_i_8_n_0 ),
        .I2(active_mem2_in_read_state_reg[3]),
        .O(\EN2[7]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hDDDDDDDD000D0000)) 
    \EN2[8]_i_1 
       (.I0(locked_EN2_reg_n_0),
        .I1(s00_axis_tuser[11]),
        .I2(active_mem2_in_read_state_reg[1]),
        .I3(active_mem2_in_read_state_reg[2]),
        .I4(\EN2[14]_i_3_n_0 ),
        .I5(\EN2[8]_i_2_n_0 ),
        .O(\EN2[8]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h00000044F0000000)) 
    \EN2[8]_i_2 
       (.I0(\cnt_inner_valid[15]_i_3_n_0 ),
        .I1(\EN2[15]_i_5_n_0 ),
        .I2(\EN2[7]_i_2_n_0 ),
        .I3(\cnt_inner_valid_reg_n_0_[5] ),
        .I4(\cnt_inner_valid_reg_n_0_[4] ),
        .I5(\EN2[0]_i_2_n_0 ),
        .O(\EN2[8]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h404040404040FF40)) 
    \EN2[9]_i_1 
       (.I0(\cnt_inner_valid[15]_i_3_n_0 ),
        .I1(\EN2[13]_i_2_n_0 ),
        .I2(\EN2[15]_i_5_n_0 ),
        .I3(\EN2[15]_i_6_n_0 ),
        .I4(active_mem2_in_read_state_reg[1]),
        .I5(active_mem2_in_read_state_reg[2]),
        .O(\EN2[9]_i_1_n_0 ));
  FDRE \EN2_reg[0] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_aresetn),
        .D(\EN2[0]_i_1_n_0 ),
        .Q(\EN2_reg_n_0_[0] ),
        .R(\EN2[15]_i_1_n_0 ));
  FDRE \EN2_reg[10] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_aresetn),
        .D(\EN2[10]_i_1_n_0 ),
        .Q(\EN2_reg_n_0_[10] ),
        .R(\EN2[15]_i_1_n_0 ));
  FDRE \EN2_reg[11] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_aresetn),
        .D(\EN2[11]_i_1_n_0 ),
        .Q(\EN2_reg_n_0_[11] ),
        .R(\EN2[15]_i_1_n_0 ));
  FDRE \EN2_reg[12] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_aresetn),
        .D(\EN2[12]_i_1_n_0 ),
        .Q(\EN2_reg_n_0_[12] ),
        .R(\EN2[15]_i_1_n_0 ));
  FDRE \EN2_reg[13] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_aresetn),
        .D(\EN2[13]_i_1_n_0 ),
        .Q(\EN2_reg_n_0_[13] ),
        .R(\EN2[15]_i_1_n_0 ));
  FDRE \EN2_reg[14] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_aresetn),
        .D(\EN2[14]_i_1_n_0 ),
        .Q(\EN2_reg_n_0_[14] ),
        .R(\EN2[15]_i_1_n_0 ));
  FDRE \EN2_reg[15] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_aresetn),
        .D(\EN2[15]_i_2_n_0 ),
        .Q(\EN2_reg_n_0_[15] ),
        .R(\EN2[15]_i_1_n_0 ));
  FDRE \EN2_reg[1] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_aresetn),
        .D(\EN2[1]_i_1_n_0 ),
        .Q(\EN2_reg_n_0_[1] ),
        .R(\EN2[15]_i_1_n_0 ));
  FDRE \EN2_reg[2] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_aresetn),
        .D(\EN2[2]_i_1_n_0 ),
        .Q(\EN2_reg_n_0_[2] ),
        .R(\EN2[15]_i_1_n_0 ));
  FDRE \EN2_reg[3] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_aresetn),
        .D(\EN2[3]_i_1_n_0 ),
        .Q(\EN2_reg_n_0_[3] ),
        .R(\EN2[15]_i_1_n_0 ));
  FDRE \EN2_reg[4] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_aresetn),
        .D(\EN2[4]_i_1_n_0 ),
        .Q(\EN2_reg_n_0_[4] ),
        .R(\EN2[15]_i_1_n_0 ));
  FDRE \EN2_reg[5] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_aresetn),
        .D(\EN2[5]_i_1_n_0 ),
        .Q(\EN2_reg_n_0_[5] ),
        .R(\EN2[15]_i_1_n_0 ));
  FDRE \EN2_reg[6] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_aresetn),
        .D(\EN2[6]_i_1_n_0 ),
        .Q(\EN2_reg_n_0_[6] ),
        .R(\EN2[15]_i_1_n_0 ));
  FDRE \EN2_reg[7] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_aresetn),
        .D(\EN2[7]_i_1_n_0 ),
        .Q(\EN2_reg_n_0_[7] ),
        .R(\EN2[15]_i_1_n_0 ));
  FDRE \EN2_reg[8] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_aresetn),
        .D(\EN2[8]_i_1_n_0 ),
        .Q(\EN2_reg_n_0_[8] ),
        .R(1'b0));
  FDRE \EN2_reg[9] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_aresetn),
        .D(\EN2[9]_i_1_n_0 ),
        .Q(\EN2_reg_n_0_[9] ),
        .R(\EN2[15]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair35" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \active_mem1_in_read_state[0]_i_1 
       (.I0(s00_axis_tuser[11]),
        .I1(\active_mem1_in_read_state_reg_n_0_[0] ),
        .O(\active_mem1_in_read_state[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair29" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \active_mem1_in_read_state[10]_i_1 
       (.I0(\active_mem1_in_read_state_reg[12]_i_2_n_6 ),
        .I1(s00_axis_tuser[11]),
        .O(\active_mem1_in_read_state[10]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair28" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \active_mem1_in_read_state[11]_i_1 
       (.I0(\active_mem1_in_read_state_reg[12]_i_2_n_5 ),
        .I1(s00_axis_tuser[11]),
        .O(\active_mem1_in_read_state[11]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair31" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \active_mem1_in_read_state[12]_i_1 
       (.I0(\active_mem1_in_read_state_reg[12]_i_2_n_4 ),
        .I1(s00_axis_tuser[11]),
        .O(\active_mem1_in_read_state[12]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair30" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \active_mem1_in_read_state[13]_i_1 
       (.I0(\active_mem1_in_read_state_reg[15]_i_3_n_7 ),
        .I1(s00_axis_tuser[11]),
        .O(\active_mem1_in_read_state[13]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair29" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \active_mem1_in_read_state[14]_i_1 
       (.I0(\active_mem1_in_read_state_reg[15]_i_3_n_6 ),
        .I1(s00_axis_tuser[11]),
        .O(\active_mem1_in_read_state[14]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h00010000FFFF0000)) 
    \active_mem1_in_read_state[15]_i_1 
       (.I0(\adr_read_reorder_data_mem1[5]_i_2_n_0 ),
        .I1(\adr_read_reorder_data_mem1_reg_n_0_[4] ),
        .I2(\adr_read_reorder_data_mem1_reg_n_0_[5] ),
        .I3(\adr_read_reorder_data_mem1[6]_i_3_n_0 ),
        .I4(s00_axis_aresetn),
        .I5(s00_axis_tuser[11]),
        .O(\active_mem1_in_read_state[15]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair28" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \active_mem1_in_read_state[15]_i_2 
       (.I0(\active_mem1_in_read_state_reg[15]_i_3_n_5 ),
        .I1(s00_axis_tuser[11]),
        .O(\active_mem1_in_read_state[15]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair35" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \active_mem1_in_read_state[1]_i_1 
       (.I0(\active_mem1_in_read_state_reg[4]_i_2_n_7 ),
        .I1(s00_axis_tuser[11]),
        .O(\active_mem1_in_read_state[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair34" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \active_mem1_in_read_state[2]_i_1 
       (.I0(\active_mem1_in_read_state_reg[4]_i_2_n_6 ),
        .I1(s00_axis_tuser[11]),
        .O(\active_mem1_in_read_state[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair33" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \active_mem1_in_read_state[3]_i_1 
       (.I0(\active_mem1_in_read_state_reg[4]_i_2_n_5 ),
        .I1(s00_axis_tuser[11]),
        .O(\active_mem1_in_read_state[3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair32" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \active_mem1_in_read_state[4]_i_1 
       (.I0(\active_mem1_in_read_state_reg[4]_i_2_n_4 ),
        .I1(s00_axis_tuser[11]),
        .O(\active_mem1_in_read_state[4]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair34" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \active_mem1_in_read_state[5]_i_1 
       (.I0(\active_mem1_in_read_state_reg[8]_i_2_n_7 ),
        .I1(s00_axis_tuser[11]),
        .O(\active_mem1_in_read_state[5]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair33" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \active_mem1_in_read_state[6]_i_1 
       (.I0(\active_mem1_in_read_state_reg[8]_i_2_n_6 ),
        .I1(s00_axis_tuser[11]),
        .O(\active_mem1_in_read_state[6]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair32" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \active_mem1_in_read_state[7]_i_1 
       (.I0(\active_mem1_in_read_state_reg[8]_i_2_n_5 ),
        .I1(s00_axis_tuser[11]),
        .O(\active_mem1_in_read_state[7]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair31" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \active_mem1_in_read_state[8]_i_1 
       (.I0(\active_mem1_in_read_state_reg[8]_i_2_n_4 ),
        .I1(s00_axis_tuser[11]),
        .O(\active_mem1_in_read_state[8]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair30" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \active_mem1_in_read_state[9]_i_1 
       (.I0(\active_mem1_in_read_state_reg[12]_i_2_n_7 ),
        .I1(s00_axis_tuser[11]),
        .O(\active_mem1_in_read_state[9]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \active_mem1_in_read_state_reg[0] 
       (.C(m00_axis_aclk),
        .CE(\active_mem1_in_read_state[15]_i_1_n_0 ),
        .D(\active_mem1_in_read_state[0]_i_1_n_0 ),
        .Q(\active_mem1_in_read_state_reg_n_0_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \active_mem1_in_read_state_reg[10] 
       (.C(m00_axis_aclk),
        .CE(\active_mem1_in_read_state[15]_i_1_n_0 ),
        .D(\active_mem1_in_read_state[10]_i_1_n_0 ),
        .Q(\active_mem1_in_read_state_reg_n_0_[10] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \active_mem1_in_read_state_reg[11] 
       (.C(m00_axis_aclk),
        .CE(\active_mem1_in_read_state[15]_i_1_n_0 ),
        .D(\active_mem1_in_read_state[11]_i_1_n_0 ),
        .Q(\active_mem1_in_read_state_reg_n_0_[11] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \active_mem1_in_read_state_reg[12] 
       (.C(m00_axis_aclk),
        .CE(\active_mem1_in_read_state[15]_i_1_n_0 ),
        .D(\active_mem1_in_read_state[12]_i_1_n_0 ),
        .Q(\active_mem1_in_read_state_reg_n_0_[12] ),
        .R(1'b0));
  CARRY4 \active_mem1_in_read_state_reg[12]_i_2 
       (.CI(\active_mem1_in_read_state_reg[8]_i_2_n_0 ),
        .CO({\active_mem1_in_read_state_reg[12]_i_2_n_0 ,\active_mem1_in_read_state_reg[12]_i_2_n_1 ,\active_mem1_in_read_state_reg[12]_i_2_n_2 ,\active_mem1_in_read_state_reg[12]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\active_mem1_in_read_state_reg[12]_i_2_n_4 ,\active_mem1_in_read_state_reg[12]_i_2_n_5 ,\active_mem1_in_read_state_reg[12]_i_2_n_6 ,\active_mem1_in_read_state_reg[12]_i_2_n_7 }),
        .S({\active_mem1_in_read_state_reg_n_0_[12] ,\active_mem1_in_read_state_reg_n_0_[11] ,\active_mem1_in_read_state_reg_n_0_[10] ,\active_mem1_in_read_state_reg_n_0_[9] }));
  FDRE #(
    .INIT(1'b0)) 
    \active_mem1_in_read_state_reg[13] 
       (.C(m00_axis_aclk),
        .CE(\active_mem1_in_read_state[15]_i_1_n_0 ),
        .D(\active_mem1_in_read_state[13]_i_1_n_0 ),
        .Q(\active_mem1_in_read_state_reg_n_0_[13] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \active_mem1_in_read_state_reg[14] 
       (.C(m00_axis_aclk),
        .CE(\active_mem1_in_read_state[15]_i_1_n_0 ),
        .D(\active_mem1_in_read_state[14]_i_1_n_0 ),
        .Q(\active_mem1_in_read_state_reg_n_0_[14] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \active_mem1_in_read_state_reg[15] 
       (.C(m00_axis_aclk),
        .CE(\active_mem1_in_read_state[15]_i_1_n_0 ),
        .D(\active_mem1_in_read_state[15]_i_2_n_0 ),
        .Q(\active_mem1_in_read_state_reg_n_0_[15] ),
        .R(1'b0));
  CARRY4 \active_mem1_in_read_state_reg[15]_i_3 
       (.CI(\active_mem1_in_read_state_reg[12]_i_2_n_0 ),
        .CO({\NLW_active_mem1_in_read_state_reg[15]_i_3_CO_UNCONNECTED [3:2],\active_mem1_in_read_state_reg[15]_i_3_n_2 ,\active_mem1_in_read_state_reg[15]_i_3_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\NLW_active_mem1_in_read_state_reg[15]_i_3_O_UNCONNECTED [3],\active_mem1_in_read_state_reg[15]_i_3_n_5 ,\active_mem1_in_read_state_reg[15]_i_3_n_6 ,\active_mem1_in_read_state_reg[15]_i_3_n_7 }),
        .S({1'b0,\active_mem1_in_read_state_reg_n_0_[15] ,\active_mem1_in_read_state_reg_n_0_[14] ,\active_mem1_in_read_state_reg_n_0_[13] }));
  FDRE #(
    .INIT(1'b0)) 
    \active_mem1_in_read_state_reg[1] 
       (.C(m00_axis_aclk),
        .CE(\active_mem1_in_read_state[15]_i_1_n_0 ),
        .D(\active_mem1_in_read_state[1]_i_1_n_0 ),
        .Q(\active_mem1_in_read_state_reg_n_0_[1] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \active_mem1_in_read_state_reg[2] 
       (.C(m00_axis_aclk),
        .CE(\active_mem1_in_read_state[15]_i_1_n_0 ),
        .D(\active_mem1_in_read_state[2]_i_1_n_0 ),
        .Q(\active_mem1_in_read_state_reg_n_0_[2] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \active_mem1_in_read_state_reg[3] 
       (.C(m00_axis_aclk),
        .CE(\active_mem1_in_read_state[15]_i_1_n_0 ),
        .D(\active_mem1_in_read_state[3]_i_1_n_0 ),
        .Q(\active_mem1_in_read_state_reg_n_0_[3] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \active_mem1_in_read_state_reg[4] 
       (.C(m00_axis_aclk),
        .CE(\active_mem1_in_read_state[15]_i_1_n_0 ),
        .D(\active_mem1_in_read_state[4]_i_1_n_0 ),
        .Q(\active_mem1_in_read_state_reg_n_0_[4] ),
        .R(1'b0));
  CARRY4 \active_mem1_in_read_state_reg[4]_i_2 
       (.CI(1'b0),
        .CO({\active_mem1_in_read_state_reg[4]_i_2_n_0 ,\active_mem1_in_read_state_reg[4]_i_2_n_1 ,\active_mem1_in_read_state_reg[4]_i_2_n_2 ,\active_mem1_in_read_state_reg[4]_i_2_n_3 }),
        .CYINIT(\active_mem1_in_read_state_reg_n_0_[0] ),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\active_mem1_in_read_state_reg[4]_i_2_n_4 ,\active_mem1_in_read_state_reg[4]_i_2_n_5 ,\active_mem1_in_read_state_reg[4]_i_2_n_6 ,\active_mem1_in_read_state_reg[4]_i_2_n_7 }),
        .S({\active_mem1_in_read_state_reg_n_0_[4] ,\active_mem1_in_read_state_reg_n_0_[3] ,\active_mem1_in_read_state_reg_n_0_[2] ,\active_mem1_in_read_state_reg_n_0_[1] }));
  FDRE #(
    .INIT(1'b0)) 
    \active_mem1_in_read_state_reg[5] 
       (.C(m00_axis_aclk),
        .CE(\active_mem1_in_read_state[15]_i_1_n_0 ),
        .D(\active_mem1_in_read_state[5]_i_1_n_0 ),
        .Q(\active_mem1_in_read_state_reg_n_0_[5] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \active_mem1_in_read_state_reg[6] 
       (.C(m00_axis_aclk),
        .CE(\active_mem1_in_read_state[15]_i_1_n_0 ),
        .D(\active_mem1_in_read_state[6]_i_1_n_0 ),
        .Q(\active_mem1_in_read_state_reg_n_0_[6] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \active_mem1_in_read_state_reg[7] 
       (.C(m00_axis_aclk),
        .CE(\active_mem1_in_read_state[15]_i_1_n_0 ),
        .D(\active_mem1_in_read_state[7]_i_1_n_0 ),
        .Q(\active_mem1_in_read_state_reg_n_0_[7] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \active_mem1_in_read_state_reg[8] 
       (.C(m00_axis_aclk),
        .CE(\active_mem1_in_read_state[15]_i_1_n_0 ),
        .D(\active_mem1_in_read_state[8]_i_1_n_0 ),
        .Q(\active_mem1_in_read_state_reg_n_0_[8] ),
        .R(1'b0));
  CARRY4 \active_mem1_in_read_state_reg[8]_i_2 
       (.CI(\active_mem1_in_read_state_reg[4]_i_2_n_0 ),
        .CO({\active_mem1_in_read_state_reg[8]_i_2_n_0 ,\active_mem1_in_read_state_reg[8]_i_2_n_1 ,\active_mem1_in_read_state_reg[8]_i_2_n_2 ,\active_mem1_in_read_state_reg[8]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\active_mem1_in_read_state_reg[8]_i_2_n_4 ,\active_mem1_in_read_state_reg[8]_i_2_n_5 ,\active_mem1_in_read_state_reg[8]_i_2_n_6 ,\active_mem1_in_read_state_reg[8]_i_2_n_7 }),
        .S({\active_mem1_in_read_state_reg_n_0_[8] ,\active_mem1_in_read_state_reg_n_0_[7] ,\active_mem1_in_read_state_reg_n_0_[6] ,\active_mem1_in_read_state_reg_n_0_[5] }));
  FDRE #(
    .INIT(1'b0)) 
    \active_mem1_in_read_state_reg[9] 
       (.C(m00_axis_aclk),
        .CE(\active_mem1_in_read_state[15]_i_1_n_0 ),
        .D(\active_mem1_in_read_state[9]_i_1_n_0 ),
        .Q(\active_mem1_in_read_state_reg_n_0_[9] ),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h0000000100000000)) 
    \active_mem2_in_read_state[0]_i_1 
       (.I0(\adr_read_reorder_data_mem2_reg_n_0_[4] ),
        .I1(\adr_read_reorder_data_mem2_reg_n_0_[5] ),
        .I2(\adr_read_reorder_data_mem2_reg_n_0_[3] ),
        .I3(\adr_read_reorder_data_mem2_reg_n_0_[2] ),
        .I4(\adr_read_reorder_data_mem1[10]_i_2_n_0 ),
        .I5(\adr_read_reorder_data_mem2[5]_i_3_n_0 ),
        .O(\active_mem2_in_read_state[0]_i_1_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \active_mem2_in_read_state[0]_i_3 
       (.I0(active_mem2_in_read_state_reg[0]),
        .O(\active_mem2_in_read_state[0]_i_3_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \active_mem2_in_read_state_reg[0] 
       (.C(m00_axis_aclk),
        .CE(\active_mem2_in_read_state[0]_i_1_n_0 ),
        .D(\active_mem2_in_read_state_reg[0]_i_2_n_7 ),
        .Q(active_mem2_in_read_state_reg[0]),
        .R(\adr_read_reorder_data_mem2[10]_i_1_n_0 ));
  CARRY4 \active_mem2_in_read_state_reg[0]_i_2 
       (.CI(1'b0),
        .CO({\active_mem2_in_read_state_reg[0]_i_2_n_0 ,\active_mem2_in_read_state_reg[0]_i_2_n_1 ,\active_mem2_in_read_state_reg[0]_i_2_n_2 ,\active_mem2_in_read_state_reg[0]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\active_mem2_in_read_state_reg[0]_i_2_n_4 ,\active_mem2_in_read_state_reg[0]_i_2_n_5 ,\active_mem2_in_read_state_reg[0]_i_2_n_6 ,\active_mem2_in_read_state_reg[0]_i_2_n_7 }),
        .S({active_mem2_in_read_state_reg[3:1],\active_mem2_in_read_state[0]_i_3_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \active_mem2_in_read_state_reg[10] 
       (.C(m00_axis_aclk),
        .CE(\active_mem2_in_read_state[0]_i_1_n_0 ),
        .D(\active_mem2_in_read_state_reg[8]_i_1_n_5 ),
        .Q(active_mem2_in_read_state_reg[10]),
        .R(\adr_read_reorder_data_mem2[10]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \active_mem2_in_read_state_reg[11] 
       (.C(m00_axis_aclk),
        .CE(\active_mem2_in_read_state[0]_i_1_n_0 ),
        .D(\active_mem2_in_read_state_reg[8]_i_1_n_4 ),
        .Q(active_mem2_in_read_state_reg[11]),
        .R(\adr_read_reorder_data_mem2[10]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \active_mem2_in_read_state_reg[12] 
       (.C(m00_axis_aclk),
        .CE(\active_mem2_in_read_state[0]_i_1_n_0 ),
        .D(\active_mem2_in_read_state_reg[12]_i_1_n_7 ),
        .Q(active_mem2_in_read_state_reg[12]),
        .R(\adr_read_reorder_data_mem2[10]_i_1_n_0 ));
  CARRY4 \active_mem2_in_read_state_reg[12]_i_1 
       (.CI(\active_mem2_in_read_state_reg[8]_i_1_n_0 ),
        .CO({\NLW_active_mem2_in_read_state_reg[12]_i_1_CO_UNCONNECTED [3],\active_mem2_in_read_state_reg[12]_i_1_n_1 ,\active_mem2_in_read_state_reg[12]_i_1_n_2 ,\active_mem2_in_read_state_reg[12]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\active_mem2_in_read_state_reg[12]_i_1_n_4 ,\active_mem2_in_read_state_reg[12]_i_1_n_5 ,\active_mem2_in_read_state_reg[12]_i_1_n_6 ,\active_mem2_in_read_state_reg[12]_i_1_n_7 }),
        .S(active_mem2_in_read_state_reg[15:12]));
  FDRE #(
    .INIT(1'b0)) 
    \active_mem2_in_read_state_reg[13] 
       (.C(m00_axis_aclk),
        .CE(\active_mem2_in_read_state[0]_i_1_n_0 ),
        .D(\active_mem2_in_read_state_reg[12]_i_1_n_6 ),
        .Q(active_mem2_in_read_state_reg[13]),
        .R(\adr_read_reorder_data_mem2[10]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \active_mem2_in_read_state_reg[14] 
       (.C(m00_axis_aclk),
        .CE(\active_mem2_in_read_state[0]_i_1_n_0 ),
        .D(\active_mem2_in_read_state_reg[12]_i_1_n_5 ),
        .Q(active_mem2_in_read_state_reg[14]),
        .R(\adr_read_reorder_data_mem2[10]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \active_mem2_in_read_state_reg[15] 
       (.C(m00_axis_aclk),
        .CE(\active_mem2_in_read_state[0]_i_1_n_0 ),
        .D(\active_mem2_in_read_state_reg[12]_i_1_n_4 ),
        .Q(active_mem2_in_read_state_reg[15]),
        .R(\adr_read_reorder_data_mem2[10]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \active_mem2_in_read_state_reg[1] 
       (.C(m00_axis_aclk),
        .CE(\active_mem2_in_read_state[0]_i_1_n_0 ),
        .D(\active_mem2_in_read_state_reg[0]_i_2_n_6 ),
        .Q(active_mem2_in_read_state_reg[1]),
        .R(\adr_read_reorder_data_mem2[10]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \active_mem2_in_read_state_reg[2] 
       (.C(m00_axis_aclk),
        .CE(\active_mem2_in_read_state[0]_i_1_n_0 ),
        .D(\active_mem2_in_read_state_reg[0]_i_2_n_5 ),
        .Q(active_mem2_in_read_state_reg[2]),
        .R(\adr_read_reorder_data_mem2[10]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \active_mem2_in_read_state_reg[3] 
       (.C(m00_axis_aclk),
        .CE(\active_mem2_in_read_state[0]_i_1_n_0 ),
        .D(\active_mem2_in_read_state_reg[0]_i_2_n_4 ),
        .Q(active_mem2_in_read_state_reg[3]),
        .R(\adr_read_reorder_data_mem2[10]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \active_mem2_in_read_state_reg[4] 
       (.C(m00_axis_aclk),
        .CE(\active_mem2_in_read_state[0]_i_1_n_0 ),
        .D(\active_mem2_in_read_state_reg[4]_i_1_n_7 ),
        .Q(active_mem2_in_read_state_reg[4]),
        .R(\adr_read_reorder_data_mem2[10]_i_1_n_0 ));
  CARRY4 \active_mem2_in_read_state_reg[4]_i_1 
       (.CI(\active_mem2_in_read_state_reg[0]_i_2_n_0 ),
        .CO({\active_mem2_in_read_state_reg[4]_i_1_n_0 ,\active_mem2_in_read_state_reg[4]_i_1_n_1 ,\active_mem2_in_read_state_reg[4]_i_1_n_2 ,\active_mem2_in_read_state_reg[4]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\active_mem2_in_read_state_reg[4]_i_1_n_4 ,\active_mem2_in_read_state_reg[4]_i_1_n_5 ,\active_mem2_in_read_state_reg[4]_i_1_n_6 ,\active_mem2_in_read_state_reg[4]_i_1_n_7 }),
        .S(active_mem2_in_read_state_reg[7:4]));
  FDRE #(
    .INIT(1'b0)) 
    \active_mem2_in_read_state_reg[5] 
       (.C(m00_axis_aclk),
        .CE(\active_mem2_in_read_state[0]_i_1_n_0 ),
        .D(\active_mem2_in_read_state_reg[4]_i_1_n_6 ),
        .Q(active_mem2_in_read_state_reg[5]),
        .R(\adr_read_reorder_data_mem2[10]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \active_mem2_in_read_state_reg[6] 
       (.C(m00_axis_aclk),
        .CE(\active_mem2_in_read_state[0]_i_1_n_0 ),
        .D(\active_mem2_in_read_state_reg[4]_i_1_n_5 ),
        .Q(active_mem2_in_read_state_reg[6]),
        .R(\adr_read_reorder_data_mem2[10]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \active_mem2_in_read_state_reg[7] 
       (.C(m00_axis_aclk),
        .CE(\active_mem2_in_read_state[0]_i_1_n_0 ),
        .D(\active_mem2_in_read_state_reg[4]_i_1_n_4 ),
        .Q(active_mem2_in_read_state_reg[7]),
        .R(\adr_read_reorder_data_mem2[10]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \active_mem2_in_read_state_reg[8] 
       (.C(m00_axis_aclk),
        .CE(\active_mem2_in_read_state[0]_i_1_n_0 ),
        .D(\active_mem2_in_read_state_reg[8]_i_1_n_7 ),
        .Q(active_mem2_in_read_state_reg[8]),
        .R(\adr_read_reorder_data_mem2[10]_i_1_n_0 ));
  CARRY4 \active_mem2_in_read_state_reg[8]_i_1 
       (.CI(\active_mem2_in_read_state_reg[4]_i_1_n_0 ),
        .CO({\active_mem2_in_read_state_reg[8]_i_1_n_0 ,\active_mem2_in_read_state_reg[8]_i_1_n_1 ,\active_mem2_in_read_state_reg[8]_i_1_n_2 ,\active_mem2_in_read_state_reg[8]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\active_mem2_in_read_state_reg[8]_i_1_n_4 ,\active_mem2_in_read_state_reg[8]_i_1_n_5 ,\active_mem2_in_read_state_reg[8]_i_1_n_6 ,\active_mem2_in_read_state_reg[8]_i_1_n_7 }),
        .S(active_mem2_in_read_state_reg[11:8]));
  FDRE #(
    .INIT(1'b0)) 
    \active_mem2_in_read_state_reg[9] 
       (.C(m00_axis_aclk),
        .CE(\active_mem2_in_read_state[0]_i_1_n_0 ),
        .D(\active_mem2_in_read_state_reg[8]_i_1_n_6 ),
        .Q(active_mem2_in_read_state_reg[9]),
        .R(\adr_read_reorder_data_mem2[10]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h7F)) 
    \adr_read_reorder_data_mem1[0]_i_1 
       (.I0(s00_axis_tuser[11]),
        .I1(s00_axis_aresetn),
        .I2(\adr_read_reorder_data_mem1_reg_n_0_[0] ),
        .O(\adr_read_reorder_data_mem1[0]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \adr_read_reorder_data_mem1[10]_i_1 
       (.I0(s00_axis_aresetn),
        .I1(s00_axis_tuser[11]),
        .O(\adr_read_reorder_data_mem1[10]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'hB)) 
    \adr_read_reorder_data_mem1[10]_i_2 
       (.I0(s00_axis_tuser[11]),
        .I1(s00_axis_aresetn),
        .O(\adr_read_reorder_data_mem1[10]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFF9AAAAAAA)) 
    \adr_read_reorder_data_mem1[10]_i_3 
       (.I0(\adr_read_reorder_data_mem1_reg_n_0_[10] ),
        .I1(\adr_read_reorder_data_mem1[10]_i_4_n_0 ),
        .I2(\adr_read_reorder_data_mem1_reg_n_0_[9] ),
        .I3(\adr_read_reorder_data_mem1_reg_n_0_[7] ),
        .I4(\adr_read_reorder_data_mem1_reg_n_0_[8] ),
        .I5(\adr_read_reorder_data_mem2[10]_i_2_n_0 ),
        .O(\adr_read_reorder_data_mem1[10]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'hB)) 
    \adr_read_reorder_data_mem1[10]_i_4 
       (.I0(\adr_read_reorder_data_mem1[6]_i_4_n_0 ),
        .I1(\adr_read_reorder_data_mem1_reg_n_0_[6] ),
        .O(\adr_read_reorder_data_mem1[10]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hBBBBEEEEBBBBEEEF)) 
    \adr_read_reorder_data_mem1[1]_i_1 
       (.I0(\adr_read_reorder_data_mem2[10]_i_2_n_0 ),
        .I1(\adr_read_reorder_data_mem1_reg_n_0_[0] ),
        .I2(\adr_read_reorder_data_mem1[3]_i_2_n_0 ),
        .I3(\adr_read_reorder_data_mem1_reg_n_0_[3] ),
        .I4(\adr_read_reorder_data_mem1_reg_n_0_[1] ),
        .I5(\adr_read_reorder_data_mem1_reg_n_0_[2] ),
        .O(\adr_read_reorder_data_mem1[1]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hBBBBEEEEEEEEEEEF)) 
    \adr_read_reorder_data_mem1[2]_i_1 
       (.I0(\adr_read_reorder_data_mem2[10]_i_2_n_0 ),
        .I1(\adr_read_reorder_data_mem1_reg_n_0_[2] ),
        .I2(\adr_read_reorder_data_mem1[3]_i_2_n_0 ),
        .I3(\adr_read_reorder_data_mem1_reg_n_0_[3] ),
        .I4(\adr_read_reorder_data_mem1_reg_n_0_[1] ),
        .I5(\adr_read_reorder_data_mem1_reg_n_0_[0] ),
        .O(\adr_read_reorder_data_mem1[2]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hBBEEEEEEEEEEEEEF)) 
    \adr_read_reorder_data_mem1[3]_i_1 
       (.I0(\adr_read_reorder_data_mem2[10]_i_2_n_0 ),
        .I1(\adr_read_reorder_data_mem1_reg_n_0_[3] ),
        .I2(\adr_read_reorder_data_mem1[3]_i_2_n_0 ),
        .I3(\adr_read_reorder_data_mem1_reg_n_0_[2] ),
        .I4(\adr_read_reorder_data_mem1_reg_n_0_[0] ),
        .I5(\adr_read_reorder_data_mem1_reg_n_0_[1] ),
        .O(\adr_read_reorder_data_mem1[3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair26" *) 
  LUT3 #(
    .INIT(8'hFE)) 
    \adr_read_reorder_data_mem1[3]_i_2 
       (.I0(\adr_read_reorder_data_mem1_reg_n_0_[5] ),
        .I1(\adr_read_reorder_data_mem1_reg_n_0_[4] ),
        .I2(\adr_read_reorder_data_mem1[5]_i_2_n_0 ),
        .O(\adr_read_reorder_data_mem1[3]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hEBEBEBEBEBEBEBFF)) 
    \adr_read_reorder_data_mem1[4]_i_1 
       (.I0(\adr_read_reorder_data_mem2[10]_i_2_n_0 ),
        .I1(\adr_read_reorder_data_mem1[4]_i_2_n_0 ),
        .I2(\adr_read_reorder_data_mem1_reg_n_0_[4] ),
        .I3(\adr_read_reorder_data_mem1[5]_i_2_n_0 ),
        .I4(\adr_read_reorder_data_mem1[6]_i_3_n_0 ),
        .I5(\adr_read_reorder_data_mem1_reg_n_0_[5] ),
        .O(\adr_read_reorder_data_mem1[4]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h7FFF)) 
    \adr_read_reorder_data_mem1[4]_i_2 
       (.I0(\adr_read_reorder_data_mem1_reg_n_0_[2] ),
        .I1(\adr_read_reorder_data_mem1_reg_n_0_[0] ),
        .I2(\adr_read_reorder_data_mem1_reg_n_0_[1] ),
        .I3(\adr_read_reorder_data_mem1_reg_n_0_[3] ),
        .O(\adr_read_reorder_data_mem1[4]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFAAABAAABFFFF)) 
    \adr_read_reorder_data_mem1[5]_i_1 
       (.I0(\adr_read_reorder_data_mem2[10]_i_2_n_0 ),
        .I1(\adr_read_reorder_data_mem1_reg_n_0_[4] ),
        .I2(\adr_read_reorder_data_mem1[6]_i_3_n_0 ),
        .I3(\adr_read_reorder_data_mem1[5]_i_2_n_0 ),
        .I4(\adr_read_reorder_data_mem1_reg_n_0_[5] ),
        .I5(\adr_read_reorder_data_mem1[5]_i_3_n_0 ),
        .O(\adr_read_reorder_data_mem1[5]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT5 #(
    .INIT(32'hFFFF7FFF)) 
    \adr_read_reorder_data_mem1[5]_i_2 
       (.I0(\adr_read_reorder_data_mem1_reg_n_0_[10] ),
        .I1(\adr_read_reorder_data_mem1_reg_n_0_[8] ),
        .I2(\adr_read_reorder_data_mem1_reg_n_0_[7] ),
        .I3(\adr_read_reorder_data_mem1_reg_n_0_[9] ),
        .I4(\adr_read_reorder_data_mem1_reg_n_0_[6] ),
        .O(\adr_read_reorder_data_mem1[5]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT5 #(
    .INIT(32'h7FFFFFFF)) 
    \adr_read_reorder_data_mem1[5]_i_3 
       (.I0(\adr_read_reorder_data_mem1_reg_n_0_[3] ),
        .I1(\adr_read_reorder_data_mem1_reg_n_0_[1] ),
        .I2(\adr_read_reorder_data_mem1_reg_n_0_[0] ),
        .I3(\adr_read_reorder_data_mem1_reg_n_0_[2] ),
        .I4(\adr_read_reorder_data_mem1_reg_n_0_[4] ),
        .O(\adr_read_reorder_data_mem1[5]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFAAABAAABFFFF)) 
    \adr_read_reorder_data_mem1[6]_i_1 
       (.I0(\adr_read_reorder_data_mem2[10]_i_2_n_0 ),
        .I1(\adr_read_reorder_data_mem1[6]_i_2_n_0 ),
        .I2(\adr_read_reorder_data_mem1[6]_i_3_n_0 ),
        .I3(m00_axis_tvalid_r_i_2_n_0),
        .I4(\adr_read_reorder_data_mem1_reg_n_0_[6] ),
        .I5(\adr_read_reorder_data_mem1[6]_i_4_n_0 ),
        .O(\adr_read_reorder_data_mem1[6]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair26" *) 
  LUT2 #(
    .INIT(4'hE)) 
    \adr_read_reorder_data_mem1[6]_i_2 
       (.I0(\adr_read_reorder_data_mem1_reg_n_0_[4] ),
        .I1(\adr_read_reorder_data_mem1_reg_n_0_[5] ),
        .O(\adr_read_reorder_data_mem1[6]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT4 #(
    .INIT(16'hFFFE)) 
    \adr_read_reorder_data_mem1[6]_i_3 
       (.I0(\adr_read_reorder_data_mem1_reg_n_0_[2] ),
        .I1(\adr_read_reorder_data_mem1_reg_n_0_[0] ),
        .I2(\adr_read_reorder_data_mem1_reg_n_0_[3] ),
        .I3(\adr_read_reorder_data_mem1_reg_n_0_[1] ),
        .O(\adr_read_reorder_data_mem1[6]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h7FFFFFFFFFFFFFFF)) 
    \adr_read_reorder_data_mem1[6]_i_4 
       (.I0(\adr_read_reorder_data_mem1_reg_n_0_[4] ),
        .I1(\adr_read_reorder_data_mem1_reg_n_0_[2] ),
        .I2(\adr_read_reorder_data_mem1_reg_n_0_[0] ),
        .I3(\adr_read_reorder_data_mem1_reg_n_0_[1] ),
        .I4(\adr_read_reorder_data_mem1_reg_n_0_[3] ),
        .I5(\adr_read_reorder_data_mem1_reg_n_0_[5] ),
        .O(\adr_read_reorder_data_mem1[6]_i_4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT4 #(
    .INIT(16'h9FFF)) 
    \adr_read_reorder_data_mem1[7]_i_1 
       (.I0(\adr_read_reorder_data_mem1_reg_n_0_[7] ),
        .I1(\adr_read_reorder_data_mem1[10]_i_4_n_0 ),
        .I2(s00_axis_tuser[11]),
        .I3(s00_axis_aresetn),
        .O(\adr_read_reorder_data_mem1[7]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT5 #(
    .INIT(32'h9AFFFFFF)) 
    \adr_read_reorder_data_mem1[8]_i_1 
       (.I0(\adr_read_reorder_data_mem1_reg_n_0_[8] ),
        .I1(\adr_read_reorder_data_mem1[10]_i_4_n_0 ),
        .I2(\adr_read_reorder_data_mem1_reg_n_0_[7] ),
        .I3(s00_axis_tuser[11]),
        .I4(s00_axis_aresetn),
        .O(\adr_read_reorder_data_mem1[8]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h9AAAFFFFFFFFFFFF)) 
    \adr_read_reorder_data_mem1[9]_i_1 
       (.I0(\adr_read_reorder_data_mem1_reg_n_0_[9] ),
        .I1(\adr_read_reorder_data_mem1[10]_i_4_n_0 ),
        .I2(\adr_read_reorder_data_mem1_reg_n_0_[8] ),
        .I3(\adr_read_reorder_data_mem1_reg_n_0_[7] ),
        .I4(s00_axis_tuser[11]),
        .I5(s00_axis_aresetn),
        .O(\adr_read_reorder_data_mem1[9]_i_1_n_0 ));
  FDSE \adr_read_reorder_data_mem1_reg[0] 
       (.C(m00_axis_aclk),
        .CE(\adr_read_reorder_data_mem1[10]_i_2_n_0 ),
        .D(\adr_read_reorder_data_mem1[0]_i_1_n_0 ),
        .Q(\adr_read_reorder_data_mem1_reg_n_0_[0] ),
        .S(\adr_read_reorder_data_mem1[10]_i_1_n_0 ));
  FDSE \adr_read_reorder_data_mem1_reg[10] 
       (.C(m00_axis_aclk),
        .CE(\adr_read_reorder_data_mem1[10]_i_2_n_0 ),
        .D(\adr_read_reorder_data_mem1[10]_i_3_n_0 ),
        .Q(\adr_read_reorder_data_mem1_reg_n_0_[10] ),
        .S(\adr_read_reorder_data_mem1[10]_i_1_n_0 ));
  FDSE \adr_read_reorder_data_mem1_reg[1] 
       (.C(m00_axis_aclk),
        .CE(\adr_read_reorder_data_mem1[10]_i_2_n_0 ),
        .D(\adr_read_reorder_data_mem1[1]_i_1_n_0 ),
        .Q(\adr_read_reorder_data_mem1_reg_n_0_[1] ),
        .S(\adr_read_reorder_data_mem1[10]_i_1_n_0 ));
  FDSE \adr_read_reorder_data_mem1_reg[2] 
       (.C(m00_axis_aclk),
        .CE(\adr_read_reorder_data_mem1[10]_i_2_n_0 ),
        .D(\adr_read_reorder_data_mem1[2]_i_1_n_0 ),
        .Q(\adr_read_reorder_data_mem1_reg_n_0_[2] ),
        .S(\adr_read_reorder_data_mem1[10]_i_1_n_0 ));
  FDSE \adr_read_reorder_data_mem1_reg[3] 
       (.C(m00_axis_aclk),
        .CE(\adr_read_reorder_data_mem1[10]_i_2_n_0 ),
        .D(\adr_read_reorder_data_mem1[3]_i_1_n_0 ),
        .Q(\adr_read_reorder_data_mem1_reg_n_0_[3] ),
        .S(\adr_read_reorder_data_mem1[10]_i_1_n_0 ));
  FDSE \adr_read_reorder_data_mem1_reg[4] 
       (.C(m00_axis_aclk),
        .CE(\adr_read_reorder_data_mem1[10]_i_2_n_0 ),
        .D(\adr_read_reorder_data_mem1[4]_i_1_n_0 ),
        .Q(\adr_read_reorder_data_mem1_reg_n_0_[4] ),
        .S(\adr_read_reorder_data_mem1[10]_i_1_n_0 ));
  FDSE \adr_read_reorder_data_mem1_reg[5] 
       (.C(m00_axis_aclk),
        .CE(\adr_read_reorder_data_mem1[10]_i_2_n_0 ),
        .D(\adr_read_reorder_data_mem1[5]_i_1_n_0 ),
        .Q(\adr_read_reorder_data_mem1_reg_n_0_[5] ),
        .S(\adr_read_reorder_data_mem1[10]_i_1_n_0 ));
  FDSE \adr_read_reorder_data_mem1_reg[6] 
       (.C(m00_axis_aclk),
        .CE(\adr_read_reorder_data_mem1[10]_i_2_n_0 ),
        .D(\adr_read_reorder_data_mem1[6]_i_1_n_0 ),
        .Q(\adr_read_reorder_data_mem1_reg_n_0_[6] ),
        .S(\adr_read_reorder_data_mem1[10]_i_1_n_0 ));
  FDSE \adr_read_reorder_data_mem1_reg[7] 
       (.C(m00_axis_aclk),
        .CE(\adr_read_reorder_data_mem1[10]_i_2_n_0 ),
        .D(\adr_read_reorder_data_mem1[7]_i_1_n_0 ),
        .Q(\adr_read_reorder_data_mem1_reg_n_0_[7] ),
        .S(\adr_read_reorder_data_mem1[10]_i_1_n_0 ));
  FDSE \adr_read_reorder_data_mem1_reg[8] 
       (.C(m00_axis_aclk),
        .CE(\adr_read_reorder_data_mem1[10]_i_2_n_0 ),
        .D(\adr_read_reorder_data_mem1[8]_i_1_n_0 ),
        .Q(\adr_read_reorder_data_mem1_reg_n_0_[8] ),
        .S(\adr_read_reorder_data_mem1[10]_i_1_n_0 ));
  FDSE \adr_read_reorder_data_mem1_reg[9] 
       (.C(m00_axis_aclk),
        .CE(\adr_read_reorder_data_mem1[10]_i_2_n_0 ),
        .D(\adr_read_reorder_data_mem1[9]_i_1_n_0 ),
        .Q(\adr_read_reorder_data_mem1_reg_n_0_[9] ),
        .S(\adr_read_reorder_data_mem1[10]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'hDF)) 
    \adr_read_reorder_data_mem2[0]_i_1 
       (.I0(s00_axis_aresetn),
        .I1(s00_axis_tuser[11]),
        .I2(\adr_read_reorder_data_mem2_reg_n_0_[0] ),
        .O(p_1_in[0]));
  LUT2 #(
    .INIT(4'h8)) 
    \adr_read_reorder_data_mem2[10]_i_1 
       (.I0(s00_axis_tuser[11]),
        .I1(s00_axis_aresetn),
        .O(\adr_read_reorder_data_mem2[10]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h7)) 
    \adr_read_reorder_data_mem2[10]_i_2 
       (.I0(s00_axis_aresetn),
        .I1(s00_axis_tuser[11]),
        .O(\adr_read_reorder_data_mem2[10]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFF9AAAAAAA)) 
    \adr_read_reorder_data_mem2[10]_i_3 
       (.I0(\adr_read_reorder_data_mem2_reg_n_0_[10] ),
        .I1(\adr_read_reorder_data_mem2[10]_i_4_n_0 ),
        .I2(\adr_read_reorder_data_mem2_reg_n_0_[9] ),
        .I3(\adr_read_reorder_data_mem2_reg_n_0_[7] ),
        .I4(\adr_read_reorder_data_mem2_reg_n_0_[8] ),
        .I5(\adr_read_reorder_data_mem1[10]_i_2_n_0 ),
        .O(p_1_in[10]));
  (* SOFT_HLUTNM = "soft_lutpair37" *) 
  LUT2 #(
    .INIT(4'hB)) 
    \adr_read_reorder_data_mem2[10]_i_4 
       (.I0(\adr_read_reorder_data_mem2[6]_i_5_n_0 ),
        .I1(\adr_read_reorder_data_mem2_reg_n_0_[6] ),
        .O(\adr_read_reorder_data_mem2[10]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFF8FF8FFFF)) 
    \adr_read_reorder_data_mem2[1]_i_1 
       (.I0(\adr_read_reorder_data_mem2[1]_i_2_n_0 ),
        .I1(\adr_read_reorder_data_mem2[1]_i_3_n_0 ),
        .I2(\adr_read_reorder_data_mem2_reg_n_0_[0] ),
        .I3(\adr_read_reorder_data_mem2_reg_n_0_[1] ),
        .I4(s00_axis_aresetn),
        .I5(s00_axis_tuser[11]),
        .O(p_1_in[1]));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT5 #(
    .INIT(32'h00000001)) 
    \adr_read_reorder_data_mem2[1]_i_2 
       (.I0(\adr_read_reorder_data_mem2_reg_n_0_[6] ),
        .I1(\adr_read_reorder_data_mem2_reg_n_0_[2] ),
        .I2(\adr_read_reorder_data_mem2_reg_n_0_[3] ),
        .I3(\adr_read_reorder_data_mem2_reg_n_0_[5] ),
        .I4(\adr_read_reorder_data_mem2_reg_n_0_[4] ),
        .O(\adr_read_reorder_data_mem2[1]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT5 #(
    .INIT(32'h00008000)) 
    \adr_read_reorder_data_mem2[1]_i_3 
       (.I0(\adr_read_reorder_data_mem2_reg_n_0_[10] ),
        .I1(\adr_read_reorder_data_mem2_reg_n_0_[8] ),
        .I2(\adr_read_reorder_data_mem2_reg_n_0_[7] ),
        .I3(\adr_read_reorder_data_mem2_reg_n_0_[9] ),
        .I4(\adr_read_reorder_data_mem2_reg_n_0_[0] ),
        .O(\adr_read_reorder_data_mem2[1]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hEBEBEBEBFFEBEBEB)) 
    \adr_read_reorder_data_mem2[2]_i_1 
       (.I0(\adr_read_reorder_data_mem1[10]_i_2_n_0 ),
        .I1(\adr_read_reorder_data_mem2[2]_i_2_n_0 ),
        .I2(\adr_read_reorder_data_mem2_reg_n_0_[2] ),
        .I3(\adr_read_reorder_data_mem2[5]_i_3_n_0 ),
        .I4(\adr_read_reorder_data_mem2[6]_i_3_n_0 ),
        .I5(\adr_read_reorder_data_mem2_reg_n_0_[3] ),
        .O(p_1_in[2]));
  (* SOFT_HLUTNM = "soft_lutpair24" *) 
  LUT2 #(
    .INIT(4'h7)) 
    \adr_read_reorder_data_mem2[2]_i_2 
       (.I0(\adr_read_reorder_data_mem2_reg_n_0_[0] ),
        .I1(\adr_read_reorder_data_mem2_reg_n_0_[1] ),
        .O(\adr_read_reorder_data_mem2[2]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hEBEBEBEBFFEBEBEB)) 
    \adr_read_reorder_data_mem2[3]_i_1 
       (.I0(\adr_read_reorder_data_mem1[10]_i_2_n_0 ),
        .I1(\adr_read_reorder_data_mem2[3]_i_2_n_0 ),
        .I2(\adr_read_reorder_data_mem2_reg_n_0_[3] ),
        .I3(\adr_read_reorder_data_mem2[5]_i_3_n_0 ),
        .I4(\adr_read_reorder_data_mem2[6]_i_3_n_0 ),
        .I5(\adr_read_reorder_data_mem2_reg_n_0_[2] ),
        .O(p_1_in[3]));
  (* SOFT_HLUTNM = "soft_lutpair24" *) 
  LUT3 #(
    .INIT(8'h7F)) 
    \adr_read_reorder_data_mem2[3]_i_2 
       (.I0(\adr_read_reorder_data_mem2_reg_n_0_[1] ),
        .I1(\adr_read_reorder_data_mem2_reg_n_0_[0] ),
        .I2(\adr_read_reorder_data_mem2_reg_n_0_[2] ),
        .O(\adr_read_reorder_data_mem2[3]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hEBEBEBEBFFEBEBEB)) 
    \adr_read_reorder_data_mem2[4]_i_1 
       (.I0(\adr_read_reorder_data_mem1[10]_i_2_n_0 ),
        .I1(\adr_read_reorder_data_mem2[4]_i_2_n_0 ),
        .I2(\adr_read_reorder_data_mem2_reg_n_0_[4] ),
        .I3(\adr_read_reorder_data_mem2[5]_i_3_n_0 ),
        .I4(\adr_read_reorder_data_mem2[6]_i_2_n_0 ),
        .I5(\adr_read_reorder_data_mem2_reg_n_0_[5] ),
        .O(p_1_in[4]));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT4 #(
    .INIT(16'h7FFF)) 
    \adr_read_reorder_data_mem2[4]_i_2 
       (.I0(\adr_read_reorder_data_mem2_reg_n_0_[2] ),
        .I1(\adr_read_reorder_data_mem2_reg_n_0_[0] ),
        .I2(\adr_read_reorder_data_mem2_reg_n_0_[1] ),
        .I3(\adr_read_reorder_data_mem2_reg_n_0_[3] ),
        .O(\adr_read_reorder_data_mem2[4]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hEBEBEBEBFFEBEBEB)) 
    \adr_read_reorder_data_mem2[5]_i_1 
       (.I0(\adr_read_reorder_data_mem1[10]_i_2_n_0 ),
        .I1(\adr_read_reorder_data_mem2[5]_i_2_n_0 ),
        .I2(\adr_read_reorder_data_mem2_reg_n_0_[5] ),
        .I3(\adr_read_reorder_data_mem2[5]_i_3_n_0 ),
        .I4(\adr_read_reorder_data_mem2[6]_i_2_n_0 ),
        .I5(\adr_read_reorder_data_mem2_reg_n_0_[4] ),
        .O(p_1_in[5]));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT5 #(
    .INIT(32'h7FFFFFFF)) 
    \adr_read_reorder_data_mem2[5]_i_2 
       (.I0(\adr_read_reorder_data_mem2_reg_n_0_[3] ),
        .I1(\adr_read_reorder_data_mem2_reg_n_0_[1] ),
        .I2(\adr_read_reorder_data_mem2_reg_n_0_[0] ),
        .I3(\adr_read_reorder_data_mem2_reg_n_0_[2] ),
        .I4(\adr_read_reorder_data_mem2_reg_n_0_[4] ),
        .O(\adr_read_reorder_data_mem2[5]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair37" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \adr_read_reorder_data_mem2[5]_i_3 
       (.I0(\adr_read_reorder_data_mem2[6]_i_4_n_0 ),
        .I1(\adr_read_reorder_data_mem2_reg_n_0_[6] ),
        .O(\adr_read_reorder_data_mem2[5]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFEAAAEAAAFFFF)) 
    \adr_read_reorder_data_mem2[6]_i_1 
       (.I0(\adr_read_reorder_data_mem1[10]_i_2_n_0 ),
        .I1(\adr_read_reorder_data_mem2[6]_i_2_n_0 ),
        .I2(\adr_read_reorder_data_mem2[6]_i_3_n_0 ),
        .I3(\adr_read_reorder_data_mem2[6]_i_4_n_0 ),
        .I4(\adr_read_reorder_data_mem2_reg_n_0_[6] ),
        .I5(\adr_read_reorder_data_mem2[6]_i_5_n_0 ),
        .O(p_1_in[6]));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT2 #(
    .INIT(4'h1)) 
    \adr_read_reorder_data_mem2[6]_i_2 
       (.I0(\adr_read_reorder_data_mem2_reg_n_0_[2] ),
        .I1(\adr_read_reorder_data_mem2_reg_n_0_[3] ),
        .O(\adr_read_reorder_data_mem2[6]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    \adr_read_reorder_data_mem2[6]_i_3 
       (.I0(\adr_read_reorder_data_mem2_reg_n_0_[4] ),
        .I1(\adr_read_reorder_data_mem2_reg_n_0_[5] ),
        .O(\adr_read_reorder_data_mem2[6]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h0000000040000000)) 
    \adr_read_reorder_data_mem2[6]_i_4 
       (.I0(\adr_read_reorder_data_mem2_reg_n_0_[0] ),
        .I1(\adr_read_reorder_data_mem2_reg_n_0_[9] ),
        .I2(\adr_read_reorder_data_mem2_reg_n_0_[7] ),
        .I3(\adr_read_reorder_data_mem2_reg_n_0_[8] ),
        .I4(\adr_read_reorder_data_mem2_reg_n_0_[10] ),
        .I5(\adr_read_reorder_data_mem2_reg_n_0_[1] ),
        .O(\adr_read_reorder_data_mem2[6]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h7FFFFFFFFFFFFFFF)) 
    \adr_read_reorder_data_mem2[6]_i_5 
       (.I0(\adr_read_reorder_data_mem2_reg_n_0_[4] ),
        .I1(\adr_read_reorder_data_mem2_reg_n_0_[2] ),
        .I2(\adr_read_reorder_data_mem2_reg_n_0_[0] ),
        .I3(\adr_read_reorder_data_mem2_reg_n_0_[1] ),
        .I4(\adr_read_reorder_data_mem2_reg_n_0_[3] ),
        .I5(\adr_read_reorder_data_mem2_reg_n_0_[5] ),
        .O(\adr_read_reorder_data_mem2[6]_i_5_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT4 #(
    .INIT(16'hFF9F)) 
    \adr_read_reorder_data_mem2[7]_i_1 
       (.I0(\adr_read_reorder_data_mem2_reg_n_0_[7] ),
        .I1(\adr_read_reorder_data_mem2[10]_i_4_n_0 ),
        .I2(s00_axis_aresetn),
        .I3(s00_axis_tuser[11]),
        .O(p_1_in[7]));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT5 #(
    .INIT(32'hFFFF9AFF)) 
    \adr_read_reorder_data_mem2[8]_i_1 
       (.I0(\adr_read_reorder_data_mem2_reg_n_0_[8] ),
        .I1(\adr_read_reorder_data_mem2[10]_i_4_n_0 ),
        .I2(\adr_read_reorder_data_mem2_reg_n_0_[7] ),
        .I3(s00_axis_aresetn),
        .I4(s00_axis_tuser[11]),
        .O(p_1_in[8]));
  LUT6 #(
    .INIT(64'hFFFFFFFF9AAAFFFF)) 
    \adr_read_reorder_data_mem2[9]_i_1 
       (.I0(\adr_read_reorder_data_mem2_reg_n_0_[9] ),
        .I1(\adr_read_reorder_data_mem2[10]_i_4_n_0 ),
        .I2(\adr_read_reorder_data_mem2_reg_n_0_[8] ),
        .I3(\adr_read_reorder_data_mem2_reg_n_0_[7] ),
        .I4(s00_axis_aresetn),
        .I5(s00_axis_tuser[11]),
        .O(p_1_in[9]));
  FDSE \adr_read_reorder_data_mem2_reg[0] 
       (.C(m00_axis_aclk),
        .CE(\adr_read_reorder_data_mem2[10]_i_2_n_0 ),
        .D(p_1_in[0]),
        .Q(\adr_read_reorder_data_mem2_reg_n_0_[0] ),
        .S(\adr_read_reorder_data_mem2[10]_i_1_n_0 ));
  FDSE \adr_read_reorder_data_mem2_reg[10] 
       (.C(m00_axis_aclk),
        .CE(\adr_read_reorder_data_mem2[10]_i_2_n_0 ),
        .D(p_1_in[10]),
        .Q(\adr_read_reorder_data_mem2_reg_n_0_[10] ),
        .S(\adr_read_reorder_data_mem2[10]_i_1_n_0 ));
  FDSE \adr_read_reorder_data_mem2_reg[1] 
       (.C(m00_axis_aclk),
        .CE(\adr_read_reorder_data_mem2[10]_i_2_n_0 ),
        .D(p_1_in[1]),
        .Q(\adr_read_reorder_data_mem2_reg_n_0_[1] ),
        .S(\adr_read_reorder_data_mem2[10]_i_1_n_0 ));
  FDSE \adr_read_reorder_data_mem2_reg[2] 
       (.C(m00_axis_aclk),
        .CE(\adr_read_reorder_data_mem2[10]_i_2_n_0 ),
        .D(p_1_in[2]),
        .Q(\adr_read_reorder_data_mem2_reg_n_0_[2] ),
        .S(\adr_read_reorder_data_mem2[10]_i_1_n_0 ));
  FDSE \adr_read_reorder_data_mem2_reg[3] 
       (.C(m00_axis_aclk),
        .CE(\adr_read_reorder_data_mem2[10]_i_2_n_0 ),
        .D(p_1_in[3]),
        .Q(\adr_read_reorder_data_mem2_reg_n_0_[3] ),
        .S(\adr_read_reorder_data_mem2[10]_i_1_n_0 ));
  FDSE \adr_read_reorder_data_mem2_reg[4] 
       (.C(m00_axis_aclk),
        .CE(\adr_read_reorder_data_mem2[10]_i_2_n_0 ),
        .D(p_1_in[4]),
        .Q(\adr_read_reorder_data_mem2_reg_n_0_[4] ),
        .S(\adr_read_reorder_data_mem2[10]_i_1_n_0 ));
  FDSE \adr_read_reorder_data_mem2_reg[5] 
       (.C(m00_axis_aclk),
        .CE(\adr_read_reorder_data_mem2[10]_i_2_n_0 ),
        .D(p_1_in[5]),
        .Q(\adr_read_reorder_data_mem2_reg_n_0_[5] ),
        .S(\adr_read_reorder_data_mem2[10]_i_1_n_0 ));
  FDSE \adr_read_reorder_data_mem2_reg[6] 
       (.C(m00_axis_aclk),
        .CE(\adr_read_reorder_data_mem2[10]_i_2_n_0 ),
        .D(p_1_in[6]),
        .Q(\adr_read_reorder_data_mem2_reg_n_0_[6] ),
        .S(\adr_read_reorder_data_mem2[10]_i_1_n_0 ));
  FDSE \adr_read_reorder_data_mem2_reg[7] 
       (.C(m00_axis_aclk),
        .CE(\adr_read_reorder_data_mem2[10]_i_2_n_0 ),
        .D(p_1_in[7]),
        .Q(\adr_read_reorder_data_mem2_reg_n_0_[7] ),
        .S(\adr_read_reorder_data_mem2[10]_i_1_n_0 ));
  FDSE \adr_read_reorder_data_mem2_reg[8] 
       (.C(m00_axis_aclk),
        .CE(\adr_read_reorder_data_mem2[10]_i_2_n_0 ),
        .D(p_1_in[8]),
        .Q(\adr_read_reorder_data_mem2_reg_n_0_[8] ),
        .S(\adr_read_reorder_data_mem2[10]_i_1_n_0 ));
  FDSE \adr_read_reorder_data_mem2_reg[9] 
       (.C(m00_axis_aclk),
        .CE(\adr_read_reorder_data_mem2[10]_i_2_n_0 ),
        .D(p_1_in[9]),
        .Q(\adr_read_reorder_data_mem2_reg_n_0_[9] ),
        .S(\adr_read_reorder_data_mem2[10]_i_1_n_0 ));
  CARRY4 cnt_inner_valid0_carry
       (.CI(1'b0),
        .CO({cnt_inner_valid0_carry_n_0,cnt_inner_valid0_carry_n_1,cnt_inner_valid0_carry_n_2,cnt_inner_valid0_carry_n_3}),
        .CYINIT(\cnt_inner_valid_reg_n_0_[0] ),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({cnt_inner_valid0_carry_n_4,cnt_inner_valid0_carry_n_5,cnt_inner_valid0_carry_n_6,cnt_inner_valid0_carry_n_7}),
        .S({\cnt_inner_valid_reg_n_0_[4] ,\cnt_inner_valid_reg_n_0_[3] ,\cnt_inner_valid_reg_n_0_[2] ,\cnt_inner_valid_reg_n_0_[1] }));
  CARRY4 cnt_inner_valid0_carry__0
       (.CI(cnt_inner_valid0_carry_n_0),
        .CO({cnt_inner_valid0_carry__0_n_0,cnt_inner_valid0_carry__0_n_1,cnt_inner_valid0_carry__0_n_2,cnt_inner_valid0_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({cnt_inner_valid0_carry__0_n_4,cnt_inner_valid0_carry__0_n_5,cnt_inner_valid0_carry__0_n_6,cnt_inner_valid0_carry__0_n_7}),
        .S({\cnt_inner_valid_reg_n_0_[8] ,\cnt_inner_valid_reg_n_0_[7] ,\cnt_inner_valid_reg_n_0_[6] ,\cnt_inner_valid_reg_n_0_[5] }));
  CARRY4 cnt_inner_valid0_carry__1
       (.CI(cnt_inner_valid0_carry__0_n_0),
        .CO({cnt_inner_valid0_carry__1_n_0,cnt_inner_valid0_carry__1_n_1,cnt_inner_valid0_carry__1_n_2,cnt_inner_valid0_carry__1_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({cnt_inner_valid0_carry__1_n_4,cnt_inner_valid0_carry__1_n_5,cnt_inner_valid0_carry__1_n_6,cnt_inner_valid0_carry__1_n_7}),
        .S({\cnt_inner_valid_reg_n_0_[12] ,\cnt_inner_valid_reg_n_0_[11] ,\cnt_inner_valid_reg_n_0_[10] ,\cnt_inner_valid_reg_n_0_[9] }));
  CARRY4 cnt_inner_valid0_carry__2
       (.CI(cnt_inner_valid0_carry__1_n_0),
        .CO({NLW_cnt_inner_valid0_carry__2_CO_UNCONNECTED[3:2],cnt_inner_valid0_carry__2_n_2,cnt_inner_valid0_carry__2_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({NLW_cnt_inner_valid0_carry__2_O_UNCONNECTED[3],cnt_inner_valid0_carry__2_n_5,cnt_inner_valid0_carry__2_n_6,cnt_inner_valid0_carry__2_n_7}),
        .S({1'b0,\cnt_inner_valid_reg_n_0_[15] ,\cnt_inner_valid_reg_n_0_[14] ,\cnt_inner_valid_reg_n_0_[13] }));
  LUT5 #(
    .INIT(32'h0000FFFE)) 
    \cnt_inner_valid[0]_i_1 
       (.I0(\cnt_inner_valid[15]_i_4_n_0 ),
        .I1(\cnt_inner_valid[15]_i_3_n_0 ),
        .I2(\cnt_inner_valid_reg_n_0_[4] ),
        .I3(\cnt_inner_valid_reg_n_0_[5] ),
        .I4(\cnt_inner_valid_reg_n_0_[0] ),
        .O(cnt_inner_valid[0]));
  LUT6 #(
    .INIT(64'hFFFFFFFE00000000)) 
    \cnt_inner_valid[10]_i_1 
       (.I0(\cnt_inner_valid_reg_n_0_[5] ),
        .I1(\cnt_inner_valid_reg_n_0_[4] ),
        .I2(\cnt_inner_valid[15]_i_3_n_0 ),
        .I3(\cnt_inner_valid[15]_i_4_n_0 ),
        .I4(\cnt_inner_valid_reg_n_0_[0] ),
        .I5(cnt_inner_valid0_carry__1_n_6),
        .O(cnt_inner_valid[10]));
  LUT6 #(
    .INIT(64'hFFFFFFFE00000000)) 
    \cnt_inner_valid[11]_i_1 
       (.I0(\cnt_inner_valid_reg_n_0_[5] ),
        .I1(\cnt_inner_valid_reg_n_0_[4] ),
        .I2(\cnt_inner_valid[15]_i_3_n_0 ),
        .I3(\cnt_inner_valid[15]_i_4_n_0 ),
        .I4(\cnt_inner_valid_reg_n_0_[0] ),
        .I5(cnt_inner_valid0_carry__1_n_5),
        .O(cnt_inner_valid[11]));
  LUT6 #(
    .INIT(64'hFFFFFFFE00000000)) 
    \cnt_inner_valid[12]_i_1 
       (.I0(\cnt_inner_valid_reg_n_0_[5] ),
        .I1(\cnt_inner_valid_reg_n_0_[4] ),
        .I2(\cnt_inner_valid[15]_i_3_n_0 ),
        .I3(\cnt_inner_valid[15]_i_4_n_0 ),
        .I4(\cnt_inner_valid_reg_n_0_[0] ),
        .I5(cnt_inner_valid0_carry__1_n_4),
        .O(cnt_inner_valid[12]));
  LUT6 #(
    .INIT(64'hFFFFFFFE00000000)) 
    \cnt_inner_valid[13]_i_1 
       (.I0(\cnt_inner_valid_reg_n_0_[5] ),
        .I1(\cnt_inner_valid_reg_n_0_[4] ),
        .I2(\cnt_inner_valid[15]_i_3_n_0 ),
        .I3(\cnt_inner_valid[15]_i_4_n_0 ),
        .I4(\cnt_inner_valid_reg_n_0_[0] ),
        .I5(cnt_inner_valid0_carry__2_n_7),
        .O(cnt_inner_valid[13]));
  LUT6 #(
    .INIT(64'hFFFFFFFE00000000)) 
    \cnt_inner_valid[14]_i_1 
       (.I0(\cnt_inner_valid_reg_n_0_[5] ),
        .I1(\cnt_inner_valid_reg_n_0_[4] ),
        .I2(\cnt_inner_valid[15]_i_3_n_0 ),
        .I3(\cnt_inner_valid[15]_i_4_n_0 ),
        .I4(\cnt_inner_valid_reg_n_0_[0] ),
        .I5(cnt_inner_valid0_carry__2_n_6),
        .O(cnt_inner_valid[14]));
  LUT2 #(
    .INIT(4'h7)) 
    \cnt_inner_valid[15]_i_1 
       (.I0(s00_axis_tvalid),
        .I1(s00_axis_aresetn),
        .O(\cnt_inner_valid[15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFE00000000)) 
    \cnt_inner_valid[15]_i_2 
       (.I0(\cnt_inner_valid_reg_n_0_[5] ),
        .I1(\cnt_inner_valid_reg_n_0_[4] ),
        .I2(\cnt_inner_valid[15]_i_3_n_0 ),
        .I3(\cnt_inner_valid[15]_i_4_n_0 ),
        .I4(\cnt_inner_valid_reg_n_0_[0] ),
        .I5(cnt_inner_valid0_carry__2_n_5),
        .O(cnt_inner_valid[15]));
  (* SOFT_HLUTNM = "soft_lutpair36" *) 
  LUT2 #(
    .INIT(4'hB)) 
    \cnt_inner_valid[15]_i_3 
       (.I0(\cnt_inner_valid[15]_i_5_n_0 ),
        .I1(\cnt_inner_valid_reg_n_0_[7] ),
        .O(\cnt_inner_valid[15]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFF7FF)) 
    \cnt_inner_valid[15]_i_4 
       (.I0(\cnt_inner_valid_reg_n_0_[9] ),
        .I1(\cnt_inner_valid_reg_n_0_[10] ),
        .I2(\cnt_inner_valid_reg_n_0_[3] ),
        .I3(\cnt_inner_valid_reg_n_0_[8] ),
        .I4(\cnt_inner_valid_reg_n_0_[2] ),
        .I5(\cnt_inner_valid_reg_n_0_[1] ),
        .O(\cnt_inner_valid[15]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \cnt_inner_valid[15]_i_5 
       (.I0(\cnt_inner_valid_reg_n_0_[12] ),
        .I1(\cnt_inner_valid_reg_n_0_[13] ),
        .I2(\cnt_inner_valid_reg_n_0_[15] ),
        .I3(\cnt_inner_valid_reg_n_0_[14] ),
        .I4(\cnt_inner_valid_reg_n_0_[11] ),
        .I5(\cnt_inner_valid_reg_n_0_[6] ),
        .O(\cnt_inner_valid[15]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFE00000000)) 
    \cnt_inner_valid[1]_i_1 
       (.I0(\cnt_inner_valid_reg_n_0_[5] ),
        .I1(\cnt_inner_valid_reg_n_0_[4] ),
        .I2(\cnt_inner_valid[15]_i_3_n_0 ),
        .I3(\cnt_inner_valid[15]_i_4_n_0 ),
        .I4(\cnt_inner_valid_reg_n_0_[0] ),
        .I5(cnt_inner_valid0_carry_n_7),
        .O(cnt_inner_valid[1]));
  LUT6 #(
    .INIT(64'hFFFFFFFE00000000)) 
    \cnt_inner_valid[2]_i_1 
       (.I0(\cnt_inner_valid_reg_n_0_[5] ),
        .I1(\cnt_inner_valid_reg_n_0_[4] ),
        .I2(\cnt_inner_valid[15]_i_3_n_0 ),
        .I3(\cnt_inner_valid[15]_i_4_n_0 ),
        .I4(\cnt_inner_valid_reg_n_0_[0] ),
        .I5(cnt_inner_valid0_carry_n_6),
        .O(cnt_inner_valid[2]));
  LUT6 #(
    .INIT(64'hFFFFFFFE00000000)) 
    \cnt_inner_valid[3]_i_1 
       (.I0(\cnt_inner_valid_reg_n_0_[5] ),
        .I1(\cnt_inner_valid_reg_n_0_[4] ),
        .I2(\cnt_inner_valid[15]_i_3_n_0 ),
        .I3(\cnt_inner_valid[15]_i_4_n_0 ),
        .I4(\cnt_inner_valid_reg_n_0_[0] ),
        .I5(cnt_inner_valid0_carry_n_5),
        .O(cnt_inner_valid[3]));
  LUT6 #(
    .INIT(64'hFFFFFFFE00000000)) 
    \cnt_inner_valid[4]_i_1 
       (.I0(\cnt_inner_valid_reg_n_0_[5] ),
        .I1(\cnt_inner_valid_reg_n_0_[4] ),
        .I2(\cnt_inner_valid[15]_i_3_n_0 ),
        .I3(\cnt_inner_valid[15]_i_4_n_0 ),
        .I4(\cnt_inner_valid_reg_n_0_[0] ),
        .I5(cnt_inner_valid0_carry_n_4),
        .O(cnt_inner_valid[4]));
  LUT6 #(
    .INIT(64'hFFFFFFFE00000000)) 
    \cnt_inner_valid[5]_i_1 
       (.I0(\cnt_inner_valid_reg_n_0_[5] ),
        .I1(\cnt_inner_valid_reg_n_0_[4] ),
        .I2(\cnt_inner_valid[15]_i_3_n_0 ),
        .I3(\cnt_inner_valid[15]_i_4_n_0 ),
        .I4(\cnt_inner_valid_reg_n_0_[0] ),
        .I5(cnt_inner_valid0_carry__0_n_7),
        .O(cnt_inner_valid[5]));
  LUT6 #(
    .INIT(64'hFFFFFFFE00000000)) 
    \cnt_inner_valid[6]_i_1 
       (.I0(\cnt_inner_valid_reg_n_0_[5] ),
        .I1(\cnt_inner_valid_reg_n_0_[4] ),
        .I2(\cnt_inner_valid[15]_i_3_n_0 ),
        .I3(\cnt_inner_valid[15]_i_4_n_0 ),
        .I4(\cnt_inner_valid_reg_n_0_[0] ),
        .I5(cnt_inner_valid0_carry__0_n_6),
        .O(cnt_inner_valid[6]));
  LUT6 #(
    .INIT(64'hFFFFFFFE00000000)) 
    \cnt_inner_valid[7]_i_1 
       (.I0(\cnt_inner_valid_reg_n_0_[5] ),
        .I1(\cnt_inner_valid_reg_n_0_[4] ),
        .I2(\cnt_inner_valid[15]_i_3_n_0 ),
        .I3(\cnt_inner_valid[15]_i_4_n_0 ),
        .I4(\cnt_inner_valid_reg_n_0_[0] ),
        .I5(cnt_inner_valid0_carry__0_n_5),
        .O(cnt_inner_valid[7]));
  LUT6 #(
    .INIT(64'hFFFFFFFE00000000)) 
    \cnt_inner_valid[8]_i_1 
       (.I0(\cnt_inner_valid_reg_n_0_[5] ),
        .I1(\cnt_inner_valid_reg_n_0_[4] ),
        .I2(\cnt_inner_valid[15]_i_3_n_0 ),
        .I3(\cnt_inner_valid[15]_i_4_n_0 ),
        .I4(\cnt_inner_valid_reg_n_0_[0] ),
        .I5(cnt_inner_valid0_carry__0_n_4),
        .O(cnt_inner_valid[8]));
  LUT6 #(
    .INIT(64'hFFFFFFFE00000000)) 
    \cnt_inner_valid[9]_i_1 
       (.I0(\cnt_inner_valid_reg_n_0_[5] ),
        .I1(\cnt_inner_valid_reg_n_0_[4] ),
        .I2(\cnt_inner_valid[15]_i_3_n_0 ),
        .I3(\cnt_inner_valid[15]_i_4_n_0 ),
        .I4(\cnt_inner_valid_reg_n_0_[0] ),
        .I5(cnt_inner_valid0_carry__1_n_7),
        .O(cnt_inner_valid[9]));
  FDRE \cnt_inner_valid_reg[0] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(cnt_inner_valid[0]),
        .Q(\cnt_inner_valid_reg_n_0_[0] ),
        .R(\cnt_inner_valid[15]_i_1_n_0 ));
  FDRE \cnt_inner_valid_reg[10] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(cnt_inner_valid[10]),
        .Q(\cnt_inner_valid_reg_n_0_[10] ),
        .R(\cnt_inner_valid[15]_i_1_n_0 ));
  FDRE \cnt_inner_valid_reg[11] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(cnt_inner_valid[11]),
        .Q(\cnt_inner_valid_reg_n_0_[11] ),
        .R(\cnt_inner_valid[15]_i_1_n_0 ));
  FDRE \cnt_inner_valid_reg[12] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(cnt_inner_valid[12]),
        .Q(\cnt_inner_valid_reg_n_0_[12] ),
        .R(\cnt_inner_valid[15]_i_1_n_0 ));
  FDRE \cnt_inner_valid_reg[13] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(cnt_inner_valid[13]),
        .Q(\cnt_inner_valid_reg_n_0_[13] ),
        .R(\cnt_inner_valid[15]_i_1_n_0 ));
  FDRE \cnt_inner_valid_reg[14] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(cnt_inner_valid[14]),
        .Q(\cnt_inner_valid_reg_n_0_[14] ),
        .R(\cnt_inner_valid[15]_i_1_n_0 ));
  FDRE \cnt_inner_valid_reg[15] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(cnt_inner_valid[15]),
        .Q(\cnt_inner_valid_reg_n_0_[15] ),
        .R(\cnt_inner_valid[15]_i_1_n_0 ));
  FDRE \cnt_inner_valid_reg[1] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(cnt_inner_valid[1]),
        .Q(\cnt_inner_valid_reg_n_0_[1] ),
        .R(\cnt_inner_valid[15]_i_1_n_0 ));
  FDRE \cnt_inner_valid_reg[2] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(cnt_inner_valid[2]),
        .Q(\cnt_inner_valid_reg_n_0_[2] ),
        .R(\cnt_inner_valid[15]_i_1_n_0 ));
  FDRE \cnt_inner_valid_reg[3] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(cnt_inner_valid[3]),
        .Q(\cnt_inner_valid_reg_n_0_[3] ),
        .R(\cnt_inner_valid[15]_i_1_n_0 ));
  FDRE \cnt_inner_valid_reg[4] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(cnt_inner_valid[4]),
        .Q(\cnt_inner_valid_reg_n_0_[4] ),
        .R(\cnt_inner_valid[15]_i_1_n_0 ));
  FDRE \cnt_inner_valid_reg[5] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(cnt_inner_valid[5]),
        .Q(\cnt_inner_valid_reg_n_0_[5] ),
        .R(\cnt_inner_valid[15]_i_1_n_0 ));
  FDRE \cnt_inner_valid_reg[6] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(cnt_inner_valid[6]),
        .Q(\cnt_inner_valid_reg_n_0_[6] ),
        .R(\cnt_inner_valid[15]_i_1_n_0 ));
  FDRE \cnt_inner_valid_reg[7] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(cnt_inner_valid[7]),
        .Q(\cnt_inner_valid_reg_n_0_[7] ),
        .R(\cnt_inner_valid[15]_i_1_n_0 ));
  FDRE \cnt_inner_valid_reg[8] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(cnt_inner_valid[8]),
        .Q(\cnt_inner_valid_reg_n_0_[8] ),
        .R(\cnt_inner_valid[15]_i_1_n_0 ));
  FDRE \cnt_inner_valid_reg[9] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(cnt_inner_valid[9]),
        .Q(\cnt_inner_valid_reg_n_0_[9] ),
        .R(\cnt_inner_valid[15]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hFF40)) 
    \cnt_last[0]_i_2 
       (.I0(\adr_read_reorder_data_mem1[6]_i_3_n_0 ),
        .I1(m00_axis_tvalid),
        .I2(s00_axis_tuser[11]),
        .I3(\cnt_last[0]_i_4_n_0 ),
        .O(last));
  LUT1 #(
    .INIT(2'h1)) 
    \cnt_last[0]_i_3 
       (.I0(cnt_last_reg[0]),
        .O(\cnt_last[0]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000010)) 
    \cnt_last[0]_i_4 
       (.I0(s00_axis_tuser[1]),
        .I1(s00_axis_tuser[2]),
        .I2(m00_axis_tvalid),
        .I3(s00_axis_tuser[0]),
        .I4(s00_axis_tuser[11]),
        .I5(s00_axis_tuser[3]),
        .O(\cnt_last[0]_i_4_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_last_reg[0] 
       (.C(last),
        .CE(1'b1),
        .D(\cnt_last_reg[0]_i_1_n_7 ),
        .Q(cnt_last_reg[0]),
        .R(m00_axis_tlast));
  CARRY4 \cnt_last_reg[0]_i_1 
       (.CI(1'b0),
        .CO({\cnt_last_reg[0]_i_1_n_0 ,\cnt_last_reg[0]_i_1_n_1 ,\cnt_last_reg[0]_i_1_n_2 ,\cnt_last_reg[0]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\cnt_last_reg[0]_i_1_n_4 ,\cnt_last_reg[0]_i_1_n_5 ,\cnt_last_reg[0]_i_1_n_6 ,\cnt_last_reg[0]_i_1_n_7 }),
        .S({cnt_last_reg[3:1],\cnt_last[0]_i_3_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_last_reg[10] 
       (.C(last),
        .CE(1'b1),
        .D(\cnt_last_reg[8]_i_1_n_5 ),
        .Q(cnt_last_reg[10]),
        .R(m00_axis_tlast));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_last_reg[11] 
       (.C(last),
        .CE(1'b1),
        .D(\cnt_last_reg[8]_i_1_n_4 ),
        .Q(cnt_last_reg[11]),
        .R(m00_axis_tlast));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_last_reg[12] 
       (.C(last),
        .CE(1'b1),
        .D(\cnt_last_reg[12]_i_1_n_7 ),
        .Q(cnt_last_reg[12]),
        .R(m00_axis_tlast));
  CARRY4 \cnt_last_reg[12]_i_1 
       (.CI(\cnt_last_reg[8]_i_1_n_0 ),
        .CO({\NLW_cnt_last_reg[12]_i_1_CO_UNCONNECTED [3],\cnt_last_reg[12]_i_1_n_1 ,\cnt_last_reg[12]_i_1_n_2 ,\cnt_last_reg[12]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\cnt_last_reg[12]_i_1_n_4 ,\cnt_last_reg[12]_i_1_n_5 ,\cnt_last_reg[12]_i_1_n_6 ,\cnt_last_reg[12]_i_1_n_7 }),
        .S(cnt_last_reg[15:12]));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_last_reg[13] 
       (.C(last),
        .CE(1'b1),
        .D(\cnt_last_reg[12]_i_1_n_6 ),
        .Q(cnt_last_reg[13]),
        .R(m00_axis_tlast));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_last_reg[14] 
       (.C(last),
        .CE(1'b1),
        .D(\cnt_last_reg[12]_i_1_n_5 ),
        .Q(cnt_last_reg[14]),
        .R(m00_axis_tlast));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_last_reg[15] 
       (.C(last),
        .CE(1'b1),
        .D(\cnt_last_reg[12]_i_1_n_4 ),
        .Q(cnt_last_reg[15]),
        .R(m00_axis_tlast));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_last_reg[1] 
       (.C(last),
        .CE(1'b1),
        .D(\cnt_last_reg[0]_i_1_n_6 ),
        .Q(cnt_last_reg[1]),
        .R(m00_axis_tlast));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_last_reg[2] 
       (.C(last),
        .CE(1'b1),
        .D(\cnt_last_reg[0]_i_1_n_5 ),
        .Q(cnt_last_reg[2]),
        .R(m00_axis_tlast));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_last_reg[3] 
       (.C(last),
        .CE(1'b1),
        .D(\cnt_last_reg[0]_i_1_n_4 ),
        .Q(cnt_last_reg[3]),
        .R(m00_axis_tlast));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_last_reg[4] 
       (.C(last),
        .CE(1'b1),
        .D(\cnt_last_reg[4]_i_1_n_7 ),
        .Q(cnt_last_reg[4]),
        .R(m00_axis_tlast));
  CARRY4 \cnt_last_reg[4]_i_1 
       (.CI(\cnt_last_reg[0]_i_1_n_0 ),
        .CO({\cnt_last_reg[4]_i_1_n_0 ,\cnt_last_reg[4]_i_1_n_1 ,\cnt_last_reg[4]_i_1_n_2 ,\cnt_last_reg[4]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\cnt_last_reg[4]_i_1_n_4 ,\cnt_last_reg[4]_i_1_n_5 ,\cnt_last_reg[4]_i_1_n_6 ,\cnt_last_reg[4]_i_1_n_7 }),
        .S(cnt_last_reg[7:4]));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_last_reg[5] 
       (.C(last),
        .CE(1'b1),
        .D(\cnt_last_reg[4]_i_1_n_6 ),
        .Q(cnt_last_reg[5]),
        .R(m00_axis_tlast));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_last_reg[6] 
       (.C(last),
        .CE(1'b1),
        .D(\cnt_last_reg[4]_i_1_n_5 ),
        .Q(cnt_last_reg[6]),
        .R(m00_axis_tlast));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_last_reg[7] 
       (.C(last),
        .CE(1'b1),
        .D(\cnt_last_reg[4]_i_1_n_4 ),
        .Q(cnt_last_reg[7]),
        .R(m00_axis_tlast));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_last_reg[8] 
       (.C(last),
        .CE(1'b1),
        .D(\cnt_last_reg[8]_i_1_n_7 ),
        .Q(cnt_last_reg[8]),
        .R(m00_axis_tlast));
  CARRY4 \cnt_last_reg[8]_i_1 
       (.CI(\cnt_last_reg[4]_i_1_n_0 ),
        .CO({\cnt_last_reg[8]_i_1_n_0 ,\cnt_last_reg[8]_i_1_n_1 ,\cnt_last_reg[8]_i_1_n_2 ,\cnt_last_reg[8]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\cnt_last_reg[8]_i_1_n_4 ,\cnt_last_reg[8]_i_1_n_5 ,\cnt_last_reg[8]_i_1_n_6 ,\cnt_last_reg[8]_i_1_n_7 }),
        .S(cnt_last_reg[11:8]));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_last_reg[9] 
       (.C(last),
        .CE(1'b1),
        .D(\cnt_last_reg[8]_i_1_n_6 ),
        .Q(cnt_last_reg[9]),
        .R(m00_axis_tlast));
  LUT1 #(
    .INIT(2'h1)) 
    \ctrl_mux_out_1[3]_i_1 
       (.I0(s00_axis_tuser[11]),
        .O(\ctrl_mux_out_1[3]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hBF8F)) 
    \ctrl_mux_out_1[4]_i_1 
       (.I0(\active_mem1_in_read_state_reg_n_0_[4] ),
        .I1(s00_axis_aresetn),
        .I2(s00_axis_tuser[11]),
        .I3(ctrl_mux_out_1[4]),
        .O(\ctrl_mux_out_1[4]_i_1_n_0 ));
  FDRE \ctrl_mux_out_1_reg[0] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_aresetn),
        .D(\active_mem1_in_read_state_reg_n_0_[0] ),
        .Q(ctrl_mux_out_1[0]),
        .R(\ctrl_mux_out_1[3]_i_1_n_0 ));
  FDRE \ctrl_mux_out_1_reg[1] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_aresetn),
        .D(\active_mem1_in_read_state_reg_n_0_[1] ),
        .Q(ctrl_mux_out_1[1]),
        .R(\ctrl_mux_out_1[3]_i_1_n_0 ));
  FDRE \ctrl_mux_out_1_reg[2] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_aresetn),
        .D(\active_mem1_in_read_state_reg_n_0_[2] ),
        .Q(ctrl_mux_out_1[2]),
        .R(\ctrl_mux_out_1[3]_i_1_n_0 ));
  FDRE \ctrl_mux_out_1_reg[3] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_aresetn),
        .D(\active_mem1_in_read_state_reg_n_0_[3] ),
        .Q(ctrl_mux_out_1[3]),
        .R(\ctrl_mux_out_1[3]_i_1_n_0 ));
  FDRE \ctrl_mux_out_1_reg[4] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(\ctrl_mux_out_1[4]_i_1_n_0 ),
        .Q(ctrl_mux_out_1[4]),
        .R(1'b0));
  LUT4 #(
    .INIT(16'hFBF8)) 
    \ctrl_mux_out_2[4]_i_1 
       (.I0(active_mem2_in_read_state_reg[4]),
        .I1(s00_axis_aresetn),
        .I2(s00_axis_tuser[11]),
        .I3(ctrl_mux_out_2[4]),
        .O(\ctrl_mux_out_2[4]_i_1_n_0 ));
  FDRE \ctrl_mux_out_2_reg[0] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_aresetn),
        .D(active_mem2_in_read_state_reg[0]),
        .Q(ctrl_mux_out_2[0]),
        .R(s00_axis_tuser[11]));
  FDRE \ctrl_mux_out_2_reg[1] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_aresetn),
        .D(active_mem2_in_read_state_reg[1]),
        .Q(ctrl_mux_out_2[1]),
        .R(s00_axis_tuser[11]));
  FDRE \ctrl_mux_out_2_reg[2] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_aresetn),
        .D(active_mem2_in_read_state_reg[2]),
        .Q(ctrl_mux_out_2[2]),
        .R(s00_axis_tuser[11]));
  FDRE \ctrl_mux_out_2_reg[3] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_aresetn),
        .D(active_mem2_in_read_state_reg[3]),
        .Q(ctrl_mux_out_2[3]),
        .R(s00_axis_tuser[11]));
  FDRE \ctrl_mux_out_2_reg[4] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(\ctrl_mux_out_2[4]_i_1_n_0 ),
        .Q(ctrl_mux_out_2[4]),
        .R(1'b0));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst \genblk1[0].rams_sp_rf_rst_inst0 
       (.Q(\EN1_reg_n_0_[0] ),
        .addr({\genblk1[15].rams_sp_rf_rst_inst0_n_24 ,\genblk1[15].rams_sp_rf_rst_inst0_n_25 ,\genblk1[15].rams_sp_rf_rst_inst0_n_26 ,\genblk1[15].rams_sp_rf_rst_inst0_n_27 ,\genblk1[15].rams_sp_rf_rst_inst0_n_28 ,\genblk1[15].rams_sp_rf_rst_inst0_n_29 ,\genblk1[15].rams_sp_rf_rst_inst0_n_30 ,\genblk1[15].rams_sp_rf_rst_inst0_n_31 ,\genblk1[15].rams_sp_rf_rst_inst0_n_32 ,\genblk1[15].rams_sp_rf_rst_inst0_n_33 ,\genblk1[15].rams_sp_rf_rst_inst0_n_34 }),
        .dout0_out(\_DOUT_1[0]_0 ),
        .m00_axis_aclk(m00_axis_aclk),
        .s00_axis_tdata(s00_axis_tdata),
        .s00_axis_tuser(s00_axis_tuser[11]),
        .s00_axis_tvalid(s00_axis_tvalid));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_0 \genblk1[0].rams_sp_rf_rst_inst1 
       (.Q(\EN2_reg_n_0_[0] ),
        .addr(ADDR2),
        .dout0_out(\_DOUT_2[0]_1 ),
        .m00_axis_aclk(m00_axis_aclk),
        .s00_axis_tdata(s00_axis_tdata),
        .s00_axis_tuser(s00_axis_tuser[11]),
        .s00_axis_tvalid(s00_axis_tvalid));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_1 \genblk1[10].rams_sp_rf_rst_inst0 
       (.Q(\EN1_reg_n_0_[10] ),
        .addr({\genblk1[15].rams_sp_rf_rst_inst0_n_24 ,\genblk1[15].rams_sp_rf_rst_inst0_n_25 ,\genblk1[15].rams_sp_rf_rst_inst0_n_26 ,\genblk1[15].rams_sp_rf_rst_inst0_n_27 ,\genblk1[15].rams_sp_rf_rst_inst0_n_28 ,\genblk1[15].rams_sp_rf_rst_inst0_n_29 ,\genblk1[15].rams_sp_rf_rst_inst0_n_30 ,\genblk1[15].rams_sp_rf_rst_inst0_n_31 ,\genblk1[15].rams_sp_rf_rst_inst0_n_32 ,\genblk1[15].rams_sp_rf_rst_inst0_n_33 ,\genblk1[15].rams_sp_rf_rst_inst0_n_34 }),
        .dout0_out(\_DOUT_1[10]_20 ),
        .m00_axis_aclk(m00_axis_aclk),
        .s00_axis_tdata(s00_axis_tdata),
        .s00_axis_tuser(s00_axis_tuser[11]),
        .s00_axis_tvalid(s00_axis_tvalid));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_2 \genblk1[10].rams_sp_rf_rst_inst1 
       (.Q(\EN2_reg_n_0_[10] ),
        .addr(ADDR2),
        .dout0_out(\_DOUT_2[10]_21 ),
        .m00_axis_aclk(m00_axis_aclk),
        .s00_axis_tdata(s00_axis_tdata),
        .s00_axis_tuser(s00_axis_tuser[11]),
        .s00_axis_tvalid(s00_axis_tvalid));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_3 \genblk1[11].rams_sp_rf_rst_inst0 
       (.Q(\EN1_reg_n_0_[11] ),
        .addr({\genblk1[15].rams_sp_rf_rst_inst0_n_24 ,\genblk1[15].rams_sp_rf_rst_inst0_n_25 ,\genblk1[15].rams_sp_rf_rst_inst0_n_26 ,\genblk1[15].rams_sp_rf_rst_inst0_n_27 ,\genblk1[15].rams_sp_rf_rst_inst0_n_28 ,\genblk1[15].rams_sp_rf_rst_inst0_n_29 ,\genblk1[15].rams_sp_rf_rst_inst0_n_30 ,\genblk1[15].rams_sp_rf_rst_inst0_n_31 ,\genblk1[15].rams_sp_rf_rst_inst0_n_32 ,\genblk1[15].rams_sp_rf_rst_inst0_n_33 ,\genblk1[15].rams_sp_rf_rst_inst0_n_34 }),
        .ctrl_mux_out_1(ctrl_mux_out_1[1:0]),
        .dout0_out(\_DOUT_1[10]_20 ),
        .m00_axis_aclk(m00_axis_aclk),
        .ram_reg_0_0(\genblk1[11].rams_sp_rf_rst_inst0_n_0 ),
        .ram_reg_0_1(\genblk1[11].rams_sp_rf_rst_inst0_n_1 ),
        .ram_reg_0_10(\genblk1[11].rams_sp_rf_rst_inst0_n_10 ),
        .ram_reg_0_11(\genblk1[11].rams_sp_rf_rst_inst0_n_11 ),
        .ram_reg_0_12(\genblk1[11].rams_sp_rf_rst_inst0_n_12 ),
        .ram_reg_0_13(\genblk1[11].rams_sp_rf_rst_inst0_n_13 ),
        .ram_reg_0_14(\genblk1[11].rams_sp_rf_rst_inst0_n_14 ),
        .ram_reg_0_15(\genblk1[11].rams_sp_rf_rst_inst0_n_15 ),
        .ram_reg_0_16(\genblk1[11].rams_sp_rf_rst_inst0_n_16 ),
        .ram_reg_0_17(\genblk1[11].rams_sp_rf_rst_inst0_n_17 ),
        .ram_reg_0_2(\genblk1[11].rams_sp_rf_rst_inst0_n_2 ),
        .ram_reg_0_3(\genblk1[11].rams_sp_rf_rst_inst0_n_3 ),
        .ram_reg_0_4(\genblk1[11].rams_sp_rf_rst_inst0_n_4 ),
        .ram_reg_0_5(\genblk1[11].rams_sp_rf_rst_inst0_n_5 ),
        .ram_reg_0_6(\genblk1[11].rams_sp_rf_rst_inst0_n_6 ),
        .ram_reg_0_7(\genblk1[11].rams_sp_rf_rst_inst0_n_7 ),
        .ram_reg_0_8(\genblk1[11].rams_sp_rf_rst_inst0_n_8 ),
        .ram_reg_0_9(\genblk1[11].rams_sp_rf_rst_inst0_n_9 ),
        .ram_reg_1_0(\genblk1[11].rams_sp_rf_rst_inst0_n_18 ),
        .ram_reg_1_1(\genblk1[11].rams_sp_rf_rst_inst0_n_19 ),
        .ram_reg_1_2(\genblk1[11].rams_sp_rf_rst_inst0_n_20 ),
        .ram_reg_1_3(\genblk1[11].rams_sp_rf_rst_inst0_n_21 ),
        .ram_reg_1_4(\genblk1[11].rams_sp_rf_rst_inst0_n_22 ),
        .ram_reg_1_5(\genblk1[11].rams_sp_rf_rst_inst0_n_23 ),
        .s00_axis_tdata(s00_axis_tdata),
        .s00_axis_tuser(s00_axis_tuser[11]),
        .s00_axis_tvalid(s00_axis_tvalid),
        .\y_reg[23]_i_1 (\_DOUT_1[9]_18 ),
        .\y_reg[23]_i_1_0 (\_DOUT_1[8]_16 ));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_4 \genblk1[11].rams_sp_rf_rst_inst1 
       (.Q(\EN2_reg_n_0_[11] ),
        .addr(ADDR2),
        .ctrl_mux_out_2(ctrl_mux_out_2[1:0]),
        .dout0_out(\_DOUT_2[10]_21 ),
        .m00_axis_aclk(m00_axis_aclk),
        .ram_reg_0_0(\genblk1[11].rams_sp_rf_rst_inst1_n_0 ),
        .ram_reg_0_1(\genblk1[11].rams_sp_rf_rst_inst1_n_1 ),
        .ram_reg_0_10(\genblk1[11].rams_sp_rf_rst_inst1_n_10 ),
        .ram_reg_0_11(\genblk1[11].rams_sp_rf_rst_inst1_n_11 ),
        .ram_reg_0_12(\genblk1[11].rams_sp_rf_rst_inst1_n_12 ),
        .ram_reg_0_13(\genblk1[11].rams_sp_rf_rst_inst1_n_13 ),
        .ram_reg_0_14(\genblk1[11].rams_sp_rf_rst_inst1_n_14 ),
        .ram_reg_0_15(\genblk1[11].rams_sp_rf_rst_inst1_n_15 ),
        .ram_reg_0_16(\genblk1[11].rams_sp_rf_rst_inst1_n_16 ),
        .ram_reg_0_17(\genblk1[11].rams_sp_rf_rst_inst1_n_17 ),
        .ram_reg_0_2(\genblk1[11].rams_sp_rf_rst_inst1_n_2 ),
        .ram_reg_0_3(\genblk1[11].rams_sp_rf_rst_inst1_n_3 ),
        .ram_reg_0_4(\genblk1[11].rams_sp_rf_rst_inst1_n_4 ),
        .ram_reg_0_5(\genblk1[11].rams_sp_rf_rst_inst1_n_5 ),
        .ram_reg_0_6(\genblk1[11].rams_sp_rf_rst_inst1_n_6 ),
        .ram_reg_0_7(\genblk1[11].rams_sp_rf_rst_inst1_n_7 ),
        .ram_reg_0_8(\genblk1[11].rams_sp_rf_rst_inst1_n_8 ),
        .ram_reg_0_9(\genblk1[11].rams_sp_rf_rst_inst1_n_9 ),
        .ram_reg_1_0(\genblk1[11].rams_sp_rf_rst_inst1_n_18 ),
        .ram_reg_1_1(\genblk1[11].rams_sp_rf_rst_inst1_n_19 ),
        .ram_reg_1_2(\genblk1[11].rams_sp_rf_rst_inst1_n_20 ),
        .ram_reg_1_3(\genblk1[11].rams_sp_rf_rst_inst1_n_21 ),
        .ram_reg_1_4(\genblk1[11].rams_sp_rf_rst_inst1_n_22 ),
        .ram_reg_1_5(\genblk1[11].rams_sp_rf_rst_inst1_n_23 ),
        .s00_axis_tdata(s00_axis_tdata),
        .s00_axis_tuser(s00_axis_tuser[11]),
        .s00_axis_tvalid(s00_axis_tvalid),
        .\y_reg[23]_i_1__0 (\_DOUT_2[9]_19 ),
        .\y_reg[23]_i_1__0_0 (\_DOUT_2[8]_17 ));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_5 \genblk1[12].rams_sp_rf_rst_inst0 
       (.Q(\EN1_reg_n_0_[12] ),
        .addr({\genblk1[15].rams_sp_rf_rst_inst0_n_24 ,\genblk1[15].rams_sp_rf_rst_inst0_n_25 ,\genblk1[15].rams_sp_rf_rst_inst0_n_26 ,\genblk1[15].rams_sp_rf_rst_inst0_n_27 ,\genblk1[15].rams_sp_rf_rst_inst0_n_28 ,\genblk1[15].rams_sp_rf_rst_inst0_n_29 ,\genblk1[15].rams_sp_rf_rst_inst0_n_30 ,\genblk1[15].rams_sp_rf_rst_inst0_n_31 ,\genblk1[15].rams_sp_rf_rst_inst0_n_32 ,\genblk1[15].rams_sp_rf_rst_inst0_n_33 ,\genblk1[15].rams_sp_rf_rst_inst0_n_34 }),
        .dout0_out(\_DOUT_1[12]_24 ),
        .m00_axis_aclk(m00_axis_aclk),
        .s00_axis_tdata(s00_axis_tdata),
        .s00_axis_tuser(s00_axis_tuser[11]),
        .s00_axis_tvalid(s00_axis_tvalid));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_6 \genblk1[12].rams_sp_rf_rst_inst1 
       (.Q(\EN2_reg_n_0_[12] ),
        .addr(ADDR2),
        .dout0_out(\_DOUT_2[12]_25 ),
        .m00_axis_aclk(m00_axis_aclk),
        .s00_axis_tdata(s00_axis_tdata),
        .s00_axis_tuser(s00_axis_tuser[11]),
        .s00_axis_tvalid(s00_axis_tvalid));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_7 \genblk1[13].rams_sp_rf_rst_inst0 
       (.Q(\EN1_reg_n_0_[13] ),
        .addr({\genblk1[15].rams_sp_rf_rst_inst0_n_24 ,\genblk1[15].rams_sp_rf_rst_inst0_n_25 ,\genblk1[15].rams_sp_rf_rst_inst0_n_26 ,\genblk1[15].rams_sp_rf_rst_inst0_n_27 ,\genblk1[15].rams_sp_rf_rst_inst0_n_28 ,\genblk1[15].rams_sp_rf_rst_inst0_n_29 ,\genblk1[15].rams_sp_rf_rst_inst0_n_30 ,\genblk1[15].rams_sp_rf_rst_inst0_n_31 ,\genblk1[15].rams_sp_rf_rst_inst0_n_32 ,\genblk1[15].rams_sp_rf_rst_inst0_n_33 ,\genblk1[15].rams_sp_rf_rst_inst0_n_34 }),
        .dout0_out(\_DOUT_1[13]_26 ),
        .m00_axis_aclk(m00_axis_aclk),
        .s00_axis_tdata(s00_axis_tdata),
        .s00_axis_tuser(s00_axis_tuser[11]),
        .s00_axis_tvalid(s00_axis_tvalid));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_8 \genblk1[13].rams_sp_rf_rst_inst1 
       (.Q(\EN2_reg_n_0_[13] ),
        .addr(ADDR2),
        .dout0_out(\_DOUT_2[13]_27 ),
        .m00_axis_aclk(m00_axis_aclk),
        .s00_axis_tdata(s00_axis_tdata),
        .s00_axis_tuser(s00_axis_tuser[11]),
        .s00_axis_tvalid(s00_axis_tvalid));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_9 \genblk1[14].rams_sp_rf_rst_inst0 
       (.Q(\EN1_reg_n_0_[14] ),
        .addr({\genblk1[15].rams_sp_rf_rst_inst0_n_24 ,\genblk1[15].rams_sp_rf_rst_inst0_n_25 ,\genblk1[15].rams_sp_rf_rst_inst0_n_26 ,\genblk1[15].rams_sp_rf_rst_inst0_n_27 ,\genblk1[15].rams_sp_rf_rst_inst0_n_28 ,\genblk1[15].rams_sp_rf_rst_inst0_n_29 ,\genblk1[15].rams_sp_rf_rst_inst0_n_30 ,\genblk1[15].rams_sp_rf_rst_inst0_n_31 ,\genblk1[15].rams_sp_rf_rst_inst0_n_32 ,\genblk1[15].rams_sp_rf_rst_inst0_n_33 ,\genblk1[15].rams_sp_rf_rst_inst0_n_34 }),
        .dout0_out(\_DOUT_1[14]_28 ),
        .m00_axis_aclk(m00_axis_aclk),
        .s00_axis_tdata(s00_axis_tdata),
        .s00_axis_tuser(s00_axis_tuser[11]),
        .s00_axis_tvalid(s00_axis_tvalid));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_10 \genblk1[14].rams_sp_rf_rst_inst1 
       (.Q(\EN2_reg_n_0_[14] ),
        .addr(ADDR2),
        .dout0_out(\_DOUT_2[14]_29 ),
        .m00_axis_aclk(m00_axis_aclk),
        .s00_axis_tdata(s00_axis_tdata),
        .s00_axis_tuser(s00_axis_tuser[11]),
        .s00_axis_tvalid(s00_axis_tvalid));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_11 \genblk1[15].rams_sp_rf_rst_inst0 
       (.Q({\adr_read_reorder_data_mem1_reg_n_0_[10] ,\adr_read_reorder_data_mem1_reg_n_0_[9] ,\adr_read_reorder_data_mem1_reg_n_0_[8] ,\adr_read_reorder_data_mem1_reg_n_0_[7] ,\adr_read_reorder_data_mem1_reg_n_0_[6] ,\adr_read_reorder_data_mem1_reg_n_0_[5] ,\adr_read_reorder_data_mem1_reg_n_0_[4] ,\adr_read_reorder_data_mem1_reg_n_0_[3] ,\adr_read_reorder_data_mem1_reg_n_0_[2] ,\adr_read_reorder_data_mem1_reg_n_0_[1] ,\adr_read_reorder_data_mem1_reg_n_0_[0] }),
        .addr({\genblk1[15].rams_sp_rf_rst_inst0_n_24 ,\genblk1[15].rams_sp_rf_rst_inst0_n_25 ,\genblk1[15].rams_sp_rf_rst_inst0_n_26 ,\genblk1[15].rams_sp_rf_rst_inst0_n_27 ,\genblk1[15].rams_sp_rf_rst_inst0_n_28 ,\genblk1[15].rams_sp_rf_rst_inst0_n_29 ,\genblk1[15].rams_sp_rf_rst_inst0_n_30 ,\genblk1[15].rams_sp_rf_rst_inst0_n_31 ,\genblk1[15].rams_sp_rf_rst_inst0_n_32 ,\genblk1[15].rams_sp_rf_rst_inst0_n_33 ,\genblk1[15].rams_sp_rf_rst_inst0_n_34 }),
        .ctrl_mux_out_1(ctrl_mux_out_1[1:0]),
        .dout0_out(\_DOUT_1[14]_28 ),
        .m00_axis_aclk(m00_axis_aclk),
        .ram_reg_0_0(\genblk1[15].rams_sp_rf_rst_inst0_n_0 ),
        .ram_reg_0_1(\genblk1[15].rams_sp_rf_rst_inst0_n_1 ),
        .ram_reg_0_10(\genblk1[15].rams_sp_rf_rst_inst0_n_10 ),
        .ram_reg_0_11(\genblk1[15].rams_sp_rf_rst_inst0_n_11 ),
        .ram_reg_0_12(\genblk1[15].rams_sp_rf_rst_inst0_n_12 ),
        .ram_reg_0_13(\genblk1[15].rams_sp_rf_rst_inst0_n_13 ),
        .ram_reg_0_14(\genblk1[15].rams_sp_rf_rst_inst0_n_14 ),
        .ram_reg_0_15(\genblk1[15].rams_sp_rf_rst_inst0_n_15 ),
        .ram_reg_0_16(\genblk1[15].rams_sp_rf_rst_inst0_n_16 ),
        .ram_reg_0_17(\genblk1[15].rams_sp_rf_rst_inst0_n_17 ),
        .ram_reg_0_18(\EN1_reg_n_0_[15] ),
        .ram_reg_0_2(\genblk1[15].rams_sp_rf_rst_inst0_n_2 ),
        .ram_reg_0_3(\genblk1[15].rams_sp_rf_rst_inst0_n_3 ),
        .ram_reg_0_4(\genblk1[15].rams_sp_rf_rst_inst0_n_4 ),
        .ram_reg_0_5(\genblk1[15].rams_sp_rf_rst_inst0_n_5 ),
        .ram_reg_0_6(\genblk1[15].rams_sp_rf_rst_inst0_n_6 ),
        .ram_reg_0_7(\genblk1[15].rams_sp_rf_rst_inst0_n_7 ),
        .ram_reg_0_8(\genblk1[15].rams_sp_rf_rst_inst0_n_8 ),
        .ram_reg_0_9(\genblk1[15].rams_sp_rf_rst_inst0_n_9 ),
        .ram_reg_1_0(\genblk1[15].rams_sp_rf_rst_inst0_n_18 ),
        .ram_reg_1_1(\genblk1[15].rams_sp_rf_rst_inst0_n_19 ),
        .ram_reg_1_2(\genblk1[15].rams_sp_rf_rst_inst0_n_20 ),
        .ram_reg_1_3(\genblk1[15].rams_sp_rf_rst_inst0_n_21 ),
        .ram_reg_1_4(\genblk1[15].rams_sp_rf_rst_inst0_n_22 ),
        .ram_reg_1_5(\genblk1[15].rams_sp_rf_rst_inst0_n_23 ),
        .s00_axis_tdata(s00_axis_tdata),
        .s00_axis_tuser(s00_axis_tuser[11:0]),
        .s00_axis_tvalid(s00_axis_tvalid),
        .\y_reg[23]_i_1 (\_DOUT_1[13]_26 ),
        .\y_reg[23]_i_1_0 (\_DOUT_1[12]_24 ));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_12 \genblk1[15].rams_sp_rf_rst_inst1 
       (.Q({\adr_read_reorder_data_mem2_reg_n_0_[10] ,\adr_read_reorder_data_mem2_reg_n_0_[9] ,\adr_read_reorder_data_mem2_reg_n_0_[8] ,\adr_read_reorder_data_mem2_reg_n_0_[7] ,\adr_read_reorder_data_mem2_reg_n_0_[6] ,\adr_read_reorder_data_mem2_reg_n_0_[5] ,\adr_read_reorder_data_mem2_reg_n_0_[4] ,\adr_read_reorder_data_mem2_reg_n_0_[3] ,\adr_read_reorder_data_mem2_reg_n_0_[2] ,\adr_read_reorder_data_mem2_reg_n_0_[1] ,\adr_read_reorder_data_mem2_reg_n_0_[0] }),
        .addr(ADDR2),
        .ctrl_mux_out_2(ctrl_mux_out_2[1:0]),
        .dout0_out(\_DOUT_2[14]_29 ),
        .m00_axis_aclk(m00_axis_aclk),
        .ram_reg_0_0(\genblk1[15].rams_sp_rf_rst_inst1_n_0 ),
        .ram_reg_0_1(\genblk1[15].rams_sp_rf_rst_inst1_n_1 ),
        .ram_reg_0_10(\genblk1[15].rams_sp_rf_rst_inst1_n_10 ),
        .ram_reg_0_11(\genblk1[15].rams_sp_rf_rst_inst1_n_11 ),
        .ram_reg_0_12(\genblk1[15].rams_sp_rf_rst_inst1_n_12 ),
        .ram_reg_0_13(\genblk1[15].rams_sp_rf_rst_inst1_n_13 ),
        .ram_reg_0_14(\genblk1[15].rams_sp_rf_rst_inst1_n_14 ),
        .ram_reg_0_15(\genblk1[15].rams_sp_rf_rst_inst1_n_15 ),
        .ram_reg_0_16(\genblk1[15].rams_sp_rf_rst_inst1_n_16 ),
        .ram_reg_0_17(\genblk1[15].rams_sp_rf_rst_inst1_n_17 ),
        .ram_reg_0_18(\EN2_reg_n_0_[15] ),
        .ram_reg_0_2(\genblk1[15].rams_sp_rf_rst_inst1_n_2 ),
        .ram_reg_0_3(\genblk1[15].rams_sp_rf_rst_inst1_n_3 ),
        .ram_reg_0_4(\genblk1[15].rams_sp_rf_rst_inst1_n_4 ),
        .ram_reg_0_5(\genblk1[15].rams_sp_rf_rst_inst1_n_5 ),
        .ram_reg_0_6(\genblk1[15].rams_sp_rf_rst_inst1_n_6 ),
        .ram_reg_0_7(\genblk1[15].rams_sp_rf_rst_inst1_n_7 ),
        .ram_reg_0_8(\genblk1[15].rams_sp_rf_rst_inst1_n_8 ),
        .ram_reg_0_9(\genblk1[15].rams_sp_rf_rst_inst1_n_9 ),
        .ram_reg_1_0(\genblk1[15].rams_sp_rf_rst_inst1_n_18 ),
        .ram_reg_1_1(\genblk1[15].rams_sp_rf_rst_inst1_n_19 ),
        .ram_reg_1_2(\genblk1[15].rams_sp_rf_rst_inst1_n_20 ),
        .ram_reg_1_3(\genblk1[15].rams_sp_rf_rst_inst1_n_21 ),
        .ram_reg_1_4(\genblk1[15].rams_sp_rf_rst_inst1_n_22 ),
        .ram_reg_1_5(\genblk1[15].rams_sp_rf_rst_inst1_n_23 ),
        .s00_axis_tdata(s00_axis_tdata),
        .s00_axis_tuser(s00_axis_tuser[11:0]),
        .s00_axis_tvalid(s00_axis_tvalid),
        .\y_reg[23]_i_1__0 (\_DOUT_2[13]_27 ),
        .\y_reg[23]_i_1__0_0 (\_DOUT_2[12]_25 ));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_13 \genblk1[1].rams_sp_rf_rst_inst0 
       (.Q(\EN1_reg_n_0_[1] ),
        .addr({\genblk1[15].rams_sp_rf_rst_inst0_n_24 ,\genblk1[15].rams_sp_rf_rst_inst0_n_25 ,\genblk1[15].rams_sp_rf_rst_inst0_n_26 ,\genblk1[15].rams_sp_rf_rst_inst0_n_27 ,\genblk1[15].rams_sp_rf_rst_inst0_n_28 ,\genblk1[15].rams_sp_rf_rst_inst0_n_29 ,\genblk1[15].rams_sp_rf_rst_inst0_n_30 ,\genblk1[15].rams_sp_rf_rst_inst0_n_31 ,\genblk1[15].rams_sp_rf_rst_inst0_n_32 ,\genblk1[15].rams_sp_rf_rst_inst0_n_33 ,\genblk1[15].rams_sp_rf_rst_inst0_n_34 }),
        .dout0_out(\_DOUT_1[1]_2 ),
        .m00_axis_aclk(m00_axis_aclk),
        .s00_axis_tdata(s00_axis_tdata),
        .s00_axis_tuser(s00_axis_tuser[11]),
        .s00_axis_tvalid(s00_axis_tvalid));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_14 \genblk1[1].rams_sp_rf_rst_inst1 
       (.Q(\EN2_reg_n_0_[1] ),
        .addr(ADDR2),
        .dout0_out(\_DOUT_2[1]_3 ),
        .m00_axis_aclk(m00_axis_aclk),
        .s00_axis_tdata(s00_axis_tdata),
        .s00_axis_tuser(s00_axis_tuser[11]),
        .s00_axis_tvalid(s00_axis_tvalid));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_15 \genblk1[2].rams_sp_rf_rst_inst0 
       (.Q(\EN1_reg_n_0_[2] ),
        .addr({\genblk1[15].rams_sp_rf_rst_inst0_n_24 ,\genblk1[15].rams_sp_rf_rst_inst0_n_25 ,\genblk1[15].rams_sp_rf_rst_inst0_n_26 ,\genblk1[15].rams_sp_rf_rst_inst0_n_27 ,\genblk1[15].rams_sp_rf_rst_inst0_n_28 ,\genblk1[15].rams_sp_rf_rst_inst0_n_29 ,\genblk1[15].rams_sp_rf_rst_inst0_n_30 ,\genblk1[15].rams_sp_rf_rst_inst0_n_31 ,\genblk1[15].rams_sp_rf_rst_inst0_n_32 ,\genblk1[15].rams_sp_rf_rst_inst0_n_33 ,\genblk1[15].rams_sp_rf_rst_inst0_n_34 }),
        .dout0_out(\_DOUT_1[2]_4 ),
        .m00_axis_aclk(m00_axis_aclk),
        .s00_axis_tdata(s00_axis_tdata),
        .s00_axis_tuser(s00_axis_tuser[11]),
        .s00_axis_tvalid(s00_axis_tvalid));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_16 \genblk1[2].rams_sp_rf_rst_inst1 
       (.Q(\EN2_reg_n_0_[2] ),
        .addr(ADDR2),
        .dout0_out(\_DOUT_2[2]_5 ),
        .m00_axis_aclk(m00_axis_aclk),
        .s00_axis_tdata(s00_axis_tdata),
        .s00_axis_tuser(s00_axis_tuser[11]),
        .s00_axis_tvalid(s00_axis_tvalid));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_17 \genblk1[3].rams_sp_rf_rst_inst0 
       (.D({\genblk1[3].rams_sp_rf_rst_inst0_n_0 ,\genblk1[3].rams_sp_rf_rst_inst0_n_1 ,\genblk1[3].rams_sp_rf_rst_inst0_n_2 ,\genblk1[3].rams_sp_rf_rst_inst0_n_3 ,\genblk1[3].rams_sp_rf_rst_inst0_n_4 ,\genblk1[3].rams_sp_rf_rst_inst0_n_5 ,\genblk1[3].rams_sp_rf_rst_inst0_n_6 ,\genblk1[3].rams_sp_rf_rst_inst0_n_7 ,\genblk1[3].rams_sp_rf_rst_inst0_n_8 ,\genblk1[3].rams_sp_rf_rst_inst0_n_9 ,\genblk1[3].rams_sp_rf_rst_inst0_n_10 ,\genblk1[3].rams_sp_rf_rst_inst0_n_11 ,\genblk1[3].rams_sp_rf_rst_inst0_n_12 ,\genblk1[3].rams_sp_rf_rst_inst0_n_13 ,\genblk1[3].rams_sp_rf_rst_inst0_n_14 ,\genblk1[3].rams_sp_rf_rst_inst0_n_15 ,\genblk1[3].rams_sp_rf_rst_inst0_n_16 ,\genblk1[3].rams_sp_rf_rst_inst0_n_17 ,\genblk1[3].rams_sp_rf_rst_inst0_n_18 ,\genblk1[3].rams_sp_rf_rst_inst0_n_19 ,\genblk1[3].rams_sp_rf_rst_inst0_n_20 ,\genblk1[3].rams_sp_rf_rst_inst0_n_21 ,\genblk1[3].rams_sp_rf_rst_inst0_n_22 ,\genblk1[3].rams_sp_rf_rst_inst0_n_23 }),
        .Q(\EN1_reg_n_0_[3] ),
        .addr({\genblk1[15].rams_sp_rf_rst_inst0_n_24 ,\genblk1[15].rams_sp_rf_rst_inst0_n_25 ,\genblk1[15].rams_sp_rf_rst_inst0_n_26 ,\genblk1[15].rams_sp_rf_rst_inst0_n_27 ,\genblk1[15].rams_sp_rf_rst_inst0_n_28 ,\genblk1[15].rams_sp_rf_rst_inst0_n_29 ,\genblk1[15].rams_sp_rf_rst_inst0_n_30 ,\genblk1[15].rams_sp_rf_rst_inst0_n_31 ,\genblk1[15].rams_sp_rf_rst_inst0_n_32 ,\genblk1[15].rams_sp_rf_rst_inst0_n_33 ,\genblk1[15].rams_sp_rf_rst_inst0_n_34 }),
        .ctrl_mux_out_1(ctrl_mux_out_1),
        .dout0_out(\_DOUT_1[2]_4 ),
        .m00_axis_aclk(m00_axis_aclk),
        .s00_axis_tdata(s00_axis_tdata),
        .s00_axis_tuser(s00_axis_tuser[11]),
        .s00_axis_tvalid(s00_axis_tvalid),
        .\y_reg[0] (\genblk1[11].rams_sp_rf_rst_inst0_n_0 ),
        .\y_reg[0]_0 (\genblk1[15].rams_sp_rf_rst_inst0_n_0 ),
        .\y_reg[0]_i_1_0 (\genblk1[7].rams_sp_rf_rst_inst0_n_0 ),
        .\y_reg[10] (\genblk1[11].rams_sp_rf_rst_inst0_n_10 ),
        .\y_reg[10]_0 (\genblk1[15].rams_sp_rf_rst_inst0_n_10 ),
        .\y_reg[10]_i_1_0 (\genblk1[7].rams_sp_rf_rst_inst0_n_10 ),
        .\y_reg[11] (\genblk1[11].rams_sp_rf_rst_inst0_n_11 ),
        .\y_reg[11]_0 (\genblk1[15].rams_sp_rf_rst_inst0_n_11 ),
        .\y_reg[11]_i_1_0 (\genblk1[7].rams_sp_rf_rst_inst0_n_11 ),
        .\y_reg[12] (\genblk1[11].rams_sp_rf_rst_inst0_n_12 ),
        .\y_reg[12]_0 (\genblk1[15].rams_sp_rf_rst_inst0_n_12 ),
        .\y_reg[12]_i_1_0 (\genblk1[7].rams_sp_rf_rst_inst0_n_12 ),
        .\y_reg[13] (\genblk1[11].rams_sp_rf_rst_inst0_n_13 ),
        .\y_reg[13]_0 (\genblk1[15].rams_sp_rf_rst_inst0_n_13 ),
        .\y_reg[13]_i_1_0 (\genblk1[7].rams_sp_rf_rst_inst0_n_13 ),
        .\y_reg[14] (\genblk1[11].rams_sp_rf_rst_inst0_n_14 ),
        .\y_reg[14]_0 (\genblk1[15].rams_sp_rf_rst_inst0_n_14 ),
        .\y_reg[14]_i_1_0 (\genblk1[7].rams_sp_rf_rst_inst0_n_14 ),
        .\y_reg[15] (\genblk1[11].rams_sp_rf_rst_inst0_n_15 ),
        .\y_reg[15]_0 (\genblk1[15].rams_sp_rf_rst_inst0_n_15 ),
        .\y_reg[15]_i_1_0 (\genblk1[7].rams_sp_rf_rst_inst0_n_15 ),
        .\y_reg[16] (\genblk1[11].rams_sp_rf_rst_inst0_n_16 ),
        .\y_reg[16]_0 (\genblk1[15].rams_sp_rf_rst_inst0_n_16 ),
        .\y_reg[16]_i_1_0 (\genblk1[7].rams_sp_rf_rst_inst0_n_16 ),
        .\y_reg[17] (\genblk1[11].rams_sp_rf_rst_inst0_n_17 ),
        .\y_reg[17]_0 (\genblk1[15].rams_sp_rf_rst_inst0_n_17 ),
        .\y_reg[17]_i_1_0 (\genblk1[7].rams_sp_rf_rst_inst0_n_17 ),
        .\y_reg[18] (\genblk1[11].rams_sp_rf_rst_inst0_n_18 ),
        .\y_reg[18]_0 (\genblk1[15].rams_sp_rf_rst_inst0_n_18 ),
        .\y_reg[18]_i_1_0 (\genblk1[7].rams_sp_rf_rst_inst0_n_18 ),
        .\y_reg[19] (\genblk1[11].rams_sp_rf_rst_inst0_n_19 ),
        .\y_reg[19]_0 (\genblk1[15].rams_sp_rf_rst_inst0_n_19 ),
        .\y_reg[19]_i_1_0 (\genblk1[7].rams_sp_rf_rst_inst0_n_19 ),
        .\y_reg[1] (\genblk1[11].rams_sp_rf_rst_inst0_n_1 ),
        .\y_reg[1]_0 (\genblk1[15].rams_sp_rf_rst_inst0_n_1 ),
        .\y_reg[1]_i_1_0 (\genblk1[7].rams_sp_rf_rst_inst0_n_1 ),
        .\y_reg[20] (\genblk1[11].rams_sp_rf_rst_inst0_n_20 ),
        .\y_reg[20]_0 (\genblk1[15].rams_sp_rf_rst_inst0_n_20 ),
        .\y_reg[20]_i_1_0 (\genblk1[7].rams_sp_rf_rst_inst0_n_20 ),
        .\y_reg[21] (\genblk1[11].rams_sp_rf_rst_inst0_n_21 ),
        .\y_reg[21]_0 (\genblk1[15].rams_sp_rf_rst_inst0_n_21 ),
        .\y_reg[21]_i_1_0 (\genblk1[7].rams_sp_rf_rst_inst0_n_21 ),
        .\y_reg[22] (\genblk1[11].rams_sp_rf_rst_inst0_n_22 ),
        .\y_reg[22]_0 (\genblk1[15].rams_sp_rf_rst_inst0_n_22 ),
        .\y_reg[22]_i_1_0 (\genblk1[7].rams_sp_rf_rst_inst0_n_22 ),
        .\y_reg[23] (\genblk1[11].rams_sp_rf_rst_inst0_n_23 ),
        .\y_reg[23]_0 (\genblk1[15].rams_sp_rf_rst_inst0_n_23 ),
        .\y_reg[23]_i_1_0 (\genblk1[7].rams_sp_rf_rst_inst0_n_23 ),
        .\y_reg[23]_i_3_0 (\_DOUT_1[1]_2 ),
        .\y_reg[23]_i_3_1 (\_DOUT_1[0]_0 ),
        .\y_reg[2] (\genblk1[11].rams_sp_rf_rst_inst0_n_2 ),
        .\y_reg[2]_0 (\genblk1[15].rams_sp_rf_rst_inst0_n_2 ),
        .\y_reg[2]_i_1_0 (\genblk1[7].rams_sp_rf_rst_inst0_n_2 ),
        .\y_reg[3] (\genblk1[11].rams_sp_rf_rst_inst0_n_3 ),
        .\y_reg[3]_0 (\genblk1[15].rams_sp_rf_rst_inst0_n_3 ),
        .\y_reg[3]_i_1_0 (\genblk1[7].rams_sp_rf_rst_inst0_n_3 ),
        .\y_reg[4] (\genblk1[11].rams_sp_rf_rst_inst0_n_4 ),
        .\y_reg[4]_0 (\genblk1[15].rams_sp_rf_rst_inst0_n_4 ),
        .\y_reg[4]_i_1_0 (\genblk1[7].rams_sp_rf_rst_inst0_n_4 ),
        .\y_reg[5] (\genblk1[11].rams_sp_rf_rst_inst0_n_5 ),
        .\y_reg[5]_0 (\genblk1[15].rams_sp_rf_rst_inst0_n_5 ),
        .\y_reg[5]_i_1_0 (\genblk1[7].rams_sp_rf_rst_inst0_n_5 ),
        .\y_reg[6] (\genblk1[11].rams_sp_rf_rst_inst0_n_6 ),
        .\y_reg[6]_0 (\genblk1[15].rams_sp_rf_rst_inst0_n_6 ),
        .\y_reg[6]_i_1_0 (\genblk1[7].rams_sp_rf_rst_inst0_n_6 ),
        .\y_reg[7] (\genblk1[11].rams_sp_rf_rst_inst0_n_7 ),
        .\y_reg[7]_0 (\genblk1[15].rams_sp_rf_rst_inst0_n_7 ),
        .\y_reg[7]_i_1_0 (\genblk1[7].rams_sp_rf_rst_inst0_n_7 ),
        .\y_reg[8] (\genblk1[11].rams_sp_rf_rst_inst0_n_8 ),
        .\y_reg[8]_0 (\genblk1[15].rams_sp_rf_rst_inst0_n_8 ),
        .\y_reg[8]_i_1_0 (\genblk1[7].rams_sp_rf_rst_inst0_n_8 ),
        .\y_reg[9] (\genblk1[11].rams_sp_rf_rst_inst0_n_9 ),
        .\y_reg[9]_0 (\genblk1[15].rams_sp_rf_rst_inst0_n_9 ),
        .\y_reg[9]_i_1_0 (\genblk1[7].rams_sp_rf_rst_inst0_n_9 ));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_18 \genblk1[3].rams_sp_rf_rst_inst1 
       (.D({\genblk1[3].rams_sp_rf_rst_inst1_n_0 ,\genblk1[3].rams_sp_rf_rst_inst1_n_1 ,\genblk1[3].rams_sp_rf_rst_inst1_n_2 ,\genblk1[3].rams_sp_rf_rst_inst1_n_3 ,\genblk1[3].rams_sp_rf_rst_inst1_n_4 ,\genblk1[3].rams_sp_rf_rst_inst1_n_5 ,\genblk1[3].rams_sp_rf_rst_inst1_n_6 ,\genblk1[3].rams_sp_rf_rst_inst1_n_7 ,\genblk1[3].rams_sp_rf_rst_inst1_n_8 ,\genblk1[3].rams_sp_rf_rst_inst1_n_9 ,\genblk1[3].rams_sp_rf_rst_inst1_n_10 ,\genblk1[3].rams_sp_rf_rst_inst1_n_11 ,\genblk1[3].rams_sp_rf_rst_inst1_n_12 ,\genblk1[3].rams_sp_rf_rst_inst1_n_13 ,\genblk1[3].rams_sp_rf_rst_inst1_n_14 ,\genblk1[3].rams_sp_rf_rst_inst1_n_15 ,\genblk1[3].rams_sp_rf_rst_inst1_n_16 ,\genblk1[3].rams_sp_rf_rst_inst1_n_17 ,\genblk1[3].rams_sp_rf_rst_inst1_n_18 ,\genblk1[3].rams_sp_rf_rst_inst1_n_19 ,\genblk1[3].rams_sp_rf_rst_inst1_n_20 ,\genblk1[3].rams_sp_rf_rst_inst1_n_21 ,\genblk1[3].rams_sp_rf_rst_inst1_n_22 ,\genblk1[3].rams_sp_rf_rst_inst1_n_23 }),
        .Q(\EN2_reg_n_0_[3] ),
        .addr(ADDR2),
        .ctrl_mux_out_2(ctrl_mux_out_2),
        .dout0_out(\_DOUT_2[2]_5 ),
        .m00_axis_aclk(m00_axis_aclk),
        .s00_axis_tdata(s00_axis_tdata),
        .s00_axis_tuser(s00_axis_tuser[11]),
        .s00_axis_tvalid(s00_axis_tvalid),
        .\y_reg[0] (\genblk1[11].rams_sp_rf_rst_inst1_n_0 ),
        .\y_reg[0]_0 (\genblk1[15].rams_sp_rf_rst_inst1_n_0 ),
        .\y_reg[0]_i_1__0_0 (\genblk1[7].rams_sp_rf_rst_inst1_n_0 ),
        .\y_reg[10] (\genblk1[11].rams_sp_rf_rst_inst1_n_10 ),
        .\y_reg[10]_0 (\genblk1[15].rams_sp_rf_rst_inst1_n_10 ),
        .\y_reg[10]_i_1__0_0 (\genblk1[7].rams_sp_rf_rst_inst1_n_10 ),
        .\y_reg[11] (\genblk1[11].rams_sp_rf_rst_inst1_n_11 ),
        .\y_reg[11]_0 (\genblk1[15].rams_sp_rf_rst_inst1_n_11 ),
        .\y_reg[11]_i_1__0_0 (\genblk1[7].rams_sp_rf_rst_inst1_n_11 ),
        .\y_reg[12] (\genblk1[11].rams_sp_rf_rst_inst1_n_12 ),
        .\y_reg[12]_0 (\genblk1[15].rams_sp_rf_rst_inst1_n_12 ),
        .\y_reg[12]_i_1__0_0 (\genblk1[7].rams_sp_rf_rst_inst1_n_12 ),
        .\y_reg[13] (\genblk1[11].rams_sp_rf_rst_inst1_n_13 ),
        .\y_reg[13]_0 (\genblk1[15].rams_sp_rf_rst_inst1_n_13 ),
        .\y_reg[13]_i_1__0_0 (\genblk1[7].rams_sp_rf_rst_inst1_n_13 ),
        .\y_reg[14] (\genblk1[11].rams_sp_rf_rst_inst1_n_14 ),
        .\y_reg[14]_0 (\genblk1[15].rams_sp_rf_rst_inst1_n_14 ),
        .\y_reg[14]_i_1__0_0 (\genblk1[7].rams_sp_rf_rst_inst1_n_14 ),
        .\y_reg[15] (\genblk1[11].rams_sp_rf_rst_inst1_n_15 ),
        .\y_reg[15]_0 (\genblk1[15].rams_sp_rf_rst_inst1_n_15 ),
        .\y_reg[15]_i_1__0_0 (\genblk1[7].rams_sp_rf_rst_inst1_n_15 ),
        .\y_reg[16] (\genblk1[11].rams_sp_rf_rst_inst1_n_16 ),
        .\y_reg[16]_0 (\genblk1[15].rams_sp_rf_rst_inst1_n_16 ),
        .\y_reg[16]_i_1__0_0 (\genblk1[7].rams_sp_rf_rst_inst1_n_16 ),
        .\y_reg[17] (\genblk1[11].rams_sp_rf_rst_inst1_n_17 ),
        .\y_reg[17]_0 (\genblk1[15].rams_sp_rf_rst_inst1_n_17 ),
        .\y_reg[17]_i_1__0_0 (\genblk1[7].rams_sp_rf_rst_inst1_n_17 ),
        .\y_reg[18] (\genblk1[11].rams_sp_rf_rst_inst1_n_18 ),
        .\y_reg[18]_0 (\genblk1[15].rams_sp_rf_rst_inst1_n_18 ),
        .\y_reg[18]_i_1__0_0 (\genblk1[7].rams_sp_rf_rst_inst1_n_18 ),
        .\y_reg[19] (\genblk1[11].rams_sp_rf_rst_inst1_n_19 ),
        .\y_reg[19]_0 (\genblk1[15].rams_sp_rf_rst_inst1_n_19 ),
        .\y_reg[19]_i_1__0_0 (\genblk1[7].rams_sp_rf_rst_inst1_n_19 ),
        .\y_reg[1] (\genblk1[11].rams_sp_rf_rst_inst1_n_1 ),
        .\y_reg[1]_0 (\genblk1[15].rams_sp_rf_rst_inst1_n_1 ),
        .\y_reg[1]_i_1__0_0 (\genblk1[7].rams_sp_rf_rst_inst1_n_1 ),
        .\y_reg[20] (\genblk1[11].rams_sp_rf_rst_inst1_n_20 ),
        .\y_reg[20]_0 (\genblk1[15].rams_sp_rf_rst_inst1_n_20 ),
        .\y_reg[20]_i_1__0_0 (\genblk1[7].rams_sp_rf_rst_inst1_n_20 ),
        .\y_reg[21] (\genblk1[11].rams_sp_rf_rst_inst1_n_21 ),
        .\y_reg[21]_0 (\genblk1[15].rams_sp_rf_rst_inst1_n_21 ),
        .\y_reg[21]_i_1__0_0 (\genblk1[7].rams_sp_rf_rst_inst1_n_21 ),
        .\y_reg[22] (\genblk1[11].rams_sp_rf_rst_inst1_n_22 ),
        .\y_reg[22]_0 (\genblk1[15].rams_sp_rf_rst_inst1_n_22 ),
        .\y_reg[22]_i_1__0_0 (\genblk1[7].rams_sp_rf_rst_inst1_n_22 ),
        .\y_reg[23] (\genblk1[11].rams_sp_rf_rst_inst1_n_23 ),
        .\y_reg[23]_0 (\genblk1[15].rams_sp_rf_rst_inst1_n_23 ),
        .\y_reg[23]_i_1__0_0 (\genblk1[7].rams_sp_rf_rst_inst1_n_23 ),
        .\y_reg[23]_i_3__0_0 (\_DOUT_2[1]_3 ),
        .\y_reg[23]_i_3__0_1 (\_DOUT_2[0]_1 ),
        .\y_reg[2] (\genblk1[11].rams_sp_rf_rst_inst1_n_2 ),
        .\y_reg[2]_0 (\genblk1[15].rams_sp_rf_rst_inst1_n_2 ),
        .\y_reg[2]_i_1__0_0 (\genblk1[7].rams_sp_rf_rst_inst1_n_2 ),
        .\y_reg[3] (\genblk1[11].rams_sp_rf_rst_inst1_n_3 ),
        .\y_reg[3]_0 (\genblk1[15].rams_sp_rf_rst_inst1_n_3 ),
        .\y_reg[3]_i_1__0_0 (\genblk1[7].rams_sp_rf_rst_inst1_n_3 ),
        .\y_reg[4] (\genblk1[11].rams_sp_rf_rst_inst1_n_4 ),
        .\y_reg[4]_0 (\genblk1[15].rams_sp_rf_rst_inst1_n_4 ),
        .\y_reg[4]_i_1__0_0 (\genblk1[7].rams_sp_rf_rst_inst1_n_4 ),
        .\y_reg[5] (\genblk1[11].rams_sp_rf_rst_inst1_n_5 ),
        .\y_reg[5]_0 (\genblk1[15].rams_sp_rf_rst_inst1_n_5 ),
        .\y_reg[5]_i_1__0_0 (\genblk1[7].rams_sp_rf_rst_inst1_n_5 ),
        .\y_reg[6] (\genblk1[11].rams_sp_rf_rst_inst1_n_6 ),
        .\y_reg[6]_0 (\genblk1[15].rams_sp_rf_rst_inst1_n_6 ),
        .\y_reg[6]_i_1__0_0 (\genblk1[7].rams_sp_rf_rst_inst1_n_6 ),
        .\y_reg[7] (\genblk1[11].rams_sp_rf_rst_inst1_n_7 ),
        .\y_reg[7]_0 (\genblk1[15].rams_sp_rf_rst_inst1_n_7 ),
        .\y_reg[7]_i_1__0_0 (\genblk1[7].rams_sp_rf_rst_inst1_n_7 ),
        .\y_reg[8] (\genblk1[11].rams_sp_rf_rst_inst1_n_8 ),
        .\y_reg[8]_0 (\genblk1[15].rams_sp_rf_rst_inst1_n_8 ),
        .\y_reg[8]_i_1__0_0 (\genblk1[7].rams_sp_rf_rst_inst1_n_8 ),
        .\y_reg[9] (\genblk1[11].rams_sp_rf_rst_inst1_n_9 ),
        .\y_reg[9]_0 (\genblk1[15].rams_sp_rf_rst_inst1_n_9 ),
        .\y_reg[9]_i_1__0_0 (\genblk1[7].rams_sp_rf_rst_inst1_n_9 ));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_19 \genblk1[4].rams_sp_rf_rst_inst0 
       (.Q(\EN1_reg_n_0_[4] ),
        .addr({\genblk1[15].rams_sp_rf_rst_inst0_n_24 ,\genblk1[15].rams_sp_rf_rst_inst0_n_25 ,\genblk1[15].rams_sp_rf_rst_inst0_n_26 ,\genblk1[15].rams_sp_rf_rst_inst0_n_27 ,\genblk1[15].rams_sp_rf_rst_inst0_n_28 ,\genblk1[15].rams_sp_rf_rst_inst0_n_29 ,\genblk1[15].rams_sp_rf_rst_inst0_n_30 ,\genblk1[15].rams_sp_rf_rst_inst0_n_31 ,\genblk1[15].rams_sp_rf_rst_inst0_n_32 ,\genblk1[15].rams_sp_rf_rst_inst0_n_33 ,\genblk1[15].rams_sp_rf_rst_inst0_n_34 }),
        .dout0_out(\_DOUT_1[4]_8 ),
        .m00_axis_aclk(m00_axis_aclk),
        .s00_axis_tdata(s00_axis_tdata),
        .s00_axis_tuser(s00_axis_tuser[11]),
        .s00_axis_tvalid(s00_axis_tvalid));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_20 \genblk1[4].rams_sp_rf_rst_inst1 
       (.Q(\EN2_reg_n_0_[4] ),
        .addr(ADDR2),
        .dout0_out(\_DOUT_2[4]_9 ),
        .m00_axis_aclk(m00_axis_aclk),
        .s00_axis_tdata(s00_axis_tdata),
        .s00_axis_tuser(s00_axis_tuser[11]),
        .s00_axis_tvalid(s00_axis_tvalid));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_21 \genblk1[5].rams_sp_rf_rst_inst0 
       (.Q(\EN1_reg_n_0_[5] ),
        .addr({\genblk1[15].rams_sp_rf_rst_inst0_n_24 ,\genblk1[15].rams_sp_rf_rst_inst0_n_25 ,\genblk1[15].rams_sp_rf_rst_inst0_n_26 ,\genblk1[15].rams_sp_rf_rst_inst0_n_27 ,\genblk1[15].rams_sp_rf_rst_inst0_n_28 ,\genblk1[15].rams_sp_rf_rst_inst0_n_29 ,\genblk1[15].rams_sp_rf_rst_inst0_n_30 ,\genblk1[15].rams_sp_rf_rst_inst0_n_31 ,\genblk1[15].rams_sp_rf_rst_inst0_n_32 ,\genblk1[15].rams_sp_rf_rst_inst0_n_33 ,\genblk1[15].rams_sp_rf_rst_inst0_n_34 }),
        .dout0_out(\_DOUT_1[5]_10 ),
        .m00_axis_aclk(m00_axis_aclk),
        .s00_axis_tdata(s00_axis_tdata),
        .s00_axis_tuser(s00_axis_tuser[11]),
        .s00_axis_tvalid(s00_axis_tvalid));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_22 \genblk1[5].rams_sp_rf_rst_inst1 
       (.Q(\EN2_reg_n_0_[5] ),
        .addr(ADDR2),
        .dout0_out(\_DOUT_2[5]_11 ),
        .m00_axis_aclk(m00_axis_aclk),
        .s00_axis_tdata(s00_axis_tdata),
        .s00_axis_tuser(s00_axis_tuser[11]),
        .s00_axis_tvalid(s00_axis_tvalid));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_23 \genblk1[6].rams_sp_rf_rst_inst0 
       (.Q(\EN1_reg_n_0_[6] ),
        .addr({\genblk1[15].rams_sp_rf_rst_inst0_n_24 ,\genblk1[15].rams_sp_rf_rst_inst0_n_25 ,\genblk1[15].rams_sp_rf_rst_inst0_n_26 ,\genblk1[15].rams_sp_rf_rst_inst0_n_27 ,\genblk1[15].rams_sp_rf_rst_inst0_n_28 ,\genblk1[15].rams_sp_rf_rst_inst0_n_29 ,\genblk1[15].rams_sp_rf_rst_inst0_n_30 ,\genblk1[15].rams_sp_rf_rst_inst0_n_31 ,\genblk1[15].rams_sp_rf_rst_inst0_n_32 ,\genblk1[15].rams_sp_rf_rst_inst0_n_33 ,\genblk1[15].rams_sp_rf_rst_inst0_n_34 }),
        .dout0_out(\_DOUT_1[6]_12 ),
        .m00_axis_aclk(m00_axis_aclk),
        .s00_axis_tdata(s00_axis_tdata),
        .s00_axis_tuser(s00_axis_tuser[11]),
        .s00_axis_tvalid(s00_axis_tvalid));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_24 \genblk1[6].rams_sp_rf_rst_inst1 
       (.Q(\EN2_reg_n_0_[6] ),
        .addr(ADDR2),
        .dout0_out(\_DOUT_2[6]_13 ),
        .m00_axis_aclk(m00_axis_aclk),
        .s00_axis_tdata(s00_axis_tdata),
        .s00_axis_tuser(s00_axis_tuser[11]),
        .s00_axis_tvalid(s00_axis_tvalid));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_25 \genblk1[7].rams_sp_rf_rst_inst0 
       (.Q(\EN1_reg_n_0_[7] ),
        .addr({\genblk1[15].rams_sp_rf_rst_inst0_n_24 ,\genblk1[15].rams_sp_rf_rst_inst0_n_25 ,\genblk1[15].rams_sp_rf_rst_inst0_n_26 ,\genblk1[15].rams_sp_rf_rst_inst0_n_27 ,\genblk1[15].rams_sp_rf_rst_inst0_n_28 ,\genblk1[15].rams_sp_rf_rst_inst0_n_29 ,\genblk1[15].rams_sp_rf_rst_inst0_n_30 ,\genblk1[15].rams_sp_rf_rst_inst0_n_31 ,\genblk1[15].rams_sp_rf_rst_inst0_n_32 ,\genblk1[15].rams_sp_rf_rst_inst0_n_33 ,\genblk1[15].rams_sp_rf_rst_inst0_n_34 }),
        .ctrl_mux_out_1(ctrl_mux_out_1[1:0]),
        .dout0_out(\_DOUT_1[6]_12 ),
        .m00_axis_aclk(m00_axis_aclk),
        .ram_reg_0_0(\genblk1[7].rams_sp_rf_rst_inst0_n_0 ),
        .ram_reg_0_1(\genblk1[7].rams_sp_rf_rst_inst0_n_1 ),
        .ram_reg_0_10(\genblk1[7].rams_sp_rf_rst_inst0_n_10 ),
        .ram_reg_0_11(\genblk1[7].rams_sp_rf_rst_inst0_n_11 ),
        .ram_reg_0_12(\genblk1[7].rams_sp_rf_rst_inst0_n_12 ),
        .ram_reg_0_13(\genblk1[7].rams_sp_rf_rst_inst0_n_13 ),
        .ram_reg_0_14(\genblk1[7].rams_sp_rf_rst_inst0_n_14 ),
        .ram_reg_0_15(\genblk1[7].rams_sp_rf_rst_inst0_n_15 ),
        .ram_reg_0_16(\genblk1[7].rams_sp_rf_rst_inst0_n_16 ),
        .ram_reg_0_17(\genblk1[7].rams_sp_rf_rst_inst0_n_17 ),
        .ram_reg_0_2(\genblk1[7].rams_sp_rf_rst_inst0_n_2 ),
        .ram_reg_0_3(\genblk1[7].rams_sp_rf_rst_inst0_n_3 ),
        .ram_reg_0_4(\genblk1[7].rams_sp_rf_rst_inst0_n_4 ),
        .ram_reg_0_5(\genblk1[7].rams_sp_rf_rst_inst0_n_5 ),
        .ram_reg_0_6(\genblk1[7].rams_sp_rf_rst_inst0_n_6 ),
        .ram_reg_0_7(\genblk1[7].rams_sp_rf_rst_inst0_n_7 ),
        .ram_reg_0_8(\genblk1[7].rams_sp_rf_rst_inst0_n_8 ),
        .ram_reg_0_9(\genblk1[7].rams_sp_rf_rst_inst0_n_9 ),
        .ram_reg_1_0(\genblk1[7].rams_sp_rf_rst_inst0_n_18 ),
        .ram_reg_1_1(\genblk1[7].rams_sp_rf_rst_inst0_n_19 ),
        .ram_reg_1_2(\genblk1[7].rams_sp_rf_rst_inst0_n_20 ),
        .ram_reg_1_3(\genblk1[7].rams_sp_rf_rst_inst0_n_21 ),
        .ram_reg_1_4(\genblk1[7].rams_sp_rf_rst_inst0_n_22 ),
        .ram_reg_1_5(\genblk1[7].rams_sp_rf_rst_inst0_n_23 ),
        .s00_axis_tdata(s00_axis_tdata),
        .s00_axis_tuser(s00_axis_tuser[11]),
        .s00_axis_tvalid(s00_axis_tvalid),
        .\y_reg[23]_i_3 (\_DOUT_1[5]_10 ),
        .\y_reg[23]_i_3_0 (\_DOUT_1[4]_8 ));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_26 \genblk1[7].rams_sp_rf_rst_inst1 
       (.Q(\EN2_reg_n_0_[7] ),
        .addr(ADDR2),
        .ctrl_mux_out_2(ctrl_mux_out_2[1:0]),
        .dout0_out(\_DOUT_2[6]_13 ),
        .m00_axis_aclk(m00_axis_aclk),
        .ram_reg_0_0(\genblk1[7].rams_sp_rf_rst_inst1_n_0 ),
        .ram_reg_0_1(\genblk1[7].rams_sp_rf_rst_inst1_n_1 ),
        .ram_reg_0_10(\genblk1[7].rams_sp_rf_rst_inst1_n_10 ),
        .ram_reg_0_11(\genblk1[7].rams_sp_rf_rst_inst1_n_11 ),
        .ram_reg_0_12(\genblk1[7].rams_sp_rf_rst_inst1_n_12 ),
        .ram_reg_0_13(\genblk1[7].rams_sp_rf_rst_inst1_n_13 ),
        .ram_reg_0_14(\genblk1[7].rams_sp_rf_rst_inst1_n_14 ),
        .ram_reg_0_15(\genblk1[7].rams_sp_rf_rst_inst1_n_15 ),
        .ram_reg_0_16(\genblk1[7].rams_sp_rf_rst_inst1_n_16 ),
        .ram_reg_0_17(\genblk1[7].rams_sp_rf_rst_inst1_n_17 ),
        .ram_reg_0_2(\genblk1[7].rams_sp_rf_rst_inst1_n_2 ),
        .ram_reg_0_3(\genblk1[7].rams_sp_rf_rst_inst1_n_3 ),
        .ram_reg_0_4(\genblk1[7].rams_sp_rf_rst_inst1_n_4 ),
        .ram_reg_0_5(\genblk1[7].rams_sp_rf_rst_inst1_n_5 ),
        .ram_reg_0_6(\genblk1[7].rams_sp_rf_rst_inst1_n_6 ),
        .ram_reg_0_7(\genblk1[7].rams_sp_rf_rst_inst1_n_7 ),
        .ram_reg_0_8(\genblk1[7].rams_sp_rf_rst_inst1_n_8 ),
        .ram_reg_0_9(\genblk1[7].rams_sp_rf_rst_inst1_n_9 ),
        .ram_reg_1_0(\genblk1[7].rams_sp_rf_rst_inst1_n_18 ),
        .ram_reg_1_1(\genblk1[7].rams_sp_rf_rst_inst1_n_19 ),
        .ram_reg_1_2(\genblk1[7].rams_sp_rf_rst_inst1_n_20 ),
        .ram_reg_1_3(\genblk1[7].rams_sp_rf_rst_inst1_n_21 ),
        .ram_reg_1_4(\genblk1[7].rams_sp_rf_rst_inst1_n_22 ),
        .ram_reg_1_5(\genblk1[7].rams_sp_rf_rst_inst1_n_23 ),
        .s00_axis_tdata(s00_axis_tdata),
        .s00_axis_tuser(s00_axis_tuser[11]),
        .s00_axis_tvalid(s00_axis_tvalid),
        .\y_reg[23]_i_3__0 (\_DOUT_2[5]_11 ),
        .\y_reg[23]_i_3__0_0 (\_DOUT_2[4]_9 ));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_27 \genblk1[8].rams_sp_rf_rst_inst0 
       (.Q(\EN1_reg_n_0_[8] ),
        .addr({\genblk1[15].rams_sp_rf_rst_inst0_n_24 ,\genblk1[15].rams_sp_rf_rst_inst0_n_25 ,\genblk1[15].rams_sp_rf_rst_inst0_n_26 ,\genblk1[15].rams_sp_rf_rst_inst0_n_27 ,\genblk1[15].rams_sp_rf_rst_inst0_n_28 ,\genblk1[15].rams_sp_rf_rst_inst0_n_29 ,\genblk1[15].rams_sp_rf_rst_inst0_n_30 ,\genblk1[15].rams_sp_rf_rst_inst0_n_31 ,\genblk1[15].rams_sp_rf_rst_inst0_n_32 ,\genblk1[15].rams_sp_rf_rst_inst0_n_33 ,\genblk1[15].rams_sp_rf_rst_inst0_n_34 }),
        .dout0_out(\_DOUT_1[8]_16 ),
        .m00_axis_aclk(m00_axis_aclk),
        .s00_axis_tdata(s00_axis_tdata),
        .s00_axis_tuser(s00_axis_tuser[11]),
        .s00_axis_tvalid(s00_axis_tvalid));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_28 \genblk1[8].rams_sp_rf_rst_inst1 
       (.Q(\EN2_reg_n_0_[8] ),
        .addr(ADDR2),
        .dout0_out(\_DOUT_2[8]_17 ),
        .m00_axis_aclk(m00_axis_aclk),
        .s00_axis_tdata(s00_axis_tdata),
        .s00_axis_tuser(s00_axis_tuser[11]),
        .s00_axis_tvalid(s00_axis_tvalid));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_29 \genblk1[9].rams_sp_rf_rst_inst0 
       (.Q(\EN1_reg_n_0_[9] ),
        .addr({\genblk1[15].rams_sp_rf_rst_inst0_n_24 ,\genblk1[15].rams_sp_rf_rst_inst0_n_25 ,\genblk1[15].rams_sp_rf_rst_inst0_n_26 ,\genblk1[15].rams_sp_rf_rst_inst0_n_27 ,\genblk1[15].rams_sp_rf_rst_inst0_n_28 ,\genblk1[15].rams_sp_rf_rst_inst0_n_29 ,\genblk1[15].rams_sp_rf_rst_inst0_n_30 ,\genblk1[15].rams_sp_rf_rst_inst0_n_31 ,\genblk1[15].rams_sp_rf_rst_inst0_n_32 ,\genblk1[15].rams_sp_rf_rst_inst0_n_33 ,\genblk1[15].rams_sp_rf_rst_inst0_n_34 }),
        .dout0_out(\_DOUT_1[9]_18 ),
        .m00_axis_aclk(m00_axis_aclk),
        .s00_axis_tdata(s00_axis_tdata),
        .s00_axis_tuser(s00_axis_tuser[11]),
        .s00_axis_tvalid(s00_axis_tvalid));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_30 \genblk1[9].rams_sp_rf_rst_inst1 
       (.Q(\EN2_reg_n_0_[9] ),
        .addr(ADDR2),
        .dout0_out(\_DOUT_2[9]_19 ),
        .m00_axis_aclk(m00_axis_aclk),
        .s00_axis_tdata(s00_axis_tdata),
        .s00_axis_tuser(s00_axis_tuser[11]),
        .s00_axis_tvalid(s00_axis_tvalid));
  LUT6 #(
    .INIT(64'hFF08080808080808)) 
    locked_EN1_i_1
       (.I0(\EN1[15]_i_3_n_0 ),
        .I1(locked_EN1_i_2_n_0),
        .I2(\adr_read_reorder_data_mem1[10]_i_4_n_0 ),
        .I3(locked_EN1_reg_n_0),
        .I4(s00_axis_tuser[11]),
        .I5(s00_axis_aresetn),
        .O(locked_EN1_i_1_n_0));
  LUT6 #(
    .INIT(64'h0080000000000000)) 
    locked_EN1_i_2
       (.I0(\adr_read_reorder_data_mem1_reg_n_0_[9] ),
        .I1(\adr_read_reorder_data_mem1_reg_n_0_[10] ),
        .I2(\adr_read_reorder_data_mem1_reg_n_0_[8] ),
        .I3(\adr_read_reorder_data_mem1_reg_n_0_[7] ),
        .I4(\active_mem1_in_read_state_reg_n_0_[2] ),
        .I5(\active_mem1_in_read_state_reg_n_0_[1] ),
        .O(locked_EN1_i_2_n_0));
  FDRE #(
    .INIT(1'b0)) 
    locked_EN1_reg
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(locked_EN1_i_1_n_0),
        .Q(locked_EN1_reg_n_0),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h7077707070707070)) 
    locked_EN2_i_1
       (.I0(s00_axis_tuser[11]),
        .I1(s00_axis_aresetn),
        .I2(locked_EN2_reg_n_0),
        .I3(\adr_read_reorder_data_mem2[10]_i_4_n_0 ),
        .I4(locked_EN2_i_2_n_0),
        .I5(\EN2[15]_i_6_n_0 ),
        .O(locked_EN2_i_1_n_0));
  LUT6 #(
    .INIT(64'h0080000000000000)) 
    locked_EN2_i_2
       (.I0(\adr_read_reorder_data_mem2_reg_n_0_[9] ),
        .I1(\adr_read_reorder_data_mem2_reg_n_0_[10] ),
        .I2(\adr_read_reorder_data_mem2_reg_n_0_[8] ),
        .I3(\adr_read_reorder_data_mem2_reg_n_0_[7] ),
        .I4(active_mem2_in_read_state_reg[2]),
        .I5(active_mem2_in_read_state_reg[1]),
        .O(locked_EN2_i_2_n_0));
  FDRE #(
    .INIT(1'b0)) 
    locked_EN2_reg
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(locked_EN2_i_1_n_0),
        .Q(locked_EN2_reg_n_0),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h0000000200000000)) 
    m00_axis_tlast_INST_0
       (.I0(m00_axis_tlast_INST_0_i_1_n_0),
        .I1(cnt_last_reg[1]),
        .I2(cnt_last_reg[0]),
        .I3(cnt_last_reg[3]),
        .I4(cnt_last_reg[2]),
        .I5(m00_axis_tlast_INST_0_i_2_n_0),
        .O(m00_axis_tlast));
  LUT6 #(
    .INIT(64'h0000000000000010)) 
    m00_axis_tlast_INST_0_i_1
       (.I0(cnt_last_reg[12]),
        .I1(cnt_last_reg[13]),
        .I2(cnt_last_reg[10]),
        .I3(cnt_last_reg[11]),
        .I4(cnt_last_reg[15]),
        .I5(cnt_last_reg[14]),
        .O(m00_axis_tlast_INST_0_i_1_n_0));
  LUT6 #(
    .INIT(64'h0002000000000000)) 
    m00_axis_tlast_INST_0_i_2
       (.I0(cnt_last_reg[7]),
        .I1(cnt_last_reg[6]),
        .I2(cnt_last_reg[4]),
        .I3(cnt_last_reg[5]),
        .I4(cnt_last_reg[9]),
        .I5(cnt_last_reg[8]),
        .O(m00_axis_tlast_INST_0_i_2_n_0));
  LUT6 #(
    .INIT(64'hFFFF008000800080)) 
    m00_axis_tvalid_r_i_1
       (.I0(m00_axis_tvalid_r_i_2_n_0),
        .I1(s00_axis_tuser[11]),
        .I2(s00_axis_aresetn),
        .I3(locked_EN1_reg_n_0),
        .I4(m00_axis_tvalid_r_i_3_n_0),
        .I5(m00_axis_tvalid_r_i_4_n_0),
        .O(m00_axis_tvalid_r_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT4 #(
    .INIT(16'h7FFF)) 
    m00_axis_tvalid_r_i_2
       (.I0(\adr_read_reorder_data_mem1_reg_n_0_[9] ),
        .I1(\adr_read_reorder_data_mem1_reg_n_0_[7] ),
        .I2(\adr_read_reorder_data_mem1_reg_n_0_[8] ),
        .I3(\adr_read_reorder_data_mem1_reg_n_0_[10] ),
        .O(m00_axis_tvalid_r_i_2_n_0));
  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT4 #(
    .INIT(16'h7FFF)) 
    m00_axis_tvalid_r_i_3
       (.I0(\adr_read_reorder_data_mem2_reg_n_0_[9] ),
        .I1(\adr_read_reorder_data_mem2_reg_n_0_[7] ),
        .I2(\adr_read_reorder_data_mem2_reg_n_0_[8] ),
        .I3(\adr_read_reorder_data_mem2_reg_n_0_[10] ),
        .O(m00_axis_tvalid_r_i_3_n_0));
  LUT6 #(
    .INIT(64'h1010101010101000)) 
    m00_axis_tvalid_r_i_4
       (.I0(locked_EN2_reg_n_0),
        .I1(s00_axis_tuser[11]),
        .I2(s00_axis_aresetn),
        .I3(s00_axis_tuser[12]),
        .I4(s00_axis_tuser[13]),
        .I5(s00_axis_tuser[14]),
        .O(m00_axis_tvalid_r_i_4_n_0));
  FDRE m00_axis_tvalid_r_reg
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(m00_axis_tvalid_r_i_1_n_0),
        .Q(m00_axis_tvalid),
        .R(1'b0));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_multiplexer multiplexer2_inst
       (.D({\genblk1[3].rams_sp_rf_rst_inst1_n_0 ,\genblk1[3].rams_sp_rf_rst_inst1_n_1 ,\genblk1[3].rams_sp_rf_rst_inst1_n_2 ,\genblk1[3].rams_sp_rf_rst_inst1_n_3 ,\genblk1[3].rams_sp_rf_rst_inst1_n_4 ,\genblk1[3].rams_sp_rf_rst_inst1_n_5 ,\genblk1[3].rams_sp_rf_rst_inst1_n_6 ,\genblk1[3].rams_sp_rf_rst_inst1_n_7 ,\genblk1[3].rams_sp_rf_rst_inst1_n_8 ,\genblk1[3].rams_sp_rf_rst_inst1_n_9 ,\genblk1[3].rams_sp_rf_rst_inst1_n_10 ,\genblk1[3].rams_sp_rf_rst_inst1_n_11 ,\genblk1[3].rams_sp_rf_rst_inst1_n_12 ,\genblk1[3].rams_sp_rf_rst_inst1_n_13 ,\genblk1[3].rams_sp_rf_rst_inst1_n_14 ,\genblk1[3].rams_sp_rf_rst_inst1_n_15 ,\genblk1[3].rams_sp_rf_rst_inst1_n_16 ,\genblk1[3].rams_sp_rf_rst_inst1_n_17 ,\genblk1[3].rams_sp_rf_rst_inst1_n_18 ,\genblk1[3].rams_sp_rf_rst_inst1_n_19 ,\genblk1[3].rams_sp_rf_rst_inst1_n_20 ,\genblk1[3].rams_sp_rf_rst_inst1_n_21 ,\genblk1[3].rams_sp_rf_rst_inst1_n_22 ,\genblk1[3].rams_sp_rf_rst_inst1_n_23 }),
        .Q(out_mux_2),
        .ctrl_mux_out_2(ctrl_mux_out_2));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_multiplexer_31 multiplexer_inst
       (.D({\genblk1[3].rams_sp_rf_rst_inst0_n_0 ,\genblk1[3].rams_sp_rf_rst_inst0_n_1 ,\genblk1[3].rams_sp_rf_rst_inst0_n_2 ,\genblk1[3].rams_sp_rf_rst_inst0_n_3 ,\genblk1[3].rams_sp_rf_rst_inst0_n_4 ,\genblk1[3].rams_sp_rf_rst_inst0_n_5 ,\genblk1[3].rams_sp_rf_rst_inst0_n_6 ,\genblk1[3].rams_sp_rf_rst_inst0_n_7 ,\genblk1[3].rams_sp_rf_rst_inst0_n_8 ,\genblk1[3].rams_sp_rf_rst_inst0_n_9 ,\genblk1[3].rams_sp_rf_rst_inst0_n_10 ,\genblk1[3].rams_sp_rf_rst_inst0_n_11 ,\genblk1[3].rams_sp_rf_rst_inst0_n_12 ,\genblk1[3].rams_sp_rf_rst_inst0_n_13 ,\genblk1[3].rams_sp_rf_rst_inst0_n_14 ,\genblk1[3].rams_sp_rf_rst_inst0_n_15 ,\genblk1[3].rams_sp_rf_rst_inst0_n_16 ,\genblk1[3].rams_sp_rf_rst_inst0_n_17 ,\genblk1[3].rams_sp_rf_rst_inst0_n_18 ,\genblk1[3].rams_sp_rf_rst_inst0_n_19 ,\genblk1[3].rams_sp_rf_rst_inst0_n_20 ,\genblk1[3].rams_sp_rf_rst_inst0_n_21 ,\genblk1[3].rams_sp_rf_rst_inst0_n_22 ,\genblk1[3].rams_sp_rf_rst_inst0_n_23 }),
        .Q(out_mux_2),
        .ctrl_mux_out_1(ctrl_mux_out_1),
        .m00_axis_tdata(m00_axis_tdata),
        .s00_axis_tuser(s00_axis_tuser[11]));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst
   (dout0_out,
    m00_axis_aclk,
    Q,
    addr,
    s00_axis_tdata,
    s00_axis_tuser,
    s00_axis_tvalid);
  output [23:0]dout0_out;
  input m00_axis_aclk;
  input [0:0]Q;
  input [10:0]addr;
  input [23:0]s00_axis_tdata;
  input [0:0]s00_axis_tuser;
  input s00_axis_tvalid;

  wire [0:0]Q;
  wire [10:0]addr;
  wire [23:0]dout0_out;
  wire m00_axis_aclk;
  wire ram_reg_0_i_12_n_0;
  wire [23:0]s00_axis_tdata;
  wire [0:0]s00_axis_tuser;
  wire s00_axis_tvalid;
  wire NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED;
  wire NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED;
  wire NLW_ram_reg_0_DBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_SBITERR_UNCONNECTED;
  wire [31:16]NLW_ram_reg_0_DOADO_UNCONNECTED;
  wire [31:0]NLW_ram_reg_0_DOBDO_UNCONNECTED;
  wire [3:2]NLW_ram_reg_0_DOPADOP_UNCONNECTED;
  wire [3:0]NLW_ram_reg_0_DOPBDOP_UNCONNECTED;
  wire [7:0]NLW_ram_reg_0_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_ram_reg_0_RDADDRECC_UNCONNECTED;
  wire [15:6]NLW_ram_reg_1_DOADO_UNCONNECTED;
  wire [15:0]NLW_ram_reg_1_DOBDO_UNCONNECTED;
  wire [1:0]NLW_ram_reg_1_DOPADOP_UNCONNECTED;
  wire [1:0]NLW_ram_reg_1_DOPBDOP_UNCONNECTED;

  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p2_d16" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "46080" *) 
  (* RTL_RAM_NAME = "ram" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "2047" *) 
  (* bram_slice_begin = "0" *) 
  (* bram_slice_end = "17" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "2047" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "17" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(18),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(18),
    .WRITE_WIDTH_B(0)) 
    ram_reg_0
       (.ADDRARDADDR({1'b1,addr,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CASCADEINA(1'b1),
        .CASCADEINB(1'b0),
        .CASCADEOUTA(NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED),
        .CASCADEOUTB(NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED),
        .CLKARDCLK(m00_axis_aclk),
        .CLKBWRCLK(1'b0),
        .DBITERR(NLW_ram_reg_0_DBITERR_UNCONNECTED),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,s00_axis_tdata[15:0]}),
        .DIBDI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0,s00_axis_tdata[17:16]}),
        .DIPBDIP({1'b1,1'b1,1'b1,1'b1}),
        .DOADO({NLW_ram_reg_0_DOADO_UNCONNECTED[31:16],dout0_out[15:0]}),
        .DOBDO(NLW_ram_reg_0_DOBDO_UNCONNECTED[31:0]),
        .DOPADOP({NLW_ram_reg_0_DOPADOP_UNCONNECTED[3:2],dout0_out[17:16]}),
        .DOPBDOP(NLW_ram_reg_0_DOPBDOP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_ram_reg_0_ECCPARITY_UNCONNECTED[7:0]),
        .ENARDEN(Q),
        .ENBWREN(1'b0),
        .INJECTDBITERR(NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED),
        .INJECTSBITERR(NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED),
        .RDADDRECC(NLW_ram_reg_0_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_ram_reg_0_SBITERR_UNCONNECTED),
        .WEA({ram_reg_0_i_12_n_0,ram_reg_0_i_12_n_0,ram_reg_0_i_12_n_0,ram_reg_0_i_12_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
  LUT3 #(
    .INIT(8'h20)) 
    ram_reg_0_i_12
       (.I0(Q),
        .I1(s00_axis_tuser),
        .I2(s00_axis_tvalid),
        .O(ram_reg_0_i_12_n_0));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d6" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "46080" *) 
  (* RTL_RAM_NAME = "ram" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "2047" *) 
  (* bram_slice_begin = "18" *) 
  (* bram_slice_end = "23" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "2047" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "18" *) 
  (* ram_slice_end = "23" *) 
  RAMB18E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .INIT_A(18'h00000),
    .INIT_B(18'h00000),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(18'h00000),
    .SRVAL_B(18'h00000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(0)) 
    ram_reg_1
       (.ADDRARDADDR({addr,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CLKARDCLK(m00_axis_aclk),
        .CLKBWRCLK(1'b0),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,s00_axis_tdata[23:18]}),
        .DIBDI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0}),
        .DIPBDIP({1'b1,1'b1}),
        .DOADO({NLW_ram_reg_1_DOADO_UNCONNECTED[15:6],dout0_out[23:18]}),
        .DOBDO(NLW_ram_reg_1_DOBDO_UNCONNECTED[15:0]),
        .DOPADOP(NLW_ram_reg_1_DOPADOP_UNCONNECTED[1:0]),
        .DOPBDOP(NLW_ram_reg_1_DOPBDOP_UNCONNECTED[1:0]),
        .ENARDEN(Q),
        .ENBWREN(1'b0),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .WEA({ram_reg_0_i_12_n_0,ram_reg_0_i_12_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0}));
endmodule

(* ORIG_REF_NAME = "rams_sp_rf_rst" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_0
   (dout0_out,
    m00_axis_aclk,
    Q,
    addr,
    s00_axis_tdata,
    s00_axis_tuser,
    s00_axis_tvalid);
  output [23:0]dout0_out;
  input m00_axis_aclk;
  input [0:0]Q;
  input [10:0]addr;
  input [23:0]s00_axis_tdata;
  input [0:0]s00_axis_tuser;
  input s00_axis_tvalid;

  wire [0:0]Q;
  wire [10:0]addr;
  wire [23:0]dout0_out;
  wire m00_axis_aclk;
  wire ram_reg_0_i_12__0_n_0;
  wire [23:0]s00_axis_tdata;
  wire [0:0]s00_axis_tuser;
  wire s00_axis_tvalid;
  wire NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED;
  wire NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED;
  wire NLW_ram_reg_0_DBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_SBITERR_UNCONNECTED;
  wire [31:16]NLW_ram_reg_0_DOADO_UNCONNECTED;
  wire [31:0]NLW_ram_reg_0_DOBDO_UNCONNECTED;
  wire [3:2]NLW_ram_reg_0_DOPADOP_UNCONNECTED;
  wire [3:0]NLW_ram_reg_0_DOPBDOP_UNCONNECTED;
  wire [7:0]NLW_ram_reg_0_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_ram_reg_0_RDADDRECC_UNCONNECTED;
  wire [15:6]NLW_ram_reg_1_DOADO_UNCONNECTED;
  wire [15:0]NLW_ram_reg_1_DOBDO_UNCONNECTED;
  wire [1:0]NLW_ram_reg_1_DOPADOP_UNCONNECTED;
  wire [1:0]NLW_ram_reg_1_DOPBDOP_UNCONNECTED;

  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p2_d16" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "46080" *) 
  (* RTL_RAM_NAME = "ram" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "2047" *) 
  (* bram_slice_begin = "0" *) 
  (* bram_slice_end = "17" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "2047" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "17" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(18),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(18),
    .WRITE_WIDTH_B(0)) 
    ram_reg_0
       (.ADDRARDADDR({1'b1,addr,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CASCADEINA(1'b1),
        .CASCADEINB(1'b0),
        .CASCADEOUTA(NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED),
        .CASCADEOUTB(NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED),
        .CLKARDCLK(m00_axis_aclk),
        .CLKBWRCLK(1'b0),
        .DBITERR(NLW_ram_reg_0_DBITERR_UNCONNECTED),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,s00_axis_tdata[15:0]}),
        .DIBDI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0,s00_axis_tdata[17:16]}),
        .DIPBDIP({1'b1,1'b1,1'b1,1'b1}),
        .DOADO({NLW_ram_reg_0_DOADO_UNCONNECTED[31:16],dout0_out[15:0]}),
        .DOBDO(NLW_ram_reg_0_DOBDO_UNCONNECTED[31:0]),
        .DOPADOP({NLW_ram_reg_0_DOPADOP_UNCONNECTED[3:2],dout0_out[17:16]}),
        .DOPBDOP(NLW_ram_reg_0_DOPBDOP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_ram_reg_0_ECCPARITY_UNCONNECTED[7:0]),
        .ENARDEN(Q),
        .ENBWREN(1'b0),
        .INJECTDBITERR(NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED),
        .INJECTSBITERR(NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED),
        .RDADDRECC(NLW_ram_reg_0_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_ram_reg_0_SBITERR_UNCONNECTED),
        .WEA({ram_reg_0_i_12__0_n_0,ram_reg_0_i_12__0_n_0,ram_reg_0_i_12__0_n_0,ram_reg_0_i_12__0_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
  LUT3 #(
    .INIT(8'h80)) 
    ram_reg_0_i_12__0
       (.I0(Q),
        .I1(s00_axis_tuser),
        .I2(s00_axis_tvalid),
        .O(ram_reg_0_i_12__0_n_0));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d6" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "46080" *) 
  (* RTL_RAM_NAME = "ram" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "2047" *) 
  (* bram_slice_begin = "18" *) 
  (* bram_slice_end = "23" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "2047" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "18" *) 
  (* ram_slice_end = "23" *) 
  RAMB18E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .INIT_A(18'h00000),
    .INIT_B(18'h00000),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(18'h00000),
    .SRVAL_B(18'h00000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(0)) 
    ram_reg_1
       (.ADDRARDADDR({addr,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CLKARDCLK(m00_axis_aclk),
        .CLKBWRCLK(1'b0),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,s00_axis_tdata[23:18]}),
        .DIBDI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0}),
        .DIPBDIP({1'b1,1'b1}),
        .DOADO({NLW_ram_reg_1_DOADO_UNCONNECTED[15:6],dout0_out[23:18]}),
        .DOBDO(NLW_ram_reg_1_DOBDO_UNCONNECTED[15:0]),
        .DOPADOP(NLW_ram_reg_1_DOPADOP_UNCONNECTED[1:0]),
        .DOPBDOP(NLW_ram_reg_1_DOPBDOP_UNCONNECTED[1:0]),
        .ENARDEN(Q),
        .ENBWREN(1'b0),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .WEA({ram_reg_0_i_12__0_n_0,ram_reg_0_i_12__0_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0}));
endmodule

(* ORIG_REF_NAME = "rams_sp_rf_rst" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_1
   (dout0_out,
    m00_axis_aclk,
    Q,
    addr,
    s00_axis_tdata,
    s00_axis_tuser,
    s00_axis_tvalid);
  output [23:0]dout0_out;
  input m00_axis_aclk;
  input [0:0]Q;
  input [10:0]addr;
  input [23:0]s00_axis_tdata;
  input [0:0]s00_axis_tuser;
  input s00_axis_tvalid;

  wire [0:0]Q;
  wire [10:0]addr;
  wire [23:0]dout0_out;
  wire m00_axis_aclk;
  wire ram_reg_0_i_1__17_n_0;
  wire [23:0]s00_axis_tdata;
  wire [0:0]s00_axis_tuser;
  wire s00_axis_tvalid;
  wire NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED;
  wire NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED;
  wire NLW_ram_reg_0_DBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_SBITERR_UNCONNECTED;
  wire [31:16]NLW_ram_reg_0_DOADO_UNCONNECTED;
  wire [31:0]NLW_ram_reg_0_DOBDO_UNCONNECTED;
  wire [3:2]NLW_ram_reg_0_DOPADOP_UNCONNECTED;
  wire [3:0]NLW_ram_reg_0_DOPBDOP_UNCONNECTED;
  wire [7:0]NLW_ram_reg_0_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_ram_reg_0_RDADDRECC_UNCONNECTED;
  wire [15:6]NLW_ram_reg_1_DOADO_UNCONNECTED;
  wire [15:0]NLW_ram_reg_1_DOBDO_UNCONNECTED;
  wire [1:0]NLW_ram_reg_1_DOPADOP_UNCONNECTED;
  wire [1:0]NLW_ram_reg_1_DOPBDOP_UNCONNECTED;

  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p2_d16" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "46080" *) 
  (* RTL_RAM_NAME = "ram" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "2047" *) 
  (* bram_slice_begin = "0" *) 
  (* bram_slice_end = "17" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "2047" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "17" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(18),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(18),
    .WRITE_WIDTH_B(0)) 
    ram_reg_0
       (.ADDRARDADDR({1'b1,addr,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CASCADEINA(1'b1),
        .CASCADEINB(1'b0),
        .CASCADEOUTA(NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED),
        .CASCADEOUTB(NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED),
        .CLKARDCLK(m00_axis_aclk),
        .CLKBWRCLK(1'b0),
        .DBITERR(NLW_ram_reg_0_DBITERR_UNCONNECTED),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,s00_axis_tdata[15:0]}),
        .DIBDI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0,s00_axis_tdata[17:16]}),
        .DIPBDIP({1'b1,1'b1,1'b1,1'b1}),
        .DOADO({NLW_ram_reg_0_DOADO_UNCONNECTED[31:16],dout0_out[15:0]}),
        .DOBDO(NLW_ram_reg_0_DOBDO_UNCONNECTED[31:0]),
        .DOPADOP({NLW_ram_reg_0_DOPADOP_UNCONNECTED[3:2],dout0_out[17:16]}),
        .DOPBDOP(NLW_ram_reg_0_DOPBDOP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_ram_reg_0_ECCPARITY_UNCONNECTED[7:0]),
        .ENARDEN(Q),
        .ENBWREN(1'b0),
        .INJECTDBITERR(NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED),
        .INJECTSBITERR(NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED),
        .RDADDRECC(NLW_ram_reg_0_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_ram_reg_0_SBITERR_UNCONNECTED),
        .WEA({ram_reg_0_i_1__17_n_0,ram_reg_0_i_1__17_n_0,ram_reg_0_i_1__17_n_0,ram_reg_0_i_1__17_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
  LUT3 #(
    .INIT(8'h20)) 
    ram_reg_0_i_1__17
       (.I0(Q),
        .I1(s00_axis_tuser),
        .I2(s00_axis_tvalid),
        .O(ram_reg_0_i_1__17_n_0));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d6" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "46080" *) 
  (* RTL_RAM_NAME = "ram" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "2047" *) 
  (* bram_slice_begin = "18" *) 
  (* bram_slice_end = "23" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "2047" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "18" *) 
  (* ram_slice_end = "23" *) 
  RAMB18E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .INIT_A(18'h00000),
    .INIT_B(18'h00000),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(18'h00000),
    .SRVAL_B(18'h00000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(0)) 
    ram_reg_1
       (.ADDRARDADDR({addr,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CLKARDCLK(m00_axis_aclk),
        .CLKBWRCLK(1'b0),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,s00_axis_tdata[23:18]}),
        .DIBDI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0}),
        .DIPBDIP({1'b1,1'b1}),
        .DOADO({NLW_ram_reg_1_DOADO_UNCONNECTED[15:6],dout0_out[23:18]}),
        .DOBDO(NLW_ram_reg_1_DOBDO_UNCONNECTED[15:0]),
        .DOPADOP(NLW_ram_reg_1_DOPADOP_UNCONNECTED[1:0]),
        .DOPBDOP(NLW_ram_reg_1_DOPBDOP_UNCONNECTED[1:0]),
        .ENARDEN(Q),
        .ENBWREN(1'b0),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .WEA({ram_reg_0_i_1__17_n_0,ram_reg_0_i_1__17_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0}));
endmodule

(* ORIG_REF_NAME = "rams_sp_rf_rst" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_10
   (dout0_out,
    m00_axis_aclk,
    Q,
    addr,
    s00_axis_tdata,
    s00_axis_tuser,
    s00_axis_tvalid);
  output [23:0]dout0_out;
  input m00_axis_aclk;
  input [0:0]Q;
  input [10:0]addr;
  input [23:0]s00_axis_tdata;
  input [0:0]s00_axis_tuser;
  input s00_axis_tvalid;

  wire [0:0]Q;
  wire [10:0]addr;
  wire [23:0]dout0_out;
  wire m00_axis_aclk;
  wire ram_reg_0_i_1__26_n_0;
  wire [23:0]s00_axis_tdata;
  wire [0:0]s00_axis_tuser;
  wire s00_axis_tvalid;
  wire NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED;
  wire NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED;
  wire NLW_ram_reg_0_DBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_SBITERR_UNCONNECTED;
  wire [31:16]NLW_ram_reg_0_DOADO_UNCONNECTED;
  wire [31:0]NLW_ram_reg_0_DOBDO_UNCONNECTED;
  wire [3:2]NLW_ram_reg_0_DOPADOP_UNCONNECTED;
  wire [3:0]NLW_ram_reg_0_DOPBDOP_UNCONNECTED;
  wire [7:0]NLW_ram_reg_0_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_ram_reg_0_RDADDRECC_UNCONNECTED;
  wire [15:6]NLW_ram_reg_1_DOADO_UNCONNECTED;
  wire [15:0]NLW_ram_reg_1_DOBDO_UNCONNECTED;
  wire [1:0]NLW_ram_reg_1_DOPADOP_UNCONNECTED;
  wire [1:0]NLW_ram_reg_1_DOPBDOP_UNCONNECTED;

  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p2_d16" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "46080" *) 
  (* RTL_RAM_NAME = "ram" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "2047" *) 
  (* bram_slice_begin = "0" *) 
  (* bram_slice_end = "17" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "2047" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "17" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(18),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(18),
    .WRITE_WIDTH_B(0)) 
    ram_reg_0
       (.ADDRARDADDR({1'b1,addr,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CASCADEINA(1'b1),
        .CASCADEINB(1'b0),
        .CASCADEOUTA(NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED),
        .CASCADEOUTB(NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED),
        .CLKARDCLK(m00_axis_aclk),
        .CLKBWRCLK(1'b0),
        .DBITERR(NLW_ram_reg_0_DBITERR_UNCONNECTED),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,s00_axis_tdata[15:0]}),
        .DIBDI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0,s00_axis_tdata[17:16]}),
        .DIPBDIP({1'b1,1'b1,1'b1,1'b1}),
        .DOADO({NLW_ram_reg_0_DOADO_UNCONNECTED[31:16],dout0_out[15:0]}),
        .DOBDO(NLW_ram_reg_0_DOBDO_UNCONNECTED[31:0]),
        .DOPADOP({NLW_ram_reg_0_DOPADOP_UNCONNECTED[3:2],dout0_out[17:16]}),
        .DOPBDOP(NLW_ram_reg_0_DOPBDOP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_ram_reg_0_ECCPARITY_UNCONNECTED[7:0]),
        .ENARDEN(Q),
        .ENBWREN(1'b0),
        .INJECTDBITERR(NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED),
        .INJECTSBITERR(NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED),
        .RDADDRECC(NLW_ram_reg_0_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_ram_reg_0_SBITERR_UNCONNECTED),
        .WEA({ram_reg_0_i_1__26_n_0,ram_reg_0_i_1__26_n_0,ram_reg_0_i_1__26_n_0,ram_reg_0_i_1__26_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
  LUT3 #(
    .INIT(8'h80)) 
    ram_reg_0_i_1__26
       (.I0(Q),
        .I1(s00_axis_tuser),
        .I2(s00_axis_tvalid),
        .O(ram_reg_0_i_1__26_n_0));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d6" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "46080" *) 
  (* RTL_RAM_NAME = "ram" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "2047" *) 
  (* bram_slice_begin = "18" *) 
  (* bram_slice_end = "23" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "2047" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "18" *) 
  (* ram_slice_end = "23" *) 
  RAMB18E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .INIT_A(18'h00000),
    .INIT_B(18'h00000),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(18'h00000),
    .SRVAL_B(18'h00000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(0)) 
    ram_reg_1
       (.ADDRARDADDR({addr,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CLKARDCLK(m00_axis_aclk),
        .CLKBWRCLK(1'b0),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,s00_axis_tdata[23:18]}),
        .DIBDI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0}),
        .DIPBDIP({1'b1,1'b1}),
        .DOADO({NLW_ram_reg_1_DOADO_UNCONNECTED[15:6],dout0_out[23:18]}),
        .DOBDO(NLW_ram_reg_1_DOBDO_UNCONNECTED[15:0]),
        .DOPADOP(NLW_ram_reg_1_DOPADOP_UNCONNECTED[1:0]),
        .DOPBDOP(NLW_ram_reg_1_DOPBDOP_UNCONNECTED[1:0]),
        .ENARDEN(Q),
        .ENBWREN(1'b0),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .WEA({ram_reg_0_i_1__26_n_0,ram_reg_0_i_1__26_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0}));
endmodule

(* ORIG_REF_NAME = "rams_sp_rf_rst" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_11
   (ram_reg_0_0,
    ram_reg_0_1,
    ram_reg_0_2,
    ram_reg_0_3,
    ram_reg_0_4,
    ram_reg_0_5,
    ram_reg_0_6,
    ram_reg_0_7,
    ram_reg_0_8,
    ram_reg_0_9,
    ram_reg_0_10,
    ram_reg_0_11,
    ram_reg_0_12,
    ram_reg_0_13,
    ram_reg_0_14,
    ram_reg_0_15,
    ram_reg_0_16,
    ram_reg_0_17,
    ram_reg_1_0,
    ram_reg_1_1,
    ram_reg_1_2,
    ram_reg_1_3,
    ram_reg_1_4,
    ram_reg_1_5,
    addr,
    dout0_out,
    ctrl_mux_out_1,
    \y_reg[23]_i_1 ,
    \y_reg[23]_i_1_0 ,
    Q,
    s00_axis_tuser,
    m00_axis_aclk,
    ram_reg_0_18,
    s00_axis_tdata,
    s00_axis_tvalid);
  output ram_reg_0_0;
  output ram_reg_0_1;
  output ram_reg_0_2;
  output ram_reg_0_3;
  output ram_reg_0_4;
  output ram_reg_0_5;
  output ram_reg_0_6;
  output ram_reg_0_7;
  output ram_reg_0_8;
  output ram_reg_0_9;
  output ram_reg_0_10;
  output ram_reg_0_11;
  output ram_reg_0_12;
  output ram_reg_0_13;
  output ram_reg_0_14;
  output ram_reg_0_15;
  output ram_reg_0_16;
  output ram_reg_0_17;
  output ram_reg_1_0;
  output ram_reg_1_1;
  output ram_reg_1_2;
  output ram_reg_1_3;
  output ram_reg_1_4;
  output ram_reg_1_5;
  output [10:0]addr;
  input [23:0]dout0_out;
  input [1:0]ctrl_mux_out_1;
  input [23:0]\y_reg[23]_i_1 ;
  input [23:0]\y_reg[23]_i_1_0 ;
  input [10:0]Q;
  input [11:0]s00_axis_tuser;
  input m00_axis_aclk;
  input [0:0]ram_reg_0_18;
  input [23:0]s00_axis_tdata;
  input s00_axis_tvalid;

  wire [10:0]Q;
  wire [23:0]\_DOUT_1[15]_30 ;
  wire [10:0]addr;
  wire [1:0]ctrl_mux_out_1;
  wire [23:0]dout0_out;
  wire m00_axis_aclk;
  wire ram_reg_0_0;
  wire ram_reg_0_1;
  wire ram_reg_0_10;
  wire ram_reg_0_11;
  wire ram_reg_0_12;
  wire ram_reg_0_13;
  wire ram_reg_0_14;
  wire ram_reg_0_15;
  wire ram_reg_0_16;
  wire ram_reg_0_17;
  wire [0:0]ram_reg_0_18;
  wire ram_reg_0_2;
  wire ram_reg_0_3;
  wire ram_reg_0_4;
  wire ram_reg_0_5;
  wire ram_reg_0_6;
  wire ram_reg_0_7;
  wire ram_reg_0_8;
  wire ram_reg_0_9;
  wire ram_reg_0_i_1__27_n_0;
  wire ram_reg_1_0;
  wire ram_reg_1_1;
  wire ram_reg_1_2;
  wire ram_reg_1_3;
  wire ram_reg_1_4;
  wire ram_reg_1_5;
  wire [23:0]s00_axis_tdata;
  wire [11:0]s00_axis_tuser;
  wire s00_axis_tvalid;
  wire [23:0]\y_reg[23]_i_1 ;
  wire [23:0]\y_reg[23]_i_1_0 ;
  wire NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED;
  wire NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED;
  wire NLW_ram_reg_0_DBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_SBITERR_UNCONNECTED;
  wire [31:16]NLW_ram_reg_0_DOADO_UNCONNECTED;
  wire [31:0]NLW_ram_reg_0_DOBDO_UNCONNECTED;
  wire [3:2]NLW_ram_reg_0_DOPADOP_UNCONNECTED;
  wire [3:0]NLW_ram_reg_0_DOPBDOP_UNCONNECTED;
  wire [7:0]NLW_ram_reg_0_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_ram_reg_0_RDADDRECC_UNCONNECTED;
  wire [15:6]NLW_ram_reg_1_DOADO_UNCONNECTED;
  wire [15:0]NLW_ram_reg_1_DOBDO_UNCONNECTED;
  wire [1:0]NLW_ram_reg_1_DOPADOP_UNCONNECTED;
  wire [1:0]NLW_ram_reg_1_DOPBDOP_UNCONNECTED;

  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p2_d16" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "46080" *) 
  (* RTL_RAM_NAME = "ram" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "2047" *) 
  (* bram_slice_begin = "0" *) 
  (* bram_slice_end = "17" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "2047" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "17" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(18),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(18),
    .WRITE_WIDTH_B(0)) 
    ram_reg_0
       (.ADDRARDADDR({1'b1,addr,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CASCADEINA(1'b1),
        .CASCADEINB(1'b0),
        .CASCADEOUTA(NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED),
        .CASCADEOUTB(NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED),
        .CLKARDCLK(m00_axis_aclk),
        .CLKBWRCLK(1'b0),
        .DBITERR(NLW_ram_reg_0_DBITERR_UNCONNECTED),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,s00_axis_tdata[15:0]}),
        .DIBDI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0,s00_axis_tdata[17:16]}),
        .DIPBDIP({1'b1,1'b1,1'b1,1'b1}),
        .DOADO({NLW_ram_reg_0_DOADO_UNCONNECTED[31:16],\_DOUT_1[15]_30 [15:0]}),
        .DOBDO(NLW_ram_reg_0_DOBDO_UNCONNECTED[31:0]),
        .DOPADOP({NLW_ram_reg_0_DOPADOP_UNCONNECTED[3:2],\_DOUT_1[15]_30 [17:16]}),
        .DOPBDOP(NLW_ram_reg_0_DOPBDOP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_ram_reg_0_ECCPARITY_UNCONNECTED[7:0]),
        .ENARDEN(ram_reg_0_18),
        .ENBWREN(1'b0),
        .INJECTDBITERR(NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED),
        .INJECTSBITERR(NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED),
        .RDADDRECC(NLW_ram_reg_0_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_ram_reg_0_SBITERR_UNCONNECTED),
        .WEA({ram_reg_0_i_1__27_n_0,ram_reg_0_i_1__27_n_0,ram_reg_0_i_1__27_n_0,ram_reg_0_i_1__27_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
  LUT3 #(
    .INIT(8'hB8)) 
    ram_reg_0_i_10
       (.I0(Q[1]),
        .I1(s00_axis_tuser[11]),
        .I2(s00_axis_tuser[1]),
        .O(addr[1]));
  LUT3 #(
    .INIT(8'hB8)) 
    ram_reg_0_i_11
       (.I0(Q[0]),
        .I1(s00_axis_tuser[11]),
        .I2(s00_axis_tuser[0]),
        .O(addr[0]));
  LUT3 #(
    .INIT(8'h20)) 
    ram_reg_0_i_1__27
       (.I0(ram_reg_0_18),
        .I1(s00_axis_tuser[11]),
        .I2(s00_axis_tvalid),
        .O(ram_reg_0_i_1__27_n_0));
  LUT3 #(
    .INIT(8'hB8)) 
    ram_reg_0_i_1__29
       (.I0(Q[10]),
        .I1(s00_axis_tuser[11]),
        .I2(s00_axis_tuser[10]),
        .O(addr[10]));
  LUT3 #(
    .INIT(8'hB8)) 
    ram_reg_0_i_2
       (.I0(Q[9]),
        .I1(s00_axis_tuser[11]),
        .I2(s00_axis_tuser[9]),
        .O(addr[9]));
  LUT3 #(
    .INIT(8'hB8)) 
    ram_reg_0_i_3
       (.I0(Q[8]),
        .I1(s00_axis_tuser[11]),
        .I2(s00_axis_tuser[8]),
        .O(addr[8]));
  LUT3 #(
    .INIT(8'hB8)) 
    ram_reg_0_i_4
       (.I0(Q[7]),
        .I1(s00_axis_tuser[11]),
        .I2(s00_axis_tuser[7]),
        .O(addr[7]));
  LUT3 #(
    .INIT(8'hB8)) 
    ram_reg_0_i_5
       (.I0(Q[6]),
        .I1(s00_axis_tuser[11]),
        .I2(s00_axis_tuser[6]),
        .O(addr[6]));
  LUT3 #(
    .INIT(8'hB8)) 
    ram_reg_0_i_6
       (.I0(Q[5]),
        .I1(s00_axis_tuser[11]),
        .I2(s00_axis_tuser[5]),
        .O(addr[5]));
  LUT3 #(
    .INIT(8'hB8)) 
    ram_reg_0_i_7
       (.I0(Q[4]),
        .I1(s00_axis_tuser[11]),
        .I2(s00_axis_tuser[4]),
        .O(addr[4]));
  LUT3 #(
    .INIT(8'hB8)) 
    ram_reg_0_i_8
       (.I0(Q[3]),
        .I1(s00_axis_tuser[11]),
        .I2(s00_axis_tuser[3]),
        .O(addr[3]));
  LUT3 #(
    .INIT(8'hB8)) 
    ram_reg_0_i_9
       (.I0(Q[2]),
        .I1(s00_axis_tuser[11]),
        .I2(s00_axis_tuser[2]),
        .O(addr[2]));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d6" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "46080" *) 
  (* RTL_RAM_NAME = "ram" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "2047" *) 
  (* bram_slice_begin = "18" *) 
  (* bram_slice_end = "23" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "2047" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "18" *) 
  (* ram_slice_end = "23" *) 
  RAMB18E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .INIT_A(18'h00000),
    .INIT_B(18'h00000),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(18'h00000),
    .SRVAL_B(18'h00000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(0)) 
    ram_reg_1
       (.ADDRARDADDR({addr,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CLKARDCLK(m00_axis_aclk),
        .CLKBWRCLK(1'b0),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,s00_axis_tdata[23:18]}),
        .DIBDI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0}),
        .DIPBDIP({1'b1,1'b1}),
        .DOADO({NLW_ram_reg_1_DOADO_UNCONNECTED[15:6],\_DOUT_1[15]_30 [23:18]}),
        .DOBDO(NLW_ram_reg_1_DOBDO_UNCONNECTED[15:0]),
        .DOPADOP(NLW_ram_reg_1_DOPADOP_UNCONNECTED[1:0]),
        .DOPBDOP(NLW_ram_reg_1_DOPBDOP_UNCONNECTED[1:0]),
        .ENARDEN(ram_reg_0_18),
        .ENBWREN(1'b0),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .WEA({ram_reg_0_i_1__27_n_0,ram_reg_0_i_1__27_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0}));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[0]_i_4 
       (.I0(\_DOUT_1[15]_30 [0]),
        .I1(dout0_out[0]),
        .I2(ctrl_mux_out_1[1]),
        .I3(\y_reg[23]_i_1 [0]),
        .I4(ctrl_mux_out_1[0]),
        .I5(\y_reg[23]_i_1_0 [0]),
        .O(ram_reg_0_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[10]_i_4 
       (.I0(\_DOUT_1[15]_30 [10]),
        .I1(dout0_out[10]),
        .I2(ctrl_mux_out_1[1]),
        .I3(\y_reg[23]_i_1 [10]),
        .I4(ctrl_mux_out_1[0]),
        .I5(\y_reg[23]_i_1_0 [10]),
        .O(ram_reg_0_10));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[11]_i_4 
       (.I0(\_DOUT_1[15]_30 [11]),
        .I1(dout0_out[11]),
        .I2(ctrl_mux_out_1[1]),
        .I3(\y_reg[23]_i_1 [11]),
        .I4(ctrl_mux_out_1[0]),
        .I5(\y_reg[23]_i_1_0 [11]),
        .O(ram_reg_0_11));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[12]_i_4 
       (.I0(\_DOUT_1[15]_30 [12]),
        .I1(dout0_out[12]),
        .I2(ctrl_mux_out_1[1]),
        .I3(\y_reg[23]_i_1 [12]),
        .I4(ctrl_mux_out_1[0]),
        .I5(\y_reg[23]_i_1_0 [12]),
        .O(ram_reg_0_12));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[13]_i_4 
       (.I0(\_DOUT_1[15]_30 [13]),
        .I1(dout0_out[13]),
        .I2(ctrl_mux_out_1[1]),
        .I3(\y_reg[23]_i_1 [13]),
        .I4(ctrl_mux_out_1[0]),
        .I5(\y_reg[23]_i_1_0 [13]),
        .O(ram_reg_0_13));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[14]_i_4 
       (.I0(\_DOUT_1[15]_30 [14]),
        .I1(dout0_out[14]),
        .I2(ctrl_mux_out_1[1]),
        .I3(\y_reg[23]_i_1 [14]),
        .I4(ctrl_mux_out_1[0]),
        .I5(\y_reg[23]_i_1_0 [14]),
        .O(ram_reg_0_14));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[15]_i_4 
       (.I0(\_DOUT_1[15]_30 [15]),
        .I1(dout0_out[15]),
        .I2(ctrl_mux_out_1[1]),
        .I3(\y_reg[23]_i_1 [15]),
        .I4(ctrl_mux_out_1[0]),
        .I5(\y_reg[23]_i_1_0 [15]),
        .O(ram_reg_0_15));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[16]_i_4 
       (.I0(\_DOUT_1[15]_30 [16]),
        .I1(dout0_out[16]),
        .I2(ctrl_mux_out_1[1]),
        .I3(\y_reg[23]_i_1 [16]),
        .I4(ctrl_mux_out_1[0]),
        .I5(\y_reg[23]_i_1_0 [16]),
        .O(ram_reg_0_16));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[17]_i_4 
       (.I0(\_DOUT_1[15]_30 [17]),
        .I1(dout0_out[17]),
        .I2(ctrl_mux_out_1[1]),
        .I3(\y_reg[23]_i_1 [17]),
        .I4(ctrl_mux_out_1[0]),
        .I5(\y_reg[23]_i_1_0 [17]),
        .O(ram_reg_0_17));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[18]_i_4 
       (.I0(\_DOUT_1[15]_30 [18]),
        .I1(dout0_out[18]),
        .I2(ctrl_mux_out_1[1]),
        .I3(\y_reg[23]_i_1 [18]),
        .I4(ctrl_mux_out_1[0]),
        .I5(\y_reg[23]_i_1_0 [18]),
        .O(ram_reg_1_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[19]_i_4 
       (.I0(\_DOUT_1[15]_30 [19]),
        .I1(dout0_out[19]),
        .I2(ctrl_mux_out_1[1]),
        .I3(\y_reg[23]_i_1 [19]),
        .I4(ctrl_mux_out_1[0]),
        .I5(\y_reg[23]_i_1_0 [19]),
        .O(ram_reg_1_1));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[1]_i_4 
       (.I0(\_DOUT_1[15]_30 [1]),
        .I1(dout0_out[1]),
        .I2(ctrl_mux_out_1[1]),
        .I3(\y_reg[23]_i_1 [1]),
        .I4(ctrl_mux_out_1[0]),
        .I5(\y_reg[23]_i_1_0 [1]),
        .O(ram_reg_0_1));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[20]_i_4 
       (.I0(\_DOUT_1[15]_30 [20]),
        .I1(dout0_out[20]),
        .I2(ctrl_mux_out_1[1]),
        .I3(\y_reg[23]_i_1 [20]),
        .I4(ctrl_mux_out_1[0]),
        .I5(\y_reg[23]_i_1_0 [20]),
        .O(ram_reg_1_2));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[21]_i_4 
       (.I0(\_DOUT_1[15]_30 [21]),
        .I1(dout0_out[21]),
        .I2(ctrl_mux_out_1[1]),
        .I3(\y_reg[23]_i_1 [21]),
        .I4(ctrl_mux_out_1[0]),
        .I5(\y_reg[23]_i_1_0 [21]),
        .O(ram_reg_1_3));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[22]_i_4 
       (.I0(\_DOUT_1[15]_30 [22]),
        .I1(dout0_out[22]),
        .I2(ctrl_mux_out_1[1]),
        .I3(\y_reg[23]_i_1 [22]),
        .I4(ctrl_mux_out_1[0]),
        .I5(\y_reg[23]_i_1_0 [22]),
        .O(ram_reg_1_4));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[23]_i_5 
       (.I0(\_DOUT_1[15]_30 [23]),
        .I1(dout0_out[23]),
        .I2(ctrl_mux_out_1[1]),
        .I3(\y_reg[23]_i_1 [23]),
        .I4(ctrl_mux_out_1[0]),
        .I5(\y_reg[23]_i_1_0 [23]),
        .O(ram_reg_1_5));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[2]_i_4 
       (.I0(\_DOUT_1[15]_30 [2]),
        .I1(dout0_out[2]),
        .I2(ctrl_mux_out_1[1]),
        .I3(\y_reg[23]_i_1 [2]),
        .I4(ctrl_mux_out_1[0]),
        .I5(\y_reg[23]_i_1_0 [2]),
        .O(ram_reg_0_2));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[3]_i_4 
       (.I0(\_DOUT_1[15]_30 [3]),
        .I1(dout0_out[3]),
        .I2(ctrl_mux_out_1[1]),
        .I3(\y_reg[23]_i_1 [3]),
        .I4(ctrl_mux_out_1[0]),
        .I5(\y_reg[23]_i_1_0 [3]),
        .O(ram_reg_0_3));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[4]_i_4 
       (.I0(\_DOUT_1[15]_30 [4]),
        .I1(dout0_out[4]),
        .I2(ctrl_mux_out_1[1]),
        .I3(\y_reg[23]_i_1 [4]),
        .I4(ctrl_mux_out_1[0]),
        .I5(\y_reg[23]_i_1_0 [4]),
        .O(ram_reg_0_4));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[5]_i_4 
       (.I0(\_DOUT_1[15]_30 [5]),
        .I1(dout0_out[5]),
        .I2(ctrl_mux_out_1[1]),
        .I3(\y_reg[23]_i_1 [5]),
        .I4(ctrl_mux_out_1[0]),
        .I5(\y_reg[23]_i_1_0 [5]),
        .O(ram_reg_0_5));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[6]_i_4 
       (.I0(\_DOUT_1[15]_30 [6]),
        .I1(dout0_out[6]),
        .I2(ctrl_mux_out_1[1]),
        .I3(\y_reg[23]_i_1 [6]),
        .I4(ctrl_mux_out_1[0]),
        .I5(\y_reg[23]_i_1_0 [6]),
        .O(ram_reg_0_6));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[7]_i_4 
       (.I0(\_DOUT_1[15]_30 [7]),
        .I1(dout0_out[7]),
        .I2(ctrl_mux_out_1[1]),
        .I3(\y_reg[23]_i_1 [7]),
        .I4(ctrl_mux_out_1[0]),
        .I5(\y_reg[23]_i_1_0 [7]),
        .O(ram_reg_0_7));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[8]_i_4 
       (.I0(\_DOUT_1[15]_30 [8]),
        .I1(dout0_out[8]),
        .I2(ctrl_mux_out_1[1]),
        .I3(\y_reg[23]_i_1 [8]),
        .I4(ctrl_mux_out_1[0]),
        .I5(\y_reg[23]_i_1_0 [8]),
        .O(ram_reg_0_8));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[9]_i_4 
       (.I0(\_DOUT_1[15]_30 [9]),
        .I1(dout0_out[9]),
        .I2(ctrl_mux_out_1[1]),
        .I3(\y_reg[23]_i_1 [9]),
        .I4(ctrl_mux_out_1[0]),
        .I5(\y_reg[23]_i_1_0 [9]),
        .O(ram_reg_0_9));
endmodule

(* ORIG_REF_NAME = "rams_sp_rf_rst" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_12
   (ram_reg_0_0,
    ram_reg_0_1,
    ram_reg_0_2,
    ram_reg_0_3,
    ram_reg_0_4,
    ram_reg_0_5,
    ram_reg_0_6,
    ram_reg_0_7,
    ram_reg_0_8,
    ram_reg_0_9,
    ram_reg_0_10,
    ram_reg_0_11,
    ram_reg_0_12,
    ram_reg_0_13,
    ram_reg_0_14,
    ram_reg_0_15,
    ram_reg_0_16,
    ram_reg_0_17,
    ram_reg_1_0,
    ram_reg_1_1,
    ram_reg_1_2,
    ram_reg_1_3,
    ram_reg_1_4,
    ram_reg_1_5,
    addr,
    dout0_out,
    ctrl_mux_out_2,
    \y_reg[23]_i_1__0 ,
    \y_reg[23]_i_1__0_0 ,
    s00_axis_tuser,
    Q,
    m00_axis_aclk,
    ram_reg_0_18,
    s00_axis_tdata,
    s00_axis_tvalid);
  output ram_reg_0_0;
  output ram_reg_0_1;
  output ram_reg_0_2;
  output ram_reg_0_3;
  output ram_reg_0_4;
  output ram_reg_0_5;
  output ram_reg_0_6;
  output ram_reg_0_7;
  output ram_reg_0_8;
  output ram_reg_0_9;
  output ram_reg_0_10;
  output ram_reg_0_11;
  output ram_reg_0_12;
  output ram_reg_0_13;
  output ram_reg_0_14;
  output ram_reg_0_15;
  output ram_reg_0_16;
  output ram_reg_0_17;
  output ram_reg_1_0;
  output ram_reg_1_1;
  output ram_reg_1_2;
  output ram_reg_1_3;
  output ram_reg_1_4;
  output ram_reg_1_5;
  output [10:0]addr;
  input [23:0]dout0_out;
  input [1:0]ctrl_mux_out_2;
  input [23:0]\y_reg[23]_i_1__0 ;
  input [23:0]\y_reg[23]_i_1__0_0 ;
  input [11:0]s00_axis_tuser;
  input [10:0]Q;
  input m00_axis_aclk;
  input [0:0]ram_reg_0_18;
  input [23:0]s00_axis_tdata;
  input s00_axis_tvalid;

  wire [10:0]Q;
  wire [23:0]\_DOUT_2[15]_31 ;
  wire [10:0]addr;
  wire [1:0]ctrl_mux_out_2;
  wire [23:0]dout0_out;
  wire m00_axis_aclk;
  wire ram_reg_0_0;
  wire ram_reg_0_1;
  wire ram_reg_0_10;
  wire ram_reg_0_11;
  wire ram_reg_0_12;
  wire ram_reg_0_13;
  wire ram_reg_0_14;
  wire ram_reg_0_15;
  wire ram_reg_0_16;
  wire ram_reg_0_17;
  wire [0:0]ram_reg_0_18;
  wire ram_reg_0_2;
  wire ram_reg_0_3;
  wire ram_reg_0_4;
  wire ram_reg_0_5;
  wire ram_reg_0_6;
  wire ram_reg_0_7;
  wire ram_reg_0_8;
  wire ram_reg_0_9;
  wire ram_reg_0_i_1__28_n_0;
  wire ram_reg_1_0;
  wire ram_reg_1_1;
  wire ram_reg_1_2;
  wire ram_reg_1_3;
  wire ram_reg_1_4;
  wire ram_reg_1_5;
  wire [23:0]s00_axis_tdata;
  wire [11:0]s00_axis_tuser;
  wire s00_axis_tvalid;
  wire [23:0]\y_reg[23]_i_1__0 ;
  wire [23:0]\y_reg[23]_i_1__0_0 ;
  wire NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED;
  wire NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED;
  wire NLW_ram_reg_0_DBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_SBITERR_UNCONNECTED;
  wire [31:16]NLW_ram_reg_0_DOADO_UNCONNECTED;
  wire [31:0]NLW_ram_reg_0_DOBDO_UNCONNECTED;
  wire [3:2]NLW_ram_reg_0_DOPADOP_UNCONNECTED;
  wire [3:0]NLW_ram_reg_0_DOPBDOP_UNCONNECTED;
  wire [7:0]NLW_ram_reg_0_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_ram_reg_0_RDADDRECC_UNCONNECTED;
  wire [15:6]NLW_ram_reg_1_DOADO_UNCONNECTED;
  wire [15:0]NLW_ram_reg_1_DOBDO_UNCONNECTED;
  wire [1:0]NLW_ram_reg_1_DOPADOP_UNCONNECTED;
  wire [1:0]NLW_ram_reg_1_DOPBDOP_UNCONNECTED;

  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p2_d16" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "46080" *) 
  (* RTL_RAM_NAME = "ram" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "2047" *) 
  (* bram_slice_begin = "0" *) 
  (* bram_slice_end = "17" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "2047" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "17" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(18),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(18),
    .WRITE_WIDTH_B(0)) 
    ram_reg_0
       (.ADDRARDADDR({1'b1,addr,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CASCADEINA(1'b1),
        .CASCADEINB(1'b0),
        .CASCADEOUTA(NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED),
        .CASCADEOUTB(NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED),
        .CLKARDCLK(m00_axis_aclk),
        .CLKBWRCLK(1'b0),
        .DBITERR(NLW_ram_reg_0_DBITERR_UNCONNECTED),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,s00_axis_tdata[15:0]}),
        .DIBDI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0,s00_axis_tdata[17:16]}),
        .DIPBDIP({1'b1,1'b1,1'b1,1'b1}),
        .DOADO({NLW_ram_reg_0_DOADO_UNCONNECTED[31:16],\_DOUT_2[15]_31 [15:0]}),
        .DOBDO(NLW_ram_reg_0_DOBDO_UNCONNECTED[31:0]),
        .DOPADOP({NLW_ram_reg_0_DOPADOP_UNCONNECTED[3:2],\_DOUT_2[15]_31 [17:16]}),
        .DOPBDOP(NLW_ram_reg_0_DOPBDOP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_ram_reg_0_ECCPARITY_UNCONNECTED[7:0]),
        .ENARDEN(ram_reg_0_18),
        .ENBWREN(1'b0),
        .INJECTDBITERR(NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED),
        .INJECTSBITERR(NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED),
        .RDADDRECC(NLW_ram_reg_0_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_ram_reg_0_SBITERR_UNCONNECTED),
        .WEA({ram_reg_0_i_1__28_n_0,ram_reg_0_i_1__28_n_0,ram_reg_0_i_1__28_n_0,ram_reg_0_i_1__28_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
  LUT3 #(
    .INIT(8'hB8)) 
    ram_reg_0_i_10__0
       (.I0(s00_axis_tuser[1]),
        .I1(s00_axis_tuser[11]),
        .I2(Q[1]),
        .O(addr[1]));
  LUT3 #(
    .INIT(8'hB8)) 
    ram_reg_0_i_11__0
       (.I0(s00_axis_tuser[0]),
        .I1(s00_axis_tuser[11]),
        .I2(Q[0]),
        .O(addr[0]));
  LUT3 #(
    .INIT(8'h80)) 
    ram_reg_0_i_1__28
       (.I0(ram_reg_0_18),
        .I1(s00_axis_tuser[11]),
        .I2(s00_axis_tvalid),
        .O(ram_reg_0_i_1__28_n_0));
  LUT3 #(
    .INIT(8'hB8)) 
    ram_reg_0_i_1__30
       (.I0(s00_axis_tuser[10]),
        .I1(s00_axis_tuser[11]),
        .I2(Q[10]),
        .O(addr[10]));
  LUT3 #(
    .INIT(8'hB8)) 
    ram_reg_0_i_2__0
       (.I0(s00_axis_tuser[9]),
        .I1(s00_axis_tuser[11]),
        .I2(Q[9]),
        .O(addr[9]));
  LUT3 #(
    .INIT(8'hB8)) 
    ram_reg_0_i_3__0
       (.I0(s00_axis_tuser[8]),
        .I1(s00_axis_tuser[11]),
        .I2(Q[8]),
        .O(addr[8]));
  LUT3 #(
    .INIT(8'hB8)) 
    ram_reg_0_i_4__0
       (.I0(s00_axis_tuser[7]),
        .I1(s00_axis_tuser[11]),
        .I2(Q[7]),
        .O(addr[7]));
  LUT3 #(
    .INIT(8'hB8)) 
    ram_reg_0_i_5__0
       (.I0(s00_axis_tuser[6]),
        .I1(s00_axis_tuser[11]),
        .I2(Q[6]),
        .O(addr[6]));
  LUT3 #(
    .INIT(8'hB8)) 
    ram_reg_0_i_6__0
       (.I0(s00_axis_tuser[5]),
        .I1(s00_axis_tuser[11]),
        .I2(Q[5]),
        .O(addr[5]));
  LUT3 #(
    .INIT(8'hB8)) 
    ram_reg_0_i_7__0
       (.I0(s00_axis_tuser[4]),
        .I1(s00_axis_tuser[11]),
        .I2(Q[4]),
        .O(addr[4]));
  LUT3 #(
    .INIT(8'hB8)) 
    ram_reg_0_i_8__0
       (.I0(s00_axis_tuser[3]),
        .I1(s00_axis_tuser[11]),
        .I2(Q[3]),
        .O(addr[3]));
  LUT3 #(
    .INIT(8'hB8)) 
    ram_reg_0_i_9__0
       (.I0(s00_axis_tuser[2]),
        .I1(s00_axis_tuser[11]),
        .I2(Q[2]),
        .O(addr[2]));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d6" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "46080" *) 
  (* RTL_RAM_NAME = "ram" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "2047" *) 
  (* bram_slice_begin = "18" *) 
  (* bram_slice_end = "23" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "2047" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "18" *) 
  (* ram_slice_end = "23" *) 
  RAMB18E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .INIT_A(18'h00000),
    .INIT_B(18'h00000),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(18'h00000),
    .SRVAL_B(18'h00000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(0)) 
    ram_reg_1
       (.ADDRARDADDR({addr,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CLKARDCLK(m00_axis_aclk),
        .CLKBWRCLK(1'b0),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,s00_axis_tdata[23:18]}),
        .DIBDI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0}),
        .DIPBDIP({1'b1,1'b1}),
        .DOADO({NLW_ram_reg_1_DOADO_UNCONNECTED[15:6],\_DOUT_2[15]_31 [23:18]}),
        .DOBDO(NLW_ram_reg_1_DOBDO_UNCONNECTED[15:0]),
        .DOPADOP(NLW_ram_reg_1_DOPADOP_UNCONNECTED[1:0]),
        .DOPBDOP(NLW_ram_reg_1_DOPBDOP_UNCONNECTED[1:0]),
        .ENARDEN(ram_reg_0_18),
        .ENBWREN(1'b0),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .WEA({ram_reg_0_i_1__28_n_0,ram_reg_0_i_1__28_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0}));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[0]_i_4__0 
       (.I0(\_DOUT_2[15]_31 [0]),
        .I1(dout0_out[0]),
        .I2(ctrl_mux_out_2[1]),
        .I3(\y_reg[23]_i_1__0 [0]),
        .I4(ctrl_mux_out_2[0]),
        .I5(\y_reg[23]_i_1__0_0 [0]),
        .O(ram_reg_0_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[10]_i_4__0 
       (.I0(\_DOUT_2[15]_31 [10]),
        .I1(dout0_out[10]),
        .I2(ctrl_mux_out_2[1]),
        .I3(\y_reg[23]_i_1__0 [10]),
        .I4(ctrl_mux_out_2[0]),
        .I5(\y_reg[23]_i_1__0_0 [10]),
        .O(ram_reg_0_10));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[11]_i_4__0 
       (.I0(\_DOUT_2[15]_31 [11]),
        .I1(dout0_out[11]),
        .I2(ctrl_mux_out_2[1]),
        .I3(\y_reg[23]_i_1__0 [11]),
        .I4(ctrl_mux_out_2[0]),
        .I5(\y_reg[23]_i_1__0_0 [11]),
        .O(ram_reg_0_11));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[12]_i_4__0 
       (.I0(\_DOUT_2[15]_31 [12]),
        .I1(dout0_out[12]),
        .I2(ctrl_mux_out_2[1]),
        .I3(\y_reg[23]_i_1__0 [12]),
        .I4(ctrl_mux_out_2[0]),
        .I5(\y_reg[23]_i_1__0_0 [12]),
        .O(ram_reg_0_12));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[13]_i_4__0 
       (.I0(\_DOUT_2[15]_31 [13]),
        .I1(dout0_out[13]),
        .I2(ctrl_mux_out_2[1]),
        .I3(\y_reg[23]_i_1__0 [13]),
        .I4(ctrl_mux_out_2[0]),
        .I5(\y_reg[23]_i_1__0_0 [13]),
        .O(ram_reg_0_13));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[14]_i_4__0 
       (.I0(\_DOUT_2[15]_31 [14]),
        .I1(dout0_out[14]),
        .I2(ctrl_mux_out_2[1]),
        .I3(\y_reg[23]_i_1__0 [14]),
        .I4(ctrl_mux_out_2[0]),
        .I5(\y_reg[23]_i_1__0_0 [14]),
        .O(ram_reg_0_14));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[15]_i_4__0 
       (.I0(\_DOUT_2[15]_31 [15]),
        .I1(dout0_out[15]),
        .I2(ctrl_mux_out_2[1]),
        .I3(\y_reg[23]_i_1__0 [15]),
        .I4(ctrl_mux_out_2[0]),
        .I5(\y_reg[23]_i_1__0_0 [15]),
        .O(ram_reg_0_15));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[16]_i_4__0 
       (.I0(\_DOUT_2[15]_31 [16]),
        .I1(dout0_out[16]),
        .I2(ctrl_mux_out_2[1]),
        .I3(\y_reg[23]_i_1__0 [16]),
        .I4(ctrl_mux_out_2[0]),
        .I5(\y_reg[23]_i_1__0_0 [16]),
        .O(ram_reg_0_16));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[17]_i_4__0 
       (.I0(\_DOUT_2[15]_31 [17]),
        .I1(dout0_out[17]),
        .I2(ctrl_mux_out_2[1]),
        .I3(\y_reg[23]_i_1__0 [17]),
        .I4(ctrl_mux_out_2[0]),
        .I5(\y_reg[23]_i_1__0_0 [17]),
        .O(ram_reg_0_17));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[18]_i_4__0 
       (.I0(\_DOUT_2[15]_31 [18]),
        .I1(dout0_out[18]),
        .I2(ctrl_mux_out_2[1]),
        .I3(\y_reg[23]_i_1__0 [18]),
        .I4(ctrl_mux_out_2[0]),
        .I5(\y_reg[23]_i_1__0_0 [18]),
        .O(ram_reg_1_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[19]_i_4__0 
       (.I0(\_DOUT_2[15]_31 [19]),
        .I1(dout0_out[19]),
        .I2(ctrl_mux_out_2[1]),
        .I3(\y_reg[23]_i_1__0 [19]),
        .I4(ctrl_mux_out_2[0]),
        .I5(\y_reg[23]_i_1__0_0 [19]),
        .O(ram_reg_1_1));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[1]_i_4__0 
       (.I0(\_DOUT_2[15]_31 [1]),
        .I1(dout0_out[1]),
        .I2(ctrl_mux_out_2[1]),
        .I3(\y_reg[23]_i_1__0 [1]),
        .I4(ctrl_mux_out_2[0]),
        .I5(\y_reg[23]_i_1__0_0 [1]),
        .O(ram_reg_0_1));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[20]_i_4__0 
       (.I0(\_DOUT_2[15]_31 [20]),
        .I1(dout0_out[20]),
        .I2(ctrl_mux_out_2[1]),
        .I3(\y_reg[23]_i_1__0 [20]),
        .I4(ctrl_mux_out_2[0]),
        .I5(\y_reg[23]_i_1__0_0 [20]),
        .O(ram_reg_1_2));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[21]_i_4__0 
       (.I0(\_DOUT_2[15]_31 [21]),
        .I1(dout0_out[21]),
        .I2(ctrl_mux_out_2[1]),
        .I3(\y_reg[23]_i_1__0 [21]),
        .I4(ctrl_mux_out_2[0]),
        .I5(\y_reg[23]_i_1__0_0 [21]),
        .O(ram_reg_1_3));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[22]_i_4__0 
       (.I0(\_DOUT_2[15]_31 [22]),
        .I1(dout0_out[22]),
        .I2(ctrl_mux_out_2[1]),
        .I3(\y_reg[23]_i_1__0 [22]),
        .I4(ctrl_mux_out_2[0]),
        .I5(\y_reg[23]_i_1__0_0 [22]),
        .O(ram_reg_1_4));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[23]_i_5__0 
       (.I0(\_DOUT_2[15]_31 [23]),
        .I1(dout0_out[23]),
        .I2(ctrl_mux_out_2[1]),
        .I3(\y_reg[23]_i_1__0 [23]),
        .I4(ctrl_mux_out_2[0]),
        .I5(\y_reg[23]_i_1__0_0 [23]),
        .O(ram_reg_1_5));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[2]_i_4__0 
       (.I0(\_DOUT_2[15]_31 [2]),
        .I1(dout0_out[2]),
        .I2(ctrl_mux_out_2[1]),
        .I3(\y_reg[23]_i_1__0 [2]),
        .I4(ctrl_mux_out_2[0]),
        .I5(\y_reg[23]_i_1__0_0 [2]),
        .O(ram_reg_0_2));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[3]_i_4__0 
       (.I0(\_DOUT_2[15]_31 [3]),
        .I1(dout0_out[3]),
        .I2(ctrl_mux_out_2[1]),
        .I3(\y_reg[23]_i_1__0 [3]),
        .I4(ctrl_mux_out_2[0]),
        .I5(\y_reg[23]_i_1__0_0 [3]),
        .O(ram_reg_0_3));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[4]_i_4__0 
       (.I0(\_DOUT_2[15]_31 [4]),
        .I1(dout0_out[4]),
        .I2(ctrl_mux_out_2[1]),
        .I3(\y_reg[23]_i_1__0 [4]),
        .I4(ctrl_mux_out_2[0]),
        .I5(\y_reg[23]_i_1__0_0 [4]),
        .O(ram_reg_0_4));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[5]_i_4__0 
       (.I0(\_DOUT_2[15]_31 [5]),
        .I1(dout0_out[5]),
        .I2(ctrl_mux_out_2[1]),
        .I3(\y_reg[23]_i_1__0 [5]),
        .I4(ctrl_mux_out_2[0]),
        .I5(\y_reg[23]_i_1__0_0 [5]),
        .O(ram_reg_0_5));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[6]_i_4__0 
       (.I0(\_DOUT_2[15]_31 [6]),
        .I1(dout0_out[6]),
        .I2(ctrl_mux_out_2[1]),
        .I3(\y_reg[23]_i_1__0 [6]),
        .I4(ctrl_mux_out_2[0]),
        .I5(\y_reg[23]_i_1__0_0 [6]),
        .O(ram_reg_0_6));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[7]_i_4__0 
       (.I0(\_DOUT_2[15]_31 [7]),
        .I1(dout0_out[7]),
        .I2(ctrl_mux_out_2[1]),
        .I3(\y_reg[23]_i_1__0 [7]),
        .I4(ctrl_mux_out_2[0]),
        .I5(\y_reg[23]_i_1__0_0 [7]),
        .O(ram_reg_0_7));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[8]_i_4__0 
       (.I0(\_DOUT_2[15]_31 [8]),
        .I1(dout0_out[8]),
        .I2(ctrl_mux_out_2[1]),
        .I3(\y_reg[23]_i_1__0 [8]),
        .I4(ctrl_mux_out_2[0]),
        .I5(\y_reg[23]_i_1__0_0 [8]),
        .O(ram_reg_0_8));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[9]_i_4__0 
       (.I0(\_DOUT_2[15]_31 [9]),
        .I1(dout0_out[9]),
        .I2(ctrl_mux_out_2[1]),
        .I3(\y_reg[23]_i_1__0 [9]),
        .I4(ctrl_mux_out_2[0]),
        .I5(\y_reg[23]_i_1__0_0 [9]),
        .O(ram_reg_0_9));
endmodule

(* ORIG_REF_NAME = "rams_sp_rf_rst" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_13
   (dout0_out,
    m00_axis_aclk,
    Q,
    addr,
    s00_axis_tdata,
    s00_axis_tuser,
    s00_axis_tvalid);
  output [23:0]dout0_out;
  input m00_axis_aclk;
  input [0:0]Q;
  input [10:0]addr;
  input [23:0]s00_axis_tdata;
  input [0:0]s00_axis_tuser;
  input s00_axis_tvalid;

  wire [0:0]Q;
  wire [10:0]addr;
  wire [23:0]dout0_out;
  wire m00_axis_aclk;
  wire ram_reg_0_i_1_n_0;
  wire [23:0]s00_axis_tdata;
  wire [0:0]s00_axis_tuser;
  wire s00_axis_tvalid;
  wire NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED;
  wire NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED;
  wire NLW_ram_reg_0_DBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_SBITERR_UNCONNECTED;
  wire [31:16]NLW_ram_reg_0_DOADO_UNCONNECTED;
  wire [31:0]NLW_ram_reg_0_DOBDO_UNCONNECTED;
  wire [3:2]NLW_ram_reg_0_DOPADOP_UNCONNECTED;
  wire [3:0]NLW_ram_reg_0_DOPBDOP_UNCONNECTED;
  wire [7:0]NLW_ram_reg_0_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_ram_reg_0_RDADDRECC_UNCONNECTED;
  wire [15:6]NLW_ram_reg_1_DOADO_UNCONNECTED;
  wire [15:0]NLW_ram_reg_1_DOBDO_UNCONNECTED;
  wire [1:0]NLW_ram_reg_1_DOPADOP_UNCONNECTED;
  wire [1:0]NLW_ram_reg_1_DOPBDOP_UNCONNECTED;

  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p2_d16" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "46080" *) 
  (* RTL_RAM_NAME = "ram" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "2047" *) 
  (* bram_slice_begin = "0" *) 
  (* bram_slice_end = "17" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "2047" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "17" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(18),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(18),
    .WRITE_WIDTH_B(0)) 
    ram_reg_0
       (.ADDRARDADDR({1'b1,addr,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CASCADEINA(1'b1),
        .CASCADEINB(1'b0),
        .CASCADEOUTA(NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED),
        .CASCADEOUTB(NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED),
        .CLKARDCLK(m00_axis_aclk),
        .CLKBWRCLK(1'b0),
        .DBITERR(NLW_ram_reg_0_DBITERR_UNCONNECTED),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,s00_axis_tdata[15:0]}),
        .DIBDI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0,s00_axis_tdata[17:16]}),
        .DIPBDIP({1'b1,1'b1,1'b1,1'b1}),
        .DOADO({NLW_ram_reg_0_DOADO_UNCONNECTED[31:16],dout0_out[15:0]}),
        .DOBDO(NLW_ram_reg_0_DOBDO_UNCONNECTED[31:0]),
        .DOPADOP({NLW_ram_reg_0_DOPADOP_UNCONNECTED[3:2],dout0_out[17:16]}),
        .DOPBDOP(NLW_ram_reg_0_DOPBDOP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_ram_reg_0_ECCPARITY_UNCONNECTED[7:0]),
        .ENARDEN(Q),
        .ENBWREN(1'b0),
        .INJECTDBITERR(NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED),
        .INJECTSBITERR(NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED),
        .RDADDRECC(NLW_ram_reg_0_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_ram_reg_0_SBITERR_UNCONNECTED),
        .WEA({ram_reg_0_i_1_n_0,ram_reg_0_i_1_n_0,ram_reg_0_i_1_n_0,ram_reg_0_i_1_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
  LUT3 #(
    .INIT(8'h20)) 
    ram_reg_0_i_1
       (.I0(Q),
        .I1(s00_axis_tuser),
        .I2(s00_axis_tvalid),
        .O(ram_reg_0_i_1_n_0));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d6" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "46080" *) 
  (* RTL_RAM_NAME = "ram" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "2047" *) 
  (* bram_slice_begin = "18" *) 
  (* bram_slice_end = "23" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "2047" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "18" *) 
  (* ram_slice_end = "23" *) 
  RAMB18E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .INIT_A(18'h00000),
    .INIT_B(18'h00000),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(18'h00000),
    .SRVAL_B(18'h00000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(0)) 
    ram_reg_1
       (.ADDRARDADDR({addr,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CLKARDCLK(m00_axis_aclk),
        .CLKBWRCLK(1'b0),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,s00_axis_tdata[23:18]}),
        .DIBDI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0}),
        .DIPBDIP({1'b1,1'b1}),
        .DOADO({NLW_ram_reg_1_DOADO_UNCONNECTED[15:6],dout0_out[23:18]}),
        .DOBDO(NLW_ram_reg_1_DOBDO_UNCONNECTED[15:0]),
        .DOPADOP(NLW_ram_reg_1_DOPADOP_UNCONNECTED[1:0]),
        .DOPBDOP(NLW_ram_reg_1_DOPBDOP_UNCONNECTED[1:0]),
        .ENARDEN(Q),
        .ENBWREN(1'b0),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .WEA({ram_reg_0_i_1_n_0,ram_reg_0_i_1_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0}));
endmodule

(* ORIG_REF_NAME = "rams_sp_rf_rst" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_14
   (dout0_out,
    m00_axis_aclk,
    Q,
    addr,
    s00_axis_tdata,
    s00_axis_tuser,
    s00_axis_tvalid);
  output [23:0]dout0_out;
  input m00_axis_aclk;
  input [0:0]Q;
  input [10:0]addr;
  input [23:0]s00_axis_tdata;
  input [0:0]s00_axis_tuser;
  input s00_axis_tvalid;

  wire [0:0]Q;
  wire [10:0]addr;
  wire [23:0]dout0_out;
  wire m00_axis_aclk;
  wire ram_reg_0_i_1__0_n_0;
  wire [23:0]s00_axis_tdata;
  wire [0:0]s00_axis_tuser;
  wire s00_axis_tvalid;
  wire NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED;
  wire NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED;
  wire NLW_ram_reg_0_DBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_SBITERR_UNCONNECTED;
  wire [31:16]NLW_ram_reg_0_DOADO_UNCONNECTED;
  wire [31:0]NLW_ram_reg_0_DOBDO_UNCONNECTED;
  wire [3:2]NLW_ram_reg_0_DOPADOP_UNCONNECTED;
  wire [3:0]NLW_ram_reg_0_DOPBDOP_UNCONNECTED;
  wire [7:0]NLW_ram_reg_0_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_ram_reg_0_RDADDRECC_UNCONNECTED;
  wire [15:6]NLW_ram_reg_1_DOADO_UNCONNECTED;
  wire [15:0]NLW_ram_reg_1_DOBDO_UNCONNECTED;
  wire [1:0]NLW_ram_reg_1_DOPADOP_UNCONNECTED;
  wire [1:0]NLW_ram_reg_1_DOPBDOP_UNCONNECTED;

  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p2_d16" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "46080" *) 
  (* RTL_RAM_NAME = "ram" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "2047" *) 
  (* bram_slice_begin = "0" *) 
  (* bram_slice_end = "17" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "2047" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "17" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(18),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(18),
    .WRITE_WIDTH_B(0)) 
    ram_reg_0
       (.ADDRARDADDR({1'b1,addr,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CASCADEINA(1'b1),
        .CASCADEINB(1'b0),
        .CASCADEOUTA(NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED),
        .CASCADEOUTB(NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED),
        .CLKARDCLK(m00_axis_aclk),
        .CLKBWRCLK(1'b0),
        .DBITERR(NLW_ram_reg_0_DBITERR_UNCONNECTED),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,s00_axis_tdata[15:0]}),
        .DIBDI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0,s00_axis_tdata[17:16]}),
        .DIPBDIP({1'b1,1'b1,1'b1,1'b1}),
        .DOADO({NLW_ram_reg_0_DOADO_UNCONNECTED[31:16],dout0_out[15:0]}),
        .DOBDO(NLW_ram_reg_0_DOBDO_UNCONNECTED[31:0]),
        .DOPADOP({NLW_ram_reg_0_DOPADOP_UNCONNECTED[3:2],dout0_out[17:16]}),
        .DOPBDOP(NLW_ram_reg_0_DOPBDOP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_ram_reg_0_ECCPARITY_UNCONNECTED[7:0]),
        .ENARDEN(Q),
        .ENBWREN(1'b0),
        .INJECTDBITERR(NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED),
        .INJECTSBITERR(NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED),
        .RDADDRECC(NLW_ram_reg_0_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_ram_reg_0_SBITERR_UNCONNECTED),
        .WEA({ram_reg_0_i_1__0_n_0,ram_reg_0_i_1__0_n_0,ram_reg_0_i_1__0_n_0,ram_reg_0_i_1__0_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
  LUT3 #(
    .INIT(8'h80)) 
    ram_reg_0_i_1__0
       (.I0(Q),
        .I1(s00_axis_tuser),
        .I2(s00_axis_tvalid),
        .O(ram_reg_0_i_1__0_n_0));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d6" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "46080" *) 
  (* RTL_RAM_NAME = "ram" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "2047" *) 
  (* bram_slice_begin = "18" *) 
  (* bram_slice_end = "23" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "2047" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "18" *) 
  (* ram_slice_end = "23" *) 
  RAMB18E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .INIT_A(18'h00000),
    .INIT_B(18'h00000),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(18'h00000),
    .SRVAL_B(18'h00000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(0)) 
    ram_reg_1
       (.ADDRARDADDR({addr,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CLKARDCLK(m00_axis_aclk),
        .CLKBWRCLK(1'b0),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,s00_axis_tdata[23:18]}),
        .DIBDI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0}),
        .DIPBDIP({1'b1,1'b1}),
        .DOADO({NLW_ram_reg_1_DOADO_UNCONNECTED[15:6],dout0_out[23:18]}),
        .DOBDO(NLW_ram_reg_1_DOBDO_UNCONNECTED[15:0]),
        .DOPADOP(NLW_ram_reg_1_DOPADOP_UNCONNECTED[1:0]),
        .DOPBDOP(NLW_ram_reg_1_DOPBDOP_UNCONNECTED[1:0]),
        .ENARDEN(Q),
        .ENBWREN(1'b0),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .WEA({ram_reg_0_i_1__0_n_0,ram_reg_0_i_1__0_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0}));
endmodule

(* ORIG_REF_NAME = "rams_sp_rf_rst" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_15
   (dout0_out,
    m00_axis_aclk,
    Q,
    addr,
    s00_axis_tdata,
    s00_axis_tuser,
    s00_axis_tvalid);
  output [23:0]dout0_out;
  input m00_axis_aclk;
  input [0:0]Q;
  input [10:0]addr;
  input [23:0]s00_axis_tdata;
  input [0:0]s00_axis_tuser;
  input s00_axis_tvalid;

  wire [0:0]Q;
  wire [10:0]addr;
  wire [23:0]dout0_out;
  wire m00_axis_aclk;
  wire ram_reg_0_i_1__1_n_0;
  wire [23:0]s00_axis_tdata;
  wire [0:0]s00_axis_tuser;
  wire s00_axis_tvalid;
  wire NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED;
  wire NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED;
  wire NLW_ram_reg_0_DBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_SBITERR_UNCONNECTED;
  wire [31:16]NLW_ram_reg_0_DOADO_UNCONNECTED;
  wire [31:0]NLW_ram_reg_0_DOBDO_UNCONNECTED;
  wire [3:2]NLW_ram_reg_0_DOPADOP_UNCONNECTED;
  wire [3:0]NLW_ram_reg_0_DOPBDOP_UNCONNECTED;
  wire [7:0]NLW_ram_reg_0_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_ram_reg_0_RDADDRECC_UNCONNECTED;
  wire [15:6]NLW_ram_reg_1_DOADO_UNCONNECTED;
  wire [15:0]NLW_ram_reg_1_DOBDO_UNCONNECTED;
  wire [1:0]NLW_ram_reg_1_DOPADOP_UNCONNECTED;
  wire [1:0]NLW_ram_reg_1_DOPBDOP_UNCONNECTED;

  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p2_d16" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "46080" *) 
  (* RTL_RAM_NAME = "ram" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "2047" *) 
  (* bram_slice_begin = "0" *) 
  (* bram_slice_end = "17" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "2047" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "17" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(18),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(18),
    .WRITE_WIDTH_B(0)) 
    ram_reg_0
       (.ADDRARDADDR({1'b1,addr,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CASCADEINA(1'b1),
        .CASCADEINB(1'b0),
        .CASCADEOUTA(NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED),
        .CASCADEOUTB(NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED),
        .CLKARDCLK(m00_axis_aclk),
        .CLKBWRCLK(1'b0),
        .DBITERR(NLW_ram_reg_0_DBITERR_UNCONNECTED),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,s00_axis_tdata[15:0]}),
        .DIBDI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0,s00_axis_tdata[17:16]}),
        .DIPBDIP({1'b1,1'b1,1'b1,1'b1}),
        .DOADO({NLW_ram_reg_0_DOADO_UNCONNECTED[31:16],dout0_out[15:0]}),
        .DOBDO(NLW_ram_reg_0_DOBDO_UNCONNECTED[31:0]),
        .DOPADOP({NLW_ram_reg_0_DOPADOP_UNCONNECTED[3:2],dout0_out[17:16]}),
        .DOPBDOP(NLW_ram_reg_0_DOPBDOP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_ram_reg_0_ECCPARITY_UNCONNECTED[7:0]),
        .ENARDEN(Q),
        .ENBWREN(1'b0),
        .INJECTDBITERR(NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED),
        .INJECTSBITERR(NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED),
        .RDADDRECC(NLW_ram_reg_0_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_ram_reg_0_SBITERR_UNCONNECTED),
        .WEA({ram_reg_0_i_1__1_n_0,ram_reg_0_i_1__1_n_0,ram_reg_0_i_1__1_n_0,ram_reg_0_i_1__1_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
  LUT3 #(
    .INIT(8'h20)) 
    ram_reg_0_i_1__1
       (.I0(Q),
        .I1(s00_axis_tuser),
        .I2(s00_axis_tvalid),
        .O(ram_reg_0_i_1__1_n_0));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d6" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "46080" *) 
  (* RTL_RAM_NAME = "ram" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "2047" *) 
  (* bram_slice_begin = "18" *) 
  (* bram_slice_end = "23" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "2047" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "18" *) 
  (* ram_slice_end = "23" *) 
  RAMB18E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .INIT_A(18'h00000),
    .INIT_B(18'h00000),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(18'h00000),
    .SRVAL_B(18'h00000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(0)) 
    ram_reg_1
       (.ADDRARDADDR({addr,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CLKARDCLK(m00_axis_aclk),
        .CLKBWRCLK(1'b0),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,s00_axis_tdata[23:18]}),
        .DIBDI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0}),
        .DIPBDIP({1'b1,1'b1}),
        .DOADO({NLW_ram_reg_1_DOADO_UNCONNECTED[15:6],dout0_out[23:18]}),
        .DOBDO(NLW_ram_reg_1_DOBDO_UNCONNECTED[15:0]),
        .DOPADOP(NLW_ram_reg_1_DOPADOP_UNCONNECTED[1:0]),
        .DOPBDOP(NLW_ram_reg_1_DOPBDOP_UNCONNECTED[1:0]),
        .ENARDEN(Q),
        .ENBWREN(1'b0),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .WEA({ram_reg_0_i_1__1_n_0,ram_reg_0_i_1__1_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0}));
endmodule

(* ORIG_REF_NAME = "rams_sp_rf_rst" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_16
   (dout0_out,
    m00_axis_aclk,
    Q,
    addr,
    s00_axis_tdata,
    s00_axis_tuser,
    s00_axis_tvalid);
  output [23:0]dout0_out;
  input m00_axis_aclk;
  input [0:0]Q;
  input [10:0]addr;
  input [23:0]s00_axis_tdata;
  input [0:0]s00_axis_tuser;
  input s00_axis_tvalid;

  wire [0:0]Q;
  wire [10:0]addr;
  wire [23:0]dout0_out;
  wire m00_axis_aclk;
  wire ram_reg_0_i_1__2_n_0;
  wire [23:0]s00_axis_tdata;
  wire [0:0]s00_axis_tuser;
  wire s00_axis_tvalid;
  wire NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED;
  wire NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED;
  wire NLW_ram_reg_0_DBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_SBITERR_UNCONNECTED;
  wire [31:16]NLW_ram_reg_0_DOADO_UNCONNECTED;
  wire [31:0]NLW_ram_reg_0_DOBDO_UNCONNECTED;
  wire [3:2]NLW_ram_reg_0_DOPADOP_UNCONNECTED;
  wire [3:0]NLW_ram_reg_0_DOPBDOP_UNCONNECTED;
  wire [7:0]NLW_ram_reg_0_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_ram_reg_0_RDADDRECC_UNCONNECTED;
  wire [15:6]NLW_ram_reg_1_DOADO_UNCONNECTED;
  wire [15:0]NLW_ram_reg_1_DOBDO_UNCONNECTED;
  wire [1:0]NLW_ram_reg_1_DOPADOP_UNCONNECTED;
  wire [1:0]NLW_ram_reg_1_DOPBDOP_UNCONNECTED;

  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p2_d16" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "46080" *) 
  (* RTL_RAM_NAME = "ram" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "2047" *) 
  (* bram_slice_begin = "0" *) 
  (* bram_slice_end = "17" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "2047" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "17" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(18),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(18),
    .WRITE_WIDTH_B(0)) 
    ram_reg_0
       (.ADDRARDADDR({1'b1,addr,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CASCADEINA(1'b1),
        .CASCADEINB(1'b0),
        .CASCADEOUTA(NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED),
        .CASCADEOUTB(NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED),
        .CLKARDCLK(m00_axis_aclk),
        .CLKBWRCLK(1'b0),
        .DBITERR(NLW_ram_reg_0_DBITERR_UNCONNECTED),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,s00_axis_tdata[15:0]}),
        .DIBDI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0,s00_axis_tdata[17:16]}),
        .DIPBDIP({1'b1,1'b1,1'b1,1'b1}),
        .DOADO({NLW_ram_reg_0_DOADO_UNCONNECTED[31:16],dout0_out[15:0]}),
        .DOBDO(NLW_ram_reg_0_DOBDO_UNCONNECTED[31:0]),
        .DOPADOP({NLW_ram_reg_0_DOPADOP_UNCONNECTED[3:2],dout0_out[17:16]}),
        .DOPBDOP(NLW_ram_reg_0_DOPBDOP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_ram_reg_0_ECCPARITY_UNCONNECTED[7:0]),
        .ENARDEN(Q),
        .ENBWREN(1'b0),
        .INJECTDBITERR(NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED),
        .INJECTSBITERR(NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED),
        .RDADDRECC(NLW_ram_reg_0_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_ram_reg_0_SBITERR_UNCONNECTED),
        .WEA({ram_reg_0_i_1__2_n_0,ram_reg_0_i_1__2_n_0,ram_reg_0_i_1__2_n_0,ram_reg_0_i_1__2_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
  LUT3 #(
    .INIT(8'h80)) 
    ram_reg_0_i_1__2
       (.I0(Q),
        .I1(s00_axis_tuser),
        .I2(s00_axis_tvalid),
        .O(ram_reg_0_i_1__2_n_0));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d6" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "46080" *) 
  (* RTL_RAM_NAME = "ram" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "2047" *) 
  (* bram_slice_begin = "18" *) 
  (* bram_slice_end = "23" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "2047" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "18" *) 
  (* ram_slice_end = "23" *) 
  RAMB18E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .INIT_A(18'h00000),
    .INIT_B(18'h00000),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(18'h00000),
    .SRVAL_B(18'h00000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(0)) 
    ram_reg_1
       (.ADDRARDADDR({addr,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CLKARDCLK(m00_axis_aclk),
        .CLKBWRCLK(1'b0),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,s00_axis_tdata[23:18]}),
        .DIBDI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0}),
        .DIPBDIP({1'b1,1'b1}),
        .DOADO({NLW_ram_reg_1_DOADO_UNCONNECTED[15:6],dout0_out[23:18]}),
        .DOBDO(NLW_ram_reg_1_DOBDO_UNCONNECTED[15:0]),
        .DOPADOP(NLW_ram_reg_1_DOPADOP_UNCONNECTED[1:0]),
        .DOPBDOP(NLW_ram_reg_1_DOPBDOP_UNCONNECTED[1:0]),
        .ENARDEN(Q),
        .ENBWREN(1'b0),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .WEA({ram_reg_0_i_1__2_n_0,ram_reg_0_i_1__2_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0}));
endmodule

(* ORIG_REF_NAME = "rams_sp_rf_rst" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_17
   (D,
    ctrl_mux_out_1,
    \y_reg[0] ,
    \y_reg[0]_0 ,
    \y_reg[1] ,
    \y_reg[1]_0 ,
    \y_reg[2] ,
    \y_reg[2]_0 ,
    \y_reg[3] ,
    \y_reg[3]_0 ,
    \y_reg[4] ,
    \y_reg[4]_0 ,
    \y_reg[5] ,
    \y_reg[5]_0 ,
    \y_reg[6] ,
    \y_reg[6]_0 ,
    \y_reg[7] ,
    \y_reg[7]_0 ,
    \y_reg[8] ,
    \y_reg[8]_0 ,
    \y_reg[9] ,
    \y_reg[9]_0 ,
    \y_reg[10] ,
    \y_reg[10]_0 ,
    \y_reg[11] ,
    \y_reg[11]_0 ,
    \y_reg[12] ,
    \y_reg[12]_0 ,
    \y_reg[13] ,
    \y_reg[13]_0 ,
    \y_reg[14] ,
    \y_reg[14]_0 ,
    \y_reg[15] ,
    \y_reg[15]_0 ,
    \y_reg[16] ,
    \y_reg[16]_0 ,
    \y_reg[17] ,
    \y_reg[17]_0 ,
    \y_reg[18] ,
    \y_reg[18]_0 ,
    \y_reg[19] ,
    \y_reg[19]_0 ,
    \y_reg[20] ,
    \y_reg[20]_0 ,
    \y_reg[21] ,
    \y_reg[21]_0 ,
    \y_reg[22] ,
    \y_reg[22]_0 ,
    \y_reg[23] ,
    \y_reg[23]_0 ,
    \y_reg[0]_i_1_0 ,
    dout0_out,
    \y_reg[23]_i_3_0 ,
    \y_reg[23]_i_3_1 ,
    \y_reg[1]_i_1_0 ,
    \y_reg[2]_i_1_0 ,
    \y_reg[3]_i_1_0 ,
    \y_reg[4]_i_1_0 ,
    \y_reg[5]_i_1_0 ,
    \y_reg[6]_i_1_0 ,
    \y_reg[7]_i_1_0 ,
    \y_reg[8]_i_1_0 ,
    \y_reg[9]_i_1_0 ,
    \y_reg[10]_i_1_0 ,
    \y_reg[11]_i_1_0 ,
    \y_reg[12]_i_1_0 ,
    \y_reg[13]_i_1_0 ,
    \y_reg[14]_i_1_0 ,
    \y_reg[15]_i_1_0 ,
    \y_reg[16]_i_1_0 ,
    \y_reg[17]_i_1_0 ,
    \y_reg[18]_i_1_0 ,
    \y_reg[19]_i_1_0 ,
    \y_reg[20]_i_1_0 ,
    \y_reg[21]_i_1_0 ,
    \y_reg[22]_i_1_0 ,
    \y_reg[23]_i_1_0 ,
    m00_axis_aclk,
    Q,
    addr,
    s00_axis_tdata,
    s00_axis_tuser,
    s00_axis_tvalid);
  output [23:0]D;
  input [4:0]ctrl_mux_out_1;
  input \y_reg[0] ;
  input \y_reg[0]_0 ;
  input \y_reg[1] ;
  input \y_reg[1]_0 ;
  input \y_reg[2] ;
  input \y_reg[2]_0 ;
  input \y_reg[3] ;
  input \y_reg[3]_0 ;
  input \y_reg[4] ;
  input \y_reg[4]_0 ;
  input \y_reg[5] ;
  input \y_reg[5]_0 ;
  input \y_reg[6] ;
  input \y_reg[6]_0 ;
  input \y_reg[7] ;
  input \y_reg[7]_0 ;
  input \y_reg[8] ;
  input \y_reg[8]_0 ;
  input \y_reg[9] ;
  input \y_reg[9]_0 ;
  input \y_reg[10] ;
  input \y_reg[10]_0 ;
  input \y_reg[11] ;
  input \y_reg[11]_0 ;
  input \y_reg[12] ;
  input \y_reg[12]_0 ;
  input \y_reg[13] ;
  input \y_reg[13]_0 ;
  input \y_reg[14] ;
  input \y_reg[14]_0 ;
  input \y_reg[15] ;
  input \y_reg[15]_0 ;
  input \y_reg[16] ;
  input \y_reg[16]_0 ;
  input \y_reg[17] ;
  input \y_reg[17]_0 ;
  input \y_reg[18] ;
  input \y_reg[18]_0 ;
  input \y_reg[19] ;
  input \y_reg[19]_0 ;
  input \y_reg[20] ;
  input \y_reg[20]_0 ;
  input \y_reg[21] ;
  input \y_reg[21]_0 ;
  input \y_reg[22] ;
  input \y_reg[22]_0 ;
  input \y_reg[23] ;
  input \y_reg[23]_0 ;
  input \y_reg[0]_i_1_0 ;
  input [23:0]dout0_out;
  input [23:0]\y_reg[23]_i_3_0 ;
  input [23:0]\y_reg[23]_i_3_1 ;
  input \y_reg[1]_i_1_0 ;
  input \y_reg[2]_i_1_0 ;
  input \y_reg[3]_i_1_0 ;
  input \y_reg[4]_i_1_0 ;
  input \y_reg[5]_i_1_0 ;
  input \y_reg[6]_i_1_0 ;
  input \y_reg[7]_i_1_0 ;
  input \y_reg[8]_i_1_0 ;
  input \y_reg[9]_i_1_0 ;
  input \y_reg[10]_i_1_0 ;
  input \y_reg[11]_i_1_0 ;
  input \y_reg[12]_i_1_0 ;
  input \y_reg[13]_i_1_0 ;
  input \y_reg[14]_i_1_0 ;
  input \y_reg[15]_i_1_0 ;
  input \y_reg[16]_i_1_0 ;
  input \y_reg[17]_i_1_0 ;
  input \y_reg[18]_i_1_0 ;
  input \y_reg[19]_i_1_0 ;
  input \y_reg[20]_i_1_0 ;
  input \y_reg[21]_i_1_0 ;
  input \y_reg[22]_i_1_0 ;
  input \y_reg[23]_i_1_0 ;
  input m00_axis_aclk;
  input [0:0]Q;
  input [10:0]addr;
  input [23:0]s00_axis_tdata;
  input [0:0]s00_axis_tuser;
  input s00_axis_tvalid;

  wire [23:0]D;
  wire [0:0]Q;
  wire [23:0]\_DOUT_1[3]_6 ;
  wire [10:0]addr;
  wire [4:0]ctrl_mux_out_1;
  wire [23:0]dout0_out;
  wire m00_axis_aclk;
  wire ram_reg_0_i_1__3_n_0;
  wire [23:0]s00_axis_tdata;
  wire [0:0]s00_axis_tuser;
  wire s00_axis_tvalid;
  wire \y_reg[0] ;
  wire \y_reg[0]_0 ;
  wire \y_reg[0]_i_1_0 ;
  wire \y_reg[0]_i_2_n_0 ;
  wire \y_reg[0]_i_5_n_0 ;
  wire \y_reg[10] ;
  wire \y_reg[10]_0 ;
  wire \y_reg[10]_i_1_0 ;
  wire \y_reg[10]_i_2_n_0 ;
  wire \y_reg[10]_i_5_n_0 ;
  wire \y_reg[11] ;
  wire \y_reg[11]_0 ;
  wire \y_reg[11]_i_1_0 ;
  wire \y_reg[11]_i_2_n_0 ;
  wire \y_reg[11]_i_5_n_0 ;
  wire \y_reg[12] ;
  wire \y_reg[12]_0 ;
  wire \y_reg[12]_i_1_0 ;
  wire \y_reg[12]_i_2_n_0 ;
  wire \y_reg[12]_i_5_n_0 ;
  wire \y_reg[13] ;
  wire \y_reg[13]_0 ;
  wire \y_reg[13]_i_1_0 ;
  wire \y_reg[13]_i_2_n_0 ;
  wire \y_reg[13]_i_5_n_0 ;
  wire \y_reg[14] ;
  wire \y_reg[14]_0 ;
  wire \y_reg[14]_i_1_0 ;
  wire \y_reg[14]_i_2_n_0 ;
  wire \y_reg[14]_i_5_n_0 ;
  wire \y_reg[15] ;
  wire \y_reg[15]_0 ;
  wire \y_reg[15]_i_1_0 ;
  wire \y_reg[15]_i_2_n_0 ;
  wire \y_reg[15]_i_5_n_0 ;
  wire \y_reg[16] ;
  wire \y_reg[16]_0 ;
  wire \y_reg[16]_i_1_0 ;
  wire \y_reg[16]_i_2_n_0 ;
  wire \y_reg[16]_i_5_n_0 ;
  wire \y_reg[17] ;
  wire \y_reg[17]_0 ;
  wire \y_reg[17]_i_1_0 ;
  wire \y_reg[17]_i_2_n_0 ;
  wire \y_reg[17]_i_5_n_0 ;
  wire \y_reg[18] ;
  wire \y_reg[18]_0 ;
  wire \y_reg[18]_i_1_0 ;
  wire \y_reg[18]_i_2_n_0 ;
  wire \y_reg[18]_i_5_n_0 ;
  wire \y_reg[19] ;
  wire \y_reg[19]_0 ;
  wire \y_reg[19]_i_1_0 ;
  wire \y_reg[19]_i_2_n_0 ;
  wire \y_reg[19]_i_5_n_0 ;
  wire \y_reg[1] ;
  wire \y_reg[1]_0 ;
  wire \y_reg[1]_i_1_0 ;
  wire \y_reg[1]_i_2_n_0 ;
  wire \y_reg[1]_i_5_n_0 ;
  wire \y_reg[20] ;
  wire \y_reg[20]_0 ;
  wire \y_reg[20]_i_1_0 ;
  wire \y_reg[20]_i_2_n_0 ;
  wire \y_reg[20]_i_5_n_0 ;
  wire \y_reg[21] ;
  wire \y_reg[21]_0 ;
  wire \y_reg[21]_i_1_0 ;
  wire \y_reg[21]_i_2_n_0 ;
  wire \y_reg[21]_i_5_n_0 ;
  wire \y_reg[22] ;
  wire \y_reg[22]_0 ;
  wire \y_reg[22]_i_1_0 ;
  wire \y_reg[22]_i_2_n_0 ;
  wire \y_reg[22]_i_5_n_0 ;
  wire \y_reg[23] ;
  wire \y_reg[23]_0 ;
  wire \y_reg[23]_i_1_0 ;
  wire [23:0]\y_reg[23]_i_3_0 ;
  wire [23:0]\y_reg[23]_i_3_1 ;
  wire \y_reg[23]_i_3_n_0 ;
  wire \y_reg[23]_i_6_n_0 ;
  wire \y_reg[2] ;
  wire \y_reg[2]_0 ;
  wire \y_reg[2]_i_1_0 ;
  wire \y_reg[2]_i_2_n_0 ;
  wire \y_reg[2]_i_5_n_0 ;
  wire \y_reg[3] ;
  wire \y_reg[3]_0 ;
  wire \y_reg[3]_i_1_0 ;
  wire \y_reg[3]_i_2_n_0 ;
  wire \y_reg[3]_i_5_n_0 ;
  wire \y_reg[4] ;
  wire \y_reg[4]_0 ;
  wire \y_reg[4]_i_1_0 ;
  wire \y_reg[4]_i_2_n_0 ;
  wire \y_reg[4]_i_5_n_0 ;
  wire \y_reg[5] ;
  wire \y_reg[5]_0 ;
  wire \y_reg[5]_i_1_0 ;
  wire \y_reg[5]_i_2_n_0 ;
  wire \y_reg[5]_i_5_n_0 ;
  wire \y_reg[6] ;
  wire \y_reg[6]_0 ;
  wire \y_reg[6]_i_1_0 ;
  wire \y_reg[6]_i_2_n_0 ;
  wire \y_reg[6]_i_5_n_0 ;
  wire \y_reg[7] ;
  wire \y_reg[7]_0 ;
  wire \y_reg[7]_i_1_0 ;
  wire \y_reg[7]_i_2_n_0 ;
  wire \y_reg[7]_i_5_n_0 ;
  wire \y_reg[8] ;
  wire \y_reg[8]_0 ;
  wire \y_reg[8]_i_1_0 ;
  wire \y_reg[8]_i_2_n_0 ;
  wire \y_reg[8]_i_5_n_0 ;
  wire \y_reg[9] ;
  wire \y_reg[9]_0 ;
  wire \y_reg[9]_i_1_0 ;
  wire \y_reg[9]_i_2_n_0 ;
  wire \y_reg[9]_i_5_n_0 ;
  wire NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED;
  wire NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED;
  wire NLW_ram_reg_0_DBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_SBITERR_UNCONNECTED;
  wire [31:16]NLW_ram_reg_0_DOADO_UNCONNECTED;
  wire [31:0]NLW_ram_reg_0_DOBDO_UNCONNECTED;
  wire [3:2]NLW_ram_reg_0_DOPADOP_UNCONNECTED;
  wire [3:0]NLW_ram_reg_0_DOPBDOP_UNCONNECTED;
  wire [7:0]NLW_ram_reg_0_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_ram_reg_0_RDADDRECC_UNCONNECTED;
  wire [15:6]NLW_ram_reg_1_DOADO_UNCONNECTED;
  wire [15:0]NLW_ram_reg_1_DOBDO_UNCONNECTED;
  wire [1:0]NLW_ram_reg_1_DOPADOP_UNCONNECTED;
  wire [1:0]NLW_ram_reg_1_DOPBDOP_UNCONNECTED;

  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p2_d16" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "46080" *) 
  (* RTL_RAM_NAME = "ram" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "2047" *) 
  (* bram_slice_begin = "0" *) 
  (* bram_slice_end = "17" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "2047" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "17" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(18),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(18),
    .WRITE_WIDTH_B(0)) 
    ram_reg_0
       (.ADDRARDADDR({1'b1,addr,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CASCADEINA(1'b1),
        .CASCADEINB(1'b0),
        .CASCADEOUTA(NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED),
        .CASCADEOUTB(NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED),
        .CLKARDCLK(m00_axis_aclk),
        .CLKBWRCLK(1'b0),
        .DBITERR(NLW_ram_reg_0_DBITERR_UNCONNECTED),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,s00_axis_tdata[15:0]}),
        .DIBDI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0,s00_axis_tdata[17:16]}),
        .DIPBDIP({1'b1,1'b1,1'b1,1'b1}),
        .DOADO({NLW_ram_reg_0_DOADO_UNCONNECTED[31:16],\_DOUT_1[3]_6 [15:0]}),
        .DOBDO(NLW_ram_reg_0_DOBDO_UNCONNECTED[31:0]),
        .DOPADOP({NLW_ram_reg_0_DOPADOP_UNCONNECTED[3:2],\_DOUT_1[3]_6 [17:16]}),
        .DOPBDOP(NLW_ram_reg_0_DOPBDOP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_ram_reg_0_ECCPARITY_UNCONNECTED[7:0]),
        .ENARDEN(Q),
        .ENBWREN(1'b0),
        .INJECTDBITERR(NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED),
        .INJECTSBITERR(NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED),
        .RDADDRECC(NLW_ram_reg_0_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_ram_reg_0_SBITERR_UNCONNECTED),
        .WEA({ram_reg_0_i_1__3_n_0,ram_reg_0_i_1__3_n_0,ram_reg_0_i_1__3_n_0,ram_reg_0_i_1__3_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
  LUT3 #(
    .INIT(8'h20)) 
    ram_reg_0_i_1__3
       (.I0(Q),
        .I1(s00_axis_tuser),
        .I2(s00_axis_tvalid),
        .O(ram_reg_0_i_1__3_n_0));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d6" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "46080" *) 
  (* RTL_RAM_NAME = "ram" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "2047" *) 
  (* bram_slice_begin = "18" *) 
  (* bram_slice_end = "23" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "2047" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "18" *) 
  (* ram_slice_end = "23" *) 
  RAMB18E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .INIT_A(18'h00000),
    .INIT_B(18'h00000),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(18'h00000),
    .SRVAL_B(18'h00000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(0)) 
    ram_reg_1
       (.ADDRARDADDR({addr,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CLKARDCLK(m00_axis_aclk),
        .CLKBWRCLK(1'b0),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,s00_axis_tdata[23:18]}),
        .DIBDI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0}),
        .DIPBDIP({1'b1,1'b1}),
        .DOADO({NLW_ram_reg_1_DOADO_UNCONNECTED[15:6],\_DOUT_1[3]_6 [23:18]}),
        .DOBDO(NLW_ram_reg_1_DOBDO_UNCONNECTED[15:0]),
        .DOPADOP(NLW_ram_reg_1_DOPADOP_UNCONNECTED[1:0]),
        .DOPBDOP(NLW_ram_reg_1_DOPBDOP_UNCONNECTED[1:0]),
        .ENARDEN(Q),
        .ENBWREN(1'b0),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .WEA({ram_reg_0_i_1__3_n_0,ram_reg_0_i_1__3_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0}));
  LUT6 #(
    .INIT(64'h00000000EEE222E2)) 
    \y_reg[0]_i_1 
       (.I0(\y_reg[0]_i_2_n_0 ),
        .I1(ctrl_mux_out_1[3]),
        .I2(\y_reg[0] ),
        .I3(ctrl_mux_out_1[2]),
        .I4(\y_reg[0]_0 ),
        .I5(ctrl_mux_out_1[4]),
        .O(D[0]));
  MUXF7 \y_reg[0]_i_2 
       (.I0(\y_reg[0]_i_5_n_0 ),
        .I1(\y_reg[0]_i_1_0 ),
        .O(\y_reg[0]_i_2_n_0 ),
        .S(ctrl_mux_out_1[2]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[0]_i_5 
       (.I0(\_DOUT_1[3]_6 [0]),
        .I1(dout0_out[0]),
        .I2(ctrl_mux_out_1[1]),
        .I3(\y_reg[23]_i_3_0 [0]),
        .I4(ctrl_mux_out_1[0]),
        .I5(\y_reg[23]_i_3_1 [0]),
        .O(\y_reg[0]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h00000000EEE222E2)) 
    \y_reg[10]_i_1 
       (.I0(\y_reg[10]_i_2_n_0 ),
        .I1(ctrl_mux_out_1[3]),
        .I2(\y_reg[10] ),
        .I3(ctrl_mux_out_1[2]),
        .I4(\y_reg[10]_0 ),
        .I5(ctrl_mux_out_1[4]),
        .O(D[10]));
  MUXF7 \y_reg[10]_i_2 
       (.I0(\y_reg[10]_i_5_n_0 ),
        .I1(\y_reg[10]_i_1_0 ),
        .O(\y_reg[10]_i_2_n_0 ),
        .S(ctrl_mux_out_1[2]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[10]_i_5 
       (.I0(\_DOUT_1[3]_6 [10]),
        .I1(dout0_out[10]),
        .I2(ctrl_mux_out_1[1]),
        .I3(\y_reg[23]_i_3_0 [10]),
        .I4(ctrl_mux_out_1[0]),
        .I5(\y_reg[23]_i_3_1 [10]),
        .O(\y_reg[10]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h00000000EEE222E2)) 
    \y_reg[11]_i_1 
       (.I0(\y_reg[11]_i_2_n_0 ),
        .I1(ctrl_mux_out_1[3]),
        .I2(\y_reg[11] ),
        .I3(ctrl_mux_out_1[2]),
        .I4(\y_reg[11]_0 ),
        .I5(ctrl_mux_out_1[4]),
        .O(D[11]));
  MUXF7 \y_reg[11]_i_2 
       (.I0(\y_reg[11]_i_5_n_0 ),
        .I1(\y_reg[11]_i_1_0 ),
        .O(\y_reg[11]_i_2_n_0 ),
        .S(ctrl_mux_out_1[2]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[11]_i_5 
       (.I0(\_DOUT_1[3]_6 [11]),
        .I1(dout0_out[11]),
        .I2(ctrl_mux_out_1[1]),
        .I3(\y_reg[23]_i_3_0 [11]),
        .I4(ctrl_mux_out_1[0]),
        .I5(\y_reg[23]_i_3_1 [11]),
        .O(\y_reg[11]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h00000000EEE222E2)) 
    \y_reg[12]_i_1 
       (.I0(\y_reg[12]_i_2_n_0 ),
        .I1(ctrl_mux_out_1[3]),
        .I2(\y_reg[12] ),
        .I3(ctrl_mux_out_1[2]),
        .I4(\y_reg[12]_0 ),
        .I5(ctrl_mux_out_1[4]),
        .O(D[12]));
  MUXF7 \y_reg[12]_i_2 
       (.I0(\y_reg[12]_i_5_n_0 ),
        .I1(\y_reg[12]_i_1_0 ),
        .O(\y_reg[12]_i_2_n_0 ),
        .S(ctrl_mux_out_1[2]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[12]_i_5 
       (.I0(\_DOUT_1[3]_6 [12]),
        .I1(dout0_out[12]),
        .I2(ctrl_mux_out_1[1]),
        .I3(\y_reg[23]_i_3_0 [12]),
        .I4(ctrl_mux_out_1[0]),
        .I5(\y_reg[23]_i_3_1 [12]),
        .O(\y_reg[12]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h00000000EEE222E2)) 
    \y_reg[13]_i_1 
       (.I0(\y_reg[13]_i_2_n_0 ),
        .I1(ctrl_mux_out_1[3]),
        .I2(\y_reg[13] ),
        .I3(ctrl_mux_out_1[2]),
        .I4(\y_reg[13]_0 ),
        .I5(ctrl_mux_out_1[4]),
        .O(D[13]));
  MUXF7 \y_reg[13]_i_2 
       (.I0(\y_reg[13]_i_5_n_0 ),
        .I1(\y_reg[13]_i_1_0 ),
        .O(\y_reg[13]_i_2_n_0 ),
        .S(ctrl_mux_out_1[2]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[13]_i_5 
       (.I0(\_DOUT_1[3]_6 [13]),
        .I1(dout0_out[13]),
        .I2(ctrl_mux_out_1[1]),
        .I3(\y_reg[23]_i_3_0 [13]),
        .I4(ctrl_mux_out_1[0]),
        .I5(\y_reg[23]_i_3_1 [13]),
        .O(\y_reg[13]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h00000000EEE222E2)) 
    \y_reg[14]_i_1 
       (.I0(\y_reg[14]_i_2_n_0 ),
        .I1(ctrl_mux_out_1[3]),
        .I2(\y_reg[14] ),
        .I3(ctrl_mux_out_1[2]),
        .I4(\y_reg[14]_0 ),
        .I5(ctrl_mux_out_1[4]),
        .O(D[14]));
  MUXF7 \y_reg[14]_i_2 
       (.I0(\y_reg[14]_i_5_n_0 ),
        .I1(\y_reg[14]_i_1_0 ),
        .O(\y_reg[14]_i_2_n_0 ),
        .S(ctrl_mux_out_1[2]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[14]_i_5 
       (.I0(\_DOUT_1[3]_6 [14]),
        .I1(dout0_out[14]),
        .I2(ctrl_mux_out_1[1]),
        .I3(\y_reg[23]_i_3_0 [14]),
        .I4(ctrl_mux_out_1[0]),
        .I5(\y_reg[23]_i_3_1 [14]),
        .O(\y_reg[14]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h00000000EEE222E2)) 
    \y_reg[15]_i_1 
       (.I0(\y_reg[15]_i_2_n_0 ),
        .I1(ctrl_mux_out_1[3]),
        .I2(\y_reg[15] ),
        .I3(ctrl_mux_out_1[2]),
        .I4(\y_reg[15]_0 ),
        .I5(ctrl_mux_out_1[4]),
        .O(D[15]));
  MUXF7 \y_reg[15]_i_2 
       (.I0(\y_reg[15]_i_5_n_0 ),
        .I1(\y_reg[15]_i_1_0 ),
        .O(\y_reg[15]_i_2_n_0 ),
        .S(ctrl_mux_out_1[2]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[15]_i_5 
       (.I0(\_DOUT_1[3]_6 [15]),
        .I1(dout0_out[15]),
        .I2(ctrl_mux_out_1[1]),
        .I3(\y_reg[23]_i_3_0 [15]),
        .I4(ctrl_mux_out_1[0]),
        .I5(\y_reg[23]_i_3_1 [15]),
        .O(\y_reg[15]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h00000000EEE222E2)) 
    \y_reg[16]_i_1 
       (.I0(\y_reg[16]_i_2_n_0 ),
        .I1(ctrl_mux_out_1[3]),
        .I2(\y_reg[16] ),
        .I3(ctrl_mux_out_1[2]),
        .I4(\y_reg[16]_0 ),
        .I5(ctrl_mux_out_1[4]),
        .O(D[16]));
  MUXF7 \y_reg[16]_i_2 
       (.I0(\y_reg[16]_i_5_n_0 ),
        .I1(\y_reg[16]_i_1_0 ),
        .O(\y_reg[16]_i_2_n_0 ),
        .S(ctrl_mux_out_1[2]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[16]_i_5 
       (.I0(\_DOUT_1[3]_6 [16]),
        .I1(dout0_out[16]),
        .I2(ctrl_mux_out_1[1]),
        .I3(\y_reg[23]_i_3_0 [16]),
        .I4(ctrl_mux_out_1[0]),
        .I5(\y_reg[23]_i_3_1 [16]),
        .O(\y_reg[16]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h00000000EEE222E2)) 
    \y_reg[17]_i_1 
       (.I0(\y_reg[17]_i_2_n_0 ),
        .I1(ctrl_mux_out_1[3]),
        .I2(\y_reg[17] ),
        .I3(ctrl_mux_out_1[2]),
        .I4(\y_reg[17]_0 ),
        .I5(ctrl_mux_out_1[4]),
        .O(D[17]));
  MUXF7 \y_reg[17]_i_2 
       (.I0(\y_reg[17]_i_5_n_0 ),
        .I1(\y_reg[17]_i_1_0 ),
        .O(\y_reg[17]_i_2_n_0 ),
        .S(ctrl_mux_out_1[2]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[17]_i_5 
       (.I0(\_DOUT_1[3]_6 [17]),
        .I1(dout0_out[17]),
        .I2(ctrl_mux_out_1[1]),
        .I3(\y_reg[23]_i_3_0 [17]),
        .I4(ctrl_mux_out_1[0]),
        .I5(\y_reg[23]_i_3_1 [17]),
        .O(\y_reg[17]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h00000000EEE222E2)) 
    \y_reg[18]_i_1 
       (.I0(\y_reg[18]_i_2_n_0 ),
        .I1(ctrl_mux_out_1[3]),
        .I2(\y_reg[18] ),
        .I3(ctrl_mux_out_1[2]),
        .I4(\y_reg[18]_0 ),
        .I5(ctrl_mux_out_1[4]),
        .O(D[18]));
  MUXF7 \y_reg[18]_i_2 
       (.I0(\y_reg[18]_i_5_n_0 ),
        .I1(\y_reg[18]_i_1_0 ),
        .O(\y_reg[18]_i_2_n_0 ),
        .S(ctrl_mux_out_1[2]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[18]_i_5 
       (.I0(\_DOUT_1[3]_6 [18]),
        .I1(dout0_out[18]),
        .I2(ctrl_mux_out_1[1]),
        .I3(\y_reg[23]_i_3_0 [18]),
        .I4(ctrl_mux_out_1[0]),
        .I5(\y_reg[23]_i_3_1 [18]),
        .O(\y_reg[18]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h00000000EEE222E2)) 
    \y_reg[19]_i_1 
       (.I0(\y_reg[19]_i_2_n_0 ),
        .I1(ctrl_mux_out_1[3]),
        .I2(\y_reg[19] ),
        .I3(ctrl_mux_out_1[2]),
        .I4(\y_reg[19]_0 ),
        .I5(ctrl_mux_out_1[4]),
        .O(D[19]));
  MUXF7 \y_reg[19]_i_2 
       (.I0(\y_reg[19]_i_5_n_0 ),
        .I1(\y_reg[19]_i_1_0 ),
        .O(\y_reg[19]_i_2_n_0 ),
        .S(ctrl_mux_out_1[2]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[19]_i_5 
       (.I0(\_DOUT_1[3]_6 [19]),
        .I1(dout0_out[19]),
        .I2(ctrl_mux_out_1[1]),
        .I3(\y_reg[23]_i_3_0 [19]),
        .I4(ctrl_mux_out_1[0]),
        .I5(\y_reg[23]_i_3_1 [19]),
        .O(\y_reg[19]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h00000000EEE222E2)) 
    \y_reg[1]_i_1 
       (.I0(\y_reg[1]_i_2_n_0 ),
        .I1(ctrl_mux_out_1[3]),
        .I2(\y_reg[1] ),
        .I3(ctrl_mux_out_1[2]),
        .I4(\y_reg[1]_0 ),
        .I5(ctrl_mux_out_1[4]),
        .O(D[1]));
  MUXF7 \y_reg[1]_i_2 
       (.I0(\y_reg[1]_i_5_n_0 ),
        .I1(\y_reg[1]_i_1_0 ),
        .O(\y_reg[1]_i_2_n_0 ),
        .S(ctrl_mux_out_1[2]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[1]_i_5 
       (.I0(\_DOUT_1[3]_6 [1]),
        .I1(dout0_out[1]),
        .I2(ctrl_mux_out_1[1]),
        .I3(\y_reg[23]_i_3_0 [1]),
        .I4(ctrl_mux_out_1[0]),
        .I5(\y_reg[23]_i_3_1 [1]),
        .O(\y_reg[1]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h00000000EEE222E2)) 
    \y_reg[20]_i_1 
       (.I0(\y_reg[20]_i_2_n_0 ),
        .I1(ctrl_mux_out_1[3]),
        .I2(\y_reg[20] ),
        .I3(ctrl_mux_out_1[2]),
        .I4(\y_reg[20]_0 ),
        .I5(ctrl_mux_out_1[4]),
        .O(D[20]));
  MUXF7 \y_reg[20]_i_2 
       (.I0(\y_reg[20]_i_5_n_0 ),
        .I1(\y_reg[20]_i_1_0 ),
        .O(\y_reg[20]_i_2_n_0 ),
        .S(ctrl_mux_out_1[2]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[20]_i_5 
       (.I0(\_DOUT_1[3]_6 [20]),
        .I1(dout0_out[20]),
        .I2(ctrl_mux_out_1[1]),
        .I3(\y_reg[23]_i_3_0 [20]),
        .I4(ctrl_mux_out_1[0]),
        .I5(\y_reg[23]_i_3_1 [20]),
        .O(\y_reg[20]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h00000000EEE222E2)) 
    \y_reg[21]_i_1 
       (.I0(\y_reg[21]_i_2_n_0 ),
        .I1(ctrl_mux_out_1[3]),
        .I2(\y_reg[21] ),
        .I3(ctrl_mux_out_1[2]),
        .I4(\y_reg[21]_0 ),
        .I5(ctrl_mux_out_1[4]),
        .O(D[21]));
  MUXF7 \y_reg[21]_i_2 
       (.I0(\y_reg[21]_i_5_n_0 ),
        .I1(\y_reg[21]_i_1_0 ),
        .O(\y_reg[21]_i_2_n_0 ),
        .S(ctrl_mux_out_1[2]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[21]_i_5 
       (.I0(\_DOUT_1[3]_6 [21]),
        .I1(dout0_out[21]),
        .I2(ctrl_mux_out_1[1]),
        .I3(\y_reg[23]_i_3_0 [21]),
        .I4(ctrl_mux_out_1[0]),
        .I5(\y_reg[23]_i_3_1 [21]),
        .O(\y_reg[21]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h00000000EEE222E2)) 
    \y_reg[22]_i_1 
       (.I0(\y_reg[22]_i_2_n_0 ),
        .I1(ctrl_mux_out_1[3]),
        .I2(\y_reg[22] ),
        .I3(ctrl_mux_out_1[2]),
        .I4(\y_reg[22]_0 ),
        .I5(ctrl_mux_out_1[4]),
        .O(D[22]));
  MUXF7 \y_reg[22]_i_2 
       (.I0(\y_reg[22]_i_5_n_0 ),
        .I1(\y_reg[22]_i_1_0 ),
        .O(\y_reg[22]_i_2_n_0 ),
        .S(ctrl_mux_out_1[2]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[22]_i_5 
       (.I0(\_DOUT_1[3]_6 [22]),
        .I1(dout0_out[22]),
        .I2(ctrl_mux_out_1[1]),
        .I3(\y_reg[23]_i_3_0 [22]),
        .I4(ctrl_mux_out_1[0]),
        .I5(\y_reg[23]_i_3_1 [22]),
        .O(\y_reg[22]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h00000000EEE222E2)) 
    \y_reg[23]_i_1 
       (.I0(\y_reg[23]_i_3_n_0 ),
        .I1(ctrl_mux_out_1[3]),
        .I2(\y_reg[23] ),
        .I3(ctrl_mux_out_1[2]),
        .I4(\y_reg[23]_0 ),
        .I5(ctrl_mux_out_1[4]),
        .O(D[23]));
  MUXF7 \y_reg[23]_i_3 
       (.I0(\y_reg[23]_i_6_n_0 ),
        .I1(\y_reg[23]_i_1_0 ),
        .O(\y_reg[23]_i_3_n_0 ),
        .S(ctrl_mux_out_1[2]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[23]_i_6 
       (.I0(\_DOUT_1[3]_6 [23]),
        .I1(dout0_out[23]),
        .I2(ctrl_mux_out_1[1]),
        .I3(\y_reg[23]_i_3_0 [23]),
        .I4(ctrl_mux_out_1[0]),
        .I5(\y_reg[23]_i_3_1 [23]),
        .O(\y_reg[23]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'h00000000EEE222E2)) 
    \y_reg[2]_i_1 
       (.I0(\y_reg[2]_i_2_n_0 ),
        .I1(ctrl_mux_out_1[3]),
        .I2(\y_reg[2] ),
        .I3(ctrl_mux_out_1[2]),
        .I4(\y_reg[2]_0 ),
        .I5(ctrl_mux_out_1[4]),
        .O(D[2]));
  MUXF7 \y_reg[2]_i_2 
       (.I0(\y_reg[2]_i_5_n_0 ),
        .I1(\y_reg[2]_i_1_0 ),
        .O(\y_reg[2]_i_2_n_0 ),
        .S(ctrl_mux_out_1[2]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[2]_i_5 
       (.I0(\_DOUT_1[3]_6 [2]),
        .I1(dout0_out[2]),
        .I2(ctrl_mux_out_1[1]),
        .I3(\y_reg[23]_i_3_0 [2]),
        .I4(ctrl_mux_out_1[0]),
        .I5(\y_reg[23]_i_3_1 [2]),
        .O(\y_reg[2]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h00000000EEE222E2)) 
    \y_reg[3]_i_1 
       (.I0(\y_reg[3]_i_2_n_0 ),
        .I1(ctrl_mux_out_1[3]),
        .I2(\y_reg[3] ),
        .I3(ctrl_mux_out_1[2]),
        .I4(\y_reg[3]_0 ),
        .I5(ctrl_mux_out_1[4]),
        .O(D[3]));
  MUXF7 \y_reg[3]_i_2 
       (.I0(\y_reg[3]_i_5_n_0 ),
        .I1(\y_reg[3]_i_1_0 ),
        .O(\y_reg[3]_i_2_n_0 ),
        .S(ctrl_mux_out_1[2]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[3]_i_5 
       (.I0(\_DOUT_1[3]_6 [3]),
        .I1(dout0_out[3]),
        .I2(ctrl_mux_out_1[1]),
        .I3(\y_reg[23]_i_3_0 [3]),
        .I4(ctrl_mux_out_1[0]),
        .I5(\y_reg[23]_i_3_1 [3]),
        .O(\y_reg[3]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h00000000EEE222E2)) 
    \y_reg[4]_i_1 
       (.I0(\y_reg[4]_i_2_n_0 ),
        .I1(ctrl_mux_out_1[3]),
        .I2(\y_reg[4] ),
        .I3(ctrl_mux_out_1[2]),
        .I4(\y_reg[4]_0 ),
        .I5(ctrl_mux_out_1[4]),
        .O(D[4]));
  MUXF7 \y_reg[4]_i_2 
       (.I0(\y_reg[4]_i_5_n_0 ),
        .I1(\y_reg[4]_i_1_0 ),
        .O(\y_reg[4]_i_2_n_0 ),
        .S(ctrl_mux_out_1[2]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[4]_i_5 
       (.I0(\_DOUT_1[3]_6 [4]),
        .I1(dout0_out[4]),
        .I2(ctrl_mux_out_1[1]),
        .I3(\y_reg[23]_i_3_0 [4]),
        .I4(ctrl_mux_out_1[0]),
        .I5(\y_reg[23]_i_3_1 [4]),
        .O(\y_reg[4]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h00000000EEE222E2)) 
    \y_reg[5]_i_1 
       (.I0(\y_reg[5]_i_2_n_0 ),
        .I1(ctrl_mux_out_1[3]),
        .I2(\y_reg[5] ),
        .I3(ctrl_mux_out_1[2]),
        .I4(\y_reg[5]_0 ),
        .I5(ctrl_mux_out_1[4]),
        .O(D[5]));
  MUXF7 \y_reg[5]_i_2 
       (.I0(\y_reg[5]_i_5_n_0 ),
        .I1(\y_reg[5]_i_1_0 ),
        .O(\y_reg[5]_i_2_n_0 ),
        .S(ctrl_mux_out_1[2]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[5]_i_5 
       (.I0(\_DOUT_1[3]_6 [5]),
        .I1(dout0_out[5]),
        .I2(ctrl_mux_out_1[1]),
        .I3(\y_reg[23]_i_3_0 [5]),
        .I4(ctrl_mux_out_1[0]),
        .I5(\y_reg[23]_i_3_1 [5]),
        .O(\y_reg[5]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h00000000EEE222E2)) 
    \y_reg[6]_i_1 
       (.I0(\y_reg[6]_i_2_n_0 ),
        .I1(ctrl_mux_out_1[3]),
        .I2(\y_reg[6] ),
        .I3(ctrl_mux_out_1[2]),
        .I4(\y_reg[6]_0 ),
        .I5(ctrl_mux_out_1[4]),
        .O(D[6]));
  MUXF7 \y_reg[6]_i_2 
       (.I0(\y_reg[6]_i_5_n_0 ),
        .I1(\y_reg[6]_i_1_0 ),
        .O(\y_reg[6]_i_2_n_0 ),
        .S(ctrl_mux_out_1[2]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[6]_i_5 
       (.I0(\_DOUT_1[3]_6 [6]),
        .I1(dout0_out[6]),
        .I2(ctrl_mux_out_1[1]),
        .I3(\y_reg[23]_i_3_0 [6]),
        .I4(ctrl_mux_out_1[0]),
        .I5(\y_reg[23]_i_3_1 [6]),
        .O(\y_reg[6]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h00000000EEE222E2)) 
    \y_reg[7]_i_1 
       (.I0(\y_reg[7]_i_2_n_0 ),
        .I1(ctrl_mux_out_1[3]),
        .I2(\y_reg[7] ),
        .I3(ctrl_mux_out_1[2]),
        .I4(\y_reg[7]_0 ),
        .I5(ctrl_mux_out_1[4]),
        .O(D[7]));
  MUXF7 \y_reg[7]_i_2 
       (.I0(\y_reg[7]_i_5_n_0 ),
        .I1(\y_reg[7]_i_1_0 ),
        .O(\y_reg[7]_i_2_n_0 ),
        .S(ctrl_mux_out_1[2]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[7]_i_5 
       (.I0(\_DOUT_1[3]_6 [7]),
        .I1(dout0_out[7]),
        .I2(ctrl_mux_out_1[1]),
        .I3(\y_reg[23]_i_3_0 [7]),
        .I4(ctrl_mux_out_1[0]),
        .I5(\y_reg[23]_i_3_1 [7]),
        .O(\y_reg[7]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h00000000EEE222E2)) 
    \y_reg[8]_i_1 
       (.I0(\y_reg[8]_i_2_n_0 ),
        .I1(ctrl_mux_out_1[3]),
        .I2(\y_reg[8] ),
        .I3(ctrl_mux_out_1[2]),
        .I4(\y_reg[8]_0 ),
        .I5(ctrl_mux_out_1[4]),
        .O(D[8]));
  MUXF7 \y_reg[8]_i_2 
       (.I0(\y_reg[8]_i_5_n_0 ),
        .I1(\y_reg[8]_i_1_0 ),
        .O(\y_reg[8]_i_2_n_0 ),
        .S(ctrl_mux_out_1[2]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[8]_i_5 
       (.I0(\_DOUT_1[3]_6 [8]),
        .I1(dout0_out[8]),
        .I2(ctrl_mux_out_1[1]),
        .I3(\y_reg[23]_i_3_0 [8]),
        .I4(ctrl_mux_out_1[0]),
        .I5(\y_reg[23]_i_3_1 [8]),
        .O(\y_reg[8]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h00000000EEE222E2)) 
    \y_reg[9]_i_1 
       (.I0(\y_reg[9]_i_2_n_0 ),
        .I1(ctrl_mux_out_1[3]),
        .I2(\y_reg[9] ),
        .I3(ctrl_mux_out_1[2]),
        .I4(\y_reg[9]_0 ),
        .I5(ctrl_mux_out_1[4]),
        .O(D[9]));
  MUXF7 \y_reg[9]_i_2 
       (.I0(\y_reg[9]_i_5_n_0 ),
        .I1(\y_reg[9]_i_1_0 ),
        .O(\y_reg[9]_i_2_n_0 ),
        .S(ctrl_mux_out_1[2]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[9]_i_5 
       (.I0(\_DOUT_1[3]_6 [9]),
        .I1(dout0_out[9]),
        .I2(ctrl_mux_out_1[1]),
        .I3(\y_reg[23]_i_3_0 [9]),
        .I4(ctrl_mux_out_1[0]),
        .I5(\y_reg[23]_i_3_1 [9]),
        .O(\y_reg[9]_i_5_n_0 ));
endmodule

(* ORIG_REF_NAME = "rams_sp_rf_rst" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_18
   (D,
    ctrl_mux_out_2,
    \y_reg[0] ,
    \y_reg[0]_0 ,
    \y_reg[1] ,
    \y_reg[1]_0 ,
    \y_reg[2] ,
    \y_reg[2]_0 ,
    \y_reg[3] ,
    \y_reg[3]_0 ,
    \y_reg[4] ,
    \y_reg[4]_0 ,
    \y_reg[5] ,
    \y_reg[5]_0 ,
    \y_reg[6] ,
    \y_reg[6]_0 ,
    \y_reg[7] ,
    \y_reg[7]_0 ,
    \y_reg[8] ,
    \y_reg[8]_0 ,
    \y_reg[9] ,
    \y_reg[9]_0 ,
    \y_reg[10] ,
    \y_reg[10]_0 ,
    \y_reg[11] ,
    \y_reg[11]_0 ,
    \y_reg[12] ,
    \y_reg[12]_0 ,
    \y_reg[13] ,
    \y_reg[13]_0 ,
    \y_reg[14] ,
    \y_reg[14]_0 ,
    \y_reg[15] ,
    \y_reg[15]_0 ,
    \y_reg[16] ,
    \y_reg[16]_0 ,
    \y_reg[17] ,
    \y_reg[17]_0 ,
    \y_reg[18] ,
    \y_reg[18]_0 ,
    \y_reg[19] ,
    \y_reg[19]_0 ,
    \y_reg[20] ,
    \y_reg[20]_0 ,
    \y_reg[21] ,
    \y_reg[21]_0 ,
    \y_reg[22] ,
    \y_reg[22]_0 ,
    \y_reg[23] ,
    \y_reg[23]_0 ,
    \y_reg[0]_i_1__0_0 ,
    dout0_out,
    \y_reg[23]_i_3__0_0 ,
    \y_reg[23]_i_3__0_1 ,
    \y_reg[1]_i_1__0_0 ,
    \y_reg[2]_i_1__0_0 ,
    \y_reg[3]_i_1__0_0 ,
    \y_reg[4]_i_1__0_0 ,
    \y_reg[5]_i_1__0_0 ,
    \y_reg[6]_i_1__0_0 ,
    \y_reg[7]_i_1__0_0 ,
    \y_reg[8]_i_1__0_0 ,
    \y_reg[9]_i_1__0_0 ,
    \y_reg[10]_i_1__0_0 ,
    \y_reg[11]_i_1__0_0 ,
    \y_reg[12]_i_1__0_0 ,
    \y_reg[13]_i_1__0_0 ,
    \y_reg[14]_i_1__0_0 ,
    \y_reg[15]_i_1__0_0 ,
    \y_reg[16]_i_1__0_0 ,
    \y_reg[17]_i_1__0_0 ,
    \y_reg[18]_i_1__0_0 ,
    \y_reg[19]_i_1__0_0 ,
    \y_reg[20]_i_1__0_0 ,
    \y_reg[21]_i_1__0_0 ,
    \y_reg[22]_i_1__0_0 ,
    \y_reg[23]_i_1__0_0 ,
    m00_axis_aclk,
    Q,
    addr,
    s00_axis_tdata,
    s00_axis_tuser,
    s00_axis_tvalid);
  output [23:0]D;
  input [4:0]ctrl_mux_out_2;
  input \y_reg[0] ;
  input \y_reg[0]_0 ;
  input \y_reg[1] ;
  input \y_reg[1]_0 ;
  input \y_reg[2] ;
  input \y_reg[2]_0 ;
  input \y_reg[3] ;
  input \y_reg[3]_0 ;
  input \y_reg[4] ;
  input \y_reg[4]_0 ;
  input \y_reg[5] ;
  input \y_reg[5]_0 ;
  input \y_reg[6] ;
  input \y_reg[6]_0 ;
  input \y_reg[7] ;
  input \y_reg[7]_0 ;
  input \y_reg[8] ;
  input \y_reg[8]_0 ;
  input \y_reg[9] ;
  input \y_reg[9]_0 ;
  input \y_reg[10] ;
  input \y_reg[10]_0 ;
  input \y_reg[11] ;
  input \y_reg[11]_0 ;
  input \y_reg[12] ;
  input \y_reg[12]_0 ;
  input \y_reg[13] ;
  input \y_reg[13]_0 ;
  input \y_reg[14] ;
  input \y_reg[14]_0 ;
  input \y_reg[15] ;
  input \y_reg[15]_0 ;
  input \y_reg[16] ;
  input \y_reg[16]_0 ;
  input \y_reg[17] ;
  input \y_reg[17]_0 ;
  input \y_reg[18] ;
  input \y_reg[18]_0 ;
  input \y_reg[19] ;
  input \y_reg[19]_0 ;
  input \y_reg[20] ;
  input \y_reg[20]_0 ;
  input \y_reg[21] ;
  input \y_reg[21]_0 ;
  input \y_reg[22] ;
  input \y_reg[22]_0 ;
  input \y_reg[23] ;
  input \y_reg[23]_0 ;
  input \y_reg[0]_i_1__0_0 ;
  input [23:0]dout0_out;
  input [23:0]\y_reg[23]_i_3__0_0 ;
  input [23:0]\y_reg[23]_i_3__0_1 ;
  input \y_reg[1]_i_1__0_0 ;
  input \y_reg[2]_i_1__0_0 ;
  input \y_reg[3]_i_1__0_0 ;
  input \y_reg[4]_i_1__0_0 ;
  input \y_reg[5]_i_1__0_0 ;
  input \y_reg[6]_i_1__0_0 ;
  input \y_reg[7]_i_1__0_0 ;
  input \y_reg[8]_i_1__0_0 ;
  input \y_reg[9]_i_1__0_0 ;
  input \y_reg[10]_i_1__0_0 ;
  input \y_reg[11]_i_1__0_0 ;
  input \y_reg[12]_i_1__0_0 ;
  input \y_reg[13]_i_1__0_0 ;
  input \y_reg[14]_i_1__0_0 ;
  input \y_reg[15]_i_1__0_0 ;
  input \y_reg[16]_i_1__0_0 ;
  input \y_reg[17]_i_1__0_0 ;
  input \y_reg[18]_i_1__0_0 ;
  input \y_reg[19]_i_1__0_0 ;
  input \y_reg[20]_i_1__0_0 ;
  input \y_reg[21]_i_1__0_0 ;
  input \y_reg[22]_i_1__0_0 ;
  input \y_reg[23]_i_1__0_0 ;
  input m00_axis_aclk;
  input [0:0]Q;
  input [10:0]addr;
  input [23:0]s00_axis_tdata;
  input [0:0]s00_axis_tuser;
  input s00_axis_tvalid;

  wire [23:0]D;
  wire [0:0]Q;
  wire [23:0]\_DOUT_2[3]_7 ;
  wire [10:0]addr;
  wire [4:0]ctrl_mux_out_2;
  wire [23:0]dout0_out;
  wire m00_axis_aclk;
  wire ram_reg_0_i_1__4_n_0;
  wire [23:0]s00_axis_tdata;
  wire [0:0]s00_axis_tuser;
  wire s00_axis_tvalid;
  wire \y_reg[0] ;
  wire \y_reg[0]_0 ;
  wire \y_reg[0]_i_1__0_0 ;
  wire \y_reg[0]_i_2__0_n_0 ;
  wire \y_reg[0]_i_5__0_n_0 ;
  wire \y_reg[10] ;
  wire \y_reg[10]_0 ;
  wire \y_reg[10]_i_1__0_0 ;
  wire \y_reg[10]_i_2__0_n_0 ;
  wire \y_reg[10]_i_5__0_n_0 ;
  wire \y_reg[11] ;
  wire \y_reg[11]_0 ;
  wire \y_reg[11]_i_1__0_0 ;
  wire \y_reg[11]_i_2__0_n_0 ;
  wire \y_reg[11]_i_5__0_n_0 ;
  wire \y_reg[12] ;
  wire \y_reg[12]_0 ;
  wire \y_reg[12]_i_1__0_0 ;
  wire \y_reg[12]_i_2__0_n_0 ;
  wire \y_reg[12]_i_5__0_n_0 ;
  wire \y_reg[13] ;
  wire \y_reg[13]_0 ;
  wire \y_reg[13]_i_1__0_0 ;
  wire \y_reg[13]_i_2__0_n_0 ;
  wire \y_reg[13]_i_5__0_n_0 ;
  wire \y_reg[14] ;
  wire \y_reg[14]_0 ;
  wire \y_reg[14]_i_1__0_0 ;
  wire \y_reg[14]_i_2__0_n_0 ;
  wire \y_reg[14]_i_5__0_n_0 ;
  wire \y_reg[15] ;
  wire \y_reg[15]_0 ;
  wire \y_reg[15]_i_1__0_0 ;
  wire \y_reg[15]_i_2__0_n_0 ;
  wire \y_reg[15]_i_5__0_n_0 ;
  wire \y_reg[16] ;
  wire \y_reg[16]_0 ;
  wire \y_reg[16]_i_1__0_0 ;
  wire \y_reg[16]_i_2__0_n_0 ;
  wire \y_reg[16]_i_5__0_n_0 ;
  wire \y_reg[17] ;
  wire \y_reg[17]_0 ;
  wire \y_reg[17]_i_1__0_0 ;
  wire \y_reg[17]_i_2__0_n_0 ;
  wire \y_reg[17]_i_5__0_n_0 ;
  wire \y_reg[18] ;
  wire \y_reg[18]_0 ;
  wire \y_reg[18]_i_1__0_0 ;
  wire \y_reg[18]_i_2__0_n_0 ;
  wire \y_reg[18]_i_5__0_n_0 ;
  wire \y_reg[19] ;
  wire \y_reg[19]_0 ;
  wire \y_reg[19]_i_1__0_0 ;
  wire \y_reg[19]_i_2__0_n_0 ;
  wire \y_reg[19]_i_5__0_n_0 ;
  wire \y_reg[1] ;
  wire \y_reg[1]_0 ;
  wire \y_reg[1]_i_1__0_0 ;
  wire \y_reg[1]_i_2__0_n_0 ;
  wire \y_reg[1]_i_5__0_n_0 ;
  wire \y_reg[20] ;
  wire \y_reg[20]_0 ;
  wire \y_reg[20]_i_1__0_0 ;
  wire \y_reg[20]_i_2__0_n_0 ;
  wire \y_reg[20]_i_5__0_n_0 ;
  wire \y_reg[21] ;
  wire \y_reg[21]_0 ;
  wire \y_reg[21]_i_1__0_0 ;
  wire \y_reg[21]_i_2__0_n_0 ;
  wire \y_reg[21]_i_5__0_n_0 ;
  wire \y_reg[22] ;
  wire \y_reg[22]_0 ;
  wire \y_reg[22]_i_1__0_0 ;
  wire \y_reg[22]_i_2__0_n_0 ;
  wire \y_reg[22]_i_5__0_n_0 ;
  wire \y_reg[23] ;
  wire \y_reg[23]_0 ;
  wire \y_reg[23]_i_1__0_0 ;
  wire [23:0]\y_reg[23]_i_3__0_0 ;
  wire [23:0]\y_reg[23]_i_3__0_1 ;
  wire \y_reg[23]_i_3__0_n_0 ;
  wire \y_reg[23]_i_6__0_n_0 ;
  wire \y_reg[2] ;
  wire \y_reg[2]_0 ;
  wire \y_reg[2]_i_1__0_0 ;
  wire \y_reg[2]_i_2__0_n_0 ;
  wire \y_reg[2]_i_5__0_n_0 ;
  wire \y_reg[3] ;
  wire \y_reg[3]_0 ;
  wire \y_reg[3]_i_1__0_0 ;
  wire \y_reg[3]_i_2__0_n_0 ;
  wire \y_reg[3]_i_5__0_n_0 ;
  wire \y_reg[4] ;
  wire \y_reg[4]_0 ;
  wire \y_reg[4]_i_1__0_0 ;
  wire \y_reg[4]_i_2__0_n_0 ;
  wire \y_reg[4]_i_5__0_n_0 ;
  wire \y_reg[5] ;
  wire \y_reg[5]_0 ;
  wire \y_reg[5]_i_1__0_0 ;
  wire \y_reg[5]_i_2__0_n_0 ;
  wire \y_reg[5]_i_5__0_n_0 ;
  wire \y_reg[6] ;
  wire \y_reg[6]_0 ;
  wire \y_reg[6]_i_1__0_0 ;
  wire \y_reg[6]_i_2__0_n_0 ;
  wire \y_reg[6]_i_5__0_n_0 ;
  wire \y_reg[7] ;
  wire \y_reg[7]_0 ;
  wire \y_reg[7]_i_1__0_0 ;
  wire \y_reg[7]_i_2__0_n_0 ;
  wire \y_reg[7]_i_5__0_n_0 ;
  wire \y_reg[8] ;
  wire \y_reg[8]_0 ;
  wire \y_reg[8]_i_1__0_0 ;
  wire \y_reg[8]_i_2__0_n_0 ;
  wire \y_reg[8]_i_5__0_n_0 ;
  wire \y_reg[9] ;
  wire \y_reg[9]_0 ;
  wire \y_reg[9]_i_1__0_0 ;
  wire \y_reg[9]_i_2__0_n_0 ;
  wire \y_reg[9]_i_5__0_n_0 ;
  wire NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED;
  wire NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED;
  wire NLW_ram_reg_0_DBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_SBITERR_UNCONNECTED;
  wire [31:16]NLW_ram_reg_0_DOADO_UNCONNECTED;
  wire [31:0]NLW_ram_reg_0_DOBDO_UNCONNECTED;
  wire [3:2]NLW_ram_reg_0_DOPADOP_UNCONNECTED;
  wire [3:0]NLW_ram_reg_0_DOPBDOP_UNCONNECTED;
  wire [7:0]NLW_ram_reg_0_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_ram_reg_0_RDADDRECC_UNCONNECTED;
  wire [15:6]NLW_ram_reg_1_DOADO_UNCONNECTED;
  wire [15:0]NLW_ram_reg_1_DOBDO_UNCONNECTED;
  wire [1:0]NLW_ram_reg_1_DOPADOP_UNCONNECTED;
  wire [1:0]NLW_ram_reg_1_DOPBDOP_UNCONNECTED;

  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p2_d16" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "46080" *) 
  (* RTL_RAM_NAME = "ram" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "2047" *) 
  (* bram_slice_begin = "0" *) 
  (* bram_slice_end = "17" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "2047" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "17" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(18),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(18),
    .WRITE_WIDTH_B(0)) 
    ram_reg_0
       (.ADDRARDADDR({1'b1,addr,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CASCADEINA(1'b1),
        .CASCADEINB(1'b0),
        .CASCADEOUTA(NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED),
        .CASCADEOUTB(NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED),
        .CLKARDCLK(m00_axis_aclk),
        .CLKBWRCLK(1'b0),
        .DBITERR(NLW_ram_reg_0_DBITERR_UNCONNECTED),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,s00_axis_tdata[15:0]}),
        .DIBDI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0,s00_axis_tdata[17:16]}),
        .DIPBDIP({1'b1,1'b1,1'b1,1'b1}),
        .DOADO({NLW_ram_reg_0_DOADO_UNCONNECTED[31:16],\_DOUT_2[3]_7 [15:0]}),
        .DOBDO(NLW_ram_reg_0_DOBDO_UNCONNECTED[31:0]),
        .DOPADOP({NLW_ram_reg_0_DOPADOP_UNCONNECTED[3:2],\_DOUT_2[3]_7 [17:16]}),
        .DOPBDOP(NLW_ram_reg_0_DOPBDOP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_ram_reg_0_ECCPARITY_UNCONNECTED[7:0]),
        .ENARDEN(Q),
        .ENBWREN(1'b0),
        .INJECTDBITERR(NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED),
        .INJECTSBITERR(NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED),
        .RDADDRECC(NLW_ram_reg_0_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_ram_reg_0_SBITERR_UNCONNECTED),
        .WEA({ram_reg_0_i_1__4_n_0,ram_reg_0_i_1__4_n_0,ram_reg_0_i_1__4_n_0,ram_reg_0_i_1__4_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
  LUT3 #(
    .INIT(8'h80)) 
    ram_reg_0_i_1__4
       (.I0(Q),
        .I1(s00_axis_tuser),
        .I2(s00_axis_tvalid),
        .O(ram_reg_0_i_1__4_n_0));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d6" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "46080" *) 
  (* RTL_RAM_NAME = "ram" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "2047" *) 
  (* bram_slice_begin = "18" *) 
  (* bram_slice_end = "23" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "2047" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "18" *) 
  (* ram_slice_end = "23" *) 
  RAMB18E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .INIT_A(18'h00000),
    .INIT_B(18'h00000),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(18'h00000),
    .SRVAL_B(18'h00000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(0)) 
    ram_reg_1
       (.ADDRARDADDR({addr,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CLKARDCLK(m00_axis_aclk),
        .CLKBWRCLK(1'b0),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,s00_axis_tdata[23:18]}),
        .DIBDI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0}),
        .DIPBDIP({1'b1,1'b1}),
        .DOADO({NLW_ram_reg_1_DOADO_UNCONNECTED[15:6],\_DOUT_2[3]_7 [23:18]}),
        .DOBDO(NLW_ram_reg_1_DOBDO_UNCONNECTED[15:0]),
        .DOPADOP(NLW_ram_reg_1_DOPADOP_UNCONNECTED[1:0]),
        .DOPBDOP(NLW_ram_reg_1_DOPBDOP_UNCONNECTED[1:0]),
        .ENARDEN(Q),
        .ENBWREN(1'b0),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .WEA({ram_reg_0_i_1__4_n_0,ram_reg_0_i_1__4_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0}));
  LUT6 #(
    .INIT(64'h00000000EEE222E2)) 
    \y_reg[0]_i_1__0 
       (.I0(\y_reg[0]_i_2__0_n_0 ),
        .I1(ctrl_mux_out_2[3]),
        .I2(\y_reg[0] ),
        .I3(ctrl_mux_out_2[2]),
        .I4(\y_reg[0]_0 ),
        .I5(ctrl_mux_out_2[4]),
        .O(D[0]));
  MUXF7 \y_reg[0]_i_2__0 
       (.I0(\y_reg[0]_i_5__0_n_0 ),
        .I1(\y_reg[0]_i_1__0_0 ),
        .O(\y_reg[0]_i_2__0_n_0 ),
        .S(ctrl_mux_out_2[2]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[0]_i_5__0 
       (.I0(\_DOUT_2[3]_7 [0]),
        .I1(dout0_out[0]),
        .I2(ctrl_mux_out_2[1]),
        .I3(\y_reg[23]_i_3__0_0 [0]),
        .I4(ctrl_mux_out_2[0]),
        .I5(\y_reg[23]_i_3__0_1 [0]),
        .O(\y_reg[0]_i_5__0_n_0 ));
  LUT6 #(
    .INIT(64'h00000000EEE222E2)) 
    \y_reg[10]_i_1__0 
       (.I0(\y_reg[10]_i_2__0_n_0 ),
        .I1(ctrl_mux_out_2[3]),
        .I2(\y_reg[10] ),
        .I3(ctrl_mux_out_2[2]),
        .I4(\y_reg[10]_0 ),
        .I5(ctrl_mux_out_2[4]),
        .O(D[10]));
  MUXF7 \y_reg[10]_i_2__0 
       (.I0(\y_reg[10]_i_5__0_n_0 ),
        .I1(\y_reg[10]_i_1__0_0 ),
        .O(\y_reg[10]_i_2__0_n_0 ),
        .S(ctrl_mux_out_2[2]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[10]_i_5__0 
       (.I0(\_DOUT_2[3]_7 [10]),
        .I1(dout0_out[10]),
        .I2(ctrl_mux_out_2[1]),
        .I3(\y_reg[23]_i_3__0_0 [10]),
        .I4(ctrl_mux_out_2[0]),
        .I5(\y_reg[23]_i_3__0_1 [10]),
        .O(\y_reg[10]_i_5__0_n_0 ));
  LUT6 #(
    .INIT(64'h00000000EEE222E2)) 
    \y_reg[11]_i_1__0 
       (.I0(\y_reg[11]_i_2__0_n_0 ),
        .I1(ctrl_mux_out_2[3]),
        .I2(\y_reg[11] ),
        .I3(ctrl_mux_out_2[2]),
        .I4(\y_reg[11]_0 ),
        .I5(ctrl_mux_out_2[4]),
        .O(D[11]));
  MUXF7 \y_reg[11]_i_2__0 
       (.I0(\y_reg[11]_i_5__0_n_0 ),
        .I1(\y_reg[11]_i_1__0_0 ),
        .O(\y_reg[11]_i_2__0_n_0 ),
        .S(ctrl_mux_out_2[2]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[11]_i_5__0 
       (.I0(\_DOUT_2[3]_7 [11]),
        .I1(dout0_out[11]),
        .I2(ctrl_mux_out_2[1]),
        .I3(\y_reg[23]_i_3__0_0 [11]),
        .I4(ctrl_mux_out_2[0]),
        .I5(\y_reg[23]_i_3__0_1 [11]),
        .O(\y_reg[11]_i_5__0_n_0 ));
  LUT6 #(
    .INIT(64'h00000000EEE222E2)) 
    \y_reg[12]_i_1__0 
       (.I0(\y_reg[12]_i_2__0_n_0 ),
        .I1(ctrl_mux_out_2[3]),
        .I2(\y_reg[12] ),
        .I3(ctrl_mux_out_2[2]),
        .I4(\y_reg[12]_0 ),
        .I5(ctrl_mux_out_2[4]),
        .O(D[12]));
  MUXF7 \y_reg[12]_i_2__0 
       (.I0(\y_reg[12]_i_5__0_n_0 ),
        .I1(\y_reg[12]_i_1__0_0 ),
        .O(\y_reg[12]_i_2__0_n_0 ),
        .S(ctrl_mux_out_2[2]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[12]_i_5__0 
       (.I0(\_DOUT_2[3]_7 [12]),
        .I1(dout0_out[12]),
        .I2(ctrl_mux_out_2[1]),
        .I3(\y_reg[23]_i_3__0_0 [12]),
        .I4(ctrl_mux_out_2[0]),
        .I5(\y_reg[23]_i_3__0_1 [12]),
        .O(\y_reg[12]_i_5__0_n_0 ));
  LUT6 #(
    .INIT(64'h00000000EEE222E2)) 
    \y_reg[13]_i_1__0 
       (.I0(\y_reg[13]_i_2__0_n_0 ),
        .I1(ctrl_mux_out_2[3]),
        .I2(\y_reg[13] ),
        .I3(ctrl_mux_out_2[2]),
        .I4(\y_reg[13]_0 ),
        .I5(ctrl_mux_out_2[4]),
        .O(D[13]));
  MUXF7 \y_reg[13]_i_2__0 
       (.I0(\y_reg[13]_i_5__0_n_0 ),
        .I1(\y_reg[13]_i_1__0_0 ),
        .O(\y_reg[13]_i_2__0_n_0 ),
        .S(ctrl_mux_out_2[2]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[13]_i_5__0 
       (.I0(\_DOUT_2[3]_7 [13]),
        .I1(dout0_out[13]),
        .I2(ctrl_mux_out_2[1]),
        .I3(\y_reg[23]_i_3__0_0 [13]),
        .I4(ctrl_mux_out_2[0]),
        .I5(\y_reg[23]_i_3__0_1 [13]),
        .O(\y_reg[13]_i_5__0_n_0 ));
  LUT6 #(
    .INIT(64'h00000000EEE222E2)) 
    \y_reg[14]_i_1__0 
       (.I0(\y_reg[14]_i_2__0_n_0 ),
        .I1(ctrl_mux_out_2[3]),
        .I2(\y_reg[14] ),
        .I3(ctrl_mux_out_2[2]),
        .I4(\y_reg[14]_0 ),
        .I5(ctrl_mux_out_2[4]),
        .O(D[14]));
  MUXF7 \y_reg[14]_i_2__0 
       (.I0(\y_reg[14]_i_5__0_n_0 ),
        .I1(\y_reg[14]_i_1__0_0 ),
        .O(\y_reg[14]_i_2__0_n_0 ),
        .S(ctrl_mux_out_2[2]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[14]_i_5__0 
       (.I0(\_DOUT_2[3]_7 [14]),
        .I1(dout0_out[14]),
        .I2(ctrl_mux_out_2[1]),
        .I3(\y_reg[23]_i_3__0_0 [14]),
        .I4(ctrl_mux_out_2[0]),
        .I5(\y_reg[23]_i_3__0_1 [14]),
        .O(\y_reg[14]_i_5__0_n_0 ));
  LUT6 #(
    .INIT(64'h00000000EEE222E2)) 
    \y_reg[15]_i_1__0 
       (.I0(\y_reg[15]_i_2__0_n_0 ),
        .I1(ctrl_mux_out_2[3]),
        .I2(\y_reg[15] ),
        .I3(ctrl_mux_out_2[2]),
        .I4(\y_reg[15]_0 ),
        .I5(ctrl_mux_out_2[4]),
        .O(D[15]));
  MUXF7 \y_reg[15]_i_2__0 
       (.I0(\y_reg[15]_i_5__0_n_0 ),
        .I1(\y_reg[15]_i_1__0_0 ),
        .O(\y_reg[15]_i_2__0_n_0 ),
        .S(ctrl_mux_out_2[2]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[15]_i_5__0 
       (.I0(\_DOUT_2[3]_7 [15]),
        .I1(dout0_out[15]),
        .I2(ctrl_mux_out_2[1]),
        .I3(\y_reg[23]_i_3__0_0 [15]),
        .I4(ctrl_mux_out_2[0]),
        .I5(\y_reg[23]_i_3__0_1 [15]),
        .O(\y_reg[15]_i_5__0_n_0 ));
  LUT6 #(
    .INIT(64'h00000000EEE222E2)) 
    \y_reg[16]_i_1__0 
       (.I0(\y_reg[16]_i_2__0_n_0 ),
        .I1(ctrl_mux_out_2[3]),
        .I2(\y_reg[16] ),
        .I3(ctrl_mux_out_2[2]),
        .I4(\y_reg[16]_0 ),
        .I5(ctrl_mux_out_2[4]),
        .O(D[16]));
  MUXF7 \y_reg[16]_i_2__0 
       (.I0(\y_reg[16]_i_5__0_n_0 ),
        .I1(\y_reg[16]_i_1__0_0 ),
        .O(\y_reg[16]_i_2__0_n_0 ),
        .S(ctrl_mux_out_2[2]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[16]_i_5__0 
       (.I0(\_DOUT_2[3]_7 [16]),
        .I1(dout0_out[16]),
        .I2(ctrl_mux_out_2[1]),
        .I3(\y_reg[23]_i_3__0_0 [16]),
        .I4(ctrl_mux_out_2[0]),
        .I5(\y_reg[23]_i_3__0_1 [16]),
        .O(\y_reg[16]_i_5__0_n_0 ));
  LUT6 #(
    .INIT(64'h00000000EEE222E2)) 
    \y_reg[17]_i_1__0 
       (.I0(\y_reg[17]_i_2__0_n_0 ),
        .I1(ctrl_mux_out_2[3]),
        .I2(\y_reg[17] ),
        .I3(ctrl_mux_out_2[2]),
        .I4(\y_reg[17]_0 ),
        .I5(ctrl_mux_out_2[4]),
        .O(D[17]));
  MUXF7 \y_reg[17]_i_2__0 
       (.I0(\y_reg[17]_i_5__0_n_0 ),
        .I1(\y_reg[17]_i_1__0_0 ),
        .O(\y_reg[17]_i_2__0_n_0 ),
        .S(ctrl_mux_out_2[2]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[17]_i_5__0 
       (.I0(\_DOUT_2[3]_7 [17]),
        .I1(dout0_out[17]),
        .I2(ctrl_mux_out_2[1]),
        .I3(\y_reg[23]_i_3__0_0 [17]),
        .I4(ctrl_mux_out_2[0]),
        .I5(\y_reg[23]_i_3__0_1 [17]),
        .O(\y_reg[17]_i_5__0_n_0 ));
  LUT6 #(
    .INIT(64'h00000000EEE222E2)) 
    \y_reg[18]_i_1__0 
       (.I0(\y_reg[18]_i_2__0_n_0 ),
        .I1(ctrl_mux_out_2[3]),
        .I2(\y_reg[18] ),
        .I3(ctrl_mux_out_2[2]),
        .I4(\y_reg[18]_0 ),
        .I5(ctrl_mux_out_2[4]),
        .O(D[18]));
  MUXF7 \y_reg[18]_i_2__0 
       (.I0(\y_reg[18]_i_5__0_n_0 ),
        .I1(\y_reg[18]_i_1__0_0 ),
        .O(\y_reg[18]_i_2__0_n_0 ),
        .S(ctrl_mux_out_2[2]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[18]_i_5__0 
       (.I0(\_DOUT_2[3]_7 [18]),
        .I1(dout0_out[18]),
        .I2(ctrl_mux_out_2[1]),
        .I3(\y_reg[23]_i_3__0_0 [18]),
        .I4(ctrl_mux_out_2[0]),
        .I5(\y_reg[23]_i_3__0_1 [18]),
        .O(\y_reg[18]_i_5__0_n_0 ));
  LUT6 #(
    .INIT(64'h00000000EEE222E2)) 
    \y_reg[19]_i_1__0 
       (.I0(\y_reg[19]_i_2__0_n_0 ),
        .I1(ctrl_mux_out_2[3]),
        .I2(\y_reg[19] ),
        .I3(ctrl_mux_out_2[2]),
        .I4(\y_reg[19]_0 ),
        .I5(ctrl_mux_out_2[4]),
        .O(D[19]));
  MUXF7 \y_reg[19]_i_2__0 
       (.I0(\y_reg[19]_i_5__0_n_0 ),
        .I1(\y_reg[19]_i_1__0_0 ),
        .O(\y_reg[19]_i_2__0_n_0 ),
        .S(ctrl_mux_out_2[2]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[19]_i_5__0 
       (.I0(\_DOUT_2[3]_7 [19]),
        .I1(dout0_out[19]),
        .I2(ctrl_mux_out_2[1]),
        .I3(\y_reg[23]_i_3__0_0 [19]),
        .I4(ctrl_mux_out_2[0]),
        .I5(\y_reg[23]_i_3__0_1 [19]),
        .O(\y_reg[19]_i_5__0_n_0 ));
  LUT6 #(
    .INIT(64'h00000000EEE222E2)) 
    \y_reg[1]_i_1__0 
       (.I0(\y_reg[1]_i_2__0_n_0 ),
        .I1(ctrl_mux_out_2[3]),
        .I2(\y_reg[1] ),
        .I3(ctrl_mux_out_2[2]),
        .I4(\y_reg[1]_0 ),
        .I5(ctrl_mux_out_2[4]),
        .O(D[1]));
  MUXF7 \y_reg[1]_i_2__0 
       (.I0(\y_reg[1]_i_5__0_n_0 ),
        .I1(\y_reg[1]_i_1__0_0 ),
        .O(\y_reg[1]_i_2__0_n_0 ),
        .S(ctrl_mux_out_2[2]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[1]_i_5__0 
       (.I0(\_DOUT_2[3]_7 [1]),
        .I1(dout0_out[1]),
        .I2(ctrl_mux_out_2[1]),
        .I3(\y_reg[23]_i_3__0_0 [1]),
        .I4(ctrl_mux_out_2[0]),
        .I5(\y_reg[23]_i_3__0_1 [1]),
        .O(\y_reg[1]_i_5__0_n_0 ));
  LUT6 #(
    .INIT(64'h00000000EEE222E2)) 
    \y_reg[20]_i_1__0 
       (.I0(\y_reg[20]_i_2__0_n_0 ),
        .I1(ctrl_mux_out_2[3]),
        .I2(\y_reg[20] ),
        .I3(ctrl_mux_out_2[2]),
        .I4(\y_reg[20]_0 ),
        .I5(ctrl_mux_out_2[4]),
        .O(D[20]));
  MUXF7 \y_reg[20]_i_2__0 
       (.I0(\y_reg[20]_i_5__0_n_0 ),
        .I1(\y_reg[20]_i_1__0_0 ),
        .O(\y_reg[20]_i_2__0_n_0 ),
        .S(ctrl_mux_out_2[2]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[20]_i_5__0 
       (.I0(\_DOUT_2[3]_7 [20]),
        .I1(dout0_out[20]),
        .I2(ctrl_mux_out_2[1]),
        .I3(\y_reg[23]_i_3__0_0 [20]),
        .I4(ctrl_mux_out_2[0]),
        .I5(\y_reg[23]_i_3__0_1 [20]),
        .O(\y_reg[20]_i_5__0_n_0 ));
  LUT6 #(
    .INIT(64'h00000000EEE222E2)) 
    \y_reg[21]_i_1__0 
       (.I0(\y_reg[21]_i_2__0_n_0 ),
        .I1(ctrl_mux_out_2[3]),
        .I2(\y_reg[21] ),
        .I3(ctrl_mux_out_2[2]),
        .I4(\y_reg[21]_0 ),
        .I5(ctrl_mux_out_2[4]),
        .O(D[21]));
  MUXF7 \y_reg[21]_i_2__0 
       (.I0(\y_reg[21]_i_5__0_n_0 ),
        .I1(\y_reg[21]_i_1__0_0 ),
        .O(\y_reg[21]_i_2__0_n_0 ),
        .S(ctrl_mux_out_2[2]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[21]_i_5__0 
       (.I0(\_DOUT_2[3]_7 [21]),
        .I1(dout0_out[21]),
        .I2(ctrl_mux_out_2[1]),
        .I3(\y_reg[23]_i_3__0_0 [21]),
        .I4(ctrl_mux_out_2[0]),
        .I5(\y_reg[23]_i_3__0_1 [21]),
        .O(\y_reg[21]_i_5__0_n_0 ));
  LUT6 #(
    .INIT(64'h00000000EEE222E2)) 
    \y_reg[22]_i_1__0 
       (.I0(\y_reg[22]_i_2__0_n_0 ),
        .I1(ctrl_mux_out_2[3]),
        .I2(\y_reg[22] ),
        .I3(ctrl_mux_out_2[2]),
        .I4(\y_reg[22]_0 ),
        .I5(ctrl_mux_out_2[4]),
        .O(D[22]));
  MUXF7 \y_reg[22]_i_2__0 
       (.I0(\y_reg[22]_i_5__0_n_0 ),
        .I1(\y_reg[22]_i_1__0_0 ),
        .O(\y_reg[22]_i_2__0_n_0 ),
        .S(ctrl_mux_out_2[2]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[22]_i_5__0 
       (.I0(\_DOUT_2[3]_7 [22]),
        .I1(dout0_out[22]),
        .I2(ctrl_mux_out_2[1]),
        .I3(\y_reg[23]_i_3__0_0 [22]),
        .I4(ctrl_mux_out_2[0]),
        .I5(\y_reg[23]_i_3__0_1 [22]),
        .O(\y_reg[22]_i_5__0_n_0 ));
  LUT6 #(
    .INIT(64'h00000000EEE222E2)) 
    \y_reg[23]_i_1__0 
       (.I0(\y_reg[23]_i_3__0_n_0 ),
        .I1(ctrl_mux_out_2[3]),
        .I2(\y_reg[23] ),
        .I3(ctrl_mux_out_2[2]),
        .I4(\y_reg[23]_0 ),
        .I5(ctrl_mux_out_2[4]),
        .O(D[23]));
  MUXF7 \y_reg[23]_i_3__0 
       (.I0(\y_reg[23]_i_6__0_n_0 ),
        .I1(\y_reg[23]_i_1__0_0 ),
        .O(\y_reg[23]_i_3__0_n_0 ),
        .S(ctrl_mux_out_2[2]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[23]_i_6__0 
       (.I0(\_DOUT_2[3]_7 [23]),
        .I1(dout0_out[23]),
        .I2(ctrl_mux_out_2[1]),
        .I3(\y_reg[23]_i_3__0_0 [23]),
        .I4(ctrl_mux_out_2[0]),
        .I5(\y_reg[23]_i_3__0_1 [23]),
        .O(\y_reg[23]_i_6__0_n_0 ));
  LUT6 #(
    .INIT(64'h00000000EEE222E2)) 
    \y_reg[2]_i_1__0 
       (.I0(\y_reg[2]_i_2__0_n_0 ),
        .I1(ctrl_mux_out_2[3]),
        .I2(\y_reg[2] ),
        .I3(ctrl_mux_out_2[2]),
        .I4(\y_reg[2]_0 ),
        .I5(ctrl_mux_out_2[4]),
        .O(D[2]));
  MUXF7 \y_reg[2]_i_2__0 
       (.I0(\y_reg[2]_i_5__0_n_0 ),
        .I1(\y_reg[2]_i_1__0_0 ),
        .O(\y_reg[2]_i_2__0_n_0 ),
        .S(ctrl_mux_out_2[2]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[2]_i_5__0 
       (.I0(\_DOUT_2[3]_7 [2]),
        .I1(dout0_out[2]),
        .I2(ctrl_mux_out_2[1]),
        .I3(\y_reg[23]_i_3__0_0 [2]),
        .I4(ctrl_mux_out_2[0]),
        .I5(\y_reg[23]_i_3__0_1 [2]),
        .O(\y_reg[2]_i_5__0_n_0 ));
  LUT6 #(
    .INIT(64'h00000000EEE222E2)) 
    \y_reg[3]_i_1__0 
       (.I0(\y_reg[3]_i_2__0_n_0 ),
        .I1(ctrl_mux_out_2[3]),
        .I2(\y_reg[3] ),
        .I3(ctrl_mux_out_2[2]),
        .I4(\y_reg[3]_0 ),
        .I5(ctrl_mux_out_2[4]),
        .O(D[3]));
  MUXF7 \y_reg[3]_i_2__0 
       (.I0(\y_reg[3]_i_5__0_n_0 ),
        .I1(\y_reg[3]_i_1__0_0 ),
        .O(\y_reg[3]_i_2__0_n_0 ),
        .S(ctrl_mux_out_2[2]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[3]_i_5__0 
       (.I0(\_DOUT_2[3]_7 [3]),
        .I1(dout0_out[3]),
        .I2(ctrl_mux_out_2[1]),
        .I3(\y_reg[23]_i_3__0_0 [3]),
        .I4(ctrl_mux_out_2[0]),
        .I5(\y_reg[23]_i_3__0_1 [3]),
        .O(\y_reg[3]_i_5__0_n_0 ));
  LUT6 #(
    .INIT(64'h00000000EEE222E2)) 
    \y_reg[4]_i_1__0 
       (.I0(\y_reg[4]_i_2__0_n_0 ),
        .I1(ctrl_mux_out_2[3]),
        .I2(\y_reg[4] ),
        .I3(ctrl_mux_out_2[2]),
        .I4(\y_reg[4]_0 ),
        .I5(ctrl_mux_out_2[4]),
        .O(D[4]));
  MUXF7 \y_reg[4]_i_2__0 
       (.I0(\y_reg[4]_i_5__0_n_0 ),
        .I1(\y_reg[4]_i_1__0_0 ),
        .O(\y_reg[4]_i_2__0_n_0 ),
        .S(ctrl_mux_out_2[2]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[4]_i_5__0 
       (.I0(\_DOUT_2[3]_7 [4]),
        .I1(dout0_out[4]),
        .I2(ctrl_mux_out_2[1]),
        .I3(\y_reg[23]_i_3__0_0 [4]),
        .I4(ctrl_mux_out_2[0]),
        .I5(\y_reg[23]_i_3__0_1 [4]),
        .O(\y_reg[4]_i_5__0_n_0 ));
  LUT6 #(
    .INIT(64'h00000000EEE222E2)) 
    \y_reg[5]_i_1__0 
       (.I0(\y_reg[5]_i_2__0_n_0 ),
        .I1(ctrl_mux_out_2[3]),
        .I2(\y_reg[5] ),
        .I3(ctrl_mux_out_2[2]),
        .I4(\y_reg[5]_0 ),
        .I5(ctrl_mux_out_2[4]),
        .O(D[5]));
  MUXF7 \y_reg[5]_i_2__0 
       (.I0(\y_reg[5]_i_5__0_n_0 ),
        .I1(\y_reg[5]_i_1__0_0 ),
        .O(\y_reg[5]_i_2__0_n_0 ),
        .S(ctrl_mux_out_2[2]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[5]_i_5__0 
       (.I0(\_DOUT_2[3]_7 [5]),
        .I1(dout0_out[5]),
        .I2(ctrl_mux_out_2[1]),
        .I3(\y_reg[23]_i_3__0_0 [5]),
        .I4(ctrl_mux_out_2[0]),
        .I5(\y_reg[23]_i_3__0_1 [5]),
        .O(\y_reg[5]_i_5__0_n_0 ));
  LUT6 #(
    .INIT(64'h00000000EEE222E2)) 
    \y_reg[6]_i_1__0 
       (.I0(\y_reg[6]_i_2__0_n_0 ),
        .I1(ctrl_mux_out_2[3]),
        .I2(\y_reg[6] ),
        .I3(ctrl_mux_out_2[2]),
        .I4(\y_reg[6]_0 ),
        .I5(ctrl_mux_out_2[4]),
        .O(D[6]));
  MUXF7 \y_reg[6]_i_2__0 
       (.I0(\y_reg[6]_i_5__0_n_0 ),
        .I1(\y_reg[6]_i_1__0_0 ),
        .O(\y_reg[6]_i_2__0_n_0 ),
        .S(ctrl_mux_out_2[2]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[6]_i_5__0 
       (.I0(\_DOUT_2[3]_7 [6]),
        .I1(dout0_out[6]),
        .I2(ctrl_mux_out_2[1]),
        .I3(\y_reg[23]_i_3__0_0 [6]),
        .I4(ctrl_mux_out_2[0]),
        .I5(\y_reg[23]_i_3__0_1 [6]),
        .O(\y_reg[6]_i_5__0_n_0 ));
  LUT6 #(
    .INIT(64'h00000000EEE222E2)) 
    \y_reg[7]_i_1__0 
       (.I0(\y_reg[7]_i_2__0_n_0 ),
        .I1(ctrl_mux_out_2[3]),
        .I2(\y_reg[7] ),
        .I3(ctrl_mux_out_2[2]),
        .I4(\y_reg[7]_0 ),
        .I5(ctrl_mux_out_2[4]),
        .O(D[7]));
  MUXF7 \y_reg[7]_i_2__0 
       (.I0(\y_reg[7]_i_5__0_n_0 ),
        .I1(\y_reg[7]_i_1__0_0 ),
        .O(\y_reg[7]_i_2__0_n_0 ),
        .S(ctrl_mux_out_2[2]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[7]_i_5__0 
       (.I0(\_DOUT_2[3]_7 [7]),
        .I1(dout0_out[7]),
        .I2(ctrl_mux_out_2[1]),
        .I3(\y_reg[23]_i_3__0_0 [7]),
        .I4(ctrl_mux_out_2[0]),
        .I5(\y_reg[23]_i_3__0_1 [7]),
        .O(\y_reg[7]_i_5__0_n_0 ));
  LUT6 #(
    .INIT(64'h00000000EEE222E2)) 
    \y_reg[8]_i_1__0 
       (.I0(\y_reg[8]_i_2__0_n_0 ),
        .I1(ctrl_mux_out_2[3]),
        .I2(\y_reg[8] ),
        .I3(ctrl_mux_out_2[2]),
        .I4(\y_reg[8]_0 ),
        .I5(ctrl_mux_out_2[4]),
        .O(D[8]));
  MUXF7 \y_reg[8]_i_2__0 
       (.I0(\y_reg[8]_i_5__0_n_0 ),
        .I1(\y_reg[8]_i_1__0_0 ),
        .O(\y_reg[8]_i_2__0_n_0 ),
        .S(ctrl_mux_out_2[2]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[8]_i_5__0 
       (.I0(\_DOUT_2[3]_7 [8]),
        .I1(dout0_out[8]),
        .I2(ctrl_mux_out_2[1]),
        .I3(\y_reg[23]_i_3__0_0 [8]),
        .I4(ctrl_mux_out_2[0]),
        .I5(\y_reg[23]_i_3__0_1 [8]),
        .O(\y_reg[8]_i_5__0_n_0 ));
  LUT6 #(
    .INIT(64'h00000000EEE222E2)) 
    \y_reg[9]_i_1__0 
       (.I0(\y_reg[9]_i_2__0_n_0 ),
        .I1(ctrl_mux_out_2[3]),
        .I2(\y_reg[9] ),
        .I3(ctrl_mux_out_2[2]),
        .I4(\y_reg[9]_0 ),
        .I5(ctrl_mux_out_2[4]),
        .O(D[9]));
  MUXF7 \y_reg[9]_i_2__0 
       (.I0(\y_reg[9]_i_5__0_n_0 ),
        .I1(\y_reg[9]_i_1__0_0 ),
        .O(\y_reg[9]_i_2__0_n_0 ),
        .S(ctrl_mux_out_2[2]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[9]_i_5__0 
       (.I0(\_DOUT_2[3]_7 [9]),
        .I1(dout0_out[9]),
        .I2(ctrl_mux_out_2[1]),
        .I3(\y_reg[23]_i_3__0_0 [9]),
        .I4(ctrl_mux_out_2[0]),
        .I5(\y_reg[23]_i_3__0_1 [9]),
        .O(\y_reg[9]_i_5__0_n_0 ));
endmodule

(* ORIG_REF_NAME = "rams_sp_rf_rst" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_19
   (dout0_out,
    m00_axis_aclk,
    Q,
    addr,
    s00_axis_tdata,
    s00_axis_tuser,
    s00_axis_tvalid);
  output [23:0]dout0_out;
  input m00_axis_aclk;
  input [0:0]Q;
  input [10:0]addr;
  input [23:0]s00_axis_tdata;
  input [0:0]s00_axis_tuser;
  input s00_axis_tvalid;

  wire [0:0]Q;
  wire [10:0]addr;
  wire [23:0]dout0_out;
  wire m00_axis_aclk;
  wire ram_reg_0_i_1__5_n_0;
  wire [23:0]s00_axis_tdata;
  wire [0:0]s00_axis_tuser;
  wire s00_axis_tvalid;
  wire NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED;
  wire NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED;
  wire NLW_ram_reg_0_DBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_SBITERR_UNCONNECTED;
  wire [31:16]NLW_ram_reg_0_DOADO_UNCONNECTED;
  wire [31:0]NLW_ram_reg_0_DOBDO_UNCONNECTED;
  wire [3:2]NLW_ram_reg_0_DOPADOP_UNCONNECTED;
  wire [3:0]NLW_ram_reg_0_DOPBDOP_UNCONNECTED;
  wire [7:0]NLW_ram_reg_0_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_ram_reg_0_RDADDRECC_UNCONNECTED;
  wire [15:6]NLW_ram_reg_1_DOADO_UNCONNECTED;
  wire [15:0]NLW_ram_reg_1_DOBDO_UNCONNECTED;
  wire [1:0]NLW_ram_reg_1_DOPADOP_UNCONNECTED;
  wire [1:0]NLW_ram_reg_1_DOPBDOP_UNCONNECTED;

  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p2_d16" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "46080" *) 
  (* RTL_RAM_NAME = "ram" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "2047" *) 
  (* bram_slice_begin = "0" *) 
  (* bram_slice_end = "17" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "2047" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "17" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(18),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(18),
    .WRITE_WIDTH_B(0)) 
    ram_reg_0
       (.ADDRARDADDR({1'b1,addr,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CASCADEINA(1'b1),
        .CASCADEINB(1'b0),
        .CASCADEOUTA(NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED),
        .CASCADEOUTB(NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED),
        .CLKARDCLK(m00_axis_aclk),
        .CLKBWRCLK(1'b0),
        .DBITERR(NLW_ram_reg_0_DBITERR_UNCONNECTED),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,s00_axis_tdata[15:0]}),
        .DIBDI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0,s00_axis_tdata[17:16]}),
        .DIPBDIP({1'b1,1'b1,1'b1,1'b1}),
        .DOADO({NLW_ram_reg_0_DOADO_UNCONNECTED[31:16],dout0_out[15:0]}),
        .DOBDO(NLW_ram_reg_0_DOBDO_UNCONNECTED[31:0]),
        .DOPADOP({NLW_ram_reg_0_DOPADOP_UNCONNECTED[3:2],dout0_out[17:16]}),
        .DOPBDOP(NLW_ram_reg_0_DOPBDOP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_ram_reg_0_ECCPARITY_UNCONNECTED[7:0]),
        .ENARDEN(Q),
        .ENBWREN(1'b0),
        .INJECTDBITERR(NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED),
        .INJECTSBITERR(NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED),
        .RDADDRECC(NLW_ram_reg_0_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_ram_reg_0_SBITERR_UNCONNECTED),
        .WEA({ram_reg_0_i_1__5_n_0,ram_reg_0_i_1__5_n_0,ram_reg_0_i_1__5_n_0,ram_reg_0_i_1__5_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
  LUT3 #(
    .INIT(8'h20)) 
    ram_reg_0_i_1__5
       (.I0(Q),
        .I1(s00_axis_tuser),
        .I2(s00_axis_tvalid),
        .O(ram_reg_0_i_1__5_n_0));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d6" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "46080" *) 
  (* RTL_RAM_NAME = "ram" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "2047" *) 
  (* bram_slice_begin = "18" *) 
  (* bram_slice_end = "23" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "2047" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "18" *) 
  (* ram_slice_end = "23" *) 
  RAMB18E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .INIT_A(18'h00000),
    .INIT_B(18'h00000),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(18'h00000),
    .SRVAL_B(18'h00000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(0)) 
    ram_reg_1
       (.ADDRARDADDR({addr,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CLKARDCLK(m00_axis_aclk),
        .CLKBWRCLK(1'b0),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,s00_axis_tdata[23:18]}),
        .DIBDI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0}),
        .DIPBDIP({1'b1,1'b1}),
        .DOADO({NLW_ram_reg_1_DOADO_UNCONNECTED[15:6],dout0_out[23:18]}),
        .DOBDO(NLW_ram_reg_1_DOBDO_UNCONNECTED[15:0]),
        .DOPADOP(NLW_ram_reg_1_DOPADOP_UNCONNECTED[1:0]),
        .DOPBDOP(NLW_ram_reg_1_DOPBDOP_UNCONNECTED[1:0]),
        .ENARDEN(Q),
        .ENBWREN(1'b0),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .WEA({ram_reg_0_i_1__5_n_0,ram_reg_0_i_1__5_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0}));
endmodule

(* ORIG_REF_NAME = "rams_sp_rf_rst" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_2
   (dout0_out,
    m00_axis_aclk,
    Q,
    addr,
    s00_axis_tdata,
    s00_axis_tuser,
    s00_axis_tvalid);
  output [23:0]dout0_out;
  input m00_axis_aclk;
  input [0:0]Q;
  input [10:0]addr;
  input [23:0]s00_axis_tdata;
  input [0:0]s00_axis_tuser;
  input s00_axis_tvalid;

  wire [0:0]Q;
  wire [10:0]addr;
  wire [23:0]dout0_out;
  wire m00_axis_aclk;
  wire ram_reg_0_i_1__18_n_0;
  wire [23:0]s00_axis_tdata;
  wire [0:0]s00_axis_tuser;
  wire s00_axis_tvalid;
  wire NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED;
  wire NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED;
  wire NLW_ram_reg_0_DBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_SBITERR_UNCONNECTED;
  wire [31:16]NLW_ram_reg_0_DOADO_UNCONNECTED;
  wire [31:0]NLW_ram_reg_0_DOBDO_UNCONNECTED;
  wire [3:2]NLW_ram_reg_0_DOPADOP_UNCONNECTED;
  wire [3:0]NLW_ram_reg_0_DOPBDOP_UNCONNECTED;
  wire [7:0]NLW_ram_reg_0_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_ram_reg_0_RDADDRECC_UNCONNECTED;
  wire [15:6]NLW_ram_reg_1_DOADO_UNCONNECTED;
  wire [15:0]NLW_ram_reg_1_DOBDO_UNCONNECTED;
  wire [1:0]NLW_ram_reg_1_DOPADOP_UNCONNECTED;
  wire [1:0]NLW_ram_reg_1_DOPBDOP_UNCONNECTED;

  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p2_d16" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "46080" *) 
  (* RTL_RAM_NAME = "ram" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "2047" *) 
  (* bram_slice_begin = "0" *) 
  (* bram_slice_end = "17" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "2047" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "17" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(18),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(18),
    .WRITE_WIDTH_B(0)) 
    ram_reg_0
       (.ADDRARDADDR({1'b1,addr,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CASCADEINA(1'b1),
        .CASCADEINB(1'b0),
        .CASCADEOUTA(NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED),
        .CASCADEOUTB(NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED),
        .CLKARDCLK(m00_axis_aclk),
        .CLKBWRCLK(1'b0),
        .DBITERR(NLW_ram_reg_0_DBITERR_UNCONNECTED),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,s00_axis_tdata[15:0]}),
        .DIBDI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0,s00_axis_tdata[17:16]}),
        .DIPBDIP({1'b1,1'b1,1'b1,1'b1}),
        .DOADO({NLW_ram_reg_0_DOADO_UNCONNECTED[31:16],dout0_out[15:0]}),
        .DOBDO(NLW_ram_reg_0_DOBDO_UNCONNECTED[31:0]),
        .DOPADOP({NLW_ram_reg_0_DOPADOP_UNCONNECTED[3:2],dout0_out[17:16]}),
        .DOPBDOP(NLW_ram_reg_0_DOPBDOP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_ram_reg_0_ECCPARITY_UNCONNECTED[7:0]),
        .ENARDEN(Q),
        .ENBWREN(1'b0),
        .INJECTDBITERR(NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED),
        .INJECTSBITERR(NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED),
        .RDADDRECC(NLW_ram_reg_0_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_ram_reg_0_SBITERR_UNCONNECTED),
        .WEA({ram_reg_0_i_1__18_n_0,ram_reg_0_i_1__18_n_0,ram_reg_0_i_1__18_n_0,ram_reg_0_i_1__18_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
  LUT3 #(
    .INIT(8'h80)) 
    ram_reg_0_i_1__18
       (.I0(Q),
        .I1(s00_axis_tuser),
        .I2(s00_axis_tvalid),
        .O(ram_reg_0_i_1__18_n_0));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d6" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "46080" *) 
  (* RTL_RAM_NAME = "ram" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "2047" *) 
  (* bram_slice_begin = "18" *) 
  (* bram_slice_end = "23" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "2047" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "18" *) 
  (* ram_slice_end = "23" *) 
  RAMB18E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .INIT_A(18'h00000),
    .INIT_B(18'h00000),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(18'h00000),
    .SRVAL_B(18'h00000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(0)) 
    ram_reg_1
       (.ADDRARDADDR({addr,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CLKARDCLK(m00_axis_aclk),
        .CLKBWRCLK(1'b0),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,s00_axis_tdata[23:18]}),
        .DIBDI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0}),
        .DIPBDIP({1'b1,1'b1}),
        .DOADO({NLW_ram_reg_1_DOADO_UNCONNECTED[15:6],dout0_out[23:18]}),
        .DOBDO(NLW_ram_reg_1_DOBDO_UNCONNECTED[15:0]),
        .DOPADOP(NLW_ram_reg_1_DOPADOP_UNCONNECTED[1:0]),
        .DOPBDOP(NLW_ram_reg_1_DOPBDOP_UNCONNECTED[1:0]),
        .ENARDEN(Q),
        .ENBWREN(1'b0),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .WEA({ram_reg_0_i_1__18_n_0,ram_reg_0_i_1__18_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0}));
endmodule

(* ORIG_REF_NAME = "rams_sp_rf_rst" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_20
   (dout0_out,
    m00_axis_aclk,
    Q,
    addr,
    s00_axis_tdata,
    s00_axis_tuser,
    s00_axis_tvalid);
  output [23:0]dout0_out;
  input m00_axis_aclk;
  input [0:0]Q;
  input [10:0]addr;
  input [23:0]s00_axis_tdata;
  input [0:0]s00_axis_tuser;
  input s00_axis_tvalid;

  wire [0:0]Q;
  wire [10:0]addr;
  wire [23:0]dout0_out;
  wire m00_axis_aclk;
  wire ram_reg_0_i_1__6_n_0;
  wire [23:0]s00_axis_tdata;
  wire [0:0]s00_axis_tuser;
  wire s00_axis_tvalid;
  wire NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED;
  wire NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED;
  wire NLW_ram_reg_0_DBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_SBITERR_UNCONNECTED;
  wire [31:16]NLW_ram_reg_0_DOADO_UNCONNECTED;
  wire [31:0]NLW_ram_reg_0_DOBDO_UNCONNECTED;
  wire [3:2]NLW_ram_reg_0_DOPADOP_UNCONNECTED;
  wire [3:0]NLW_ram_reg_0_DOPBDOP_UNCONNECTED;
  wire [7:0]NLW_ram_reg_0_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_ram_reg_0_RDADDRECC_UNCONNECTED;
  wire [15:6]NLW_ram_reg_1_DOADO_UNCONNECTED;
  wire [15:0]NLW_ram_reg_1_DOBDO_UNCONNECTED;
  wire [1:0]NLW_ram_reg_1_DOPADOP_UNCONNECTED;
  wire [1:0]NLW_ram_reg_1_DOPBDOP_UNCONNECTED;

  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p2_d16" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "46080" *) 
  (* RTL_RAM_NAME = "ram" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "2047" *) 
  (* bram_slice_begin = "0" *) 
  (* bram_slice_end = "17" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "2047" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "17" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(18),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(18),
    .WRITE_WIDTH_B(0)) 
    ram_reg_0
       (.ADDRARDADDR({1'b1,addr,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CASCADEINA(1'b1),
        .CASCADEINB(1'b0),
        .CASCADEOUTA(NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED),
        .CASCADEOUTB(NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED),
        .CLKARDCLK(m00_axis_aclk),
        .CLKBWRCLK(1'b0),
        .DBITERR(NLW_ram_reg_0_DBITERR_UNCONNECTED),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,s00_axis_tdata[15:0]}),
        .DIBDI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0,s00_axis_tdata[17:16]}),
        .DIPBDIP({1'b1,1'b1,1'b1,1'b1}),
        .DOADO({NLW_ram_reg_0_DOADO_UNCONNECTED[31:16],dout0_out[15:0]}),
        .DOBDO(NLW_ram_reg_0_DOBDO_UNCONNECTED[31:0]),
        .DOPADOP({NLW_ram_reg_0_DOPADOP_UNCONNECTED[3:2],dout0_out[17:16]}),
        .DOPBDOP(NLW_ram_reg_0_DOPBDOP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_ram_reg_0_ECCPARITY_UNCONNECTED[7:0]),
        .ENARDEN(Q),
        .ENBWREN(1'b0),
        .INJECTDBITERR(NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED),
        .INJECTSBITERR(NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED),
        .RDADDRECC(NLW_ram_reg_0_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_ram_reg_0_SBITERR_UNCONNECTED),
        .WEA({ram_reg_0_i_1__6_n_0,ram_reg_0_i_1__6_n_0,ram_reg_0_i_1__6_n_0,ram_reg_0_i_1__6_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
  LUT3 #(
    .INIT(8'h80)) 
    ram_reg_0_i_1__6
       (.I0(Q),
        .I1(s00_axis_tuser),
        .I2(s00_axis_tvalid),
        .O(ram_reg_0_i_1__6_n_0));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d6" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "46080" *) 
  (* RTL_RAM_NAME = "ram" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "2047" *) 
  (* bram_slice_begin = "18" *) 
  (* bram_slice_end = "23" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "2047" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "18" *) 
  (* ram_slice_end = "23" *) 
  RAMB18E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .INIT_A(18'h00000),
    .INIT_B(18'h00000),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(18'h00000),
    .SRVAL_B(18'h00000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(0)) 
    ram_reg_1
       (.ADDRARDADDR({addr,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CLKARDCLK(m00_axis_aclk),
        .CLKBWRCLK(1'b0),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,s00_axis_tdata[23:18]}),
        .DIBDI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0}),
        .DIPBDIP({1'b1,1'b1}),
        .DOADO({NLW_ram_reg_1_DOADO_UNCONNECTED[15:6],dout0_out[23:18]}),
        .DOBDO(NLW_ram_reg_1_DOBDO_UNCONNECTED[15:0]),
        .DOPADOP(NLW_ram_reg_1_DOPADOP_UNCONNECTED[1:0]),
        .DOPBDOP(NLW_ram_reg_1_DOPBDOP_UNCONNECTED[1:0]),
        .ENARDEN(Q),
        .ENBWREN(1'b0),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .WEA({ram_reg_0_i_1__6_n_0,ram_reg_0_i_1__6_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0}));
endmodule

(* ORIG_REF_NAME = "rams_sp_rf_rst" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_21
   (dout0_out,
    m00_axis_aclk,
    Q,
    addr,
    s00_axis_tdata,
    s00_axis_tuser,
    s00_axis_tvalid);
  output [23:0]dout0_out;
  input m00_axis_aclk;
  input [0:0]Q;
  input [10:0]addr;
  input [23:0]s00_axis_tdata;
  input [0:0]s00_axis_tuser;
  input s00_axis_tvalid;

  wire [0:0]Q;
  wire [10:0]addr;
  wire [23:0]dout0_out;
  wire m00_axis_aclk;
  wire ram_reg_0_i_1__7_n_0;
  wire [23:0]s00_axis_tdata;
  wire [0:0]s00_axis_tuser;
  wire s00_axis_tvalid;
  wire NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED;
  wire NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED;
  wire NLW_ram_reg_0_DBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_SBITERR_UNCONNECTED;
  wire [31:16]NLW_ram_reg_0_DOADO_UNCONNECTED;
  wire [31:0]NLW_ram_reg_0_DOBDO_UNCONNECTED;
  wire [3:2]NLW_ram_reg_0_DOPADOP_UNCONNECTED;
  wire [3:0]NLW_ram_reg_0_DOPBDOP_UNCONNECTED;
  wire [7:0]NLW_ram_reg_0_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_ram_reg_0_RDADDRECC_UNCONNECTED;
  wire [15:6]NLW_ram_reg_1_DOADO_UNCONNECTED;
  wire [15:0]NLW_ram_reg_1_DOBDO_UNCONNECTED;
  wire [1:0]NLW_ram_reg_1_DOPADOP_UNCONNECTED;
  wire [1:0]NLW_ram_reg_1_DOPBDOP_UNCONNECTED;

  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p2_d16" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "46080" *) 
  (* RTL_RAM_NAME = "ram" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "2047" *) 
  (* bram_slice_begin = "0" *) 
  (* bram_slice_end = "17" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "2047" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "17" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(18),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(18),
    .WRITE_WIDTH_B(0)) 
    ram_reg_0
       (.ADDRARDADDR({1'b1,addr,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CASCADEINA(1'b1),
        .CASCADEINB(1'b0),
        .CASCADEOUTA(NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED),
        .CASCADEOUTB(NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED),
        .CLKARDCLK(m00_axis_aclk),
        .CLKBWRCLK(1'b0),
        .DBITERR(NLW_ram_reg_0_DBITERR_UNCONNECTED),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,s00_axis_tdata[15:0]}),
        .DIBDI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0,s00_axis_tdata[17:16]}),
        .DIPBDIP({1'b1,1'b1,1'b1,1'b1}),
        .DOADO({NLW_ram_reg_0_DOADO_UNCONNECTED[31:16],dout0_out[15:0]}),
        .DOBDO(NLW_ram_reg_0_DOBDO_UNCONNECTED[31:0]),
        .DOPADOP({NLW_ram_reg_0_DOPADOP_UNCONNECTED[3:2],dout0_out[17:16]}),
        .DOPBDOP(NLW_ram_reg_0_DOPBDOP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_ram_reg_0_ECCPARITY_UNCONNECTED[7:0]),
        .ENARDEN(Q),
        .ENBWREN(1'b0),
        .INJECTDBITERR(NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED),
        .INJECTSBITERR(NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED),
        .RDADDRECC(NLW_ram_reg_0_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_ram_reg_0_SBITERR_UNCONNECTED),
        .WEA({ram_reg_0_i_1__7_n_0,ram_reg_0_i_1__7_n_0,ram_reg_0_i_1__7_n_0,ram_reg_0_i_1__7_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
  LUT3 #(
    .INIT(8'h20)) 
    ram_reg_0_i_1__7
       (.I0(Q),
        .I1(s00_axis_tuser),
        .I2(s00_axis_tvalid),
        .O(ram_reg_0_i_1__7_n_0));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d6" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "46080" *) 
  (* RTL_RAM_NAME = "ram" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "2047" *) 
  (* bram_slice_begin = "18" *) 
  (* bram_slice_end = "23" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "2047" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "18" *) 
  (* ram_slice_end = "23" *) 
  RAMB18E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .INIT_A(18'h00000),
    .INIT_B(18'h00000),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(18'h00000),
    .SRVAL_B(18'h00000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(0)) 
    ram_reg_1
       (.ADDRARDADDR({addr,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CLKARDCLK(m00_axis_aclk),
        .CLKBWRCLK(1'b0),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,s00_axis_tdata[23:18]}),
        .DIBDI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0}),
        .DIPBDIP({1'b1,1'b1}),
        .DOADO({NLW_ram_reg_1_DOADO_UNCONNECTED[15:6],dout0_out[23:18]}),
        .DOBDO(NLW_ram_reg_1_DOBDO_UNCONNECTED[15:0]),
        .DOPADOP(NLW_ram_reg_1_DOPADOP_UNCONNECTED[1:0]),
        .DOPBDOP(NLW_ram_reg_1_DOPBDOP_UNCONNECTED[1:0]),
        .ENARDEN(Q),
        .ENBWREN(1'b0),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .WEA({ram_reg_0_i_1__7_n_0,ram_reg_0_i_1__7_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0}));
endmodule

(* ORIG_REF_NAME = "rams_sp_rf_rst" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_22
   (dout0_out,
    m00_axis_aclk,
    Q,
    addr,
    s00_axis_tdata,
    s00_axis_tuser,
    s00_axis_tvalid);
  output [23:0]dout0_out;
  input m00_axis_aclk;
  input [0:0]Q;
  input [10:0]addr;
  input [23:0]s00_axis_tdata;
  input [0:0]s00_axis_tuser;
  input s00_axis_tvalid;

  wire [0:0]Q;
  wire [10:0]addr;
  wire [23:0]dout0_out;
  wire m00_axis_aclk;
  wire ram_reg_0_i_1__8_n_0;
  wire [23:0]s00_axis_tdata;
  wire [0:0]s00_axis_tuser;
  wire s00_axis_tvalid;
  wire NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED;
  wire NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED;
  wire NLW_ram_reg_0_DBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_SBITERR_UNCONNECTED;
  wire [31:16]NLW_ram_reg_0_DOADO_UNCONNECTED;
  wire [31:0]NLW_ram_reg_0_DOBDO_UNCONNECTED;
  wire [3:2]NLW_ram_reg_0_DOPADOP_UNCONNECTED;
  wire [3:0]NLW_ram_reg_0_DOPBDOP_UNCONNECTED;
  wire [7:0]NLW_ram_reg_0_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_ram_reg_0_RDADDRECC_UNCONNECTED;
  wire [15:6]NLW_ram_reg_1_DOADO_UNCONNECTED;
  wire [15:0]NLW_ram_reg_1_DOBDO_UNCONNECTED;
  wire [1:0]NLW_ram_reg_1_DOPADOP_UNCONNECTED;
  wire [1:0]NLW_ram_reg_1_DOPBDOP_UNCONNECTED;

  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p2_d16" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "46080" *) 
  (* RTL_RAM_NAME = "ram" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "2047" *) 
  (* bram_slice_begin = "0" *) 
  (* bram_slice_end = "17" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "2047" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "17" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(18),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(18),
    .WRITE_WIDTH_B(0)) 
    ram_reg_0
       (.ADDRARDADDR({1'b1,addr,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CASCADEINA(1'b1),
        .CASCADEINB(1'b0),
        .CASCADEOUTA(NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED),
        .CASCADEOUTB(NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED),
        .CLKARDCLK(m00_axis_aclk),
        .CLKBWRCLK(1'b0),
        .DBITERR(NLW_ram_reg_0_DBITERR_UNCONNECTED),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,s00_axis_tdata[15:0]}),
        .DIBDI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0,s00_axis_tdata[17:16]}),
        .DIPBDIP({1'b1,1'b1,1'b1,1'b1}),
        .DOADO({NLW_ram_reg_0_DOADO_UNCONNECTED[31:16],dout0_out[15:0]}),
        .DOBDO(NLW_ram_reg_0_DOBDO_UNCONNECTED[31:0]),
        .DOPADOP({NLW_ram_reg_0_DOPADOP_UNCONNECTED[3:2],dout0_out[17:16]}),
        .DOPBDOP(NLW_ram_reg_0_DOPBDOP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_ram_reg_0_ECCPARITY_UNCONNECTED[7:0]),
        .ENARDEN(Q),
        .ENBWREN(1'b0),
        .INJECTDBITERR(NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED),
        .INJECTSBITERR(NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED),
        .RDADDRECC(NLW_ram_reg_0_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_ram_reg_0_SBITERR_UNCONNECTED),
        .WEA({ram_reg_0_i_1__8_n_0,ram_reg_0_i_1__8_n_0,ram_reg_0_i_1__8_n_0,ram_reg_0_i_1__8_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
  LUT3 #(
    .INIT(8'h80)) 
    ram_reg_0_i_1__8
       (.I0(Q),
        .I1(s00_axis_tuser),
        .I2(s00_axis_tvalid),
        .O(ram_reg_0_i_1__8_n_0));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d6" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "46080" *) 
  (* RTL_RAM_NAME = "ram" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "2047" *) 
  (* bram_slice_begin = "18" *) 
  (* bram_slice_end = "23" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "2047" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "18" *) 
  (* ram_slice_end = "23" *) 
  RAMB18E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .INIT_A(18'h00000),
    .INIT_B(18'h00000),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(18'h00000),
    .SRVAL_B(18'h00000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(0)) 
    ram_reg_1
       (.ADDRARDADDR({addr,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CLKARDCLK(m00_axis_aclk),
        .CLKBWRCLK(1'b0),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,s00_axis_tdata[23:18]}),
        .DIBDI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0}),
        .DIPBDIP({1'b1,1'b1}),
        .DOADO({NLW_ram_reg_1_DOADO_UNCONNECTED[15:6],dout0_out[23:18]}),
        .DOBDO(NLW_ram_reg_1_DOBDO_UNCONNECTED[15:0]),
        .DOPADOP(NLW_ram_reg_1_DOPADOP_UNCONNECTED[1:0]),
        .DOPBDOP(NLW_ram_reg_1_DOPBDOP_UNCONNECTED[1:0]),
        .ENARDEN(Q),
        .ENBWREN(1'b0),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .WEA({ram_reg_0_i_1__8_n_0,ram_reg_0_i_1__8_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0}));
endmodule

(* ORIG_REF_NAME = "rams_sp_rf_rst" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_23
   (dout0_out,
    m00_axis_aclk,
    Q,
    addr,
    s00_axis_tdata,
    s00_axis_tuser,
    s00_axis_tvalid);
  output [23:0]dout0_out;
  input m00_axis_aclk;
  input [0:0]Q;
  input [10:0]addr;
  input [23:0]s00_axis_tdata;
  input [0:0]s00_axis_tuser;
  input s00_axis_tvalid;

  wire [0:0]Q;
  wire [10:0]addr;
  wire [23:0]dout0_out;
  wire m00_axis_aclk;
  wire ram_reg_0_i_1__9_n_0;
  wire [23:0]s00_axis_tdata;
  wire [0:0]s00_axis_tuser;
  wire s00_axis_tvalid;
  wire NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED;
  wire NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED;
  wire NLW_ram_reg_0_DBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_SBITERR_UNCONNECTED;
  wire [31:16]NLW_ram_reg_0_DOADO_UNCONNECTED;
  wire [31:0]NLW_ram_reg_0_DOBDO_UNCONNECTED;
  wire [3:2]NLW_ram_reg_0_DOPADOP_UNCONNECTED;
  wire [3:0]NLW_ram_reg_0_DOPBDOP_UNCONNECTED;
  wire [7:0]NLW_ram_reg_0_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_ram_reg_0_RDADDRECC_UNCONNECTED;
  wire [15:6]NLW_ram_reg_1_DOADO_UNCONNECTED;
  wire [15:0]NLW_ram_reg_1_DOBDO_UNCONNECTED;
  wire [1:0]NLW_ram_reg_1_DOPADOP_UNCONNECTED;
  wire [1:0]NLW_ram_reg_1_DOPBDOP_UNCONNECTED;

  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p2_d16" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "46080" *) 
  (* RTL_RAM_NAME = "ram" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "2047" *) 
  (* bram_slice_begin = "0" *) 
  (* bram_slice_end = "17" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "2047" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "17" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(18),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(18),
    .WRITE_WIDTH_B(0)) 
    ram_reg_0
       (.ADDRARDADDR({1'b1,addr,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CASCADEINA(1'b1),
        .CASCADEINB(1'b0),
        .CASCADEOUTA(NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED),
        .CASCADEOUTB(NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED),
        .CLKARDCLK(m00_axis_aclk),
        .CLKBWRCLK(1'b0),
        .DBITERR(NLW_ram_reg_0_DBITERR_UNCONNECTED),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,s00_axis_tdata[15:0]}),
        .DIBDI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0,s00_axis_tdata[17:16]}),
        .DIPBDIP({1'b1,1'b1,1'b1,1'b1}),
        .DOADO({NLW_ram_reg_0_DOADO_UNCONNECTED[31:16],dout0_out[15:0]}),
        .DOBDO(NLW_ram_reg_0_DOBDO_UNCONNECTED[31:0]),
        .DOPADOP({NLW_ram_reg_0_DOPADOP_UNCONNECTED[3:2],dout0_out[17:16]}),
        .DOPBDOP(NLW_ram_reg_0_DOPBDOP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_ram_reg_0_ECCPARITY_UNCONNECTED[7:0]),
        .ENARDEN(Q),
        .ENBWREN(1'b0),
        .INJECTDBITERR(NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED),
        .INJECTSBITERR(NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED),
        .RDADDRECC(NLW_ram_reg_0_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_ram_reg_0_SBITERR_UNCONNECTED),
        .WEA({ram_reg_0_i_1__9_n_0,ram_reg_0_i_1__9_n_0,ram_reg_0_i_1__9_n_0,ram_reg_0_i_1__9_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
  LUT3 #(
    .INIT(8'h20)) 
    ram_reg_0_i_1__9
       (.I0(Q),
        .I1(s00_axis_tuser),
        .I2(s00_axis_tvalid),
        .O(ram_reg_0_i_1__9_n_0));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d6" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "46080" *) 
  (* RTL_RAM_NAME = "ram" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "2047" *) 
  (* bram_slice_begin = "18" *) 
  (* bram_slice_end = "23" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "2047" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "18" *) 
  (* ram_slice_end = "23" *) 
  RAMB18E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .INIT_A(18'h00000),
    .INIT_B(18'h00000),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(18'h00000),
    .SRVAL_B(18'h00000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(0)) 
    ram_reg_1
       (.ADDRARDADDR({addr,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CLKARDCLK(m00_axis_aclk),
        .CLKBWRCLK(1'b0),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,s00_axis_tdata[23:18]}),
        .DIBDI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0}),
        .DIPBDIP({1'b1,1'b1}),
        .DOADO({NLW_ram_reg_1_DOADO_UNCONNECTED[15:6],dout0_out[23:18]}),
        .DOBDO(NLW_ram_reg_1_DOBDO_UNCONNECTED[15:0]),
        .DOPADOP(NLW_ram_reg_1_DOPADOP_UNCONNECTED[1:0]),
        .DOPBDOP(NLW_ram_reg_1_DOPBDOP_UNCONNECTED[1:0]),
        .ENARDEN(Q),
        .ENBWREN(1'b0),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .WEA({ram_reg_0_i_1__9_n_0,ram_reg_0_i_1__9_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0}));
endmodule

(* ORIG_REF_NAME = "rams_sp_rf_rst" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_24
   (dout0_out,
    m00_axis_aclk,
    Q,
    addr,
    s00_axis_tdata,
    s00_axis_tuser,
    s00_axis_tvalid);
  output [23:0]dout0_out;
  input m00_axis_aclk;
  input [0:0]Q;
  input [10:0]addr;
  input [23:0]s00_axis_tdata;
  input [0:0]s00_axis_tuser;
  input s00_axis_tvalid;

  wire [0:0]Q;
  wire [10:0]addr;
  wire [23:0]dout0_out;
  wire m00_axis_aclk;
  wire ram_reg_0_i_1__10_n_0;
  wire [23:0]s00_axis_tdata;
  wire [0:0]s00_axis_tuser;
  wire s00_axis_tvalid;
  wire NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED;
  wire NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED;
  wire NLW_ram_reg_0_DBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_SBITERR_UNCONNECTED;
  wire [31:16]NLW_ram_reg_0_DOADO_UNCONNECTED;
  wire [31:0]NLW_ram_reg_0_DOBDO_UNCONNECTED;
  wire [3:2]NLW_ram_reg_0_DOPADOP_UNCONNECTED;
  wire [3:0]NLW_ram_reg_0_DOPBDOP_UNCONNECTED;
  wire [7:0]NLW_ram_reg_0_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_ram_reg_0_RDADDRECC_UNCONNECTED;
  wire [15:6]NLW_ram_reg_1_DOADO_UNCONNECTED;
  wire [15:0]NLW_ram_reg_1_DOBDO_UNCONNECTED;
  wire [1:0]NLW_ram_reg_1_DOPADOP_UNCONNECTED;
  wire [1:0]NLW_ram_reg_1_DOPBDOP_UNCONNECTED;

  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p2_d16" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "46080" *) 
  (* RTL_RAM_NAME = "ram" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "2047" *) 
  (* bram_slice_begin = "0" *) 
  (* bram_slice_end = "17" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "2047" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "17" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(18),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(18),
    .WRITE_WIDTH_B(0)) 
    ram_reg_0
       (.ADDRARDADDR({1'b1,addr,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CASCADEINA(1'b1),
        .CASCADEINB(1'b0),
        .CASCADEOUTA(NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED),
        .CASCADEOUTB(NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED),
        .CLKARDCLK(m00_axis_aclk),
        .CLKBWRCLK(1'b0),
        .DBITERR(NLW_ram_reg_0_DBITERR_UNCONNECTED),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,s00_axis_tdata[15:0]}),
        .DIBDI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0,s00_axis_tdata[17:16]}),
        .DIPBDIP({1'b1,1'b1,1'b1,1'b1}),
        .DOADO({NLW_ram_reg_0_DOADO_UNCONNECTED[31:16],dout0_out[15:0]}),
        .DOBDO(NLW_ram_reg_0_DOBDO_UNCONNECTED[31:0]),
        .DOPADOP({NLW_ram_reg_0_DOPADOP_UNCONNECTED[3:2],dout0_out[17:16]}),
        .DOPBDOP(NLW_ram_reg_0_DOPBDOP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_ram_reg_0_ECCPARITY_UNCONNECTED[7:0]),
        .ENARDEN(Q),
        .ENBWREN(1'b0),
        .INJECTDBITERR(NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED),
        .INJECTSBITERR(NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED),
        .RDADDRECC(NLW_ram_reg_0_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_ram_reg_0_SBITERR_UNCONNECTED),
        .WEA({ram_reg_0_i_1__10_n_0,ram_reg_0_i_1__10_n_0,ram_reg_0_i_1__10_n_0,ram_reg_0_i_1__10_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
  LUT3 #(
    .INIT(8'h80)) 
    ram_reg_0_i_1__10
       (.I0(Q),
        .I1(s00_axis_tuser),
        .I2(s00_axis_tvalid),
        .O(ram_reg_0_i_1__10_n_0));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d6" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "46080" *) 
  (* RTL_RAM_NAME = "ram" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "2047" *) 
  (* bram_slice_begin = "18" *) 
  (* bram_slice_end = "23" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "2047" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "18" *) 
  (* ram_slice_end = "23" *) 
  RAMB18E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .INIT_A(18'h00000),
    .INIT_B(18'h00000),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(18'h00000),
    .SRVAL_B(18'h00000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(0)) 
    ram_reg_1
       (.ADDRARDADDR({addr,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CLKARDCLK(m00_axis_aclk),
        .CLKBWRCLK(1'b0),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,s00_axis_tdata[23:18]}),
        .DIBDI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0}),
        .DIPBDIP({1'b1,1'b1}),
        .DOADO({NLW_ram_reg_1_DOADO_UNCONNECTED[15:6],dout0_out[23:18]}),
        .DOBDO(NLW_ram_reg_1_DOBDO_UNCONNECTED[15:0]),
        .DOPADOP(NLW_ram_reg_1_DOPADOP_UNCONNECTED[1:0]),
        .DOPBDOP(NLW_ram_reg_1_DOPBDOP_UNCONNECTED[1:0]),
        .ENARDEN(Q),
        .ENBWREN(1'b0),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .WEA({ram_reg_0_i_1__10_n_0,ram_reg_0_i_1__10_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0}));
endmodule

(* ORIG_REF_NAME = "rams_sp_rf_rst" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_25
   (ram_reg_0_0,
    ram_reg_0_1,
    ram_reg_0_2,
    ram_reg_0_3,
    ram_reg_0_4,
    ram_reg_0_5,
    ram_reg_0_6,
    ram_reg_0_7,
    ram_reg_0_8,
    ram_reg_0_9,
    ram_reg_0_10,
    ram_reg_0_11,
    ram_reg_0_12,
    ram_reg_0_13,
    ram_reg_0_14,
    ram_reg_0_15,
    ram_reg_0_16,
    ram_reg_0_17,
    ram_reg_1_0,
    ram_reg_1_1,
    ram_reg_1_2,
    ram_reg_1_3,
    ram_reg_1_4,
    ram_reg_1_5,
    dout0_out,
    ctrl_mux_out_1,
    \y_reg[23]_i_3 ,
    \y_reg[23]_i_3_0 ,
    m00_axis_aclk,
    Q,
    addr,
    s00_axis_tdata,
    s00_axis_tuser,
    s00_axis_tvalid);
  output ram_reg_0_0;
  output ram_reg_0_1;
  output ram_reg_0_2;
  output ram_reg_0_3;
  output ram_reg_0_4;
  output ram_reg_0_5;
  output ram_reg_0_6;
  output ram_reg_0_7;
  output ram_reg_0_8;
  output ram_reg_0_9;
  output ram_reg_0_10;
  output ram_reg_0_11;
  output ram_reg_0_12;
  output ram_reg_0_13;
  output ram_reg_0_14;
  output ram_reg_0_15;
  output ram_reg_0_16;
  output ram_reg_0_17;
  output ram_reg_1_0;
  output ram_reg_1_1;
  output ram_reg_1_2;
  output ram_reg_1_3;
  output ram_reg_1_4;
  output ram_reg_1_5;
  input [23:0]dout0_out;
  input [1:0]ctrl_mux_out_1;
  input [23:0]\y_reg[23]_i_3 ;
  input [23:0]\y_reg[23]_i_3_0 ;
  input m00_axis_aclk;
  input [0:0]Q;
  input [10:0]addr;
  input [23:0]s00_axis_tdata;
  input [0:0]s00_axis_tuser;
  input s00_axis_tvalid;

  wire [0:0]Q;
  wire [23:0]\_DOUT_1[7]_14 ;
  wire [10:0]addr;
  wire [1:0]ctrl_mux_out_1;
  wire [23:0]dout0_out;
  wire m00_axis_aclk;
  wire ram_reg_0_0;
  wire ram_reg_0_1;
  wire ram_reg_0_10;
  wire ram_reg_0_11;
  wire ram_reg_0_12;
  wire ram_reg_0_13;
  wire ram_reg_0_14;
  wire ram_reg_0_15;
  wire ram_reg_0_16;
  wire ram_reg_0_17;
  wire ram_reg_0_2;
  wire ram_reg_0_3;
  wire ram_reg_0_4;
  wire ram_reg_0_5;
  wire ram_reg_0_6;
  wire ram_reg_0_7;
  wire ram_reg_0_8;
  wire ram_reg_0_9;
  wire ram_reg_0_i_1__11_n_0;
  wire ram_reg_1_0;
  wire ram_reg_1_1;
  wire ram_reg_1_2;
  wire ram_reg_1_3;
  wire ram_reg_1_4;
  wire ram_reg_1_5;
  wire [23:0]s00_axis_tdata;
  wire [0:0]s00_axis_tuser;
  wire s00_axis_tvalid;
  wire [23:0]\y_reg[23]_i_3 ;
  wire [23:0]\y_reg[23]_i_3_0 ;
  wire NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED;
  wire NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED;
  wire NLW_ram_reg_0_DBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_SBITERR_UNCONNECTED;
  wire [31:16]NLW_ram_reg_0_DOADO_UNCONNECTED;
  wire [31:0]NLW_ram_reg_0_DOBDO_UNCONNECTED;
  wire [3:2]NLW_ram_reg_0_DOPADOP_UNCONNECTED;
  wire [3:0]NLW_ram_reg_0_DOPBDOP_UNCONNECTED;
  wire [7:0]NLW_ram_reg_0_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_ram_reg_0_RDADDRECC_UNCONNECTED;
  wire [15:6]NLW_ram_reg_1_DOADO_UNCONNECTED;
  wire [15:0]NLW_ram_reg_1_DOBDO_UNCONNECTED;
  wire [1:0]NLW_ram_reg_1_DOPADOP_UNCONNECTED;
  wire [1:0]NLW_ram_reg_1_DOPBDOP_UNCONNECTED;

  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p2_d16" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "46080" *) 
  (* RTL_RAM_NAME = "ram" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "2047" *) 
  (* bram_slice_begin = "0" *) 
  (* bram_slice_end = "17" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "2047" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "17" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(18),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(18),
    .WRITE_WIDTH_B(0)) 
    ram_reg_0
       (.ADDRARDADDR({1'b1,addr,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CASCADEINA(1'b1),
        .CASCADEINB(1'b0),
        .CASCADEOUTA(NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED),
        .CASCADEOUTB(NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED),
        .CLKARDCLK(m00_axis_aclk),
        .CLKBWRCLK(1'b0),
        .DBITERR(NLW_ram_reg_0_DBITERR_UNCONNECTED),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,s00_axis_tdata[15:0]}),
        .DIBDI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0,s00_axis_tdata[17:16]}),
        .DIPBDIP({1'b1,1'b1,1'b1,1'b1}),
        .DOADO({NLW_ram_reg_0_DOADO_UNCONNECTED[31:16],\_DOUT_1[7]_14 [15:0]}),
        .DOBDO(NLW_ram_reg_0_DOBDO_UNCONNECTED[31:0]),
        .DOPADOP({NLW_ram_reg_0_DOPADOP_UNCONNECTED[3:2],\_DOUT_1[7]_14 [17:16]}),
        .DOPBDOP(NLW_ram_reg_0_DOPBDOP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_ram_reg_0_ECCPARITY_UNCONNECTED[7:0]),
        .ENARDEN(Q),
        .ENBWREN(1'b0),
        .INJECTDBITERR(NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED),
        .INJECTSBITERR(NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED),
        .RDADDRECC(NLW_ram_reg_0_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_ram_reg_0_SBITERR_UNCONNECTED),
        .WEA({ram_reg_0_i_1__11_n_0,ram_reg_0_i_1__11_n_0,ram_reg_0_i_1__11_n_0,ram_reg_0_i_1__11_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
  LUT3 #(
    .INIT(8'h20)) 
    ram_reg_0_i_1__11
       (.I0(Q),
        .I1(s00_axis_tuser),
        .I2(s00_axis_tvalid),
        .O(ram_reg_0_i_1__11_n_0));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d6" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "46080" *) 
  (* RTL_RAM_NAME = "ram" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "2047" *) 
  (* bram_slice_begin = "18" *) 
  (* bram_slice_end = "23" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "2047" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "18" *) 
  (* ram_slice_end = "23" *) 
  RAMB18E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .INIT_A(18'h00000),
    .INIT_B(18'h00000),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(18'h00000),
    .SRVAL_B(18'h00000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(0)) 
    ram_reg_1
       (.ADDRARDADDR({addr,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CLKARDCLK(m00_axis_aclk),
        .CLKBWRCLK(1'b0),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,s00_axis_tdata[23:18]}),
        .DIBDI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0}),
        .DIPBDIP({1'b1,1'b1}),
        .DOADO({NLW_ram_reg_1_DOADO_UNCONNECTED[15:6],\_DOUT_1[7]_14 [23:18]}),
        .DOBDO(NLW_ram_reg_1_DOBDO_UNCONNECTED[15:0]),
        .DOPADOP(NLW_ram_reg_1_DOPADOP_UNCONNECTED[1:0]),
        .DOPBDOP(NLW_ram_reg_1_DOPBDOP_UNCONNECTED[1:0]),
        .ENARDEN(Q),
        .ENBWREN(1'b0),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .WEA({ram_reg_0_i_1__11_n_0,ram_reg_0_i_1__11_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0}));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[0]_i_6 
       (.I0(\_DOUT_1[7]_14 [0]),
        .I1(dout0_out[0]),
        .I2(ctrl_mux_out_1[1]),
        .I3(\y_reg[23]_i_3 [0]),
        .I4(ctrl_mux_out_1[0]),
        .I5(\y_reg[23]_i_3_0 [0]),
        .O(ram_reg_0_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[10]_i_6 
       (.I0(\_DOUT_1[7]_14 [10]),
        .I1(dout0_out[10]),
        .I2(ctrl_mux_out_1[1]),
        .I3(\y_reg[23]_i_3 [10]),
        .I4(ctrl_mux_out_1[0]),
        .I5(\y_reg[23]_i_3_0 [10]),
        .O(ram_reg_0_10));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[11]_i_6 
       (.I0(\_DOUT_1[7]_14 [11]),
        .I1(dout0_out[11]),
        .I2(ctrl_mux_out_1[1]),
        .I3(\y_reg[23]_i_3 [11]),
        .I4(ctrl_mux_out_1[0]),
        .I5(\y_reg[23]_i_3_0 [11]),
        .O(ram_reg_0_11));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[12]_i_6 
       (.I0(\_DOUT_1[7]_14 [12]),
        .I1(dout0_out[12]),
        .I2(ctrl_mux_out_1[1]),
        .I3(\y_reg[23]_i_3 [12]),
        .I4(ctrl_mux_out_1[0]),
        .I5(\y_reg[23]_i_3_0 [12]),
        .O(ram_reg_0_12));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[13]_i_6 
       (.I0(\_DOUT_1[7]_14 [13]),
        .I1(dout0_out[13]),
        .I2(ctrl_mux_out_1[1]),
        .I3(\y_reg[23]_i_3 [13]),
        .I4(ctrl_mux_out_1[0]),
        .I5(\y_reg[23]_i_3_0 [13]),
        .O(ram_reg_0_13));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[14]_i_6 
       (.I0(\_DOUT_1[7]_14 [14]),
        .I1(dout0_out[14]),
        .I2(ctrl_mux_out_1[1]),
        .I3(\y_reg[23]_i_3 [14]),
        .I4(ctrl_mux_out_1[0]),
        .I5(\y_reg[23]_i_3_0 [14]),
        .O(ram_reg_0_14));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[15]_i_6 
       (.I0(\_DOUT_1[7]_14 [15]),
        .I1(dout0_out[15]),
        .I2(ctrl_mux_out_1[1]),
        .I3(\y_reg[23]_i_3 [15]),
        .I4(ctrl_mux_out_1[0]),
        .I5(\y_reg[23]_i_3_0 [15]),
        .O(ram_reg_0_15));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[16]_i_6 
       (.I0(\_DOUT_1[7]_14 [16]),
        .I1(dout0_out[16]),
        .I2(ctrl_mux_out_1[1]),
        .I3(\y_reg[23]_i_3 [16]),
        .I4(ctrl_mux_out_1[0]),
        .I5(\y_reg[23]_i_3_0 [16]),
        .O(ram_reg_0_16));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[17]_i_6 
       (.I0(\_DOUT_1[7]_14 [17]),
        .I1(dout0_out[17]),
        .I2(ctrl_mux_out_1[1]),
        .I3(\y_reg[23]_i_3 [17]),
        .I4(ctrl_mux_out_1[0]),
        .I5(\y_reg[23]_i_3_0 [17]),
        .O(ram_reg_0_17));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[18]_i_6 
       (.I0(\_DOUT_1[7]_14 [18]),
        .I1(dout0_out[18]),
        .I2(ctrl_mux_out_1[1]),
        .I3(\y_reg[23]_i_3 [18]),
        .I4(ctrl_mux_out_1[0]),
        .I5(\y_reg[23]_i_3_0 [18]),
        .O(ram_reg_1_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[19]_i_6 
       (.I0(\_DOUT_1[7]_14 [19]),
        .I1(dout0_out[19]),
        .I2(ctrl_mux_out_1[1]),
        .I3(\y_reg[23]_i_3 [19]),
        .I4(ctrl_mux_out_1[0]),
        .I5(\y_reg[23]_i_3_0 [19]),
        .O(ram_reg_1_1));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[1]_i_6 
       (.I0(\_DOUT_1[7]_14 [1]),
        .I1(dout0_out[1]),
        .I2(ctrl_mux_out_1[1]),
        .I3(\y_reg[23]_i_3 [1]),
        .I4(ctrl_mux_out_1[0]),
        .I5(\y_reg[23]_i_3_0 [1]),
        .O(ram_reg_0_1));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[20]_i_6 
       (.I0(\_DOUT_1[7]_14 [20]),
        .I1(dout0_out[20]),
        .I2(ctrl_mux_out_1[1]),
        .I3(\y_reg[23]_i_3 [20]),
        .I4(ctrl_mux_out_1[0]),
        .I5(\y_reg[23]_i_3_0 [20]),
        .O(ram_reg_1_2));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[21]_i_6 
       (.I0(\_DOUT_1[7]_14 [21]),
        .I1(dout0_out[21]),
        .I2(ctrl_mux_out_1[1]),
        .I3(\y_reg[23]_i_3 [21]),
        .I4(ctrl_mux_out_1[0]),
        .I5(\y_reg[23]_i_3_0 [21]),
        .O(ram_reg_1_3));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[22]_i_6 
       (.I0(\_DOUT_1[7]_14 [22]),
        .I1(dout0_out[22]),
        .I2(ctrl_mux_out_1[1]),
        .I3(\y_reg[23]_i_3 [22]),
        .I4(ctrl_mux_out_1[0]),
        .I5(\y_reg[23]_i_3_0 [22]),
        .O(ram_reg_1_4));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[23]_i_7 
       (.I0(\_DOUT_1[7]_14 [23]),
        .I1(dout0_out[23]),
        .I2(ctrl_mux_out_1[1]),
        .I3(\y_reg[23]_i_3 [23]),
        .I4(ctrl_mux_out_1[0]),
        .I5(\y_reg[23]_i_3_0 [23]),
        .O(ram_reg_1_5));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[2]_i_6 
       (.I0(\_DOUT_1[7]_14 [2]),
        .I1(dout0_out[2]),
        .I2(ctrl_mux_out_1[1]),
        .I3(\y_reg[23]_i_3 [2]),
        .I4(ctrl_mux_out_1[0]),
        .I5(\y_reg[23]_i_3_0 [2]),
        .O(ram_reg_0_2));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[3]_i_6 
       (.I0(\_DOUT_1[7]_14 [3]),
        .I1(dout0_out[3]),
        .I2(ctrl_mux_out_1[1]),
        .I3(\y_reg[23]_i_3 [3]),
        .I4(ctrl_mux_out_1[0]),
        .I5(\y_reg[23]_i_3_0 [3]),
        .O(ram_reg_0_3));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[4]_i_6 
       (.I0(\_DOUT_1[7]_14 [4]),
        .I1(dout0_out[4]),
        .I2(ctrl_mux_out_1[1]),
        .I3(\y_reg[23]_i_3 [4]),
        .I4(ctrl_mux_out_1[0]),
        .I5(\y_reg[23]_i_3_0 [4]),
        .O(ram_reg_0_4));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[5]_i_6 
       (.I0(\_DOUT_1[7]_14 [5]),
        .I1(dout0_out[5]),
        .I2(ctrl_mux_out_1[1]),
        .I3(\y_reg[23]_i_3 [5]),
        .I4(ctrl_mux_out_1[0]),
        .I5(\y_reg[23]_i_3_0 [5]),
        .O(ram_reg_0_5));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[6]_i_6 
       (.I0(\_DOUT_1[7]_14 [6]),
        .I1(dout0_out[6]),
        .I2(ctrl_mux_out_1[1]),
        .I3(\y_reg[23]_i_3 [6]),
        .I4(ctrl_mux_out_1[0]),
        .I5(\y_reg[23]_i_3_0 [6]),
        .O(ram_reg_0_6));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[7]_i_6 
       (.I0(\_DOUT_1[7]_14 [7]),
        .I1(dout0_out[7]),
        .I2(ctrl_mux_out_1[1]),
        .I3(\y_reg[23]_i_3 [7]),
        .I4(ctrl_mux_out_1[0]),
        .I5(\y_reg[23]_i_3_0 [7]),
        .O(ram_reg_0_7));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[8]_i_6 
       (.I0(\_DOUT_1[7]_14 [8]),
        .I1(dout0_out[8]),
        .I2(ctrl_mux_out_1[1]),
        .I3(\y_reg[23]_i_3 [8]),
        .I4(ctrl_mux_out_1[0]),
        .I5(\y_reg[23]_i_3_0 [8]),
        .O(ram_reg_0_8));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[9]_i_6 
       (.I0(\_DOUT_1[7]_14 [9]),
        .I1(dout0_out[9]),
        .I2(ctrl_mux_out_1[1]),
        .I3(\y_reg[23]_i_3 [9]),
        .I4(ctrl_mux_out_1[0]),
        .I5(\y_reg[23]_i_3_0 [9]),
        .O(ram_reg_0_9));
endmodule

(* ORIG_REF_NAME = "rams_sp_rf_rst" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_26
   (ram_reg_0_0,
    ram_reg_0_1,
    ram_reg_0_2,
    ram_reg_0_3,
    ram_reg_0_4,
    ram_reg_0_5,
    ram_reg_0_6,
    ram_reg_0_7,
    ram_reg_0_8,
    ram_reg_0_9,
    ram_reg_0_10,
    ram_reg_0_11,
    ram_reg_0_12,
    ram_reg_0_13,
    ram_reg_0_14,
    ram_reg_0_15,
    ram_reg_0_16,
    ram_reg_0_17,
    ram_reg_1_0,
    ram_reg_1_1,
    ram_reg_1_2,
    ram_reg_1_3,
    ram_reg_1_4,
    ram_reg_1_5,
    dout0_out,
    ctrl_mux_out_2,
    \y_reg[23]_i_3__0 ,
    \y_reg[23]_i_3__0_0 ,
    m00_axis_aclk,
    Q,
    addr,
    s00_axis_tdata,
    s00_axis_tuser,
    s00_axis_tvalid);
  output ram_reg_0_0;
  output ram_reg_0_1;
  output ram_reg_0_2;
  output ram_reg_0_3;
  output ram_reg_0_4;
  output ram_reg_0_5;
  output ram_reg_0_6;
  output ram_reg_0_7;
  output ram_reg_0_8;
  output ram_reg_0_9;
  output ram_reg_0_10;
  output ram_reg_0_11;
  output ram_reg_0_12;
  output ram_reg_0_13;
  output ram_reg_0_14;
  output ram_reg_0_15;
  output ram_reg_0_16;
  output ram_reg_0_17;
  output ram_reg_1_0;
  output ram_reg_1_1;
  output ram_reg_1_2;
  output ram_reg_1_3;
  output ram_reg_1_4;
  output ram_reg_1_5;
  input [23:0]dout0_out;
  input [1:0]ctrl_mux_out_2;
  input [23:0]\y_reg[23]_i_3__0 ;
  input [23:0]\y_reg[23]_i_3__0_0 ;
  input m00_axis_aclk;
  input [0:0]Q;
  input [10:0]addr;
  input [23:0]s00_axis_tdata;
  input [0:0]s00_axis_tuser;
  input s00_axis_tvalid;

  wire [0:0]Q;
  wire [23:0]\_DOUT_2[7]_15 ;
  wire [10:0]addr;
  wire [1:0]ctrl_mux_out_2;
  wire [23:0]dout0_out;
  wire m00_axis_aclk;
  wire ram_reg_0_0;
  wire ram_reg_0_1;
  wire ram_reg_0_10;
  wire ram_reg_0_11;
  wire ram_reg_0_12;
  wire ram_reg_0_13;
  wire ram_reg_0_14;
  wire ram_reg_0_15;
  wire ram_reg_0_16;
  wire ram_reg_0_17;
  wire ram_reg_0_2;
  wire ram_reg_0_3;
  wire ram_reg_0_4;
  wire ram_reg_0_5;
  wire ram_reg_0_6;
  wire ram_reg_0_7;
  wire ram_reg_0_8;
  wire ram_reg_0_9;
  wire ram_reg_0_i_1__12_n_0;
  wire ram_reg_1_0;
  wire ram_reg_1_1;
  wire ram_reg_1_2;
  wire ram_reg_1_3;
  wire ram_reg_1_4;
  wire ram_reg_1_5;
  wire [23:0]s00_axis_tdata;
  wire [0:0]s00_axis_tuser;
  wire s00_axis_tvalid;
  wire [23:0]\y_reg[23]_i_3__0 ;
  wire [23:0]\y_reg[23]_i_3__0_0 ;
  wire NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED;
  wire NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED;
  wire NLW_ram_reg_0_DBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_SBITERR_UNCONNECTED;
  wire [31:16]NLW_ram_reg_0_DOADO_UNCONNECTED;
  wire [31:0]NLW_ram_reg_0_DOBDO_UNCONNECTED;
  wire [3:2]NLW_ram_reg_0_DOPADOP_UNCONNECTED;
  wire [3:0]NLW_ram_reg_0_DOPBDOP_UNCONNECTED;
  wire [7:0]NLW_ram_reg_0_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_ram_reg_0_RDADDRECC_UNCONNECTED;
  wire [15:6]NLW_ram_reg_1_DOADO_UNCONNECTED;
  wire [15:0]NLW_ram_reg_1_DOBDO_UNCONNECTED;
  wire [1:0]NLW_ram_reg_1_DOPADOP_UNCONNECTED;
  wire [1:0]NLW_ram_reg_1_DOPBDOP_UNCONNECTED;

  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p2_d16" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "46080" *) 
  (* RTL_RAM_NAME = "ram" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "2047" *) 
  (* bram_slice_begin = "0" *) 
  (* bram_slice_end = "17" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "2047" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "17" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(18),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(18),
    .WRITE_WIDTH_B(0)) 
    ram_reg_0
       (.ADDRARDADDR({1'b1,addr,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CASCADEINA(1'b1),
        .CASCADEINB(1'b0),
        .CASCADEOUTA(NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED),
        .CASCADEOUTB(NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED),
        .CLKARDCLK(m00_axis_aclk),
        .CLKBWRCLK(1'b0),
        .DBITERR(NLW_ram_reg_0_DBITERR_UNCONNECTED),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,s00_axis_tdata[15:0]}),
        .DIBDI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0,s00_axis_tdata[17:16]}),
        .DIPBDIP({1'b1,1'b1,1'b1,1'b1}),
        .DOADO({NLW_ram_reg_0_DOADO_UNCONNECTED[31:16],\_DOUT_2[7]_15 [15:0]}),
        .DOBDO(NLW_ram_reg_0_DOBDO_UNCONNECTED[31:0]),
        .DOPADOP({NLW_ram_reg_0_DOPADOP_UNCONNECTED[3:2],\_DOUT_2[7]_15 [17:16]}),
        .DOPBDOP(NLW_ram_reg_0_DOPBDOP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_ram_reg_0_ECCPARITY_UNCONNECTED[7:0]),
        .ENARDEN(Q),
        .ENBWREN(1'b0),
        .INJECTDBITERR(NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED),
        .INJECTSBITERR(NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED),
        .RDADDRECC(NLW_ram_reg_0_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_ram_reg_0_SBITERR_UNCONNECTED),
        .WEA({ram_reg_0_i_1__12_n_0,ram_reg_0_i_1__12_n_0,ram_reg_0_i_1__12_n_0,ram_reg_0_i_1__12_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
  LUT3 #(
    .INIT(8'h80)) 
    ram_reg_0_i_1__12
       (.I0(Q),
        .I1(s00_axis_tuser),
        .I2(s00_axis_tvalid),
        .O(ram_reg_0_i_1__12_n_0));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d6" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "46080" *) 
  (* RTL_RAM_NAME = "ram" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "2047" *) 
  (* bram_slice_begin = "18" *) 
  (* bram_slice_end = "23" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "2047" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "18" *) 
  (* ram_slice_end = "23" *) 
  RAMB18E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .INIT_A(18'h00000),
    .INIT_B(18'h00000),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(18'h00000),
    .SRVAL_B(18'h00000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(0)) 
    ram_reg_1
       (.ADDRARDADDR({addr,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CLKARDCLK(m00_axis_aclk),
        .CLKBWRCLK(1'b0),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,s00_axis_tdata[23:18]}),
        .DIBDI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0}),
        .DIPBDIP({1'b1,1'b1}),
        .DOADO({NLW_ram_reg_1_DOADO_UNCONNECTED[15:6],\_DOUT_2[7]_15 [23:18]}),
        .DOBDO(NLW_ram_reg_1_DOBDO_UNCONNECTED[15:0]),
        .DOPADOP(NLW_ram_reg_1_DOPADOP_UNCONNECTED[1:0]),
        .DOPBDOP(NLW_ram_reg_1_DOPBDOP_UNCONNECTED[1:0]),
        .ENARDEN(Q),
        .ENBWREN(1'b0),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .WEA({ram_reg_0_i_1__12_n_0,ram_reg_0_i_1__12_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0}));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[0]_i_6__0 
       (.I0(\_DOUT_2[7]_15 [0]),
        .I1(dout0_out[0]),
        .I2(ctrl_mux_out_2[1]),
        .I3(\y_reg[23]_i_3__0 [0]),
        .I4(ctrl_mux_out_2[0]),
        .I5(\y_reg[23]_i_3__0_0 [0]),
        .O(ram_reg_0_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[10]_i_6__0 
       (.I0(\_DOUT_2[7]_15 [10]),
        .I1(dout0_out[10]),
        .I2(ctrl_mux_out_2[1]),
        .I3(\y_reg[23]_i_3__0 [10]),
        .I4(ctrl_mux_out_2[0]),
        .I5(\y_reg[23]_i_3__0_0 [10]),
        .O(ram_reg_0_10));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[11]_i_6__0 
       (.I0(\_DOUT_2[7]_15 [11]),
        .I1(dout0_out[11]),
        .I2(ctrl_mux_out_2[1]),
        .I3(\y_reg[23]_i_3__0 [11]),
        .I4(ctrl_mux_out_2[0]),
        .I5(\y_reg[23]_i_3__0_0 [11]),
        .O(ram_reg_0_11));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[12]_i_6__0 
       (.I0(\_DOUT_2[7]_15 [12]),
        .I1(dout0_out[12]),
        .I2(ctrl_mux_out_2[1]),
        .I3(\y_reg[23]_i_3__0 [12]),
        .I4(ctrl_mux_out_2[0]),
        .I5(\y_reg[23]_i_3__0_0 [12]),
        .O(ram_reg_0_12));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[13]_i_6__0 
       (.I0(\_DOUT_2[7]_15 [13]),
        .I1(dout0_out[13]),
        .I2(ctrl_mux_out_2[1]),
        .I3(\y_reg[23]_i_3__0 [13]),
        .I4(ctrl_mux_out_2[0]),
        .I5(\y_reg[23]_i_3__0_0 [13]),
        .O(ram_reg_0_13));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[14]_i_6__0 
       (.I0(\_DOUT_2[7]_15 [14]),
        .I1(dout0_out[14]),
        .I2(ctrl_mux_out_2[1]),
        .I3(\y_reg[23]_i_3__0 [14]),
        .I4(ctrl_mux_out_2[0]),
        .I5(\y_reg[23]_i_3__0_0 [14]),
        .O(ram_reg_0_14));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[15]_i_6__0 
       (.I0(\_DOUT_2[7]_15 [15]),
        .I1(dout0_out[15]),
        .I2(ctrl_mux_out_2[1]),
        .I3(\y_reg[23]_i_3__0 [15]),
        .I4(ctrl_mux_out_2[0]),
        .I5(\y_reg[23]_i_3__0_0 [15]),
        .O(ram_reg_0_15));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[16]_i_6__0 
       (.I0(\_DOUT_2[7]_15 [16]),
        .I1(dout0_out[16]),
        .I2(ctrl_mux_out_2[1]),
        .I3(\y_reg[23]_i_3__0 [16]),
        .I4(ctrl_mux_out_2[0]),
        .I5(\y_reg[23]_i_3__0_0 [16]),
        .O(ram_reg_0_16));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[17]_i_6__0 
       (.I0(\_DOUT_2[7]_15 [17]),
        .I1(dout0_out[17]),
        .I2(ctrl_mux_out_2[1]),
        .I3(\y_reg[23]_i_3__0 [17]),
        .I4(ctrl_mux_out_2[0]),
        .I5(\y_reg[23]_i_3__0_0 [17]),
        .O(ram_reg_0_17));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[18]_i_6__0 
       (.I0(\_DOUT_2[7]_15 [18]),
        .I1(dout0_out[18]),
        .I2(ctrl_mux_out_2[1]),
        .I3(\y_reg[23]_i_3__0 [18]),
        .I4(ctrl_mux_out_2[0]),
        .I5(\y_reg[23]_i_3__0_0 [18]),
        .O(ram_reg_1_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[19]_i_6__0 
       (.I0(\_DOUT_2[7]_15 [19]),
        .I1(dout0_out[19]),
        .I2(ctrl_mux_out_2[1]),
        .I3(\y_reg[23]_i_3__0 [19]),
        .I4(ctrl_mux_out_2[0]),
        .I5(\y_reg[23]_i_3__0_0 [19]),
        .O(ram_reg_1_1));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[1]_i_6__0 
       (.I0(\_DOUT_2[7]_15 [1]),
        .I1(dout0_out[1]),
        .I2(ctrl_mux_out_2[1]),
        .I3(\y_reg[23]_i_3__0 [1]),
        .I4(ctrl_mux_out_2[0]),
        .I5(\y_reg[23]_i_3__0_0 [1]),
        .O(ram_reg_0_1));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[20]_i_6__0 
       (.I0(\_DOUT_2[7]_15 [20]),
        .I1(dout0_out[20]),
        .I2(ctrl_mux_out_2[1]),
        .I3(\y_reg[23]_i_3__0 [20]),
        .I4(ctrl_mux_out_2[0]),
        .I5(\y_reg[23]_i_3__0_0 [20]),
        .O(ram_reg_1_2));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[21]_i_6__0 
       (.I0(\_DOUT_2[7]_15 [21]),
        .I1(dout0_out[21]),
        .I2(ctrl_mux_out_2[1]),
        .I3(\y_reg[23]_i_3__0 [21]),
        .I4(ctrl_mux_out_2[0]),
        .I5(\y_reg[23]_i_3__0_0 [21]),
        .O(ram_reg_1_3));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[22]_i_6__0 
       (.I0(\_DOUT_2[7]_15 [22]),
        .I1(dout0_out[22]),
        .I2(ctrl_mux_out_2[1]),
        .I3(\y_reg[23]_i_3__0 [22]),
        .I4(ctrl_mux_out_2[0]),
        .I5(\y_reg[23]_i_3__0_0 [22]),
        .O(ram_reg_1_4));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[23]_i_7__0 
       (.I0(\_DOUT_2[7]_15 [23]),
        .I1(dout0_out[23]),
        .I2(ctrl_mux_out_2[1]),
        .I3(\y_reg[23]_i_3__0 [23]),
        .I4(ctrl_mux_out_2[0]),
        .I5(\y_reg[23]_i_3__0_0 [23]),
        .O(ram_reg_1_5));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[2]_i_6__0 
       (.I0(\_DOUT_2[7]_15 [2]),
        .I1(dout0_out[2]),
        .I2(ctrl_mux_out_2[1]),
        .I3(\y_reg[23]_i_3__0 [2]),
        .I4(ctrl_mux_out_2[0]),
        .I5(\y_reg[23]_i_3__0_0 [2]),
        .O(ram_reg_0_2));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[3]_i_6__0 
       (.I0(\_DOUT_2[7]_15 [3]),
        .I1(dout0_out[3]),
        .I2(ctrl_mux_out_2[1]),
        .I3(\y_reg[23]_i_3__0 [3]),
        .I4(ctrl_mux_out_2[0]),
        .I5(\y_reg[23]_i_3__0_0 [3]),
        .O(ram_reg_0_3));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[4]_i_6__0 
       (.I0(\_DOUT_2[7]_15 [4]),
        .I1(dout0_out[4]),
        .I2(ctrl_mux_out_2[1]),
        .I3(\y_reg[23]_i_3__0 [4]),
        .I4(ctrl_mux_out_2[0]),
        .I5(\y_reg[23]_i_3__0_0 [4]),
        .O(ram_reg_0_4));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[5]_i_6__0 
       (.I0(\_DOUT_2[7]_15 [5]),
        .I1(dout0_out[5]),
        .I2(ctrl_mux_out_2[1]),
        .I3(\y_reg[23]_i_3__0 [5]),
        .I4(ctrl_mux_out_2[0]),
        .I5(\y_reg[23]_i_3__0_0 [5]),
        .O(ram_reg_0_5));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[6]_i_6__0 
       (.I0(\_DOUT_2[7]_15 [6]),
        .I1(dout0_out[6]),
        .I2(ctrl_mux_out_2[1]),
        .I3(\y_reg[23]_i_3__0 [6]),
        .I4(ctrl_mux_out_2[0]),
        .I5(\y_reg[23]_i_3__0_0 [6]),
        .O(ram_reg_0_6));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[7]_i_6__0 
       (.I0(\_DOUT_2[7]_15 [7]),
        .I1(dout0_out[7]),
        .I2(ctrl_mux_out_2[1]),
        .I3(\y_reg[23]_i_3__0 [7]),
        .I4(ctrl_mux_out_2[0]),
        .I5(\y_reg[23]_i_3__0_0 [7]),
        .O(ram_reg_0_7));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[8]_i_6__0 
       (.I0(\_DOUT_2[7]_15 [8]),
        .I1(dout0_out[8]),
        .I2(ctrl_mux_out_2[1]),
        .I3(\y_reg[23]_i_3__0 [8]),
        .I4(ctrl_mux_out_2[0]),
        .I5(\y_reg[23]_i_3__0_0 [8]),
        .O(ram_reg_0_8));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[9]_i_6__0 
       (.I0(\_DOUT_2[7]_15 [9]),
        .I1(dout0_out[9]),
        .I2(ctrl_mux_out_2[1]),
        .I3(\y_reg[23]_i_3__0 [9]),
        .I4(ctrl_mux_out_2[0]),
        .I5(\y_reg[23]_i_3__0_0 [9]),
        .O(ram_reg_0_9));
endmodule

(* ORIG_REF_NAME = "rams_sp_rf_rst" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_27
   (dout0_out,
    m00_axis_aclk,
    Q,
    addr,
    s00_axis_tdata,
    s00_axis_tuser,
    s00_axis_tvalid);
  output [23:0]dout0_out;
  input m00_axis_aclk;
  input [0:0]Q;
  input [10:0]addr;
  input [23:0]s00_axis_tdata;
  input [0:0]s00_axis_tuser;
  input s00_axis_tvalid;

  wire [0:0]Q;
  wire [10:0]addr;
  wire [23:0]dout0_out;
  wire m00_axis_aclk;
  wire ram_reg_0_i_1__13_n_0;
  wire [23:0]s00_axis_tdata;
  wire [0:0]s00_axis_tuser;
  wire s00_axis_tvalid;
  wire NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED;
  wire NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED;
  wire NLW_ram_reg_0_DBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_SBITERR_UNCONNECTED;
  wire [31:16]NLW_ram_reg_0_DOADO_UNCONNECTED;
  wire [31:0]NLW_ram_reg_0_DOBDO_UNCONNECTED;
  wire [3:2]NLW_ram_reg_0_DOPADOP_UNCONNECTED;
  wire [3:0]NLW_ram_reg_0_DOPBDOP_UNCONNECTED;
  wire [7:0]NLW_ram_reg_0_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_ram_reg_0_RDADDRECC_UNCONNECTED;
  wire [15:6]NLW_ram_reg_1_DOADO_UNCONNECTED;
  wire [15:0]NLW_ram_reg_1_DOBDO_UNCONNECTED;
  wire [1:0]NLW_ram_reg_1_DOPADOP_UNCONNECTED;
  wire [1:0]NLW_ram_reg_1_DOPBDOP_UNCONNECTED;

  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p2_d16" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "46080" *) 
  (* RTL_RAM_NAME = "ram" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "2047" *) 
  (* bram_slice_begin = "0" *) 
  (* bram_slice_end = "17" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "2047" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "17" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(18),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(18),
    .WRITE_WIDTH_B(0)) 
    ram_reg_0
       (.ADDRARDADDR({1'b1,addr,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CASCADEINA(1'b1),
        .CASCADEINB(1'b0),
        .CASCADEOUTA(NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED),
        .CASCADEOUTB(NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED),
        .CLKARDCLK(m00_axis_aclk),
        .CLKBWRCLK(1'b0),
        .DBITERR(NLW_ram_reg_0_DBITERR_UNCONNECTED),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,s00_axis_tdata[15:0]}),
        .DIBDI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0,s00_axis_tdata[17:16]}),
        .DIPBDIP({1'b1,1'b1,1'b1,1'b1}),
        .DOADO({NLW_ram_reg_0_DOADO_UNCONNECTED[31:16],dout0_out[15:0]}),
        .DOBDO(NLW_ram_reg_0_DOBDO_UNCONNECTED[31:0]),
        .DOPADOP({NLW_ram_reg_0_DOPADOP_UNCONNECTED[3:2],dout0_out[17:16]}),
        .DOPBDOP(NLW_ram_reg_0_DOPBDOP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_ram_reg_0_ECCPARITY_UNCONNECTED[7:0]),
        .ENARDEN(Q),
        .ENBWREN(1'b0),
        .INJECTDBITERR(NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED),
        .INJECTSBITERR(NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED),
        .RDADDRECC(NLW_ram_reg_0_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_ram_reg_0_SBITERR_UNCONNECTED),
        .WEA({ram_reg_0_i_1__13_n_0,ram_reg_0_i_1__13_n_0,ram_reg_0_i_1__13_n_0,ram_reg_0_i_1__13_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
  LUT3 #(
    .INIT(8'h20)) 
    ram_reg_0_i_1__13
       (.I0(Q),
        .I1(s00_axis_tuser),
        .I2(s00_axis_tvalid),
        .O(ram_reg_0_i_1__13_n_0));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d6" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "46080" *) 
  (* RTL_RAM_NAME = "ram" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "2047" *) 
  (* bram_slice_begin = "18" *) 
  (* bram_slice_end = "23" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "2047" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "18" *) 
  (* ram_slice_end = "23" *) 
  RAMB18E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .INIT_A(18'h00000),
    .INIT_B(18'h00000),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(18'h00000),
    .SRVAL_B(18'h00000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(0)) 
    ram_reg_1
       (.ADDRARDADDR({addr,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CLKARDCLK(m00_axis_aclk),
        .CLKBWRCLK(1'b0),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,s00_axis_tdata[23:18]}),
        .DIBDI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0}),
        .DIPBDIP({1'b1,1'b1}),
        .DOADO({NLW_ram_reg_1_DOADO_UNCONNECTED[15:6],dout0_out[23:18]}),
        .DOBDO(NLW_ram_reg_1_DOBDO_UNCONNECTED[15:0]),
        .DOPADOP(NLW_ram_reg_1_DOPADOP_UNCONNECTED[1:0]),
        .DOPBDOP(NLW_ram_reg_1_DOPBDOP_UNCONNECTED[1:0]),
        .ENARDEN(Q),
        .ENBWREN(1'b0),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .WEA({ram_reg_0_i_1__13_n_0,ram_reg_0_i_1__13_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0}));
endmodule

(* ORIG_REF_NAME = "rams_sp_rf_rst" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_28
   (dout0_out,
    m00_axis_aclk,
    Q,
    addr,
    s00_axis_tdata,
    s00_axis_tuser,
    s00_axis_tvalid);
  output [23:0]dout0_out;
  input m00_axis_aclk;
  input [0:0]Q;
  input [10:0]addr;
  input [23:0]s00_axis_tdata;
  input [0:0]s00_axis_tuser;
  input s00_axis_tvalid;

  wire [0:0]Q;
  wire [10:0]addr;
  wire [23:0]dout0_out;
  wire m00_axis_aclk;
  wire ram_reg_0_i_1__14_n_0;
  wire [23:0]s00_axis_tdata;
  wire [0:0]s00_axis_tuser;
  wire s00_axis_tvalid;
  wire NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED;
  wire NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED;
  wire NLW_ram_reg_0_DBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_SBITERR_UNCONNECTED;
  wire [31:16]NLW_ram_reg_0_DOADO_UNCONNECTED;
  wire [31:0]NLW_ram_reg_0_DOBDO_UNCONNECTED;
  wire [3:2]NLW_ram_reg_0_DOPADOP_UNCONNECTED;
  wire [3:0]NLW_ram_reg_0_DOPBDOP_UNCONNECTED;
  wire [7:0]NLW_ram_reg_0_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_ram_reg_0_RDADDRECC_UNCONNECTED;
  wire [15:6]NLW_ram_reg_1_DOADO_UNCONNECTED;
  wire [15:0]NLW_ram_reg_1_DOBDO_UNCONNECTED;
  wire [1:0]NLW_ram_reg_1_DOPADOP_UNCONNECTED;
  wire [1:0]NLW_ram_reg_1_DOPBDOP_UNCONNECTED;

  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p2_d16" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "46080" *) 
  (* RTL_RAM_NAME = "ram" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "2047" *) 
  (* bram_slice_begin = "0" *) 
  (* bram_slice_end = "17" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "2047" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "17" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(18),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(18),
    .WRITE_WIDTH_B(0)) 
    ram_reg_0
       (.ADDRARDADDR({1'b1,addr,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CASCADEINA(1'b1),
        .CASCADEINB(1'b0),
        .CASCADEOUTA(NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED),
        .CASCADEOUTB(NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED),
        .CLKARDCLK(m00_axis_aclk),
        .CLKBWRCLK(1'b0),
        .DBITERR(NLW_ram_reg_0_DBITERR_UNCONNECTED),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,s00_axis_tdata[15:0]}),
        .DIBDI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0,s00_axis_tdata[17:16]}),
        .DIPBDIP({1'b1,1'b1,1'b1,1'b1}),
        .DOADO({NLW_ram_reg_0_DOADO_UNCONNECTED[31:16],dout0_out[15:0]}),
        .DOBDO(NLW_ram_reg_0_DOBDO_UNCONNECTED[31:0]),
        .DOPADOP({NLW_ram_reg_0_DOPADOP_UNCONNECTED[3:2],dout0_out[17:16]}),
        .DOPBDOP(NLW_ram_reg_0_DOPBDOP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_ram_reg_0_ECCPARITY_UNCONNECTED[7:0]),
        .ENARDEN(Q),
        .ENBWREN(1'b0),
        .INJECTDBITERR(NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED),
        .INJECTSBITERR(NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED),
        .RDADDRECC(NLW_ram_reg_0_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_ram_reg_0_SBITERR_UNCONNECTED),
        .WEA({ram_reg_0_i_1__14_n_0,ram_reg_0_i_1__14_n_0,ram_reg_0_i_1__14_n_0,ram_reg_0_i_1__14_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
  LUT3 #(
    .INIT(8'h80)) 
    ram_reg_0_i_1__14
       (.I0(Q),
        .I1(s00_axis_tuser),
        .I2(s00_axis_tvalid),
        .O(ram_reg_0_i_1__14_n_0));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d6" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "46080" *) 
  (* RTL_RAM_NAME = "ram" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "2047" *) 
  (* bram_slice_begin = "18" *) 
  (* bram_slice_end = "23" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "2047" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "18" *) 
  (* ram_slice_end = "23" *) 
  RAMB18E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .INIT_A(18'h00000),
    .INIT_B(18'h00000),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(18'h00000),
    .SRVAL_B(18'h00000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(0)) 
    ram_reg_1
       (.ADDRARDADDR({addr,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CLKARDCLK(m00_axis_aclk),
        .CLKBWRCLK(1'b0),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,s00_axis_tdata[23:18]}),
        .DIBDI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0}),
        .DIPBDIP({1'b1,1'b1}),
        .DOADO({NLW_ram_reg_1_DOADO_UNCONNECTED[15:6],dout0_out[23:18]}),
        .DOBDO(NLW_ram_reg_1_DOBDO_UNCONNECTED[15:0]),
        .DOPADOP(NLW_ram_reg_1_DOPADOP_UNCONNECTED[1:0]),
        .DOPBDOP(NLW_ram_reg_1_DOPBDOP_UNCONNECTED[1:0]),
        .ENARDEN(Q),
        .ENBWREN(1'b0),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .WEA({ram_reg_0_i_1__14_n_0,ram_reg_0_i_1__14_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0}));
endmodule

(* ORIG_REF_NAME = "rams_sp_rf_rst" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_29
   (dout0_out,
    m00_axis_aclk,
    Q,
    addr,
    s00_axis_tdata,
    s00_axis_tuser,
    s00_axis_tvalid);
  output [23:0]dout0_out;
  input m00_axis_aclk;
  input [0:0]Q;
  input [10:0]addr;
  input [23:0]s00_axis_tdata;
  input [0:0]s00_axis_tuser;
  input s00_axis_tvalid;

  wire [0:0]Q;
  wire [10:0]addr;
  wire [23:0]dout0_out;
  wire m00_axis_aclk;
  wire ram_reg_0_i_1__15_n_0;
  wire [23:0]s00_axis_tdata;
  wire [0:0]s00_axis_tuser;
  wire s00_axis_tvalid;
  wire NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED;
  wire NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED;
  wire NLW_ram_reg_0_DBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_SBITERR_UNCONNECTED;
  wire [31:16]NLW_ram_reg_0_DOADO_UNCONNECTED;
  wire [31:0]NLW_ram_reg_0_DOBDO_UNCONNECTED;
  wire [3:2]NLW_ram_reg_0_DOPADOP_UNCONNECTED;
  wire [3:0]NLW_ram_reg_0_DOPBDOP_UNCONNECTED;
  wire [7:0]NLW_ram_reg_0_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_ram_reg_0_RDADDRECC_UNCONNECTED;
  wire [15:6]NLW_ram_reg_1_DOADO_UNCONNECTED;
  wire [15:0]NLW_ram_reg_1_DOBDO_UNCONNECTED;
  wire [1:0]NLW_ram_reg_1_DOPADOP_UNCONNECTED;
  wire [1:0]NLW_ram_reg_1_DOPBDOP_UNCONNECTED;

  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p2_d16" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "46080" *) 
  (* RTL_RAM_NAME = "ram" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "2047" *) 
  (* bram_slice_begin = "0" *) 
  (* bram_slice_end = "17" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "2047" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "17" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(18),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(18),
    .WRITE_WIDTH_B(0)) 
    ram_reg_0
       (.ADDRARDADDR({1'b1,addr,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CASCADEINA(1'b1),
        .CASCADEINB(1'b0),
        .CASCADEOUTA(NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED),
        .CASCADEOUTB(NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED),
        .CLKARDCLK(m00_axis_aclk),
        .CLKBWRCLK(1'b0),
        .DBITERR(NLW_ram_reg_0_DBITERR_UNCONNECTED),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,s00_axis_tdata[15:0]}),
        .DIBDI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0,s00_axis_tdata[17:16]}),
        .DIPBDIP({1'b1,1'b1,1'b1,1'b1}),
        .DOADO({NLW_ram_reg_0_DOADO_UNCONNECTED[31:16],dout0_out[15:0]}),
        .DOBDO(NLW_ram_reg_0_DOBDO_UNCONNECTED[31:0]),
        .DOPADOP({NLW_ram_reg_0_DOPADOP_UNCONNECTED[3:2],dout0_out[17:16]}),
        .DOPBDOP(NLW_ram_reg_0_DOPBDOP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_ram_reg_0_ECCPARITY_UNCONNECTED[7:0]),
        .ENARDEN(Q),
        .ENBWREN(1'b0),
        .INJECTDBITERR(NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED),
        .INJECTSBITERR(NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED),
        .RDADDRECC(NLW_ram_reg_0_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_ram_reg_0_SBITERR_UNCONNECTED),
        .WEA({ram_reg_0_i_1__15_n_0,ram_reg_0_i_1__15_n_0,ram_reg_0_i_1__15_n_0,ram_reg_0_i_1__15_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
  LUT3 #(
    .INIT(8'h20)) 
    ram_reg_0_i_1__15
       (.I0(Q),
        .I1(s00_axis_tuser),
        .I2(s00_axis_tvalid),
        .O(ram_reg_0_i_1__15_n_0));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d6" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "46080" *) 
  (* RTL_RAM_NAME = "ram" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "2047" *) 
  (* bram_slice_begin = "18" *) 
  (* bram_slice_end = "23" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "2047" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "18" *) 
  (* ram_slice_end = "23" *) 
  RAMB18E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .INIT_A(18'h00000),
    .INIT_B(18'h00000),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(18'h00000),
    .SRVAL_B(18'h00000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(0)) 
    ram_reg_1
       (.ADDRARDADDR({addr,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CLKARDCLK(m00_axis_aclk),
        .CLKBWRCLK(1'b0),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,s00_axis_tdata[23:18]}),
        .DIBDI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0}),
        .DIPBDIP({1'b1,1'b1}),
        .DOADO({NLW_ram_reg_1_DOADO_UNCONNECTED[15:6],dout0_out[23:18]}),
        .DOBDO(NLW_ram_reg_1_DOBDO_UNCONNECTED[15:0]),
        .DOPADOP(NLW_ram_reg_1_DOPADOP_UNCONNECTED[1:0]),
        .DOPBDOP(NLW_ram_reg_1_DOPBDOP_UNCONNECTED[1:0]),
        .ENARDEN(Q),
        .ENBWREN(1'b0),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .WEA({ram_reg_0_i_1__15_n_0,ram_reg_0_i_1__15_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0}));
endmodule

(* ORIG_REF_NAME = "rams_sp_rf_rst" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_3
   (ram_reg_0_0,
    ram_reg_0_1,
    ram_reg_0_2,
    ram_reg_0_3,
    ram_reg_0_4,
    ram_reg_0_5,
    ram_reg_0_6,
    ram_reg_0_7,
    ram_reg_0_8,
    ram_reg_0_9,
    ram_reg_0_10,
    ram_reg_0_11,
    ram_reg_0_12,
    ram_reg_0_13,
    ram_reg_0_14,
    ram_reg_0_15,
    ram_reg_0_16,
    ram_reg_0_17,
    ram_reg_1_0,
    ram_reg_1_1,
    ram_reg_1_2,
    ram_reg_1_3,
    ram_reg_1_4,
    ram_reg_1_5,
    dout0_out,
    ctrl_mux_out_1,
    \y_reg[23]_i_1 ,
    \y_reg[23]_i_1_0 ,
    m00_axis_aclk,
    Q,
    addr,
    s00_axis_tdata,
    s00_axis_tuser,
    s00_axis_tvalid);
  output ram_reg_0_0;
  output ram_reg_0_1;
  output ram_reg_0_2;
  output ram_reg_0_3;
  output ram_reg_0_4;
  output ram_reg_0_5;
  output ram_reg_0_6;
  output ram_reg_0_7;
  output ram_reg_0_8;
  output ram_reg_0_9;
  output ram_reg_0_10;
  output ram_reg_0_11;
  output ram_reg_0_12;
  output ram_reg_0_13;
  output ram_reg_0_14;
  output ram_reg_0_15;
  output ram_reg_0_16;
  output ram_reg_0_17;
  output ram_reg_1_0;
  output ram_reg_1_1;
  output ram_reg_1_2;
  output ram_reg_1_3;
  output ram_reg_1_4;
  output ram_reg_1_5;
  input [23:0]dout0_out;
  input [1:0]ctrl_mux_out_1;
  input [23:0]\y_reg[23]_i_1 ;
  input [23:0]\y_reg[23]_i_1_0 ;
  input m00_axis_aclk;
  input [0:0]Q;
  input [10:0]addr;
  input [23:0]s00_axis_tdata;
  input [0:0]s00_axis_tuser;
  input s00_axis_tvalid;

  wire [0:0]Q;
  wire [23:0]\_DOUT_1[11]_22 ;
  wire [10:0]addr;
  wire [1:0]ctrl_mux_out_1;
  wire [23:0]dout0_out;
  wire m00_axis_aclk;
  wire ram_reg_0_0;
  wire ram_reg_0_1;
  wire ram_reg_0_10;
  wire ram_reg_0_11;
  wire ram_reg_0_12;
  wire ram_reg_0_13;
  wire ram_reg_0_14;
  wire ram_reg_0_15;
  wire ram_reg_0_16;
  wire ram_reg_0_17;
  wire ram_reg_0_2;
  wire ram_reg_0_3;
  wire ram_reg_0_4;
  wire ram_reg_0_5;
  wire ram_reg_0_6;
  wire ram_reg_0_7;
  wire ram_reg_0_8;
  wire ram_reg_0_9;
  wire ram_reg_0_i_1__19_n_0;
  wire ram_reg_1_0;
  wire ram_reg_1_1;
  wire ram_reg_1_2;
  wire ram_reg_1_3;
  wire ram_reg_1_4;
  wire ram_reg_1_5;
  wire [23:0]s00_axis_tdata;
  wire [0:0]s00_axis_tuser;
  wire s00_axis_tvalid;
  wire [23:0]\y_reg[23]_i_1 ;
  wire [23:0]\y_reg[23]_i_1_0 ;
  wire NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED;
  wire NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED;
  wire NLW_ram_reg_0_DBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_SBITERR_UNCONNECTED;
  wire [31:16]NLW_ram_reg_0_DOADO_UNCONNECTED;
  wire [31:0]NLW_ram_reg_0_DOBDO_UNCONNECTED;
  wire [3:2]NLW_ram_reg_0_DOPADOP_UNCONNECTED;
  wire [3:0]NLW_ram_reg_0_DOPBDOP_UNCONNECTED;
  wire [7:0]NLW_ram_reg_0_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_ram_reg_0_RDADDRECC_UNCONNECTED;
  wire [15:6]NLW_ram_reg_1_DOADO_UNCONNECTED;
  wire [15:0]NLW_ram_reg_1_DOBDO_UNCONNECTED;
  wire [1:0]NLW_ram_reg_1_DOPADOP_UNCONNECTED;
  wire [1:0]NLW_ram_reg_1_DOPBDOP_UNCONNECTED;

  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p2_d16" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "46080" *) 
  (* RTL_RAM_NAME = "ram" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "2047" *) 
  (* bram_slice_begin = "0" *) 
  (* bram_slice_end = "17" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "2047" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "17" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(18),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(18),
    .WRITE_WIDTH_B(0)) 
    ram_reg_0
       (.ADDRARDADDR({1'b1,addr,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CASCADEINA(1'b1),
        .CASCADEINB(1'b0),
        .CASCADEOUTA(NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED),
        .CASCADEOUTB(NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED),
        .CLKARDCLK(m00_axis_aclk),
        .CLKBWRCLK(1'b0),
        .DBITERR(NLW_ram_reg_0_DBITERR_UNCONNECTED),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,s00_axis_tdata[15:0]}),
        .DIBDI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0,s00_axis_tdata[17:16]}),
        .DIPBDIP({1'b1,1'b1,1'b1,1'b1}),
        .DOADO({NLW_ram_reg_0_DOADO_UNCONNECTED[31:16],\_DOUT_1[11]_22 [15:0]}),
        .DOBDO(NLW_ram_reg_0_DOBDO_UNCONNECTED[31:0]),
        .DOPADOP({NLW_ram_reg_0_DOPADOP_UNCONNECTED[3:2],\_DOUT_1[11]_22 [17:16]}),
        .DOPBDOP(NLW_ram_reg_0_DOPBDOP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_ram_reg_0_ECCPARITY_UNCONNECTED[7:0]),
        .ENARDEN(Q),
        .ENBWREN(1'b0),
        .INJECTDBITERR(NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED),
        .INJECTSBITERR(NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED),
        .RDADDRECC(NLW_ram_reg_0_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_ram_reg_0_SBITERR_UNCONNECTED),
        .WEA({ram_reg_0_i_1__19_n_0,ram_reg_0_i_1__19_n_0,ram_reg_0_i_1__19_n_0,ram_reg_0_i_1__19_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
  LUT3 #(
    .INIT(8'h20)) 
    ram_reg_0_i_1__19
       (.I0(Q),
        .I1(s00_axis_tuser),
        .I2(s00_axis_tvalid),
        .O(ram_reg_0_i_1__19_n_0));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d6" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "46080" *) 
  (* RTL_RAM_NAME = "ram" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "2047" *) 
  (* bram_slice_begin = "18" *) 
  (* bram_slice_end = "23" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "2047" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "18" *) 
  (* ram_slice_end = "23" *) 
  RAMB18E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .INIT_A(18'h00000),
    .INIT_B(18'h00000),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(18'h00000),
    .SRVAL_B(18'h00000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(0)) 
    ram_reg_1
       (.ADDRARDADDR({addr,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CLKARDCLK(m00_axis_aclk),
        .CLKBWRCLK(1'b0),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,s00_axis_tdata[23:18]}),
        .DIBDI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0}),
        .DIPBDIP({1'b1,1'b1}),
        .DOADO({NLW_ram_reg_1_DOADO_UNCONNECTED[15:6],\_DOUT_1[11]_22 [23:18]}),
        .DOBDO(NLW_ram_reg_1_DOBDO_UNCONNECTED[15:0]),
        .DOPADOP(NLW_ram_reg_1_DOPADOP_UNCONNECTED[1:0]),
        .DOPBDOP(NLW_ram_reg_1_DOPBDOP_UNCONNECTED[1:0]),
        .ENARDEN(Q),
        .ENBWREN(1'b0),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .WEA({ram_reg_0_i_1__19_n_0,ram_reg_0_i_1__19_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0}));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[0]_i_3 
       (.I0(\_DOUT_1[11]_22 [0]),
        .I1(dout0_out[0]),
        .I2(ctrl_mux_out_1[1]),
        .I3(\y_reg[23]_i_1 [0]),
        .I4(ctrl_mux_out_1[0]),
        .I5(\y_reg[23]_i_1_0 [0]),
        .O(ram_reg_0_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[10]_i_3 
       (.I0(\_DOUT_1[11]_22 [10]),
        .I1(dout0_out[10]),
        .I2(ctrl_mux_out_1[1]),
        .I3(\y_reg[23]_i_1 [10]),
        .I4(ctrl_mux_out_1[0]),
        .I5(\y_reg[23]_i_1_0 [10]),
        .O(ram_reg_0_10));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[11]_i_3 
       (.I0(\_DOUT_1[11]_22 [11]),
        .I1(dout0_out[11]),
        .I2(ctrl_mux_out_1[1]),
        .I3(\y_reg[23]_i_1 [11]),
        .I4(ctrl_mux_out_1[0]),
        .I5(\y_reg[23]_i_1_0 [11]),
        .O(ram_reg_0_11));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[12]_i_3 
       (.I0(\_DOUT_1[11]_22 [12]),
        .I1(dout0_out[12]),
        .I2(ctrl_mux_out_1[1]),
        .I3(\y_reg[23]_i_1 [12]),
        .I4(ctrl_mux_out_1[0]),
        .I5(\y_reg[23]_i_1_0 [12]),
        .O(ram_reg_0_12));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[13]_i_3 
       (.I0(\_DOUT_1[11]_22 [13]),
        .I1(dout0_out[13]),
        .I2(ctrl_mux_out_1[1]),
        .I3(\y_reg[23]_i_1 [13]),
        .I4(ctrl_mux_out_1[0]),
        .I5(\y_reg[23]_i_1_0 [13]),
        .O(ram_reg_0_13));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[14]_i_3 
       (.I0(\_DOUT_1[11]_22 [14]),
        .I1(dout0_out[14]),
        .I2(ctrl_mux_out_1[1]),
        .I3(\y_reg[23]_i_1 [14]),
        .I4(ctrl_mux_out_1[0]),
        .I5(\y_reg[23]_i_1_0 [14]),
        .O(ram_reg_0_14));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[15]_i_3 
       (.I0(\_DOUT_1[11]_22 [15]),
        .I1(dout0_out[15]),
        .I2(ctrl_mux_out_1[1]),
        .I3(\y_reg[23]_i_1 [15]),
        .I4(ctrl_mux_out_1[0]),
        .I5(\y_reg[23]_i_1_0 [15]),
        .O(ram_reg_0_15));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[16]_i_3 
       (.I0(\_DOUT_1[11]_22 [16]),
        .I1(dout0_out[16]),
        .I2(ctrl_mux_out_1[1]),
        .I3(\y_reg[23]_i_1 [16]),
        .I4(ctrl_mux_out_1[0]),
        .I5(\y_reg[23]_i_1_0 [16]),
        .O(ram_reg_0_16));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[17]_i_3 
       (.I0(\_DOUT_1[11]_22 [17]),
        .I1(dout0_out[17]),
        .I2(ctrl_mux_out_1[1]),
        .I3(\y_reg[23]_i_1 [17]),
        .I4(ctrl_mux_out_1[0]),
        .I5(\y_reg[23]_i_1_0 [17]),
        .O(ram_reg_0_17));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[18]_i_3 
       (.I0(\_DOUT_1[11]_22 [18]),
        .I1(dout0_out[18]),
        .I2(ctrl_mux_out_1[1]),
        .I3(\y_reg[23]_i_1 [18]),
        .I4(ctrl_mux_out_1[0]),
        .I5(\y_reg[23]_i_1_0 [18]),
        .O(ram_reg_1_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[19]_i_3 
       (.I0(\_DOUT_1[11]_22 [19]),
        .I1(dout0_out[19]),
        .I2(ctrl_mux_out_1[1]),
        .I3(\y_reg[23]_i_1 [19]),
        .I4(ctrl_mux_out_1[0]),
        .I5(\y_reg[23]_i_1_0 [19]),
        .O(ram_reg_1_1));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[1]_i_3 
       (.I0(\_DOUT_1[11]_22 [1]),
        .I1(dout0_out[1]),
        .I2(ctrl_mux_out_1[1]),
        .I3(\y_reg[23]_i_1 [1]),
        .I4(ctrl_mux_out_1[0]),
        .I5(\y_reg[23]_i_1_0 [1]),
        .O(ram_reg_0_1));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[20]_i_3 
       (.I0(\_DOUT_1[11]_22 [20]),
        .I1(dout0_out[20]),
        .I2(ctrl_mux_out_1[1]),
        .I3(\y_reg[23]_i_1 [20]),
        .I4(ctrl_mux_out_1[0]),
        .I5(\y_reg[23]_i_1_0 [20]),
        .O(ram_reg_1_2));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[21]_i_3 
       (.I0(\_DOUT_1[11]_22 [21]),
        .I1(dout0_out[21]),
        .I2(ctrl_mux_out_1[1]),
        .I3(\y_reg[23]_i_1 [21]),
        .I4(ctrl_mux_out_1[0]),
        .I5(\y_reg[23]_i_1_0 [21]),
        .O(ram_reg_1_3));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[22]_i_3 
       (.I0(\_DOUT_1[11]_22 [22]),
        .I1(dout0_out[22]),
        .I2(ctrl_mux_out_1[1]),
        .I3(\y_reg[23]_i_1 [22]),
        .I4(ctrl_mux_out_1[0]),
        .I5(\y_reg[23]_i_1_0 [22]),
        .O(ram_reg_1_4));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[23]_i_4 
       (.I0(\_DOUT_1[11]_22 [23]),
        .I1(dout0_out[23]),
        .I2(ctrl_mux_out_1[1]),
        .I3(\y_reg[23]_i_1 [23]),
        .I4(ctrl_mux_out_1[0]),
        .I5(\y_reg[23]_i_1_0 [23]),
        .O(ram_reg_1_5));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[2]_i_3 
       (.I0(\_DOUT_1[11]_22 [2]),
        .I1(dout0_out[2]),
        .I2(ctrl_mux_out_1[1]),
        .I3(\y_reg[23]_i_1 [2]),
        .I4(ctrl_mux_out_1[0]),
        .I5(\y_reg[23]_i_1_0 [2]),
        .O(ram_reg_0_2));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[3]_i_3 
       (.I0(\_DOUT_1[11]_22 [3]),
        .I1(dout0_out[3]),
        .I2(ctrl_mux_out_1[1]),
        .I3(\y_reg[23]_i_1 [3]),
        .I4(ctrl_mux_out_1[0]),
        .I5(\y_reg[23]_i_1_0 [3]),
        .O(ram_reg_0_3));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[4]_i_3 
       (.I0(\_DOUT_1[11]_22 [4]),
        .I1(dout0_out[4]),
        .I2(ctrl_mux_out_1[1]),
        .I3(\y_reg[23]_i_1 [4]),
        .I4(ctrl_mux_out_1[0]),
        .I5(\y_reg[23]_i_1_0 [4]),
        .O(ram_reg_0_4));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[5]_i_3 
       (.I0(\_DOUT_1[11]_22 [5]),
        .I1(dout0_out[5]),
        .I2(ctrl_mux_out_1[1]),
        .I3(\y_reg[23]_i_1 [5]),
        .I4(ctrl_mux_out_1[0]),
        .I5(\y_reg[23]_i_1_0 [5]),
        .O(ram_reg_0_5));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[6]_i_3 
       (.I0(\_DOUT_1[11]_22 [6]),
        .I1(dout0_out[6]),
        .I2(ctrl_mux_out_1[1]),
        .I3(\y_reg[23]_i_1 [6]),
        .I4(ctrl_mux_out_1[0]),
        .I5(\y_reg[23]_i_1_0 [6]),
        .O(ram_reg_0_6));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[7]_i_3 
       (.I0(\_DOUT_1[11]_22 [7]),
        .I1(dout0_out[7]),
        .I2(ctrl_mux_out_1[1]),
        .I3(\y_reg[23]_i_1 [7]),
        .I4(ctrl_mux_out_1[0]),
        .I5(\y_reg[23]_i_1_0 [7]),
        .O(ram_reg_0_7));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[8]_i_3 
       (.I0(\_DOUT_1[11]_22 [8]),
        .I1(dout0_out[8]),
        .I2(ctrl_mux_out_1[1]),
        .I3(\y_reg[23]_i_1 [8]),
        .I4(ctrl_mux_out_1[0]),
        .I5(\y_reg[23]_i_1_0 [8]),
        .O(ram_reg_0_8));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[9]_i_3 
       (.I0(\_DOUT_1[11]_22 [9]),
        .I1(dout0_out[9]),
        .I2(ctrl_mux_out_1[1]),
        .I3(\y_reg[23]_i_1 [9]),
        .I4(ctrl_mux_out_1[0]),
        .I5(\y_reg[23]_i_1_0 [9]),
        .O(ram_reg_0_9));
endmodule

(* ORIG_REF_NAME = "rams_sp_rf_rst" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_30
   (dout0_out,
    m00_axis_aclk,
    Q,
    addr,
    s00_axis_tdata,
    s00_axis_tuser,
    s00_axis_tvalid);
  output [23:0]dout0_out;
  input m00_axis_aclk;
  input [0:0]Q;
  input [10:0]addr;
  input [23:0]s00_axis_tdata;
  input [0:0]s00_axis_tuser;
  input s00_axis_tvalid;

  wire [0:0]Q;
  wire [10:0]addr;
  wire [23:0]dout0_out;
  wire m00_axis_aclk;
  wire ram_reg_0_i_1__16_n_0;
  wire [23:0]s00_axis_tdata;
  wire [0:0]s00_axis_tuser;
  wire s00_axis_tvalid;
  wire NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED;
  wire NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED;
  wire NLW_ram_reg_0_DBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_SBITERR_UNCONNECTED;
  wire [31:16]NLW_ram_reg_0_DOADO_UNCONNECTED;
  wire [31:0]NLW_ram_reg_0_DOBDO_UNCONNECTED;
  wire [3:2]NLW_ram_reg_0_DOPADOP_UNCONNECTED;
  wire [3:0]NLW_ram_reg_0_DOPBDOP_UNCONNECTED;
  wire [7:0]NLW_ram_reg_0_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_ram_reg_0_RDADDRECC_UNCONNECTED;
  wire [15:6]NLW_ram_reg_1_DOADO_UNCONNECTED;
  wire [15:0]NLW_ram_reg_1_DOBDO_UNCONNECTED;
  wire [1:0]NLW_ram_reg_1_DOPADOP_UNCONNECTED;
  wire [1:0]NLW_ram_reg_1_DOPBDOP_UNCONNECTED;

  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p2_d16" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "46080" *) 
  (* RTL_RAM_NAME = "ram" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "2047" *) 
  (* bram_slice_begin = "0" *) 
  (* bram_slice_end = "17" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "2047" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "17" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(18),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(18),
    .WRITE_WIDTH_B(0)) 
    ram_reg_0
       (.ADDRARDADDR({1'b1,addr,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CASCADEINA(1'b1),
        .CASCADEINB(1'b0),
        .CASCADEOUTA(NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED),
        .CASCADEOUTB(NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED),
        .CLKARDCLK(m00_axis_aclk),
        .CLKBWRCLK(1'b0),
        .DBITERR(NLW_ram_reg_0_DBITERR_UNCONNECTED),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,s00_axis_tdata[15:0]}),
        .DIBDI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0,s00_axis_tdata[17:16]}),
        .DIPBDIP({1'b1,1'b1,1'b1,1'b1}),
        .DOADO({NLW_ram_reg_0_DOADO_UNCONNECTED[31:16],dout0_out[15:0]}),
        .DOBDO(NLW_ram_reg_0_DOBDO_UNCONNECTED[31:0]),
        .DOPADOP({NLW_ram_reg_0_DOPADOP_UNCONNECTED[3:2],dout0_out[17:16]}),
        .DOPBDOP(NLW_ram_reg_0_DOPBDOP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_ram_reg_0_ECCPARITY_UNCONNECTED[7:0]),
        .ENARDEN(Q),
        .ENBWREN(1'b0),
        .INJECTDBITERR(NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED),
        .INJECTSBITERR(NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED),
        .RDADDRECC(NLW_ram_reg_0_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_ram_reg_0_SBITERR_UNCONNECTED),
        .WEA({ram_reg_0_i_1__16_n_0,ram_reg_0_i_1__16_n_0,ram_reg_0_i_1__16_n_0,ram_reg_0_i_1__16_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
  LUT3 #(
    .INIT(8'h80)) 
    ram_reg_0_i_1__16
       (.I0(Q),
        .I1(s00_axis_tuser),
        .I2(s00_axis_tvalid),
        .O(ram_reg_0_i_1__16_n_0));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d6" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "46080" *) 
  (* RTL_RAM_NAME = "ram" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "2047" *) 
  (* bram_slice_begin = "18" *) 
  (* bram_slice_end = "23" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "2047" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "18" *) 
  (* ram_slice_end = "23" *) 
  RAMB18E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .INIT_A(18'h00000),
    .INIT_B(18'h00000),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(18'h00000),
    .SRVAL_B(18'h00000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(0)) 
    ram_reg_1
       (.ADDRARDADDR({addr,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CLKARDCLK(m00_axis_aclk),
        .CLKBWRCLK(1'b0),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,s00_axis_tdata[23:18]}),
        .DIBDI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0}),
        .DIPBDIP({1'b1,1'b1}),
        .DOADO({NLW_ram_reg_1_DOADO_UNCONNECTED[15:6],dout0_out[23:18]}),
        .DOBDO(NLW_ram_reg_1_DOBDO_UNCONNECTED[15:0]),
        .DOPADOP(NLW_ram_reg_1_DOPADOP_UNCONNECTED[1:0]),
        .DOPBDOP(NLW_ram_reg_1_DOPBDOP_UNCONNECTED[1:0]),
        .ENARDEN(Q),
        .ENBWREN(1'b0),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .WEA({ram_reg_0_i_1__16_n_0,ram_reg_0_i_1__16_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0}));
endmodule

(* ORIG_REF_NAME = "rams_sp_rf_rst" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_4
   (ram_reg_0_0,
    ram_reg_0_1,
    ram_reg_0_2,
    ram_reg_0_3,
    ram_reg_0_4,
    ram_reg_0_5,
    ram_reg_0_6,
    ram_reg_0_7,
    ram_reg_0_8,
    ram_reg_0_9,
    ram_reg_0_10,
    ram_reg_0_11,
    ram_reg_0_12,
    ram_reg_0_13,
    ram_reg_0_14,
    ram_reg_0_15,
    ram_reg_0_16,
    ram_reg_0_17,
    ram_reg_1_0,
    ram_reg_1_1,
    ram_reg_1_2,
    ram_reg_1_3,
    ram_reg_1_4,
    ram_reg_1_5,
    dout0_out,
    ctrl_mux_out_2,
    \y_reg[23]_i_1__0 ,
    \y_reg[23]_i_1__0_0 ,
    m00_axis_aclk,
    Q,
    addr,
    s00_axis_tdata,
    s00_axis_tuser,
    s00_axis_tvalid);
  output ram_reg_0_0;
  output ram_reg_0_1;
  output ram_reg_0_2;
  output ram_reg_0_3;
  output ram_reg_0_4;
  output ram_reg_0_5;
  output ram_reg_0_6;
  output ram_reg_0_7;
  output ram_reg_0_8;
  output ram_reg_0_9;
  output ram_reg_0_10;
  output ram_reg_0_11;
  output ram_reg_0_12;
  output ram_reg_0_13;
  output ram_reg_0_14;
  output ram_reg_0_15;
  output ram_reg_0_16;
  output ram_reg_0_17;
  output ram_reg_1_0;
  output ram_reg_1_1;
  output ram_reg_1_2;
  output ram_reg_1_3;
  output ram_reg_1_4;
  output ram_reg_1_5;
  input [23:0]dout0_out;
  input [1:0]ctrl_mux_out_2;
  input [23:0]\y_reg[23]_i_1__0 ;
  input [23:0]\y_reg[23]_i_1__0_0 ;
  input m00_axis_aclk;
  input [0:0]Q;
  input [10:0]addr;
  input [23:0]s00_axis_tdata;
  input [0:0]s00_axis_tuser;
  input s00_axis_tvalid;

  wire [0:0]Q;
  wire [23:0]\_DOUT_2[11]_23 ;
  wire [10:0]addr;
  wire [1:0]ctrl_mux_out_2;
  wire [23:0]dout0_out;
  wire m00_axis_aclk;
  wire ram_reg_0_0;
  wire ram_reg_0_1;
  wire ram_reg_0_10;
  wire ram_reg_0_11;
  wire ram_reg_0_12;
  wire ram_reg_0_13;
  wire ram_reg_0_14;
  wire ram_reg_0_15;
  wire ram_reg_0_16;
  wire ram_reg_0_17;
  wire ram_reg_0_2;
  wire ram_reg_0_3;
  wire ram_reg_0_4;
  wire ram_reg_0_5;
  wire ram_reg_0_6;
  wire ram_reg_0_7;
  wire ram_reg_0_8;
  wire ram_reg_0_9;
  wire ram_reg_0_i_1__20_n_0;
  wire ram_reg_1_0;
  wire ram_reg_1_1;
  wire ram_reg_1_2;
  wire ram_reg_1_3;
  wire ram_reg_1_4;
  wire ram_reg_1_5;
  wire [23:0]s00_axis_tdata;
  wire [0:0]s00_axis_tuser;
  wire s00_axis_tvalid;
  wire [23:0]\y_reg[23]_i_1__0 ;
  wire [23:0]\y_reg[23]_i_1__0_0 ;
  wire NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED;
  wire NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED;
  wire NLW_ram_reg_0_DBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_SBITERR_UNCONNECTED;
  wire [31:16]NLW_ram_reg_0_DOADO_UNCONNECTED;
  wire [31:0]NLW_ram_reg_0_DOBDO_UNCONNECTED;
  wire [3:2]NLW_ram_reg_0_DOPADOP_UNCONNECTED;
  wire [3:0]NLW_ram_reg_0_DOPBDOP_UNCONNECTED;
  wire [7:0]NLW_ram_reg_0_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_ram_reg_0_RDADDRECC_UNCONNECTED;
  wire [15:6]NLW_ram_reg_1_DOADO_UNCONNECTED;
  wire [15:0]NLW_ram_reg_1_DOBDO_UNCONNECTED;
  wire [1:0]NLW_ram_reg_1_DOPADOP_UNCONNECTED;
  wire [1:0]NLW_ram_reg_1_DOPBDOP_UNCONNECTED;

  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p2_d16" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "46080" *) 
  (* RTL_RAM_NAME = "ram" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "2047" *) 
  (* bram_slice_begin = "0" *) 
  (* bram_slice_end = "17" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "2047" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "17" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(18),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(18),
    .WRITE_WIDTH_B(0)) 
    ram_reg_0
       (.ADDRARDADDR({1'b1,addr,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CASCADEINA(1'b1),
        .CASCADEINB(1'b0),
        .CASCADEOUTA(NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED),
        .CASCADEOUTB(NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED),
        .CLKARDCLK(m00_axis_aclk),
        .CLKBWRCLK(1'b0),
        .DBITERR(NLW_ram_reg_0_DBITERR_UNCONNECTED),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,s00_axis_tdata[15:0]}),
        .DIBDI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0,s00_axis_tdata[17:16]}),
        .DIPBDIP({1'b1,1'b1,1'b1,1'b1}),
        .DOADO({NLW_ram_reg_0_DOADO_UNCONNECTED[31:16],\_DOUT_2[11]_23 [15:0]}),
        .DOBDO(NLW_ram_reg_0_DOBDO_UNCONNECTED[31:0]),
        .DOPADOP({NLW_ram_reg_0_DOPADOP_UNCONNECTED[3:2],\_DOUT_2[11]_23 [17:16]}),
        .DOPBDOP(NLW_ram_reg_0_DOPBDOP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_ram_reg_0_ECCPARITY_UNCONNECTED[7:0]),
        .ENARDEN(Q),
        .ENBWREN(1'b0),
        .INJECTDBITERR(NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED),
        .INJECTSBITERR(NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED),
        .RDADDRECC(NLW_ram_reg_0_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_ram_reg_0_SBITERR_UNCONNECTED),
        .WEA({ram_reg_0_i_1__20_n_0,ram_reg_0_i_1__20_n_0,ram_reg_0_i_1__20_n_0,ram_reg_0_i_1__20_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
  LUT3 #(
    .INIT(8'h80)) 
    ram_reg_0_i_1__20
       (.I0(Q),
        .I1(s00_axis_tuser),
        .I2(s00_axis_tvalid),
        .O(ram_reg_0_i_1__20_n_0));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d6" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "46080" *) 
  (* RTL_RAM_NAME = "ram" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "2047" *) 
  (* bram_slice_begin = "18" *) 
  (* bram_slice_end = "23" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "2047" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "18" *) 
  (* ram_slice_end = "23" *) 
  RAMB18E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .INIT_A(18'h00000),
    .INIT_B(18'h00000),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(18'h00000),
    .SRVAL_B(18'h00000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(0)) 
    ram_reg_1
       (.ADDRARDADDR({addr,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CLKARDCLK(m00_axis_aclk),
        .CLKBWRCLK(1'b0),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,s00_axis_tdata[23:18]}),
        .DIBDI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0}),
        .DIPBDIP({1'b1,1'b1}),
        .DOADO({NLW_ram_reg_1_DOADO_UNCONNECTED[15:6],\_DOUT_2[11]_23 [23:18]}),
        .DOBDO(NLW_ram_reg_1_DOBDO_UNCONNECTED[15:0]),
        .DOPADOP(NLW_ram_reg_1_DOPADOP_UNCONNECTED[1:0]),
        .DOPBDOP(NLW_ram_reg_1_DOPBDOP_UNCONNECTED[1:0]),
        .ENARDEN(Q),
        .ENBWREN(1'b0),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .WEA({ram_reg_0_i_1__20_n_0,ram_reg_0_i_1__20_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0}));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[0]_i_3__0 
       (.I0(\_DOUT_2[11]_23 [0]),
        .I1(dout0_out[0]),
        .I2(ctrl_mux_out_2[1]),
        .I3(\y_reg[23]_i_1__0 [0]),
        .I4(ctrl_mux_out_2[0]),
        .I5(\y_reg[23]_i_1__0_0 [0]),
        .O(ram_reg_0_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[10]_i_3__0 
       (.I0(\_DOUT_2[11]_23 [10]),
        .I1(dout0_out[10]),
        .I2(ctrl_mux_out_2[1]),
        .I3(\y_reg[23]_i_1__0 [10]),
        .I4(ctrl_mux_out_2[0]),
        .I5(\y_reg[23]_i_1__0_0 [10]),
        .O(ram_reg_0_10));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[11]_i_3__0 
       (.I0(\_DOUT_2[11]_23 [11]),
        .I1(dout0_out[11]),
        .I2(ctrl_mux_out_2[1]),
        .I3(\y_reg[23]_i_1__0 [11]),
        .I4(ctrl_mux_out_2[0]),
        .I5(\y_reg[23]_i_1__0_0 [11]),
        .O(ram_reg_0_11));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[12]_i_3__0 
       (.I0(\_DOUT_2[11]_23 [12]),
        .I1(dout0_out[12]),
        .I2(ctrl_mux_out_2[1]),
        .I3(\y_reg[23]_i_1__0 [12]),
        .I4(ctrl_mux_out_2[0]),
        .I5(\y_reg[23]_i_1__0_0 [12]),
        .O(ram_reg_0_12));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[13]_i_3__0 
       (.I0(\_DOUT_2[11]_23 [13]),
        .I1(dout0_out[13]),
        .I2(ctrl_mux_out_2[1]),
        .I3(\y_reg[23]_i_1__0 [13]),
        .I4(ctrl_mux_out_2[0]),
        .I5(\y_reg[23]_i_1__0_0 [13]),
        .O(ram_reg_0_13));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[14]_i_3__0 
       (.I0(\_DOUT_2[11]_23 [14]),
        .I1(dout0_out[14]),
        .I2(ctrl_mux_out_2[1]),
        .I3(\y_reg[23]_i_1__0 [14]),
        .I4(ctrl_mux_out_2[0]),
        .I5(\y_reg[23]_i_1__0_0 [14]),
        .O(ram_reg_0_14));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[15]_i_3__0 
       (.I0(\_DOUT_2[11]_23 [15]),
        .I1(dout0_out[15]),
        .I2(ctrl_mux_out_2[1]),
        .I3(\y_reg[23]_i_1__0 [15]),
        .I4(ctrl_mux_out_2[0]),
        .I5(\y_reg[23]_i_1__0_0 [15]),
        .O(ram_reg_0_15));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[16]_i_3__0 
       (.I0(\_DOUT_2[11]_23 [16]),
        .I1(dout0_out[16]),
        .I2(ctrl_mux_out_2[1]),
        .I3(\y_reg[23]_i_1__0 [16]),
        .I4(ctrl_mux_out_2[0]),
        .I5(\y_reg[23]_i_1__0_0 [16]),
        .O(ram_reg_0_16));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[17]_i_3__0 
       (.I0(\_DOUT_2[11]_23 [17]),
        .I1(dout0_out[17]),
        .I2(ctrl_mux_out_2[1]),
        .I3(\y_reg[23]_i_1__0 [17]),
        .I4(ctrl_mux_out_2[0]),
        .I5(\y_reg[23]_i_1__0_0 [17]),
        .O(ram_reg_0_17));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[18]_i_3__0 
       (.I0(\_DOUT_2[11]_23 [18]),
        .I1(dout0_out[18]),
        .I2(ctrl_mux_out_2[1]),
        .I3(\y_reg[23]_i_1__0 [18]),
        .I4(ctrl_mux_out_2[0]),
        .I5(\y_reg[23]_i_1__0_0 [18]),
        .O(ram_reg_1_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[19]_i_3__0 
       (.I0(\_DOUT_2[11]_23 [19]),
        .I1(dout0_out[19]),
        .I2(ctrl_mux_out_2[1]),
        .I3(\y_reg[23]_i_1__0 [19]),
        .I4(ctrl_mux_out_2[0]),
        .I5(\y_reg[23]_i_1__0_0 [19]),
        .O(ram_reg_1_1));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[1]_i_3__0 
       (.I0(\_DOUT_2[11]_23 [1]),
        .I1(dout0_out[1]),
        .I2(ctrl_mux_out_2[1]),
        .I3(\y_reg[23]_i_1__0 [1]),
        .I4(ctrl_mux_out_2[0]),
        .I5(\y_reg[23]_i_1__0_0 [1]),
        .O(ram_reg_0_1));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[20]_i_3__0 
       (.I0(\_DOUT_2[11]_23 [20]),
        .I1(dout0_out[20]),
        .I2(ctrl_mux_out_2[1]),
        .I3(\y_reg[23]_i_1__0 [20]),
        .I4(ctrl_mux_out_2[0]),
        .I5(\y_reg[23]_i_1__0_0 [20]),
        .O(ram_reg_1_2));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[21]_i_3__0 
       (.I0(\_DOUT_2[11]_23 [21]),
        .I1(dout0_out[21]),
        .I2(ctrl_mux_out_2[1]),
        .I3(\y_reg[23]_i_1__0 [21]),
        .I4(ctrl_mux_out_2[0]),
        .I5(\y_reg[23]_i_1__0_0 [21]),
        .O(ram_reg_1_3));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[22]_i_3__0 
       (.I0(\_DOUT_2[11]_23 [22]),
        .I1(dout0_out[22]),
        .I2(ctrl_mux_out_2[1]),
        .I3(\y_reg[23]_i_1__0 [22]),
        .I4(ctrl_mux_out_2[0]),
        .I5(\y_reg[23]_i_1__0_0 [22]),
        .O(ram_reg_1_4));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[23]_i_4__0 
       (.I0(\_DOUT_2[11]_23 [23]),
        .I1(dout0_out[23]),
        .I2(ctrl_mux_out_2[1]),
        .I3(\y_reg[23]_i_1__0 [23]),
        .I4(ctrl_mux_out_2[0]),
        .I5(\y_reg[23]_i_1__0_0 [23]),
        .O(ram_reg_1_5));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[2]_i_3__0 
       (.I0(\_DOUT_2[11]_23 [2]),
        .I1(dout0_out[2]),
        .I2(ctrl_mux_out_2[1]),
        .I3(\y_reg[23]_i_1__0 [2]),
        .I4(ctrl_mux_out_2[0]),
        .I5(\y_reg[23]_i_1__0_0 [2]),
        .O(ram_reg_0_2));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[3]_i_3__0 
       (.I0(\_DOUT_2[11]_23 [3]),
        .I1(dout0_out[3]),
        .I2(ctrl_mux_out_2[1]),
        .I3(\y_reg[23]_i_1__0 [3]),
        .I4(ctrl_mux_out_2[0]),
        .I5(\y_reg[23]_i_1__0_0 [3]),
        .O(ram_reg_0_3));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[4]_i_3__0 
       (.I0(\_DOUT_2[11]_23 [4]),
        .I1(dout0_out[4]),
        .I2(ctrl_mux_out_2[1]),
        .I3(\y_reg[23]_i_1__0 [4]),
        .I4(ctrl_mux_out_2[0]),
        .I5(\y_reg[23]_i_1__0_0 [4]),
        .O(ram_reg_0_4));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[5]_i_3__0 
       (.I0(\_DOUT_2[11]_23 [5]),
        .I1(dout0_out[5]),
        .I2(ctrl_mux_out_2[1]),
        .I3(\y_reg[23]_i_1__0 [5]),
        .I4(ctrl_mux_out_2[0]),
        .I5(\y_reg[23]_i_1__0_0 [5]),
        .O(ram_reg_0_5));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[6]_i_3__0 
       (.I0(\_DOUT_2[11]_23 [6]),
        .I1(dout0_out[6]),
        .I2(ctrl_mux_out_2[1]),
        .I3(\y_reg[23]_i_1__0 [6]),
        .I4(ctrl_mux_out_2[0]),
        .I5(\y_reg[23]_i_1__0_0 [6]),
        .O(ram_reg_0_6));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[7]_i_3__0 
       (.I0(\_DOUT_2[11]_23 [7]),
        .I1(dout0_out[7]),
        .I2(ctrl_mux_out_2[1]),
        .I3(\y_reg[23]_i_1__0 [7]),
        .I4(ctrl_mux_out_2[0]),
        .I5(\y_reg[23]_i_1__0_0 [7]),
        .O(ram_reg_0_7));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[8]_i_3__0 
       (.I0(\_DOUT_2[11]_23 [8]),
        .I1(dout0_out[8]),
        .I2(ctrl_mux_out_2[1]),
        .I3(\y_reg[23]_i_1__0 [8]),
        .I4(ctrl_mux_out_2[0]),
        .I5(\y_reg[23]_i_1__0_0 [8]),
        .O(ram_reg_0_8));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_reg[9]_i_3__0 
       (.I0(\_DOUT_2[11]_23 [9]),
        .I1(dout0_out[9]),
        .I2(ctrl_mux_out_2[1]),
        .I3(\y_reg[23]_i_1__0 [9]),
        .I4(ctrl_mux_out_2[0]),
        .I5(\y_reg[23]_i_1__0_0 [9]),
        .O(ram_reg_0_9));
endmodule

(* ORIG_REF_NAME = "rams_sp_rf_rst" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_5
   (dout0_out,
    m00_axis_aclk,
    Q,
    addr,
    s00_axis_tdata,
    s00_axis_tuser,
    s00_axis_tvalid);
  output [23:0]dout0_out;
  input m00_axis_aclk;
  input [0:0]Q;
  input [10:0]addr;
  input [23:0]s00_axis_tdata;
  input [0:0]s00_axis_tuser;
  input s00_axis_tvalid;

  wire [0:0]Q;
  wire [10:0]addr;
  wire [23:0]dout0_out;
  wire m00_axis_aclk;
  wire ram_reg_0_i_1__21_n_0;
  wire [23:0]s00_axis_tdata;
  wire [0:0]s00_axis_tuser;
  wire s00_axis_tvalid;
  wire NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED;
  wire NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED;
  wire NLW_ram_reg_0_DBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_SBITERR_UNCONNECTED;
  wire [31:16]NLW_ram_reg_0_DOADO_UNCONNECTED;
  wire [31:0]NLW_ram_reg_0_DOBDO_UNCONNECTED;
  wire [3:2]NLW_ram_reg_0_DOPADOP_UNCONNECTED;
  wire [3:0]NLW_ram_reg_0_DOPBDOP_UNCONNECTED;
  wire [7:0]NLW_ram_reg_0_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_ram_reg_0_RDADDRECC_UNCONNECTED;
  wire [15:6]NLW_ram_reg_1_DOADO_UNCONNECTED;
  wire [15:0]NLW_ram_reg_1_DOBDO_UNCONNECTED;
  wire [1:0]NLW_ram_reg_1_DOPADOP_UNCONNECTED;
  wire [1:0]NLW_ram_reg_1_DOPBDOP_UNCONNECTED;

  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p2_d16" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "46080" *) 
  (* RTL_RAM_NAME = "ram" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "2047" *) 
  (* bram_slice_begin = "0" *) 
  (* bram_slice_end = "17" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "2047" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "17" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(18),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(18),
    .WRITE_WIDTH_B(0)) 
    ram_reg_0
       (.ADDRARDADDR({1'b1,addr,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CASCADEINA(1'b1),
        .CASCADEINB(1'b0),
        .CASCADEOUTA(NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED),
        .CASCADEOUTB(NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED),
        .CLKARDCLK(m00_axis_aclk),
        .CLKBWRCLK(1'b0),
        .DBITERR(NLW_ram_reg_0_DBITERR_UNCONNECTED),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,s00_axis_tdata[15:0]}),
        .DIBDI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0,s00_axis_tdata[17:16]}),
        .DIPBDIP({1'b1,1'b1,1'b1,1'b1}),
        .DOADO({NLW_ram_reg_0_DOADO_UNCONNECTED[31:16],dout0_out[15:0]}),
        .DOBDO(NLW_ram_reg_0_DOBDO_UNCONNECTED[31:0]),
        .DOPADOP({NLW_ram_reg_0_DOPADOP_UNCONNECTED[3:2],dout0_out[17:16]}),
        .DOPBDOP(NLW_ram_reg_0_DOPBDOP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_ram_reg_0_ECCPARITY_UNCONNECTED[7:0]),
        .ENARDEN(Q),
        .ENBWREN(1'b0),
        .INJECTDBITERR(NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED),
        .INJECTSBITERR(NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED),
        .RDADDRECC(NLW_ram_reg_0_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_ram_reg_0_SBITERR_UNCONNECTED),
        .WEA({ram_reg_0_i_1__21_n_0,ram_reg_0_i_1__21_n_0,ram_reg_0_i_1__21_n_0,ram_reg_0_i_1__21_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
  LUT3 #(
    .INIT(8'h20)) 
    ram_reg_0_i_1__21
       (.I0(Q),
        .I1(s00_axis_tuser),
        .I2(s00_axis_tvalid),
        .O(ram_reg_0_i_1__21_n_0));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d6" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "46080" *) 
  (* RTL_RAM_NAME = "ram" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "2047" *) 
  (* bram_slice_begin = "18" *) 
  (* bram_slice_end = "23" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "2047" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "18" *) 
  (* ram_slice_end = "23" *) 
  RAMB18E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .INIT_A(18'h00000),
    .INIT_B(18'h00000),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(18'h00000),
    .SRVAL_B(18'h00000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(0)) 
    ram_reg_1
       (.ADDRARDADDR({addr,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CLKARDCLK(m00_axis_aclk),
        .CLKBWRCLK(1'b0),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,s00_axis_tdata[23:18]}),
        .DIBDI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0}),
        .DIPBDIP({1'b1,1'b1}),
        .DOADO({NLW_ram_reg_1_DOADO_UNCONNECTED[15:6],dout0_out[23:18]}),
        .DOBDO(NLW_ram_reg_1_DOBDO_UNCONNECTED[15:0]),
        .DOPADOP(NLW_ram_reg_1_DOPADOP_UNCONNECTED[1:0]),
        .DOPBDOP(NLW_ram_reg_1_DOPBDOP_UNCONNECTED[1:0]),
        .ENARDEN(Q),
        .ENBWREN(1'b0),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .WEA({ram_reg_0_i_1__21_n_0,ram_reg_0_i_1__21_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0}));
endmodule

(* ORIG_REF_NAME = "rams_sp_rf_rst" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_6
   (dout0_out,
    m00_axis_aclk,
    Q,
    addr,
    s00_axis_tdata,
    s00_axis_tuser,
    s00_axis_tvalid);
  output [23:0]dout0_out;
  input m00_axis_aclk;
  input [0:0]Q;
  input [10:0]addr;
  input [23:0]s00_axis_tdata;
  input [0:0]s00_axis_tuser;
  input s00_axis_tvalid;

  wire [0:0]Q;
  wire [10:0]addr;
  wire [23:0]dout0_out;
  wire m00_axis_aclk;
  wire ram_reg_0_i_1__22_n_0;
  wire [23:0]s00_axis_tdata;
  wire [0:0]s00_axis_tuser;
  wire s00_axis_tvalid;
  wire NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED;
  wire NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED;
  wire NLW_ram_reg_0_DBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_SBITERR_UNCONNECTED;
  wire [31:16]NLW_ram_reg_0_DOADO_UNCONNECTED;
  wire [31:0]NLW_ram_reg_0_DOBDO_UNCONNECTED;
  wire [3:2]NLW_ram_reg_0_DOPADOP_UNCONNECTED;
  wire [3:0]NLW_ram_reg_0_DOPBDOP_UNCONNECTED;
  wire [7:0]NLW_ram_reg_0_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_ram_reg_0_RDADDRECC_UNCONNECTED;
  wire [15:6]NLW_ram_reg_1_DOADO_UNCONNECTED;
  wire [15:0]NLW_ram_reg_1_DOBDO_UNCONNECTED;
  wire [1:0]NLW_ram_reg_1_DOPADOP_UNCONNECTED;
  wire [1:0]NLW_ram_reg_1_DOPBDOP_UNCONNECTED;

  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p2_d16" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "46080" *) 
  (* RTL_RAM_NAME = "ram" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "2047" *) 
  (* bram_slice_begin = "0" *) 
  (* bram_slice_end = "17" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "2047" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "17" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(18),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(18),
    .WRITE_WIDTH_B(0)) 
    ram_reg_0
       (.ADDRARDADDR({1'b1,addr,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CASCADEINA(1'b1),
        .CASCADEINB(1'b0),
        .CASCADEOUTA(NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED),
        .CASCADEOUTB(NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED),
        .CLKARDCLK(m00_axis_aclk),
        .CLKBWRCLK(1'b0),
        .DBITERR(NLW_ram_reg_0_DBITERR_UNCONNECTED),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,s00_axis_tdata[15:0]}),
        .DIBDI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0,s00_axis_tdata[17:16]}),
        .DIPBDIP({1'b1,1'b1,1'b1,1'b1}),
        .DOADO({NLW_ram_reg_0_DOADO_UNCONNECTED[31:16],dout0_out[15:0]}),
        .DOBDO(NLW_ram_reg_0_DOBDO_UNCONNECTED[31:0]),
        .DOPADOP({NLW_ram_reg_0_DOPADOP_UNCONNECTED[3:2],dout0_out[17:16]}),
        .DOPBDOP(NLW_ram_reg_0_DOPBDOP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_ram_reg_0_ECCPARITY_UNCONNECTED[7:0]),
        .ENARDEN(Q),
        .ENBWREN(1'b0),
        .INJECTDBITERR(NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED),
        .INJECTSBITERR(NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED),
        .RDADDRECC(NLW_ram_reg_0_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_ram_reg_0_SBITERR_UNCONNECTED),
        .WEA({ram_reg_0_i_1__22_n_0,ram_reg_0_i_1__22_n_0,ram_reg_0_i_1__22_n_0,ram_reg_0_i_1__22_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
  LUT3 #(
    .INIT(8'h80)) 
    ram_reg_0_i_1__22
       (.I0(Q),
        .I1(s00_axis_tuser),
        .I2(s00_axis_tvalid),
        .O(ram_reg_0_i_1__22_n_0));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d6" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "46080" *) 
  (* RTL_RAM_NAME = "ram" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "2047" *) 
  (* bram_slice_begin = "18" *) 
  (* bram_slice_end = "23" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "2047" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "18" *) 
  (* ram_slice_end = "23" *) 
  RAMB18E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .INIT_A(18'h00000),
    .INIT_B(18'h00000),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(18'h00000),
    .SRVAL_B(18'h00000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(0)) 
    ram_reg_1
       (.ADDRARDADDR({addr,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CLKARDCLK(m00_axis_aclk),
        .CLKBWRCLK(1'b0),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,s00_axis_tdata[23:18]}),
        .DIBDI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0}),
        .DIPBDIP({1'b1,1'b1}),
        .DOADO({NLW_ram_reg_1_DOADO_UNCONNECTED[15:6],dout0_out[23:18]}),
        .DOBDO(NLW_ram_reg_1_DOBDO_UNCONNECTED[15:0]),
        .DOPADOP(NLW_ram_reg_1_DOPADOP_UNCONNECTED[1:0]),
        .DOPBDOP(NLW_ram_reg_1_DOPBDOP_UNCONNECTED[1:0]),
        .ENARDEN(Q),
        .ENBWREN(1'b0),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .WEA({ram_reg_0_i_1__22_n_0,ram_reg_0_i_1__22_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0}));
endmodule

(* ORIG_REF_NAME = "rams_sp_rf_rst" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_7
   (dout0_out,
    m00_axis_aclk,
    Q,
    addr,
    s00_axis_tdata,
    s00_axis_tuser,
    s00_axis_tvalid);
  output [23:0]dout0_out;
  input m00_axis_aclk;
  input [0:0]Q;
  input [10:0]addr;
  input [23:0]s00_axis_tdata;
  input [0:0]s00_axis_tuser;
  input s00_axis_tvalid;

  wire [0:0]Q;
  wire [10:0]addr;
  wire [23:0]dout0_out;
  wire m00_axis_aclk;
  wire ram_reg_0_i_1__23_n_0;
  wire [23:0]s00_axis_tdata;
  wire [0:0]s00_axis_tuser;
  wire s00_axis_tvalid;
  wire NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED;
  wire NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED;
  wire NLW_ram_reg_0_DBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_SBITERR_UNCONNECTED;
  wire [31:16]NLW_ram_reg_0_DOADO_UNCONNECTED;
  wire [31:0]NLW_ram_reg_0_DOBDO_UNCONNECTED;
  wire [3:2]NLW_ram_reg_0_DOPADOP_UNCONNECTED;
  wire [3:0]NLW_ram_reg_0_DOPBDOP_UNCONNECTED;
  wire [7:0]NLW_ram_reg_0_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_ram_reg_0_RDADDRECC_UNCONNECTED;
  wire [15:6]NLW_ram_reg_1_DOADO_UNCONNECTED;
  wire [15:0]NLW_ram_reg_1_DOBDO_UNCONNECTED;
  wire [1:0]NLW_ram_reg_1_DOPADOP_UNCONNECTED;
  wire [1:0]NLW_ram_reg_1_DOPBDOP_UNCONNECTED;

  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p2_d16" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "46080" *) 
  (* RTL_RAM_NAME = "ram" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "2047" *) 
  (* bram_slice_begin = "0" *) 
  (* bram_slice_end = "17" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "2047" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "17" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(18),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(18),
    .WRITE_WIDTH_B(0)) 
    ram_reg_0
       (.ADDRARDADDR({1'b1,addr,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CASCADEINA(1'b1),
        .CASCADEINB(1'b0),
        .CASCADEOUTA(NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED),
        .CASCADEOUTB(NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED),
        .CLKARDCLK(m00_axis_aclk),
        .CLKBWRCLK(1'b0),
        .DBITERR(NLW_ram_reg_0_DBITERR_UNCONNECTED),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,s00_axis_tdata[15:0]}),
        .DIBDI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0,s00_axis_tdata[17:16]}),
        .DIPBDIP({1'b1,1'b1,1'b1,1'b1}),
        .DOADO({NLW_ram_reg_0_DOADO_UNCONNECTED[31:16],dout0_out[15:0]}),
        .DOBDO(NLW_ram_reg_0_DOBDO_UNCONNECTED[31:0]),
        .DOPADOP({NLW_ram_reg_0_DOPADOP_UNCONNECTED[3:2],dout0_out[17:16]}),
        .DOPBDOP(NLW_ram_reg_0_DOPBDOP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_ram_reg_0_ECCPARITY_UNCONNECTED[7:0]),
        .ENARDEN(Q),
        .ENBWREN(1'b0),
        .INJECTDBITERR(NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED),
        .INJECTSBITERR(NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED),
        .RDADDRECC(NLW_ram_reg_0_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_ram_reg_0_SBITERR_UNCONNECTED),
        .WEA({ram_reg_0_i_1__23_n_0,ram_reg_0_i_1__23_n_0,ram_reg_0_i_1__23_n_0,ram_reg_0_i_1__23_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
  LUT3 #(
    .INIT(8'h20)) 
    ram_reg_0_i_1__23
       (.I0(Q),
        .I1(s00_axis_tuser),
        .I2(s00_axis_tvalid),
        .O(ram_reg_0_i_1__23_n_0));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d6" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "46080" *) 
  (* RTL_RAM_NAME = "ram" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "2047" *) 
  (* bram_slice_begin = "18" *) 
  (* bram_slice_end = "23" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "2047" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "18" *) 
  (* ram_slice_end = "23" *) 
  RAMB18E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .INIT_A(18'h00000),
    .INIT_B(18'h00000),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(18'h00000),
    .SRVAL_B(18'h00000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(0)) 
    ram_reg_1
       (.ADDRARDADDR({addr,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CLKARDCLK(m00_axis_aclk),
        .CLKBWRCLK(1'b0),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,s00_axis_tdata[23:18]}),
        .DIBDI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0}),
        .DIPBDIP({1'b1,1'b1}),
        .DOADO({NLW_ram_reg_1_DOADO_UNCONNECTED[15:6],dout0_out[23:18]}),
        .DOBDO(NLW_ram_reg_1_DOBDO_UNCONNECTED[15:0]),
        .DOPADOP(NLW_ram_reg_1_DOPADOP_UNCONNECTED[1:0]),
        .DOPBDOP(NLW_ram_reg_1_DOPBDOP_UNCONNECTED[1:0]),
        .ENARDEN(Q),
        .ENBWREN(1'b0),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .WEA({ram_reg_0_i_1__23_n_0,ram_reg_0_i_1__23_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0}));
endmodule

(* ORIG_REF_NAME = "rams_sp_rf_rst" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_8
   (dout0_out,
    m00_axis_aclk,
    Q,
    addr,
    s00_axis_tdata,
    s00_axis_tuser,
    s00_axis_tvalid);
  output [23:0]dout0_out;
  input m00_axis_aclk;
  input [0:0]Q;
  input [10:0]addr;
  input [23:0]s00_axis_tdata;
  input [0:0]s00_axis_tuser;
  input s00_axis_tvalid;

  wire [0:0]Q;
  wire [10:0]addr;
  wire [23:0]dout0_out;
  wire m00_axis_aclk;
  wire ram_reg_0_i_1__24_n_0;
  wire [23:0]s00_axis_tdata;
  wire [0:0]s00_axis_tuser;
  wire s00_axis_tvalid;
  wire NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED;
  wire NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED;
  wire NLW_ram_reg_0_DBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_SBITERR_UNCONNECTED;
  wire [31:16]NLW_ram_reg_0_DOADO_UNCONNECTED;
  wire [31:0]NLW_ram_reg_0_DOBDO_UNCONNECTED;
  wire [3:2]NLW_ram_reg_0_DOPADOP_UNCONNECTED;
  wire [3:0]NLW_ram_reg_0_DOPBDOP_UNCONNECTED;
  wire [7:0]NLW_ram_reg_0_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_ram_reg_0_RDADDRECC_UNCONNECTED;
  wire [15:6]NLW_ram_reg_1_DOADO_UNCONNECTED;
  wire [15:0]NLW_ram_reg_1_DOBDO_UNCONNECTED;
  wire [1:0]NLW_ram_reg_1_DOPADOP_UNCONNECTED;
  wire [1:0]NLW_ram_reg_1_DOPBDOP_UNCONNECTED;

  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p2_d16" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "46080" *) 
  (* RTL_RAM_NAME = "ram" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "2047" *) 
  (* bram_slice_begin = "0" *) 
  (* bram_slice_end = "17" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "2047" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "17" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(18),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(18),
    .WRITE_WIDTH_B(0)) 
    ram_reg_0
       (.ADDRARDADDR({1'b1,addr,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CASCADEINA(1'b1),
        .CASCADEINB(1'b0),
        .CASCADEOUTA(NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED),
        .CASCADEOUTB(NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED),
        .CLKARDCLK(m00_axis_aclk),
        .CLKBWRCLK(1'b0),
        .DBITERR(NLW_ram_reg_0_DBITERR_UNCONNECTED),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,s00_axis_tdata[15:0]}),
        .DIBDI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0,s00_axis_tdata[17:16]}),
        .DIPBDIP({1'b1,1'b1,1'b1,1'b1}),
        .DOADO({NLW_ram_reg_0_DOADO_UNCONNECTED[31:16],dout0_out[15:0]}),
        .DOBDO(NLW_ram_reg_0_DOBDO_UNCONNECTED[31:0]),
        .DOPADOP({NLW_ram_reg_0_DOPADOP_UNCONNECTED[3:2],dout0_out[17:16]}),
        .DOPBDOP(NLW_ram_reg_0_DOPBDOP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_ram_reg_0_ECCPARITY_UNCONNECTED[7:0]),
        .ENARDEN(Q),
        .ENBWREN(1'b0),
        .INJECTDBITERR(NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED),
        .INJECTSBITERR(NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED),
        .RDADDRECC(NLW_ram_reg_0_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_ram_reg_0_SBITERR_UNCONNECTED),
        .WEA({ram_reg_0_i_1__24_n_0,ram_reg_0_i_1__24_n_0,ram_reg_0_i_1__24_n_0,ram_reg_0_i_1__24_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
  LUT3 #(
    .INIT(8'h80)) 
    ram_reg_0_i_1__24
       (.I0(Q),
        .I1(s00_axis_tuser),
        .I2(s00_axis_tvalid),
        .O(ram_reg_0_i_1__24_n_0));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d6" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "46080" *) 
  (* RTL_RAM_NAME = "ram" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "2047" *) 
  (* bram_slice_begin = "18" *) 
  (* bram_slice_end = "23" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "2047" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "18" *) 
  (* ram_slice_end = "23" *) 
  RAMB18E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .INIT_A(18'h00000),
    .INIT_B(18'h00000),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(18'h00000),
    .SRVAL_B(18'h00000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(0)) 
    ram_reg_1
       (.ADDRARDADDR({addr,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CLKARDCLK(m00_axis_aclk),
        .CLKBWRCLK(1'b0),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,s00_axis_tdata[23:18]}),
        .DIBDI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0}),
        .DIPBDIP({1'b1,1'b1}),
        .DOADO({NLW_ram_reg_1_DOADO_UNCONNECTED[15:6],dout0_out[23:18]}),
        .DOBDO(NLW_ram_reg_1_DOBDO_UNCONNECTED[15:0]),
        .DOPADOP(NLW_ram_reg_1_DOPADOP_UNCONNECTED[1:0]),
        .DOPBDOP(NLW_ram_reg_1_DOPBDOP_UNCONNECTED[1:0]),
        .ENARDEN(Q),
        .ENBWREN(1'b0),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .WEA({ram_reg_0_i_1__24_n_0,ram_reg_0_i_1__24_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0}));
endmodule

(* ORIG_REF_NAME = "rams_sp_rf_rst" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_9
   (dout0_out,
    m00_axis_aclk,
    Q,
    addr,
    s00_axis_tdata,
    s00_axis_tuser,
    s00_axis_tvalid);
  output [23:0]dout0_out;
  input m00_axis_aclk;
  input [0:0]Q;
  input [10:0]addr;
  input [23:0]s00_axis_tdata;
  input [0:0]s00_axis_tuser;
  input s00_axis_tvalid;

  wire [0:0]Q;
  wire [10:0]addr;
  wire [23:0]dout0_out;
  wire m00_axis_aclk;
  wire ram_reg_0_i_1__25_n_0;
  wire [23:0]s00_axis_tdata;
  wire [0:0]s00_axis_tuser;
  wire s00_axis_tvalid;
  wire NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED;
  wire NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED;
  wire NLW_ram_reg_0_DBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_SBITERR_UNCONNECTED;
  wire [31:16]NLW_ram_reg_0_DOADO_UNCONNECTED;
  wire [31:0]NLW_ram_reg_0_DOBDO_UNCONNECTED;
  wire [3:2]NLW_ram_reg_0_DOPADOP_UNCONNECTED;
  wire [3:0]NLW_ram_reg_0_DOPBDOP_UNCONNECTED;
  wire [7:0]NLW_ram_reg_0_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_ram_reg_0_RDADDRECC_UNCONNECTED;
  wire [15:6]NLW_ram_reg_1_DOADO_UNCONNECTED;
  wire [15:0]NLW_ram_reg_1_DOBDO_UNCONNECTED;
  wire [1:0]NLW_ram_reg_1_DOPADOP_UNCONNECTED;
  wire [1:0]NLW_ram_reg_1_DOPBDOP_UNCONNECTED;

  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p2_d16" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "46080" *) 
  (* RTL_RAM_NAME = "ram" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "2047" *) 
  (* bram_slice_begin = "0" *) 
  (* bram_slice_end = "17" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "2047" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "17" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(18),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(18),
    .WRITE_WIDTH_B(0)) 
    ram_reg_0
       (.ADDRARDADDR({1'b1,addr,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CASCADEINA(1'b1),
        .CASCADEINB(1'b0),
        .CASCADEOUTA(NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED),
        .CASCADEOUTB(NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED),
        .CLKARDCLK(m00_axis_aclk),
        .CLKBWRCLK(1'b0),
        .DBITERR(NLW_ram_reg_0_DBITERR_UNCONNECTED),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,s00_axis_tdata[15:0]}),
        .DIBDI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0,s00_axis_tdata[17:16]}),
        .DIPBDIP({1'b1,1'b1,1'b1,1'b1}),
        .DOADO({NLW_ram_reg_0_DOADO_UNCONNECTED[31:16],dout0_out[15:0]}),
        .DOBDO(NLW_ram_reg_0_DOBDO_UNCONNECTED[31:0]),
        .DOPADOP({NLW_ram_reg_0_DOPADOP_UNCONNECTED[3:2],dout0_out[17:16]}),
        .DOPBDOP(NLW_ram_reg_0_DOPBDOP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_ram_reg_0_ECCPARITY_UNCONNECTED[7:0]),
        .ENARDEN(Q),
        .ENBWREN(1'b0),
        .INJECTDBITERR(NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED),
        .INJECTSBITERR(NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED),
        .RDADDRECC(NLW_ram_reg_0_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_ram_reg_0_SBITERR_UNCONNECTED),
        .WEA({ram_reg_0_i_1__25_n_0,ram_reg_0_i_1__25_n_0,ram_reg_0_i_1__25_n_0,ram_reg_0_i_1__25_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
  LUT3 #(
    .INIT(8'h20)) 
    ram_reg_0_i_1__25
       (.I0(Q),
        .I1(s00_axis_tuser),
        .I2(s00_axis_tvalid),
        .O(ram_reg_0_i_1__25_n_0));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d6" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "46080" *) 
  (* RTL_RAM_NAME = "ram" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "2047" *) 
  (* bram_slice_begin = "18" *) 
  (* bram_slice_end = "23" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "2047" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "18" *) 
  (* ram_slice_end = "23" *) 
  RAMB18E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .INIT_A(18'h00000),
    .INIT_B(18'h00000),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(18'h00000),
    .SRVAL_B(18'h00000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(0)) 
    ram_reg_1
       (.ADDRARDADDR({addr,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CLKARDCLK(m00_axis_aclk),
        .CLKBWRCLK(1'b0),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,s00_axis_tdata[23:18]}),
        .DIBDI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0}),
        .DIPBDIP({1'b1,1'b1}),
        .DOADO({NLW_ram_reg_1_DOADO_UNCONNECTED[15:6],dout0_out[23:18]}),
        .DOBDO(NLW_ram_reg_1_DOBDO_UNCONNECTED[15:0]),
        .DOPADOP(NLW_ram_reg_1_DOPADOP_UNCONNECTED[1:0]),
        .DOPBDOP(NLW_ram_reg_1_DOPBDOP_UNCONNECTED[1:0]),
        .ENARDEN(Q),
        .ENBWREN(1'b0),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .WEA({ram_reg_0_i_1__25_n_0,ram_reg_0_i_1__25_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0}));
endmodule

(* CHECK_LICENSE_TYPE = "test_bd_pixelReOrder_0_0,pixelReOrder_v1_0,{}" *) (* DowngradeIPIdentifiedWarnings = "yes" *) (* X_CORE_INFO = "pixelReOrder_v1_0,Vivado 2019.1" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (m00_axis_aclk,
    m00_axis_aresetn,
    m00_axis_tvalid,
    m00_axis_tdata,
    m00_axis_tstrb,
    m00_axis_tlast,
    m00_axis_tready,
    s00_axis_aclk,
    s00_axis_aresetn,
    s00_axis_tready,
    s00_axis_tdata,
    s00_axis_tuser,
    s00_axis_tstrb,
    s00_axis_tlast,
    s00_axis_tvalid);
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 M00_AXIS_CLK CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME M00_AXIS_CLK, ASSOCIATED_BUSIF M00_AXIS, ASSOCIATED_RESET m00_axis_aresetn, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN test_bd_m00_axis_aclk_0, INSERT_VIP 0" *) input m00_axis_aclk;
  (* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 M00_AXIS_RST RST" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME M00_AXIS_RST, POLARITY ACTIVE_LOW, INSERT_VIP 0" *) input m00_axis_aresetn;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 M00_AXIS TVALID" *) output m00_axis_tvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 M00_AXIS TDATA" *) output [31:0]m00_axis_tdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 M00_AXIS TSTRB" *) output [3:0]m00_axis_tstrb;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 M00_AXIS TLAST" *) output m00_axis_tlast;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 M00_AXIS TREADY" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME M00_AXIS, WIZ_DATA_WIDTH 32, TDATA_NUM_BYTES 4, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 1, HAS_TKEEP 0, HAS_TLAST 1, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN test_bd_m00_axis_aclk_0, LAYERED_METADATA undef, INSERT_VIP 0" *) input m00_axis_tready;
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 S00_AXIS_CLK CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME S00_AXIS_CLK, ASSOCIATED_BUSIF S00_AXIS, ASSOCIATED_RESET s00_axis_aresetn, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN test_bd_m00_axis_aclk_0, INSERT_VIP 0" *) input s00_axis_aclk;
  (* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 S00_AXIS_RST RST" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME S00_AXIS_RST, POLARITY ACTIVE_LOW, INSERT_VIP 0" *) input s00_axis_aresetn;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 S00_AXIS TREADY" *) output s00_axis_tready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 S00_AXIS TDATA" *) input [31:0]s00_axis_tdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 S00_AXIS TUSER" *) input [47:0]s00_axis_tuser;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 S00_AXIS TSTRB" *) input [3:0]s00_axis_tstrb;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 S00_AXIS TLAST" *) input s00_axis_tlast;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 S00_AXIS TVALID" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME S00_AXIS, WIZ_DATA_WIDTH 32, TDATA_NUM_BYTES 4, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 48, HAS_TREADY 1, HAS_TSTRB 1, HAS_TKEEP 0, HAS_TLAST 1, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN test_bd_m00_axis_aclk_0, LAYERED_METADATA undef, INSERT_VIP 0" *) input s00_axis_tvalid;

  wire \<const0> ;
  wire \<const1> ;
  wire m00_axis_aclk;
  wire [23:0]\^m00_axis_tdata ;
  wire m00_axis_tlast;
  wire m00_axis_tvalid;
  wire s00_axis_aresetn;
  wire [31:0]s00_axis_tdata;
  wire [47:0]s00_axis_tuser;
  wire s00_axis_tvalid;

  assign m00_axis_tdata[31] = \<const0> ;
  assign m00_axis_tdata[30] = \<const0> ;
  assign m00_axis_tdata[29] = \<const0> ;
  assign m00_axis_tdata[28] = \<const0> ;
  assign m00_axis_tdata[27] = \<const0> ;
  assign m00_axis_tdata[26] = \<const0> ;
  assign m00_axis_tdata[25] = \<const0> ;
  assign m00_axis_tdata[24] = \<const0> ;
  assign m00_axis_tdata[23:0] = \^m00_axis_tdata [23:0];
  assign m00_axis_tstrb[3] = \<const1> ;
  assign m00_axis_tstrb[2] = \<const1> ;
  assign m00_axis_tstrb[1] = \<const1> ;
  assign m00_axis_tstrb[0] = \<const1> ;
  assign s00_axis_tready = \<const1> ;
  GND GND
       (.G(\<const0> ));
  VCC VCC
       (.P(\<const1> ));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_pixelReOrder_v1_0 inst
       (.m00_axis_aclk(m00_axis_aclk),
        .m00_axis_tdata(\^m00_axis_tdata ),
        .m00_axis_tlast(m00_axis_tlast),
        .m00_axis_tvalid(m00_axis_tvalid),
        .s00_axis_aresetn(s00_axis_aresetn),
        .s00_axis_tdata(s00_axis_tdata[23:0]),
        .s00_axis_tuser({s00_axis_tuser[23:20],s00_axis_tuser[10:0]}),
        .s00_axis_tvalid(s00_axis_tvalid));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
