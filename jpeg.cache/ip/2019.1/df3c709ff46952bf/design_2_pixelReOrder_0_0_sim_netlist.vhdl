-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1 (lin64) Build 2552052 Fri May 24 14:47:09 MDT 2019
-- Date        : Sat Sep 25 16:58:12 2021
-- Host        : alex-HP-Compaq-8200-Elite-CMT-PC running 64-bit Ubuntu 20.04.2 LTS
-- Command     : write_vhdl -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_2_pixelReOrder_0_0_sim_netlist.vhdl
-- Design      : design_2_pixelReOrder_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7z020clg484-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst is
  port (
    dout0_out : out STD_LOGIC_VECTOR ( 31 downto 0 );
    m00_axis_aclk : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 0 to 0 );
    ADDRARDADDR : in STD_LOGIC_VECTOR ( 10 downto 0 );
    ram_reg_1_0 : in STD_LOGIC_VECTOR ( 31 downto 0 );
    addr : in STD_LOGIC_VECTOR ( 10 downto 0 );
    WE_r : in STD_LOGIC;
    square_r : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst is
  signal ram_reg_0_i_1_n_0 : STD_LOGIC;
  signal NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_DBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_SBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_DOADO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 16 );
  signal NLW_ram_reg_0_DOBDO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_ram_reg_0_DOPADOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal NLW_ram_reg_0_DOPBDOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_ram_reg_0_ECCPARITY_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal NLW_ram_reg_0_RDADDRECC_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_ram_reg_1_CASCADEOUTA_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_1_CASCADEOUTB_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_1_DBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_1_INJECTDBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_1_INJECTSBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_1_SBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_1_DOADO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 14 );
  signal NLW_ram_reg_1_DOBDO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_ram_reg_1_DOPADOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_ram_reg_1_DOPBDOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_ram_reg_1_ECCPARITY_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal NLW_ram_reg_1_RDADDRECC_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ : string;
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of ram_reg_0 : label is "p2_d16";
  attribute METHODOLOGY_DRC_VIOS : string;
  attribute METHODOLOGY_DRC_VIOS of ram_reg_0 : label is "{SYNTH-6 {cell *THIS*}}";
  attribute RTL_RAM_BITS : integer;
  attribute RTL_RAM_BITS of ram_reg_0 : label is 61472;
  attribute RTL_RAM_NAME : string;
  attribute RTL_RAM_NAME of ram_reg_0 : label is "ram";
  attribute bram_addr_begin : integer;
  attribute bram_addr_begin of ram_reg_0 : label is 0;
  attribute bram_addr_end : integer;
  attribute bram_addr_end of ram_reg_0 : label is 2047;
  attribute bram_slice_begin : integer;
  attribute bram_slice_begin of ram_reg_0 : label is 0;
  attribute bram_slice_end : integer;
  attribute bram_slice_end of ram_reg_0 : label is 17;
  attribute ram_addr_begin : integer;
  attribute ram_addr_begin of ram_reg_0 : label is 0;
  attribute ram_addr_end : integer;
  attribute ram_addr_end of ram_reg_0 : label is 2047;
  attribute ram_offset : integer;
  attribute ram_offset of ram_reg_0 : label is 0;
  attribute ram_slice_begin : integer;
  attribute ram_slice_begin of ram_reg_0 : label is 0;
  attribute ram_slice_end : integer;
  attribute ram_slice_end of ram_reg_0 : label is 17;
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of ram_reg_1 : label is "p0_d14";
  attribute METHODOLOGY_DRC_VIOS of ram_reg_1 : label is "{SYNTH-6 {cell *THIS*}}";
  attribute RTL_RAM_BITS of ram_reg_1 : label is 61472;
  attribute RTL_RAM_NAME of ram_reg_1 : label is "ram";
  attribute bram_addr_begin of ram_reg_1 : label is 0;
  attribute bram_addr_end of ram_reg_1 : label is 2047;
  attribute bram_slice_begin of ram_reg_1 : label is 18;
  attribute bram_slice_end of ram_reg_1 : label is 31;
  attribute ram_addr_begin of ram_reg_1 : label is 0;
  attribute ram_addr_end of ram_reg_1 : label is 2047;
  attribute ram_offset of ram_reg_1 : label is 0;
  attribute ram_slice_begin of ram_reg_1 : label is 18;
  attribute ram_slice_end of ram_reg_1 : label is 31;
begin
ram_reg_0: unisim.vcomponents.RAMB36E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      EN_ECC_READ => false,
      EN_ECC_WRITE => false,
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      RAM_EXTENSION_A => "NONE",
      RAM_EXTENSION_B => "NONE",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "PERFORMANCE",
      READ_WIDTH_A => 18,
      READ_WIDTH_B => 0,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "READ_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 18,
      WRITE_WIDTH_B => 0
    )
        port map (
      ADDRARDADDR(15) => '1',
      ADDRARDADDR(14 downto 4) => ADDRARDADDR(10 downto 0),
      ADDRARDADDR(3 downto 0) => B"1111",
      ADDRBWRADDR(15 downto 0) => B"1111111111111111",
      CASCADEINA => '1',
      CASCADEINB => '0',
      CASCADEOUTA => NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED,
      CASCADEOUTB => NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED,
      CLKARDCLK => m00_axis_aclk,
      CLKBWRCLK => '0',
      DBITERR => NLW_ram_reg_0_DBITERR_UNCONNECTED,
      DIADI(31 downto 16) => B"0000000000000000",
      DIADI(15 downto 0) => ram_reg_1_0(15 downto 0),
      DIBDI(31 downto 0) => B"11111111111111111111111111111111",
      DIPADIP(3 downto 2) => B"00",
      DIPADIP(1 downto 0) => ram_reg_1_0(17 downto 16),
      DIPBDIP(3 downto 0) => B"1111",
      DOADO(31 downto 16) => NLW_ram_reg_0_DOADO_UNCONNECTED(31 downto 16),
      DOADO(15 downto 0) => dout0_out(15 downto 0),
      DOBDO(31 downto 0) => NLW_ram_reg_0_DOBDO_UNCONNECTED(31 downto 0),
      DOPADOP(3 downto 2) => NLW_ram_reg_0_DOPADOP_UNCONNECTED(3 downto 2),
      DOPADOP(1 downto 0) => dout0_out(17 downto 16),
      DOPBDOP(3 downto 0) => NLW_ram_reg_0_DOPBDOP_UNCONNECTED(3 downto 0),
      ECCPARITY(7 downto 0) => NLW_ram_reg_0_ECCPARITY_UNCONNECTED(7 downto 0),
      ENARDEN => Q(0),
      ENBWREN => '0',
      INJECTDBITERR => NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED,
      INJECTSBITERR => NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED,
      RDADDRECC(8 downto 0) => NLW_ram_reg_0_RDADDRECC_UNCONNECTED(8 downto 0),
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      SBITERR => NLW_ram_reg_0_SBITERR_UNCONNECTED,
      WEA(3) => ram_reg_0_i_1_n_0,
      WEA(2) => ram_reg_0_i_1_n_0,
      WEA(1) => ram_reg_0_i_1_n_0,
      WEA(0) => ram_reg_0_i_1_n_0,
      WEBWE(7 downto 0) => B"00000000"
    );
ram_reg_0_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => Q(0),
      I1 => WE_r,
      I2 => square_r,
      O => ram_reg_0_i_1_n_0
    );
ram_reg_1: unisim.vcomponents.RAMB36E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      EN_ECC_READ => false,
      EN_ECC_WRITE => false,
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      RAM_EXTENSION_A => "NONE",
      RAM_EXTENSION_B => "NONE",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "PERFORMANCE",
      READ_WIDTH_A => 18,
      READ_WIDTH_B => 0,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "READ_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 18,
      WRITE_WIDTH_B => 0
    )
        port map (
      ADDRARDADDR(15) => '1',
      ADDRARDADDR(14 downto 4) => addr(10 downto 0),
      ADDRARDADDR(3 downto 0) => B"1111",
      ADDRBWRADDR(15 downto 0) => B"1111111111111111",
      CASCADEINA => '1',
      CASCADEINB => '0',
      CASCADEOUTA => NLW_ram_reg_1_CASCADEOUTA_UNCONNECTED,
      CASCADEOUTB => NLW_ram_reg_1_CASCADEOUTB_UNCONNECTED,
      CLKARDCLK => m00_axis_aclk,
      CLKBWRCLK => '0',
      DBITERR => NLW_ram_reg_1_DBITERR_UNCONNECTED,
      DIADI(31 downto 14) => B"000000000000000000",
      DIADI(13 downto 0) => ram_reg_1_0(31 downto 18),
      DIBDI(31 downto 0) => B"11111111111111111111111111111111",
      DIPADIP(3 downto 0) => B"0000",
      DIPBDIP(3 downto 0) => B"1111",
      DOADO(31 downto 14) => NLW_ram_reg_1_DOADO_UNCONNECTED(31 downto 14),
      DOADO(13 downto 0) => dout0_out(31 downto 18),
      DOBDO(31 downto 0) => NLW_ram_reg_1_DOBDO_UNCONNECTED(31 downto 0),
      DOPADOP(3 downto 0) => NLW_ram_reg_1_DOPADOP_UNCONNECTED(3 downto 0),
      DOPBDOP(3 downto 0) => NLW_ram_reg_1_DOPBDOP_UNCONNECTED(3 downto 0),
      ECCPARITY(7 downto 0) => NLW_ram_reg_1_ECCPARITY_UNCONNECTED(7 downto 0),
      ENARDEN => Q(0),
      ENBWREN => '0',
      INJECTDBITERR => NLW_ram_reg_1_INJECTDBITERR_UNCONNECTED,
      INJECTSBITERR => NLW_ram_reg_1_INJECTSBITERR_UNCONNECTED,
      RDADDRECC(8 downto 0) => NLW_ram_reg_1_RDADDRECC_UNCONNECTED(8 downto 0),
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      SBITERR => NLW_ram_reg_1_SBITERR_UNCONNECTED,
      WEA(3) => ram_reg_0_i_1_n_0,
      WEA(2) => ram_reg_0_i_1_n_0,
      WEA(1) => ram_reg_0_i_1_n_0,
      WEA(0) => ram_reg_0_i_1_n_0,
      WEBWE(7 downto 0) => B"00000000"
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_0 is
  port (
    dout0_out : out STD_LOGIC_VECTOR ( 31 downto 0 );
    m00_axis_aclk : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 0 to 0 );
    ADDRARDADDR : in STD_LOGIC_VECTOR ( 10 downto 0 );
    ram_reg_1_0 : in STD_LOGIC_VECTOR ( 31 downto 0 );
    addr : in STD_LOGIC_VECTOR ( 10 downto 0 );
    WE_r : in STD_LOGIC;
    square_r : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_0 : entity is "rams_sp_rf_rst";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_0;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_0 is
  signal \ram_reg_0_i_1__0_n_0\ : STD_LOGIC;
  signal NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_DBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_SBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_DOADO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 16 );
  signal NLW_ram_reg_0_DOBDO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_ram_reg_0_DOPADOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal NLW_ram_reg_0_DOPBDOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_ram_reg_0_ECCPARITY_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal NLW_ram_reg_0_RDADDRECC_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_ram_reg_1_CASCADEOUTA_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_1_CASCADEOUTB_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_1_DBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_1_INJECTDBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_1_INJECTSBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_1_SBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_1_DOADO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 14 );
  signal NLW_ram_reg_1_DOBDO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_ram_reg_1_DOPADOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_ram_reg_1_DOPBDOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_ram_reg_1_ECCPARITY_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal NLW_ram_reg_1_RDADDRECC_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ : string;
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of ram_reg_0 : label is "p2_d16";
  attribute METHODOLOGY_DRC_VIOS : string;
  attribute METHODOLOGY_DRC_VIOS of ram_reg_0 : label is "{SYNTH-6 {cell *THIS*}}";
  attribute RTL_RAM_BITS : integer;
  attribute RTL_RAM_BITS of ram_reg_0 : label is 61472;
  attribute RTL_RAM_NAME : string;
  attribute RTL_RAM_NAME of ram_reg_0 : label is "ram";
  attribute bram_addr_begin : integer;
  attribute bram_addr_begin of ram_reg_0 : label is 0;
  attribute bram_addr_end : integer;
  attribute bram_addr_end of ram_reg_0 : label is 2047;
  attribute bram_slice_begin : integer;
  attribute bram_slice_begin of ram_reg_0 : label is 0;
  attribute bram_slice_end : integer;
  attribute bram_slice_end of ram_reg_0 : label is 17;
  attribute ram_addr_begin : integer;
  attribute ram_addr_begin of ram_reg_0 : label is 0;
  attribute ram_addr_end : integer;
  attribute ram_addr_end of ram_reg_0 : label is 2047;
  attribute ram_offset : integer;
  attribute ram_offset of ram_reg_0 : label is 0;
  attribute ram_slice_begin : integer;
  attribute ram_slice_begin of ram_reg_0 : label is 0;
  attribute ram_slice_end : integer;
  attribute ram_slice_end of ram_reg_0 : label is 17;
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of ram_reg_1 : label is "p0_d14";
  attribute METHODOLOGY_DRC_VIOS of ram_reg_1 : label is "{SYNTH-6 {cell *THIS*}}";
  attribute RTL_RAM_BITS of ram_reg_1 : label is 61472;
  attribute RTL_RAM_NAME of ram_reg_1 : label is "ram";
  attribute bram_addr_begin of ram_reg_1 : label is 0;
  attribute bram_addr_end of ram_reg_1 : label is 2047;
  attribute bram_slice_begin of ram_reg_1 : label is 18;
  attribute bram_slice_end of ram_reg_1 : label is 31;
  attribute ram_addr_begin of ram_reg_1 : label is 0;
  attribute ram_addr_end of ram_reg_1 : label is 2047;
  attribute ram_offset of ram_reg_1 : label is 0;
  attribute ram_slice_begin of ram_reg_1 : label is 18;
  attribute ram_slice_end of ram_reg_1 : label is 31;
begin
ram_reg_0: unisim.vcomponents.RAMB36E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      EN_ECC_READ => false,
      EN_ECC_WRITE => false,
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      RAM_EXTENSION_A => "NONE",
      RAM_EXTENSION_B => "NONE",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "PERFORMANCE",
      READ_WIDTH_A => 18,
      READ_WIDTH_B => 0,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "READ_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 18,
      WRITE_WIDTH_B => 0
    )
        port map (
      ADDRARDADDR(15) => '1',
      ADDRARDADDR(14 downto 4) => ADDRARDADDR(10 downto 0),
      ADDRARDADDR(3 downto 0) => B"1111",
      ADDRBWRADDR(15 downto 0) => B"1111111111111111",
      CASCADEINA => '1',
      CASCADEINB => '0',
      CASCADEOUTA => NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED,
      CASCADEOUTB => NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED,
      CLKARDCLK => m00_axis_aclk,
      CLKBWRCLK => '0',
      DBITERR => NLW_ram_reg_0_DBITERR_UNCONNECTED,
      DIADI(31 downto 16) => B"0000000000000000",
      DIADI(15 downto 0) => ram_reg_1_0(15 downto 0),
      DIBDI(31 downto 0) => B"11111111111111111111111111111111",
      DIPADIP(3 downto 2) => B"00",
      DIPADIP(1 downto 0) => ram_reg_1_0(17 downto 16),
      DIPBDIP(3 downto 0) => B"1111",
      DOADO(31 downto 16) => NLW_ram_reg_0_DOADO_UNCONNECTED(31 downto 16),
      DOADO(15 downto 0) => dout0_out(15 downto 0),
      DOBDO(31 downto 0) => NLW_ram_reg_0_DOBDO_UNCONNECTED(31 downto 0),
      DOPADOP(3 downto 2) => NLW_ram_reg_0_DOPADOP_UNCONNECTED(3 downto 2),
      DOPADOP(1 downto 0) => dout0_out(17 downto 16),
      DOPBDOP(3 downto 0) => NLW_ram_reg_0_DOPBDOP_UNCONNECTED(3 downto 0),
      ECCPARITY(7 downto 0) => NLW_ram_reg_0_ECCPARITY_UNCONNECTED(7 downto 0),
      ENARDEN => Q(0),
      ENBWREN => '0',
      INJECTDBITERR => NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED,
      INJECTSBITERR => NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED,
      RDADDRECC(8 downto 0) => NLW_ram_reg_0_RDADDRECC_UNCONNECTED(8 downto 0),
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      SBITERR => NLW_ram_reg_0_SBITERR_UNCONNECTED,
      WEA(3) => \ram_reg_0_i_1__0_n_0\,
      WEA(2) => \ram_reg_0_i_1__0_n_0\,
      WEA(1) => \ram_reg_0_i_1__0_n_0\,
      WEA(0) => \ram_reg_0_i_1__0_n_0\,
      WEBWE(7 downto 0) => B"00000000"
    );
\ram_reg_0_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => Q(0),
      I1 => WE_r,
      I2 => square_r,
      O => \ram_reg_0_i_1__0_n_0\
    );
ram_reg_1: unisim.vcomponents.RAMB36E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      EN_ECC_READ => false,
      EN_ECC_WRITE => false,
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      RAM_EXTENSION_A => "NONE",
      RAM_EXTENSION_B => "NONE",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "PERFORMANCE",
      READ_WIDTH_A => 18,
      READ_WIDTH_B => 0,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "READ_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 18,
      WRITE_WIDTH_B => 0
    )
        port map (
      ADDRARDADDR(15) => '1',
      ADDRARDADDR(14 downto 4) => addr(10 downto 0),
      ADDRARDADDR(3 downto 0) => B"1111",
      ADDRBWRADDR(15 downto 0) => B"1111111111111111",
      CASCADEINA => '1',
      CASCADEINB => '0',
      CASCADEOUTA => NLW_ram_reg_1_CASCADEOUTA_UNCONNECTED,
      CASCADEOUTB => NLW_ram_reg_1_CASCADEOUTB_UNCONNECTED,
      CLKARDCLK => m00_axis_aclk,
      CLKBWRCLK => '0',
      DBITERR => NLW_ram_reg_1_DBITERR_UNCONNECTED,
      DIADI(31 downto 14) => B"000000000000000000",
      DIADI(13 downto 0) => ram_reg_1_0(31 downto 18),
      DIBDI(31 downto 0) => B"11111111111111111111111111111111",
      DIPADIP(3 downto 0) => B"0000",
      DIPBDIP(3 downto 0) => B"1111",
      DOADO(31 downto 14) => NLW_ram_reg_1_DOADO_UNCONNECTED(31 downto 14),
      DOADO(13 downto 0) => dout0_out(31 downto 18),
      DOBDO(31 downto 0) => NLW_ram_reg_1_DOBDO_UNCONNECTED(31 downto 0),
      DOPADOP(3 downto 0) => NLW_ram_reg_1_DOPADOP_UNCONNECTED(3 downto 0),
      DOPBDOP(3 downto 0) => NLW_ram_reg_1_DOPBDOP_UNCONNECTED(3 downto 0),
      ECCPARITY(7 downto 0) => NLW_ram_reg_1_ECCPARITY_UNCONNECTED(7 downto 0),
      ENARDEN => Q(0),
      ENBWREN => '0',
      INJECTDBITERR => NLW_ram_reg_1_INJECTDBITERR_UNCONNECTED,
      INJECTSBITERR => NLW_ram_reg_1_INJECTSBITERR_UNCONNECTED,
      RDADDRECC(8 downto 0) => NLW_ram_reg_1_RDADDRECC_UNCONNECTED(8 downto 0),
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      SBITERR => NLW_ram_reg_1_SBITERR_UNCONNECTED,
      WEA(3) => \ram_reg_0_i_1__0_n_0\,
      WEA(2) => \ram_reg_0_i_1__0_n_0\,
      WEA(1) => \ram_reg_0_i_1__0_n_0\,
      WEA(0) => \ram_reg_0_i_1__0_n_0\,
      WEBWE(7 downto 0) => B"00000000"
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_1 is
  port (
    dout0_out : out STD_LOGIC_VECTOR ( 31 downto 0 );
    m00_axis_aclk : in STD_LOGIC;
    en : in STD_LOGIC;
    ADDRARDADDR : in STD_LOGIC_VECTOR ( 10 downto 0 );
    Q : in STD_LOGIC_VECTOR ( 31 downto 0 );
    addr : in STD_LOGIC_VECTOR ( 10 downto 0 );
    WE_r : in STD_LOGIC;
    square_r : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_1 : entity is "rams_sp_rf_rst";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_1;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_1 is
  signal \ram_reg_0_i_1__1_n_0\ : STD_LOGIC;
  signal NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_DBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_SBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_DOADO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 16 );
  signal NLW_ram_reg_0_DOBDO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_ram_reg_0_DOPADOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal NLW_ram_reg_0_DOPBDOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_ram_reg_0_ECCPARITY_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal NLW_ram_reg_0_RDADDRECC_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_ram_reg_1_CASCADEOUTA_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_1_CASCADEOUTB_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_1_DBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_1_INJECTDBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_1_INJECTSBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_1_SBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_1_DOADO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 14 );
  signal NLW_ram_reg_1_DOBDO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_ram_reg_1_DOPADOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_ram_reg_1_DOPBDOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_ram_reg_1_ECCPARITY_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal NLW_ram_reg_1_RDADDRECC_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ : string;
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of ram_reg_0 : label is "p2_d16";
  attribute METHODOLOGY_DRC_VIOS : string;
  attribute METHODOLOGY_DRC_VIOS of ram_reg_0 : label is "{SYNTH-6 {cell *THIS*}}";
  attribute RTL_RAM_BITS : integer;
  attribute RTL_RAM_BITS of ram_reg_0 : label is 61472;
  attribute RTL_RAM_NAME : string;
  attribute RTL_RAM_NAME of ram_reg_0 : label is "ram";
  attribute bram_addr_begin : integer;
  attribute bram_addr_begin of ram_reg_0 : label is 0;
  attribute bram_addr_end : integer;
  attribute bram_addr_end of ram_reg_0 : label is 2047;
  attribute bram_slice_begin : integer;
  attribute bram_slice_begin of ram_reg_0 : label is 0;
  attribute bram_slice_end : integer;
  attribute bram_slice_end of ram_reg_0 : label is 17;
  attribute ram_addr_begin : integer;
  attribute ram_addr_begin of ram_reg_0 : label is 0;
  attribute ram_addr_end : integer;
  attribute ram_addr_end of ram_reg_0 : label is 2047;
  attribute ram_offset : integer;
  attribute ram_offset of ram_reg_0 : label is 0;
  attribute ram_slice_begin : integer;
  attribute ram_slice_begin of ram_reg_0 : label is 0;
  attribute ram_slice_end : integer;
  attribute ram_slice_end of ram_reg_0 : label is 17;
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of ram_reg_1 : label is "p0_d14";
  attribute METHODOLOGY_DRC_VIOS of ram_reg_1 : label is "{SYNTH-6 {cell *THIS*}}";
  attribute RTL_RAM_BITS of ram_reg_1 : label is 61472;
  attribute RTL_RAM_NAME of ram_reg_1 : label is "ram";
  attribute bram_addr_begin of ram_reg_1 : label is 0;
  attribute bram_addr_end of ram_reg_1 : label is 2047;
  attribute bram_slice_begin of ram_reg_1 : label is 18;
  attribute bram_slice_end of ram_reg_1 : label is 31;
  attribute ram_addr_begin of ram_reg_1 : label is 0;
  attribute ram_addr_end of ram_reg_1 : label is 2047;
  attribute ram_offset of ram_reg_1 : label is 0;
  attribute ram_slice_begin of ram_reg_1 : label is 18;
  attribute ram_slice_end of ram_reg_1 : label is 31;
begin
ram_reg_0: unisim.vcomponents.RAMB36E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      EN_ECC_READ => false,
      EN_ECC_WRITE => false,
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      RAM_EXTENSION_A => "NONE",
      RAM_EXTENSION_B => "NONE",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "PERFORMANCE",
      READ_WIDTH_A => 18,
      READ_WIDTH_B => 0,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "READ_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 18,
      WRITE_WIDTH_B => 0
    )
        port map (
      ADDRARDADDR(15) => '1',
      ADDRARDADDR(14 downto 4) => ADDRARDADDR(10 downto 0),
      ADDRARDADDR(3 downto 0) => B"1111",
      ADDRBWRADDR(15 downto 0) => B"1111111111111111",
      CASCADEINA => '1',
      CASCADEINB => '0',
      CASCADEOUTA => NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED,
      CASCADEOUTB => NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED,
      CLKARDCLK => m00_axis_aclk,
      CLKBWRCLK => '0',
      DBITERR => NLW_ram_reg_0_DBITERR_UNCONNECTED,
      DIADI(31 downto 16) => B"0000000000000000",
      DIADI(15 downto 0) => Q(15 downto 0),
      DIBDI(31 downto 0) => B"11111111111111111111111111111111",
      DIPADIP(3 downto 2) => B"00",
      DIPADIP(1 downto 0) => Q(17 downto 16),
      DIPBDIP(3 downto 0) => B"1111",
      DOADO(31 downto 16) => NLW_ram_reg_0_DOADO_UNCONNECTED(31 downto 16),
      DOADO(15 downto 0) => dout0_out(15 downto 0),
      DOBDO(31 downto 0) => NLW_ram_reg_0_DOBDO_UNCONNECTED(31 downto 0),
      DOPADOP(3 downto 2) => NLW_ram_reg_0_DOPADOP_UNCONNECTED(3 downto 2),
      DOPADOP(1 downto 0) => dout0_out(17 downto 16),
      DOPBDOP(3 downto 0) => NLW_ram_reg_0_DOPBDOP_UNCONNECTED(3 downto 0),
      ECCPARITY(7 downto 0) => NLW_ram_reg_0_ECCPARITY_UNCONNECTED(7 downto 0),
      ENARDEN => en,
      ENBWREN => '0',
      INJECTDBITERR => NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED,
      INJECTSBITERR => NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED,
      RDADDRECC(8 downto 0) => NLW_ram_reg_0_RDADDRECC_UNCONNECTED(8 downto 0),
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      SBITERR => NLW_ram_reg_0_SBITERR_UNCONNECTED,
      WEA(3) => \ram_reg_0_i_1__1_n_0\,
      WEA(2) => \ram_reg_0_i_1__1_n_0\,
      WEA(1) => \ram_reg_0_i_1__1_n_0\,
      WEA(0) => \ram_reg_0_i_1__1_n_0\,
      WEBWE(7 downto 0) => B"00000000"
    );
\ram_reg_0_i_1__1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => en,
      I1 => WE_r,
      I2 => square_r,
      O => \ram_reg_0_i_1__1_n_0\
    );
ram_reg_1: unisim.vcomponents.RAMB36E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      EN_ECC_READ => false,
      EN_ECC_WRITE => false,
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      RAM_EXTENSION_A => "NONE",
      RAM_EXTENSION_B => "NONE",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "PERFORMANCE",
      READ_WIDTH_A => 18,
      READ_WIDTH_B => 0,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "READ_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 18,
      WRITE_WIDTH_B => 0
    )
        port map (
      ADDRARDADDR(15) => '1',
      ADDRARDADDR(14 downto 4) => addr(10 downto 0),
      ADDRARDADDR(3 downto 0) => B"1111",
      ADDRBWRADDR(15 downto 0) => B"1111111111111111",
      CASCADEINA => '1',
      CASCADEINB => '0',
      CASCADEOUTA => NLW_ram_reg_1_CASCADEOUTA_UNCONNECTED,
      CASCADEOUTB => NLW_ram_reg_1_CASCADEOUTB_UNCONNECTED,
      CLKARDCLK => m00_axis_aclk,
      CLKBWRCLK => '0',
      DBITERR => NLW_ram_reg_1_DBITERR_UNCONNECTED,
      DIADI(31 downto 14) => B"000000000000000000",
      DIADI(13 downto 0) => Q(31 downto 18),
      DIBDI(31 downto 0) => B"11111111111111111111111111111111",
      DIPADIP(3 downto 0) => B"0000",
      DIPBDIP(3 downto 0) => B"1111",
      DOADO(31 downto 14) => NLW_ram_reg_1_DOADO_UNCONNECTED(31 downto 14),
      DOADO(13 downto 0) => dout0_out(31 downto 18),
      DOBDO(31 downto 0) => NLW_ram_reg_1_DOBDO_UNCONNECTED(31 downto 0),
      DOPADOP(3 downto 0) => NLW_ram_reg_1_DOPADOP_UNCONNECTED(3 downto 0),
      DOPBDOP(3 downto 0) => NLW_ram_reg_1_DOPBDOP_UNCONNECTED(3 downto 0),
      ECCPARITY(7 downto 0) => NLW_ram_reg_1_ECCPARITY_UNCONNECTED(7 downto 0),
      ENARDEN => en,
      ENBWREN => '0',
      INJECTDBITERR => NLW_ram_reg_1_INJECTDBITERR_UNCONNECTED,
      INJECTSBITERR => NLW_ram_reg_1_INJECTSBITERR_UNCONNECTED,
      RDADDRECC(8 downto 0) => NLW_ram_reg_1_RDADDRECC_UNCONNECTED(8 downto 0),
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      SBITERR => NLW_ram_reg_1_SBITERR_UNCONNECTED,
      WEA(3) => \ram_reg_0_i_1__1_n_0\,
      WEA(2) => \ram_reg_0_i_1__1_n_0\,
      WEA(1) => \ram_reg_0_i_1__1_n_0\,
      WEA(0) => \ram_reg_0_i_1__1_n_0\,
      WEBWE(7 downto 0) => B"00000000"
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_10 is
  port (
    ram_reg_1_0 : out STD_LOGIC;
    ram_reg_1_1 : out STD_LOGIC;
    ram_reg_1_2 : out STD_LOGIC;
    ram_reg_1_3 : out STD_LOGIC;
    \s00_axis_tuser[18]\ : out STD_LOGIC;
    ram_reg_1_4 : out STD_LOGIC;
    \s00_axis_tuser[18]_0\ : out STD_LOGIC;
    ram_reg_1_5 : out STD_LOGIC;
    ram_reg_1_6 : out STD_LOGIC;
    ram_reg_1_7 : out STD_LOGIC;
    ram_reg_1_8 : out STD_LOGIC;
    \s00_axis_tuser[18]_1\ : out STD_LOGIC;
    ram_reg_1_9 : out STD_LOGIC;
    ram_reg_1_10 : out STD_LOGIC;
    ram_reg_0_0 : out STD_LOGIC;
    \s00_axis_tuser[18]_2\ : out STD_LOGIC;
    \s00_axis_tuser[18]_3\ : out STD_LOGIC;
    ram_reg_0_1 : out STD_LOGIC;
    ram_reg_0_2 : out STD_LOGIC;
    ram_reg_0_3 : out STD_LOGIC;
    \s00_axis_tuser[18]_4\ : out STD_LOGIC;
    ram_reg_0_4 : out STD_LOGIC;
    \s00_axis_tuser[18]_5\ : out STD_LOGIC;
    ram_reg_0_5 : out STD_LOGIC;
    ram_reg_0_6 : out STD_LOGIC;
    ram_reg_0_7 : out STD_LOGIC;
    ram_reg_0_8 : out STD_LOGIC;
    \s00_axis_tuser[18]_6\ : out STD_LOGIC;
    ram_reg_0_9 : out STD_LOGIC;
    ram_reg_0_10 : out STD_LOGIC;
    ram_reg_0_11 : out STD_LOGIC;
    \s00_axis_tuser[18]_7\ : out STD_LOGIC;
    dout0_out : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s00_axis_tuser : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \m00_axis_tdata_r_reg[31]\ : in STD_LOGIC_VECTOR ( 31 downto 0 );
    \m00_axis_tdata_r_reg[31]_0\ : in STD_LOGIC_VECTOR ( 31 downto 0 );
    \m00_axis_tdata_r_reg[27]\ : in STD_LOGIC;
    \m00_axis_tdata_r_reg[25]\ : in STD_LOGIC;
    \m00_axis_tdata_r_reg[20]\ : in STD_LOGIC;
    \m00_axis_tdata_r_reg[16]\ : in STD_LOGIC;
    \m00_axis_tdata_r_reg[15]\ : in STD_LOGIC;
    \m00_axis_tdata_r_reg[11]\ : in STD_LOGIC;
    \m00_axis_tdata_r_reg[9]\ : in STD_LOGIC;
    \m00_axis_tdata_r_reg[4]\ : in STD_LOGIC;
    \m00_axis_tdata_r_reg[0]\ : in STD_LOGIC;
    m00_axis_aclk : in STD_LOGIC;
    en : in STD_LOGIC;
    ADDRARDADDR : in STD_LOGIC_VECTOR ( 10 downto 0 );
    Q : in STD_LOGIC_VECTOR ( 31 downto 0 );
    addr : in STD_LOGIC_VECTOR ( 10 downto 0 );
    WE_r : in STD_LOGIC;
    square_r : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_10 : entity is "rams_sp_rf_rst";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_10;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_10 is
  signal DOUTB_w : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \m00_axis_tdata_r[0]_i_5_n_0\ : STD_LOGIC;
  signal \m00_axis_tdata_r[11]_i_5_n_0\ : STD_LOGIC;
  signal \m00_axis_tdata_r[15]_i_5_n_0\ : STD_LOGIC;
  signal \m00_axis_tdata_r[16]_i_5_n_0\ : STD_LOGIC;
  signal \m00_axis_tdata_r[20]_i_5_n_0\ : STD_LOGIC;
  signal \m00_axis_tdata_r[25]_i_5_n_0\ : STD_LOGIC;
  signal \m00_axis_tdata_r[27]_i_5_n_0\ : STD_LOGIC;
  signal \m00_axis_tdata_r[4]_i_5_n_0\ : STD_LOGIC;
  signal \m00_axis_tdata_r[9]_i_5_n_0\ : STD_LOGIC;
  signal \ram_reg_0_i_1__10_n_0\ : STD_LOGIC;
  signal NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_DBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_SBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_DOADO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 16 );
  signal NLW_ram_reg_0_DOBDO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_ram_reg_0_DOPADOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal NLW_ram_reg_0_DOPBDOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_ram_reg_0_ECCPARITY_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal NLW_ram_reg_0_RDADDRECC_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_ram_reg_1_CASCADEOUTA_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_1_CASCADEOUTB_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_1_DBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_1_INJECTDBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_1_INJECTSBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_1_SBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_1_DOADO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 14 );
  signal NLW_ram_reg_1_DOBDO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_ram_reg_1_DOPADOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_ram_reg_1_DOPBDOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_ram_reg_1_ECCPARITY_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal NLW_ram_reg_1_RDADDRECC_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ : string;
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of ram_reg_0 : label is "p2_d16";
  attribute METHODOLOGY_DRC_VIOS : string;
  attribute METHODOLOGY_DRC_VIOS of ram_reg_0 : label is "{SYNTH-6 {cell *THIS*}}";
  attribute RTL_RAM_BITS : integer;
  attribute RTL_RAM_BITS of ram_reg_0 : label is 61472;
  attribute RTL_RAM_NAME : string;
  attribute RTL_RAM_NAME of ram_reg_0 : label is "ram";
  attribute bram_addr_begin : integer;
  attribute bram_addr_begin of ram_reg_0 : label is 0;
  attribute bram_addr_end : integer;
  attribute bram_addr_end of ram_reg_0 : label is 2047;
  attribute bram_slice_begin : integer;
  attribute bram_slice_begin of ram_reg_0 : label is 0;
  attribute bram_slice_end : integer;
  attribute bram_slice_end of ram_reg_0 : label is 17;
  attribute ram_addr_begin : integer;
  attribute ram_addr_begin of ram_reg_0 : label is 0;
  attribute ram_addr_end : integer;
  attribute ram_addr_end of ram_reg_0 : label is 2047;
  attribute ram_offset : integer;
  attribute ram_offset of ram_reg_0 : label is 0;
  attribute ram_slice_begin : integer;
  attribute ram_slice_begin of ram_reg_0 : label is 0;
  attribute ram_slice_end : integer;
  attribute ram_slice_end of ram_reg_0 : label is 17;
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of ram_reg_1 : label is "p0_d14";
  attribute METHODOLOGY_DRC_VIOS of ram_reg_1 : label is "{SYNTH-6 {cell *THIS*}}";
  attribute RTL_RAM_BITS of ram_reg_1 : label is 61472;
  attribute RTL_RAM_NAME of ram_reg_1 : label is "ram";
  attribute bram_addr_begin of ram_reg_1 : label is 0;
  attribute bram_addr_end of ram_reg_1 : label is 2047;
  attribute bram_slice_begin of ram_reg_1 : label is 18;
  attribute bram_slice_end of ram_reg_1 : label is 31;
  attribute ram_addr_begin of ram_reg_1 : label is 0;
  attribute ram_addr_end of ram_reg_1 : label is 2047;
  attribute ram_offset of ram_reg_1 : label is 0;
  attribute ram_slice_begin of ram_reg_1 : label is 18;
  attribute ram_slice_end of ram_reg_1 : label is 31;
begin
\m00_axis_tdata_r[0]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => DOUTB_w(0),
      I1 => dout0_out(0),
      I2 => s00_axis_tuser(1),
      I3 => \m00_axis_tdata_r_reg[31]\(0),
      I4 => s00_axis_tuser(0),
      I5 => \m00_axis_tdata_r_reg[31]_0\(0),
      O => \m00_axis_tdata_r[0]_i_5_n_0\
    );
\m00_axis_tdata_r[10]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => DOUTB_w(10),
      I1 => dout0_out(10),
      I2 => s00_axis_tuser(1),
      I3 => \m00_axis_tdata_r_reg[31]\(10),
      I4 => s00_axis_tuser(0),
      I5 => \m00_axis_tdata_r_reg[31]_0\(10),
      O => ram_reg_0_4
    );
\m00_axis_tdata_r[11]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => DOUTB_w(11),
      I1 => dout0_out(11),
      I2 => s00_axis_tuser(1),
      I3 => \m00_axis_tdata_r_reg[31]\(11),
      I4 => s00_axis_tuser(0),
      I5 => \m00_axis_tdata_r_reg[31]_0\(11),
      O => \m00_axis_tdata_r[11]_i_5_n_0\
    );
\m00_axis_tdata_r[12]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => DOUTB_w(12),
      I1 => dout0_out(12),
      I2 => s00_axis_tuser(1),
      I3 => \m00_axis_tdata_r_reg[31]\(12),
      I4 => s00_axis_tuser(0),
      I5 => \m00_axis_tdata_r_reg[31]_0\(12),
      O => ram_reg_0_3
    );
\m00_axis_tdata_r[13]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => DOUTB_w(13),
      I1 => dout0_out(13),
      I2 => s00_axis_tuser(1),
      I3 => \m00_axis_tdata_r_reg[31]\(13),
      I4 => s00_axis_tuser(0),
      I5 => \m00_axis_tdata_r_reg[31]_0\(13),
      O => ram_reg_0_2
    );
\m00_axis_tdata_r[14]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => DOUTB_w(14),
      I1 => dout0_out(14),
      I2 => s00_axis_tuser(1),
      I3 => \m00_axis_tdata_r_reg[31]\(14),
      I4 => s00_axis_tuser(0),
      I5 => \m00_axis_tdata_r_reg[31]_0\(14),
      O => ram_reg_0_1
    );
\m00_axis_tdata_r[15]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => DOUTB_w(15),
      I1 => dout0_out(15),
      I2 => s00_axis_tuser(1),
      I3 => \m00_axis_tdata_r_reg[31]\(15),
      I4 => s00_axis_tuser(0),
      I5 => \m00_axis_tdata_r_reg[31]_0\(15),
      O => \m00_axis_tdata_r[15]_i_5_n_0\
    );
\m00_axis_tdata_r[16]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => DOUTB_w(16),
      I1 => dout0_out(16),
      I2 => s00_axis_tuser(1),
      I3 => \m00_axis_tdata_r_reg[31]\(16),
      I4 => s00_axis_tuser(0),
      I5 => \m00_axis_tdata_r_reg[31]_0\(16),
      O => \m00_axis_tdata_r[16]_i_5_n_0\
    );
\m00_axis_tdata_r[17]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => DOUTB_w(17),
      I1 => dout0_out(17),
      I2 => s00_axis_tuser(1),
      I3 => \m00_axis_tdata_r_reg[31]\(17),
      I4 => s00_axis_tuser(0),
      I5 => \m00_axis_tdata_r_reg[31]_0\(17),
      O => ram_reg_0_0
    );
\m00_axis_tdata_r[18]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => DOUTB_w(18),
      I1 => dout0_out(18),
      I2 => s00_axis_tuser(1),
      I3 => \m00_axis_tdata_r_reg[31]\(18),
      I4 => s00_axis_tuser(0),
      I5 => \m00_axis_tdata_r_reg[31]_0\(18),
      O => ram_reg_1_10
    );
\m00_axis_tdata_r[19]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => DOUTB_w(19),
      I1 => dout0_out(19),
      I2 => s00_axis_tuser(1),
      I3 => \m00_axis_tdata_r_reg[31]\(19),
      I4 => s00_axis_tuser(0),
      I5 => \m00_axis_tdata_r_reg[31]_0\(19),
      O => ram_reg_1_9
    );
\m00_axis_tdata_r[1]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => DOUTB_w(1),
      I1 => dout0_out(1),
      I2 => s00_axis_tuser(1),
      I3 => \m00_axis_tdata_r_reg[31]\(1),
      I4 => s00_axis_tuser(0),
      I5 => \m00_axis_tdata_r_reg[31]_0\(1),
      O => ram_reg_0_11
    );
\m00_axis_tdata_r[20]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => DOUTB_w(20),
      I1 => dout0_out(20),
      I2 => s00_axis_tuser(1),
      I3 => \m00_axis_tdata_r_reg[31]\(20),
      I4 => s00_axis_tuser(0),
      I5 => \m00_axis_tdata_r_reg[31]_0\(20),
      O => \m00_axis_tdata_r[20]_i_5_n_0\
    );
\m00_axis_tdata_r[21]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => DOUTB_w(21),
      I1 => dout0_out(21),
      I2 => s00_axis_tuser(1),
      I3 => \m00_axis_tdata_r_reg[31]\(21),
      I4 => s00_axis_tuser(0),
      I5 => \m00_axis_tdata_r_reg[31]_0\(21),
      O => ram_reg_1_8
    );
\m00_axis_tdata_r[22]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => DOUTB_w(22),
      I1 => dout0_out(22),
      I2 => s00_axis_tuser(1),
      I3 => \m00_axis_tdata_r_reg[31]\(22),
      I4 => s00_axis_tuser(0),
      I5 => \m00_axis_tdata_r_reg[31]_0\(22),
      O => ram_reg_1_7
    );
\m00_axis_tdata_r[23]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => DOUTB_w(23),
      I1 => dout0_out(23),
      I2 => s00_axis_tuser(1),
      I3 => \m00_axis_tdata_r_reg[31]\(23),
      I4 => s00_axis_tuser(0),
      I5 => \m00_axis_tdata_r_reg[31]_0\(23),
      O => ram_reg_1_6
    );
\m00_axis_tdata_r[24]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => DOUTB_w(24),
      I1 => dout0_out(24),
      I2 => s00_axis_tuser(1),
      I3 => \m00_axis_tdata_r_reg[31]\(24),
      I4 => s00_axis_tuser(0),
      I5 => \m00_axis_tdata_r_reg[31]_0\(24),
      O => ram_reg_1_5
    );
\m00_axis_tdata_r[25]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => DOUTB_w(25),
      I1 => dout0_out(25),
      I2 => s00_axis_tuser(1),
      I3 => \m00_axis_tdata_r_reg[31]\(25),
      I4 => s00_axis_tuser(0),
      I5 => \m00_axis_tdata_r_reg[31]_0\(25),
      O => \m00_axis_tdata_r[25]_i_5_n_0\
    );
\m00_axis_tdata_r[26]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => DOUTB_w(26),
      I1 => dout0_out(26),
      I2 => s00_axis_tuser(1),
      I3 => \m00_axis_tdata_r_reg[31]\(26),
      I4 => s00_axis_tuser(0),
      I5 => \m00_axis_tdata_r_reg[31]_0\(26),
      O => ram_reg_1_4
    );
\m00_axis_tdata_r[27]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => DOUTB_w(27),
      I1 => dout0_out(27),
      I2 => s00_axis_tuser(1),
      I3 => \m00_axis_tdata_r_reg[31]\(27),
      I4 => s00_axis_tuser(0),
      I5 => \m00_axis_tdata_r_reg[31]_0\(27),
      O => \m00_axis_tdata_r[27]_i_5_n_0\
    );
\m00_axis_tdata_r[28]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => DOUTB_w(28),
      I1 => dout0_out(28),
      I2 => s00_axis_tuser(1),
      I3 => \m00_axis_tdata_r_reg[31]\(28),
      I4 => s00_axis_tuser(0),
      I5 => \m00_axis_tdata_r_reg[31]_0\(28),
      O => ram_reg_1_3
    );
\m00_axis_tdata_r[29]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => DOUTB_w(29),
      I1 => dout0_out(29),
      I2 => s00_axis_tuser(1),
      I3 => \m00_axis_tdata_r_reg[31]\(29),
      I4 => s00_axis_tuser(0),
      I5 => \m00_axis_tdata_r_reg[31]_0\(29),
      O => ram_reg_1_2
    );
\m00_axis_tdata_r[2]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => DOUTB_w(2),
      I1 => dout0_out(2),
      I2 => s00_axis_tuser(1),
      I3 => \m00_axis_tdata_r_reg[31]\(2),
      I4 => s00_axis_tuser(0),
      I5 => \m00_axis_tdata_r_reg[31]_0\(2),
      O => ram_reg_0_10
    );
\m00_axis_tdata_r[30]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => DOUTB_w(30),
      I1 => dout0_out(30),
      I2 => s00_axis_tuser(1),
      I3 => \m00_axis_tdata_r_reg[31]\(30),
      I4 => s00_axis_tuser(0),
      I5 => \m00_axis_tdata_r_reg[31]_0\(30),
      O => ram_reg_1_1
    );
\m00_axis_tdata_r[31]_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => DOUTB_w(31),
      I1 => dout0_out(31),
      I2 => s00_axis_tuser(1),
      I3 => \m00_axis_tdata_r_reg[31]\(31),
      I4 => s00_axis_tuser(0),
      I5 => \m00_axis_tdata_r_reg[31]_0\(31),
      O => ram_reg_1_0
    );
\m00_axis_tdata_r[3]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => DOUTB_w(3),
      I1 => dout0_out(3),
      I2 => s00_axis_tuser(1),
      I3 => \m00_axis_tdata_r_reg[31]\(3),
      I4 => s00_axis_tuser(0),
      I5 => \m00_axis_tdata_r_reg[31]_0\(3),
      O => ram_reg_0_9
    );
\m00_axis_tdata_r[4]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => DOUTB_w(4),
      I1 => dout0_out(4),
      I2 => s00_axis_tuser(1),
      I3 => \m00_axis_tdata_r_reg[31]\(4),
      I4 => s00_axis_tuser(0),
      I5 => \m00_axis_tdata_r_reg[31]_0\(4),
      O => \m00_axis_tdata_r[4]_i_5_n_0\
    );
\m00_axis_tdata_r[5]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => DOUTB_w(5),
      I1 => dout0_out(5),
      I2 => s00_axis_tuser(1),
      I3 => \m00_axis_tdata_r_reg[31]\(5),
      I4 => s00_axis_tuser(0),
      I5 => \m00_axis_tdata_r_reg[31]_0\(5),
      O => ram_reg_0_8
    );
\m00_axis_tdata_r[6]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => DOUTB_w(6),
      I1 => dout0_out(6),
      I2 => s00_axis_tuser(1),
      I3 => \m00_axis_tdata_r_reg[31]\(6),
      I4 => s00_axis_tuser(0),
      I5 => \m00_axis_tdata_r_reg[31]_0\(6),
      O => ram_reg_0_7
    );
\m00_axis_tdata_r[7]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => DOUTB_w(7),
      I1 => dout0_out(7),
      I2 => s00_axis_tuser(1),
      I3 => \m00_axis_tdata_r_reg[31]\(7),
      I4 => s00_axis_tuser(0),
      I5 => \m00_axis_tdata_r_reg[31]_0\(7),
      O => ram_reg_0_6
    );
\m00_axis_tdata_r[8]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => DOUTB_w(8),
      I1 => dout0_out(8),
      I2 => s00_axis_tuser(1),
      I3 => \m00_axis_tdata_r_reg[31]\(8),
      I4 => s00_axis_tuser(0),
      I5 => \m00_axis_tdata_r_reg[31]_0\(8),
      O => ram_reg_0_5
    );
\m00_axis_tdata_r[9]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => DOUTB_w(9),
      I1 => dout0_out(9),
      I2 => s00_axis_tuser(1),
      I3 => \m00_axis_tdata_r_reg[31]\(9),
      I4 => s00_axis_tuser(0),
      I5 => \m00_axis_tdata_r_reg[31]_0\(9),
      O => \m00_axis_tdata_r[9]_i_5_n_0\
    );
\m00_axis_tdata_r_reg[0]_i_4\: unisim.vcomponents.MUXF7
     port map (
      I0 => \m00_axis_tdata_r[0]_i_5_n_0\,
      I1 => \m00_axis_tdata_r_reg[0]\,
      O => \s00_axis_tuser[18]_7\,
      S => s00_axis_tuser(2)
    );
\m00_axis_tdata_r_reg[11]_i_4\: unisim.vcomponents.MUXF7
     port map (
      I0 => \m00_axis_tdata_r[11]_i_5_n_0\,
      I1 => \m00_axis_tdata_r_reg[11]\,
      O => \s00_axis_tuser[18]_4\,
      S => s00_axis_tuser(2)
    );
\m00_axis_tdata_r_reg[15]_i_4\: unisim.vcomponents.MUXF7
     port map (
      I0 => \m00_axis_tdata_r[15]_i_5_n_0\,
      I1 => \m00_axis_tdata_r_reg[15]\,
      O => \s00_axis_tuser[18]_3\,
      S => s00_axis_tuser(2)
    );
\m00_axis_tdata_r_reg[16]_i_4\: unisim.vcomponents.MUXF7
     port map (
      I0 => \m00_axis_tdata_r[16]_i_5_n_0\,
      I1 => \m00_axis_tdata_r_reg[16]\,
      O => \s00_axis_tuser[18]_2\,
      S => s00_axis_tuser(2)
    );
\m00_axis_tdata_r_reg[20]_i_4\: unisim.vcomponents.MUXF7
     port map (
      I0 => \m00_axis_tdata_r[20]_i_5_n_0\,
      I1 => \m00_axis_tdata_r_reg[20]\,
      O => \s00_axis_tuser[18]_1\,
      S => s00_axis_tuser(2)
    );
\m00_axis_tdata_r_reg[25]_i_4\: unisim.vcomponents.MUXF7
     port map (
      I0 => \m00_axis_tdata_r[25]_i_5_n_0\,
      I1 => \m00_axis_tdata_r_reg[25]\,
      O => \s00_axis_tuser[18]_0\,
      S => s00_axis_tuser(2)
    );
\m00_axis_tdata_r_reg[27]_i_4\: unisim.vcomponents.MUXF7
     port map (
      I0 => \m00_axis_tdata_r[27]_i_5_n_0\,
      I1 => \m00_axis_tdata_r_reg[27]\,
      O => \s00_axis_tuser[18]\,
      S => s00_axis_tuser(2)
    );
\m00_axis_tdata_r_reg[4]_i_4\: unisim.vcomponents.MUXF7
     port map (
      I0 => \m00_axis_tdata_r[4]_i_5_n_0\,
      I1 => \m00_axis_tdata_r_reg[4]\,
      O => \s00_axis_tuser[18]_6\,
      S => s00_axis_tuser(2)
    );
\m00_axis_tdata_r_reg[9]_i_4\: unisim.vcomponents.MUXF7
     port map (
      I0 => \m00_axis_tdata_r[9]_i_5_n_0\,
      I1 => \m00_axis_tdata_r_reg[9]\,
      O => \s00_axis_tuser[18]_5\,
      S => s00_axis_tuser(2)
    );
ram_reg_0: unisim.vcomponents.RAMB36E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      EN_ECC_READ => false,
      EN_ECC_WRITE => false,
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      RAM_EXTENSION_A => "NONE",
      RAM_EXTENSION_B => "NONE",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "PERFORMANCE",
      READ_WIDTH_A => 18,
      READ_WIDTH_B => 0,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "READ_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 18,
      WRITE_WIDTH_B => 0
    )
        port map (
      ADDRARDADDR(15) => '1',
      ADDRARDADDR(14 downto 4) => ADDRARDADDR(10 downto 0),
      ADDRARDADDR(3 downto 0) => B"1111",
      ADDRBWRADDR(15 downto 0) => B"1111111111111111",
      CASCADEINA => '1',
      CASCADEINB => '0',
      CASCADEOUTA => NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED,
      CASCADEOUTB => NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED,
      CLKARDCLK => m00_axis_aclk,
      CLKBWRCLK => '0',
      DBITERR => NLW_ram_reg_0_DBITERR_UNCONNECTED,
      DIADI(31 downto 16) => B"0000000000000000",
      DIADI(15 downto 0) => Q(15 downto 0),
      DIBDI(31 downto 0) => B"11111111111111111111111111111111",
      DIPADIP(3 downto 2) => B"00",
      DIPADIP(1 downto 0) => Q(17 downto 16),
      DIPBDIP(3 downto 0) => B"1111",
      DOADO(31 downto 16) => NLW_ram_reg_0_DOADO_UNCONNECTED(31 downto 16),
      DOADO(15 downto 0) => DOUTB_w(15 downto 0),
      DOBDO(31 downto 0) => NLW_ram_reg_0_DOBDO_UNCONNECTED(31 downto 0),
      DOPADOP(3 downto 2) => NLW_ram_reg_0_DOPADOP_UNCONNECTED(3 downto 2),
      DOPADOP(1 downto 0) => DOUTB_w(17 downto 16),
      DOPBDOP(3 downto 0) => NLW_ram_reg_0_DOPBDOP_UNCONNECTED(3 downto 0),
      ECCPARITY(7 downto 0) => NLW_ram_reg_0_ECCPARITY_UNCONNECTED(7 downto 0),
      ENARDEN => en,
      ENBWREN => '0',
      INJECTDBITERR => NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED,
      INJECTSBITERR => NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED,
      RDADDRECC(8 downto 0) => NLW_ram_reg_0_RDADDRECC_UNCONNECTED(8 downto 0),
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      SBITERR => NLW_ram_reg_0_SBITERR_UNCONNECTED,
      WEA(3) => \ram_reg_0_i_1__10_n_0\,
      WEA(2) => \ram_reg_0_i_1__10_n_0\,
      WEA(1) => \ram_reg_0_i_1__10_n_0\,
      WEA(0) => \ram_reg_0_i_1__10_n_0\,
      WEBWE(7 downto 0) => B"00000000"
    );
\ram_reg_0_i_1__10\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => en,
      I1 => WE_r,
      I2 => square_r,
      O => \ram_reg_0_i_1__10_n_0\
    );
ram_reg_1: unisim.vcomponents.RAMB36E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      EN_ECC_READ => false,
      EN_ECC_WRITE => false,
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      RAM_EXTENSION_A => "NONE",
      RAM_EXTENSION_B => "NONE",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "PERFORMANCE",
      READ_WIDTH_A => 18,
      READ_WIDTH_B => 0,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "READ_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 18,
      WRITE_WIDTH_B => 0
    )
        port map (
      ADDRARDADDR(15) => '1',
      ADDRARDADDR(14 downto 4) => addr(10 downto 0),
      ADDRARDADDR(3 downto 0) => B"1111",
      ADDRBWRADDR(15 downto 0) => B"1111111111111111",
      CASCADEINA => '1',
      CASCADEINB => '0',
      CASCADEOUTA => NLW_ram_reg_1_CASCADEOUTA_UNCONNECTED,
      CASCADEOUTB => NLW_ram_reg_1_CASCADEOUTB_UNCONNECTED,
      CLKARDCLK => m00_axis_aclk,
      CLKBWRCLK => '0',
      DBITERR => NLW_ram_reg_1_DBITERR_UNCONNECTED,
      DIADI(31 downto 14) => B"000000000000000000",
      DIADI(13 downto 0) => Q(31 downto 18),
      DIBDI(31 downto 0) => B"11111111111111111111111111111111",
      DIPADIP(3 downto 0) => B"0000",
      DIPBDIP(3 downto 0) => B"1111",
      DOADO(31 downto 14) => NLW_ram_reg_1_DOADO_UNCONNECTED(31 downto 14),
      DOADO(13 downto 0) => DOUTB_w(31 downto 18),
      DOBDO(31 downto 0) => NLW_ram_reg_1_DOBDO_UNCONNECTED(31 downto 0),
      DOPADOP(3 downto 0) => NLW_ram_reg_1_DOPADOP_UNCONNECTED(3 downto 0),
      DOPBDOP(3 downto 0) => NLW_ram_reg_1_DOPBDOP_UNCONNECTED(3 downto 0),
      ECCPARITY(7 downto 0) => NLW_ram_reg_1_ECCPARITY_UNCONNECTED(7 downto 0),
      ENARDEN => en,
      ENBWREN => '0',
      INJECTDBITERR => NLW_ram_reg_1_INJECTDBITERR_UNCONNECTED,
      INJECTSBITERR => NLW_ram_reg_1_INJECTSBITERR_UNCONNECTED,
      RDADDRECC(8 downto 0) => NLW_ram_reg_1_RDADDRECC_UNCONNECTED(8 downto 0),
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      SBITERR => NLW_ram_reg_1_SBITERR_UNCONNECTED,
      WEA(3) => \ram_reg_0_i_1__10_n_0\,
      WEA(2) => \ram_reg_0_i_1__10_n_0\,
      WEA(1) => \ram_reg_0_i_1__10_n_0\,
      WEA(0) => \ram_reg_0_i_1__10_n_0\,
      WEBWE(7 downto 0) => B"00000000"
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_11 is
  port (
    dout0_out : out STD_LOGIC_VECTOR ( 31 downto 0 );
    m00_axis_aclk : in STD_LOGIC;
    en : in STD_LOGIC;
    ADDRARDADDR : in STD_LOGIC_VECTOR ( 10 downto 0 );
    Q : in STD_LOGIC_VECTOR ( 31 downto 0 );
    addr : in STD_LOGIC_VECTOR ( 10 downto 0 );
    WE_r : in STD_LOGIC;
    square_r : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_11 : entity is "rams_sp_rf_rst";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_11;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_11 is
  signal \ram_reg_0_i_1__11_n_0\ : STD_LOGIC;
  signal NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_DBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_SBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_DOADO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 16 );
  signal NLW_ram_reg_0_DOBDO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_ram_reg_0_DOPADOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal NLW_ram_reg_0_DOPBDOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_ram_reg_0_ECCPARITY_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal NLW_ram_reg_0_RDADDRECC_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_ram_reg_1_CASCADEOUTA_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_1_CASCADEOUTB_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_1_DBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_1_INJECTDBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_1_INJECTSBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_1_SBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_1_DOADO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 14 );
  signal NLW_ram_reg_1_DOBDO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_ram_reg_1_DOPADOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_ram_reg_1_DOPBDOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_ram_reg_1_ECCPARITY_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal NLW_ram_reg_1_RDADDRECC_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ : string;
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of ram_reg_0 : label is "p2_d16";
  attribute METHODOLOGY_DRC_VIOS : string;
  attribute METHODOLOGY_DRC_VIOS of ram_reg_0 : label is "{SYNTH-6 {cell *THIS*}}";
  attribute RTL_RAM_BITS : integer;
  attribute RTL_RAM_BITS of ram_reg_0 : label is 61472;
  attribute RTL_RAM_NAME : string;
  attribute RTL_RAM_NAME of ram_reg_0 : label is "ram";
  attribute bram_addr_begin : integer;
  attribute bram_addr_begin of ram_reg_0 : label is 0;
  attribute bram_addr_end : integer;
  attribute bram_addr_end of ram_reg_0 : label is 2047;
  attribute bram_slice_begin : integer;
  attribute bram_slice_begin of ram_reg_0 : label is 0;
  attribute bram_slice_end : integer;
  attribute bram_slice_end of ram_reg_0 : label is 17;
  attribute ram_addr_begin : integer;
  attribute ram_addr_begin of ram_reg_0 : label is 0;
  attribute ram_addr_end : integer;
  attribute ram_addr_end of ram_reg_0 : label is 2047;
  attribute ram_offset : integer;
  attribute ram_offset of ram_reg_0 : label is 0;
  attribute ram_slice_begin : integer;
  attribute ram_slice_begin of ram_reg_0 : label is 0;
  attribute ram_slice_end : integer;
  attribute ram_slice_end of ram_reg_0 : label is 17;
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of ram_reg_1 : label is "p0_d14";
  attribute METHODOLOGY_DRC_VIOS of ram_reg_1 : label is "{SYNTH-6 {cell *THIS*}}";
  attribute RTL_RAM_BITS of ram_reg_1 : label is 61472;
  attribute RTL_RAM_NAME of ram_reg_1 : label is "ram";
  attribute bram_addr_begin of ram_reg_1 : label is 0;
  attribute bram_addr_end of ram_reg_1 : label is 2047;
  attribute bram_slice_begin of ram_reg_1 : label is 18;
  attribute bram_slice_end of ram_reg_1 : label is 31;
  attribute ram_addr_begin of ram_reg_1 : label is 0;
  attribute ram_addr_end of ram_reg_1 : label is 2047;
  attribute ram_offset of ram_reg_1 : label is 0;
  attribute ram_slice_begin of ram_reg_1 : label is 18;
  attribute ram_slice_end of ram_reg_1 : label is 31;
begin
ram_reg_0: unisim.vcomponents.RAMB36E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      EN_ECC_READ => false,
      EN_ECC_WRITE => false,
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      RAM_EXTENSION_A => "NONE",
      RAM_EXTENSION_B => "NONE",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "PERFORMANCE",
      READ_WIDTH_A => 18,
      READ_WIDTH_B => 0,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "READ_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 18,
      WRITE_WIDTH_B => 0
    )
        port map (
      ADDRARDADDR(15) => '1',
      ADDRARDADDR(14 downto 4) => ADDRARDADDR(10 downto 0),
      ADDRARDADDR(3 downto 0) => B"1111",
      ADDRBWRADDR(15 downto 0) => B"1111111111111111",
      CASCADEINA => '1',
      CASCADEINB => '0',
      CASCADEOUTA => NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED,
      CASCADEOUTB => NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED,
      CLKARDCLK => m00_axis_aclk,
      CLKBWRCLK => '0',
      DBITERR => NLW_ram_reg_0_DBITERR_UNCONNECTED,
      DIADI(31 downto 16) => B"0000000000000000",
      DIADI(15 downto 0) => Q(15 downto 0),
      DIBDI(31 downto 0) => B"11111111111111111111111111111111",
      DIPADIP(3 downto 2) => B"00",
      DIPADIP(1 downto 0) => Q(17 downto 16),
      DIPBDIP(3 downto 0) => B"1111",
      DOADO(31 downto 16) => NLW_ram_reg_0_DOADO_UNCONNECTED(31 downto 16),
      DOADO(15 downto 0) => dout0_out(15 downto 0),
      DOBDO(31 downto 0) => NLW_ram_reg_0_DOBDO_UNCONNECTED(31 downto 0),
      DOPADOP(3 downto 2) => NLW_ram_reg_0_DOPADOP_UNCONNECTED(3 downto 2),
      DOPADOP(1 downto 0) => dout0_out(17 downto 16),
      DOPBDOP(3 downto 0) => NLW_ram_reg_0_DOPBDOP_UNCONNECTED(3 downto 0),
      ECCPARITY(7 downto 0) => NLW_ram_reg_0_ECCPARITY_UNCONNECTED(7 downto 0),
      ENARDEN => en,
      ENBWREN => '0',
      INJECTDBITERR => NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED,
      INJECTSBITERR => NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED,
      RDADDRECC(8 downto 0) => NLW_ram_reg_0_RDADDRECC_UNCONNECTED(8 downto 0),
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      SBITERR => NLW_ram_reg_0_SBITERR_UNCONNECTED,
      WEA(3) => \ram_reg_0_i_1__11_n_0\,
      WEA(2) => \ram_reg_0_i_1__11_n_0\,
      WEA(1) => \ram_reg_0_i_1__11_n_0\,
      WEA(0) => \ram_reg_0_i_1__11_n_0\,
      WEBWE(7 downto 0) => B"00000000"
    );
\ram_reg_0_i_1__11\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => en,
      I1 => WE_r,
      I2 => square_r,
      O => \ram_reg_0_i_1__11_n_0\
    );
ram_reg_1: unisim.vcomponents.RAMB36E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      EN_ECC_READ => false,
      EN_ECC_WRITE => false,
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      RAM_EXTENSION_A => "NONE",
      RAM_EXTENSION_B => "NONE",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "PERFORMANCE",
      READ_WIDTH_A => 18,
      READ_WIDTH_B => 0,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "READ_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 18,
      WRITE_WIDTH_B => 0
    )
        port map (
      ADDRARDADDR(15) => '1',
      ADDRARDADDR(14 downto 4) => addr(10 downto 0),
      ADDRARDADDR(3 downto 0) => B"1111",
      ADDRBWRADDR(15 downto 0) => B"1111111111111111",
      CASCADEINA => '1',
      CASCADEINB => '0',
      CASCADEOUTA => NLW_ram_reg_1_CASCADEOUTA_UNCONNECTED,
      CASCADEOUTB => NLW_ram_reg_1_CASCADEOUTB_UNCONNECTED,
      CLKARDCLK => m00_axis_aclk,
      CLKBWRCLK => '0',
      DBITERR => NLW_ram_reg_1_DBITERR_UNCONNECTED,
      DIADI(31 downto 14) => B"000000000000000000",
      DIADI(13 downto 0) => Q(31 downto 18),
      DIBDI(31 downto 0) => B"11111111111111111111111111111111",
      DIPADIP(3 downto 0) => B"0000",
      DIPBDIP(3 downto 0) => B"1111",
      DOADO(31 downto 14) => NLW_ram_reg_1_DOADO_UNCONNECTED(31 downto 14),
      DOADO(13 downto 0) => dout0_out(31 downto 18),
      DOBDO(31 downto 0) => NLW_ram_reg_1_DOBDO_UNCONNECTED(31 downto 0),
      DOPADOP(3 downto 0) => NLW_ram_reg_1_DOPADOP_UNCONNECTED(3 downto 0),
      DOPBDOP(3 downto 0) => NLW_ram_reg_1_DOPBDOP_UNCONNECTED(3 downto 0),
      ECCPARITY(7 downto 0) => NLW_ram_reg_1_ECCPARITY_UNCONNECTED(7 downto 0),
      ENARDEN => en,
      ENBWREN => '0',
      INJECTDBITERR => NLW_ram_reg_1_INJECTDBITERR_UNCONNECTED,
      INJECTSBITERR => NLW_ram_reg_1_INJECTSBITERR_UNCONNECTED,
      RDADDRECC(8 downto 0) => NLW_ram_reg_1_RDADDRECC_UNCONNECTED(8 downto 0),
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      SBITERR => NLW_ram_reg_1_SBITERR_UNCONNECTED,
      WEA(3) => \ram_reg_0_i_1__11_n_0\,
      WEA(2) => \ram_reg_0_i_1__11_n_0\,
      WEA(1) => \ram_reg_0_i_1__11_n_0\,
      WEA(0) => \ram_reg_0_i_1__11_n_0\,
      WEBWE(7 downto 0) => B"00000000"
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_12 is
  port (
    dout0_out : out STD_LOGIC_VECTOR ( 31 downto 0 );
    m00_axis_aclk : in STD_LOGIC;
    en : in STD_LOGIC;
    ADDRARDADDR : in STD_LOGIC_VECTOR ( 10 downto 0 );
    Q : in STD_LOGIC_VECTOR ( 31 downto 0 );
    addr : in STD_LOGIC_VECTOR ( 10 downto 0 );
    WE_r : in STD_LOGIC;
    square_r : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_12 : entity is "rams_sp_rf_rst";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_12;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_12 is
  signal \ram_reg_0_i_1__12_n_0\ : STD_LOGIC;
  signal NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_DBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_SBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_DOADO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 16 );
  signal NLW_ram_reg_0_DOBDO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_ram_reg_0_DOPADOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal NLW_ram_reg_0_DOPBDOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_ram_reg_0_ECCPARITY_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal NLW_ram_reg_0_RDADDRECC_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_ram_reg_1_CASCADEOUTA_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_1_CASCADEOUTB_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_1_DBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_1_INJECTDBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_1_INJECTSBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_1_SBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_1_DOADO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 14 );
  signal NLW_ram_reg_1_DOBDO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_ram_reg_1_DOPADOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_ram_reg_1_DOPBDOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_ram_reg_1_ECCPARITY_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal NLW_ram_reg_1_RDADDRECC_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ : string;
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of ram_reg_0 : label is "p2_d16";
  attribute METHODOLOGY_DRC_VIOS : string;
  attribute METHODOLOGY_DRC_VIOS of ram_reg_0 : label is "{SYNTH-6 {cell *THIS*}}";
  attribute RTL_RAM_BITS : integer;
  attribute RTL_RAM_BITS of ram_reg_0 : label is 61472;
  attribute RTL_RAM_NAME : string;
  attribute RTL_RAM_NAME of ram_reg_0 : label is "ram";
  attribute bram_addr_begin : integer;
  attribute bram_addr_begin of ram_reg_0 : label is 0;
  attribute bram_addr_end : integer;
  attribute bram_addr_end of ram_reg_0 : label is 2047;
  attribute bram_slice_begin : integer;
  attribute bram_slice_begin of ram_reg_0 : label is 0;
  attribute bram_slice_end : integer;
  attribute bram_slice_end of ram_reg_0 : label is 17;
  attribute ram_addr_begin : integer;
  attribute ram_addr_begin of ram_reg_0 : label is 0;
  attribute ram_addr_end : integer;
  attribute ram_addr_end of ram_reg_0 : label is 2047;
  attribute ram_offset : integer;
  attribute ram_offset of ram_reg_0 : label is 0;
  attribute ram_slice_begin : integer;
  attribute ram_slice_begin of ram_reg_0 : label is 0;
  attribute ram_slice_end : integer;
  attribute ram_slice_end of ram_reg_0 : label is 17;
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of ram_reg_1 : label is "p0_d14";
  attribute METHODOLOGY_DRC_VIOS of ram_reg_1 : label is "{SYNTH-6 {cell *THIS*}}";
  attribute RTL_RAM_BITS of ram_reg_1 : label is 61472;
  attribute RTL_RAM_NAME of ram_reg_1 : label is "ram";
  attribute bram_addr_begin of ram_reg_1 : label is 0;
  attribute bram_addr_end of ram_reg_1 : label is 2047;
  attribute bram_slice_begin of ram_reg_1 : label is 18;
  attribute bram_slice_end of ram_reg_1 : label is 31;
  attribute ram_addr_begin of ram_reg_1 : label is 0;
  attribute ram_addr_end of ram_reg_1 : label is 2047;
  attribute ram_offset of ram_reg_1 : label is 0;
  attribute ram_slice_begin of ram_reg_1 : label is 18;
  attribute ram_slice_end of ram_reg_1 : label is 31;
begin
ram_reg_0: unisim.vcomponents.RAMB36E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      EN_ECC_READ => false,
      EN_ECC_WRITE => false,
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      RAM_EXTENSION_A => "NONE",
      RAM_EXTENSION_B => "NONE",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "PERFORMANCE",
      READ_WIDTH_A => 18,
      READ_WIDTH_B => 0,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "READ_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 18,
      WRITE_WIDTH_B => 0
    )
        port map (
      ADDRARDADDR(15) => '1',
      ADDRARDADDR(14 downto 4) => ADDRARDADDR(10 downto 0),
      ADDRARDADDR(3 downto 0) => B"1111",
      ADDRBWRADDR(15 downto 0) => B"1111111111111111",
      CASCADEINA => '1',
      CASCADEINB => '0',
      CASCADEOUTA => NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED,
      CASCADEOUTB => NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED,
      CLKARDCLK => m00_axis_aclk,
      CLKBWRCLK => '0',
      DBITERR => NLW_ram_reg_0_DBITERR_UNCONNECTED,
      DIADI(31 downto 16) => B"0000000000000000",
      DIADI(15 downto 0) => Q(15 downto 0),
      DIBDI(31 downto 0) => B"11111111111111111111111111111111",
      DIPADIP(3 downto 2) => B"00",
      DIPADIP(1 downto 0) => Q(17 downto 16),
      DIPBDIP(3 downto 0) => B"1111",
      DOADO(31 downto 16) => NLW_ram_reg_0_DOADO_UNCONNECTED(31 downto 16),
      DOADO(15 downto 0) => dout0_out(15 downto 0),
      DOBDO(31 downto 0) => NLW_ram_reg_0_DOBDO_UNCONNECTED(31 downto 0),
      DOPADOP(3 downto 2) => NLW_ram_reg_0_DOPADOP_UNCONNECTED(3 downto 2),
      DOPADOP(1 downto 0) => dout0_out(17 downto 16),
      DOPBDOP(3 downto 0) => NLW_ram_reg_0_DOPBDOP_UNCONNECTED(3 downto 0),
      ECCPARITY(7 downto 0) => NLW_ram_reg_0_ECCPARITY_UNCONNECTED(7 downto 0),
      ENARDEN => en,
      ENBWREN => '0',
      INJECTDBITERR => NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED,
      INJECTSBITERR => NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED,
      RDADDRECC(8 downto 0) => NLW_ram_reg_0_RDADDRECC_UNCONNECTED(8 downto 0),
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      SBITERR => NLW_ram_reg_0_SBITERR_UNCONNECTED,
      WEA(3) => \ram_reg_0_i_1__12_n_0\,
      WEA(2) => \ram_reg_0_i_1__12_n_0\,
      WEA(1) => \ram_reg_0_i_1__12_n_0\,
      WEA(0) => \ram_reg_0_i_1__12_n_0\,
      WEBWE(7 downto 0) => B"00000000"
    );
\ram_reg_0_i_1__12\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => en,
      I1 => WE_r,
      I2 => square_r,
      O => \ram_reg_0_i_1__12_n_0\
    );
ram_reg_1: unisim.vcomponents.RAMB36E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      EN_ECC_READ => false,
      EN_ECC_WRITE => false,
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      RAM_EXTENSION_A => "NONE",
      RAM_EXTENSION_B => "NONE",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "PERFORMANCE",
      READ_WIDTH_A => 18,
      READ_WIDTH_B => 0,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "READ_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 18,
      WRITE_WIDTH_B => 0
    )
        port map (
      ADDRARDADDR(15) => '1',
      ADDRARDADDR(14 downto 4) => addr(10 downto 0),
      ADDRARDADDR(3 downto 0) => B"1111",
      ADDRBWRADDR(15 downto 0) => B"1111111111111111",
      CASCADEINA => '1',
      CASCADEINB => '0',
      CASCADEOUTA => NLW_ram_reg_1_CASCADEOUTA_UNCONNECTED,
      CASCADEOUTB => NLW_ram_reg_1_CASCADEOUTB_UNCONNECTED,
      CLKARDCLK => m00_axis_aclk,
      CLKBWRCLK => '0',
      DBITERR => NLW_ram_reg_1_DBITERR_UNCONNECTED,
      DIADI(31 downto 14) => B"000000000000000000",
      DIADI(13 downto 0) => Q(31 downto 18),
      DIBDI(31 downto 0) => B"11111111111111111111111111111111",
      DIPADIP(3 downto 0) => B"0000",
      DIPBDIP(3 downto 0) => B"1111",
      DOADO(31 downto 14) => NLW_ram_reg_1_DOADO_UNCONNECTED(31 downto 14),
      DOADO(13 downto 0) => dout0_out(31 downto 18),
      DOBDO(31 downto 0) => NLW_ram_reg_1_DOBDO_UNCONNECTED(31 downto 0),
      DOPADOP(3 downto 0) => NLW_ram_reg_1_DOPADOP_UNCONNECTED(3 downto 0),
      DOPBDOP(3 downto 0) => NLW_ram_reg_1_DOPBDOP_UNCONNECTED(3 downto 0),
      ECCPARITY(7 downto 0) => NLW_ram_reg_1_ECCPARITY_UNCONNECTED(7 downto 0),
      ENARDEN => en,
      ENBWREN => '0',
      INJECTDBITERR => NLW_ram_reg_1_INJECTDBITERR_UNCONNECTED,
      INJECTSBITERR => NLW_ram_reg_1_INJECTSBITERR_UNCONNECTED,
      RDADDRECC(8 downto 0) => NLW_ram_reg_1_RDADDRECC_UNCONNECTED(8 downto 0),
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      SBITERR => NLW_ram_reg_1_SBITERR_UNCONNECTED,
      WEA(3) => \ram_reg_0_i_1__12_n_0\,
      WEA(2) => \ram_reg_0_i_1__12_n_0\,
      WEA(1) => \ram_reg_0_i_1__12_n_0\,
      WEA(0) => \ram_reg_0_i_1__12_n_0\,
      WEBWE(7 downto 0) => B"00000000"
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_13 is
  port (
    dout0_out : out STD_LOGIC_VECTOR ( 31 downto 0 );
    m00_axis_aclk : in STD_LOGIC;
    en : in STD_LOGIC;
    ADDRARDADDR : in STD_LOGIC_VECTOR ( 10 downto 0 );
    Q : in STD_LOGIC_VECTOR ( 31 downto 0 );
    addr : in STD_LOGIC_VECTOR ( 10 downto 0 );
    WE_r : in STD_LOGIC;
    square_r : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_13 : entity is "rams_sp_rf_rst";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_13;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_13 is
  signal \ram_reg_0_i_1__13_n_0\ : STD_LOGIC;
  signal NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_DBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_SBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_DOADO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 16 );
  signal NLW_ram_reg_0_DOBDO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_ram_reg_0_DOPADOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal NLW_ram_reg_0_DOPBDOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_ram_reg_0_ECCPARITY_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal NLW_ram_reg_0_RDADDRECC_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_ram_reg_1_CASCADEOUTA_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_1_CASCADEOUTB_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_1_DBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_1_INJECTDBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_1_INJECTSBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_1_SBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_1_DOADO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 14 );
  signal NLW_ram_reg_1_DOBDO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_ram_reg_1_DOPADOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_ram_reg_1_DOPBDOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_ram_reg_1_ECCPARITY_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal NLW_ram_reg_1_RDADDRECC_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ : string;
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of ram_reg_0 : label is "p2_d16";
  attribute METHODOLOGY_DRC_VIOS : string;
  attribute METHODOLOGY_DRC_VIOS of ram_reg_0 : label is "{SYNTH-6 {cell *THIS*}}";
  attribute RTL_RAM_BITS : integer;
  attribute RTL_RAM_BITS of ram_reg_0 : label is 61472;
  attribute RTL_RAM_NAME : string;
  attribute RTL_RAM_NAME of ram_reg_0 : label is "ram";
  attribute bram_addr_begin : integer;
  attribute bram_addr_begin of ram_reg_0 : label is 0;
  attribute bram_addr_end : integer;
  attribute bram_addr_end of ram_reg_0 : label is 2047;
  attribute bram_slice_begin : integer;
  attribute bram_slice_begin of ram_reg_0 : label is 0;
  attribute bram_slice_end : integer;
  attribute bram_slice_end of ram_reg_0 : label is 17;
  attribute ram_addr_begin : integer;
  attribute ram_addr_begin of ram_reg_0 : label is 0;
  attribute ram_addr_end : integer;
  attribute ram_addr_end of ram_reg_0 : label is 2047;
  attribute ram_offset : integer;
  attribute ram_offset of ram_reg_0 : label is 0;
  attribute ram_slice_begin : integer;
  attribute ram_slice_begin of ram_reg_0 : label is 0;
  attribute ram_slice_end : integer;
  attribute ram_slice_end of ram_reg_0 : label is 17;
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of ram_reg_1 : label is "p0_d14";
  attribute METHODOLOGY_DRC_VIOS of ram_reg_1 : label is "{SYNTH-6 {cell *THIS*}}";
  attribute RTL_RAM_BITS of ram_reg_1 : label is 61472;
  attribute RTL_RAM_NAME of ram_reg_1 : label is "ram";
  attribute bram_addr_begin of ram_reg_1 : label is 0;
  attribute bram_addr_end of ram_reg_1 : label is 2047;
  attribute bram_slice_begin of ram_reg_1 : label is 18;
  attribute bram_slice_end of ram_reg_1 : label is 31;
  attribute ram_addr_begin of ram_reg_1 : label is 0;
  attribute ram_addr_end of ram_reg_1 : label is 2047;
  attribute ram_offset of ram_reg_1 : label is 0;
  attribute ram_slice_begin of ram_reg_1 : label is 18;
  attribute ram_slice_end of ram_reg_1 : label is 31;
begin
ram_reg_0: unisim.vcomponents.RAMB36E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      EN_ECC_READ => false,
      EN_ECC_WRITE => false,
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      RAM_EXTENSION_A => "NONE",
      RAM_EXTENSION_B => "NONE",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "PERFORMANCE",
      READ_WIDTH_A => 18,
      READ_WIDTH_B => 0,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "READ_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 18,
      WRITE_WIDTH_B => 0
    )
        port map (
      ADDRARDADDR(15) => '1',
      ADDRARDADDR(14 downto 4) => ADDRARDADDR(10 downto 0),
      ADDRARDADDR(3 downto 0) => B"1111",
      ADDRBWRADDR(15 downto 0) => B"1111111111111111",
      CASCADEINA => '1',
      CASCADEINB => '0',
      CASCADEOUTA => NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED,
      CASCADEOUTB => NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED,
      CLKARDCLK => m00_axis_aclk,
      CLKBWRCLK => '0',
      DBITERR => NLW_ram_reg_0_DBITERR_UNCONNECTED,
      DIADI(31 downto 16) => B"0000000000000000",
      DIADI(15 downto 0) => Q(15 downto 0),
      DIBDI(31 downto 0) => B"11111111111111111111111111111111",
      DIPADIP(3 downto 2) => B"00",
      DIPADIP(1 downto 0) => Q(17 downto 16),
      DIPBDIP(3 downto 0) => B"1111",
      DOADO(31 downto 16) => NLW_ram_reg_0_DOADO_UNCONNECTED(31 downto 16),
      DOADO(15 downto 0) => dout0_out(15 downto 0),
      DOBDO(31 downto 0) => NLW_ram_reg_0_DOBDO_UNCONNECTED(31 downto 0),
      DOPADOP(3 downto 2) => NLW_ram_reg_0_DOPADOP_UNCONNECTED(3 downto 2),
      DOPADOP(1 downto 0) => dout0_out(17 downto 16),
      DOPBDOP(3 downto 0) => NLW_ram_reg_0_DOPBDOP_UNCONNECTED(3 downto 0),
      ECCPARITY(7 downto 0) => NLW_ram_reg_0_ECCPARITY_UNCONNECTED(7 downto 0),
      ENARDEN => en,
      ENBWREN => '0',
      INJECTDBITERR => NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED,
      INJECTSBITERR => NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED,
      RDADDRECC(8 downto 0) => NLW_ram_reg_0_RDADDRECC_UNCONNECTED(8 downto 0),
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      SBITERR => NLW_ram_reg_0_SBITERR_UNCONNECTED,
      WEA(3) => \ram_reg_0_i_1__13_n_0\,
      WEA(2) => \ram_reg_0_i_1__13_n_0\,
      WEA(1) => \ram_reg_0_i_1__13_n_0\,
      WEA(0) => \ram_reg_0_i_1__13_n_0\,
      WEBWE(7 downto 0) => B"00000000"
    );
\ram_reg_0_i_1__13\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => en,
      I1 => WE_r,
      I2 => square_r,
      O => \ram_reg_0_i_1__13_n_0\
    );
ram_reg_1: unisim.vcomponents.RAMB36E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      EN_ECC_READ => false,
      EN_ECC_WRITE => false,
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      RAM_EXTENSION_A => "NONE",
      RAM_EXTENSION_B => "NONE",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "PERFORMANCE",
      READ_WIDTH_A => 18,
      READ_WIDTH_B => 0,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "READ_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 18,
      WRITE_WIDTH_B => 0
    )
        port map (
      ADDRARDADDR(15) => '1',
      ADDRARDADDR(14 downto 4) => addr(10 downto 0),
      ADDRARDADDR(3 downto 0) => B"1111",
      ADDRBWRADDR(15 downto 0) => B"1111111111111111",
      CASCADEINA => '1',
      CASCADEINB => '0',
      CASCADEOUTA => NLW_ram_reg_1_CASCADEOUTA_UNCONNECTED,
      CASCADEOUTB => NLW_ram_reg_1_CASCADEOUTB_UNCONNECTED,
      CLKARDCLK => m00_axis_aclk,
      CLKBWRCLK => '0',
      DBITERR => NLW_ram_reg_1_DBITERR_UNCONNECTED,
      DIADI(31 downto 14) => B"000000000000000000",
      DIADI(13 downto 0) => Q(31 downto 18),
      DIBDI(31 downto 0) => B"11111111111111111111111111111111",
      DIPADIP(3 downto 0) => B"0000",
      DIPBDIP(3 downto 0) => B"1111",
      DOADO(31 downto 14) => NLW_ram_reg_1_DOADO_UNCONNECTED(31 downto 14),
      DOADO(13 downto 0) => dout0_out(31 downto 18),
      DOBDO(31 downto 0) => NLW_ram_reg_1_DOBDO_UNCONNECTED(31 downto 0),
      DOPADOP(3 downto 0) => NLW_ram_reg_1_DOPADOP_UNCONNECTED(3 downto 0),
      DOPBDOP(3 downto 0) => NLW_ram_reg_1_DOPBDOP_UNCONNECTED(3 downto 0),
      ECCPARITY(7 downto 0) => NLW_ram_reg_1_ECCPARITY_UNCONNECTED(7 downto 0),
      ENARDEN => en,
      ENBWREN => '0',
      INJECTDBITERR => NLW_ram_reg_1_INJECTDBITERR_UNCONNECTED,
      INJECTSBITERR => NLW_ram_reg_1_INJECTSBITERR_UNCONNECTED,
      RDADDRECC(8 downto 0) => NLW_ram_reg_1_RDADDRECC_UNCONNECTED(8 downto 0),
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      SBITERR => NLW_ram_reg_1_SBITERR_UNCONNECTED,
      WEA(3) => \ram_reg_0_i_1__13_n_0\,
      WEA(2) => \ram_reg_0_i_1__13_n_0\,
      WEA(1) => \ram_reg_0_i_1__13_n_0\,
      WEA(0) => \ram_reg_0_i_1__13_n_0\,
      WEBWE(7 downto 0) => B"00000000"
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_14 is
  port (
    D : out STD_LOGIC_VECTOR ( 21 downto 0 );
    ram_reg_1_0 : out STD_LOGIC;
    ram_reg_1_1 : out STD_LOGIC;
    ram_reg_1_2 : out STD_LOGIC;
    ram_reg_1_3 : out STD_LOGIC;
    ram_reg_0_0 : out STD_LOGIC;
    ram_reg_0_1 : out STD_LOGIC;
    ram_reg_0_2 : out STD_LOGIC;
    ram_reg_0_3 : out STD_LOGIC;
    ram_reg_0_4 : out STD_LOGIC;
    ram_reg_0_5 : out STD_LOGIC;
    s00_axis_tuser : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \m00_axis_tdata_r_reg[30]\ : in STD_LOGIC;
    \m00_axis_tdata_r_reg[30]_0\ : in STD_LOGIC;
    \m00_axis_tdata_r_reg[29]\ : in STD_LOGIC;
    \m00_axis_tdata_r_reg[29]_0\ : in STD_LOGIC;
    \m00_axis_tdata_r_reg[28]\ : in STD_LOGIC;
    \m00_axis_tdata_r_reg[28]_0\ : in STD_LOGIC;
    \m00_axis_tdata_r_reg[26]\ : in STD_LOGIC;
    \m00_axis_tdata_r_reg[26]_0\ : in STD_LOGIC;
    \m00_axis_tdata_r_reg[24]\ : in STD_LOGIC;
    \m00_axis_tdata_r_reg[24]_0\ : in STD_LOGIC;
    \m00_axis_tdata_r_reg[23]\ : in STD_LOGIC;
    \m00_axis_tdata_r_reg[23]_0\ : in STD_LOGIC;
    \m00_axis_tdata_r_reg[22]\ : in STD_LOGIC;
    \m00_axis_tdata_r_reg[22]_0\ : in STD_LOGIC;
    \m00_axis_tdata_r_reg[21]\ : in STD_LOGIC;
    \m00_axis_tdata_r_reg[21]_0\ : in STD_LOGIC;
    \m00_axis_tdata_r_reg[19]\ : in STD_LOGIC;
    \m00_axis_tdata_r_reg[19]_0\ : in STD_LOGIC;
    \m00_axis_tdata_r_reg[18]\ : in STD_LOGIC;
    \m00_axis_tdata_r_reg[18]_0\ : in STD_LOGIC;
    \m00_axis_tdata_r_reg[17]\ : in STD_LOGIC;
    \m00_axis_tdata_r_reg[17]_0\ : in STD_LOGIC;
    \m00_axis_tdata_r_reg[14]\ : in STD_LOGIC;
    \m00_axis_tdata_r_reg[14]_0\ : in STD_LOGIC;
    \m00_axis_tdata_r_reg[13]\ : in STD_LOGIC;
    \m00_axis_tdata_r_reg[13]_0\ : in STD_LOGIC;
    \m00_axis_tdata_r_reg[12]\ : in STD_LOGIC;
    \m00_axis_tdata_r_reg[12]_0\ : in STD_LOGIC;
    \m00_axis_tdata_r_reg[10]\ : in STD_LOGIC;
    \m00_axis_tdata_r_reg[10]_0\ : in STD_LOGIC;
    \m00_axis_tdata_r_reg[8]\ : in STD_LOGIC;
    \m00_axis_tdata_r_reg[8]_0\ : in STD_LOGIC;
    \m00_axis_tdata_r_reg[7]\ : in STD_LOGIC;
    \m00_axis_tdata_r_reg[7]_0\ : in STD_LOGIC;
    \m00_axis_tdata_r_reg[6]\ : in STD_LOGIC;
    \m00_axis_tdata_r_reg[6]_0\ : in STD_LOGIC;
    \m00_axis_tdata_r_reg[5]\ : in STD_LOGIC;
    \m00_axis_tdata_r_reg[5]_0\ : in STD_LOGIC;
    \m00_axis_tdata_r_reg[3]\ : in STD_LOGIC;
    \m00_axis_tdata_r_reg[3]_0\ : in STD_LOGIC;
    \m00_axis_tdata_r_reg[2]\ : in STD_LOGIC;
    \m00_axis_tdata_r_reg[2]_0\ : in STD_LOGIC;
    \m00_axis_tdata_r_reg[1]\ : in STD_LOGIC;
    \m00_axis_tdata_r_reg[1]_0\ : in STD_LOGIC;
    dout0_out : in STD_LOGIC_VECTOR ( 31 downto 0 );
    \m00_axis_tdata_r_reg[31]\ : in STD_LOGIC_VECTOR ( 31 downto 0 );
    \m00_axis_tdata_r_reg[31]_0\ : in STD_LOGIC_VECTOR ( 31 downto 0 );
    m00_axis_aclk : in STD_LOGIC;
    en : in STD_LOGIC;
    ADDRARDADDR : in STD_LOGIC_VECTOR ( 10 downto 0 );
    Q : in STD_LOGIC_VECTOR ( 31 downto 0 );
    addr : in STD_LOGIC_VECTOR ( 10 downto 0 );
    WE_r : in STD_LOGIC;
    square_r : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_14 : entity is "rams_sp_rf_rst";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_14;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_14 is
  signal DOUTF_w : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \m00_axis_tdata_r[10]_i_2_n_0\ : STD_LOGIC;
  signal \m00_axis_tdata_r[12]_i_2_n_0\ : STD_LOGIC;
  signal \m00_axis_tdata_r[13]_i_2_n_0\ : STD_LOGIC;
  signal \m00_axis_tdata_r[14]_i_2_n_0\ : STD_LOGIC;
  signal \m00_axis_tdata_r[17]_i_2_n_0\ : STD_LOGIC;
  signal \m00_axis_tdata_r[18]_i_2_n_0\ : STD_LOGIC;
  signal \m00_axis_tdata_r[19]_i_2_n_0\ : STD_LOGIC;
  signal \m00_axis_tdata_r[1]_i_2_n_0\ : STD_LOGIC;
  signal \m00_axis_tdata_r[21]_i_2_n_0\ : STD_LOGIC;
  signal \m00_axis_tdata_r[22]_i_2_n_0\ : STD_LOGIC;
  signal \m00_axis_tdata_r[23]_i_2_n_0\ : STD_LOGIC;
  signal \m00_axis_tdata_r[24]_i_2_n_0\ : STD_LOGIC;
  signal \m00_axis_tdata_r[26]_i_2_n_0\ : STD_LOGIC;
  signal \m00_axis_tdata_r[28]_i_2_n_0\ : STD_LOGIC;
  signal \m00_axis_tdata_r[29]_i_2_n_0\ : STD_LOGIC;
  signal \m00_axis_tdata_r[2]_i_2_n_0\ : STD_LOGIC;
  signal \m00_axis_tdata_r[30]_i_2_n_0\ : STD_LOGIC;
  signal \m00_axis_tdata_r[3]_i_2_n_0\ : STD_LOGIC;
  signal \m00_axis_tdata_r[5]_i_2_n_0\ : STD_LOGIC;
  signal \m00_axis_tdata_r[6]_i_2_n_0\ : STD_LOGIC;
  signal \m00_axis_tdata_r[7]_i_2_n_0\ : STD_LOGIC;
  signal \m00_axis_tdata_r[8]_i_2_n_0\ : STD_LOGIC;
  signal \ram_reg_0_i_1__14_n_0\ : STD_LOGIC;
  signal NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_DBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_SBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_DOADO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 16 );
  signal NLW_ram_reg_0_DOBDO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_ram_reg_0_DOPADOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal NLW_ram_reg_0_DOPBDOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_ram_reg_0_ECCPARITY_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal NLW_ram_reg_0_RDADDRECC_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_ram_reg_1_CASCADEOUTA_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_1_CASCADEOUTB_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_1_DBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_1_INJECTDBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_1_INJECTSBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_1_SBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_1_DOADO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 14 );
  signal NLW_ram_reg_1_DOBDO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_ram_reg_1_DOPADOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_ram_reg_1_DOPBDOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_ram_reg_1_ECCPARITY_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal NLW_ram_reg_1_RDADDRECC_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ : string;
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of ram_reg_0 : label is "p2_d16";
  attribute METHODOLOGY_DRC_VIOS : string;
  attribute METHODOLOGY_DRC_VIOS of ram_reg_0 : label is "{SYNTH-6 {cell *THIS*}}";
  attribute RTL_RAM_BITS : integer;
  attribute RTL_RAM_BITS of ram_reg_0 : label is 61472;
  attribute RTL_RAM_NAME : string;
  attribute RTL_RAM_NAME of ram_reg_0 : label is "ram";
  attribute bram_addr_begin : integer;
  attribute bram_addr_begin of ram_reg_0 : label is 0;
  attribute bram_addr_end : integer;
  attribute bram_addr_end of ram_reg_0 : label is 2047;
  attribute bram_slice_begin : integer;
  attribute bram_slice_begin of ram_reg_0 : label is 0;
  attribute bram_slice_end : integer;
  attribute bram_slice_end of ram_reg_0 : label is 17;
  attribute ram_addr_begin : integer;
  attribute ram_addr_begin of ram_reg_0 : label is 0;
  attribute ram_addr_end : integer;
  attribute ram_addr_end of ram_reg_0 : label is 2047;
  attribute ram_offset : integer;
  attribute ram_offset of ram_reg_0 : label is 0;
  attribute ram_slice_begin : integer;
  attribute ram_slice_begin of ram_reg_0 : label is 0;
  attribute ram_slice_end : integer;
  attribute ram_slice_end of ram_reg_0 : label is 17;
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of ram_reg_1 : label is "p0_d14";
  attribute METHODOLOGY_DRC_VIOS of ram_reg_1 : label is "{SYNTH-6 {cell *THIS*}}";
  attribute RTL_RAM_BITS of ram_reg_1 : label is 61472;
  attribute RTL_RAM_NAME of ram_reg_1 : label is "ram";
  attribute bram_addr_begin of ram_reg_1 : label is 0;
  attribute bram_addr_end of ram_reg_1 : label is 2047;
  attribute bram_slice_begin of ram_reg_1 : label is 18;
  attribute bram_slice_end of ram_reg_1 : label is 31;
  attribute ram_addr_begin of ram_reg_1 : label is 0;
  attribute ram_addr_end of ram_reg_1 : label is 2047;
  attribute ram_offset of ram_reg_1 : label is 0;
  attribute ram_slice_begin of ram_reg_1 : label is 18;
  attribute ram_slice_end of ram_reg_1 : label is 31;
begin
\m00_axis_tdata_r[0]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => DOUTF_w(0),
      I1 => dout0_out(0),
      I2 => s00_axis_tuser(1),
      I3 => \m00_axis_tdata_r_reg[31]\(0),
      I4 => s00_axis_tuser(0),
      I5 => \m00_axis_tdata_r_reg[31]_0\(0),
      O => ram_reg_0_5
    );
\m00_axis_tdata_r[10]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DFD58A80"
    )
        port map (
      I0 => s00_axis_tuser(3),
      I1 => \m00_axis_tdata_r[10]_i_2_n_0\,
      I2 => s00_axis_tuser(2),
      I3 => \m00_axis_tdata_r_reg[10]\,
      I4 => \m00_axis_tdata_r_reg[10]_0\,
      O => D(7)
    );
\m00_axis_tdata_r[10]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => DOUTF_w(10),
      I1 => dout0_out(10),
      I2 => s00_axis_tuser(1),
      I3 => \m00_axis_tdata_r_reg[31]\(10),
      I4 => s00_axis_tuser(0),
      I5 => \m00_axis_tdata_r_reg[31]_0\(10),
      O => \m00_axis_tdata_r[10]_i_2_n_0\
    );
\m00_axis_tdata_r[11]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => DOUTF_w(11),
      I1 => dout0_out(11),
      I2 => s00_axis_tuser(1),
      I3 => \m00_axis_tdata_r_reg[31]\(11),
      I4 => s00_axis_tuser(0),
      I5 => \m00_axis_tdata_r_reg[31]_0\(11),
      O => ram_reg_0_2
    );
\m00_axis_tdata_r[12]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DFD58A80"
    )
        port map (
      I0 => s00_axis_tuser(3),
      I1 => \m00_axis_tdata_r[12]_i_2_n_0\,
      I2 => s00_axis_tuser(2),
      I3 => \m00_axis_tdata_r_reg[12]\,
      I4 => \m00_axis_tdata_r_reg[12]_0\,
      O => D(8)
    );
\m00_axis_tdata_r[12]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => DOUTF_w(12),
      I1 => dout0_out(12),
      I2 => s00_axis_tuser(1),
      I3 => \m00_axis_tdata_r_reg[31]\(12),
      I4 => s00_axis_tuser(0),
      I5 => \m00_axis_tdata_r_reg[31]_0\(12),
      O => \m00_axis_tdata_r[12]_i_2_n_0\
    );
\m00_axis_tdata_r[13]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DFD58A80"
    )
        port map (
      I0 => s00_axis_tuser(3),
      I1 => \m00_axis_tdata_r[13]_i_2_n_0\,
      I2 => s00_axis_tuser(2),
      I3 => \m00_axis_tdata_r_reg[13]\,
      I4 => \m00_axis_tdata_r_reg[13]_0\,
      O => D(9)
    );
\m00_axis_tdata_r[13]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => DOUTF_w(13),
      I1 => dout0_out(13),
      I2 => s00_axis_tuser(1),
      I3 => \m00_axis_tdata_r_reg[31]\(13),
      I4 => s00_axis_tuser(0),
      I5 => \m00_axis_tdata_r_reg[31]_0\(13),
      O => \m00_axis_tdata_r[13]_i_2_n_0\
    );
\m00_axis_tdata_r[14]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DFD58A80"
    )
        port map (
      I0 => s00_axis_tuser(3),
      I1 => \m00_axis_tdata_r[14]_i_2_n_0\,
      I2 => s00_axis_tuser(2),
      I3 => \m00_axis_tdata_r_reg[14]\,
      I4 => \m00_axis_tdata_r_reg[14]_0\,
      O => D(10)
    );
\m00_axis_tdata_r[14]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => DOUTF_w(14),
      I1 => dout0_out(14),
      I2 => s00_axis_tuser(1),
      I3 => \m00_axis_tdata_r_reg[31]\(14),
      I4 => s00_axis_tuser(0),
      I5 => \m00_axis_tdata_r_reg[31]_0\(14),
      O => \m00_axis_tdata_r[14]_i_2_n_0\
    );
\m00_axis_tdata_r[15]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => DOUTF_w(15),
      I1 => dout0_out(15),
      I2 => s00_axis_tuser(1),
      I3 => \m00_axis_tdata_r_reg[31]\(15),
      I4 => s00_axis_tuser(0),
      I5 => \m00_axis_tdata_r_reg[31]_0\(15),
      O => ram_reg_0_1
    );
\m00_axis_tdata_r[16]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => DOUTF_w(16),
      I1 => dout0_out(16),
      I2 => s00_axis_tuser(1),
      I3 => \m00_axis_tdata_r_reg[31]\(16),
      I4 => s00_axis_tuser(0),
      I5 => \m00_axis_tdata_r_reg[31]_0\(16),
      O => ram_reg_0_0
    );
\m00_axis_tdata_r[17]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DFD58A80"
    )
        port map (
      I0 => s00_axis_tuser(3),
      I1 => \m00_axis_tdata_r[17]_i_2_n_0\,
      I2 => s00_axis_tuser(2),
      I3 => \m00_axis_tdata_r_reg[17]\,
      I4 => \m00_axis_tdata_r_reg[17]_0\,
      O => D(11)
    );
\m00_axis_tdata_r[17]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => DOUTF_w(17),
      I1 => dout0_out(17),
      I2 => s00_axis_tuser(1),
      I3 => \m00_axis_tdata_r_reg[31]\(17),
      I4 => s00_axis_tuser(0),
      I5 => \m00_axis_tdata_r_reg[31]_0\(17),
      O => \m00_axis_tdata_r[17]_i_2_n_0\
    );
\m00_axis_tdata_r[18]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DFD58A80"
    )
        port map (
      I0 => s00_axis_tuser(3),
      I1 => \m00_axis_tdata_r[18]_i_2_n_0\,
      I2 => s00_axis_tuser(2),
      I3 => \m00_axis_tdata_r_reg[18]\,
      I4 => \m00_axis_tdata_r_reg[18]_0\,
      O => D(12)
    );
\m00_axis_tdata_r[18]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => DOUTF_w(18),
      I1 => dout0_out(18),
      I2 => s00_axis_tuser(1),
      I3 => \m00_axis_tdata_r_reg[31]\(18),
      I4 => s00_axis_tuser(0),
      I5 => \m00_axis_tdata_r_reg[31]_0\(18),
      O => \m00_axis_tdata_r[18]_i_2_n_0\
    );
\m00_axis_tdata_r[19]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DFD58A80"
    )
        port map (
      I0 => s00_axis_tuser(3),
      I1 => \m00_axis_tdata_r[19]_i_2_n_0\,
      I2 => s00_axis_tuser(2),
      I3 => \m00_axis_tdata_r_reg[19]\,
      I4 => \m00_axis_tdata_r_reg[19]_0\,
      O => D(13)
    );
\m00_axis_tdata_r[19]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => DOUTF_w(19),
      I1 => dout0_out(19),
      I2 => s00_axis_tuser(1),
      I3 => \m00_axis_tdata_r_reg[31]\(19),
      I4 => s00_axis_tuser(0),
      I5 => \m00_axis_tdata_r_reg[31]_0\(19),
      O => \m00_axis_tdata_r[19]_i_2_n_0\
    );
\m00_axis_tdata_r[1]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DFD58A80"
    )
        port map (
      I0 => s00_axis_tuser(3),
      I1 => \m00_axis_tdata_r[1]_i_2_n_0\,
      I2 => s00_axis_tuser(2),
      I3 => \m00_axis_tdata_r_reg[1]\,
      I4 => \m00_axis_tdata_r_reg[1]_0\,
      O => D(0)
    );
\m00_axis_tdata_r[1]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => DOUTF_w(1),
      I1 => dout0_out(1),
      I2 => s00_axis_tuser(1),
      I3 => \m00_axis_tdata_r_reg[31]\(1),
      I4 => s00_axis_tuser(0),
      I5 => \m00_axis_tdata_r_reg[31]_0\(1),
      O => \m00_axis_tdata_r[1]_i_2_n_0\
    );
\m00_axis_tdata_r[20]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => DOUTF_w(20),
      I1 => dout0_out(20),
      I2 => s00_axis_tuser(1),
      I3 => \m00_axis_tdata_r_reg[31]\(20),
      I4 => s00_axis_tuser(0),
      I5 => \m00_axis_tdata_r_reg[31]_0\(20),
      O => ram_reg_1_3
    );
\m00_axis_tdata_r[21]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DFD58A80"
    )
        port map (
      I0 => s00_axis_tuser(3),
      I1 => \m00_axis_tdata_r[21]_i_2_n_0\,
      I2 => s00_axis_tuser(2),
      I3 => \m00_axis_tdata_r_reg[21]\,
      I4 => \m00_axis_tdata_r_reg[21]_0\,
      O => D(14)
    );
\m00_axis_tdata_r[21]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => DOUTF_w(21),
      I1 => dout0_out(21),
      I2 => s00_axis_tuser(1),
      I3 => \m00_axis_tdata_r_reg[31]\(21),
      I4 => s00_axis_tuser(0),
      I5 => \m00_axis_tdata_r_reg[31]_0\(21),
      O => \m00_axis_tdata_r[21]_i_2_n_0\
    );
\m00_axis_tdata_r[22]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DFD58A80"
    )
        port map (
      I0 => s00_axis_tuser(3),
      I1 => \m00_axis_tdata_r[22]_i_2_n_0\,
      I2 => s00_axis_tuser(2),
      I3 => \m00_axis_tdata_r_reg[22]\,
      I4 => \m00_axis_tdata_r_reg[22]_0\,
      O => D(15)
    );
\m00_axis_tdata_r[22]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => DOUTF_w(22),
      I1 => dout0_out(22),
      I2 => s00_axis_tuser(1),
      I3 => \m00_axis_tdata_r_reg[31]\(22),
      I4 => s00_axis_tuser(0),
      I5 => \m00_axis_tdata_r_reg[31]_0\(22),
      O => \m00_axis_tdata_r[22]_i_2_n_0\
    );
\m00_axis_tdata_r[23]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DFD58A80"
    )
        port map (
      I0 => s00_axis_tuser(3),
      I1 => \m00_axis_tdata_r[23]_i_2_n_0\,
      I2 => s00_axis_tuser(2),
      I3 => \m00_axis_tdata_r_reg[23]\,
      I4 => \m00_axis_tdata_r_reg[23]_0\,
      O => D(16)
    );
\m00_axis_tdata_r[23]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => DOUTF_w(23),
      I1 => dout0_out(23),
      I2 => s00_axis_tuser(1),
      I3 => \m00_axis_tdata_r_reg[31]\(23),
      I4 => s00_axis_tuser(0),
      I5 => \m00_axis_tdata_r_reg[31]_0\(23),
      O => \m00_axis_tdata_r[23]_i_2_n_0\
    );
\m00_axis_tdata_r[24]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DFD58A80"
    )
        port map (
      I0 => s00_axis_tuser(3),
      I1 => \m00_axis_tdata_r[24]_i_2_n_0\,
      I2 => s00_axis_tuser(2),
      I3 => \m00_axis_tdata_r_reg[24]\,
      I4 => \m00_axis_tdata_r_reg[24]_0\,
      O => D(17)
    );
\m00_axis_tdata_r[24]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => DOUTF_w(24),
      I1 => dout0_out(24),
      I2 => s00_axis_tuser(1),
      I3 => \m00_axis_tdata_r_reg[31]\(24),
      I4 => s00_axis_tuser(0),
      I5 => \m00_axis_tdata_r_reg[31]_0\(24),
      O => \m00_axis_tdata_r[24]_i_2_n_0\
    );
\m00_axis_tdata_r[25]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => DOUTF_w(25),
      I1 => dout0_out(25),
      I2 => s00_axis_tuser(1),
      I3 => \m00_axis_tdata_r_reg[31]\(25),
      I4 => s00_axis_tuser(0),
      I5 => \m00_axis_tdata_r_reg[31]_0\(25),
      O => ram_reg_1_2
    );
\m00_axis_tdata_r[26]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DFD58A80"
    )
        port map (
      I0 => s00_axis_tuser(3),
      I1 => \m00_axis_tdata_r[26]_i_2_n_0\,
      I2 => s00_axis_tuser(2),
      I3 => \m00_axis_tdata_r_reg[26]\,
      I4 => \m00_axis_tdata_r_reg[26]_0\,
      O => D(18)
    );
\m00_axis_tdata_r[26]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => DOUTF_w(26),
      I1 => dout0_out(26),
      I2 => s00_axis_tuser(1),
      I3 => \m00_axis_tdata_r_reg[31]\(26),
      I4 => s00_axis_tuser(0),
      I5 => \m00_axis_tdata_r_reg[31]_0\(26),
      O => \m00_axis_tdata_r[26]_i_2_n_0\
    );
\m00_axis_tdata_r[27]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => DOUTF_w(27),
      I1 => dout0_out(27),
      I2 => s00_axis_tuser(1),
      I3 => \m00_axis_tdata_r_reg[31]\(27),
      I4 => s00_axis_tuser(0),
      I5 => \m00_axis_tdata_r_reg[31]_0\(27),
      O => ram_reg_1_1
    );
\m00_axis_tdata_r[28]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DFD58A80"
    )
        port map (
      I0 => s00_axis_tuser(3),
      I1 => \m00_axis_tdata_r[28]_i_2_n_0\,
      I2 => s00_axis_tuser(2),
      I3 => \m00_axis_tdata_r_reg[28]\,
      I4 => \m00_axis_tdata_r_reg[28]_0\,
      O => D(19)
    );
\m00_axis_tdata_r[28]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => DOUTF_w(28),
      I1 => dout0_out(28),
      I2 => s00_axis_tuser(1),
      I3 => \m00_axis_tdata_r_reg[31]\(28),
      I4 => s00_axis_tuser(0),
      I5 => \m00_axis_tdata_r_reg[31]_0\(28),
      O => \m00_axis_tdata_r[28]_i_2_n_0\
    );
\m00_axis_tdata_r[29]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DFD58A80"
    )
        port map (
      I0 => s00_axis_tuser(3),
      I1 => \m00_axis_tdata_r[29]_i_2_n_0\,
      I2 => s00_axis_tuser(2),
      I3 => \m00_axis_tdata_r_reg[29]\,
      I4 => \m00_axis_tdata_r_reg[29]_0\,
      O => D(20)
    );
\m00_axis_tdata_r[29]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => DOUTF_w(29),
      I1 => dout0_out(29),
      I2 => s00_axis_tuser(1),
      I3 => \m00_axis_tdata_r_reg[31]\(29),
      I4 => s00_axis_tuser(0),
      I5 => \m00_axis_tdata_r_reg[31]_0\(29),
      O => \m00_axis_tdata_r[29]_i_2_n_0\
    );
\m00_axis_tdata_r[2]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DFD58A80"
    )
        port map (
      I0 => s00_axis_tuser(3),
      I1 => \m00_axis_tdata_r[2]_i_2_n_0\,
      I2 => s00_axis_tuser(2),
      I3 => \m00_axis_tdata_r_reg[2]\,
      I4 => \m00_axis_tdata_r_reg[2]_0\,
      O => D(1)
    );
\m00_axis_tdata_r[2]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => DOUTF_w(2),
      I1 => dout0_out(2),
      I2 => s00_axis_tuser(1),
      I3 => \m00_axis_tdata_r_reg[31]\(2),
      I4 => s00_axis_tuser(0),
      I5 => \m00_axis_tdata_r_reg[31]_0\(2),
      O => \m00_axis_tdata_r[2]_i_2_n_0\
    );
\m00_axis_tdata_r[30]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DFD58A80"
    )
        port map (
      I0 => s00_axis_tuser(3),
      I1 => \m00_axis_tdata_r[30]_i_2_n_0\,
      I2 => s00_axis_tuser(2),
      I3 => \m00_axis_tdata_r_reg[30]\,
      I4 => \m00_axis_tdata_r_reg[30]_0\,
      O => D(21)
    );
\m00_axis_tdata_r[30]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => DOUTF_w(30),
      I1 => dout0_out(30),
      I2 => s00_axis_tuser(1),
      I3 => \m00_axis_tdata_r_reg[31]\(30),
      I4 => s00_axis_tuser(0),
      I5 => \m00_axis_tdata_r_reg[31]_0\(30),
      O => \m00_axis_tdata_r[30]_i_2_n_0\
    );
\m00_axis_tdata_r[31]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => DOUTF_w(31),
      I1 => dout0_out(31),
      I2 => s00_axis_tuser(1),
      I3 => \m00_axis_tdata_r_reg[31]\(31),
      I4 => s00_axis_tuser(0),
      I5 => \m00_axis_tdata_r_reg[31]_0\(31),
      O => ram_reg_1_0
    );
\m00_axis_tdata_r[3]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DFD58A80"
    )
        port map (
      I0 => s00_axis_tuser(3),
      I1 => \m00_axis_tdata_r[3]_i_2_n_0\,
      I2 => s00_axis_tuser(2),
      I3 => \m00_axis_tdata_r_reg[3]\,
      I4 => \m00_axis_tdata_r_reg[3]_0\,
      O => D(2)
    );
\m00_axis_tdata_r[3]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => DOUTF_w(3),
      I1 => dout0_out(3),
      I2 => s00_axis_tuser(1),
      I3 => \m00_axis_tdata_r_reg[31]\(3),
      I4 => s00_axis_tuser(0),
      I5 => \m00_axis_tdata_r_reg[31]_0\(3),
      O => \m00_axis_tdata_r[3]_i_2_n_0\
    );
\m00_axis_tdata_r[4]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => DOUTF_w(4),
      I1 => dout0_out(4),
      I2 => s00_axis_tuser(1),
      I3 => \m00_axis_tdata_r_reg[31]\(4),
      I4 => s00_axis_tuser(0),
      I5 => \m00_axis_tdata_r_reg[31]_0\(4),
      O => ram_reg_0_4
    );
\m00_axis_tdata_r[5]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DFD58A80"
    )
        port map (
      I0 => s00_axis_tuser(3),
      I1 => \m00_axis_tdata_r[5]_i_2_n_0\,
      I2 => s00_axis_tuser(2),
      I3 => \m00_axis_tdata_r_reg[5]\,
      I4 => \m00_axis_tdata_r_reg[5]_0\,
      O => D(3)
    );
\m00_axis_tdata_r[5]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => DOUTF_w(5),
      I1 => dout0_out(5),
      I2 => s00_axis_tuser(1),
      I3 => \m00_axis_tdata_r_reg[31]\(5),
      I4 => s00_axis_tuser(0),
      I5 => \m00_axis_tdata_r_reg[31]_0\(5),
      O => \m00_axis_tdata_r[5]_i_2_n_0\
    );
\m00_axis_tdata_r[6]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DFD58A80"
    )
        port map (
      I0 => s00_axis_tuser(3),
      I1 => \m00_axis_tdata_r[6]_i_2_n_0\,
      I2 => s00_axis_tuser(2),
      I3 => \m00_axis_tdata_r_reg[6]\,
      I4 => \m00_axis_tdata_r_reg[6]_0\,
      O => D(4)
    );
\m00_axis_tdata_r[6]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => DOUTF_w(6),
      I1 => dout0_out(6),
      I2 => s00_axis_tuser(1),
      I3 => \m00_axis_tdata_r_reg[31]\(6),
      I4 => s00_axis_tuser(0),
      I5 => \m00_axis_tdata_r_reg[31]_0\(6),
      O => \m00_axis_tdata_r[6]_i_2_n_0\
    );
\m00_axis_tdata_r[7]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DFD58A80"
    )
        port map (
      I0 => s00_axis_tuser(3),
      I1 => \m00_axis_tdata_r[7]_i_2_n_0\,
      I2 => s00_axis_tuser(2),
      I3 => \m00_axis_tdata_r_reg[7]\,
      I4 => \m00_axis_tdata_r_reg[7]_0\,
      O => D(5)
    );
\m00_axis_tdata_r[7]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => DOUTF_w(7),
      I1 => dout0_out(7),
      I2 => s00_axis_tuser(1),
      I3 => \m00_axis_tdata_r_reg[31]\(7),
      I4 => s00_axis_tuser(0),
      I5 => \m00_axis_tdata_r_reg[31]_0\(7),
      O => \m00_axis_tdata_r[7]_i_2_n_0\
    );
\m00_axis_tdata_r[8]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DFD58A80"
    )
        port map (
      I0 => s00_axis_tuser(3),
      I1 => \m00_axis_tdata_r[8]_i_2_n_0\,
      I2 => s00_axis_tuser(2),
      I3 => \m00_axis_tdata_r_reg[8]\,
      I4 => \m00_axis_tdata_r_reg[8]_0\,
      O => D(6)
    );
\m00_axis_tdata_r[8]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => DOUTF_w(8),
      I1 => dout0_out(8),
      I2 => s00_axis_tuser(1),
      I3 => \m00_axis_tdata_r_reg[31]\(8),
      I4 => s00_axis_tuser(0),
      I5 => \m00_axis_tdata_r_reg[31]_0\(8),
      O => \m00_axis_tdata_r[8]_i_2_n_0\
    );
\m00_axis_tdata_r[9]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => DOUTF_w(9),
      I1 => dout0_out(9),
      I2 => s00_axis_tuser(1),
      I3 => \m00_axis_tdata_r_reg[31]\(9),
      I4 => s00_axis_tuser(0),
      I5 => \m00_axis_tdata_r_reg[31]_0\(9),
      O => ram_reg_0_3
    );
ram_reg_0: unisim.vcomponents.RAMB36E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      EN_ECC_READ => false,
      EN_ECC_WRITE => false,
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      RAM_EXTENSION_A => "NONE",
      RAM_EXTENSION_B => "NONE",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "PERFORMANCE",
      READ_WIDTH_A => 18,
      READ_WIDTH_B => 0,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "READ_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 18,
      WRITE_WIDTH_B => 0
    )
        port map (
      ADDRARDADDR(15) => '1',
      ADDRARDADDR(14 downto 4) => ADDRARDADDR(10 downto 0),
      ADDRARDADDR(3 downto 0) => B"1111",
      ADDRBWRADDR(15 downto 0) => B"1111111111111111",
      CASCADEINA => '1',
      CASCADEINB => '0',
      CASCADEOUTA => NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED,
      CASCADEOUTB => NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED,
      CLKARDCLK => m00_axis_aclk,
      CLKBWRCLK => '0',
      DBITERR => NLW_ram_reg_0_DBITERR_UNCONNECTED,
      DIADI(31 downto 16) => B"0000000000000000",
      DIADI(15 downto 0) => Q(15 downto 0),
      DIBDI(31 downto 0) => B"11111111111111111111111111111111",
      DIPADIP(3 downto 2) => B"00",
      DIPADIP(1 downto 0) => Q(17 downto 16),
      DIPBDIP(3 downto 0) => B"1111",
      DOADO(31 downto 16) => NLW_ram_reg_0_DOADO_UNCONNECTED(31 downto 16),
      DOADO(15 downto 0) => DOUTF_w(15 downto 0),
      DOBDO(31 downto 0) => NLW_ram_reg_0_DOBDO_UNCONNECTED(31 downto 0),
      DOPADOP(3 downto 2) => NLW_ram_reg_0_DOPADOP_UNCONNECTED(3 downto 2),
      DOPADOP(1 downto 0) => DOUTF_w(17 downto 16),
      DOPBDOP(3 downto 0) => NLW_ram_reg_0_DOPBDOP_UNCONNECTED(3 downto 0),
      ECCPARITY(7 downto 0) => NLW_ram_reg_0_ECCPARITY_UNCONNECTED(7 downto 0),
      ENARDEN => en,
      ENBWREN => '0',
      INJECTDBITERR => NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED,
      INJECTSBITERR => NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED,
      RDADDRECC(8 downto 0) => NLW_ram_reg_0_RDADDRECC_UNCONNECTED(8 downto 0),
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      SBITERR => NLW_ram_reg_0_SBITERR_UNCONNECTED,
      WEA(3) => \ram_reg_0_i_1__14_n_0\,
      WEA(2) => \ram_reg_0_i_1__14_n_0\,
      WEA(1) => \ram_reg_0_i_1__14_n_0\,
      WEA(0) => \ram_reg_0_i_1__14_n_0\,
      WEBWE(7 downto 0) => B"00000000"
    );
\ram_reg_0_i_1__14\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => en,
      I1 => WE_r,
      I2 => square_r,
      O => \ram_reg_0_i_1__14_n_0\
    );
ram_reg_1: unisim.vcomponents.RAMB36E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      EN_ECC_READ => false,
      EN_ECC_WRITE => false,
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      RAM_EXTENSION_A => "NONE",
      RAM_EXTENSION_B => "NONE",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "PERFORMANCE",
      READ_WIDTH_A => 18,
      READ_WIDTH_B => 0,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "READ_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 18,
      WRITE_WIDTH_B => 0
    )
        port map (
      ADDRARDADDR(15) => '1',
      ADDRARDADDR(14 downto 4) => addr(10 downto 0),
      ADDRARDADDR(3 downto 0) => B"1111",
      ADDRBWRADDR(15 downto 0) => B"1111111111111111",
      CASCADEINA => '1',
      CASCADEINB => '0',
      CASCADEOUTA => NLW_ram_reg_1_CASCADEOUTA_UNCONNECTED,
      CASCADEOUTB => NLW_ram_reg_1_CASCADEOUTB_UNCONNECTED,
      CLKARDCLK => m00_axis_aclk,
      CLKBWRCLK => '0',
      DBITERR => NLW_ram_reg_1_DBITERR_UNCONNECTED,
      DIADI(31 downto 14) => B"000000000000000000",
      DIADI(13 downto 0) => Q(31 downto 18),
      DIBDI(31 downto 0) => B"11111111111111111111111111111111",
      DIPADIP(3 downto 0) => B"0000",
      DIPBDIP(3 downto 0) => B"1111",
      DOADO(31 downto 14) => NLW_ram_reg_1_DOADO_UNCONNECTED(31 downto 14),
      DOADO(13 downto 0) => DOUTF_w(31 downto 18),
      DOBDO(31 downto 0) => NLW_ram_reg_1_DOBDO_UNCONNECTED(31 downto 0),
      DOPADOP(3 downto 0) => NLW_ram_reg_1_DOPADOP_UNCONNECTED(3 downto 0),
      DOPBDOP(3 downto 0) => NLW_ram_reg_1_DOPBDOP_UNCONNECTED(3 downto 0),
      ECCPARITY(7 downto 0) => NLW_ram_reg_1_ECCPARITY_UNCONNECTED(7 downto 0),
      ENARDEN => en,
      ENBWREN => '0',
      INJECTDBITERR => NLW_ram_reg_1_INJECTDBITERR_UNCONNECTED,
      INJECTSBITERR => NLW_ram_reg_1_INJECTSBITERR_UNCONNECTED,
      RDADDRECC(8 downto 0) => NLW_ram_reg_1_RDADDRECC_UNCONNECTED(8 downto 0),
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      SBITERR => NLW_ram_reg_1_SBITERR_UNCONNECTED,
      WEA(3) => \ram_reg_0_i_1__14_n_0\,
      WEA(2) => \ram_reg_0_i_1__14_n_0\,
      WEA(1) => \ram_reg_0_i_1__14_n_0\,
      WEA(0) => \ram_reg_0_i_1__14_n_0\,
      WEBWE(7 downto 0) => B"00000000"
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_2 is
  port (
    D : out STD_LOGIC_VECTOR ( 0 to 0 );
    \s00_axis_tuser[18]\ : out STD_LOGIC;
    \s00_axis_tuser[18]_0\ : out STD_LOGIC;
    \s00_axis_tuser[18]_1\ : out STD_LOGIC;
    ram_reg_1_0 : out STD_LOGIC;
    \s00_axis_tuser[18]_2\ : out STD_LOGIC;
    ram_reg_1_1 : out STD_LOGIC;
    \s00_axis_tuser[18]_3\ : out STD_LOGIC;
    \s00_axis_tuser[18]_4\ : out STD_LOGIC;
    \s00_axis_tuser[18]_5\ : out STD_LOGIC;
    \s00_axis_tuser[18]_6\ : out STD_LOGIC;
    ram_reg_1_2 : out STD_LOGIC;
    \s00_axis_tuser[18]_7\ : out STD_LOGIC;
    \s00_axis_tuser[18]_8\ : out STD_LOGIC;
    \s00_axis_tuser[18]_9\ : out STD_LOGIC;
    ram_reg_0_0 : out STD_LOGIC;
    ram_reg_0_1 : out STD_LOGIC;
    \s00_axis_tuser[18]_10\ : out STD_LOGIC;
    \s00_axis_tuser[18]_11\ : out STD_LOGIC;
    \s00_axis_tuser[18]_12\ : out STD_LOGIC;
    ram_reg_0_2 : out STD_LOGIC;
    \s00_axis_tuser[18]_13\ : out STD_LOGIC;
    ram_reg_0_3 : out STD_LOGIC;
    \s00_axis_tuser[18]_14\ : out STD_LOGIC;
    \s00_axis_tuser[18]_15\ : out STD_LOGIC;
    \s00_axis_tuser[18]_16\ : out STD_LOGIC;
    \s00_axis_tuser[18]_17\ : out STD_LOGIC;
    ram_reg_0_4 : out STD_LOGIC;
    \s00_axis_tuser[18]_18\ : out STD_LOGIC;
    \s00_axis_tuser[18]_19\ : out STD_LOGIC;
    \s00_axis_tuser[18]_20\ : out STD_LOGIC;
    ram_reg_0_5 : out STD_LOGIC;
    \m00_axis_tdata_r_reg[31]\ : in STD_LOGIC;
    s00_axis_tuser : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \m00_axis_tdata_r_reg[31]_0\ : in STD_LOGIC;
    \m00_axis_tdata_r_reg[31]_1\ : in STD_LOGIC;
    dout0_out : in STD_LOGIC_VECTOR ( 31 downto 0 );
    \m00_axis_tdata_r_reg[31]_2\ : in STD_LOGIC_VECTOR ( 31 downto 0 );
    \m00_axis_tdata_r_reg[31]_3\ : in STD_LOGIC_VECTOR ( 31 downto 0 );
    \m00_axis_tdata_r_reg[30]\ : in STD_LOGIC;
    \m00_axis_tdata_r_reg[29]\ : in STD_LOGIC;
    \m00_axis_tdata_r_reg[28]\ : in STD_LOGIC;
    \m00_axis_tdata_r_reg[26]\ : in STD_LOGIC;
    \m00_axis_tdata_r_reg[24]\ : in STD_LOGIC;
    \m00_axis_tdata_r_reg[23]\ : in STD_LOGIC;
    \m00_axis_tdata_r_reg[22]\ : in STD_LOGIC;
    \m00_axis_tdata_r_reg[21]\ : in STD_LOGIC;
    \m00_axis_tdata_r_reg[19]\ : in STD_LOGIC;
    \m00_axis_tdata_r_reg[18]\ : in STD_LOGIC;
    \m00_axis_tdata_r_reg[17]\ : in STD_LOGIC;
    \m00_axis_tdata_r_reg[14]\ : in STD_LOGIC;
    \m00_axis_tdata_r_reg[13]\ : in STD_LOGIC;
    \m00_axis_tdata_r_reg[12]\ : in STD_LOGIC;
    \m00_axis_tdata_r_reg[10]\ : in STD_LOGIC;
    \m00_axis_tdata_r_reg[8]\ : in STD_LOGIC;
    \m00_axis_tdata_r_reg[7]\ : in STD_LOGIC;
    \m00_axis_tdata_r_reg[6]\ : in STD_LOGIC;
    \m00_axis_tdata_r_reg[5]\ : in STD_LOGIC;
    \m00_axis_tdata_r_reg[3]\ : in STD_LOGIC;
    \m00_axis_tdata_r_reg[2]\ : in STD_LOGIC;
    \m00_axis_tdata_r_reg[1]\ : in STD_LOGIC;
    m00_axis_aclk : in STD_LOGIC;
    en : in STD_LOGIC;
    ADDRARDADDR : in STD_LOGIC_VECTOR ( 10 downto 0 );
    Q : in STD_LOGIC_VECTOR ( 31 downto 0 );
    addr : in STD_LOGIC_VECTOR ( 10 downto 0 );
    WE_r : in STD_LOGIC;
    square_r : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_2 : entity is "rams_sp_rf_rst";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_2;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_2 is
  signal DOUT3_w : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \m00_axis_tdata_r[10]_i_5_n_0\ : STD_LOGIC;
  signal \m00_axis_tdata_r[12]_i_5_n_0\ : STD_LOGIC;
  signal \m00_axis_tdata_r[13]_i_5_n_0\ : STD_LOGIC;
  signal \m00_axis_tdata_r[14]_i_5_n_0\ : STD_LOGIC;
  signal \m00_axis_tdata_r[17]_i_5_n_0\ : STD_LOGIC;
  signal \m00_axis_tdata_r[18]_i_5_n_0\ : STD_LOGIC;
  signal \m00_axis_tdata_r[19]_i_5_n_0\ : STD_LOGIC;
  signal \m00_axis_tdata_r[1]_i_5_n_0\ : STD_LOGIC;
  signal \m00_axis_tdata_r[21]_i_5_n_0\ : STD_LOGIC;
  signal \m00_axis_tdata_r[22]_i_5_n_0\ : STD_LOGIC;
  signal \m00_axis_tdata_r[23]_i_5_n_0\ : STD_LOGIC;
  signal \m00_axis_tdata_r[24]_i_5_n_0\ : STD_LOGIC;
  signal \m00_axis_tdata_r[26]_i_5_n_0\ : STD_LOGIC;
  signal \m00_axis_tdata_r[28]_i_5_n_0\ : STD_LOGIC;
  signal \m00_axis_tdata_r[29]_i_5_n_0\ : STD_LOGIC;
  signal \m00_axis_tdata_r[2]_i_5_n_0\ : STD_LOGIC;
  signal \m00_axis_tdata_r[30]_i_5_n_0\ : STD_LOGIC;
  signal \m00_axis_tdata_r[31]_i_5_n_0\ : STD_LOGIC;
  signal \m00_axis_tdata_r[3]_i_5_n_0\ : STD_LOGIC;
  signal \m00_axis_tdata_r[5]_i_5_n_0\ : STD_LOGIC;
  signal \m00_axis_tdata_r[6]_i_5_n_0\ : STD_LOGIC;
  signal \m00_axis_tdata_r[7]_i_5_n_0\ : STD_LOGIC;
  signal \m00_axis_tdata_r[8]_i_5_n_0\ : STD_LOGIC;
  signal \ram_reg_0_i_1__2_n_0\ : STD_LOGIC;
  signal NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_DBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_SBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_DOADO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 16 );
  signal NLW_ram_reg_0_DOBDO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_ram_reg_0_DOPADOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal NLW_ram_reg_0_DOPBDOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_ram_reg_0_ECCPARITY_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal NLW_ram_reg_0_RDADDRECC_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_ram_reg_1_CASCADEOUTA_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_1_CASCADEOUTB_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_1_DBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_1_INJECTDBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_1_INJECTSBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_1_SBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_1_DOADO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 14 );
  signal NLW_ram_reg_1_DOBDO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_ram_reg_1_DOPADOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_ram_reg_1_DOPBDOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_ram_reg_1_ECCPARITY_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal NLW_ram_reg_1_RDADDRECC_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ : string;
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of ram_reg_0 : label is "p2_d16";
  attribute METHODOLOGY_DRC_VIOS : string;
  attribute METHODOLOGY_DRC_VIOS of ram_reg_0 : label is "{SYNTH-6 {cell *THIS*}}";
  attribute RTL_RAM_BITS : integer;
  attribute RTL_RAM_BITS of ram_reg_0 : label is 61472;
  attribute RTL_RAM_NAME : string;
  attribute RTL_RAM_NAME of ram_reg_0 : label is "ram";
  attribute bram_addr_begin : integer;
  attribute bram_addr_begin of ram_reg_0 : label is 0;
  attribute bram_addr_end : integer;
  attribute bram_addr_end of ram_reg_0 : label is 2047;
  attribute bram_slice_begin : integer;
  attribute bram_slice_begin of ram_reg_0 : label is 0;
  attribute bram_slice_end : integer;
  attribute bram_slice_end of ram_reg_0 : label is 17;
  attribute ram_addr_begin : integer;
  attribute ram_addr_begin of ram_reg_0 : label is 0;
  attribute ram_addr_end : integer;
  attribute ram_addr_end of ram_reg_0 : label is 2047;
  attribute ram_offset : integer;
  attribute ram_offset of ram_reg_0 : label is 0;
  attribute ram_slice_begin : integer;
  attribute ram_slice_begin of ram_reg_0 : label is 0;
  attribute ram_slice_end : integer;
  attribute ram_slice_end of ram_reg_0 : label is 17;
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of ram_reg_1 : label is "p0_d14";
  attribute METHODOLOGY_DRC_VIOS of ram_reg_1 : label is "{SYNTH-6 {cell *THIS*}}";
  attribute RTL_RAM_BITS of ram_reg_1 : label is 61472;
  attribute RTL_RAM_NAME of ram_reg_1 : label is "ram";
  attribute bram_addr_begin of ram_reg_1 : label is 0;
  attribute bram_addr_end of ram_reg_1 : label is 2047;
  attribute bram_slice_begin of ram_reg_1 : label is 18;
  attribute bram_slice_end of ram_reg_1 : label is 31;
  attribute ram_addr_begin of ram_reg_1 : label is 0;
  attribute ram_addr_end of ram_reg_1 : label is 2047;
  attribute ram_offset of ram_reg_1 : label is 0;
  attribute ram_slice_begin of ram_reg_1 : label is 18;
  attribute ram_slice_end of ram_reg_1 : label is 31;
begin
\m00_axis_tdata_r[0]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => DOUT3_w(0),
      I1 => dout0_out(0),
      I2 => s00_axis_tuser(1),
      I3 => \m00_axis_tdata_r_reg[31]_2\(0),
      I4 => s00_axis_tuser(0),
      I5 => \m00_axis_tdata_r_reg[31]_3\(0),
      O => ram_reg_0_5
    );
\m00_axis_tdata_r[10]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => DOUT3_w(10),
      I1 => dout0_out(10),
      I2 => s00_axis_tuser(1),
      I3 => \m00_axis_tdata_r_reg[31]_2\(10),
      I4 => s00_axis_tuser(0),
      I5 => \m00_axis_tdata_r_reg[31]_3\(10),
      O => \m00_axis_tdata_r[10]_i_5_n_0\
    );
\m00_axis_tdata_r[11]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => DOUT3_w(11),
      I1 => dout0_out(11),
      I2 => s00_axis_tuser(1),
      I3 => \m00_axis_tdata_r_reg[31]_2\(11),
      I4 => s00_axis_tuser(0),
      I5 => \m00_axis_tdata_r_reg[31]_3\(11),
      O => ram_reg_0_2
    );
\m00_axis_tdata_r[12]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => DOUT3_w(12),
      I1 => dout0_out(12),
      I2 => s00_axis_tuser(1),
      I3 => \m00_axis_tdata_r_reg[31]_2\(12),
      I4 => s00_axis_tuser(0),
      I5 => \m00_axis_tdata_r_reg[31]_3\(12),
      O => \m00_axis_tdata_r[12]_i_5_n_0\
    );
\m00_axis_tdata_r[13]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => DOUT3_w(13),
      I1 => dout0_out(13),
      I2 => s00_axis_tuser(1),
      I3 => \m00_axis_tdata_r_reg[31]_2\(13),
      I4 => s00_axis_tuser(0),
      I5 => \m00_axis_tdata_r_reg[31]_3\(13),
      O => \m00_axis_tdata_r[13]_i_5_n_0\
    );
\m00_axis_tdata_r[14]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => DOUT3_w(14),
      I1 => dout0_out(14),
      I2 => s00_axis_tuser(1),
      I3 => \m00_axis_tdata_r_reg[31]_2\(14),
      I4 => s00_axis_tuser(0),
      I5 => \m00_axis_tdata_r_reg[31]_3\(14),
      O => \m00_axis_tdata_r[14]_i_5_n_0\
    );
\m00_axis_tdata_r[15]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => DOUT3_w(15),
      I1 => dout0_out(15),
      I2 => s00_axis_tuser(1),
      I3 => \m00_axis_tdata_r_reg[31]_2\(15),
      I4 => s00_axis_tuser(0),
      I5 => \m00_axis_tdata_r_reg[31]_3\(15),
      O => ram_reg_0_1
    );
\m00_axis_tdata_r[16]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => DOUT3_w(16),
      I1 => dout0_out(16),
      I2 => s00_axis_tuser(1),
      I3 => \m00_axis_tdata_r_reg[31]_2\(16),
      I4 => s00_axis_tuser(0),
      I5 => \m00_axis_tdata_r_reg[31]_3\(16),
      O => ram_reg_0_0
    );
\m00_axis_tdata_r[17]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => DOUT3_w(17),
      I1 => dout0_out(17),
      I2 => s00_axis_tuser(1),
      I3 => \m00_axis_tdata_r_reg[31]_2\(17),
      I4 => s00_axis_tuser(0),
      I5 => \m00_axis_tdata_r_reg[31]_3\(17),
      O => \m00_axis_tdata_r[17]_i_5_n_0\
    );
\m00_axis_tdata_r[18]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => DOUT3_w(18),
      I1 => dout0_out(18),
      I2 => s00_axis_tuser(1),
      I3 => \m00_axis_tdata_r_reg[31]_2\(18),
      I4 => s00_axis_tuser(0),
      I5 => \m00_axis_tdata_r_reg[31]_3\(18),
      O => \m00_axis_tdata_r[18]_i_5_n_0\
    );
\m00_axis_tdata_r[19]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => DOUT3_w(19),
      I1 => dout0_out(19),
      I2 => s00_axis_tuser(1),
      I3 => \m00_axis_tdata_r_reg[31]_2\(19),
      I4 => s00_axis_tuser(0),
      I5 => \m00_axis_tdata_r_reg[31]_3\(19),
      O => \m00_axis_tdata_r[19]_i_5_n_0\
    );
\m00_axis_tdata_r[1]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => DOUT3_w(1),
      I1 => dout0_out(1),
      I2 => s00_axis_tuser(1),
      I3 => \m00_axis_tdata_r_reg[31]_2\(1),
      I4 => s00_axis_tuser(0),
      I5 => \m00_axis_tdata_r_reg[31]_3\(1),
      O => \m00_axis_tdata_r[1]_i_5_n_0\
    );
\m00_axis_tdata_r[20]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => DOUT3_w(20),
      I1 => dout0_out(20),
      I2 => s00_axis_tuser(1),
      I3 => \m00_axis_tdata_r_reg[31]_2\(20),
      I4 => s00_axis_tuser(0),
      I5 => \m00_axis_tdata_r_reg[31]_3\(20),
      O => ram_reg_1_2
    );
\m00_axis_tdata_r[21]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => DOUT3_w(21),
      I1 => dout0_out(21),
      I2 => s00_axis_tuser(1),
      I3 => \m00_axis_tdata_r_reg[31]_2\(21),
      I4 => s00_axis_tuser(0),
      I5 => \m00_axis_tdata_r_reg[31]_3\(21),
      O => \m00_axis_tdata_r[21]_i_5_n_0\
    );
\m00_axis_tdata_r[22]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => DOUT3_w(22),
      I1 => dout0_out(22),
      I2 => s00_axis_tuser(1),
      I3 => \m00_axis_tdata_r_reg[31]_2\(22),
      I4 => s00_axis_tuser(0),
      I5 => \m00_axis_tdata_r_reg[31]_3\(22),
      O => \m00_axis_tdata_r[22]_i_5_n_0\
    );
\m00_axis_tdata_r[23]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => DOUT3_w(23),
      I1 => dout0_out(23),
      I2 => s00_axis_tuser(1),
      I3 => \m00_axis_tdata_r_reg[31]_2\(23),
      I4 => s00_axis_tuser(0),
      I5 => \m00_axis_tdata_r_reg[31]_3\(23),
      O => \m00_axis_tdata_r[23]_i_5_n_0\
    );
\m00_axis_tdata_r[24]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => DOUT3_w(24),
      I1 => dout0_out(24),
      I2 => s00_axis_tuser(1),
      I3 => \m00_axis_tdata_r_reg[31]_2\(24),
      I4 => s00_axis_tuser(0),
      I5 => \m00_axis_tdata_r_reg[31]_3\(24),
      O => \m00_axis_tdata_r[24]_i_5_n_0\
    );
\m00_axis_tdata_r[25]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => DOUT3_w(25),
      I1 => dout0_out(25),
      I2 => s00_axis_tuser(1),
      I3 => \m00_axis_tdata_r_reg[31]_2\(25),
      I4 => s00_axis_tuser(0),
      I5 => \m00_axis_tdata_r_reg[31]_3\(25),
      O => ram_reg_1_1
    );
\m00_axis_tdata_r[26]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => DOUT3_w(26),
      I1 => dout0_out(26),
      I2 => s00_axis_tuser(1),
      I3 => \m00_axis_tdata_r_reg[31]_2\(26),
      I4 => s00_axis_tuser(0),
      I5 => \m00_axis_tdata_r_reg[31]_3\(26),
      O => \m00_axis_tdata_r[26]_i_5_n_0\
    );
\m00_axis_tdata_r[27]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => DOUT3_w(27),
      I1 => dout0_out(27),
      I2 => s00_axis_tuser(1),
      I3 => \m00_axis_tdata_r_reg[31]_2\(27),
      I4 => s00_axis_tuser(0),
      I5 => \m00_axis_tdata_r_reg[31]_3\(27),
      O => ram_reg_1_0
    );
\m00_axis_tdata_r[28]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => DOUT3_w(28),
      I1 => dout0_out(28),
      I2 => s00_axis_tuser(1),
      I3 => \m00_axis_tdata_r_reg[31]_2\(28),
      I4 => s00_axis_tuser(0),
      I5 => \m00_axis_tdata_r_reg[31]_3\(28),
      O => \m00_axis_tdata_r[28]_i_5_n_0\
    );
\m00_axis_tdata_r[29]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => DOUT3_w(29),
      I1 => dout0_out(29),
      I2 => s00_axis_tuser(1),
      I3 => \m00_axis_tdata_r_reg[31]_2\(29),
      I4 => s00_axis_tuser(0),
      I5 => \m00_axis_tdata_r_reg[31]_3\(29),
      O => \m00_axis_tdata_r[29]_i_5_n_0\
    );
\m00_axis_tdata_r[2]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => DOUT3_w(2),
      I1 => dout0_out(2),
      I2 => s00_axis_tuser(1),
      I3 => \m00_axis_tdata_r_reg[31]_2\(2),
      I4 => s00_axis_tuser(0),
      I5 => \m00_axis_tdata_r_reg[31]_3\(2),
      O => \m00_axis_tdata_r[2]_i_5_n_0\
    );
\m00_axis_tdata_r[30]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => DOUT3_w(30),
      I1 => dout0_out(30),
      I2 => s00_axis_tuser(1),
      I3 => \m00_axis_tdata_r_reg[31]_2\(30),
      I4 => s00_axis_tuser(0),
      I5 => \m00_axis_tdata_r_reg[31]_3\(30),
      O => \m00_axis_tdata_r[30]_i_5_n_0\
    );
\m00_axis_tdata_r[31]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFCA0FCAF0CA00CA"
    )
        port map (
      I0 => \m00_axis_tdata_r[31]_i_5_n_0\,
      I1 => \m00_axis_tdata_r_reg[31]\,
      I2 => s00_axis_tuser(2),
      I3 => s00_axis_tuser(3),
      I4 => \m00_axis_tdata_r_reg[31]_0\,
      I5 => \m00_axis_tdata_r_reg[31]_1\,
      O => D(0)
    );
\m00_axis_tdata_r[31]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => DOUT3_w(31),
      I1 => dout0_out(31),
      I2 => s00_axis_tuser(1),
      I3 => \m00_axis_tdata_r_reg[31]_2\(31),
      I4 => s00_axis_tuser(0),
      I5 => \m00_axis_tdata_r_reg[31]_3\(31),
      O => \m00_axis_tdata_r[31]_i_5_n_0\
    );
\m00_axis_tdata_r[3]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => DOUT3_w(3),
      I1 => dout0_out(3),
      I2 => s00_axis_tuser(1),
      I3 => \m00_axis_tdata_r_reg[31]_2\(3),
      I4 => s00_axis_tuser(0),
      I5 => \m00_axis_tdata_r_reg[31]_3\(3),
      O => \m00_axis_tdata_r[3]_i_5_n_0\
    );
\m00_axis_tdata_r[4]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => DOUT3_w(4),
      I1 => dout0_out(4),
      I2 => s00_axis_tuser(1),
      I3 => \m00_axis_tdata_r_reg[31]_2\(4),
      I4 => s00_axis_tuser(0),
      I5 => \m00_axis_tdata_r_reg[31]_3\(4),
      O => ram_reg_0_4
    );
\m00_axis_tdata_r[5]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => DOUT3_w(5),
      I1 => dout0_out(5),
      I2 => s00_axis_tuser(1),
      I3 => \m00_axis_tdata_r_reg[31]_2\(5),
      I4 => s00_axis_tuser(0),
      I5 => \m00_axis_tdata_r_reg[31]_3\(5),
      O => \m00_axis_tdata_r[5]_i_5_n_0\
    );
\m00_axis_tdata_r[6]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => DOUT3_w(6),
      I1 => dout0_out(6),
      I2 => s00_axis_tuser(1),
      I3 => \m00_axis_tdata_r_reg[31]_2\(6),
      I4 => s00_axis_tuser(0),
      I5 => \m00_axis_tdata_r_reg[31]_3\(6),
      O => \m00_axis_tdata_r[6]_i_5_n_0\
    );
\m00_axis_tdata_r[7]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => DOUT3_w(7),
      I1 => dout0_out(7),
      I2 => s00_axis_tuser(1),
      I3 => \m00_axis_tdata_r_reg[31]_2\(7),
      I4 => s00_axis_tuser(0),
      I5 => \m00_axis_tdata_r_reg[31]_3\(7),
      O => \m00_axis_tdata_r[7]_i_5_n_0\
    );
\m00_axis_tdata_r[8]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => DOUT3_w(8),
      I1 => dout0_out(8),
      I2 => s00_axis_tuser(1),
      I3 => \m00_axis_tdata_r_reg[31]_2\(8),
      I4 => s00_axis_tuser(0),
      I5 => \m00_axis_tdata_r_reg[31]_3\(8),
      O => \m00_axis_tdata_r[8]_i_5_n_0\
    );
\m00_axis_tdata_r[9]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => DOUT3_w(9),
      I1 => dout0_out(9),
      I2 => s00_axis_tuser(1),
      I3 => \m00_axis_tdata_r_reg[31]_2\(9),
      I4 => s00_axis_tuser(0),
      I5 => \m00_axis_tdata_r_reg[31]_3\(9),
      O => ram_reg_0_3
    );
\m00_axis_tdata_r_reg[10]_i_4\: unisim.vcomponents.MUXF7
     port map (
      I0 => \m00_axis_tdata_r[10]_i_5_n_0\,
      I1 => \m00_axis_tdata_r_reg[10]\,
      O => \s00_axis_tuser[18]_13\,
      S => s00_axis_tuser(2)
    );
\m00_axis_tdata_r_reg[12]_i_4\: unisim.vcomponents.MUXF7
     port map (
      I0 => \m00_axis_tdata_r[12]_i_5_n_0\,
      I1 => \m00_axis_tdata_r_reg[12]\,
      O => \s00_axis_tuser[18]_12\,
      S => s00_axis_tuser(2)
    );
\m00_axis_tdata_r_reg[13]_i_4\: unisim.vcomponents.MUXF7
     port map (
      I0 => \m00_axis_tdata_r[13]_i_5_n_0\,
      I1 => \m00_axis_tdata_r_reg[13]\,
      O => \s00_axis_tuser[18]_11\,
      S => s00_axis_tuser(2)
    );
\m00_axis_tdata_r_reg[14]_i_4\: unisim.vcomponents.MUXF7
     port map (
      I0 => \m00_axis_tdata_r[14]_i_5_n_0\,
      I1 => \m00_axis_tdata_r_reg[14]\,
      O => \s00_axis_tuser[18]_10\,
      S => s00_axis_tuser(2)
    );
\m00_axis_tdata_r_reg[17]_i_4\: unisim.vcomponents.MUXF7
     port map (
      I0 => \m00_axis_tdata_r[17]_i_5_n_0\,
      I1 => \m00_axis_tdata_r_reg[17]\,
      O => \s00_axis_tuser[18]_9\,
      S => s00_axis_tuser(2)
    );
\m00_axis_tdata_r_reg[18]_i_4\: unisim.vcomponents.MUXF7
     port map (
      I0 => \m00_axis_tdata_r[18]_i_5_n_0\,
      I1 => \m00_axis_tdata_r_reg[18]\,
      O => \s00_axis_tuser[18]_8\,
      S => s00_axis_tuser(2)
    );
\m00_axis_tdata_r_reg[19]_i_4\: unisim.vcomponents.MUXF7
     port map (
      I0 => \m00_axis_tdata_r[19]_i_5_n_0\,
      I1 => \m00_axis_tdata_r_reg[19]\,
      O => \s00_axis_tuser[18]_7\,
      S => s00_axis_tuser(2)
    );
\m00_axis_tdata_r_reg[1]_i_4\: unisim.vcomponents.MUXF7
     port map (
      I0 => \m00_axis_tdata_r[1]_i_5_n_0\,
      I1 => \m00_axis_tdata_r_reg[1]\,
      O => \s00_axis_tuser[18]_20\,
      S => s00_axis_tuser(2)
    );
\m00_axis_tdata_r_reg[21]_i_4\: unisim.vcomponents.MUXF7
     port map (
      I0 => \m00_axis_tdata_r[21]_i_5_n_0\,
      I1 => \m00_axis_tdata_r_reg[21]\,
      O => \s00_axis_tuser[18]_6\,
      S => s00_axis_tuser(2)
    );
\m00_axis_tdata_r_reg[22]_i_4\: unisim.vcomponents.MUXF7
     port map (
      I0 => \m00_axis_tdata_r[22]_i_5_n_0\,
      I1 => \m00_axis_tdata_r_reg[22]\,
      O => \s00_axis_tuser[18]_5\,
      S => s00_axis_tuser(2)
    );
\m00_axis_tdata_r_reg[23]_i_4\: unisim.vcomponents.MUXF7
     port map (
      I0 => \m00_axis_tdata_r[23]_i_5_n_0\,
      I1 => \m00_axis_tdata_r_reg[23]\,
      O => \s00_axis_tuser[18]_4\,
      S => s00_axis_tuser(2)
    );
\m00_axis_tdata_r_reg[24]_i_4\: unisim.vcomponents.MUXF7
     port map (
      I0 => \m00_axis_tdata_r[24]_i_5_n_0\,
      I1 => \m00_axis_tdata_r_reg[24]\,
      O => \s00_axis_tuser[18]_3\,
      S => s00_axis_tuser(2)
    );
\m00_axis_tdata_r_reg[26]_i_4\: unisim.vcomponents.MUXF7
     port map (
      I0 => \m00_axis_tdata_r[26]_i_5_n_0\,
      I1 => \m00_axis_tdata_r_reg[26]\,
      O => \s00_axis_tuser[18]_2\,
      S => s00_axis_tuser(2)
    );
\m00_axis_tdata_r_reg[28]_i_4\: unisim.vcomponents.MUXF7
     port map (
      I0 => \m00_axis_tdata_r[28]_i_5_n_0\,
      I1 => \m00_axis_tdata_r_reg[28]\,
      O => \s00_axis_tuser[18]_1\,
      S => s00_axis_tuser(2)
    );
\m00_axis_tdata_r_reg[29]_i_4\: unisim.vcomponents.MUXF7
     port map (
      I0 => \m00_axis_tdata_r[29]_i_5_n_0\,
      I1 => \m00_axis_tdata_r_reg[29]\,
      O => \s00_axis_tuser[18]_0\,
      S => s00_axis_tuser(2)
    );
\m00_axis_tdata_r_reg[2]_i_4\: unisim.vcomponents.MUXF7
     port map (
      I0 => \m00_axis_tdata_r[2]_i_5_n_0\,
      I1 => \m00_axis_tdata_r_reg[2]\,
      O => \s00_axis_tuser[18]_19\,
      S => s00_axis_tuser(2)
    );
\m00_axis_tdata_r_reg[30]_i_4\: unisim.vcomponents.MUXF7
     port map (
      I0 => \m00_axis_tdata_r[30]_i_5_n_0\,
      I1 => \m00_axis_tdata_r_reg[30]\,
      O => \s00_axis_tuser[18]\,
      S => s00_axis_tuser(2)
    );
\m00_axis_tdata_r_reg[3]_i_4\: unisim.vcomponents.MUXF7
     port map (
      I0 => \m00_axis_tdata_r[3]_i_5_n_0\,
      I1 => \m00_axis_tdata_r_reg[3]\,
      O => \s00_axis_tuser[18]_18\,
      S => s00_axis_tuser(2)
    );
\m00_axis_tdata_r_reg[5]_i_4\: unisim.vcomponents.MUXF7
     port map (
      I0 => \m00_axis_tdata_r[5]_i_5_n_0\,
      I1 => \m00_axis_tdata_r_reg[5]\,
      O => \s00_axis_tuser[18]_17\,
      S => s00_axis_tuser(2)
    );
\m00_axis_tdata_r_reg[6]_i_4\: unisim.vcomponents.MUXF7
     port map (
      I0 => \m00_axis_tdata_r[6]_i_5_n_0\,
      I1 => \m00_axis_tdata_r_reg[6]\,
      O => \s00_axis_tuser[18]_16\,
      S => s00_axis_tuser(2)
    );
\m00_axis_tdata_r_reg[7]_i_4\: unisim.vcomponents.MUXF7
     port map (
      I0 => \m00_axis_tdata_r[7]_i_5_n_0\,
      I1 => \m00_axis_tdata_r_reg[7]\,
      O => \s00_axis_tuser[18]_15\,
      S => s00_axis_tuser(2)
    );
\m00_axis_tdata_r_reg[8]_i_4\: unisim.vcomponents.MUXF7
     port map (
      I0 => \m00_axis_tdata_r[8]_i_5_n_0\,
      I1 => \m00_axis_tdata_r_reg[8]\,
      O => \s00_axis_tuser[18]_14\,
      S => s00_axis_tuser(2)
    );
ram_reg_0: unisim.vcomponents.RAMB36E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      EN_ECC_READ => false,
      EN_ECC_WRITE => false,
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      RAM_EXTENSION_A => "NONE",
      RAM_EXTENSION_B => "NONE",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "PERFORMANCE",
      READ_WIDTH_A => 18,
      READ_WIDTH_B => 0,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "READ_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 18,
      WRITE_WIDTH_B => 0
    )
        port map (
      ADDRARDADDR(15) => '1',
      ADDRARDADDR(14 downto 4) => ADDRARDADDR(10 downto 0),
      ADDRARDADDR(3 downto 0) => B"1111",
      ADDRBWRADDR(15 downto 0) => B"1111111111111111",
      CASCADEINA => '1',
      CASCADEINB => '0',
      CASCADEOUTA => NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED,
      CASCADEOUTB => NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED,
      CLKARDCLK => m00_axis_aclk,
      CLKBWRCLK => '0',
      DBITERR => NLW_ram_reg_0_DBITERR_UNCONNECTED,
      DIADI(31 downto 16) => B"0000000000000000",
      DIADI(15 downto 0) => Q(15 downto 0),
      DIBDI(31 downto 0) => B"11111111111111111111111111111111",
      DIPADIP(3 downto 2) => B"00",
      DIPADIP(1 downto 0) => Q(17 downto 16),
      DIPBDIP(3 downto 0) => B"1111",
      DOADO(31 downto 16) => NLW_ram_reg_0_DOADO_UNCONNECTED(31 downto 16),
      DOADO(15 downto 0) => DOUT3_w(15 downto 0),
      DOBDO(31 downto 0) => NLW_ram_reg_0_DOBDO_UNCONNECTED(31 downto 0),
      DOPADOP(3 downto 2) => NLW_ram_reg_0_DOPADOP_UNCONNECTED(3 downto 2),
      DOPADOP(1 downto 0) => DOUT3_w(17 downto 16),
      DOPBDOP(3 downto 0) => NLW_ram_reg_0_DOPBDOP_UNCONNECTED(3 downto 0),
      ECCPARITY(7 downto 0) => NLW_ram_reg_0_ECCPARITY_UNCONNECTED(7 downto 0),
      ENARDEN => en,
      ENBWREN => '0',
      INJECTDBITERR => NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED,
      INJECTSBITERR => NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED,
      RDADDRECC(8 downto 0) => NLW_ram_reg_0_RDADDRECC_UNCONNECTED(8 downto 0),
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      SBITERR => NLW_ram_reg_0_SBITERR_UNCONNECTED,
      WEA(3) => \ram_reg_0_i_1__2_n_0\,
      WEA(2) => \ram_reg_0_i_1__2_n_0\,
      WEA(1) => \ram_reg_0_i_1__2_n_0\,
      WEA(0) => \ram_reg_0_i_1__2_n_0\,
      WEBWE(7 downto 0) => B"00000000"
    );
\ram_reg_0_i_1__2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => en,
      I1 => WE_r,
      I2 => square_r,
      O => \ram_reg_0_i_1__2_n_0\
    );
ram_reg_1: unisim.vcomponents.RAMB36E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      EN_ECC_READ => false,
      EN_ECC_WRITE => false,
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      RAM_EXTENSION_A => "NONE",
      RAM_EXTENSION_B => "NONE",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "PERFORMANCE",
      READ_WIDTH_A => 18,
      READ_WIDTH_B => 0,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "READ_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 18,
      WRITE_WIDTH_B => 0
    )
        port map (
      ADDRARDADDR(15) => '1',
      ADDRARDADDR(14 downto 4) => addr(10 downto 0),
      ADDRARDADDR(3 downto 0) => B"1111",
      ADDRBWRADDR(15 downto 0) => B"1111111111111111",
      CASCADEINA => '1',
      CASCADEINB => '0',
      CASCADEOUTA => NLW_ram_reg_1_CASCADEOUTA_UNCONNECTED,
      CASCADEOUTB => NLW_ram_reg_1_CASCADEOUTB_UNCONNECTED,
      CLKARDCLK => m00_axis_aclk,
      CLKBWRCLK => '0',
      DBITERR => NLW_ram_reg_1_DBITERR_UNCONNECTED,
      DIADI(31 downto 14) => B"000000000000000000",
      DIADI(13 downto 0) => Q(31 downto 18),
      DIBDI(31 downto 0) => B"11111111111111111111111111111111",
      DIPADIP(3 downto 0) => B"0000",
      DIPBDIP(3 downto 0) => B"1111",
      DOADO(31 downto 14) => NLW_ram_reg_1_DOADO_UNCONNECTED(31 downto 14),
      DOADO(13 downto 0) => DOUT3_w(31 downto 18),
      DOBDO(31 downto 0) => NLW_ram_reg_1_DOBDO_UNCONNECTED(31 downto 0),
      DOPADOP(3 downto 0) => NLW_ram_reg_1_DOPADOP_UNCONNECTED(3 downto 0),
      DOPBDOP(3 downto 0) => NLW_ram_reg_1_DOPBDOP_UNCONNECTED(3 downto 0),
      ECCPARITY(7 downto 0) => NLW_ram_reg_1_ECCPARITY_UNCONNECTED(7 downto 0),
      ENARDEN => en,
      ENBWREN => '0',
      INJECTDBITERR => NLW_ram_reg_1_INJECTDBITERR_UNCONNECTED,
      INJECTSBITERR => NLW_ram_reg_1_INJECTSBITERR_UNCONNECTED,
      RDADDRECC(8 downto 0) => NLW_ram_reg_1_RDADDRECC_UNCONNECTED(8 downto 0),
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      SBITERR => NLW_ram_reg_1_SBITERR_UNCONNECTED,
      WEA(3) => \ram_reg_0_i_1__2_n_0\,
      WEA(2) => \ram_reg_0_i_1__2_n_0\,
      WEA(1) => \ram_reg_0_i_1__2_n_0\,
      WEA(0) => \ram_reg_0_i_1__2_n_0\,
      WEBWE(7 downto 0) => B"00000000"
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_3 is
  port (
    dout0_out : out STD_LOGIC_VECTOR ( 31 downto 0 );
    m00_axis_aclk : in STD_LOGIC;
    en : in STD_LOGIC;
    ADDRARDADDR : in STD_LOGIC_VECTOR ( 10 downto 0 );
    Q : in STD_LOGIC_VECTOR ( 31 downto 0 );
    addr : in STD_LOGIC_VECTOR ( 10 downto 0 );
    WE_r : in STD_LOGIC;
    square_r : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_3 : entity is "rams_sp_rf_rst";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_3;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_3 is
  signal \ram_reg_0_i_1__3_n_0\ : STD_LOGIC;
  signal NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_DBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_SBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_DOADO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 16 );
  signal NLW_ram_reg_0_DOBDO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_ram_reg_0_DOPADOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal NLW_ram_reg_0_DOPBDOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_ram_reg_0_ECCPARITY_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal NLW_ram_reg_0_RDADDRECC_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_ram_reg_1_CASCADEOUTA_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_1_CASCADEOUTB_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_1_DBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_1_INJECTDBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_1_INJECTSBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_1_SBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_1_DOADO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 14 );
  signal NLW_ram_reg_1_DOBDO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_ram_reg_1_DOPADOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_ram_reg_1_DOPBDOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_ram_reg_1_ECCPARITY_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal NLW_ram_reg_1_RDADDRECC_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ : string;
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of ram_reg_0 : label is "p2_d16";
  attribute METHODOLOGY_DRC_VIOS : string;
  attribute METHODOLOGY_DRC_VIOS of ram_reg_0 : label is "{SYNTH-6 {cell *THIS*}}";
  attribute RTL_RAM_BITS : integer;
  attribute RTL_RAM_BITS of ram_reg_0 : label is 61472;
  attribute RTL_RAM_NAME : string;
  attribute RTL_RAM_NAME of ram_reg_0 : label is "ram";
  attribute bram_addr_begin : integer;
  attribute bram_addr_begin of ram_reg_0 : label is 0;
  attribute bram_addr_end : integer;
  attribute bram_addr_end of ram_reg_0 : label is 2047;
  attribute bram_slice_begin : integer;
  attribute bram_slice_begin of ram_reg_0 : label is 0;
  attribute bram_slice_end : integer;
  attribute bram_slice_end of ram_reg_0 : label is 17;
  attribute ram_addr_begin : integer;
  attribute ram_addr_begin of ram_reg_0 : label is 0;
  attribute ram_addr_end : integer;
  attribute ram_addr_end of ram_reg_0 : label is 2047;
  attribute ram_offset : integer;
  attribute ram_offset of ram_reg_0 : label is 0;
  attribute ram_slice_begin : integer;
  attribute ram_slice_begin of ram_reg_0 : label is 0;
  attribute ram_slice_end : integer;
  attribute ram_slice_end of ram_reg_0 : label is 17;
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of ram_reg_1 : label is "p0_d14";
  attribute METHODOLOGY_DRC_VIOS of ram_reg_1 : label is "{SYNTH-6 {cell *THIS*}}";
  attribute RTL_RAM_BITS of ram_reg_1 : label is 61472;
  attribute RTL_RAM_NAME of ram_reg_1 : label is "ram";
  attribute bram_addr_begin of ram_reg_1 : label is 0;
  attribute bram_addr_end of ram_reg_1 : label is 2047;
  attribute bram_slice_begin of ram_reg_1 : label is 18;
  attribute bram_slice_end of ram_reg_1 : label is 31;
  attribute ram_addr_begin of ram_reg_1 : label is 0;
  attribute ram_addr_end of ram_reg_1 : label is 2047;
  attribute ram_offset of ram_reg_1 : label is 0;
  attribute ram_slice_begin of ram_reg_1 : label is 18;
  attribute ram_slice_end of ram_reg_1 : label is 31;
begin
ram_reg_0: unisim.vcomponents.RAMB36E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      EN_ECC_READ => false,
      EN_ECC_WRITE => false,
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      RAM_EXTENSION_A => "NONE",
      RAM_EXTENSION_B => "NONE",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "PERFORMANCE",
      READ_WIDTH_A => 18,
      READ_WIDTH_B => 0,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "READ_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 18,
      WRITE_WIDTH_B => 0
    )
        port map (
      ADDRARDADDR(15) => '1',
      ADDRARDADDR(14 downto 4) => ADDRARDADDR(10 downto 0),
      ADDRARDADDR(3 downto 0) => B"1111",
      ADDRBWRADDR(15 downto 0) => B"1111111111111111",
      CASCADEINA => '1',
      CASCADEINB => '0',
      CASCADEOUTA => NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED,
      CASCADEOUTB => NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED,
      CLKARDCLK => m00_axis_aclk,
      CLKBWRCLK => '0',
      DBITERR => NLW_ram_reg_0_DBITERR_UNCONNECTED,
      DIADI(31 downto 16) => B"0000000000000000",
      DIADI(15 downto 0) => Q(15 downto 0),
      DIBDI(31 downto 0) => B"11111111111111111111111111111111",
      DIPADIP(3 downto 2) => B"00",
      DIPADIP(1 downto 0) => Q(17 downto 16),
      DIPBDIP(3 downto 0) => B"1111",
      DOADO(31 downto 16) => NLW_ram_reg_0_DOADO_UNCONNECTED(31 downto 16),
      DOADO(15 downto 0) => dout0_out(15 downto 0),
      DOBDO(31 downto 0) => NLW_ram_reg_0_DOBDO_UNCONNECTED(31 downto 0),
      DOPADOP(3 downto 2) => NLW_ram_reg_0_DOPADOP_UNCONNECTED(3 downto 2),
      DOPADOP(1 downto 0) => dout0_out(17 downto 16),
      DOPBDOP(3 downto 0) => NLW_ram_reg_0_DOPBDOP_UNCONNECTED(3 downto 0),
      ECCPARITY(7 downto 0) => NLW_ram_reg_0_ECCPARITY_UNCONNECTED(7 downto 0),
      ENARDEN => en,
      ENBWREN => '0',
      INJECTDBITERR => NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED,
      INJECTSBITERR => NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED,
      RDADDRECC(8 downto 0) => NLW_ram_reg_0_RDADDRECC_UNCONNECTED(8 downto 0),
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      SBITERR => NLW_ram_reg_0_SBITERR_UNCONNECTED,
      WEA(3) => \ram_reg_0_i_1__3_n_0\,
      WEA(2) => \ram_reg_0_i_1__3_n_0\,
      WEA(1) => \ram_reg_0_i_1__3_n_0\,
      WEA(0) => \ram_reg_0_i_1__3_n_0\,
      WEBWE(7 downto 0) => B"00000000"
    );
\ram_reg_0_i_1__3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => en,
      I1 => WE_r,
      I2 => square_r,
      O => \ram_reg_0_i_1__3_n_0\
    );
ram_reg_1: unisim.vcomponents.RAMB36E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      EN_ECC_READ => false,
      EN_ECC_WRITE => false,
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      RAM_EXTENSION_A => "NONE",
      RAM_EXTENSION_B => "NONE",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "PERFORMANCE",
      READ_WIDTH_A => 18,
      READ_WIDTH_B => 0,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "READ_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 18,
      WRITE_WIDTH_B => 0
    )
        port map (
      ADDRARDADDR(15) => '1',
      ADDRARDADDR(14 downto 4) => addr(10 downto 0),
      ADDRARDADDR(3 downto 0) => B"1111",
      ADDRBWRADDR(15 downto 0) => B"1111111111111111",
      CASCADEINA => '1',
      CASCADEINB => '0',
      CASCADEOUTA => NLW_ram_reg_1_CASCADEOUTA_UNCONNECTED,
      CASCADEOUTB => NLW_ram_reg_1_CASCADEOUTB_UNCONNECTED,
      CLKARDCLK => m00_axis_aclk,
      CLKBWRCLK => '0',
      DBITERR => NLW_ram_reg_1_DBITERR_UNCONNECTED,
      DIADI(31 downto 14) => B"000000000000000000",
      DIADI(13 downto 0) => Q(31 downto 18),
      DIBDI(31 downto 0) => B"11111111111111111111111111111111",
      DIPADIP(3 downto 0) => B"0000",
      DIPBDIP(3 downto 0) => B"1111",
      DOADO(31 downto 14) => NLW_ram_reg_1_DOADO_UNCONNECTED(31 downto 14),
      DOADO(13 downto 0) => dout0_out(31 downto 18),
      DOBDO(31 downto 0) => NLW_ram_reg_1_DOBDO_UNCONNECTED(31 downto 0),
      DOPADOP(3 downto 0) => NLW_ram_reg_1_DOPADOP_UNCONNECTED(3 downto 0),
      DOPBDOP(3 downto 0) => NLW_ram_reg_1_DOPBDOP_UNCONNECTED(3 downto 0),
      ECCPARITY(7 downto 0) => NLW_ram_reg_1_ECCPARITY_UNCONNECTED(7 downto 0),
      ENARDEN => en,
      ENBWREN => '0',
      INJECTDBITERR => NLW_ram_reg_1_INJECTDBITERR_UNCONNECTED,
      INJECTSBITERR => NLW_ram_reg_1_INJECTSBITERR_UNCONNECTED,
      RDADDRECC(8 downto 0) => NLW_ram_reg_1_RDADDRECC_UNCONNECTED(8 downto 0),
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      SBITERR => NLW_ram_reg_1_SBITERR_UNCONNECTED,
      WEA(3) => \ram_reg_0_i_1__3_n_0\,
      WEA(2) => \ram_reg_0_i_1__3_n_0\,
      WEA(1) => \ram_reg_0_i_1__3_n_0\,
      WEA(0) => \ram_reg_0_i_1__3_n_0\,
      WEBWE(7 downto 0) => B"00000000"
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_4 is
  port (
    dout0_out : out STD_LOGIC_VECTOR ( 31 downto 0 );
    m00_axis_aclk : in STD_LOGIC;
    en : in STD_LOGIC;
    ADDRARDADDR : in STD_LOGIC_VECTOR ( 10 downto 0 );
    Q : in STD_LOGIC_VECTOR ( 31 downto 0 );
    addr : in STD_LOGIC_VECTOR ( 10 downto 0 );
    WE_r : in STD_LOGIC;
    square_r : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_4 : entity is "rams_sp_rf_rst";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_4;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_4 is
  signal \ram_reg_0_i_1__4_n_0\ : STD_LOGIC;
  signal NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_DBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_SBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_DOADO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 16 );
  signal NLW_ram_reg_0_DOBDO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_ram_reg_0_DOPADOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal NLW_ram_reg_0_DOPBDOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_ram_reg_0_ECCPARITY_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal NLW_ram_reg_0_RDADDRECC_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_ram_reg_1_CASCADEOUTA_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_1_CASCADEOUTB_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_1_DBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_1_INJECTDBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_1_INJECTSBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_1_SBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_1_DOADO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 14 );
  signal NLW_ram_reg_1_DOBDO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_ram_reg_1_DOPADOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_ram_reg_1_DOPBDOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_ram_reg_1_ECCPARITY_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal NLW_ram_reg_1_RDADDRECC_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ : string;
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of ram_reg_0 : label is "p2_d16";
  attribute METHODOLOGY_DRC_VIOS : string;
  attribute METHODOLOGY_DRC_VIOS of ram_reg_0 : label is "{SYNTH-6 {cell *THIS*}}";
  attribute RTL_RAM_BITS : integer;
  attribute RTL_RAM_BITS of ram_reg_0 : label is 61472;
  attribute RTL_RAM_NAME : string;
  attribute RTL_RAM_NAME of ram_reg_0 : label is "ram";
  attribute bram_addr_begin : integer;
  attribute bram_addr_begin of ram_reg_0 : label is 0;
  attribute bram_addr_end : integer;
  attribute bram_addr_end of ram_reg_0 : label is 2047;
  attribute bram_slice_begin : integer;
  attribute bram_slice_begin of ram_reg_0 : label is 0;
  attribute bram_slice_end : integer;
  attribute bram_slice_end of ram_reg_0 : label is 17;
  attribute ram_addr_begin : integer;
  attribute ram_addr_begin of ram_reg_0 : label is 0;
  attribute ram_addr_end : integer;
  attribute ram_addr_end of ram_reg_0 : label is 2047;
  attribute ram_offset : integer;
  attribute ram_offset of ram_reg_0 : label is 0;
  attribute ram_slice_begin : integer;
  attribute ram_slice_begin of ram_reg_0 : label is 0;
  attribute ram_slice_end : integer;
  attribute ram_slice_end of ram_reg_0 : label is 17;
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of ram_reg_1 : label is "p0_d14";
  attribute METHODOLOGY_DRC_VIOS of ram_reg_1 : label is "{SYNTH-6 {cell *THIS*}}";
  attribute RTL_RAM_BITS of ram_reg_1 : label is 61472;
  attribute RTL_RAM_NAME of ram_reg_1 : label is "ram";
  attribute bram_addr_begin of ram_reg_1 : label is 0;
  attribute bram_addr_end of ram_reg_1 : label is 2047;
  attribute bram_slice_begin of ram_reg_1 : label is 18;
  attribute bram_slice_end of ram_reg_1 : label is 31;
  attribute ram_addr_begin of ram_reg_1 : label is 0;
  attribute ram_addr_end of ram_reg_1 : label is 2047;
  attribute ram_offset of ram_reg_1 : label is 0;
  attribute ram_slice_begin of ram_reg_1 : label is 18;
  attribute ram_slice_end of ram_reg_1 : label is 31;
begin
ram_reg_0: unisim.vcomponents.RAMB36E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      EN_ECC_READ => false,
      EN_ECC_WRITE => false,
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      RAM_EXTENSION_A => "NONE",
      RAM_EXTENSION_B => "NONE",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "PERFORMANCE",
      READ_WIDTH_A => 18,
      READ_WIDTH_B => 0,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "READ_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 18,
      WRITE_WIDTH_B => 0
    )
        port map (
      ADDRARDADDR(15) => '1',
      ADDRARDADDR(14 downto 4) => ADDRARDADDR(10 downto 0),
      ADDRARDADDR(3 downto 0) => B"1111",
      ADDRBWRADDR(15 downto 0) => B"1111111111111111",
      CASCADEINA => '1',
      CASCADEINB => '0',
      CASCADEOUTA => NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED,
      CASCADEOUTB => NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED,
      CLKARDCLK => m00_axis_aclk,
      CLKBWRCLK => '0',
      DBITERR => NLW_ram_reg_0_DBITERR_UNCONNECTED,
      DIADI(31 downto 16) => B"0000000000000000",
      DIADI(15 downto 0) => Q(15 downto 0),
      DIBDI(31 downto 0) => B"11111111111111111111111111111111",
      DIPADIP(3 downto 2) => B"00",
      DIPADIP(1 downto 0) => Q(17 downto 16),
      DIPBDIP(3 downto 0) => B"1111",
      DOADO(31 downto 16) => NLW_ram_reg_0_DOADO_UNCONNECTED(31 downto 16),
      DOADO(15 downto 0) => dout0_out(15 downto 0),
      DOBDO(31 downto 0) => NLW_ram_reg_0_DOBDO_UNCONNECTED(31 downto 0),
      DOPADOP(3 downto 2) => NLW_ram_reg_0_DOPADOP_UNCONNECTED(3 downto 2),
      DOPADOP(1 downto 0) => dout0_out(17 downto 16),
      DOPBDOP(3 downto 0) => NLW_ram_reg_0_DOPBDOP_UNCONNECTED(3 downto 0),
      ECCPARITY(7 downto 0) => NLW_ram_reg_0_ECCPARITY_UNCONNECTED(7 downto 0),
      ENARDEN => en,
      ENBWREN => '0',
      INJECTDBITERR => NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED,
      INJECTSBITERR => NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED,
      RDADDRECC(8 downto 0) => NLW_ram_reg_0_RDADDRECC_UNCONNECTED(8 downto 0),
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      SBITERR => NLW_ram_reg_0_SBITERR_UNCONNECTED,
      WEA(3) => \ram_reg_0_i_1__4_n_0\,
      WEA(2) => \ram_reg_0_i_1__4_n_0\,
      WEA(1) => \ram_reg_0_i_1__4_n_0\,
      WEA(0) => \ram_reg_0_i_1__4_n_0\,
      WEBWE(7 downto 0) => B"00000000"
    );
\ram_reg_0_i_1__4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => en,
      I1 => WE_r,
      I2 => square_r,
      O => \ram_reg_0_i_1__4_n_0\
    );
ram_reg_1: unisim.vcomponents.RAMB36E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      EN_ECC_READ => false,
      EN_ECC_WRITE => false,
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      RAM_EXTENSION_A => "NONE",
      RAM_EXTENSION_B => "NONE",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "PERFORMANCE",
      READ_WIDTH_A => 18,
      READ_WIDTH_B => 0,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "READ_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 18,
      WRITE_WIDTH_B => 0
    )
        port map (
      ADDRARDADDR(15) => '1',
      ADDRARDADDR(14 downto 4) => addr(10 downto 0),
      ADDRARDADDR(3 downto 0) => B"1111",
      ADDRBWRADDR(15 downto 0) => B"1111111111111111",
      CASCADEINA => '1',
      CASCADEINB => '0',
      CASCADEOUTA => NLW_ram_reg_1_CASCADEOUTA_UNCONNECTED,
      CASCADEOUTB => NLW_ram_reg_1_CASCADEOUTB_UNCONNECTED,
      CLKARDCLK => m00_axis_aclk,
      CLKBWRCLK => '0',
      DBITERR => NLW_ram_reg_1_DBITERR_UNCONNECTED,
      DIADI(31 downto 14) => B"000000000000000000",
      DIADI(13 downto 0) => Q(31 downto 18),
      DIBDI(31 downto 0) => B"11111111111111111111111111111111",
      DIPADIP(3 downto 0) => B"0000",
      DIPBDIP(3 downto 0) => B"1111",
      DOADO(31 downto 14) => NLW_ram_reg_1_DOADO_UNCONNECTED(31 downto 14),
      DOADO(13 downto 0) => dout0_out(31 downto 18),
      DOBDO(31 downto 0) => NLW_ram_reg_1_DOBDO_UNCONNECTED(31 downto 0),
      DOPADOP(3 downto 0) => NLW_ram_reg_1_DOPADOP_UNCONNECTED(3 downto 0),
      DOPBDOP(3 downto 0) => NLW_ram_reg_1_DOPBDOP_UNCONNECTED(3 downto 0),
      ECCPARITY(7 downto 0) => NLW_ram_reg_1_ECCPARITY_UNCONNECTED(7 downto 0),
      ENARDEN => en,
      ENBWREN => '0',
      INJECTDBITERR => NLW_ram_reg_1_INJECTDBITERR_UNCONNECTED,
      INJECTSBITERR => NLW_ram_reg_1_INJECTSBITERR_UNCONNECTED,
      RDADDRECC(8 downto 0) => NLW_ram_reg_1_RDADDRECC_UNCONNECTED(8 downto 0),
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      SBITERR => NLW_ram_reg_1_SBITERR_UNCONNECTED,
      WEA(3) => \ram_reg_0_i_1__4_n_0\,
      WEA(2) => \ram_reg_0_i_1__4_n_0\,
      WEA(1) => \ram_reg_0_i_1__4_n_0\,
      WEA(0) => \ram_reg_0_i_1__4_n_0\,
      WEBWE(7 downto 0) => B"00000000"
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_5 is
  port (
    dout0_out : out STD_LOGIC_VECTOR ( 31 downto 0 );
    m00_axis_aclk : in STD_LOGIC;
    en : in STD_LOGIC;
    ADDRARDADDR : in STD_LOGIC_VECTOR ( 10 downto 0 );
    Q : in STD_LOGIC_VECTOR ( 31 downto 0 );
    addr : in STD_LOGIC_VECTOR ( 10 downto 0 );
    WE_r : in STD_LOGIC;
    square_r : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_5 : entity is "rams_sp_rf_rst";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_5;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_5 is
  signal \ram_reg_0_i_1__5_n_0\ : STD_LOGIC;
  signal NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_DBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_SBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_DOADO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 16 );
  signal NLW_ram_reg_0_DOBDO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_ram_reg_0_DOPADOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal NLW_ram_reg_0_DOPBDOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_ram_reg_0_ECCPARITY_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal NLW_ram_reg_0_RDADDRECC_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_ram_reg_1_CASCADEOUTA_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_1_CASCADEOUTB_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_1_DBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_1_INJECTDBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_1_INJECTSBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_1_SBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_1_DOADO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 14 );
  signal NLW_ram_reg_1_DOBDO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_ram_reg_1_DOPADOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_ram_reg_1_DOPBDOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_ram_reg_1_ECCPARITY_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal NLW_ram_reg_1_RDADDRECC_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ : string;
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of ram_reg_0 : label is "p2_d16";
  attribute METHODOLOGY_DRC_VIOS : string;
  attribute METHODOLOGY_DRC_VIOS of ram_reg_0 : label is "{SYNTH-6 {cell *THIS*}}";
  attribute RTL_RAM_BITS : integer;
  attribute RTL_RAM_BITS of ram_reg_0 : label is 61472;
  attribute RTL_RAM_NAME : string;
  attribute RTL_RAM_NAME of ram_reg_0 : label is "ram";
  attribute bram_addr_begin : integer;
  attribute bram_addr_begin of ram_reg_0 : label is 0;
  attribute bram_addr_end : integer;
  attribute bram_addr_end of ram_reg_0 : label is 2047;
  attribute bram_slice_begin : integer;
  attribute bram_slice_begin of ram_reg_0 : label is 0;
  attribute bram_slice_end : integer;
  attribute bram_slice_end of ram_reg_0 : label is 17;
  attribute ram_addr_begin : integer;
  attribute ram_addr_begin of ram_reg_0 : label is 0;
  attribute ram_addr_end : integer;
  attribute ram_addr_end of ram_reg_0 : label is 2047;
  attribute ram_offset : integer;
  attribute ram_offset of ram_reg_0 : label is 0;
  attribute ram_slice_begin : integer;
  attribute ram_slice_begin of ram_reg_0 : label is 0;
  attribute ram_slice_end : integer;
  attribute ram_slice_end of ram_reg_0 : label is 17;
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of ram_reg_1 : label is "p0_d14";
  attribute METHODOLOGY_DRC_VIOS of ram_reg_1 : label is "{SYNTH-6 {cell *THIS*}}";
  attribute RTL_RAM_BITS of ram_reg_1 : label is 61472;
  attribute RTL_RAM_NAME of ram_reg_1 : label is "ram";
  attribute bram_addr_begin of ram_reg_1 : label is 0;
  attribute bram_addr_end of ram_reg_1 : label is 2047;
  attribute bram_slice_begin of ram_reg_1 : label is 18;
  attribute bram_slice_end of ram_reg_1 : label is 31;
  attribute ram_addr_begin of ram_reg_1 : label is 0;
  attribute ram_addr_end of ram_reg_1 : label is 2047;
  attribute ram_offset of ram_reg_1 : label is 0;
  attribute ram_slice_begin of ram_reg_1 : label is 18;
  attribute ram_slice_end of ram_reg_1 : label is 31;
begin
ram_reg_0: unisim.vcomponents.RAMB36E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      EN_ECC_READ => false,
      EN_ECC_WRITE => false,
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      RAM_EXTENSION_A => "NONE",
      RAM_EXTENSION_B => "NONE",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "PERFORMANCE",
      READ_WIDTH_A => 18,
      READ_WIDTH_B => 0,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "READ_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 18,
      WRITE_WIDTH_B => 0
    )
        port map (
      ADDRARDADDR(15) => '1',
      ADDRARDADDR(14 downto 4) => ADDRARDADDR(10 downto 0),
      ADDRARDADDR(3 downto 0) => B"1111",
      ADDRBWRADDR(15 downto 0) => B"1111111111111111",
      CASCADEINA => '1',
      CASCADEINB => '0',
      CASCADEOUTA => NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED,
      CASCADEOUTB => NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED,
      CLKARDCLK => m00_axis_aclk,
      CLKBWRCLK => '0',
      DBITERR => NLW_ram_reg_0_DBITERR_UNCONNECTED,
      DIADI(31 downto 16) => B"0000000000000000",
      DIADI(15 downto 0) => Q(15 downto 0),
      DIBDI(31 downto 0) => B"11111111111111111111111111111111",
      DIPADIP(3 downto 2) => B"00",
      DIPADIP(1 downto 0) => Q(17 downto 16),
      DIPBDIP(3 downto 0) => B"1111",
      DOADO(31 downto 16) => NLW_ram_reg_0_DOADO_UNCONNECTED(31 downto 16),
      DOADO(15 downto 0) => dout0_out(15 downto 0),
      DOBDO(31 downto 0) => NLW_ram_reg_0_DOBDO_UNCONNECTED(31 downto 0),
      DOPADOP(3 downto 2) => NLW_ram_reg_0_DOPADOP_UNCONNECTED(3 downto 2),
      DOPADOP(1 downto 0) => dout0_out(17 downto 16),
      DOPBDOP(3 downto 0) => NLW_ram_reg_0_DOPBDOP_UNCONNECTED(3 downto 0),
      ECCPARITY(7 downto 0) => NLW_ram_reg_0_ECCPARITY_UNCONNECTED(7 downto 0),
      ENARDEN => en,
      ENBWREN => '0',
      INJECTDBITERR => NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED,
      INJECTSBITERR => NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED,
      RDADDRECC(8 downto 0) => NLW_ram_reg_0_RDADDRECC_UNCONNECTED(8 downto 0),
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      SBITERR => NLW_ram_reg_0_SBITERR_UNCONNECTED,
      WEA(3) => \ram_reg_0_i_1__5_n_0\,
      WEA(2) => \ram_reg_0_i_1__5_n_0\,
      WEA(1) => \ram_reg_0_i_1__5_n_0\,
      WEA(0) => \ram_reg_0_i_1__5_n_0\,
      WEBWE(7 downto 0) => B"00000000"
    );
\ram_reg_0_i_1__5\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => en,
      I1 => WE_r,
      I2 => square_r,
      O => \ram_reg_0_i_1__5_n_0\
    );
ram_reg_1: unisim.vcomponents.RAMB36E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      EN_ECC_READ => false,
      EN_ECC_WRITE => false,
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      RAM_EXTENSION_A => "NONE",
      RAM_EXTENSION_B => "NONE",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "PERFORMANCE",
      READ_WIDTH_A => 18,
      READ_WIDTH_B => 0,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "READ_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 18,
      WRITE_WIDTH_B => 0
    )
        port map (
      ADDRARDADDR(15) => '1',
      ADDRARDADDR(14 downto 4) => addr(10 downto 0),
      ADDRARDADDR(3 downto 0) => B"1111",
      ADDRBWRADDR(15 downto 0) => B"1111111111111111",
      CASCADEINA => '1',
      CASCADEINB => '0',
      CASCADEOUTA => NLW_ram_reg_1_CASCADEOUTA_UNCONNECTED,
      CASCADEOUTB => NLW_ram_reg_1_CASCADEOUTB_UNCONNECTED,
      CLKARDCLK => m00_axis_aclk,
      CLKBWRCLK => '0',
      DBITERR => NLW_ram_reg_1_DBITERR_UNCONNECTED,
      DIADI(31 downto 14) => B"000000000000000000",
      DIADI(13 downto 0) => Q(31 downto 18),
      DIBDI(31 downto 0) => B"11111111111111111111111111111111",
      DIPADIP(3 downto 0) => B"0000",
      DIPBDIP(3 downto 0) => B"1111",
      DOADO(31 downto 14) => NLW_ram_reg_1_DOADO_UNCONNECTED(31 downto 14),
      DOADO(13 downto 0) => dout0_out(31 downto 18),
      DOBDO(31 downto 0) => NLW_ram_reg_1_DOBDO_UNCONNECTED(31 downto 0),
      DOPADOP(3 downto 0) => NLW_ram_reg_1_DOPADOP_UNCONNECTED(3 downto 0),
      DOPBDOP(3 downto 0) => NLW_ram_reg_1_DOPBDOP_UNCONNECTED(3 downto 0),
      ECCPARITY(7 downto 0) => NLW_ram_reg_1_ECCPARITY_UNCONNECTED(7 downto 0),
      ENARDEN => en,
      ENBWREN => '0',
      INJECTDBITERR => NLW_ram_reg_1_INJECTDBITERR_UNCONNECTED,
      INJECTSBITERR => NLW_ram_reg_1_INJECTSBITERR_UNCONNECTED,
      RDADDRECC(8 downto 0) => NLW_ram_reg_1_RDADDRECC_UNCONNECTED(8 downto 0),
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      SBITERR => NLW_ram_reg_1_SBITERR_UNCONNECTED,
      WEA(3) => \ram_reg_0_i_1__5_n_0\,
      WEA(2) => \ram_reg_0_i_1__5_n_0\,
      WEA(1) => \ram_reg_0_i_1__5_n_0\,
      WEA(0) => \ram_reg_0_i_1__5_n_0\,
      WEBWE(7 downto 0) => B"00000000"
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_6 is
  port (
    D : out STD_LOGIC_VECTOR ( 8 downto 0 );
    ram_reg_1_0 : out STD_LOGIC;
    ram_reg_1_1 : out STD_LOGIC;
    ram_reg_1_2 : out STD_LOGIC;
    ram_reg_1_3 : out STD_LOGIC;
    ram_reg_1_4 : out STD_LOGIC;
    ram_reg_1_5 : out STD_LOGIC;
    ram_reg_1_6 : out STD_LOGIC;
    ram_reg_1_7 : out STD_LOGIC;
    ram_reg_1_8 : out STD_LOGIC;
    ram_reg_1_9 : out STD_LOGIC;
    ram_reg_1_10 : out STD_LOGIC;
    ram_reg_0_0 : out STD_LOGIC;
    ram_reg_0_1 : out STD_LOGIC;
    ram_reg_0_2 : out STD_LOGIC;
    ram_reg_0_3 : out STD_LOGIC;
    ram_reg_0_4 : out STD_LOGIC;
    ram_reg_0_5 : out STD_LOGIC;
    ram_reg_0_6 : out STD_LOGIC;
    ram_reg_0_7 : out STD_LOGIC;
    ram_reg_0_8 : out STD_LOGIC;
    ram_reg_0_9 : out STD_LOGIC;
    ram_reg_0_10 : out STD_LOGIC;
    ram_reg_0_11 : out STD_LOGIC;
    s00_axis_tuser : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \m00_axis_tdata_r_reg[27]\ : in STD_LOGIC;
    \m00_axis_tdata_r_reg[27]_0\ : in STD_LOGIC;
    \m00_axis_tdata_r_reg[25]\ : in STD_LOGIC;
    \m00_axis_tdata_r_reg[25]_0\ : in STD_LOGIC;
    \m00_axis_tdata_r_reg[20]\ : in STD_LOGIC;
    \m00_axis_tdata_r_reg[20]_0\ : in STD_LOGIC;
    \m00_axis_tdata_r_reg[16]\ : in STD_LOGIC;
    \m00_axis_tdata_r_reg[16]_0\ : in STD_LOGIC;
    \m00_axis_tdata_r_reg[15]\ : in STD_LOGIC;
    \m00_axis_tdata_r_reg[15]_0\ : in STD_LOGIC;
    \m00_axis_tdata_r_reg[11]\ : in STD_LOGIC;
    \m00_axis_tdata_r_reg[11]_0\ : in STD_LOGIC;
    \m00_axis_tdata_r_reg[9]\ : in STD_LOGIC;
    \m00_axis_tdata_r_reg[9]_0\ : in STD_LOGIC;
    \m00_axis_tdata_r_reg[4]\ : in STD_LOGIC;
    \m00_axis_tdata_r_reg[4]_0\ : in STD_LOGIC;
    \m00_axis_tdata_r_reg[0]\ : in STD_LOGIC;
    \m00_axis_tdata_r_reg[0]_0\ : in STD_LOGIC;
    dout0_out : in STD_LOGIC_VECTOR ( 31 downto 0 );
    \m00_axis_tdata_r_reg[31]\ : in STD_LOGIC_VECTOR ( 31 downto 0 );
    \m00_axis_tdata_r_reg[31]_0\ : in STD_LOGIC_VECTOR ( 31 downto 0 );
    m00_axis_aclk : in STD_LOGIC;
    en : in STD_LOGIC;
    ADDRARDADDR : in STD_LOGIC_VECTOR ( 10 downto 0 );
    Q : in STD_LOGIC_VECTOR ( 31 downto 0 );
    addr : in STD_LOGIC_VECTOR ( 10 downto 0 );
    WE_r : in STD_LOGIC;
    square_r : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_6 : entity is "rams_sp_rf_rst";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_6;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_6 is
  signal DOUT7_w : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \m00_axis_tdata_r[0]_i_2_n_0\ : STD_LOGIC;
  signal \m00_axis_tdata_r[11]_i_2_n_0\ : STD_LOGIC;
  signal \m00_axis_tdata_r[15]_i_2_n_0\ : STD_LOGIC;
  signal \m00_axis_tdata_r[16]_i_2_n_0\ : STD_LOGIC;
  signal \m00_axis_tdata_r[20]_i_2_n_0\ : STD_LOGIC;
  signal \m00_axis_tdata_r[25]_i_2_n_0\ : STD_LOGIC;
  signal \m00_axis_tdata_r[27]_i_2_n_0\ : STD_LOGIC;
  signal \m00_axis_tdata_r[4]_i_2_n_0\ : STD_LOGIC;
  signal \m00_axis_tdata_r[9]_i_2_n_0\ : STD_LOGIC;
  signal \ram_reg_0_i_1__6_n_0\ : STD_LOGIC;
  signal NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_DBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_SBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_DOADO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 16 );
  signal NLW_ram_reg_0_DOBDO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_ram_reg_0_DOPADOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal NLW_ram_reg_0_DOPBDOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_ram_reg_0_ECCPARITY_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal NLW_ram_reg_0_RDADDRECC_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_ram_reg_1_CASCADEOUTA_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_1_CASCADEOUTB_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_1_DBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_1_INJECTDBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_1_INJECTSBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_1_SBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_1_DOADO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 14 );
  signal NLW_ram_reg_1_DOBDO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_ram_reg_1_DOPADOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_ram_reg_1_DOPBDOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_ram_reg_1_ECCPARITY_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal NLW_ram_reg_1_RDADDRECC_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ : string;
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of ram_reg_0 : label is "p2_d16";
  attribute METHODOLOGY_DRC_VIOS : string;
  attribute METHODOLOGY_DRC_VIOS of ram_reg_0 : label is "{SYNTH-6 {cell *THIS*}}";
  attribute RTL_RAM_BITS : integer;
  attribute RTL_RAM_BITS of ram_reg_0 : label is 61472;
  attribute RTL_RAM_NAME : string;
  attribute RTL_RAM_NAME of ram_reg_0 : label is "ram";
  attribute bram_addr_begin : integer;
  attribute bram_addr_begin of ram_reg_0 : label is 0;
  attribute bram_addr_end : integer;
  attribute bram_addr_end of ram_reg_0 : label is 2047;
  attribute bram_slice_begin : integer;
  attribute bram_slice_begin of ram_reg_0 : label is 0;
  attribute bram_slice_end : integer;
  attribute bram_slice_end of ram_reg_0 : label is 17;
  attribute ram_addr_begin : integer;
  attribute ram_addr_begin of ram_reg_0 : label is 0;
  attribute ram_addr_end : integer;
  attribute ram_addr_end of ram_reg_0 : label is 2047;
  attribute ram_offset : integer;
  attribute ram_offset of ram_reg_0 : label is 0;
  attribute ram_slice_begin : integer;
  attribute ram_slice_begin of ram_reg_0 : label is 0;
  attribute ram_slice_end : integer;
  attribute ram_slice_end of ram_reg_0 : label is 17;
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of ram_reg_1 : label is "p0_d14";
  attribute METHODOLOGY_DRC_VIOS of ram_reg_1 : label is "{SYNTH-6 {cell *THIS*}}";
  attribute RTL_RAM_BITS of ram_reg_1 : label is 61472;
  attribute RTL_RAM_NAME of ram_reg_1 : label is "ram";
  attribute bram_addr_begin of ram_reg_1 : label is 0;
  attribute bram_addr_end of ram_reg_1 : label is 2047;
  attribute bram_slice_begin of ram_reg_1 : label is 18;
  attribute bram_slice_end of ram_reg_1 : label is 31;
  attribute ram_addr_begin of ram_reg_1 : label is 0;
  attribute ram_addr_end of ram_reg_1 : label is 2047;
  attribute ram_offset of ram_reg_1 : label is 0;
  attribute ram_slice_begin of ram_reg_1 : label is 18;
  attribute ram_slice_end of ram_reg_1 : label is 31;
begin
\m00_axis_tdata_r[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EFEA4540"
    )
        port map (
      I0 => s00_axis_tuser(3),
      I1 => \m00_axis_tdata_r[0]_i_2_n_0\,
      I2 => s00_axis_tuser(2),
      I3 => \m00_axis_tdata_r_reg[0]\,
      I4 => \m00_axis_tdata_r_reg[0]_0\,
      O => D(0)
    );
\m00_axis_tdata_r[0]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => DOUT7_w(0),
      I1 => dout0_out(0),
      I2 => s00_axis_tuser(1),
      I3 => \m00_axis_tdata_r_reg[31]\(0),
      I4 => s00_axis_tuser(0),
      I5 => \m00_axis_tdata_r_reg[31]_0\(0),
      O => \m00_axis_tdata_r[0]_i_2_n_0\
    );
\m00_axis_tdata_r[10]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => DOUT7_w(10),
      I1 => dout0_out(10),
      I2 => s00_axis_tuser(1),
      I3 => \m00_axis_tdata_r_reg[31]\(10),
      I4 => s00_axis_tuser(0),
      I5 => \m00_axis_tdata_r_reg[31]_0\(10),
      O => ram_reg_0_4
    );
\m00_axis_tdata_r[11]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EFEA4540"
    )
        port map (
      I0 => s00_axis_tuser(3),
      I1 => \m00_axis_tdata_r[11]_i_2_n_0\,
      I2 => s00_axis_tuser(2),
      I3 => \m00_axis_tdata_r_reg[11]\,
      I4 => \m00_axis_tdata_r_reg[11]_0\,
      O => D(3)
    );
\m00_axis_tdata_r[11]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => DOUT7_w(11),
      I1 => dout0_out(11),
      I2 => s00_axis_tuser(1),
      I3 => \m00_axis_tdata_r_reg[31]\(11),
      I4 => s00_axis_tuser(0),
      I5 => \m00_axis_tdata_r_reg[31]_0\(11),
      O => \m00_axis_tdata_r[11]_i_2_n_0\
    );
\m00_axis_tdata_r[12]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => DOUT7_w(12),
      I1 => dout0_out(12),
      I2 => s00_axis_tuser(1),
      I3 => \m00_axis_tdata_r_reg[31]\(12),
      I4 => s00_axis_tuser(0),
      I5 => \m00_axis_tdata_r_reg[31]_0\(12),
      O => ram_reg_0_3
    );
\m00_axis_tdata_r[13]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => DOUT7_w(13),
      I1 => dout0_out(13),
      I2 => s00_axis_tuser(1),
      I3 => \m00_axis_tdata_r_reg[31]\(13),
      I4 => s00_axis_tuser(0),
      I5 => \m00_axis_tdata_r_reg[31]_0\(13),
      O => ram_reg_0_2
    );
\m00_axis_tdata_r[14]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => DOUT7_w(14),
      I1 => dout0_out(14),
      I2 => s00_axis_tuser(1),
      I3 => \m00_axis_tdata_r_reg[31]\(14),
      I4 => s00_axis_tuser(0),
      I5 => \m00_axis_tdata_r_reg[31]_0\(14),
      O => ram_reg_0_1
    );
\m00_axis_tdata_r[15]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EFEA4540"
    )
        port map (
      I0 => s00_axis_tuser(3),
      I1 => \m00_axis_tdata_r[15]_i_2_n_0\,
      I2 => s00_axis_tuser(2),
      I3 => \m00_axis_tdata_r_reg[15]\,
      I4 => \m00_axis_tdata_r_reg[15]_0\,
      O => D(4)
    );
\m00_axis_tdata_r[15]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => DOUT7_w(15),
      I1 => dout0_out(15),
      I2 => s00_axis_tuser(1),
      I3 => \m00_axis_tdata_r_reg[31]\(15),
      I4 => s00_axis_tuser(0),
      I5 => \m00_axis_tdata_r_reg[31]_0\(15),
      O => \m00_axis_tdata_r[15]_i_2_n_0\
    );
\m00_axis_tdata_r[16]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EFEA4540"
    )
        port map (
      I0 => s00_axis_tuser(3),
      I1 => \m00_axis_tdata_r[16]_i_2_n_0\,
      I2 => s00_axis_tuser(2),
      I3 => \m00_axis_tdata_r_reg[16]\,
      I4 => \m00_axis_tdata_r_reg[16]_0\,
      O => D(5)
    );
\m00_axis_tdata_r[16]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => DOUT7_w(16),
      I1 => dout0_out(16),
      I2 => s00_axis_tuser(1),
      I3 => \m00_axis_tdata_r_reg[31]\(16),
      I4 => s00_axis_tuser(0),
      I5 => \m00_axis_tdata_r_reg[31]_0\(16),
      O => \m00_axis_tdata_r[16]_i_2_n_0\
    );
\m00_axis_tdata_r[17]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => DOUT7_w(17),
      I1 => dout0_out(17),
      I2 => s00_axis_tuser(1),
      I3 => \m00_axis_tdata_r_reg[31]\(17),
      I4 => s00_axis_tuser(0),
      I5 => \m00_axis_tdata_r_reg[31]_0\(17),
      O => ram_reg_0_0
    );
\m00_axis_tdata_r[18]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => DOUT7_w(18),
      I1 => dout0_out(18),
      I2 => s00_axis_tuser(1),
      I3 => \m00_axis_tdata_r_reg[31]\(18),
      I4 => s00_axis_tuser(0),
      I5 => \m00_axis_tdata_r_reg[31]_0\(18),
      O => ram_reg_1_10
    );
\m00_axis_tdata_r[19]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => DOUT7_w(19),
      I1 => dout0_out(19),
      I2 => s00_axis_tuser(1),
      I3 => \m00_axis_tdata_r_reg[31]\(19),
      I4 => s00_axis_tuser(0),
      I5 => \m00_axis_tdata_r_reg[31]_0\(19),
      O => ram_reg_1_9
    );
\m00_axis_tdata_r[1]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => DOUT7_w(1),
      I1 => dout0_out(1),
      I2 => s00_axis_tuser(1),
      I3 => \m00_axis_tdata_r_reg[31]\(1),
      I4 => s00_axis_tuser(0),
      I5 => \m00_axis_tdata_r_reg[31]_0\(1),
      O => ram_reg_0_11
    );
\m00_axis_tdata_r[20]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EFEA4540"
    )
        port map (
      I0 => s00_axis_tuser(3),
      I1 => \m00_axis_tdata_r[20]_i_2_n_0\,
      I2 => s00_axis_tuser(2),
      I3 => \m00_axis_tdata_r_reg[20]\,
      I4 => \m00_axis_tdata_r_reg[20]_0\,
      O => D(6)
    );
\m00_axis_tdata_r[20]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => DOUT7_w(20),
      I1 => dout0_out(20),
      I2 => s00_axis_tuser(1),
      I3 => \m00_axis_tdata_r_reg[31]\(20),
      I4 => s00_axis_tuser(0),
      I5 => \m00_axis_tdata_r_reg[31]_0\(20),
      O => \m00_axis_tdata_r[20]_i_2_n_0\
    );
\m00_axis_tdata_r[21]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => DOUT7_w(21),
      I1 => dout0_out(21),
      I2 => s00_axis_tuser(1),
      I3 => \m00_axis_tdata_r_reg[31]\(21),
      I4 => s00_axis_tuser(0),
      I5 => \m00_axis_tdata_r_reg[31]_0\(21),
      O => ram_reg_1_8
    );
\m00_axis_tdata_r[22]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => DOUT7_w(22),
      I1 => dout0_out(22),
      I2 => s00_axis_tuser(1),
      I3 => \m00_axis_tdata_r_reg[31]\(22),
      I4 => s00_axis_tuser(0),
      I5 => \m00_axis_tdata_r_reg[31]_0\(22),
      O => ram_reg_1_7
    );
\m00_axis_tdata_r[23]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => DOUT7_w(23),
      I1 => dout0_out(23),
      I2 => s00_axis_tuser(1),
      I3 => \m00_axis_tdata_r_reg[31]\(23),
      I4 => s00_axis_tuser(0),
      I5 => \m00_axis_tdata_r_reg[31]_0\(23),
      O => ram_reg_1_6
    );
\m00_axis_tdata_r[24]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => DOUT7_w(24),
      I1 => dout0_out(24),
      I2 => s00_axis_tuser(1),
      I3 => \m00_axis_tdata_r_reg[31]\(24),
      I4 => s00_axis_tuser(0),
      I5 => \m00_axis_tdata_r_reg[31]_0\(24),
      O => ram_reg_1_5
    );
\m00_axis_tdata_r[25]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EFEA4540"
    )
        port map (
      I0 => s00_axis_tuser(3),
      I1 => \m00_axis_tdata_r[25]_i_2_n_0\,
      I2 => s00_axis_tuser(2),
      I3 => \m00_axis_tdata_r_reg[25]\,
      I4 => \m00_axis_tdata_r_reg[25]_0\,
      O => D(7)
    );
\m00_axis_tdata_r[25]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => DOUT7_w(25),
      I1 => dout0_out(25),
      I2 => s00_axis_tuser(1),
      I3 => \m00_axis_tdata_r_reg[31]\(25),
      I4 => s00_axis_tuser(0),
      I5 => \m00_axis_tdata_r_reg[31]_0\(25),
      O => \m00_axis_tdata_r[25]_i_2_n_0\
    );
\m00_axis_tdata_r[26]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => DOUT7_w(26),
      I1 => dout0_out(26),
      I2 => s00_axis_tuser(1),
      I3 => \m00_axis_tdata_r_reg[31]\(26),
      I4 => s00_axis_tuser(0),
      I5 => \m00_axis_tdata_r_reg[31]_0\(26),
      O => ram_reg_1_4
    );
\m00_axis_tdata_r[27]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EFEA4540"
    )
        port map (
      I0 => s00_axis_tuser(3),
      I1 => \m00_axis_tdata_r[27]_i_2_n_0\,
      I2 => s00_axis_tuser(2),
      I3 => \m00_axis_tdata_r_reg[27]\,
      I4 => \m00_axis_tdata_r_reg[27]_0\,
      O => D(8)
    );
\m00_axis_tdata_r[27]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => DOUT7_w(27),
      I1 => dout0_out(27),
      I2 => s00_axis_tuser(1),
      I3 => \m00_axis_tdata_r_reg[31]\(27),
      I4 => s00_axis_tuser(0),
      I5 => \m00_axis_tdata_r_reg[31]_0\(27),
      O => \m00_axis_tdata_r[27]_i_2_n_0\
    );
\m00_axis_tdata_r[28]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => DOUT7_w(28),
      I1 => dout0_out(28),
      I2 => s00_axis_tuser(1),
      I3 => \m00_axis_tdata_r_reg[31]\(28),
      I4 => s00_axis_tuser(0),
      I5 => \m00_axis_tdata_r_reg[31]_0\(28),
      O => ram_reg_1_3
    );
\m00_axis_tdata_r[29]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => DOUT7_w(29),
      I1 => dout0_out(29),
      I2 => s00_axis_tuser(1),
      I3 => \m00_axis_tdata_r_reg[31]\(29),
      I4 => s00_axis_tuser(0),
      I5 => \m00_axis_tdata_r_reg[31]_0\(29),
      O => ram_reg_1_2
    );
\m00_axis_tdata_r[2]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => DOUT7_w(2),
      I1 => dout0_out(2),
      I2 => s00_axis_tuser(1),
      I3 => \m00_axis_tdata_r_reg[31]\(2),
      I4 => s00_axis_tuser(0),
      I5 => \m00_axis_tdata_r_reg[31]_0\(2),
      O => ram_reg_0_10
    );
\m00_axis_tdata_r[30]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => DOUT7_w(30),
      I1 => dout0_out(30),
      I2 => s00_axis_tuser(1),
      I3 => \m00_axis_tdata_r_reg[31]\(30),
      I4 => s00_axis_tuser(0),
      I5 => \m00_axis_tdata_r_reg[31]_0\(30),
      O => ram_reg_1_1
    );
\m00_axis_tdata_r[31]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => DOUT7_w(31),
      I1 => dout0_out(31),
      I2 => s00_axis_tuser(1),
      I3 => \m00_axis_tdata_r_reg[31]\(31),
      I4 => s00_axis_tuser(0),
      I5 => \m00_axis_tdata_r_reg[31]_0\(31),
      O => ram_reg_1_0
    );
\m00_axis_tdata_r[3]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => DOUT7_w(3),
      I1 => dout0_out(3),
      I2 => s00_axis_tuser(1),
      I3 => \m00_axis_tdata_r_reg[31]\(3),
      I4 => s00_axis_tuser(0),
      I5 => \m00_axis_tdata_r_reg[31]_0\(3),
      O => ram_reg_0_9
    );
\m00_axis_tdata_r[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EFEA4540"
    )
        port map (
      I0 => s00_axis_tuser(3),
      I1 => \m00_axis_tdata_r[4]_i_2_n_0\,
      I2 => s00_axis_tuser(2),
      I3 => \m00_axis_tdata_r_reg[4]\,
      I4 => \m00_axis_tdata_r_reg[4]_0\,
      O => D(1)
    );
\m00_axis_tdata_r[4]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => DOUT7_w(4),
      I1 => dout0_out(4),
      I2 => s00_axis_tuser(1),
      I3 => \m00_axis_tdata_r_reg[31]\(4),
      I4 => s00_axis_tuser(0),
      I5 => \m00_axis_tdata_r_reg[31]_0\(4),
      O => \m00_axis_tdata_r[4]_i_2_n_0\
    );
\m00_axis_tdata_r[5]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => DOUT7_w(5),
      I1 => dout0_out(5),
      I2 => s00_axis_tuser(1),
      I3 => \m00_axis_tdata_r_reg[31]\(5),
      I4 => s00_axis_tuser(0),
      I5 => \m00_axis_tdata_r_reg[31]_0\(5),
      O => ram_reg_0_8
    );
\m00_axis_tdata_r[6]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => DOUT7_w(6),
      I1 => dout0_out(6),
      I2 => s00_axis_tuser(1),
      I3 => \m00_axis_tdata_r_reg[31]\(6),
      I4 => s00_axis_tuser(0),
      I5 => \m00_axis_tdata_r_reg[31]_0\(6),
      O => ram_reg_0_7
    );
\m00_axis_tdata_r[7]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => DOUT7_w(7),
      I1 => dout0_out(7),
      I2 => s00_axis_tuser(1),
      I3 => \m00_axis_tdata_r_reg[31]\(7),
      I4 => s00_axis_tuser(0),
      I5 => \m00_axis_tdata_r_reg[31]_0\(7),
      O => ram_reg_0_6
    );
\m00_axis_tdata_r[8]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => DOUT7_w(8),
      I1 => dout0_out(8),
      I2 => s00_axis_tuser(1),
      I3 => \m00_axis_tdata_r_reg[31]\(8),
      I4 => s00_axis_tuser(0),
      I5 => \m00_axis_tdata_r_reg[31]_0\(8),
      O => ram_reg_0_5
    );
\m00_axis_tdata_r[9]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EFEA4540"
    )
        port map (
      I0 => s00_axis_tuser(3),
      I1 => \m00_axis_tdata_r[9]_i_2_n_0\,
      I2 => s00_axis_tuser(2),
      I3 => \m00_axis_tdata_r_reg[9]\,
      I4 => \m00_axis_tdata_r_reg[9]_0\,
      O => D(2)
    );
\m00_axis_tdata_r[9]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => DOUT7_w(9),
      I1 => dout0_out(9),
      I2 => s00_axis_tuser(1),
      I3 => \m00_axis_tdata_r_reg[31]\(9),
      I4 => s00_axis_tuser(0),
      I5 => \m00_axis_tdata_r_reg[31]_0\(9),
      O => \m00_axis_tdata_r[9]_i_2_n_0\
    );
ram_reg_0: unisim.vcomponents.RAMB36E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      EN_ECC_READ => false,
      EN_ECC_WRITE => false,
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      RAM_EXTENSION_A => "NONE",
      RAM_EXTENSION_B => "NONE",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "PERFORMANCE",
      READ_WIDTH_A => 18,
      READ_WIDTH_B => 0,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "READ_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 18,
      WRITE_WIDTH_B => 0
    )
        port map (
      ADDRARDADDR(15) => '1',
      ADDRARDADDR(14 downto 4) => ADDRARDADDR(10 downto 0),
      ADDRARDADDR(3 downto 0) => B"1111",
      ADDRBWRADDR(15 downto 0) => B"1111111111111111",
      CASCADEINA => '1',
      CASCADEINB => '0',
      CASCADEOUTA => NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED,
      CASCADEOUTB => NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED,
      CLKARDCLK => m00_axis_aclk,
      CLKBWRCLK => '0',
      DBITERR => NLW_ram_reg_0_DBITERR_UNCONNECTED,
      DIADI(31 downto 16) => B"0000000000000000",
      DIADI(15 downto 0) => Q(15 downto 0),
      DIBDI(31 downto 0) => B"11111111111111111111111111111111",
      DIPADIP(3 downto 2) => B"00",
      DIPADIP(1 downto 0) => Q(17 downto 16),
      DIPBDIP(3 downto 0) => B"1111",
      DOADO(31 downto 16) => NLW_ram_reg_0_DOADO_UNCONNECTED(31 downto 16),
      DOADO(15 downto 0) => DOUT7_w(15 downto 0),
      DOBDO(31 downto 0) => NLW_ram_reg_0_DOBDO_UNCONNECTED(31 downto 0),
      DOPADOP(3 downto 2) => NLW_ram_reg_0_DOPADOP_UNCONNECTED(3 downto 2),
      DOPADOP(1 downto 0) => DOUT7_w(17 downto 16),
      DOPBDOP(3 downto 0) => NLW_ram_reg_0_DOPBDOP_UNCONNECTED(3 downto 0),
      ECCPARITY(7 downto 0) => NLW_ram_reg_0_ECCPARITY_UNCONNECTED(7 downto 0),
      ENARDEN => en,
      ENBWREN => '0',
      INJECTDBITERR => NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED,
      INJECTSBITERR => NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED,
      RDADDRECC(8 downto 0) => NLW_ram_reg_0_RDADDRECC_UNCONNECTED(8 downto 0),
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      SBITERR => NLW_ram_reg_0_SBITERR_UNCONNECTED,
      WEA(3) => \ram_reg_0_i_1__6_n_0\,
      WEA(2) => \ram_reg_0_i_1__6_n_0\,
      WEA(1) => \ram_reg_0_i_1__6_n_0\,
      WEA(0) => \ram_reg_0_i_1__6_n_0\,
      WEBWE(7 downto 0) => B"00000000"
    );
\ram_reg_0_i_1__6\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => en,
      I1 => WE_r,
      I2 => square_r,
      O => \ram_reg_0_i_1__6_n_0\
    );
ram_reg_1: unisim.vcomponents.RAMB36E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      EN_ECC_READ => false,
      EN_ECC_WRITE => false,
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      RAM_EXTENSION_A => "NONE",
      RAM_EXTENSION_B => "NONE",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "PERFORMANCE",
      READ_WIDTH_A => 18,
      READ_WIDTH_B => 0,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "READ_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 18,
      WRITE_WIDTH_B => 0
    )
        port map (
      ADDRARDADDR(15) => '1',
      ADDRARDADDR(14 downto 4) => addr(10 downto 0),
      ADDRARDADDR(3 downto 0) => B"1111",
      ADDRBWRADDR(15 downto 0) => B"1111111111111111",
      CASCADEINA => '1',
      CASCADEINB => '0',
      CASCADEOUTA => NLW_ram_reg_1_CASCADEOUTA_UNCONNECTED,
      CASCADEOUTB => NLW_ram_reg_1_CASCADEOUTB_UNCONNECTED,
      CLKARDCLK => m00_axis_aclk,
      CLKBWRCLK => '0',
      DBITERR => NLW_ram_reg_1_DBITERR_UNCONNECTED,
      DIADI(31 downto 14) => B"000000000000000000",
      DIADI(13 downto 0) => Q(31 downto 18),
      DIBDI(31 downto 0) => B"11111111111111111111111111111111",
      DIPADIP(3 downto 0) => B"0000",
      DIPBDIP(3 downto 0) => B"1111",
      DOADO(31 downto 14) => NLW_ram_reg_1_DOADO_UNCONNECTED(31 downto 14),
      DOADO(13 downto 0) => DOUT7_w(31 downto 18),
      DOBDO(31 downto 0) => NLW_ram_reg_1_DOBDO_UNCONNECTED(31 downto 0),
      DOPADOP(3 downto 0) => NLW_ram_reg_1_DOPADOP_UNCONNECTED(3 downto 0),
      DOPBDOP(3 downto 0) => NLW_ram_reg_1_DOPBDOP_UNCONNECTED(3 downto 0),
      ECCPARITY(7 downto 0) => NLW_ram_reg_1_ECCPARITY_UNCONNECTED(7 downto 0),
      ENARDEN => en,
      ENBWREN => '0',
      INJECTDBITERR => NLW_ram_reg_1_INJECTDBITERR_UNCONNECTED,
      INJECTSBITERR => NLW_ram_reg_1_INJECTSBITERR_UNCONNECTED,
      RDADDRECC(8 downto 0) => NLW_ram_reg_1_RDADDRECC_UNCONNECTED(8 downto 0),
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      SBITERR => NLW_ram_reg_1_SBITERR_UNCONNECTED,
      WEA(3) => \ram_reg_0_i_1__6_n_0\,
      WEA(2) => \ram_reg_0_i_1__6_n_0\,
      WEA(1) => \ram_reg_0_i_1__6_n_0\,
      WEA(0) => \ram_reg_0_i_1__6_n_0\,
      WEBWE(7 downto 0) => B"00000000"
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_7 is
  port (
    dout0_out : out STD_LOGIC_VECTOR ( 31 downto 0 );
    m00_axis_aclk : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 0 to 0 );
    ADDRARDADDR : in STD_LOGIC_VECTOR ( 10 downto 0 );
    ram_reg_1_0 : in STD_LOGIC_VECTOR ( 31 downto 0 );
    addr : in STD_LOGIC_VECTOR ( 10 downto 0 );
    WE_r : in STD_LOGIC;
    square_r : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_7 : entity is "rams_sp_rf_rst";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_7;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_7 is
  signal \ram_reg_0_i_1__7_n_0\ : STD_LOGIC;
  signal NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_DBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_SBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_DOADO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 16 );
  signal NLW_ram_reg_0_DOBDO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_ram_reg_0_DOPADOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal NLW_ram_reg_0_DOPBDOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_ram_reg_0_ECCPARITY_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal NLW_ram_reg_0_RDADDRECC_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_ram_reg_1_CASCADEOUTA_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_1_CASCADEOUTB_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_1_DBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_1_INJECTDBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_1_INJECTSBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_1_SBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_1_DOADO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 14 );
  signal NLW_ram_reg_1_DOBDO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_ram_reg_1_DOPADOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_ram_reg_1_DOPBDOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_ram_reg_1_ECCPARITY_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal NLW_ram_reg_1_RDADDRECC_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ : string;
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of ram_reg_0 : label is "p2_d16";
  attribute METHODOLOGY_DRC_VIOS : string;
  attribute METHODOLOGY_DRC_VIOS of ram_reg_0 : label is "{SYNTH-6 {cell *THIS*}}";
  attribute RTL_RAM_BITS : integer;
  attribute RTL_RAM_BITS of ram_reg_0 : label is 61472;
  attribute RTL_RAM_NAME : string;
  attribute RTL_RAM_NAME of ram_reg_0 : label is "ram";
  attribute bram_addr_begin : integer;
  attribute bram_addr_begin of ram_reg_0 : label is 0;
  attribute bram_addr_end : integer;
  attribute bram_addr_end of ram_reg_0 : label is 2047;
  attribute bram_slice_begin : integer;
  attribute bram_slice_begin of ram_reg_0 : label is 0;
  attribute bram_slice_end : integer;
  attribute bram_slice_end of ram_reg_0 : label is 17;
  attribute ram_addr_begin : integer;
  attribute ram_addr_begin of ram_reg_0 : label is 0;
  attribute ram_addr_end : integer;
  attribute ram_addr_end of ram_reg_0 : label is 2047;
  attribute ram_offset : integer;
  attribute ram_offset of ram_reg_0 : label is 0;
  attribute ram_slice_begin : integer;
  attribute ram_slice_begin of ram_reg_0 : label is 0;
  attribute ram_slice_end : integer;
  attribute ram_slice_end of ram_reg_0 : label is 17;
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of ram_reg_1 : label is "p0_d14";
  attribute METHODOLOGY_DRC_VIOS of ram_reg_1 : label is "{SYNTH-6 {cell *THIS*}}";
  attribute RTL_RAM_BITS of ram_reg_1 : label is 61472;
  attribute RTL_RAM_NAME of ram_reg_1 : label is "ram";
  attribute bram_addr_begin of ram_reg_1 : label is 0;
  attribute bram_addr_end of ram_reg_1 : label is 2047;
  attribute bram_slice_begin of ram_reg_1 : label is 18;
  attribute bram_slice_end of ram_reg_1 : label is 31;
  attribute ram_addr_begin of ram_reg_1 : label is 0;
  attribute ram_addr_end of ram_reg_1 : label is 2047;
  attribute ram_offset of ram_reg_1 : label is 0;
  attribute ram_slice_begin of ram_reg_1 : label is 18;
  attribute ram_slice_end of ram_reg_1 : label is 31;
begin
ram_reg_0: unisim.vcomponents.RAMB36E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      EN_ECC_READ => false,
      EN_ECC_WRITE => false,
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      RAM_EXTENSION_A => "NONE",
      RAM_EXTENSION_B => "NONE",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "PERFORMANCE",
      READ_WIDTH_A => 18,
      READ_WIDTH_B => 0,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "READ_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 18,
      WRITE_WIDTH_B => 0
    )
        port map (
      ADDRARDADDR(15) => '1',
      ADDRARDADDR(14 downto 4) => ADDRARDADDR(10 downto 0),
      ADDRARDADDR(3 downto 0) => B"1111",
      ADDRBWRADDR(15 downto 0) => B"1111111111111111",
      CASCADEINA => '1',
      CASCADEINB => '0',
      CASCADEOUTA => NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED,
      CASCADEOUTB => NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED,
      CLKARDCLK => m00_axis_aclk,
      CLKBWRCLK => '0',
      DBITERR => NLW_ram_reg_0_DBITERR_UNCONNECTED,
      DIADI(31 downto 16) => B"0000000000000000",
      DIADI(15 downto 0) => ram_reg_1_0(15 downto 0),
      DIBDI(31 downto 0) => B"11111111111111111111111111111111",
      DIPADIP(3 downto 2) => B"00",
      DIPADIP(1 downto 0) => ram_reg_1_0(17 downto 16),
      DIPBDIP(3 downto 0) => B"1111",
      DOADO(31 downto 16) => NLW_ram_reg_0_DOADO_UNCONNECTED(31 downto 16),
      DOADO(15 downto 0) => dout0_out(15 downto 0),
      DOBDO(31 downto 0) => NLW_ram_reg_0_DOBDO_UNCONNECTED(31 downto 0),
      DOPADOP(3 downto 2) => NLW_ram_reg_0_DOPADOP_UNCONNECTED(3 downto 2),
      DOPADOP(1 downto 0) => dout0_out(17 downto 16),
      DOPBDOP(3 downto 0) => NLW_ram_reg_0_DOPBDOP_UNCONNECTED(3 downto 0),
      ECCPARITY(7 downto 0) => NLW_ram_reg_0_ECCPARITY_UNCONNECTED(7 downto 0),
      ENARDEN => Q(0),
      ENBWREN => '0',
      INJECTDBITERR => NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED,
      INJECTSBITERR => NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED,
      RDADDRECC(8 downto 0) => NLW_ram_reg_0_RDADDRECC_UNCONNECTED(8 downto 0),
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      SBITERR => NLW_ram_reg_0_SBITERR_UNCONNECTED,
      WEA(3) => \ram_reg_0_i_1__7_n_0\,
      WEA(2) => \ram_reg_0_i_1__7_n_0\,
      WEA(1) => \ram_reg_0_i_1__7_n_0\,
      WEA(0) => \ram_reg_0_i_1__7_n_0\,
      WEBWE(7 downto 0) => B"00000000"
    );
\ram_reg_0_i_1__7\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => Q(0),
      I1 => WE_r,
      I2 => square_r,
      O => \ram_reg_0_i_1__7_n_0\
    );
ram_reg_1: unisim.vcomponents.RAMB36E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      EN_ECC_READ => false,
      EN_ECC_WRITE => false,
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      RAM_EXTENSION_A => "NONE",
      RAM_EXTENSION_B => "NONE",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "PERFORMANCE",
      READ_WIDTH_A => 18,
      READ_WIDTH_B => 0,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "READ_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 18,
      WRITE_WIDTH_B => 0
    )
        port map (
      ADDRARDADDR(15) => '1',
      ADDRARDADDR(14 downto 4) => addr(10 downto 0),
      ADDRARDADDR(3 downto 0) => B"1111",
      ADDRBWRADDR(15 downto 0) => B"1111111111111111",
      CASCADEINA => '1',
      CASCADEINB => '0',
      CASCADEOUTA => NLW_ram_reg_1_CASCADEOUTA_UNCONNECTED,
      CASCADEOUTB => NLW_ram_reg_1_CASCADEOUTB_UNCONNECTED,
      CLKARDCLK => m00_axis_aclk,
      CLKBWRCLK => '0',
      DBITERR => NLW_ram_reg_1_DBITERR_UNCONNECTED,
      DIADI(31 downto 14) => B"000000000000000000",
      DIADI(13 downto 0) => ram_reg_1_0(31 downto 18),
      DIBDI(31 downto 0) => B"11111111111111111111111111111111",
      DIPADIP(3 downto 0) => B"0000",
      DIPBDIP(3 downto 0) => B"1111",
      DOADO(31 downto 14) => NLW_ram_reg_1_DOADO_UNCONNECTED(31 downto 14),
      DOADO(13 downto 0) => dout0_out(31 downto 18),
      DOBDO(31 downto 0) => NLW_ram_reg_1_DOBDO_UNCONNECTED(31 downto 0),
      DOPADOP(3 downto 0) => NLW_ram_reg_1_DOPADOP_UNCONNECTED(3 downto 0),
      DOPBDOP(3 downto 0) => NLW_ram_reg_1_DOPBDOP_UNCONNECTED(3 downto 0),
      ECCPARITY(7 downto 0) => NLW_ram_reg_1_ECCPARITY_UNCONNECTED(7 downto 0),
      ENARDEN => Q(0),
      ENBWREN => '0',
      INJECTDBITERR => NLW_ram_reg_1_INJECTDBITERR_UNCONNECTED,
      INJECTSBITERR => NLW_ram_reg_1_INJECTSBITERR_UNCONNECTED,
      RDADDRECC(8 downto 0) => NLW_ram_reg_1_RDADDRECC_UNCONNECTED(8 downto 0),
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      SBITERR => NLW_ram_reg_1_SBITERR_UNCONNECTED,
      WEA(3) => \ram_reg_0_i_1__7_n_0\,
      WEA(2) => \ram_reg_0_i_1__7_n_0\,
      WEA(1) => \ram_reg_0_i_1__7_n_0\,
      WEA(0) => \ram_reg_0_i_1__7_n_0\,
      WEBWE(7 downto 0) => B"00000000"
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_8 is
  port (
    dout0_out : out STD_LOGIC_VECTOR ( 31 downto 0 );
    m00_axis_aclk : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 0 to 0 );
    ADDRARDADDR : in STD_LOGIC_VECTOR ( 10 downto 0 );
    ram_reg_1_0 : in STD_LOGIC_VECTOR ( 31 downto 0 );
    addr : in STD_LOGIC_VECTOR ( 10 downto 0 );
    WE_r : in STD_LOGIC;
    square_r : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_8 : entity is "rams_sp_rf_rst";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_8;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_8 is
  signal \ram_reg_0_i_1__8_n_0\ : STD_LOGIC;
  signal NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_DBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_SBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_DOADO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 16 );
  signal NLW_ram_reg_0_DOBDO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_ram_reg_0_DOPADOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal NLW_ram_reg_0_DOPBDOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_ram_reg_0_ECCPARITY_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal NLW_ram_reg_0_RDADDRECC_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_ram_reg_1_CASCADEOUTA_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_1_CASCADEOUTB_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_1_DBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_1_INJECTDBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_1_INJECTSBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_1_SBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_1_DOADO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 14 );
  signal NLW_ram_reg_1_DOBDO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_ram_reg_1_DOPADOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_ram_reg_1_DOPBDOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_ram_reg_1_ECCPARITY_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal NLW_ram_reg_1_RDADDRECC_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ : string;
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of ram_reg_0 : label is "p2_d16";
  attribute METHODOLOGY_DRC_VIOS : string;
  attribute METHODOLOGY_DRC_VIOS of ram_reg_0 : label is "{SYNTH-6 {cell *THIS*}}";
  attribute RTL_RAM_BITS : integer;
  attribute RTL_RAM_BITS of ram_reg_0 : label is 61472;
  attribute RTL_RAM_NAME : string;
  attribute RTL_RAM_NAME of ram_reg_0 : label is "ram";
  attribute bram_addr_begin : integer;
  attribute bram_addr_begin of ram_reg_0 : label is 0;
  attribute bram_addr_end : integer;
  attribute bram_addr_end of ram_reg_0 : label is 2047;
  attribute bram_slice_begin : integer;
  attribute bram_slice_begin of ram_reg_0 : label is 0;
  attribute bram_slice_end : integer;
  attribute bram_slice_end of ram_reg_0 : label is 17;
  attribute ram_addr_begin : integer;
  attribute ram_addr_begin of ram_reg_0 : label is 0;
  attribute ram_addr_end : integer;
  attribute ram_addr_end of ram_reg_0 : label is 2047;
  attribute ram_offset : integer;
  attribute ram_offset of ram_reg_0 : label is 0;
  attribute ram_slice_begin : integer;
  attribute ram_slice_begin of ram_reg_0 : label is 0;
  attribute ram_slice_end : integer;
  attribute ram_slice_end of ram_reg_0 : label is 17;
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of ram_reg_1 : label is "p0_d14";
  attribute METHODOLOGY_DRC_VIOS of ram_reg_1 : label is "{SYNTH-6 {cell *THIS*}}";
  attribute RTL_RAM_BITS of ram_reg_1 : label is 61472;
  attribute RTL_RAM_NAME of ram_reg_1 : label is "ram";
  attribute bram_addr_begin of ram_reg_1 : label is 0;
  attribute bram_addr_end of ram_reg_1 : label is 2047;
  attribute bram_slice_begin of ram_reg_1 : label is 18;
  attribute bram_slice_end of ram_reg_1 : label is 31;
  attribute ram_addr_begin of ram_reg_1 : label is 0;
  attribute ram_addr_end of ram_reg_1 : label is 2047;
  attribute ram_offset of ram_reg_1 : label is 0;
  attribute ram_slice_begin of ram_reg_1 : label is 18;
  attribute ram_slice_end of ram_reg_1 : label is 31;
begin
ram_reg_0: unisim.vcomponents.RAMB36E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      EN_ECC_READ => false,
      EN_ECC_WRITE => false,
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      RAM_EXTENSION_A => "NONE",
      RAM_EXTENSION_B => "NONE",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "PERFORMANCE",
      READ_WIDTH_A => 18,
      READ_WIDTH_B => 0,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "READ_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 18,
      WRITE_WIDTH_B => 0
    )
        port map (
      ADDRARDADDR(15) => '1',
      ADDRARDADDR(14 downto 4) => ADDRARDADDR(10 downto 0),
      ADDRARDADDR(3 downto 0) => B"1111",
      ADDRBWRADDR(15 downto 0) => B"1111111111111111",
      CASCADEINA => '1',
      CASCADEINB => '0',
      CASCADEOUTA => NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED,
      CASCADEOUTB => NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED,
      CLKARDCLK => m00_axis_aclk,
      CLKBWRCLK => '0',
      DBITERR => NLW_ram_reg_0_DBITERR_UNCONNECTED,
      DIADI(31 downto 16) => B"0000000000000000",
      DIADI(15 downto 0) => ram_reg_1_0(15 downto 0),
      DIBDI(31 downto 0) => B"11111111111111111111111111111111",
      DIPADIP(3 downto 2) => B"00",
      DIPADIP(1 downto 0) => ram_reg_1_0(17 downto 16),
      DIPBDIP(3 downto 0) => B"1111",
      DOADO(31 downto 16) => NLW_ram_reg_0_DOADO_UNCONNECTED(31 downto 16),
      DOADO(15 downto 0) => dout0_out(15 downto 0),
      DOBDO(31 downto 0) => NLW_ram_reg_0_DOBDO_UNCONNECTED(31 downto 0),
      DOPADOP(3 downto 2) => NLW_ram_reg_0_DOPADOP_UNCONNECTED(3 downto 2),
      DOPADOP(1 downto 0) => dout0_out(17 downto 16),
      DOPBDOP(3 downto 0) => NLW_ram_reg_0_DOPBDOP_UNCONNECTED(3 downto 0),
      ECCPARITY(7 downto 0) => NLW_ram_reg_0_ECCPARITY_UNCONNECTED(7 downto 0),
      ENARDEN => Q(0),
      ENBWREN => '0',
      INJECTDBITERR => NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED,
      INJECTSBITERR => NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED,
      RDADDRECC(8 downto 0) => NLW_ram_reg_0_RDADDRECC_UNCONNECTED(8 downto 0),
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      SBITERR => NLW_ram_reg_0_SBITERR_UNCONNECTED,
      WEA(3) => \ram_reg_0_i_1__8_n_0\,
      WEA(2) => \ram_reg_0_i_1__8_n_0\,
      WEA(1) => \ram_reg_0_i_1__8_n_0\,
      WEA(0) => \ram_reg_0_i_1__8_n_0\,
      WEBWE(7 downto 0) => B"00000000"
    );
\ram_reg_0_i_1__8\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => Q(0),
      I1 => WE_r,
      I2 => square_r,
      O => \ram_reg_0_i_1__8_n_0\
    );
ram_reg_1: unisim.vcomponents.RAMB36E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      EN_ECC_READ => false,
      EN_ECC_WRITE => false,
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      RAM_EXTENSION_A => "NONE",
      RAM_EXTENSION_B => "NONE",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "PERFORMANCE",
      READ_WIDTH_A => 18,
      READ_WIDTH_B => 0,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "READ_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 18,
      WRITE_WIDTH_B => 0
    )
        port map (
      ADDRARDADDR(15) => '1',
      ADDRARDADDR(14 downto 4) => addr(10 downto 0),
      ADDRARDADDR(3 downto 0) => B"1111",
      ADDRBWRADDR(15 downto 0) => B"1111111111111111",
      CASCADEINA => '1',
      CASCADEINB => '0',
      CASCADEOUTA => NLW_ram_reg_1_CASCADEOUTA_UNCONNECTED,
      CASCADEOUTB => NLW_ram_reg_1_CASCADEOUTB_UNCONNECTED,
      CLKARDCLK => m00_axis_aclk,
      CLKBWRCLK => '0',
      DBITERR => NLW_ram_reg_1_DBITERR_UNCONNECTED,
      DIADI(31 downto 14) => B"000000000000000000",
      DIADI(13 downto 0) => ram_reg_1_0(31 downto 18),
      DIBDI(31 downto 0) => B"11111111111111111111111111111111",
      DIPADIP(3 downto 0) => B"0000",
      DIPBDIP(3 downto 0) => B"1111",
      DOADO(31 downto 14) => NLW_ram_reg_1_DOADO_UNCONNECTED(31 downto 14),
      DOADO(13 downto 0) => dout0_out(31 downto 18),
      DOBDO(31 downto 0) => NLW_ram_reg_1_DOBDO_UNCONNECTED(31 downto 0),
      DOPADOP(3 downto 0) => NLW_ram_reg_1_DOPADOP_UNCONNECTED(3 downto 0),
      DOPBDOP(3 downto 0) => NLW_ram_reg_1_DOPBDOP_UNCONNECTED(3 downto 0),
      ECCPARITY(7 downto 0) => NLW_ram_reg_1_ECCPARITY_UNCONNECTED(7 downto 0),
      ENARDEN => Q(0),
      ENBWREN => '0',
      INJECTDBITERR => NLW_ram_reg_1_INJECTDBITERR_UNCONNECTED,
      INJECTSBITERR => NLW_ram_reg_1_INJECTSBITERR_UNCONNECTED,
      RDADDRECC(8 downto 0) => NLW_ram_reg_1_RDADDRECC_UNCONNECTED(8 downto 0),
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      SBITERR => NLW_ram_reg_1_SBITERR_UNCONNECTED,
      WEA(3) => \ram_reg_0_i_1__8_n_0\,
      WEA(2) => \ram_reg_0_i_1__8_n_0\,
      WEA(1) => \ram_reg_0_i_1__8_n_0\,
      WEA(0) => \ram_reg_0_i_1__8_n_0\,
      WEBWE(7 downto 0) => B"00000000"
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_9 is
  port (
    dout0_out : out STD_LOGIC_VECTOR ( 31 downto 0 );
    m00_axis_aclk : in STD_LOGIC;
    en : in STD_LOGIC;
    ADDRARDADDR : in STD_LOGIC_VECTOR ( 10 downto 0 );
    Q : in STD_LOGIC_VECTOR ( 31 downto 0 );
    addr : in STD_LOGIC_VECTOR ( 10 downto 0 );
    WE_r : in STD_LOGIC;
    square_r : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_9 : entity is "rams_sp_rf_rst";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_9;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_9 is
  signal \ram_reg_0_i_1__9_n_0\ : STD_LOGIC;
  signal NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_DBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_SBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_DOADO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 16 );
  signal NLW_ram_reg_0_DOBDO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_ram_reg_0_DOPADOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal NLW_ram_reg_0_DOPBDOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_ram_reg_0_ECCPARITY_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal NLW_ram_reg_0_RDADDRECC_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_ram_reg_1_CASCADEOUTA_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_1_CASCADEOUTB_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_1_DBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_1_INJECTDBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_1_INJECTSBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_1_SBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_1_DOADO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 14 );
  signal NLW_ram_reg_1_DOBDO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_ram_reg_1_DOPADOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_ram_reg_1_DOPBDOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_ram_reg_1_ECCPARITY_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal NLW_ram_reg_1_RDADDRECC_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ : string;
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of ram_reg_0 : label is "p2_d16";
  attribute METHODOLOGY_DRC_VIOS : string;
  attribute METHODOLOGY_DRC_VIOS of ram_reg_0 : label is "{SYNTH-6 {cell *THIS*}}";
  attribute RTL_RAM_BITS : integer;
  attribute RTL_RAM_BITS of ram_reg_0 : label is 61472;
  attribute RTL_RAM_NAME : string;
  attribute RTL_RAM_NAME of ram_reg_0 : label is "ram";
  attribute bram_addr_begin : integer;
  attribute bram_addr_begin of ram_reg_0 : label is 0;
  attribute bram_addr_end : integer;
  attribute bram_addr_end of ram_reg_0 : label is 2047;
  attribute bram_slice_begin : integer;
  attribute bram_slice_begin of ram_reg_0 : label is 0;
  attribute bram_slice_end : integer;
  attribute bram_slice_end of ram_reg_0 : label is 17;
  attribute ram_addr_begin : integer;
  attribute ram_addr_begin of ram_reg_0 : label is 0;
  attribute ram_addr_end : integer;
  attribute ram_addr_end of ram_reg_0 : label is 2047;
  attribute ram_offset : integer;
  attribute ram_offset of ram_reg_0 : label is 0;
  attribute ram_slice_begin : integer;
  attribute ram_slice_begin of ram_reg_0 : label is 0;
  attribute ram_slice_end : integer;
  attribute ram_slice_end of ram_reg_0 : label is 17;
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of ram_reg_1 : label is "p0_d14";
  attribute METHODOLOGY_DRC_VIOS of ram_reg_1 : label is "{SYNTH-6 {cell *THIS*}}";
  attribute RTL_RAM_BITS of ram_reg_1 : label is 61472;
  attribute RTL_RAM_NAME of ram_reg_1 : label is "ram";
  attribute bram_addr_begin of ram_reg_1 : label is 0;
  attribute bram_addr_end of ram_reg_1 : label is 2047;
  attribute bram_slice_begin of ram_reg_1 : label is 18;
  attribute bram_slice_end of ram_reg_1 : label is 31;
  attribute ram_addr_begin of ram_reg_1 : label is 0;
  attribute ram_addr_end of ram_reg_1 : label is 2047;
  attribute ram_offset of ram_reg_1 : label is 0;
  attribute ram_slice_begin of ram_reg_1 : label is 18;
  attribute ram_slice_end of ram_reg_1 : label is 31;
begin
ram_reg_0: unisim.vcomponents.RAMB36E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      EN_ECC_READ => false,
      EN_ECC_WRITE => false,
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      RAM_EXTENSION_A => "NONE",
      RAM_EXTENSION_B => "NONE",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "PERFORMANCE",
      READ_WIDTH_A => 18,
      READ_WIDTH_B => 0,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "READ_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 18,
      WRITE_WIDTH_B => 0
    )
        port map (
      ADDRARDADDR(15) => '1',
      ADDRARDADDR(14 downto 4) => ADDRARDADDR(10 downto 0),
      ADDRARDADDR(3 downto 0) => B"1111",
      ADDRBWRADDR(15 downto 0) => B"1111111111111111",
      CASCADEINA => '1',
      CASCADEINB => '0',
      CASCADEOUTA => NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED,
      CASCADEOUTB => NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED,
      CLKARDCLK => m00_axis_aclk,
      CLKBWRCLK => '0',
      DBITERR => NLW_ram_reg_0_DBITERR_UNCONNECTED,
      DIADI(31 downto 16) => B"0000000000000000",
      DIADI(15 downto 0) => Q(15 downto 0),
      DIBDI(31 downto 0) => B"11111111111111111111111111111111",
      DIPADIP(3 downto 2) => B"00",
      DIPADIP(1 downto 0) => Q(17 downto 16),
      DIPBDIP(3 downto 0) => B"1111",
      DOADO(31 downto 16) => NLW_ram_reg_0_DOADO_UNCONNECTED(31 downto 16),
      DOADO(15 downto 0) => dout0_out(15 downto 0),
      DOBDO(31 downto 0) => NLW_ram_reg_0_DOBDO_UNCONNECTED(31 downto 0),
      DOPADOP(3 downto 2) => NLW_ram_reg_0_DOPADOP_UNCONNECTED(3 downto 2),
      DOPADOP(1 downto 0) => dout0_out(17 downto 16),
      DOPBDOP(3 downto 0) => NLW_ram_reg_0_DOPBDOP_UNCONNECTED(3 downto 0),
      ECCPARITY(7 downto 0) => NLW_ram_reg_0_ECCPARITY_UNCONNECTED(7 downto 0),
      ENARDEN => en,
      ENBWREN => '0',
      INJECTDBITERR => NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED,
      INJECTSBITERR => NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED,
      RDADDRECC(8 downto 0) => NLW_ram_reg_0_RDADDRECC_UNCONNECTED(8 downto 0),
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      SBITERR => NLW_ram_reg_0_SBITERR_UNCONNECTED,
      WEA(3) => \ram_reg_0_i_1__9_n_0\,
      WEA(2) => \ram_reg_0_i_1__9_n_0\,
      WEA(1) => \ram_reg_0_i_1__9_n_0\,
      WEA(0) => \ram_reg_0_i_1__9_n_0\,
      WEBWE(7 downto 0) => B"00000000"
    );
\ram_reg_0_i_1__9\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => en,
      I1 => WE_r,
      I2 => square_r,
      O => \ram_reg_0_i_1__9_n_0\
    );
ram_reg_1: unisim.vcomponents.RAMB36E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      EN_ECC_READ => false,
      EN_ECC_WRITE => false,
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      RAM_EXTENSION_A => "NONE",
      RAM_EXTENSION_B => "NONE",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "PERFORMANCE",
      READ_WIDTH_A => 18,
      READ_WIDTH_B => 0,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "READ_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 18,
      WRITE_WIDTH_B => 0
    )
        port map (
      ADDRARDADDR(15) => '1',
      ADDRARDADDR(14 downto 4) => addr(10 downto 0),
      ADDRARDADDR(3 downto 0) => B"1111",
      ADDRBWRADDR(15 downto 0) => B"1111111111111111",
      CASCADEINA => '1',
      CASCADEINB => '0',
      CASCADEOUTA => NLW_ram_reg_1_CASCADEOUTA_UNCONNECTED,
      CASCADEOUTB => NLW_ram_reg_1_CASCADEOUTB_UNCONNECTED,
      CLKARDCLK => m00_axis_aclk,
      CLKBWRCLK => '0',
      DBITERR => NLW_ram_reg_1_DBITERR_UNCONNECTED,
      DIADI(31 downto 14) => B"000000000000000000",
      DIADI(13 downto 0) => Q(31 downto 18),
      DIBDI(31 downto 0) => B"11111111111111111111111111111111",
      DIPADIP(3 downto 0) => B"0000",
      DIPBDIP(3 downto 0) => B"1111",
      DOADO(31 downto 14) => NLW_ram_reg_1_DOADO_UNCONNECTED(31 downto 14),
      DOADO(13 downto 0) => dout0_out(31 downto 18),
      DOBDO(31 downto 0) => NLW_ram_reg_1_DOBDO_UNCONNECTED(31 downto 0),
      DOPADOP(3 downto 0) => NLW_ram_reg_1_DOPADOP_UNCONNECTED(3 downto 0),
      DOPBDOP(3 downto 0) => NLW_ram_reg_1_DOPBDOP_UNCONNECTED(3 downto 0),
      ECCPARITY(7 downto 0) => NLW_ram_reg_1_ECCPARITY_UNCONNECTED(7 downto 0),
      ENARDEN => en,
      ENBWREN => '0',
      INJECTDBITERR => NLW_ram_reg_1_INJECTDBITERR_UNCONNECTED,
      INJECTSBITERR => NLW_ram_reg_1_INJECTSBITERR_UNCONNECTED,
      RDADDRECC(8 downto 0) => NLW_ram_reg_1_RDADDRECC_UNCONNECTED(8 downto 0),
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      SBITERR => NLW_ram_reg_1_SBITERR_UNCONNECTED,
      WEA(3) => \ram_reg_0_i_1__9_n_0\,
      WEA(2) => \ram_reg_0_i_1__9_n_0\,
      WEA(1) => \ram_reg_0_i_1__9_n_0\,
      WEA(0) => \ram_reg_0_i_1__9_n_0\,
      WEBWE(7 downto 0) => B"00000000"
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_pixelReOrder_v1_0 is
  port (
    m00_axis_tvalid : out STD_LOGIC;
    m00_axis_tdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s00_axis_tvalid : in STD_LOGIC;
    s00_axis_tuser : in STD_LOGIC_VECTOR ( 26 downto 0 );
    m00_axis_aclk : in STD_LOGIC;
    s00_axis_tdata : in STD_LOGIC_VECTOR ( 31 downto 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_pixelReOrder_v1_0;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_pixelReOrder_v1_0 is
  signal DI0 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal DOUT0 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal DOUT1_w : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal DOUT2_w : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal DOUT4_w : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal DOUT5_w : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal DOUT6_w : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal DOUT8_w : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal DOUT9_w : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal DOUTA_w : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal DOUTC_w : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal DOUTD_w : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal DOUTE_w : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal EN_r : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal \EN_r[0]_i_1_n_0\ : STD_LOGIC;
  signal \EN_r[11]_i_1_n_0\ : STD_LOGIC;
  signal \EN_r[11]_i_2_n_0\ : STD_LOGIC;
  signal \EN_r[13]_i_1_n_0\ : STD_LOGIC;
  signal \EN_r[14]_i_1_n_0\ : STD_LOGIC;
  signal \EN_r[15]_i_1_n_0\ : STD_LOGIC;
  signal \EN_r[15]_i_2_n_0\ : STD_LOGIC;
  signal \EN_r[15]_i_3_n_0\ : STD_LOGIC;
  signal \EN_r[1]_i_1_n_0\ : STD_LOGIC;
  signal \EN_r[6]_i_1_n_0\ : STD_LOGIC;
  signal \EN_r[7]_i_1_n_0\ : STD_LOGIC;
  signal \EN_r[8]_i_1_n_0\ : STD_LOGIC;
  signal \EN_r[9]_i_1_n_0\ : STD_LOGIC;
  signal \EN_r[9]_i_2_n_0\ : STD_LOGIC;
  signal WE_r : STD_LOGIC;
  signal adr_wr : STD_LOGIC_VECTOR ( 10 downto 0 );
  signal \adr_wr_reg[0]_rep_n_0\ : STD_LOGIC;
  signal \adr_wr_reg[10]_rep_n_0\ : STD_LOGIC;
  signal \adr_wr_reg[1]_rep_n_0\ : STD_LOGIC;
  signal \adr_wr_reg[2]_rep_n_0\ : STD_LOGIC;
  signal \adr_wr_reg[3]_rep_n_0\ : STD_LOGIC;
  signal \adr_wr_reg[4]_rep_n_0\ : STD_LOGIC;
  signal \adr_wr_reg[5]_rep_n_0\ : STD_LOGIC;
  signal \adr_wr_reg[6]_rep_n_0\ : STD_LOGIC;
  signal \adr_wr_reg[7]_rep_n_0\ : STD_LOGIC;
  signal \adr_wr_reg[8]_rep_n_0\ : STD_LOGIC;
  signal \adr_wr_reg[9]_rep_n_0\ : STD_LOGIC;
  signal cnt_inner_valid : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal \cnt_inner_valid0_carry__0_n_0\ : STD_LOGIC;
  signal \cnt_inner_valid0_carry__0_n_1\ : STD_LOGIC;
  signal \cnt_inner_valid0_carry__0_n_2\ : STD_LOGIC;
  signal \cnt_inner_valid0_carry__0_n_3\ : STD_LOGIC;
  signal \cnt_inner_valid0_carry__1_n_0\ : STD_LOGIC;
  signal \cnt_inner_valid0_carry__1_n_1\ : STD_LOGIC;
  signal \cnt_inner_valid0_carry__1_n_2\ : STD_LOGIC;
  signal \cnt_inner_valid0_carry__1_n_3\ : STD_LOGIC;
  signal \cnt_inner_valid0_carry__2_n_2\ : STD_LOGIC;
  signal \cnt_inner_valid0_carry__2_n_3\ : STD_LOGIC;
  signal cnt_inner_valid0_carry_n_0 : STD_LOGIC;
  signal cnt_inner_valid0_carry_n_1 : STD_LOGIC;
  signal cnt_inner_valid0_carry_n_2 : STD_LOGIC;
  signal cnt_inner_valid0_carry_n_3 : STD_LOGIC;
  signal \cnt_inner_valid[15]_i_3_n_0\ : STD_LOGIC;
  signal \cnt_inner_valid_reg_n_0_[0]\ : STD_LOGIC;
  signal \cnt_inner_valid_reg_n_0_[10]\ : STD_LOGIC;
  signal \cnt_inner_valid_reg_n_0_[11]\ : STD_LOGIC;
  signal \cnt_inner_valid_reg_n_0_[12]\ : STD_LOGIC;
  signal \cnt_inner_valid_reg_n_0_[13]\ : STD_LOGIC;
  signal \cnt_inner_valid_reg_n_0_[14]\ : STD_LOGIC;
  signal \cnt_inner_valid_reg_n_0_[15]\ : STD_LOGIC;
  signal \cnt_inner_valid_reg_n_0_[1]\ : STD_LOGIC;
  signal \cnt_inner_valid_reg_n_0_[2]\ : STD_LOGIC;
  signal \cnt_inner_valid_reg_n_0_[3]\ : STD_LOGIC;
  signal \cnt_inner_valid_reg_n_0_[4]\ : STD_LOGIC;
  signal \cnt_inner_valid_reg_n_0_[5]\ : STD_LOGIC;
  signal \cnt_inner_valid_reg_n_0_[6]\ : STD_LOGIC;
  signal \cnt_inner_valid_reg_n_0_[7]\ : STD_LOGIC;
  signal \cnt_inner_valid_reg_n_0_[8]\ : STD_LOGIC;
  signal \cnt_inner_valid_reg_n_0_[9]\ : STD_LOGIC;
  signal data0 : STD_LOGIC_VECTOR ( 15 downto 1 );
  signal \m00_axis_tdata_r[31]_i_1_n_0\ : STD_LOGIC;
  signal \m00_axis_tdata_r[31]_i_3_n_0\ : STD_LOGIC;
  signal \m00_axis_tdata_r[31]_i_4_n_0\ : STD_LOGIC;
  signal m00_axis_tvalid_r0 : STD_LOGIC;
  signal m00_axis_tvalid_r_i_2_n_0 : STD_LOGIC;
  signal m00_axis_tvalid_r_i_3_n_0 : STD_LOGIC;
  signal m00_axis_tvalid_r_i_4_n_0 : STD_LOGIC;
  signal p_0_in : STD_LOGIC;
  signal rams_sp_rf_rst_inst3_n_0 : STD_LOGIC;
  signal rams_sp_rf_rst_inst3_n_1 : STD_LOGIC;
  signal rams_sp_rf_rst_inst3_n_10 : STD_LOGIC;
  signal rams_sp_rf_rst_inst3_n_11 : STD_LOGIC;
  signal rams_sp_rf_rst_inst3_n_12 : STD_LOGIC;
  signal rams_sp_rf_rst_inst3_n_13 : STD_LOGIC;
  signal rams_sp_rf_rst_inst3_n_14 : STD_LOGIC;
  signal rams_sp_rf_rst_inst3_n_15 : STD_LOGIC;
  signal rams_sp_rf_rst_inst3_n_16 : STD_LOGIC;
  signal rams_sp_rf_rst_inst3_n_17 : STD_LOGIC;
  signal rams_sp_rf_rst_inst3_n_18 : STD_LOGIC;
  signal rams_sp_rf_rst_inst3_n_19 : STD_LOGIC;
  signal rams_sp_rf_rst_inst3_n_2 : STD_LOGIC;
  signal rams_sp_rf_rst_inst3_n_20 : STD_LOGIC;
  signal rams_sp_rf_rst_inst3_n_21 : STD_LOGIC;
  signal rams_sp_rf_rst_inst3_n_22 : STD_LOGIC;
  signal rams_sp_rf_rst_inst3_n_23 : STD_LOGIC;
  signal rams_sp_rf_rst_inst3_n_24 : STD_LOGIC;
  signal rams_sp_rf_rst_inst3_n_25 : STD_LOGIC;
  signal rams_sp_rf_rst_inst3_n_26 : STD_LOGIC;
  signal rams_sp_rf_rst_inst3_n_27 : STD_LOGIC;
  signal rams_sp_rf_rst_inst3_n_28 : STD_LOGIC;
  signal rams_sp_rf_rst_inst3_n_29 : STD_LOGIC;
  signal rams_sp_rf_rst_inst3_n_3 : STD_LOGIC;
  signal rams_sp_rf_rst_inst3_n_30 : STD_LOGIC;
  signal rams_sp_rf_rst_inst3_n_31 : STD_LOGIC;
  signal rams_sp_rf_rst_inst3_n_4 : STD_LOGIC;
  signal rams_sp_rf_rst_inst3_n_5 : STD_LOGIC;
  signal rams_sp_rf_rst_inst3_n_6 : STD_LOGIC;
  signal rams_sp_rf_rst_inst3_n_7 : STD_LOGIC;
  signal rams_sp_rf_rst_inst3_n_8 : STD_LOGIC;
  signal rams_sp_rf_rst_inst3_n_9 : STD_LOGIC;
  signal rams_sp_rf_rst_inst7_n_0 : STD_LOGIC;
  signal rams_sp_rf_rst_inst7_n_1 : STD_LOGIC;
  signal rams_sp_rf_rst_inst7_n_10 : STD_LOGIC;
  signal rams_sp_rf_rst_inst7_n_11 : STD_LOGIC;
  signal rams_sp_rf_rst_inst7_n_12 : STD_LOGIC;
  signal rams_sp_rf_rst_inst7_n_13 : STD_LOGIC;
  signal rams_sp_rf_rst_inst7_n_14 : STD_LOGIC;
  signal rams_sp_rf_rst_inst7_n_15 : STD_LOGIC;
  signal rams_sp_rf_rst_inst7_n_16 : STD_LOGIC;
  signal rams_sp_rf_rst_inst7_n_17 : STD_LOGIC;
  signal rams_sp_rf_rst_inst7_n_18 : STD_LOGIC;
  signal rams_sp_rf_rst_inst7_n_19 : STD_LOGIC;
  signal rams_sp_rf_rst_inst7_n_2 : STD_LOGIC;
  signal rams_sp_rf_rst_inst7_n_20 : STD_LOGIC;
  signal rams_sp_rf_rst_inst7_n_21 : STD_LOGIC;
  signal rams_sp_rf_rst_inst7_n_22 : STD_LOGIC;
  signal rams_sp_rf_rst_inst7_n_23 : STD_LOGIC;
  signal rams_sp_rf_rst_inst7_n_24 : STD_LOGIC;
  signal rams_sp_rf_rst_inst7_n_25 : STD_LOGIC;
  signal rams_sp_rf_rst_inst7_n_26 : STD_LOGIC;
  signal rams_sp_rf_rst_inst7_n_27 : STD_LOGIC;
  signal rams_sp_rf_rst_inst7_n_28 : STD_LOGIC;
  signal rams_sp_rf_rst_inst7_n_29 : STD_LOGIC;
  signal rams_sp_rf_rst_inst7_n_3 : STD_LOGIC;
  signal rams_sp_rf_rst_inst7_n_30 : STD_LOGIC;
  signal rams_sp_rf_rst_inst7_n_31 : STD_LOGIC;
  signal rams_sp_rf_rst_inst7_n_4 : STD_LOGIC;
  signal rams_sp_rf_rst_inst7_n_5 : STD_LOGIC;
  signal rams_sp_rf_rst_inst7_n_6 : STD_LOGIC;
  signal rams_sp_rf_rst_inst7_n_7 : STD_LOGIC;
  signal rams_sp_rf_rst_inst7_n_8 : STD_LOGIC;
  signal rams_sp_rf_rst_inst7_n_9 : STD_LOGIC;
  signal rams_sp_rf_rst_instb_n_0 : STD_LOGIC;
  signal rams_sp_rf_rst_instb_n_1 : STD_LOGIC;
  signal rams_sp_rf_rst_instb_n_10 : STD_LOGIC;
  signal rams_sp_rf_rst_instb_n_11 : STD_LOGIC;
  signal rams_sp_rf_rst_instb_n_12 : STD_LOGIC;
  signal rams_sp_rf_rst_instb_n_13 : STD_LOGIC;
  signal rams_sp_rf_rst_instb_n_14 : STD_LOGIC;
  signal rams_sp_rf_rst_instb_n_15 : STD_LOGIC;
  signal rams_sp_rf_rst_instb_n_16 : STD_LOGIC;
  signal rams_sp_rf_rst_instb_n_17 : STD_LOGIC;
  signal rams_sp_rf_rst_instb_n_18 : STD_LOGIC;
  signal rams_sp_rf_rst_instb_n_19 : STD_LOGIC;
  signal rams_sp_rf_rst_instb_n_2 : STD_LOGIC;
  signal rams_sp_rf_rst_instb_n_20 : STD_LOGIC;
  signal rams_sp_rf_rst_instb_n_21 : STD_LOGIC;
  signal rams_sp_rf_rst_instb_n_22 : STD_LOGIC;
  signal rams_sp_rf_rst_instb_n_23 : STD_LOGIC;
  signal rams_sp_rf_rst_instb_n_24 : STD_LOGIC;
  signal rams_sp_rf_rst_instb_n_25 : STD_LOGIC;
  signal rams_sp_rf_rst_instb_n_26 : STD_LOGIC;
  signal rams_sp_rf_rst_instb_n_27 : STD_LOGIC;
  signal rams_sp_rf_rst_instb_n_28 : STD_LOGIC;
  signal rams_sp_rf_rst_instb_n_29 : STD_LOGIC;
  signal rams_sp_rf_rst_instb_n_3 : STD_LOGIC;
  signal rams_sp_rf_rst_instb_n_30 : STD_LOGIC;
  signal rams_sp_rf_rst_instb_n_31 : STD_LOGIC;
  signal rams_sp_rf_rst_instb_n_4 : STD_LOGIC;
  signal rams_sp_rf_rst_instb_n_5 : STD_LOGIC;
  signal rams_sp_rf_rst_instb_n_6 : STD_LOGIC;
  signal rams_sp_rf_rst_instb_n_7 : STD_LOGIC;
  signal rams_sp_rf_rst_instb_n_8 : STD_LOGIC;
  signal rams_sp_rf_rst_instb_n_9 : STD_LOGIC;
  signal rams_sp_rf_rst_instf_n_0 : STD_LOGIC;
  signal rams_sp_rf_rst_instf_n_1 : STD_LOGIC;
  signal rams_sp_rf_rst_instf_n_10 : STD_LOGIC;
  signal rams_sp_rf_rst_instf_n_11 : STD_LOGIC;
  signal rams_sp_rf_rst_instf_n_12 : STD_LOGIC;
  signal rams_sp_rf_rst_instf_n_13 : STD_LOGIC;
  signal rams_sp_rf_rst_instf_n_14 : STD_LOGIC;
  signal rams_sp_rf_rst_instf_n_15 : STD_LOGIC;
  signal rams_sp_rf_rst_instf_n_16 : STD_LOGIC;
  signal rams_sp_rf_rst_instf_n_17 : STD_LOGIC;
  signal rams_sp_rf_rst_instf_n_18 : STD_LOGIC;
  signal rams_sp_rf_rst_instf_n_19 : STD_LOGIC;
  signal rams_sp_rf_rst_instf_n_2 : STD_LOGIC;
  signal rams_sp_rf_rst_instf_n_20 : STD_LOGIC;
  signal rams_sp_rf_rst_instf_n_21 : STD_LOGIC;
  signal rams_sp_rf_rst_instf_n_22 : STD_LOGIC;
  signal rams_sp_rf_rst_instf_n_23 : STD_LOGIC;
  signal rams_sp_rf_rst_instf_n_24 : STD_LOGIC;
  signal rams_sp_rf_rst_instf_n_25 : STD_LOGIC;
  signal rams_sp_rf_rst_instf_n_26 : STD_LOGIC;
  signal rams_sp_rf_rst_instf_n_27 : STD_LOGIC;
  signal rams_sp_rf_rst_instf_n_28 : STD_LOGIC;
  signal rams_sp_rf_rst_instf_n_29 : STD_LOGIC;
  signal rams_sp_rf_rst_instf_n_3 : STD_LOGIC;
  signal rams_sp_rf_rst_instf_n_30 : STD_LOGIC;
  signal rams_sp_rf_rst_instf_n_31 : STD_LOGIC;
  signal rams_sp_rf_rst_instf_n_4 : STD_LOGIC;
  signal rams_sp_rf_rst_instf_n_5 : STD_LOGIC;
  signal rams_sp_rf_rst_instf_n_6 : STD_LOGIC;
  signal rams_sp_rf_rst_instf_n_7 : STD_LOGIC;
  signal rams_sp_rf_rst_instf_n_8 : STD_LOGIC;
  signal rams_sp_rf_rst_instf_n_9 : STD_LOGIC;
  signal square_r : STD_LOGIC;
  signal square_r_i_1_n_0 : STD_LOGIC;
  signal \NLW_cnt_inner_valid0_carry__2_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_cnt_inner_valid0_carry__2_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \EN_r[14]_i_1\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \EN_r[15]_i_2\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \EN_r[7]_i_1\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \EN_r[8]_i_1\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \EN_r[9]_i_1\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \EN_r[9]_i_2\ : label is "soft_lutpair2";
  attribute ORIG_CELL_NAME : string;
  attribute ORIG_CELL_NAME of \adr_wr_reg[0]\ : label is "adr_wr_reg[0]";
  attribute ORIG_CELL_NAME of \adr_wr_reg[0]_rep\ : label is "adr_wr_reg[0]";
  attribute ORIG_CELL_NAME of \adr_wr_reg[10]\ : label is "adr_wr_reg[10]";
  attribute ORIG_CELL_NAME of \adr_wr_reg[10]_rep\ : label is "adr_wr_reg[10]";
  attribute ORIG_CELL_NAME of \adr_wr_reg[1]\ : label is "adr_wr_reg[1]";
  attribute ORIG_CELL_NAME of \adr_wr_reg[1]_rep\ : label is "adr_wr_reg[1]";
  attribute ORIG_CELL_NAME of \adr_wr_reg[2]\ : label is "adr_wr_reg[2]";
  attribute ORIG_CELL_NAME of \adr_wr_reg[2]_rep\ : label is "adr_wr_reg[2]";
  attribute ORIG_CELL_NAME of \adr_wr_reg[3]\ : label is "adr_wr_reg[3]";
  attribute ORIG_CELL_NAME of \adr_wr_reg[3]_rep\ : label is "adr_wr_reg[3]";
  attribute ORIG_CELL_NAME of \adr_wr_reg[4]\ : label is "adr_wr_reg[4]";
  attribute ORIG_CELL_NAME of \adr_wr_reg[4]_rep\ : label is "adr_wr_reg[4]";
  attribute ORIG_CELL_NAME of \adr_wr_reg[5]\ : label is "adr_wr_reg[5]";
  attribute ORIG_CELL_NAME of \adr_wr_reg[5]_rep\ : label is "adr_wr_reg[5]";
  attribute ORIG_CELL_NAME of \adr_wr_reg[6]\ : label is "adr_wr_reg[6]";
  attribute ORIG_CELL_NAME of \adr_wr_reg[6]_rep\ : label is "adr_wr_reg[6]";
  attribute ORIG_CELL_NAME of \adr_wr_reg[7]\ : label is "adr_wr_reg[7]";
  attribute ORIG_CELL_NAME of \adr_wr_reg[7]_rep\ : label is "adr_wr_reg[7]";
  attribute ORIG_CELL_NAME of \adr_wr_reg[8]\ : label is "adr_wr_reg[8]";
  attribute ORIG_CELL_NAME of \adr_wr_reg[8]_rep\ : label is "adr_wr_reg[8]";
  attribute ORIG_CELL_NAME of \adr_wr_reg[9]\ : label is "adr_wr_reg[9]";
  attribute ORIG_CELL_NAME of \adr_wr_reg[9]_rep\ : label is "adr_wr_reg[9]";
  attribute SOFT_HLUTNM of \cnt_inner_valid[0]_i_1\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of m00_axis_tvalid_r_i_1 : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of m00_axis_tvalid_r_i_3 : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of m00_axis_tvalid_r_i_4 : label is "soft_lutpair2";
begin
\DI0_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => s00_axis_tvalid,
      D => s00_axis_tdata(0),
      Q => DI0(0),
      R => '0'
    );
\DI0_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => s00_axis_tvalid,
      D => s00_axis_tdata(10),
      Q => DI0(10),
      R => '0'
    );
\DI0_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => s00_axis_tvalid,
      D => s00_axis_tdata(11),
      Q => DI0(11),
      R => '0'
    );
\DI0_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => s00_axis_tvalid,
      D => s00_axis_tdata(12),
      Q => DI0(12),
      R => '0'
    );
\DI0_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => s00_axis_tvalid,
      D => s00_axis_tdata(13),
      Q => DI0(13),
      R => '0'
    );
\DI0_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => s00_axis_tvalid,
      D => s00_axis_tdata(14),
      Q => DI0(14),
      R => '0'
    );
\DI0_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => s00_axis_tvalid,
      D => s00_axis_tdata(15),
      Q => DI0(15),
      R => '0'
    );
\DI0_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => s00_axis_tvalid,
      D => s00_axis_tdata(16),
      Q => DI0(16),
      R => '0'
    );
\DI0_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => s00_axis_tvalid,
      D => s00_axis_tdata(17),
      Q => DI0(17),
      R => '0'
    );
\DI0_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => s00_axis_tvalid,
      D => s00_axis_tdata(18),
      Q => DI0(18),
      R => '0'
    );
\DI0_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => s00_axis_tvalid,
      D => s00_axis_tdata(19),
      Q => DI0(19),
      R => '0'
    );
\DI0_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => s00_axis_tvalid,
      D => s00_axis_tdata(1),
      Q => DI0(1),
      R => '0'
    );
\DI0_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => s00_axis_tvalid,
      D => s00_axis_tdata(20),
      Q => DI0(20),
      R => '0'
    );
\DI0_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => s00_axis_tvalid,
      D => s00_axis_tdata(21),
      Q => DI0(21),
      R => '0'
    );
\DI0_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => s00_axis_tvalid,
      D => s00_axis_tdata(22),
      Q => DI0(22),
      R => '0'
    );
\DI0_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => s00_axis_tvalid,
      D => s00_axis_tdata(23),
      Q => DI0(23),
      R => '0'
    );
\DI0_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => s00_axis_tvalid,
      D => s00_axis_tdata(24),
      Q => DI0(24),
      R => '0'
    );
\DI0_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => s00_axis_tvalid,
      D => s00_axis_tdata(25),
      Q => DI0(25),
      R => '0'
    );
\DI0_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => s00_axis_tvalid,
      D => s00_axis_tdata(26),
      Q => DI0(26),
      R => '0'
    );
\DI0_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => s00_axis_tvalid,
      D => s00_axis_tdata(27),
      Q => DI0(27),
      R => '0'
    );
\DI0_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => s00_axis_tvalid,
      D => s00_axis_tdata(28),
      Q => DI0(28),
      R => '0'
    );
\DI0_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => s00_axis_tvalid,
      D => s00_axis_tdata(29),
      Q => DI0(29),
      R => '0'
    );
\DI0_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => s00_axis_tvalid,
      D => s00_axis_tdata(2),
      Q => DI0(2),
      R => '0'
    );
\DI0_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => s00_axis_tvalid,
      D => s00_axis_tdata(30),
      Q => DI0(30),
      R => '0'
    );
\DI0_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => s00_axis_tvalid,
      D => s00_axis_tdata(31),
      Q => DI0(31),
      R => '0'
    );
\DI0_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => s00_axis_tvalid,
      D => s00_axis_tdata(3),
      Q => DI0(3),
      R => '0'
    );
\DI0_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => s00_axis_tvalid,
      D => s00_axis_tdata(4),
      Q => DI0(4),
      R => '0'
    );
\DI0_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => s00_axis_tvalid,
      D => s00_axis_tdata(5),
      Q => DI0(5),
      R => '0'
    );
\DI0_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => s00_axis_tvalid,
      D => s00_axis_tdata(6),
      Q => DI0(6),
      R => '0'
    );
\DI0_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => s00_axis_tvalid,
      D => s00_axis_tdata(7),
      Q => DI0(7),
      R => '0'
    );
\DI0_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => s00_axis_tvalid,
      D => s00_axis_tdata(8),
      Q => DI0(8),
      R => '0'
    );
\DI0_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => s00_axis_tvalid,
      D => s00_axis_tdata(9),
      Q => DI0(9),
      R => '0'
    );
\EN_r[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000001"
    )
        port map (
      I0 => \cnt_inner_valid_reg_n_0_[4]\,
      I1 => \cnt_inner_valid_reg_n_0_[7]\,
      I2 => \cnt_inner_valid_reg_n_0_[9]\,
      I3 => \cnt_inner_valid_reg_n_0_[8]\,
      I4 => \cnt_inner_valid_reg_n_0_[10]\,
      I5 => m00_axis_tvalid_r_i_2_n_0,
      O => \EN_r[0]_i_1_n_0\
    );
\EN_r[11]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8AAA"
    )
        port map (
      I0 => s00_axis_tvalid,
      I1 => \EN_r[11]_i_2_n_0\,
      I2 => \EN_r[9]_i_2_n_0\,
      I3 => \cnt_inner_valid_reg_n_0_[5]\,
      O => \EN_r[11]_i_1_n_0\
    );
\EN_r[11]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => \cnt_inner_valid_reg_n_0_[12]\,
      I1 => \cnt_inner_valid_reg_n_0_[13]\,
      I2 => \cnt_inner_valid_reg_n_0_[15]\,
      I3 => \cnt_inner_valid_reg_n_0_[14]\,
      I4 => \cnt_inner_valid_reg_n_0_[11]\,
      I5 => \cnt_inner_valid_reg_n_0_[6]\,
      O => \EN_r[11]_i_2_n_0\
    );
\EN_r[13]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8AAA"
    )
        port map (
      I0 => s00_axis_tvalid,
      I1 => \cnt_inner_valid_reg_n_0_[5]\,
      I2 => \EN_r[15]_i_3_n_0\,
      I3 => \EN_r[9]_i_2_n_0\,
      O => \EN_r[13]_i_1_n_0\
    );
\EN_r[14]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \cnt_inner_valid_reg_n_0_[7]\,
      I1 => \cnt_inner_valid_reg_n_0_[4]\,
      O => \EN_r[14]_i_1_n_0\
    );
\EN_r[15]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2AAA"
    )
        port map (
      I0 => s00_axis_tvalid,
      I1 => \EN_r[15]_i_3_n_0\,
      I2 => \EN_r[9]_i_2_n_0\,
      I3 => \cnt_inner_valid_reg_n_0_[5]\,
      O => \EN_r[15]_i_1_n_0\
    );
\EN_r[15]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \cnt_inner_valid_reg_n_0_[4]\,
      I1 => \cnt_inner_valid_reg_n_0_[7]\,
      O => \EN_r[15]_i_2_n_0\
    );
\EN_r[15]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000002"
    )
        port map (
      I0 => \cnt_inner_valid_reg_n_0_[6]\,
      I1 => \cnt_inner_valid_reg_n_0_[12]\,
      I2 => \cnt_inner_valid_reg_n_0_[13]\,
      I3 => \cnt_inner_valid_reg_n_0_[15]\,
      I4 => \cnt_inner_valid_reg_n_0_[14]\,
      I5 => \cnt_inner_valid_reg_n_0_[11]\,
      O => \EN_r[15]_i_3_n_0\
    );
\EN_r[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000002"
    )
        port map (
      I0 => \cnt_inner_valid_reg_n_0_[4]\,
      I1 => \cnt_inner_valid_reg_n_0_[7]\,
      I2 => \cnt_inner_valid_reg_n_0_[9]\,
      I3 => \cnt_inner_valid_reg_n_0_[8]\,
      I4 => \cnt_inner_valid_reg_n_0_[10]\,
      I5 => m00_axis_tvalid_r_i_2_n_0,
      O => \EN_r[1]_i_1_n_0\
    );
\EN_r[6]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \cnt_inner_valid_reg_n_0_[4]\,
      I1 => \cnt_inner_valid_reg_n_0_[7]\,
      O => \EN_r[6]_i_1_n_0\
    );
\EN_r[7]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \cnt_inner_valid_reg_n_0_[4]\,
      I1 => \cnt_inner_valid_reg_n_0_[7]\,
      O => \EN_r[7]_i_1_n_0\
    );
\EN_r[8]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0200"
    )
        port map (
      I0 => \EN_r[9]_i_2_n_0\,
      I1 => m00_axis_tvalid_r_i_2_n_0,
      I2 => \cnt_inner_valid_reg_n_0_[4]\,
      I3 => \cnt_inner_valid_reg_n_0_[7]\,
      O => \EN_r[8]_i_1_n_0\
    );
\EN_r[9]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0080"
    )
        port map (
      I0 => \cnt_inner_valid_reg_n_0_[4]\,
      I1 => \cnt_inner_valid_reg_n_0_[7]\,
      I2 => \EN_r[9]_i_2_n_0\,
      I3 => m00_axis_tvalid_r_i_2_n_0,
      O => \EN_r[9]_i_1_n_0\
    );
\EN_r[9]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0100"
    )
        port map (
      I0 => \cnt_inner_valid_reg_n_0_[10]\,
      I1 => \cnt_inner_valid_reg_n_0_[8]\,
      I2 => \cnt_inner_valid_reg_n_0_[9]\,
      I3 => m00_axis_tvalid_r_i_3_n_0,
      O => \EN_r[9]_i_2_n_0\
    );
\EN_r_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => s00_axis_tvalid,
      D => \EN_r[0]_i_1_n_0\,
      Q => EN_r(0),
      R => '0'
    );
\EN_r_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => s00_axis_tvalid,
      D => \EN_r[14]_i_1_n_0\,
      Q => EN_r(10),
      R => \EN_r[11]_i_1_n_0\
    );
\EN_r_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => s00_axis_tvalid,
      D => \EN_r[15]_i_2_n_0\,
      Q => EN_r(11),
      R => \EN_r[11]_i_1_n_0\
    );
\EN_r_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => s00_axis_tvalid,
      D => \EN_r[14]_i_1_n_0\,
      Q => EN_r(12),
      R => \EN_r[13]_i_1_n_0\
    );
\EN_r_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => s00_axis_tvalid,
      D => \EN_r[15]_i_2_n_0\,
      Q => EN_r(13),
      R => \EN_r[13]_i_1_n_0\
    );
\EN_r_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => s00_axis_tvalid,
      D => \EN_r[14]_i_1_n_0\,
      Q => EN_r(14),
      R => \EN_r[15]_i_1_n_0\
    );
\EN_r_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => s00_axis_tvalid,
      D => \EN_r[15]_i_2_n_0\,
      Q => EN_r(15),
      R => \EN_r[15]_i_1_n_0\
    );
\EN_r_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => s00_axis_tvalid,
      D => \EN_r[1]_i_1_n_0\,
      Q => EN_r(1),
      R => '0'
    );
\EN_r_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => s00_axis_tvalid,
      D => \EN_r[6]_i_1_n_0\,
      Q => EN_r(2),
      R => \EN_r[11]_i_1_n_0\
    );
\EN_r_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => s00_axis_tvalid,
      D => \EN_r[7]_i_1_n_0\,
      Q => EN_r(3),
      R => \EN_r[11]_i_1_n_0\
    );
\EN_r_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => s00_axis_tvalid,
      D => \EN_r[6]_i_1_n_0\,
      Q => EN_r(4),
      R => \EN_r[13]_i_1_n_0\
    );
\EN_r_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => s00_axis_tvalid,
      D => \EN_r[7]_i_1_n_0\,
      Q => EN_r(5),
      R => \EN_r[13]_i_1_n_0\
    );
\EN_r_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => s00_axis_tvalid,
      D => \EN_r[6]_i_1_n_0\,
      Q => EN_r(6),
      R => \EN_r[15]_i_1_n_0\
    );
\EN_r_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => s00_axis_tvalid,
      D => \EN_r[7]_i_1_n_0\,
      Q => EN_r(7),
      R => \EN_r[15]_i_1_n_0\
    );
\EN_r_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => s00_axis_tvalid,
      D => \EN_r[8]_i_1_n_0\,
      Q => EN_r(8),
      R => '0'
    );
\EN_r_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => s00_axis_tvalid,
      D => \EN_r[9]_i_1_n_0\,
      Q => EN_r(9),
      R => '0'
    );
WE_r_reg: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => s00_axis_tvalid,
      Q => WE_r,
      R => '0'
    );
\adr_wr_reg[0]\: unisim.vcomponents.FDSE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => s00_axis_tuser(0),
      Q => adr_wr(0),
      S => p_0_in
    );
\adr_wr_reg[0]_rep\: unisim.vcomponents.FDSE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => s00_axis_tuser(0),
      Q => \adr_wr_reg[0]_rep_n_0\,
      S => p_0_in
    );
\adr_wr_reg[10]\: unisim.vcomponents.FDSE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => s00_axis_tuser(10),
      Q => adr_wr(10),
      S => p_0_in
    );
\adr_wr_reg[10]_rep\: unisim.vcomponents.FDSE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => s00_axis_tuser(10),
      Q => \adr_wr_reg[10]_rep_n_0\,
      S => p_0_in
    );
\adr_wr_reg[1]\: unisim.vcomponents.FDSE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => s00_axis_tuser(1),
      Q => adr_wr(1),
      S => p_0_in
    );
\adr_wr_reg[1]_rep\: unisim.vcomponents.FDSE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => s00_axis_tuser(1),
      Q => \adr_wr_reg[1]_rep_n_0\,
      S => p_0_in
    );
\adr_wr_reg[2]\: unisim.vcomponents.FDSE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => s00_axis_tuser(2),
      Q => adr_wr(2),
      S => p_0_in
    );
\adr_wr_reg[2]_rep\: unisim.vcomponents.FDSE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => s00_axis_tuser(2),
      Q => \adr_wr_reg[2]_rep_n_0\,
      S => p_0_in
    );
\adr_wr_reg[3]\: unisim.vcomponents.FDSE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => s00_axis_tuser(3),
      Q => adr_wr(3),
      S => p_0_in
    );
\adr_wr_reg[3]_rep\: unisim.vcomponents.FDSE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => s00_axis_tuser(3),
      Q => \adr_wr_reg[3]_rep_n_0\,
      S => p_0_in
    );
\adr_wr_reg[4]\: unisim.vcomponents.FDSE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => s00_axis_tuser(4),
      Q => adr_wr(4),
      S => p_0_in
    );
\adr_wr_reg[4]_rep\: unisim.vcomponents.FDSE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => s00_axis_tuser(4),
      Q => \adr_wr_reg[4]_rep_n_0\,
      S => p_0_in
    );
\adr_wr_reg[5]\: unisim.vcomponents.FDSE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => s00_axis_tuser(5),
      Q => adr_wr(5),
      S => p_0_in
    );
\adr_wr_reg[5]_rep\: unisim.vcomponents.FDSE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => s00_axis_tuser(5),
      Q => \adr_wr_reg[5]_rep_n_0\,
      S => p_0_in
    );
\adr_wr_reg[6]\: unisim.vcomponents.FDSE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => s00_axis_tuser(6),
      Q => adr_wr(6),
      S => p_0_in
    );
\adr_wr_reg[6]_rep\: unisim.vcomponents.FDSE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => s00_axis_tuser(6),
      Q => \adr_wr_reg[6]_rep_n_0\,
      S => p_0_in
    );
\adr_wr_reg[7]\: unisim.vcomponents.FDSE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => s00_axis_tuser(7),
      Q => adr_wr(7),
      S => p_0_in
    );
\adr_wr_reg[7]_rep\: unisim.vcomponents.FDSE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => s00_axis_tuser(7),
      Q => \adr_wr_reg[7]_rep_n_0\,
      S => p_0_in
    );
\adr_wr_reg[8]\: unisim.vcomponents.FDSE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => s00_axis_tuser(8),
      Q => adr_wr(8),
      S => p_0_in
    );
\adr_wr_reg[8]_rep\: unisim.vcomponents.FDSE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => s00_axis_tuser(8),
      Q => \adr_wr_reg[8]_rep_n_0\,
      S => p_0_in
    );
\adr_wr_reg[9]\: unisim.vcomponents.FDSE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => s00_axis_tuser(9),
      Q => adr_wr(9),
      S => p_0_in
    );
\adr_wr_reg[9]_rep\: unisim.vcomponents.FDSE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => s00_axis_tuser(9),
      Q => \adr_wr_reg[9]_rep_n_0\,
      S => p_0_in
    );
cnt_inner_valid0_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => cnt_inner_valid0_carry_n_0,
      CO(2) => cnt_inner_valid0_carry_n_1,
      CO(1) => cnt_inner_valid0_carry_n_2,
      CO(0) => cnt_inner_valid0_carry_n_3,
      CYINIT => \cnt_inner_valid_reg_n_0_[0]\,
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => data0(4 downto 1),
      S(3) => \cnt_inner_valid_reg_n_0_[4]\,
      S(2) => \cnt_inner_valid_reg_n_0_[3]\,
      S(1) => \cnt_inner_valid_reg_n_0_[2]\,
      S(0) => \cnt_inner_valid_reg_n_0_[1]\
    );
\cnt_inner_valid0_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => cnt_inner_valid0_carry_n_0,
      CO(3) => \cnt_inner_valid0_carry__0_n_0\,
      CO(2) => \cnt_inner_valid0_carry__0_n_1\,
      CO(1) => \cnt_inner_valid0_carry__0_n_2\,
      CO(0) => \cnt_inner_valid0_carry__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => data0(8 downto 5),
      S(3) => \cnt_inner_valid_reg_n_0_[8]\,
      S(2) => \cnt_inner_valid_reg_n_0_[7]\,
      S(1) => \cnt_inner_valid_reg_n_0_[6]\,
      S(0) => \cnt_inner_valid_reg_n_0_[5]\
    );
\cnt_inner_valid0_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \cnt_inner_valid0_carry__0_n_0\,
      CO(3) => \cnt_inner_valid0_carry__1_n_0\,
      CO(2) => \cnt_inner_valid0_carry__1_n_1\,
      CO(1) => \cnt_inner_valid0_carry__1_n_2\,
      CO(0) => \cnt_inner_valid0_carry__1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => data0(12 downto 9),
      S(3) => \cnt_inner_valid_reg_n_0_[12]\,
      S(2) => \cnt_inner_valid_reg_n_0_[11]\,
      S(1) => \cnt_inner_valid_reg_n_0_[10]\,
      S(0) => \cnt_inner_valid_reg_n_0_[9]\
    );
\cnt_inner_valid0_carry__2\: unisim.vcomponents.CARRY4
     port map (
      CI => \cnt_inner_valid0_carry__1_n_0\,
      CO(3 downto 2) => \NLW_cnt_inner_valid0_carry__2_CO_UNCONNECTED\(3 downto 2),
      CO(1) => \cnt_inner_valid0_carry__2_n_2\,
      CO(0) => \cnt_inner_valid0_carry__2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \NLW_cnt_inner_valid0_carry__2_O_UNCONNECTED\(3),
      O(2 downto 0) => data0(15 downto 13),
      S(3) => '0',
      S(2) => \cnt_inner_valid_reg_n_0_[15]\,
      S(1) => \cnt_inner_valid_reg_n_0_[14]\,
      S(0) => \cnt_inner_valid_reg_n_0_[13]\
    );
\cnt_inner_valid[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00FF00FE"
    )
        port map (
      I0 => \cnt_inner_valid_reg_n_0_[3]\,
      I1 => \cnt_inner_valid_reg_n_0_[2]\,
      I2 => \cnt_inner_valid_reg_n_0_[1]\,
      I3 => \cnt_inner_valid_reg_n_0_[0]\,
      I4 => \cnt_inner_valid[15]_i_3_n_0\,
      O => cnt_inner_valid(0)
    );
\cnt_inner_valid[10]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFE00000000"
    )
        port map (
      I0 => \cnt_inner_valid[15]_i_3_n_0\,
      I1 => \cnt_inner_valid_reg_n_0_[0]\,
      I2 => \cnt_inner_valid_reg_n_0_[3]\,
      I3 => \cnt_inner_valid_reg_n_0_[2]\,
      I4 => \cnt_inner_valid_reg_n_0_[1]\,
      I5 => data0(10),
      O => cnt_inner_valid(10)
    );
\cnt_inner_valid[11]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFE00000000"
    )
        port map (
      I0 => \cnt_inner_valid[15]_i_3_n_0\,
      I1 => \cnt_inner_valid_reg_n_0_[0]\,
      I2 => \cnt_inner_valid_reg_n_0_[3]\,
      I3 => \cnt_inner_valid_reg_n_0_[2]\,
      I4 => \cnt_inner_valid_reg_n_0_[1]\,
      I5 => data0(11),
      O => cnt_inner_valid(11)
    );
\cnt_inner_valid[12]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFE00000000"
    )
        port map (
      I0 => \cnt_inner_valid[15]_i_3_n_0\,
      I1 => \cnt_inner_valid_reg_n_0_[0]\,
      I2 => \cnt_inner_valid_reg_n_0_[3]\,
      I3 => \cnt_inner_valid_reg_n_0_[2]\,
      I4 => \cnt_inner_valid_reg_n_0_[1]\,
      I5 => data0(12),
      O => cnt_inner_valid(12)
    );
\cnt_inner_valid[13]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFE00000000"
    )
        port map (
      I0 => \cnt_inner_valid[15]_i_3_n_0\,
      I1 => \cnt_inner_valid_reg_n_0_[0]\,
      I2 => \cnt_inner_valid_reg_n_0_[3]\,
      I3 => \cnt_inner_valid_reg_n_0_[2]\,
      I4 => \cnt_inner_valid_reg_n_0_[1]\,
      I5 => data0(13),
      O => cnt_inner_valid(13)
    );
\cnt_inner_valid[14]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFE00000000"
    )
        port map (
      I0 => \cnt_inner_valid[15]_i_3_n_0\,
      I1 => \cnt_inner_valid_reg_n_0_[0]\,
      I2 => \cnt_inner_valid_reg_n_0_[3]\,
      I3 => \cnt_inner_valid_reg_n_0_[2]\,
      I4 => \cnt_inner_valid_reg_n_0_[1]\,
      I5 => data0(14),
      O => cnt_inner_valid(14)
    );
\cnt_inner_valid[15]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => s00_axis_tvalid,
      O => p_0_in
    );
\cnt_inner_valid[15]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFE00000000"
    )
        port map (
      I0 => \cnt_inner_valid[15]_i_3_n_0\,
      I1 => \cnt_inner_valid_reg_n_0_[0]\,
      I2 => \cnt_inner_valid_reg_n_0_[3]\,
      I3 => \cnt_inner_valid_reg_n_0_[2]\,
      I4 => \cnt_inner_valid_reg_n_0_[1]\,
      I5 => data0(15),
      O => cnt_inner_valid(15)
    );
\cnt_inner_valid[15]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFF7FFFFFFFFF"
    )
        port map (
      I0 => \cnt_inner_valid_reg_n_0_[8]\,
      I1 => \cnt_inner_valid_reg_n_0_[9]\,
      I2 => \cnt_inner_valid_reg_n_0_[10]\,
      I3 => m00_axis_tvalid_r_i_2_n_0,
      I4 => \cnt_inner_valid_reg_n_0_[4]\,
      I5 => \cnt_inner_valid_reg_n_0_[7]\,
      O => \cnt_inner_valid[15]_i_3_n_0\
    );
\cnt_inner_valid[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFE00000000"
    )
        port map (
      I0 => \cnt_inner_valid[15]_i_3_n_0\,
      I1 => \cnt_inner_valid_reg_n_0_[0]\,
      I2 => \cnt_inner_valid_reg_n_0_[3]\,
      I3 => \cnt_inner_valid_reg_n_0_[2]\,
      I4 => \cnt_inner_valid_reg_n_0_[1]\,
      I5 => data0(1),
      O => cnt_inner_valid(1)
    );
\cnt_inner_valid[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFE00000000"
    )
        port map (
      I0 => \cnt_inner_valid[15]_i_3_n_0\,
      I1 => \cnt_inner_valid_reg_n_0_[0]\,
      I2 => \cnt_inner_valid_reg_n_0_[3]\,
      I3 => \cnt_inner_valid_reg_n_0_[2]\,
      I4 => \cnt_inner_valid_reg_n_0_[1]\,
      I5 => data0(2),
      O => cnt_inner_valid(2)
    );
\cnt_inner_valid[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFE00000000"
    )
        port map (
      I0 => \cnt_inner_valid[15]_i_3_n_0\,
      I1 => \cnt_inner_valid_reg_n_0_[0]\,
      I2 => \cnt_inner_valid_reg_n_0_[3]\,
      I3 => \cnt_inner_valid_reg_n_0_[2]\,
      I4 => \cnt_inner_valid_reg_n_0_[1]\,
      I5 => data0(3),
      O => cnt_inner_valid(3)
    );
\cnt_inner_valid[4]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFE00000000"
    )
        port map (
      I0 => \cnt_inner_valid[15]_i_3_n_0\,
      I1 => \cnt_inner_valid_reg_n_0_[0]\,
      I2 => \cnt_inner_valid_reg_n_0_[3]\,
      I3 => \cnt_inner_valid_reg_n_0_[2]\,
      I4 => \cnt_inner_valid_reg_n_0_[1]\,
      I5 => data0(4),
      O => cnt_inner_valid(4)
    );
\cnt_inner_valid[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFE00000000"
    )
        port map (
      I0 => \cnt_inner_valid[15]_i_3_n_0\,
      I1 => \cnt_inner_valid_reg_n_0_[0]\,
      I2 => \cnt_inner_valid_reg_n_0_[3]\,
      I3 => \cnt_inner_valid_reg_n_0_[2]\,
      I4 => \cnt_inner_valid_reg_n_0_[1]\,
      I5 => data0(5),
      O => cnt_inner_valid(5)
    );
\cnt_inner_valid[6]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFE00000000"
    )
        port map (
      I0 => \cnt_inner_valid[15]_i_3_n_0\,
      I1 => \cnt_inner_valid_reg_n_0_[0]\,
      I2 => \cnt_inner_valid_reg_n_0_[3]\,
      I3 => \cnt_inner_valid_reg_n_0_[2]\,
      I4 => \cnt_inner_valid_reg_n_0_[1]\,
      I5 => data0(6),
      O => cnt_inner_valid(6)
    );
\cnt_inner_valid[7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFE00000000"
    )
        port map (
      I0 => \cnt_inner_valid[15]_i_3_n_0\,
      I1 => \cnt_inner_valid_reg_n_0_[0]\,
      I2 => \cnt_inner_valid_reg_n_0_[3]\,
      I3 => \cnt_inner_valid_reg_n_0_[2]\,
      I4 => \cnt_inner_valid_reg_n_0_[1]\,
      I5 => data0(7),
      O => cnt_inner_valid(7)
    );
\cnt_inner_valid[8]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFE00000000"
    )
        port map (
      I0 => \cnt_inner_valid[15]_i_3_n_0\,
      I1 => \cnt_inner_valid_reg_n_0_[0]\,
      I2 => \cnt_inner_valid_reg_n_0_[3]\,
      I3 => \cnt_inner_valid_reg_n_0_[2]\,
      I4 => \cnt_inner_valid_reg_n_0_[1]\,
      I5 => data0(8),
      O => cnt_inner_valid(8)
    );
\cnt_inner_valid[9]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFE00000000"
    )
        port map (
      I0 => \cnt_inner_valid[15]_i_3_n_0\,
      I1 => \cnt_inner_valid_reg_n_0_[0]\,
      I2 => \cnt_inner_valid_reg_n_0_[3]\,
      I3 => \cnt_inner_valid_reg_n_0_[2]\,
      I4 => \cnt_inner_valid_reg_n_0_[1]\,
      I5 => data0(9),
      O => cnt_inner_valid(9)
    );
\cnt_inner_valid_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => cnt_inner_valid(0),
      Q => \cnt_inner_valid_reg_n_0_[0]\,
      R => p_0_in
    );
\cnt_inner_valid_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => cnt_inner_valid(10),
      Q => \cnt_inner_valid_reg_n_0_[10]\,
      R => p_0_in
    );
\cnt_inner_valid_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => cnt_inner_valid(11),
      Q => \cnt_inner_valid_reg_n_0_[11]\,
      R => p_0_in
    );
\cnt_inner_valid_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => cnt_inner_valid(12),
      Q => \cnt_inner_valid_reg_n_0_[12]\,
      R => p_0_in
    );
\cnt_inner_valid_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => cnt_inner_valid(13),
      Q => \cnt_inner_valid_reg_n_0_[13]\,
      R => p_0_in
    );
\cnt_inner_valid_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => cnt_inner_valid(14),
      Q => \cnt_inner_valid_reg_n_0_[14]\,
      R => p_0_in
    );
\cnt_inner_valid_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => cnt_inner_valid(15),
      Q => \cnt_inner_valid_reg_n_0_[15]\,
      R => p_0_in
    );
\cnt_inner_valid_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => cnt_inner_valid(1),
      Q => \cnt_inner_valid_reg_n_0_[1]\,
      R => p_0_in
    );
\cnt_inner_valid_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => cnt_inner_valid(2),
      Q => \cnt_inner_valid_reg_n_0_[2]\,
      R => p_0_in
    );
\cnt_inner_valid_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => cnt_inner_valid(3),
      Q => \cnt_inner_valid_reg_n_0_[3]\,
      R => p_0_in
    );
\cnt_inner_valid_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => cnt_inner_valid(4),
      Q => \cnt_inner_valid_reg_n_0_[4]\,
      R => p_0_in
    );
\cnt_inner_valid_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => cnt_inner_valid(5),
      Q => \cnt_inner_valid_reg_n_0_[5]\,
      R => p_0_in
    );
\cnt_inner_valid_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => cnt_inner_valid(6),
      Q => \cnt_inner_valid_reg_n_0_[6]\,
      R => p_0_in
    );
\cnt_inner_valid_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => cnt_inner_valid(7),
      Q => \cnt_inner_valid_reg_n_0_[7]\,
      R => p_0_in
    );
\cnt_inner_valid_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => cnt_inner_valid(8),
      Q => \cnt_inner_valid_reg_n_0_[8]\,
      R => p_0_in
    );
\cnt_inner_valid_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => cnt_inner_valid(9),
      Q => \cnt_inner_valid_reg_n_0_[9]\,
      R => p_0_in
    );
\m00_axis_tdata_r[31]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => \m00_axis_tdata_r[31]_i_3_n_0\,
      I1 => \m00_axis_tdata_r[31]_i_4_n_0\,
      I2 => s00_axis_tvalid,
      O => \m00_axis_tdata_r[31]_i_1_n_0\
    );
\m00_axis_tdata_r[31]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000001"
    )
        port map (
      I0 => s00_axis_tuser(19),
      I1 => s00_axis_tuser(24),
      I2 => s00_axis_tuser(26),
      I3 => s00_axis_tuser(21),
      I4 => s00_axis_tuser(25),
      I5 => s00_axis_tuser(22),
      O => \m00_axis_tdata_r[31]_i_3_n_0\
    );
\m00_axis_tdata_r[31]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFE0FFFF"
    )
        port map (
      I0 => s00_axis_tuser(16),
      I1 => s00_axis_tuser(17),
      I2 => s00_axis_tuser(18),
      I3 => s00_axis_tuser(23),
      I4 => s00_axis_tuser(15),
      I5 => s00_axis_tuser(20),
      O => \m00_axis_tdata_r[31]_i_4_n_0\
    );
\m00_axis_tdata_r_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \m00_axis_tdata_r[31]_i_1_n_0\,
      D => rams_sp_rf_rst_inst7_n_8,
      Q => m00_axis_tdata(0),
      R => '0'
    );
\m00_axis_tdata_r_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \m00_axis_tdata_r[31]_i_1_n_0\,
      D => rams_sp_rf_rst_instf_n_14,
      Q => m00_axis_tdata(10),
      R => '0'
    );
\m00_axis_tdata_r_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \m00_axis_tdata_r[31]_i_1_n_0\,
      D => rams_sp_rf_rst_inst7_n_5,
      Q => m00_axis_tdata(11),
      R => '0'
    );
\m00_axis_tdata_r_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \m00_axis_tdata_r[31]_i_1_n_0\,
      D => rams_sp_rf_rst_instf_n_13,
      Q => m00_axis_tdata(12),
      R => '0'
    );
\m00_axis_tdata_r_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \m00_axis_tdata_r[31]_i_1_n_0\,
      D => rams_sp_rf_rst_instf_n_12,
      Q => m00_axis_tdata(13),
      R => '0'
    );
\m00_axis_tdata_r_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \m00_axis_tdata_r[31]_i_1_n_0\,
      D => rams_sp_rf_rst_instf_n_11,
      Q => m00_axis_tdata(14),
      R => '0'
    );
\m00_axis_tdata_r_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \m00_axis_tdata_r[31]_i_1_n_0\,
      D => rams_sp_rf_rst_inst7_n_4,
      Q => m00_axis_tdata(15),
      R => '0'
    );
\m00_axis_tdata_r_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \m00_axis_tdata_r[31]_i_1_n_0\,
      D => rams_sp_rf_rst_inst7_n_3,
      Q => m00_axis_tdata(16),
      R => '0'
    );
\m00_axis_tdata_r_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \m00_axis_tdata_r[31]_i_1_n_0\,
      D => rams_sp_rf_rst_instf_n_10,
      Q => m00_axis_tdata(17),
      R => '0'
    );
\m00_axis_tdata_r_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \m00_axis_tdata_r[31]_i_1_n_0\,
      D => rams_sp_rf_rst_instf_n_9,
      Q => m00_axis_tdata(18),
      R => '0'
    );
\m00_axis_tdata_r_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \m00_axis_tdata_r[31]_i_1_n_0\,
      D => rams_sp_rf_rst_instf_n_8,
      Q => m00_axis_tdata(19),
      R => '0'
    );
\m00_axis_tdata_r_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \m00_axis_tdata_r[31]_i_1_n_0\,
      D => rams_sp_rf_rst_instf_n_21,
      Q => m00_axis_tdata(1),
      R => '0'
    );
\m00_axis_tdata_r_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \m00_axis_tdata_r[31]_i_1_n_0\,
      D => rams_sp_rf_rst_inst7_n_2,
      Q => m00_axis_tdata(20),
      R => '0'
    );
\m00_axis_tdata_r_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \m00_axis_tdata_r[31]_i_1_n_0\,
      D => rams_sp_rf_rst_instf_n_7,
      Q => m00_axis_tdata(21),
      R => '0'
    );
\m00_axis_tdata_r_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \m00_axis_tdata_r[31]_i_1_n_0\,
      D => rams_sp_rf_rst_instf_n_6,
      Q => m00_axis_tdata(22),
      R => '0'
    );
\m00_axis_tdata_r_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \m00_axis_tdata_r[31]_i_1_n_0\,
      D => rams_sp_rf_rst_instf_n_5,
      Q => m00_axis_tdata(23),
      R => '0'
    );
\m00_axis_tdata_r_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \m00_axis_tdata_r[31]_i_1_n_0\,
      D => rams_sp_rf_rst_instf_n_4,
      Q => m00_axis_tdata(24),
      R => '0'
    );
\m00_axis_tdata_r_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \m00_axis_tdata_r[31]_i_1_n_0\,
      D => rams_sp_rf_rst_inst7_n_1,
      Q => m00_axis_tdata(25),
      R => '0'
    );
\m00_axis_tdata_r_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \m00_axis_tdata_r[31]_i_1_n_0\,
      D => rams_sp_rf_rst_instf_n_3,
      Q => m00_axis_tdata(26),
      R => '0'
    );
\m00_axis_tdata_r_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \m00_axis_tdata_r[31]_i_1_n_0\,
      D => rams_sp_rf_rst_inst7_n_0,
      Q => m00_axis_tdata(27),
      R => '0'
    );
\m00_axis_tdata_r_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \m00_axis_tdata_r[31]_i_1_n_0\,
      D => rams_sp_rf_rst_instf_n_2,
      Q => m00_axis_tdata(28),
      R => '0'
    );
\m00_axis_tdata_r_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \m00_axis_tdata_r[31]_i_1_n_0\,
      D => rams_sp_rf_rst_instf_n_1,
      Q => m00_axis_tdata(29),
      R => '0'
    );
\m00_axis_tdata_r_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \m00_axis_tdata_r[31]_i_1_n_0\,
      D => rams_sp_rf_rst_instf_n_20,
      Q => m00_axis_tdata(2),
      R => '0'
    );
\m00_axis_tdata_r_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \m00_axis_tdata_r[31]_i_1_n_0\,
      D => rams_sp_rf_rst_instf_n_0,
      Q => m00_axis_tdata(30),
      R => '0'
    );
\m00_axis_tdata_r_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \m00_axis_tdata_r[31]_i_1_n_0\,
      D => rams_sp_rf_rst_inst3_n_0,
      Q => m00_axis_tdata(31),
      R => '0'
    );
\m00_axis_tdata_r_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \m00_axis_tdata_r[31]_i_1_n_0\,
      D => rams_sp_rf_rst_instf_n_19,
      Q => m00_axis_tdata(3),
      R => '0'
    );
\m00_axis_tdata_r_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \m00_axis_tdata_r[31]_i_1_n_0\,
      D => rams_sp_rf_rst_inst7_n_7,
      Q => m00_axis_tdata(4),
      R => '0'
    );
\m00_axis_tdata_r_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \m00_axis_tdata_r[31]_i_1_n_0\,
      D => rams_sp_rf_rst_instf_n_18,
      Q => m00_axis_tdata(5),
      R => '0'
    );
\m00_axis_tdata_r_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \m00_axis_tdata_r[31]_i_1_n_0\,
      D => rams_sp_rf_rst_instf_n_17,
      Q => m00_axis_tdata(6),
      R => '0'
    );
\m00_axis_tdata_r_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \m00_axis_tdata_r[31]_i_1_n_0\,
      D => rams_sp_rf_rst_instf_n_16,
      Q => m00_axis_tdata(7),
      R => '0'
    );
\m00_axis_tdata_r_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \m00_axis_tdata_r[31]_i_1_n_0\,
      D => rams_sp_rf_rst_instf_n_15,
      Q => m00_axis_tdata(8),
      R => '0'
    );
\m00_axis_tdata_r_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \m00_axis_tdata_r[31]_i_1_n_0\,
      D => rams_sp_rf_rst_inst7_n_6,
      Q => m00_axis_tdata(9),
      R => '0'
    );
m00_axis_tvalid_r_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00100040"
    )
        port map (
      I0 => m00_axis_tvalid_r_i_2_n_0,
      I1 => m00_axis_tvalid_r_i_3_n_0,
      I2 => m00_axis_tvalid_r_i_4_n_0,
      I3 => \cnt_inner_valid_reg_n_0_[7]\,
      I4 => \cnt_inner_valid_reg_n_0_[4]\,
      O => m00_axis_tvalid_r0
    );
m00_axis_tvalid_r_i_2: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => \EN_r[11]_i_2_n_0\,
      I1 => \cnt_inner_valid_reg_n_0_[5]\,
      O => m00_axis_tvalid_r_i_2_n_0
    );
m00_axis_tvalid_r_i_3: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => \cnt_inner_valid_reg_n_0_[1]\,
      I1 => \cnt_inner_valid_reg_n_0_[2]\,
      I2 => \cnt_inner_valid_reg_n_0_[3]\,
      I3 => \cnt_inner_valid_reg_n_0_[0]\,
      O => m00_axis_tvalid_r_i_3_n_0
    );
m00_axis_tvalid_r_i_4: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => \cnt_inner_valid_reg_n_0_[9]\,
      I1 => \cnt_inner_valid_reg_n_0_[8]\,
      I2 => \cnt_inner_valid_reg_n_0_[10]\,
      O => m00_axis_tvalid_r_i_4_n_0
    );
m00_axis_tvalid_r_reg: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => s00_axis_tvalid,
      D => m00_axis_tvalid_r0,
      Q => m00_axis_tvalid,
      R => '0'
    );
rams_sp_rf_rst_inst0: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst
     port map (
      ADDRARDADDR(10 downto 0) => adr_wr(10 downto 0),
      Q(0) => EN_r(0),
      WE_r => WE_r,
      addr(10) => \adr_wr_reg[10]_rep_n_0\,
      addr(9) => \adr_wr_reg[9]_rep_n_0\,
      addr(8) => \adr_wr_reg[8]_rep_n_0\,
      addr(7) => \adr_wr_reg[7]_rep_n_0\,
      addr(6) => \adr_wr_reg[6]_rep_n_0\,
      addr(5) => \adr_wr_reg[5]_rep_n_0\,
      addr(4) => \adr_wr_reg[4]_rep_n_0\,
      addr(3) => \adr_wr_reg[3]_rep_n_0\,
      addr(2) => \adr_wr_reg[2]_rep_n_0\,
      addr(1) => \adr_wr_reg[1]_rep_n_0\,
      addr(0) => \adr_wr_reg[0]_rep_n_0\,
      dout0_out(31 downto 0) => DOUT0(31 downto 0),
      m00_axis_aclk => m00_axis_aclk,
      ram_reg_1_0(31 downto 0) => DI0(31 downto 0),
      square_r => square_r
    );
rams_sp_rf_rst_inst1: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_0
     port map (
      ADDRARDADDR(10 downto 0) => adr_wr(10 downto 0),
      Q(0) => EN_r(1),
      WE_r => WE_r,
      addr(10) => \adr_wr_reg[10]_rep_n_0\,
      addr(9) => \adr_wr_reg[9]_rep_n_0\,
      addr(8) => \adr_wr_reg[8]_rep_n_0\,
      addr(7) => \adr_wr_reg[7]_rep_n_0\,
      addr(6) => \adr_wr_reg[6]_rep_n_0\,
      addr(5) => \adr_wr_reg[5]_rep_n_0\,
      addr(4) => \adr_wr_reg[4]_rep_n_0\,
      addr(3) => \adr_wr_reg[3]_rep_n_0\,
      addr(2) => \adr_wr_reg[2]_rep_n_0\,
      addr(1) => \adr_wr_reg[1]_rep_n_0\,
      addr(0) => \adr_wr_reg[0]_rep_n_0\,
      dout0_out(31 downto 0) => DOUT1_w(31 downto 0),
      m00_axis_aclk => m00_axis_aclk,
      ram_reg_1_0(31 downto 0) => DI0(31 downto 0),
      square_r => square_r
    );
rams_sp_rf_rst_inst2: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_1
     port map (
      ADDRARDADDR(10 downto 0) => adr_wr(10 downto 0),
      Q(31 downto 0) => DI0(31 downto 0),
      WE_r => WE_r,
      addr(10) => \adr_wr_reg[10]_rep_n_0\,
      addr(9) => \adr_wr_reg[9]_rep_n_0\,
      addr(8) => \adr_wr_reg[8]_rep_n_0\,
      addr(7) => \adr_wr_reg[7]_rep_n_0\,
      addr(6) => \adr_wr_reg[6]_rep_n_0\,
      addr(5) => \adr_wr_reg[5]_rep_n_0\,
      addr(4) => \adr_wr_reg[4]_rep_n_0\,
      addr(3) => \adr_wr_reg[3]_rep_n_0\,
      addr(2) => \adr_wr_reg[2]_rep_n_0\,
      addr(1) => \adr_wr_reg[1]_rep_n_0\,
      addr(0) => \adr_wr_reg[0]_rep_n_0\,
      dout0_out(31 downto 0) => DOUT2_w(31 downto 0),
      en => EN_r(2),
      m00_axis_aclk => m00_axis_aclk,
      square_r => square_r
    );
rams_sp_rf_rst_inst3: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_2
     port map (
      ADDRARDADDR(10 downto 0) => adr_wr(10 downto 0),
      D(0) => rams_sp_rf_rst_inst3_n_0,
      Q(31 downto 0) => DI0(31 downto 0),
      WE_r => WE_r,
      addr(10) => \adr_wr_reg[10]_rep_n_0\,
      addr(9) => \adr_wr_reg[9]_rep_n_0\,
      addr(8) => \adr_wr_reg[8]_rep_n_0\,
      addr(7) => \adr_wr_reg[7]_rep_n_0\,
      addr(6) => \adr_wr_reg[6]_rep_n_0\,
      addr(5) => \adr_wr_reg[5]_rep_n_0\,
      addr(4) => \adr_wr_reg[4]_rep_n_0\,
      addr(3) => \adr_wr_reg[3]_rep_n_0\,
      addr(2) => \adr_wr_reg[2]_rep_n_0\,
      addr(1) => \adr_wr_reg[1]_rep_n_0\,
      addr(0) => \adr_wr_reg[0]_rep_n_0\,
      dout0_out(31 downto 0) => DOUT2_w(31 downto 0),
      en => EN_r(3),
      m00_axis_aclk => m00_axis_aclk,
      \m00_axis_tdata_r_reg[10]\ => rams_sp_rf_rst_inst7_n_24,
      \m00_axis_tdata_r_reg[12]\ => rams_sp_rf_rst_inst7_n_23,
      \m00_axis_tdata_r_reg[13]\ => rams_sp_rf_rst_inst7_n_22,
      \m00_axis_tdata_r_reg[14]\ => rams_sp_rf_rst_inst7_n_21,
      \m00_axis_tdata_r_reg[17]\ => rams_sp_rf_rst_inst7_n_20,
      \m00_axis_tdata_r_reg[18]\ => rams_sp_rf_rst_inst7_n_19,
      \m00_axis_tdata_r_reg[19]\ => rams_sp_rf_rst_inst7_n_18,
      \m00_axis_tdata_r_reg[1]\ => rams_sp_rf_rst_inst7_n_31,
      \m00_axis_tdata_r_reg[21]\ => rams_sp_rf_rst_inst7_n_17,
      \m00_axis_tdata_r_reg[22]\ => rams_sp_rf_rst_inst7_n_16,
      \m00_axis_tdata_r_reg[23]\ => rams_sp_rf_rst_inst7_n_15,
      \m00_axis_tdata_r_reg[24]\ => rams_sp_rf_rst_inst7_n_14,
      \m00_axis_tdata_r_reg[26]\ => rams_sp_rf_rst_inst7_n_13,
      \m00_axis_tdata_r_reg[28]\ => rams_sp_rf_rst_inst7_n_12,
      \m00_axis_tdata_r_reg[29]\ => rams_sp_rf_rst_inst7_n_11,
      \m00_axis_tdata_r_reg[2]\ => rams_sp_rf_rst_inst7_n_30,
      \m00_axis_tdata_r_reg[30]\ => rams_sp_rf_rst_inst7_n_10,
      \m00_axis_tdata_r_reg[31]\ => rams_sp_rf_rst_inst7_n_9,
      \m00_axis_tdata_r_reg[31]_0\ => rams_sp_rf_rst_instf_n_22,
      \m00_axis_tdata_r_reg[31]_1\ => rams_sp_rf_rst_instb_n_0,
      \m00_axis_tdata_r_reg[31]_2\(31 downto 0) => DOUT1_w(31 downto 0),
      \m00_axis_tdata_r_reg[31]_3\(31 downto 0) => DOUT0(31 downto 0),
      \m00_axis_tdata_r_reg[3]\ => rams_sp_rf_rst_inst7_n_29,
      \m00_axis_tdata_r_reg[5]\ => rams_sp_rf_rst_inst7_n_28,
      \m00_axis_tdata_r_reg[6]\ => rams_sp_rf_rst_inst7_n_27,
      \m00_axis_tdata_r_reg[7]\ => rams_sp_rf_rst_inst7_n_26,
      \m00_axis_tdata_r_reg[8]\ => rams_sp_rf_rst_inst7_n_25,
      ram_reg_0_0 => rams_sp_rf_rst_inst3_n_15,
      ram_reg_0_1 => rams_sp_rf_rst_inst3_n_16,
      ram_reg_0_2 => rams_sp_rf_rst_inst3_n_20,
      ram_reg_0_3 => rams_sp_rf_rst_inst3_n_22,
      ram_reg_0_4 => rams_sp_rf_rst_inst3_n_27,
      ram_reg_0_5 => rams_sp_rf_rst_inst3_n_31,
      ram_reg_1_0 => rams_sp_rf_rst_inst3_n_4,
      ram_reg_1_1 => rams_sp_rf_rst_inst3_n_6,
      ram_reg_1_2 => rams_sp_rf_rst_inst3_n_11,
      s00_axis_tuser(3 downto 0) => s00_axis_tuser(14 downto 11),
      \s00_axis_tuser[18]\ => rams_sp_rf_rst_inst3_n_1,
      \s00_axis_tuser[18]_0\ => rams_sp_rf_rst_inst3_n_2,
      \s00_axis_tuser[18]_1\ => rams_sp_rf_rst_inst3_n_3,
      \s00_axis_tuser[18]_10\ => rams_sp_rf_rst_inst3_n_17,
      \s00_axis_tuser[18]_11\ => rams_sp_rf_rst_inst3_n_18,
      \s00_axis_tuser[18]_12\ => rams_sp_rf_rst_inst3_n_19,
      \s00_axis_tuser[18]_13\ => rams_sp_rf_rst_inst3_n_21,
      \s00_axis_tuser[18]_14\ => rams_sp_rf_rst_inst3_n_23,
      \s00_axis_tuser[18]_15\ => rams_sp_rf_rst_inst3_n_24,
      \s00_axis_tuser[18]_16\ => rams_sp_rf_rst_inst3_n_25,
      \s00_axis_tuser[18]_17\ => rams_sp_rf_rst_inst3_n_26,
      \s00_axis_tuser[18]_18\ => rams_sp_rf_rst_inst3_n_28,
      \s00_axis_tuser[18]_19\ => rams_sp_rf_rst_inst3_n_29,
      \s00_axis_tuser[18]_2\ => rams_sp_rf_rst_inst3_n_5,
      \s00_axis_tuser[18]_20\ => rams_sp_rf_rst_inst3_n_30,
      \s00_axis_tuser[18]_3\ => rams_sp_rf_rst_inst3_n_7,
      \s00_axis_tuser[18]_4\ => rams_sp_rf_rst_inst3_n_8,
      \s00_axis_tuser[18]_5\ => rams_sp_rf_rst_inst3_n_9,
      \s00_axis_tuser[18]_6\ => rams_sp_rf_rst_inst3_n_10,
      \s00_axis_tuser[18]_7\ => rams_sp_rf_rst_inst3_n_12,
      \s00_axis_tuser[18]_8\ => rams_sp_rf_rst_inst3_n_13,
      \s00_axis_tuser[18]_9\ => rams_sp_rf_rst_inst3_n_14,
      square_r => square_r
    );
rams_sp_rf_rst_inst4: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_3
     port map (
      ADDRARDADDR(10 downto 0) => adr_wr(10 downto 0),
      Q(31 downto 0) => DI0(31 downto 0),
      WE_r => WE_r,
      addr(10) => \adr_wr_reg[10]_rep_n_0\,
      addr(9) => \adr_wr_reg[9]_rep_n_0\,
      addr(8) => \adr_wr_reg[8]_rep_n_0\,
      addr(7) => \adr_wr_reg[7]_rep_n_0\,
      addr(6) => \adr_wr_reg[6]_rep_n_0\,
      addr(5) => \adr_wr_reg[5]_rep_n_0\,
      addr(4) => \adr_wr_reg[4]_rep_n_0\,
      addr(3) => \adr_wr_reg[3]_rep_n_0\,
      addr(2) => \adr_wr_reg[2]_rep_n_0\,
      addr(1) => \adr_wr_reg[1]_rep_n_0\,
      addr(0) => \adr_wr_reg[0]_rep_n_0\,
      dout0_out(31 downto 0) => DOUT4_w(31 downto 0),
      en => EN_r(4),
      m00_axis_aclk => m00_axis_aclk,
      square_r => square_r
    );
rams_sp_rf_rst_inst5: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_4
     port map (
      ADDRARDADDR(10 downto 0) => adr_wr(10 downto 0),
      Q(31 downto 0) => DI0(31 downto 0),
      WE_r => WE_r,
      addr(10) => \adr_wr_reg[10]_rep_n_0\,
      addr(9) => \adr_wr_reg[9]_rep_n_0\,
      addr(8) => \adr_wr_reg[8]_rep_n_0\,
      addr(7) => \adr_wr_reg[7]_rep_n_0\,
      addr(6) => \adr_wr_reg[6]_rep_n_0\,
      addr(5) => \adr_wr_reg[5]_rep_n_0\,
      addr(4) => \adr_wr_reg[4]_rep_n_0\,
      addr(3) => \adr_wr_reg[3]_rep_n_0\,
      addr(2) => \adr_wr_reg[2]_rep_n_0\,
      addr(1) => \adr_wr_reg[1]_rep_n_0\,
      addr(0) => \adr_wr_reg[0]_rep_n_0\,
      dout0_out(31 downto 0) => DOUT5_w(31 downto 0),
      en => EN_r(5),
      m00_axis_aclk => m00_axis_aclk,
      square_r => square_r
    );
rams_sp_rf_rst_inst6: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_5
     port map (
      ADDRARDADDR(10 downto 0) => adr_wr(10 downto 0),
      Q(31 downto 0) => DI0(31 downto 0),
      WE_r => WE_r,
      addr(10) => \adr_wr_reg[10]_rep_n_0\,
      addr(9) => \adr_wr_reg[9]_rep_n_0\,
      addr(8) => \adr_wr_reg[8]_rep_n_0\,
      addr(7) => \adr_wr_reg[7]_rep_n_0\,
      addr(6) => \adr_wr_reg[6]_rep_n_0\,
      addr(5) => \adr_wr_reg[5]_rep_n_0\,
      addr(4) => \adr_wr_reg[4]_rep_n_0\,
      addr(3) => \adr_wr_reg[3]_rep_n_0\,
      addr(2) => \adr_wr_reg[2]_rep_n_0\,
      addr(1) => \adr_wr_reg[1]_rep_n_0\,
      addr(0) => \adr_wr_reg[0]_rep_n_0\,
      dout0_out(31 downto 0) => DOUT6_w(31 downto 0),
      en => EN_r(6),
      m00_axis_aclk => m00_axis_aclk,
      square_r => square_r
    );
rams_sp_rf_rst_inst7: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_6
     port map (
      ADDRARDADDR(10 downto 0) => adr_wr(10 downto 0),
      D(8) => rams_sp_rf_rst_inst7_n_0,
      D(7) => rams_sp_rf_rst_inst7_n_1,
      D(6) => rams_sp_rf_rst_inst7_n_2,
      D(5) => rams_sp_rf_rst_inst7_n_3,
      D(4) => rams_sp_rf_rst_inst7_n_4,
      D(3) => rams_sp_rf_rst_inst7_n_5,
      D(2) => rams_sp_rf_rst_inst7_n_6,
      D(1) => rams_sp_rf_rst_inst7_n_7,
      D(0) => rams_sp_rf_rst_inst7_n_8,
      Q(31 downto 0) => DI0(31 downto 0),
      WE_r => WE_r,
      addr(10) => \adr_wr_reg[10]_rep_n_0\,
      addr(9) => \adr_wr_reg[9]_rep_n_0\,
      addr(8) => \adr_wr_reg[8]_rep_n_0\,
      addr(7) => \adr_wr_reg[7]_rep_n_0\,
      addr(6) => \adr_wr_reg[6]_rep_n_0\,
      addr(5) => \adr_wr_reg[5]_rep_n_0\,
      addr(4) => \adr_wr_reg[4]_rep_n_0\,
      addr(3) => \adr_wr_reg[3]_rep_n_0\,
      addr(2) => \adr_wr_reg[2]_rep_n_0\,
      addr(1) => \adr_wr_reg[1]_rep_n_0\,
      addr(0) => \adr_wr_reg[0]_rep_n_0\,
      dout0_out(31 downto 0) => DOUT6_w(31 downto 0),
      en => EN_r(7),
      m00_axis_aclk => m00_axis_aclk,
      \m00_axis_tdata_r_reg[0]\ => rams_sp_rf_rst_inst3_n_31,
      \m00_axis_tdata_r_reg[0]_0\ => rams_sp_rf_rst_instb_n_31,
      \m00_axis_tdata_r_reg[11]\ => rams_sp_rf_rst_inst3_n_20,
      \m00_axis_tdata_r_reg[11]_0\ => rams_sp_rf_rst_instb_n_20,
      \m00_axis_tdata_r_reg[15]\ => rams_sp_rf_rst_inst3_n_16,
      \m00_axis_tdata_r_reg[15]_0\ => rams_sp_rf_rst_instb_n_16,
      \m00_axis_tdata_r_reg[16]\ => rams_sp_rf_rst_inst3_n_15,
      \m00_axis_tdata_r_reg[16]_0\ => rams_sp_rf_rst_instb_n_15,
      \m00_axis_tdata_r_reg[20]\ => rams_sp_rf_rst_inst3_n_11,
      \m00_axis_tdata_r_reg[20]_0\ => rams_sp_rf_rst_instb_n_11,
      \m00_axis_tdata_r_reg[25]\ => rams_sp_rf_rst_inst3_n_6,
      \m00_axis_tdata_r_reg[25]_0\ => rams_sp_rf_rst_instb_n_6,
      \m00_axis_tdata_r_reg[27]\ => rams_sp_rf_rst_inst3_n_4,
      \m00_axis_tdata_r_reg[27]_0\ => rams_sp_rf_rst_instb_n_4,
      \m00_axis_tdata_r_reg[31]\(31 downto 0) => DOUT5_w(31 downto 0),
      \m00_axis_tdata_r_reg[31]_0\(31 downto 0) => DOUT4_w(31 downto 0),
      \m00_axis_tdata_r_reg[4]\ => rams_sp_rf_rst_inst3_n_27,
      \m00_axis_tdata_r_reg[4]_0\ => rams_sp_rf_rst_instb_n_27,
      \m00_axis_tdata_r_reg[9]\ => rams_sp_rf_rst_inst3_n_22,
      \m00_axis_tdata_r_reg[9]_0\ => rams_sp_rf_rst_instb_n_22,
      ram_reg_0_0 => rams_sp_rf_rst_inst7_n_20,
      ram_reg_0_1 => rams_sp_rf_rst_inst7_n_21,
      ram_reg_0_10 => rams_sp_rf_rst_inst7_n_30,
      ram_reg_0_11 => rams_sp_rf_rst_inst7_n_31,
      ram_reg_0_2 => rams_sp_rf_rst_inst7_n_22,
      ram_reg_0_3 => rams_sp_rf_rst_inst7_n_23,
      ram_reg_0_4 => rams_sp_rf_rst_inst7_n_24,
      ram_reg_0_5 => rams_sp_rf_rst_inst7_n_25,
      ram_reg_0_6 => rams_sp_rf_rst_inst7_n_26,
      ram_reg_0_7 => rams_sp_rf_rst_inst7_n_27,
      ram_reg_0_8 => rams_sp_rf_rst_inst7_n_28,
      ram_reg_0_9 => rams_sp_rf_rst_inst7_n_29,
      ram_reg_1_0 => rams_sp_rf_rst_inst7_n_9,
      ram_reg_1_1 => rams_sp_rf_rst_inst7_n_10,
      ram_reg_1_10 => rams_sp_rf_rst_inst7_n_19,
      ram_reg_1_2 => rams_sp_rf_rst_inst7_n_11,
      ram_reg_1_3 => rams_sp_rf_rst_inst7_n_12,
      ram_reg_1_4 => rams_sp_rf_rst_inst7_n_13,
      ram_reg_1_5 => rams_sp_rf_rst_inst7_n_14,
      ram_reg_1_6 => rams_sp_rf_rst_inst7_n_15,
      ram_reg_1_7 => rams_sp_rf_rst_inst7_n_16,
      ram_reg_1_8 => rams_sp_rf_rst_inst7_n_17,
      ram_reg_1_9 => rams_sp_rf_rst_inst7_n_18,
      s00_axis_tuser(3 downto 0) => s00_axis_tuser(14 downto 11),
      square_r => square_r
    );
rams_sp_rf_rst_inst8: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_7
     port map (
      ADDRARDADDR(10 downto 0) => adr_wr(10 downto 0),
      Q(0) => EN_r(8),
      WE_r => WE_r,
      addr(10) => \adr_wr_reg[10]_rep_n_0\,
      addr(9) => \adr_wr_reg[9]_rep_n_0\,
      addr(8) => \adr_wr_reg[8]_rep_n_0\,
      addr(7) => \adr_wr_reg[7]_rep_n_0\,
      addr(6) => \adr_wr_reg[6]_rep_n_0\,
      addr(5) => \adr_wr_reg[5]_rep_n_0\,
      addr(4) => \adr_wr_reg[4]_rep_n_0\,
      addr(3) => \adr_wr_reg[3]_rep_n_0\,
      addr(2) => \adr_wr_reg[2]_rep_n_0\,
      addr(1) => \adr_wr_reg[1]_rep_n_0\,
      addr(0) => \adr_wr_reg[0]_rep_n_0\,
      dout0_out(31 downto 0) => DOUT8_w(31 downto 0),
      m00_axis_aclk => m00_axis_aclk,
      ram_reg_1_0(31 downto 0) => DI0(31 downto 0),
      square_r => square_r
    );
rams_sp_rf_rst_inst9: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_8
     port map (
      ADDRARDADDR(10 downto 0) => adr_wr(10 downto 0),
      Q(0) => EN_r(9),
      WE_r => WE_r,
      addr(10) => \adr_wr_reg[10]_rep_n_0\,
      addr(9) => \adr_wr_reg[9]_rep_n_0\,
      addr(8) => \adr_wr_reg[8]_rep_n_0\,
      addr(7) => \adr_wr_reg[7]_rep_n_0\,
      addr(6) => \adr_wr_reg[6]_rep_n_0\,
      addr(5) => \adr_wr_reg[5]_rep_n_0\,
      addr(4) => \adr_wr_reg[4]_rep_n_0\,
      addr(3) => \adr_wr_reg[3]_rep_n_0\,
      addr(2) => \adr_wr_reg[2]_rep_n_0\,
      addr(1) => \adr_wr_reg[1]_rep_n_0\,
      addr(0) => \adr_wr_reg[0]_rep_n_0\,
      dout0_out(31 downto 0) => DOUT9_w(31 downto 0),
      m00_axis_aclk => m00_axis_aclk,
      ram_reg_1_0(31 downto 0) => DI0(31 downto 0),
      square_r => square_r
    );
rams_sp_rf_rst_insta: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_9
     port map (
      ADDRARDADDR(10 downto 0) => adr_wr(10 downto 0),
      Q(31 downto 0) => DI0(31 downto 0),
      WE_r => WE_r,
      addr(10) => \adr_wr_reg[10]_rep_n_0\,
      addr(9) => \adr_wr_reg[9]_rep_n_0\,
      addr(8) => \adr_wr_reg[8]_rep_n_0\,
      addr(7) => \adr_wr_reg[7]_rep_n_0\,
      addr(6) => \adr_wr_reg[6]_rep_n_0\,
      addr(5) => \adr_wr_reg[5]_rep_n_0\,
      addr(4) => \adr_wr_reg[4]_rep_n_0\,
      addr(3) => \adr_wr_reg[3]_rep_n_0\,
      addr(2) => \adr_wr_reg[2]_rep_n_0\,
      addr(1) => \adr_wr_reg[1]_rep_n_0\,
      addr(0) => \adr_wr_reg[0]_rep_n_0\,
      dout0_out(31 downto 0) => DOUTA_w(31 downto 0),
      en => EN_r(10),
      m00_axis_aclk => m00_axis_aclk,
      square_r => square_r
    );
rams_sp_rf_rst_instb: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_10
     port map (
      ADDRARDADDR(10 downto 0) => adr_wr(10 downto 0),
      Q(31 downto 0) => DI0(31 downto 0),
      WE_r => WE_r,
      addr(10) => \adr_wr_reg[10]_rep_n_0\,
      addr(9) => \adr_wr_reg[9]_rep_n_0\,
      addr(8) => \adr_wr_reg[8]_rep_n_0\,
      addr(7) => \adr_wr_reg[7]_rep_n_0\,
      addr(6) => \adr_wr_reg[6]_rep_n_0\,
      addr(5) => \adr_wr_reg[5]_rep_n_0\,
      addr(4) => \adr_wr_reg[4]_rep_n_0\,
      addr(3) => \adr_wr_reg[3]_rep_n_0\,
      addr(2) => \adr_wr_reg[2]_rep_n_0\,
      addr(1) => \adr_wr_reg[1]_rep_n_0\,
      addr(0) => \adr_wr_reg[0]_rep_n_0\,
      dout0_out(31 downto 0) => DOUTA_w(31 downto 0),
      en => EN_r(11),
      m00_axis_aclk => m00_axis_aclk,
      \m00_axis_tdata_r_reg[0]\ => rams_sp_rf_rst_instf_n_31,
      \m00_axis_tdata_r_reg[11]\ => rams_sp_rf_rst_instf_n_28,
      \m00_axis_tdata_r_reg[15]\ => rams_sp_rf_rst_instf_n_27,
      \m00_axis_tdata_r_reg[16]\ => rams_sp_rf_rst_instf_n_26,
      \m00_axis_tdata_r_reg[20]\ => rams_sp_rf_rst_instf_n_25,
      \m00_axis_tdata_r_reg[25]\ => rams_sp_rf_rst_instf_n_24,
      \m00_axis_tdata_r_reg[27]\ => rams_sp_rf_rst_instf_n_23,
      \m00_axis_tdata_r_reg[31]\(31 downto 0) => DOUT9_w(31 downto 0),
      \m00_axis_tdata_r_reg[31]_0\(31 downto 0) => DOUT8_w(31 downto 0),
      \m00_axis_tdata_r_reg[4]\ => rams_sp_rf_rst_instf_n_30,
      \m00_axis_tdata_r_reg[9]\ => rams_sp_rf_rst_instf_n_29,
      ram_reg_0_0 => rams_sp_rf_rst_instb_n_14,
      ram_reg_0_1 => rams_sp_rf_rst_instb_n_17,
      ram_reg_0_10 => rams_sp_rf_rst_instb_n_29,
      ram_reg_0_11 => rams_sp_rf_rst_instb_n_30,
      ram_reg_0_2 => rams_sp_rf_rst_instb_n_18,
      ram_reg_0_3 => rams_sp_rf_rst_instb_n_19,
      ram_reg_0_4 => rams_sp_rf_rst_instb_n_21,
      ram_reg_0_5 => rams_sp_rf_rst_instb_n_23,
      ram_reg_0_6 => rams_sp_rf_rst_instb_n_24,
      ram_reg_0_7 => rams_sp_rf_rst_instb_n_25,
      ram_reg_0_8 => rams_sp_rf_rst_instb_n_26,
      ram_reg_0_9 => rams_sp_rf_rst_instb_n_28,
      ram_reg_1_0 => rams_sp_rf_rst_instb_n_0,
      ram_reg_1_1 => rams_sp_rf_rst_instb_n_1,
      ram_reg_1_10 => rams_sp_rf_rst_instb_n_13,
      ram_reg_1_2 => rams_sp_rf_rst_instb_n_2,
      ram_reg_1_3 => rams_sp_rf_rst_instb_n_3,
      ram_reg_1_4 => rams_sp_rf_rst_instb_n_5,
      ram_reg_1_5 => rams_sp_rf_rst_instb_n_7,
      ram_reg_1_6 => rams_sp_rf_rst_instb_n_8,
      ram_reg_1_7 => rams_sp_rf_rst_instb_n_9,
      ram_reg_1_8 => rams_sp_rf_rst_instb_n_10,
      ram_reg_1_9 => rams_sp_rf_rst_instb_n_12,
      s00_axis_tuser(2 downto 0) => s00_axis_tuser(13 downto 11),
      \s00_axis_tuser[18]\ => rams_sp_rf_rst_instb_n_4,
      \s00_axis_tuser[18]_0\ => rams_sp_rf_rst_instb_n_6,
      \s00_axis_tuser[18]_1\ => rams_sp_rf_rst_instb_n_11,
      \s00_axis_tuser[18]_2\ => rams_sp_rf_rst_instb_n_15,
      \s00_axis_tuser[18]_3\ => rams_sp_rf_rst_instb_n_16,
      \s00_axis_tuser[18]_4\ => rams_sp_rf_rst_instb_n_20,
      \s00_axis_tuser[18]_5\ => rams_sp_rf_rst_instb_n_22,
      \s00_axis_tuser[18]_6\ => rams_sp_rf_rst_instb_n_27,
      \s00_axis_tuser[18]_7\ => rams_sp_rf_rst_instb_n_31,
      square_r => square_r
    );
rams_sp_rf_rst_instc: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_11
     port map (
      ADDRARDADDR(10 downto 0) => adr_wr(10 downto 0),
      Q(31 downto 0) => DI0(31 downto 0),
      WE_r => WE_r,
      addr(10) => \adr_wr_reg[10]_rep_n_0\,
      addr(9) => \adr_wr_reg[9]_rep_n_0\,
      addr(8) => \adr_wr_reg[8]_rep_n_0\,
      addr(7) => \adr_wr_reg[7]_rep_n_0\,
      addr(6) => \adr_wr_reg[6]_rep_n_0\,
      addr(5) => \adr_wr_reg[5]_rep_n_0\,
      addr(4) => \adr_wr_reg[4]_rep_n_0\,
      addr(3) => \adr_wr_reg[3]_rep_n_0\,
      addr(2) => \adr_wr_reg[2]_rep_n_0\,
      addr(1) => \adr_wr_reg[1]_rep_n_0\,
      addr(0) => \adr_wr_reg[0]_rep_n_0\,
      dout0_out(31 downto 0) => DOUTC_w(31 downto 0),
      en => EN_r(12),
      m00_axis_aclk => m00_axis_aclk,
      square_r => square_r
    );
rams_sp_rf_rst_instd: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_12
     port map (
      ADDRARDADDR(10 downto 0) => adr_wr(10 downto 0),
      Q(31 downto 0) => DI0(31 downto 0),
      WE_r => WE_r,
      addr(10) => \adr_wr_reg[10]_rep_n_0\,
      addr(9) => \adr_wr_reg[9]_rep_n_0\,
      addr(8) => \adr_wr_reg[8]_rep_n_0\,
      addr(7) => \adr_wr_reg[7]_rep_n_0\,
      addr(6) => \adr_wr_reg[6]_rep_n_0\,
      addr(5) => \adr_wr_reg[5]_rep_n_0\,
      addr(4) => \adr_wr_reg[4]_rep_n_0\,
      addr(3) => \adr_wr_reg[3]_rep_n_0\,
      addr(2) => \adr_wr_reg[2]_rep_n_0\,
      addr(1) => \adr_wr_reg[1]_rep_n_0\,
      addr(0) => \adr_wr_reg[0]_rep_n_0\,
      dout0_out(31 downto 0) => DOUTD_w(31 downto 0),
      en => EN_r(13),
      m00_axis_aclk => m00_axis_aclk,
      square_r => square_r
    );
rams_sp_rf_rst_inste: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_13
     port map (
      ADDRARDADDR(10 downto 0) => adr_wr(10 downto 0),
      Q(31 downto 0) => DI0(31 downto 0),
      WE_r => WE_r,
      addr(10) => \adr_wr_reg[10]_rep_n_0\,
      addr(9) => \adr_wr_reg[9]_rep_n_0\,
      addr(8) => \adr_wr_reg[8]_rep_n_0\,
      addr(7) => \adr_wr_reg[7]_rep_n_0\,
      addr(6) => \adr_wr_reg[6]_rep_n_0\,
      addr(5) => \adr_wr_reg[5]_rep_n_0\,
      addr(4) => \adr_wr_reg[4]_rep_n_0\,
      addr(3) => \adr_wr_reg[3]_rep_n_0\,
      addr(2) => \adr_wr_reg[2]_rep_n_0\,
      addr(1) => \adr_wr_reg[1]_rep_n_0\,
      addr(0) => \adr_wr_reg[0]_rep_n_0\,
      dout0_out(31 downto 0) => DOUTE_w(31 downto 0),
      en => EN_r(14),
      m00_axis_aclk => m00_axis_aclk,
      square_r => square_r
    );
rams_sp_rf_rst_instf: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst_14
     port map (
      ADDRARDADDR(10 downto 0) => adr_wr(10 downto 0),
      D(21) => rams_sp_rf_rst_instf_n_0,
      D(20) => rams_sp_rf_rst_instf_n_1,
      D(19) => rams_sp_rf_rst_instf_n_2,
      D(18) => rams_sp_rf_rst_instf_n_3,
      D(17) => rams_sp_rf_rst_instf_n_4,
      D(16) => rams_sp_rf_rst_instf_n_5,
      D(15) => rams_sp_rf_rst_instf_n_6,
      D(14) => rams_sp_rf_rst_instf_n_7,
      D(13) => rams_sp_rf_rst_instf_n_8,
      D(12) => rams_sp_rf_rst_instf_n_9,
      D(11) => rams_sp_rf_rst_instf_n_10,
      D(10) => rams_sp_rf_rst_instf_n_11,
      D(9) => rams_sp_rf_rst_instf_n_12,
      D(8) => rams_sp_rf_rst_instf_n_13,
      D(7) => rams_sp_rf_rst_instf_n_14,
      D(6) => rams_sp_rf_rst_instf_n_15,
      D(5) => rams_sp_rf_rst_instf_n_16,
      D(4) => rams_sp_rf_rst_instf_n_17,
      D(3) => rams_sp_rf_rst_instf_n_18,
      D(2) => rams_sp_rf_rst_instf_n_19,
      D(1) => rams_sp_rf_rst_instf_n_20,
      D(0) => rams_sp_rf_rst_instf_n_21,
      Q(31 downto 0) => DI0(31 downto 0),
      WE_r => WE_r,
      addr(10) => \adr_wr_reg[10]_rep_n_0\,
      addr(9) => \adr_wr_reg[9]_rep_n_0\,
      addr(8) => \adr_wr_reg[8]_rep_n_0\,
      addr(7) => \adr_wr_reg[7]_rep_n_0\,
      addr(6) => \adr_wr_reg[6]_rep_n_0\,
      addr(5) => \adr_wr_reg[5]_rep_n_0\,
      addr(4) => \adr_wr_reg[4]_rep_n_0\,
      addr(3) => \adr_wr_reg[3]_rep_n_0\,
      addr(2) => \adr_wr_reg[2]_rep_n_0\,
      addr(1) => \adr_wr_reg[1]_rep_n_0\,
      addr(0) => \adr_wr_reg[0]_rep_n_0\,
      dout0_out(31 downto 0) => DOUTE_w(31 downto 0),
      en => EN_r(15),
      m00_axis_aclk => m00_axis_aclk,
      \m00_axis_tdata_r_reg[10]\ => rams_sp_rf_rst_instb_n_21,
      \m00_axis_tdata_r_reg[10]_0\ => rams_sp_rf_rst_inst3_n_21,
      \m00_axis_tdata_r_reg[12]\ => rams_sp_rf_rst_instb_n_19,
      \m00_axis_tdata_r_reg[12]_0\ => rams_sp_rf_rst_inst3_n_19,
      \m00_axis_tdata_r_reg[13]\ => rams_sp_rf_rst_instb_n_18,
      \m00_axis_tdata_r_reg[13]_0\ => rams_sp_rf_rst_inst3_n_18,
      \m00_axis_tdata_r_reg[14]\ => rams_sp_rf_rst_instb_n_17,
      \m00_axis_tdata_r_reg[14]_0\ => rams_sp_rf_rst_inst3_n_17,
      \m00_axis_tdata_r_reg[17]\ => rams_sp_rf_rst_instb_n_14,
      \m00_axis_tdata_r_reg[17]_0\ => rams_sp_rf_rst_inst3_n_14,
      \m00_axis_tdata_r_reg[18]\ => rams_sp_rf_rst_instb_n_13,
      \m00_axis_tdata_r_reg[18]_0\ => rams_sp_rf_rst_inst3_n_13,
      \m00_axis_tdata_r_reg[19]\ => rams_sp_rf_rst_instb_n_12,
      \m00_axis_tdata_r_reg[19]_0\ => rams_sp_rf_rst_inst3_n_12,
      \m00_axis_tdata_r_reg[1]\ => rams_sp_rf_rst_instb_n_30,
      \m00_axis_tdata_r_reg[1]_0\ => rams_sp_rf_rst_inst3_n_30,
      \m00_axis_tdata_r_reg[21]\ => rams_sp_rf_rst_instb_n_10,
      \m00_axis_tdata_r_reg[21]_0\ => rams_sp_rf_rst_inst3_n_10,
      \m00_axis_tdata_r_reg[22]\ => rams_sp_rf_rst_instb_n_9,
      \m00_axis_tdata_r_reg[22]_0\ => rams_sp_rf_rst_inst3_n_9,
      \m00_axis_tdata_r_reg[23]\ => rams_sp_rf_rst_instb_n_8,
      \m00_axis_tdata_r_reg[23]_0\ => rams_sp_rf_rst_inst3_n_8,
      \m00_axis_tdata_r_reg[24]\ => rams_sp_rf_rst_instb_n_7,
      \m00_axis_tdata_r_reg[24]_0\ => rams_sp_rf_rst_inst3_n_7,
      \m00_axis_tdata_r_reg[26]\ => rams_sp_rf_rst_instb_n_5,
      \m00_axis_tdata_r_reg[26]_0\ => rams_sp_rf_rst_inst3_n_5,
      \m00_axis_tdata_r_reg[28]\ => rams_sp_rf_rst_instb_n_3,
      \m00_axis_tdata_r_reg[28]_0\ => rams_sp_rf_rst_inst3_n_3,
      \m00_axis_tdata_r_reg[29]\ => rams_sp_rf_rst_instb_n_2,
      \m00_axis_tdata_r_reg[29]_0\ => rams_sp_rf_rst_inst3_n_2,
      \m00_axis_tdata_r_reg[2]\ => rams_sp_rf_rst_instb_n_29,
      \m00_axis_tdata_r_reg[2]_0\ => rams_sp_rf_rst_inst3_n_29,
      \m00_axis_tdata_r_reg[30]\ => rams_sp_rf_rst_instb_n_1,
      \m00_axis_tdata_r_reg[30]_0\ => rams_sp_rf_rst_inst3_n_1,
      \m00_axis_tdata_r_reg[31]\(31 downto 0) => DOUTD_w(31 downto 0),
      \m00_axis_tdata_r_reg[31]_0\(31 downto 0) => DOUTC_w(31 downto 0),
      \m00_axis_tdata_r_reg[3]\ => rams_sp_rf_rst_instb_n_28,
      \m00_axis_tdata_r_reg[3]_0\ => rams_sp_rf_rst_inst3_n_28,
      \m00_axis_tdata_r_reg[5]\ => rams_sp_rf_rst_instb_n_26,
      \m00_axis_tdata_r_reg[5]_0\ => rams_sp_rf_rst_inst3_n_26,
      \m00_axis_tdata_r_reg[6]\ => rams_sp_rf_rst_instb_n_25,
      \m00_axis_tdata_r_reg[6]_0\ => rams_sp_rf_rst_inst3_n_25,
      \m00_axis_tdata_r_reg[7]\ => rams_sp_rf_rst_instb_n_24,
      \m00_axis_tdata_r_reg[7]_0\ => rams_sp_rf_rst_inst3_n_24,
      \m00_axis_tdata_r_reg[8]\ => rams_sp_rf_rst_instb_n_23,
      \m00_axis_tdata_r_reg[8]_0\ => rams_sp_rf_rst_inst3_n_23,
      ram_reg_0_0 => rams_sp_rf_rst_instf_n_26,
      ram_reg_0_1 => rams_sp_rf_rst_instf_n_27,
      ram_reg_0_2 => rams_sp_rf_rst_instf_n_28,
      ram_reg_0_3 => rams_sp_rf_rst_instf_n_29,
      ram_reg_0_4 => rams_sp_rf_rst_instf_n_30,
      ram_reg_0_5 => rams_sp_rf_rst_instf_n_31,
      ram_reg_1_0 => rams_sp_rf_rst_instf_n_22,
      ram_reg_1_1 => rams_sp_rf_rst_instf_n_23,
      ram_reg_1_2 => rams_sp_rf_rst_instf_n_24,
      ram_reg_1_3 => rams_sp_rf_rst_instf_n_25,
      s00_axis_tuser(3 downto 0) => s00_axis_tuser(14 downto 11),
      square_r => square_r
    );
square_r_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"3A"
    )
        port map (
      I0 => square_r,
      I1 => s00_axis_tuser(15),
      I2 => s00_axis_tvalid,
      O => square_r_i_1_n_0
    );
square_r_reg: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => square_r_i_1_n_0,
      Q => square_r,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  port (
    m00_axis_aclk : in STD_LOGIC;
    m00_axis_aresetn : in STD_LOGIC;
    m00_axis_tvalid : out STD_LOGIC;
    m00_axis_tdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    m00_axis_tstrb : out STD_LOGIC_VECTOR ( 3 downto 0 );
    m00_axis_tlast : out STD_LOGIC;
    m00_axis_tready : in STD_LOGIC;
    s00_axis_aclk : in STD_LOGIC;
    s00_axis_aresetn : in STD_LOGIC;
    s00_axis_tready : out STD_LOGIC;
    s00_axis_tdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s00_axis_tuser : in STD_LOGIC_VECTOR ( 47 downto 0 );
    s00_axis_tstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s00_axis_tlast : in STD_LOGIC;
    s00_axis_tvalid : in STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "design_2_pixelReOrder_0_0,pixelReOrder_v1_0,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "yes";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "pixelReOrder_v1_0,Vivado 2019.1";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  signal \<const1>\ : STD_LOGIC;
  attribute X_INTERFACE_INFO : string;
  attribute X_INTERFACE_INFO of m00_axis_aclk : signal is "xilinx.com:signal:clock:1.0 M00_AXIS_CLK CLK";
  attribute X_INTERFACE_PARAMETER : string;
  attribute X_INTERFACE_PARAMETER of m00_axis_aclk : signal is "XIL_INTERFACENAME M00_AXIS_CLK, ASSOCIATED_BUSIF M00_AXIS, ASSOCIATED_RESET m00_axis_aresetn, FREQ_HZ 200000000, PHASE 0.000, CLK_DOMAIN design_2_processing_system7_0_0_FCLK_CLK0, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of m00_axis_aresetn : signal is "xilinx.com:signal:reset:1.0 M00_AXIS_RST RST";
  attribute X_INTERFACE_PARAMETER of m00_axis_aresetn : signal is "XIL_INTERFACENAME M00_AXIS_RST, POLARITY ACTIVE_LOW, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of m00_axis_tlast : signal is "xilinx.com:interface:axis:1.0 M00_AXIS TLAST";
  attribute X_INTERFACE_INFO of m00_axis_tready : signal is "xilinx.com:interface:axis:1.0 M00_AXIS TREADY";
  attribute X_INTERFACE_PARAMETER of m00_axis_tready : signal is "XIL_INTERFACENAME M00_AXIS, WIZ_DATA_WIDTH 32, TDATA_NUM_BYTES 4, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 1, HAS_TKEEP 0, HAS_TLAST 1, FREQ_HZ 200000000, PHASE 0.000, CLK_DOMAIN design_2_processing_system7_0_0_FCLK_CLK0, LAYERED_METADATA undef, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of m00_axis_tvalid : signal is "xilinx.com:interface:axis:1.0 M00_AXIS TVALID";
  attribute X_INTERFACE_INFO of s00_axis_aclk : signal is "xilinx.com:signal:clock:1.0 S00_AXIS_CLK CLK";
  attribute X_INTERFACE_PARAMETER of s00_axis_aclk : signal is "XIL_INTERFACENAME S00_AXIS_CLK, ASSOCIATED_BUSIF S00_AXIS, ASSOCIATED_RESET s00_axis_aresetn, FREQ_HZ 200000000, PHASE 0.000, CLK_DOMAIN design_2_processing_system7_0_0_FCLK_CLK0, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of s00_axis_aresetn : signal is "xilinx.com:signal:reset:1.0 S00_AXIS_RST RST";
  attribute X_INTERFACE_PARAMETER of s00_axis_aresetn : signal is "XIL_INTERFACENAME S00_AXIS_RST, POLARITY ACTIVE_LOW, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of s00_axis_tlast : signal is "xilinx.com:interface:axis:1.0 S00_AXIS TLAST";
  attribute X_INTERFACE_INFO of s00_axis_tready : signal is "xilinx.com:interface:axis:1.0 S00_AXIS TREADY";
  attribute X_INTERFACE_INFO of s00_axis_tvalid : signal is "xilinx.com:interface:axis:1.0 S00_AXIS TVALID";
  attribute X_INTERFACE_PARAMETER of s00_axis_tvalid : signal is "XIL_INTERFACENAME S00_AXIS, WIZ_DATA_WIDTH 32, TDATA_NUM_BYTES 4, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 48, HAS_TREADY 1, HAS_TSTRB 1, HAS_TKEEP 0, HAS_TLAST 1, FREQ_HZ 200000000, PHASE 0.000, CLK_DOMAIN design_2_processing_system7_0_0_FCLK_CLK0, LAYERED_METADATA undef, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of m00_axis_tdata : signal is "xilinx.com:interface:axis:1.0 M00_AXIS TDATA";
  attribute X_INTERFACE_INFO of m00_axis_tstrb : signal is "xilinx.com:interface:axis:1.0 M00_AXIS TSTRB";
  attribute X_INTERFACE_INFO of s00_axis_tdata : signal is "xilinx.com:interface:axis:1.0 S00_AXIS TDATA";
  attribute X_INTERFACE_INFO of s00_axis_tstrb : signal is "xilinx.com:interface:axis:1.0 S00_AXIS TSTRB";
  attribute X_INTERFACE_INFO of s00_axis_tuser : signal is "xilinx.com:interface:axis:1.0 S00_AXIS TUSER";
begin
  s00_axis_tready <= \<const1>\;
  m00_axis_tlast <= 'Z';
  m00_axis_tstrb(0) <= 'Z';
  m00_axis_tstrb(1) <= 'Z';
  m00_axis_tstrb(2) <= 'Z';
  m00_axis_tstrb(3) <= 'Z';
VCC: unisim.vcomponents.VCC
     port map (
      P => \<const1>\
    );
inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_pixelReOrder_v1_0
     port map (
      m00_axis_aclk => m00_axis_aclk,
      m00_axis_tdata(31 downto 0) => m00_axis_tdata(31 downto 0),
      m00_axis_tvalid => m00_axis_tvalid,
      s00_axis_tdata(31 downto 0) => s00_axis_tdata(31 downto 0),
      s00_axis_tuser(26 downto 11) => s00_axis_tuser(31 downto 16),
      s00_axis_tuser(10 downto 0) => s00_axis_tuser(10 downto 0),
      s00_axis_tvalid => s00_axis_tvalid
    );
end STRUCTURE;
