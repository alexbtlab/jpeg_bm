// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (lin64) Build 2552052 Fri May 24 14:47:09 MDT 2019
// Date        : Mon Sep 27 22:57:49 2021
// Host        : alex-HP-Compaq-8200-Elite-CMT-PC running 64-bit Ubuntu 20.04.2 LTS
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_2_pixelReOrder_0_0_sim_netlist.v
// Design      : design_2_pixelReOrder_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7z020clg484-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_2_pixelReOrder_0_0,pixelReOrder_v1_0,{}" *) (* DowngradeIPIdentifiedWarnings = "yes" *) (* X_CORE_INFO = "pixelReOrder_v1_0,Vivado 2019.1" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (m00_axis_aclk,
    m00_axis_aresetn,
    m00_axis_tvalid,
    m00_axis_tdata,
    m00_axis_tstrb,
    m00_axis_tlast,
    m00_axis_tready,
    s00_axis_aclk,
    s00_axis_aresetn,
    s00_axis_tready,
    s00_axis_tdata,
    s00_axis_tuser,
    s00_axis_tstrb,
    s00_axis_tlast,
    s00_axis_tvalid);
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 M00_AXIS_CLK CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME M00_AXIS_CLK, ASSOCIATED_BUSIF M00_AXIS, ASSOCIATED_RESET m00_axis_aresetn, FREQ_HZ 200000000, PHASE 0.000, CLK_DOMAIN design_2_processing_system7_0_0_FCLK_CLK0, INSERT_VIP 0" *) input m00_axis_aclk;
  (* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 M00_AXIS_RST RST" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME M00_AXIS_RST, POLARITY ACTIVE_LOW, INSERT_VIP 0" *) input m00_axis_aresetn;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 M00_AXIS TVALID" *) output m00_axis_tvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 M00_AXIS TDATA" *) output [31:0]m00_axis_tdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 M00_AXIS TSTRB" *) output [3:0]m00_axis_tstrb;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 M00_AXIS TLAST" *) output m00_axis_tlast;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 M00_AXIS TREADY" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME M00_AXIS, WIZ_DATA_WIDTH 32, TDATA_NUM_BYTES 4, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 1, HAS_TKEEP 0, HAS_TLAST 1, FREQ_HZ 200000000, PHASE 0.000, CLK_DOMAIN design_2_processing_system7_0_0_FCLK_CLK0, LAYERED_METADATA undef, INSERT_VIP 0" *) input m00_axis_tready;
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 S00_AXIS_CLK CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME S00_AXIS_CLK, ASSOCIATED_BUSIF S00_AXIS, ASSOCIATED_RESET s00_axis_aresetn, FREQ_HZ 200000000, PHASE 0.000, CLK_DOMAIN design_2_processing_system7_0_0_FCLK_CLK0, INSERT_VIP 0" *) input s00_axis_aclk;
  (* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 S00_AXIS_RST RST" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME S00_AXIS_RST, POLARITY ACTIVE_LOW, INSERT_VIP 0" *) input s00_axis_aresetn;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 S00_AXIS TREADY" *) output s00_axis_tready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 S00_AXIS TDATA" *) input [31:0]s00_axis_tdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 S00_AXIS TUSER" *) input [47:0]s00_axis_tuser;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 S00_AXIS TSTRB" *) input [3:0]s00_axis_tstrb;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 S00_AXIS TLAST" *) input s00_axis_tlast;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 S00_AXIS TVALID" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME S00_AXIS, WIZ_DATA_WIDTH 32, TDATA_NUM_BYTES 4, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 48, HAS_TREADY 1, HAS_TSTRB 1, HAS_TKEEP 0, HAS_TLAST 1, FREQ_HZ 200000000, PHASE 0.000, CLK_DOMAIN design_2_processing_system7_0_0_FCLK_CLK0, LAYERED_METADATA undef, INSERT_VIP 0" *) input s00_axis_tvalid;

  wire \<const1> ;
  wire m00_axis_aclk;
  wire [31:0]m00_axis_tdata;
  wire m00_axis_tvalid;
  wire p_0_in;
  wire [31:0]s00_axis_tdata;
  wire [47:0]s00_axis_tuser;
  wire s00_axis_tvalid;

  assign m00_axis_tlast = \<const1> ;
  assign s00_axis_tready = \<const1> ;
  VCC VCC
       (.P(\<const1> ));
  LUT1 #(
    .INIT(2'h1)) 
    i_14
       (.I0(s00_axis_tvalid),
        .O(p_0_in));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_pixelReOrder_v1_0 inst
       (.m00_axis_aclk(m00_axis_aclk),
        .m00_axis_tdata(m00_axis_tdata),
        .m00_axis_tvalid(m00_axis_tvalid),
        .s00_axis_tdata(s00_axis_tdata),
        .s00_axis_tuser({s00_axis_tuser[31:16],s00_axis_tuser[10:0]}),
        .s00_axis_tvalid(s00_axis_tvalid));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_pixelReOrder_v1_0
   (m00_axis_tvalid,
    m00_axis_tdata,
    s00_axis_tvalid,
    m00_axis_aclk,
    s00_axis_tuser,
    s00_axis_tdata);
  output m00_axis_tvalid;
  output [31:0]m00_axis_tdata;
  input s00_axis_tvalid;
  input m00_axis_aclk;
  input [26:0]s00_axis_tuser;
  input [31:0]s00_axis_tdata;

  wire [31:0]DI0;
  wire [0:0]EN_r;
  wire \EN_r[0]_i_1_n_0 ;
  wire WE_r;
  wire [10:0]adr_wr;
  wire [15:0]cnt_inner_valid;
  wire cnt_inner_valid0_carry__0_n_0;
  wire cnt_inner_valid0_carry__0_n_1;
  wire cnt_inner_valid0_carry__0_n_2;
  wire cnt_inner_valid0_carry__0_n_3;
  wire cnt_inner_valid0_carry__1_n_0;
  wire cnt_inner_valid0_carry__1_n_1;
  wire cnt_inner_valid0_carry__1_n_2;
  wire cnt_inner_valid0_carry__1_n_3;
  wire cnt_inner_valid0_carry__2_n_2;
  wire cnt_inner_valid0_carry__2_n_3;
  wire cnt_inner_valid0_carry_n_0;
  wire cnt_inner_valid0_carry_n_1;
  wire cnt_inner_valid0_carry_n_2;
  wire cnt_inner_valid0_carry_n_3;
  wire \cnt_inner_valid[0]_i_2_n_0 ;
  wire \cnt_inner_valid[15]_i_2_n_0 ;
  wire \cnt_inner_valid[15]_i_3_n_0 ;
  wire \cnt_inner_valid[15]_i_4_n_0 ;
  wire \cnt_inner_valid[15]_i_5_n_0 ;
  wire [15:0]cnt_inner_valid_0;
  wire \ctrl_r[3]_i_1_n_0 ;
  wire \ctrl_r[3]_i_2_n_0 ;
  wire \ctrl_r[3]_i_3_n_0 ;
  wire \ctrl_r[3]_i_4_n_0 ;
  wire \ctrl_r_reg_n_0_[0] ;
  wire \ctrl_r_reg_n_0_[1] ;
  wire \ctrl_r_reg_n_0_[2] ;
  wire \ctrl_r_reg_n_0_[3] ;
  wire [15:1]data0;
  wire m00_axis_aclk;
  wire [31:0]m00_axis_tdata;
  wire m00_axis_tvalid;
  wire m00_axis_tvalid_r0;
  wire m00_axis_tvalid_r_i_2_n_0;
  wire m00_axis_tvalid_r_i_3_n_0;
  wire [31:0]s00_axis_tdata;
  wire [26:0]s00_axis_tuser;
  wire s00_axis_tvalid;
  wire square_r;
  wire square_r_i_1_n_0;
  wire [3:2]NLW_cnt_inner_valid0_carry__2_CO_UNCONNECTED;
  wire [3:3]NLW_cnt_inner_valid0_carry__2_O_UNCONNECTED;

  FDRE \DI0_reg[0] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_tvalid),
        .D(s00_axis_tdata[0]),
        .Q(DI0[0]),
        .R(1'b0));
  FDRE \DI0_reg[10] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_tvalid),
        .D(s00_axis_tdata[10]),
        .Q(DI0[10]),
        .R(1'b0));
  FDRE \DI0_reg[11] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_tvalid),
        .D(s00_axis_tdata[11]),
        .Q(DI0[11]),
        .R(1'b0));
  FDRE \DI0_reg[12] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_tvalid),
        .D(s00_axis_tdata[12]),
        .Q(DI0[12]),
        .R(1'b0));
  FDRE \DI0_reg[13] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_tvalid),
        .D(s00_axis_tdata[13]),
        .Q(DI0[13]),
        .R(1'b0));
  FDRE \DI0_reg[14] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_tvalid),
        .D(s00_axis_tdata[14]),
        .Q(DI0[14]),
        .R(1'b0));
  FDRE \DI0_reg[15] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_tvalid),
        .D(s00_axis_tdata[15]),
        .Q(DI0[15]),
        .R(1'b0));
  FDRE \DI0_reg[16] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_tvalid),
        .D(s00_axis_tdata[16]),
        .Q(DI0[16]),
        .R(1'b0));
  FDRE \DI0_reg[17] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_tvalid),
        .D(s00_axis_tdata[17]),
        .Q(DI0[17]),
        .R(1'b0));
  FDRE \DI0_reg[18] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_tvalid),
        .D(s00_axis_tdata[18]),
        .Q(DI0[18]),
        .R(1'b0));
  FDRE \DI0_reg[19] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_tvalid),
        .D(s00_axis_tdata[19]),
        .Q(DI0[19]),
        .R(1'b0));
  FDRE \DI0_reg[1] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_tvalid),
        .D(s00_axis_tdata[1]),
        .Q(DI0[1]),
        .R(1'b0));
  FDRE \DI0_reg[20] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_tvalid),
        .D(s00_axis_tdata[20]),
        .Q(DI0[20]),
        .R(1'b0));
  FDRE \DI0_reg[21] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_tvalid),
        .D(s00_axis_tdata[21]),
        .Q(DI0[21]),
        .R(1'b0));
  FDRE \DI0_reg[22] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_tvalid),
        .D(s00_axis_tdata[22]),
        .Q(DI0[22]),
        .R(1'b0));
  FDRE \DI0_reg[23] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_tvalid),
        .D(s00_axis_tdata[23]),
        .Q(DI0[23]),
        .R(1'b0));
  FDRE \DI0_reg[24] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_tvalid),
        .D(s00_axis_tdata[24]),
        .Q(DI0[24]),
        .R(1'b0));
  FDRE \DI0_reg[25] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_tvalid),
        .D(s00_axis_tdata[25]),
        .Q(DI0[25]),
        .R(1'b0));
  FDRE \DI0_reg[26] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_tvalid),
        .D(s00_axis_tdata[26]),
        .Q(DI0[26]),
        .R(1'b0));
  FDRE \DI0_reg[27] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_tvalid),
        .D(s00_axis_tdata[27]),
        .Q(DI0[27]),
        .R(1'b0));
  FDRE \DI0_reg[28] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_tvalid),
        .D(s00_axis_tdata[28]),
        .Q(DI0[28]),
        .R(1'b0));
  FDRE \DI0_reg[29] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_tvalid),
        .D(s00_axis_tdata[29]),
        .Q(DI0[29]),
        .R(1'b0));
  FDRE \DI0_reg[2] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_tvalid),
        .D(s00_axis_tdata[2]),
        .Q(DI0[2]),
        .R(1'b0));
  FDRE \DI0_reg[30] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_tvalid),
        .D(s00_axis_tdata[30]),
        .Q(DI0[30]),
        .R(1'b0));
  FDRE \DI0_reg[31] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_tvalid),
        .D(s00_axis_tdata[31]),
        .Q(DI0[31]),
        .R(1'b0));
  FDRE \DI0_reg[3] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_tvalid),
        .D(s00_axis_tdata[3]),
        .Q(DI0[3]),
        .R(1'b0));
  FDRE \DI0_reg[4] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_tvalid),
        .D(s00_axis_tdata[4]),
        .Q(DI0[4]),
        .R(1'b0));
  FDRE \DI0_reg[5] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_tvalid),
        .D(s00_axis_tdata[5]),
        .Q(DI0[5]),
        .R(1'b0));
  FDRE \DI0_reg[6] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_tvalid),
        .D(s00_axis_tdata[6]),
        .Q(DI0[6]),
        .R(1'b0));
  FDRE \DI0_reg[7] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_tvalid),
        .D(s00_axis_tdata[7]),
        .Q(DI0[7]),
        .R(1'b0));
  FDRE \DI0_reg[8] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_tvalid),
        .D(s00_axis_tdata[8]),
        .Q(DI0[8]),
        .R(1'b0));
  FDRE \DI0_reg[9] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_tvalid),
        .D(s00_axis_tdata[9]),
        .Q(DI0[9]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT2 #(
    .INIT(4'h1)) 
    \EN_r[0]_i_1 
       (.I0(cnt_inner_valid[4]),
        .I1(m00_axis_tvalid_r_i_2_n_0),
        .O(\EN_r[0]_i_1_n_0 ));
  FDRE \EN_r_reg[0] 
       (.C(m00_axis_aclk),
        .CE(s00_axis_tvalid),
        .D(\EN_r[0]_i_1_n_0 ),
        .Q(EN_r),
        .R(1'b0));
  FDRE WE_r_reg
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(s00_axis_tvalid),
        .Q(WE_r),
        .R(1'b0));
  FDSE \adr_wr_reg[0] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(s00_axis_tuser[0]),
        .Q(adr_wr[0]),
        .S(\ctrl_r[3]_i_1_n_0 ));
  FDSE \adr_wr_reg[10] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(s00_axis_tuser[10]),
        .Q(adr_wr[10]),
        .S(\ctrl_r[3]_i_1_n_0 ));
  FDSE \adr_wr_reg[1] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(s00_axis_tuser[1]),
        .Q(adr_wr[1]),
        .S(\ctrl_r[3]_i_1_n_0 ));
  FDSE \adr_wr_reg[2] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(s00_axis_tuser[2]),
        .Q(adr_wr[2]),
        .S(\ctrl_r[3]_i_1_n_0 ));
  FDSE \adr_wr_reg[3] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(s00_axis_tuser[3]),
        .Q(adr_wr[3]),
        .S(\ctrl_r[3]_i_1_n_0 ));
  FDSE \adr_wr_reg[4] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(s00_axis_tuser[4]),
        .Q(adr_wr[4]),
        .S(\ctrl_r[3]_i_1_n_0 ));
  FDSE \adr_wr_reg[5] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(s00_axis_tuser[5]),
        .Q(adr_wr[5]),
        .S(\ctrl_r[3]_i_1_n_0 ));
  FDSE \adr_wr_reg[6] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(s00_axis_tuser[6]),
        .Q(adr_wr[6]),
        .S(\ctrl_r[3]_i_1_n_0 ));
  FDSE \adr_wr_reg[7] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(s00_axis_tuser[7]),
        .Q(adr_wr[7]),
        .S(\ctrl_r[3]_i_1_n_0 ));
  FDSE \adr_wr_reg[8] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(s00_axis_tuser[8]),
        .Q(adr_wr[8]),
        .S(\ctrl_r[3]_i_1_n_0 ));
  FDSE \adr_wr_reg[9] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(s00_axis_tuser[9]),
        .Q(adr_wr[9]),
        .S(\ctrl_r[3]_i_1_n_0 ));
  CARRY4 cnt_inner_valid0_carry
       (.CI(1'b0),
        .CO({cnt_inner_valid0_carry_n_0,cnt_inner_valid0_carry_n_1,cnt_inner_valid0_carry_n_2,cnt_inner_valid0_carry_n_3}),
        .CYINIT(cnt_inner_valid[0]),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(data0[4:1]),
        .S(cnt_inner_valid[4:1]));
  CARRY4 cnt_inner_valid0_carry__0
       (.CI(cnt_inner_valid0_carry_n_0),
        .CO({cnt_inner_valid0_carry__0_n_0,cnt_inner_valid0_carry__0_n_1,cnt_inner_valid0_carry__0_n_2,cnt_inner_valid0_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(data0[8:5]),
        .S(cnt_inner_valid[8:5]));
  CARRY4 cnt_inner_valid0_carry__1
       (.CI(cnt_inner_valid0_carry__0_n_0),
        .CO({cnt_inner_valid0_carry__1_n_0,cnt_inner_valid0_carry__1_n_1,cnt_inner_valid0_carry__1_n_2,cnt_inner_valid0_carry__1_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(data0[12:9]),
        .S(cnt_inner_valid[12:9]));
  CARRY4 cnt_inner_valid0_carry__2
       (.CI(cnt_inner_valid0_carry__1_n_0),
        .CO({NLW_cnt_inner_valid0_carry__2_CO_UNCONNECTED[3:2],cnt_inner_valid0_carry__2_n_2,cnt_inner_valid0_carry__2_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({NLW_cnt_inner_valid0_carry__2_O_UNCONNECTED[3],data0[15:13]}),
        .S({1'b0,cnt_inner_valid[15:13]}));
  LUT2 #(
    .INIT(4'h1)) 
    \cnt_inner_valid[0]_i_1 
       (.I0(cnt_inner_valid[0]),
        .I1(\cnt_inner_valid[0]_i_2_n_0 ),
        .O(cnt_inner_valid_0[0]));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    \cnt_inner_valid[0]_i_2 
       (.I0(\cnt_inner_valid[15]_i_5_n_0 ),
        .I1(\cnt_inner_valid[15]_i_4_n_0 ),
        .I2(\cnt_inner_valid[15]_i_3_n_0 ),
        .I3(cnt_inner_valid[12]),
        .I4(cnt_inner_valid[6]),
        .I5(cnt_inner_valid[15]),
        .O(\cnt_inner_valid[0]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFE00000000)) 
    \cnt_inner_valid[10]_i_1 
       (.I0(\cnt_inner_valid[15]_i_2_n_0 ),
        .I1(\cnt_inner_valid[15]_i_3_n_0 ),
        .I2(\cnt_inner_valid[15]_i_4_n_0 ),
        .I3(\cnt_inner_valid[15]_i_5_n_0 ),
        .I4(cnt_inner_valid[0]),
        .I5(data0[10]),
        .O(cnt_inner_valid_0[10]));
  LUT6 #(
    .INIT(64'hFFFFFFFE00000000)) 
    \cnt_inner_valid[11]_i_1 
       (.I0(\cnt_inner_valid[15]_i_2_n_0 ),
        .I1(\cnt_inner_valid[15]_i_3_n_0 ),
        .I2(\cnt_inner_valid[15]_i_4_n_0 ),
        .I3(\cnt_inner_valid[15]_i_5_n_0 ),
        .I4(cnt_inner_valid[0]),
        .I5(data0[11]),
        .O(cnt_inner_valid_0[11]));
  LUT6 #(
    .INIT(64'hFFFFFFFE00000000)) 
    \cnt_inner_valid[12]_i_1 
       (.I0(\cnt_inner_valid[15]_i_2_n_0 ),
        .I1(\cnt_inner_valid[15]_i_3_n_0 ),
        .I2(\cnt_inner_valid[15]_i_4_n_0 ),
        .I3(\cnt_inner_valid[15]_i_5_n_0 ),
        .I4(cnt_inner_valid[0]),
        .I5(data0[12]),
        .O(cnt_inner_valid_0[12]));
  LUT6 #(
    .INIT(64'hFFFFFFFE00000000)) 
    \cnt_inner_valid[13]_i_1 
       (.I0(\cnt_inner_valid[15]_i_2_n_0 ),
        .I1(\cnt_inner_valid[15]_i_3_n_0 ),
        .I2(\cnt_inner_valid[15]_i_4_n_0 ),
        .I3(\cnt_inner_valid[15]_i_5_n_0 ),
        .I4(cnt_inner_valid[0]),
        .I5(data0[13]),
        .O(cnt_inner_valid_0[13]));
  LUT6 #(
    .INIT(64'hFFFFFFFE00000000)) 
    \cnt_inner_valid[14]_i_1 
       (.I0(\cnt_inner_valid[15]_i_2_n_0 ),
        .I1(\cnt_inner_valid[15]_i_3_n_0 ),
        .I2(\cnt_inner_valid[15]_i_4_n_0 ),
        .I3(\cnt_inner_valid[15]_i_5_n_0 ),
        .I4(cnt_inner_valid[0]),
        .I5(data0[14]),
        .O(cnt_inner_valid_0[14]));
  LUT6 #(
    .INIT(64'hFFFFFFFE00000000)) 
    \cnt_inner_valid[15]_i_1 
       (.I0(\cnt_inner_valid[15]_i_2_n_0 ),
        .I1(\cnt_inner_valid[15]_i_3_n_0 ),
        .I2(\cnt_inner_valid[15]_i_4_n_0 ),
        .I3(\cnt_inner_valid[15]_i_5_n_0 ),
        .I4(cnt_inner_valid[0]),
        .I5(data0[15]),
        .O(cnt_inner_valid_0[15]));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT3 #(
    .INIT(8'hFE)) 
    \cnt_inner_valid[15]_i_2 
       (.I0(cnt_inner_valid[12]),
        .I1(cnt_inner_valid[6]),
        .I2(cnt_inner_valid[15]),
        .O(\cnt_inner_valid[15]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'h7FFF)) 
    \cnt_inner_valid[15]_i_3 
       (.I0(cnt_inner_valid[7]),
        .I1(cnt_inner_valid[10]),
        .I2(cnt_inner_valid[8]),
        .I3(cnt_inner_valid[9]),
        .O(\cnt_inner_valid[15]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \cnt_inner_valid[15]_i_4 
       (.I0(cnt_inner_valid[5]),
        .I1(cnt_inner_valid[11]),
        .I2(cnt_inner_valid[13]),
        .I3(cnt_inner_valid[14]),
        .O(\cnt_inner_valid[15]_i_4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT4 #(
    .INIT(16'hFFFE)) 
    \cnt_inner_valid[15]_i_5 
       (.I0(cnt_inner_valid[1]),
        .I1(cnt_inner_valid[2]),
        .I2(cnt_inner_valid[3]),
        .I3(cnt_inner_valid[4]),
        .O(\cnt_inner_valid[15]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFE00000000)) 
    \cnt_inner_valid[1]_i_1 
       (.I0(\cnt_inner_valid[15]_i_2_n_0 ),
        .I1(\cnt_inner_valid[15]_i_3_n_0 ),
        .I2(\cnt_inner_valid[15]_i_4_n_0 ),
        .I3(\cnt_inner_valid[15]_i_5_n_0 ),
        .I4(cnt_inner_valid[0]),
        .I5(data0[1]),
        .O(cnt_inner_valid_0[1]));
  LUT6 #(
    .INIT(64'hFFFFFFFE00000000)) 
    \cnt_inner_valid[2]_i_1 
       (.I0(\cnt_inner_valid[15]_i_2_n_0 ),
        .I1(\cnt_inner_valid[15]_i_3_n_0 ),
        .I2(\cnt_inner_valid[15]_i_4_n_0 ),
        .I3(\cnt_inner_valid[15]_i_5_n_0 ),
        .I4(cnt_inner_valid[0]),
        .I5(data0[2]),
        .O(cnt_inner_valid_0[2]));
  LUT6 #(
    .INIT(64'hFFFFFFFE00000000)) 
    \cnt_inner_valid[3]_i_1 
       (.I0(\cnt_inner_valid[15]_i_2_n_0 ),
        .I1(\cnt_inner_valid[15]_i_3_n_0 ),
        .I2(\cnt_inner_valid[15]_i_4_n_0 ),
        .I3(\cnt_inner_valid[15]_i_5_n_0 ),
        .I4(cnt_inner_valid[0]),
        .I5(data0[3]),
        .O(cnt_inner_valid_0[3]));
  LUT6 #(
    .INIT(64'hFFFFFFFE00000000)) 
    \cnt_inner_valid[4]_i_1 
       (.I0(\cnt_inner_valid[15]_i_2_n_0 ),
        .I1(\cnt_inner_valid[15]_i_3_n_0 ),
        .I2(\cnt_inner_valid[15]_i_4_n_0 ),
        .I3(\cnt_inner_valid[15]_i_5_n_0 ),
        .I4(cnt_inner_valid[0]),
        .I5(data0[4]),
        .O(cnt_inner_valid_0[4]));
  LUT6 #(
    .INIT(64'hFFFFFFFE00000000)) 
    \cnt_inner_valid[5]_i_1 
       (.I0(\cnt_inner_valid[15]_i_2_n_0 ),
        .I1(\cnt_inner_valid[15]_i_3_n_0 ),
        .I2(\cnt_inner_valid[15]_i_4_n_0 ),
        .I3(\cnt_inner_valid[15]_i_5_n_0 ),
        .I4(cnt_inner_valid[0]),
        .I5(data0[5]),
        .O(cnt_inner_valid_0[5]));
  LUT6 #(
    .INIT(64'hFFFFFFFE00000000)) 
    \cnt_inner_valid[6]_i_1 
       (.I0(\cnt_inner_valid[15]_i_2_n_0 ),
        .I1(\cnt_inner_valid[15]_i_3_n_0 ),
        .I2(\cnt_inner_valid[15]_i_4_n_0 ),
        .I3(\cnt_inner_valid[15]_i_5_n_0 ),
        .I4(cnt_inner_valid[0]),
        .I5(data0[6]),
        .O(cnt_inner_valid_0[6]));
  LUT6 #(
    .INIT(64'hFFFFFFFE00000000)) 
    \cnt_inner_valid[7]_i_1 
       (.I0(\cnt_inner_valid[15]_i_2_n_0 ),
        .I1(\cnt_inner_valid[15]_i_3_n_0 ),
        .I2(\cnt_inner_valid[15]_i_4_n_0 ),
        .I3(\cnt_inner_valid[15]_i_5_n_0 ),
        .I4(cnt_inner_valid[0]),
        .I5(data0[7]),
        .O(cnt_inner_valid_0[7]));
  LUT6 #(
    .INIT(64'hFFFFFFFE00000000)) 
    \cnt_inner_valid[8]_i_1 
       (.I0(\cnt_inner_valid[15]_i_2_n_0 ),
        .I1(\cnt_inner_valid[15]_i_3_n_0 ),
        .I2(\cnt_inner_valid[15]_i_4_n_0 ),
        .I3(\cnt_inner_valid[15]_i_5_n_0 ),
        .I4(cnt_inner_valid[0]),
        .I5(data0[8]),
        .O(cnt_inner_valid_0[8]));
  LUT6 #(
    .INIT(64'hFFFFFFFE00000000)) 
    \cnt_inner_valid[9]_i_1 
       (.I0(\cnt_inner_valid[15]_i_2_n_0 ),
        .I1(\cnt_inner_valid[15]_i_3_n_0 ),
        .I2(\cnt_inner_valid[15]_i_4_n_0 ),
        .I3(\cnt_inner_valid[15]_i_5_n_0 ),
        .I4(cnt_inner_valid[0]),
        .I5(data0[9]),
        .O(cnt_inner_valid_0[9]));
  FDRE \cnt_inner_valid_reg[0] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(cnt_inner_valid_0[0]),
        .Q(cnt_inner_valid[0]),
        .R(\ctrl_r[3]_i_1_n_0 ));
  FDRE \cnt_inner_valid_reg[10] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(cnt_inner_valid_0[10]),
        .Q(cnt_inner_valid[10]),
        .R(\ctrl_r[3]_i_1_n_0 ));
  FDRE \cnt_inner_valid_reg[11] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(cnt_inner_valid_0[11]),
        .Q(cnt_inner_valid[11]),
        .R(\ctrl_r[3]_i_1_n_0 ));
  FDRE \cnt_inner_valid_reg[12] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(cnt_inner_valid_0[12]),
        .Q(cnt_inner_valid[12]),
        .R(\ctrl_r[3]_i_1_n_0 ));
  FDRE \cnt_inner_valid_reg[13] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(cnt_inner_valid_0[13]),
        .Q(cnt_inner_valid[13]),
        .R(\ctrl_r[3]_i_1_n_0 ));
  FDRE \cnt_inner_valid_reg[14] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(cnt_inner_valid_0[14]),
        .Q(cnt_inner_valid[14]),
        .R(\ctrl_r[3]_i_1_n_0 ));
  FDRE \cnt_inner_valid_reg[15] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(cnt_inner_valid_0[15]),
        .Q(cnt_inner_valid[15]),
        .R(\ctrl_r[3]_i_1_n_0 ));
  FDRE \cnt_inner_valid_reg[1] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(cnt_inner_valid_0[1]),
        .Q(cnt_inner_valid[1]),
        .R(\ctrl_r[3]_i_1_n_0 ));
  FDRE \cnt_inner_valid_reg[2] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(cnt_inner_valid_0[2]),
        .Q(cnt_inner_valid[2]),
        .R(\ctrl_r[3]_i_1_n_0 ));
  FDRE \cnt_inner_valid_reg[3] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(cnt_inner_valid_0[3]),
        .Q(cnt_inner_valid[3]),
        .R(\ctrl_r[3]_i_1_n_0 ));
  FDRE \cnt_inner_valid_reg[4] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(cnt_inner_valid_0[4]),
        .Q(cnt_inner_valid[4]),
        .R(\ctrl_r[3]_i_1_n_0 ));
  FDRE \cnt_inner_valid_reg[5] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(cnt_inner_valid_0[5]),
        .Q(cnt_inner_valid[5]),
        .R(\ctrl_r[3]_i_1_n_0 ));
  FDRE \cnt_inner_valid_reg[6] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(cnt_inner_valid_0[6]),
        .Q(cnt_inner_valid[6]),
        .R(\ctrl_r[3]_i_1_n_0 ));
  FDRE \cnt_inner_valid_reg[7] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(cnt_inner_valid_0[7]),
        .Q(cnt_inner_valid[7]),
        .R(\ctrl_r[3]_i_1_n_0 ));
  FDRE \cnt_inner_valid_reg[8] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(cnt_inner_valid_0[8]),
        .Q(cnt_inner_valid[8]),
        .R(\ctrl_r[3]_i_1_n_0 ));
  FDRE \cnt_inner_valid_reg[9] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(cnt_inner_valid_0[9]),
        .Q(cnt_inner_valid[9]),
        .R(\ctrl_r[3]_i_1_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \ctrl_r[3]_i_1 
       (.I0(s00_axis_tvalid),
        .O(\ctrl_r[3]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h4)) 
    \ctrl_r[3]_i_2 
       (.I0(\ctrl_r[3]_i_3_n_0 ),
        .I1(\ctrl_r[3]_i_4_n_0 ),
        .O(\ctrl_r[3]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFE0FFFF)) 
    \ctrl_r[3]_i_3 
       (.I0(s00_axis_tuser[16]),
        .I1(s00_axis_tuser[17]),
        .I2(s00_axis_tuser[18]),
        .I3(s00_axis_tuser[23]),
        .I4(s00_axis_tuser[15]),
        .I5(s00_axis_tuser[20]),
        .O(\ctrl_r[3]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    \ctrl_r[3]_i_4 
       (.I0(s00_axis_tuser[19]),
        .I1(s00_axis_tuser[24]),
        .I2(s00_axis_tuser[26]),
        .I3(s00_axis_tuser[21]),
        .I4(s00_axis_tuser[25]),
        .I5(s00_axis_tuser[22]),
        .O(\ctrl_r[3]_i_4_n_0 ));
  FDRE \ctrl_r_reg[0] 
       (.C(m00_axis_aclk),
        .CE(\ctrl_r[3]_i_2_n_0 ),
        .D(s00_axis_tuser[11]),
        .Q(\ctrl_r_reg_n_0_[0] ),
        .R(\ctrl_r[3]_i_1_n_0 ));
  FDRE \ctrl_r_reg[1] 
       (.C(m00_axis_aclk),
        .CE(\ctrl_r[3]_i_2_n_0 ),
        .D(s00_axis_tuser[12]),
        .Q(\ctrl_r_reg_n_0_[1] ),
        .R(\ctrl_r[3]_i_1_n_0 ));
  FDRE \ctrl_r_reg[2] 
       (.C(m00_axis_aclk),
        .CE(\ctrl_r[3]_i_2_n_0 ),
        .D(s00_axis_tuser[13]),
        .Q(\ctrl_r_reg_n_0_[2] ),
        .R(\ctrl_r[3]_i_1_n_0 ));
  FDRE \ctrl_r_reg[3] 
       (.C(m00_axis_aclk),
        .CE(\ctrl_r[3]_i_2_n_0 ),
        .D(s00_axis_tuser[14]),
        .Q(\ctrl_r_reg_n_0_[3] ),
        .R(\ctrl_r[3]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0505050505050514)) 
    m00_axis_tvalid_r_i_1
       (.I0(m00_axis_tvalid_r_i_2_n_0),
        .I1(cnt_inner_valid[3]),
        .I2(cnt_inner_valid[4]),
        .I3(cnt_inner_valid[2]),
        .I4(cnt_inner_valid[1]),
        .I5(cnt_inner_valid[0]),
        .O(m00_axis_tvalid_r0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    m00_axis_tvalid_r_i_2
       (.I0(cnt_inner_valid[10]),
        .I1(cnt_inner_valid[9]),
        .I2(cnt_inner_valid[7]),
        .I3(m00_axis_tvalid_r_i_3_n_0),
        .I4(\cnt_inner_valid[15]_i_4_n_0 ),
        .O(m00_axis_tvalid_r_i_2_n_0));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT4 #(
    .INIT(16'hFFFE)) 
    m00_axis_tvalid_r_i_3
       (.I0(cnt_inner_valid[8]),
        .I1(cnt_inner_valid[15]),
        .I2(cnt_inner_valid[6]),
        .I3(cnt_inner_valid[12]),
        .O(m00_axis_tvalid_r_i_3_n_0));
  FDRE m00_axis_tvalid_r_reg
       (.C(m00_axis_aclk),
        .CE(s00_axis_tvalid),
        .D(m00_axis_tvalid_r0),
        .Q(m00_axis_tvalid),
        .R(1'b0));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst rams_sp_rf_rst_inst0
       (.Q(DI0),
        .WE_r(WE_r),
        .addr(adr_wr),
        .en(EN_r),
        .m00_axis_aclk(m00_axis_aclk),
        .m00_axis_tdata(m00_axis_tdata),
        .\m00_axis_tdata[0]_0 (\ctrl_r_reg_n_0_[0] ),
        .\m00_axis_tdata[0]_1 (\ctrl_r_reg_n_0_[1] ),
        .\m00_axis_tdata[0]_2 (\ctrl_r_reg_n_0_[3] ),
        .m00_axis_tdata_0_sp_1(\ctrl_r_reg_n_0_[2] ),
        .square_r(square_r));
  LUT3 #(
    .INIT(8'h2E)) 
    square_r_i_1
       (.I0(square_r),
        .I1(s00_axis_tvalid),
        .I2(s00_axis_tuser[15]),
        .O(square_r_i_1_n_0));
  FDRE square_r_reg
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(square_r_i_1_n_0),
        .Q(square_r),
        .R(1'b0));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rams_sp_rf_rst
   (m00_axis_tdata,
    m00_axis_tdata_0_sp_1,
    \m00_axis_tdata[0]_0 ,
    \m00_axis_tdata[0]_1 ,
    \m00_axis_tdata[0]_2 ,
    m00_axis_aclk,
    en,
    addr,
    Q,
    WE_r,
    square_r);
  output [31:0]m00_axis_tdata;
  input m00_axis_tdata_0_sp_1;
  input \m00_axis_tdata[0]_0 ;
  input \m00_axis_tdata[0]_1 ;
  input \m00_axis_tdata[0]_2 ;
  input m00_axis_aclk;
  input en;
  input [10:0]addr;
  input [31:0]Q;
  input WE_r;
  input square_r;

  wire [31:0]DOUT0;
  wire [31:0]Q;
  wire WE_r;
  wire [10:0]addr;
  wire en;
  wire m00_axis_aclk;
  wire [31:0]m00_axis_tdata;
  wire \m00_axis_tdata[0]_0 ;
  wire \m00_axis_tdata[0]_1 ;
  wire \m00_axis_tdata[0]_2 ;
  wire m00_axis_tdata_0_sn_1;
  wire ram_reg_0_i_1_n_0;
  wire square_r;
  wire NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED;
  wire NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED;
  wire NLW_ram_reg_0_DBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED;
  wire NLW_ram_reg_0_SBITERR_UNCONNECTED;
  wire [31:16]NLW_ram_reg_0_DOADO_UNCONNECTED;
  wire [31:0]NLW_ram_reg_0_DOBDO_UNCONNECTED;
  wire [3:2]NLW_ram_reg_0_DOPADOP_UNCONNECTED;
  wire [3:0]NLW_ram_reg_0_DOPBDOP_UNCONNECTED;
  wire [7:0]NLW_ram_reg_0_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_ram_reg_0_RDADDRECC_UNCONNECTED;
  wire NLW_ram_reg_1_CASCADEOUTA_UNCONNECTED;
  wire NLW_ram_reg_1_CASCADEOUTB_UNCONNECTED;
  wire NLW_ram_reg_1_DBITERR_UNCONNECTED;
  wire NLW_ram_reg_1_INJECTDBITERR_UNCONNECTED;
  wire NLW_ram_reg_1_INJECTSBITERR_UNCONNECTED;
  wire NLW_ram_reg_1_SBITERR_UNCONNECTED;
  wire [31:14]NLW_ram_reg_1_DOADO_UNCONNECTED;
  wire [31:0]NLW_ram_reg_1_DOBDO_UNCONNECTED;
  wire [3:0]NLW_ram_reg_1_DOPADOP_UNCONNECTED;
  wire [3:0]NLW_ram_reg_1_DOPBDOP_UNCONNECTED;
  wire [7:0]NLW_ram_reg_1_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_ram_reg_1_RDADDRECC_UNCONNECTED;

  assign m00_axis_tdata_0_sn_1 = m00_axis_tdata_0_sp_1;
  LUT5 #(
    .INIT(32'h00000010)) 
    \m00_axis_tdata[0]_INST_0 
       (.I0(m00_axis_tdata_0_sn_1),
        .I1(\m00_axis_tdata[0]_0 ),
        .I2(DOUT0[0]),
        .I3(\m00_axis_tdata[0]_1 ),
        .I4(\m00_axis_tdata[0]_2 ),
        .O(m00_axis_tdata[0]));
  LUT5 #(
    .INIT(32'h00000010)) 
    \m00_axis_tdata[10]_INST_0 
       (.I0(m00_axis_tdata_0_sn_1),
        .I1(\m00_axis_tdata[0]_0 ),
        .I2(DOUT0[10]),
        .I3(\m00_axis_tdata[0]_1 ),
        .I4(\m00_axis_tdata[0]_2 ),
        .O(m00_axis_tdata[10]));
  LUT5 #(
    .INIT(32'h00000010)) 
    \m00_axis_tdata[11]_INST_0 
       (.I0(m00_axis_tdata_0_sn_1),
        .I1(\m00_axis_tdata[0]_0 ),
        .I2(DOUT0[11]),
        .I3(\m00_axis_tdata[0]_1 ),
        .I4(\m00_axis_tdata[0]_2 ),
        .O(m00_axis_tdata[11]));
  LUT5 #(
    .INIT(32'h00000010)) 
    \m00_axis_tdata[12]_INST_0 
       (.I0(m00_axis_tdata_0_sn_1),
        .I1(\m00_axis_tdata[0]_0 ),
        .I2(DOUT0[12]),
        .I3(\m00_axis_tdata[0]_1 ),
        .I4(\m00_axis_tdata[0]_2 ),
        .O(m00_axis_tdata[12]));
  LUT5 #(
    .INIT(32'h00000010)) 
    \m00_axis_tdata[13]_INST_0 
       (.I0(m00_axis_tdata_0_sn_1),
        .I1(\m00_axis_tdata[0]_0 ),
        .I2(DOUT0[13]),
        .I3(\m00_axis_tdata[0]_1 ),
        .I4(\m00_axis_tdata[0]_2 ),
        .O(m00_axis_tdata[13]));
  LUT5 #(
    .INIT(32'h00000010)) 
    \m00_axis_tdata[14]_INST_0 
       (.I0(m00_axis_tdata_0_sn_1),
        .I1(\m00_axis_tdata[0]_0 ),
        .I2(DOUT0[14]),
        .I3(\m00_axis_tdata[0]_1 ),
        .I4(\m00_axis_tdata[0]_2 ),
        .O(m00_axis_tdata[14]));
  LUT5 #(
    .INIT(32'h00000010)) 
    \m00_axis_tdata[15]_INST_0 
       (.I0(m00_axis_tdata_0_sn_1),
        .I1(\m00_axis_tdata[0]_0 ),
        .I2(DOUT0[15]),
        .I3(\m00_axis_tdata[0]_1 ),
        .I4(\m00_axis_tdata[0]_2 ),
        .O(m00_axis_tdata[15]));
  LUT5 #(
    .INIT(32'h00000010)) 
    \m00_axis_tdata[16]_INST_0 
       (.I0(m00_axis_tdata_0_sn_1),
        .I1(\m00_axis_tdata[0]_0 ),
        .I2(DOUT0[16]),
        .I3(\m00_axis_tdata[0]_1 ),
        .I4(\m00_axis_tdata[0]_2 ),
        .O(m00_axis_tdata[16]));
  LUT5 #(
    .INIT(32'h00000010)) 
    \m00_axis_tdata[17]_INST_0 
       (.I0(m00_axis_tdata_0_sn_1),
        .I1(\m00_axis_tdata[0]_0 ),
        .I2(DOUT0[17]),
        .I3(\m00_axis_tdata[0]_1 ),
        .I4(\m00_axis_tdata[0]_2 ),
        .O(m00_axis_tdata[17]));
  LUT5 #(
    .INIT(32'h00000010)) 
    \m00_axis_tdata[18]_INST_0 
       (.I0(m00_axis_tdata_0_sn_1),
        .I1(\m00_axis_tdata[0]_0 ),
        .I2(DOUT0[18]),
        .I3(\m00_axis_tdata[0]_1 ),
        .I4(\m00_axis_tdata[0]_2 ),
        .O(m00_axis_tdata[18]));
  LUT5 #(
    .INIT(32'h00000010)) 
    \m00_axis_tdata[19]_INST_0 
       (.I0(m00_axis_tdata_0_sn_1),
        .I1(\m00_axis_tdata[0]_0 ),
        .I2(DOUT0[19]),
        .I3(\m00_axis_tdata[0]_1 ),
        .I4(\m00_axis_tdata[0]_2 ),
        .O(m00_axis_tdata[19]));
  LUT5 #(
    .INIT(32'h00000010)) 
    \m00_axis_tdata[1]_INST_0 
       (.I0(m00_axis_tdata_0_sn_1),
        .I1(\m00_axis_tdata[0]_0 ),
        .I2(DOUT0[1]),
        .I3(\m00_axis_tdata[0]_1 ),
        .I4(\m00_axis_tdata[0]_2 ),
        .O(m00_axis_tdata[1]));
  LUT5 #(
    .INIT(32'h00000010)) 
    \m00_axis_tdata[20]_INST_0 
       (.I0(m00_axis_tdata_0_sn_1),
        .I1(\m00_axis_tdata[0]_0 ),
        .I2(DOUT0[20]),
        .I3(\m00_axis_tdata[0]_1 ),
        .I4(\m00_axis_tdata[0]_2 ),
        .O(m00_axis_tdata[20]));
  LUT5 #(
    .INIT(32'h00000010)) 
    \m00_axis_tdata[21]_INST_0 
       (.I0(m00_axis_tdata_0_sn_1),
        .I1(\m00_axis_tdata[0]_0 ),
        .I2(DOUT0[21]),
        .I3(\m00_axis_tdata[0]_1 ),
        .I4(\m00_axis_tdata[0]_2 ),
        .O(m00_axis_tdata[21]));
  LUT5 #(
    .INIT(32'h00000010)) 
    \m00_axis_tdata[22]_INST_0 
       (.I0(m00_axis_tdata_0_sn_1),
        .I1(\m00_axis_tdata[0]_0 ),
        .I2(DOUT0[22]),
        .I3(\m00_axis_tdata[0]_1 ),
        .I4(\m00_axis_tdata[0]_2 ),
        .O(m00_axis_tdata[22]));
  LUT5 #(
    .INIT(32'h00000010)) 
    \m00_axis_tdata[23]_INST_0 
       (.I0(m00_axis_tdata_0_sn_1),
        .I1(\m00_axis_tdata[0]_0 ),
        .I2(DOUT0[23]),
        .I3(\m00_axis_tdata[0]_1 ),
        .I4(\m00_axis_tdata[0]_2 ),
        .O(m00_axis_tdata[23]));
  LUT5 #(
    .INIT(32'h00000010)) 
    \m00_axis_tdata[24]_INST_0 
       (.I0(m00_axis_tdata_0_sn_1),
        .I1(\m00_axis_tdata[0]_0 ),
        .I2(DOUT0[24]),
        .I3(\m00_axis_tdata[0]_1 ),
        .I4(\m00_axis_tdata[0]_2 ),
        .O(m00_axis_tdata[24]));
  LUT5 #(
    .INIT(32'h00000010)) 
    \m00_axis_tdata[25]_INST_0 
       (.I0(m00_axis_tdata_0_sn_1),
        .I1(\m00_axis_tdata[0]_0 ),
        .I2(DOUT0[25]),
        .I3(\m00_axis_tdata[0]_1 ),
        .I4(\m00_axis_tdata[0]_2 ),
        .O(m00_axis_tdata[25]));
  LUT5 #(
    .INIT(32'h00000010)) 
    \m00_axis_tdata[26]_INST_0 
       (.I0(m00_axis_tdata_0_sn_1),
        .I1(\m00_axis_tdata[0]_0 ),
        .I2(DOUT0[26]),
        .I3(\m00_axis_tdata[0]_1 ),
        .I4(\m00_axis_tdata[0]_2 ),
        .O(m00_axis_tdata[26]));
  LUT5 #(
    .INIT(32'h00000010)) 
    \m00_axis_tdata[27]_INST_0 
       (.I0(m00_axis_tdata_0_sn_1),
        .I1(\m00_axis_tdata[0]_0 ),
        .I2(DOUT0[27]),
        .I3(\m00_axis_tdata[0]_1 ),
        .I4(\m00_axis_tdata[0]_2 ),
        .O(m00_axis_tdata[27]));
  LUT5 #(
    .INIT(32'h00000010)) 
    \m00_axis_tdata[28]_INST_0 
       (.I0(m00_axis_tdata_0_sn_1),
        .I1(\m00_axis_tdata[0]_0 ),
        .I2(DOUT0[28]),
        .I3(\m00_axis_tdata[0]_1 ),
        .I4(\m00_axis_tdata[0]_2 ),
        .O(m00_axis_tdata[28]));
  LUT5 #(
    .INIT(32'h00000010)) 
    \m00_axis_tdata[29]_INST_0 
       (.I0(m00_axis_tdata_0_sn_1),
        .I1(\m00_axis_tdata[0]_0 ),
        .I2(DOUT0[29]),
        .I3(\m00_axis_tdata[0]_1 ),
        .I4(\m00_axis_tdata[0]_2 ),
        .O(m00_axis_tdata[29]));
  LUT5 #(
    .INIT(32'h00000010)) 
    \m00_axis_tdata[2]_INST_0 
       (.I0(m00_axis_tdata_0_sn_1),
        .I1(\m00_axis_tdata[0]_0 ),
        .I2(DOUT0[2]),
        .I3(\m00_axis_tdata[0]_1 ),
        .I4(\m00_axis_tdata[0]_2 ),
        .O(m00_axis_tdata[2]));
  LUT5 #(
    .INIT(32'h00000010)) 
    \m00_axis_tdata[30]_INST_0 
       (.I0(m00_axis_tdata_0_sn_1),
        .I1(\m00_axis_tdata[0]_0 ),
        .I2(DOUT0[30]),
        .I3(\m00_axis_tdata[0]_1 ),
        .I4(\m00_axis_tdata[0]_2 ),
        .O(m00_axis_tdata[30]));
  LUT5 #(
    .INIT(32'h00000010)) 
    \m00_axis_tdata[31]_INST_0 
       (.I0(m00_axis_tdata_0_sn_1),
        .I1(\m00_axis_tdata[0]_0 ),
        .I2(DOUT0[31]),
        .I3(\m00_axis_tdata[0]_1 ),
        .I4(\m00_axis_tdata[0]_2 ),
        .O(m00_axis_tdata[31]));
  LUT5 #(
    .INIT(32'h00000010)) 
    \m00_axis_tdata[3]_INST_0 
       (.I0(m00_axis_tdata_0_sn_1),
        .I1(\m00_axis_tdata[0]_0 ),
        .I2(DOUT0[3]),
        .I3(\m00_axis_tdata[0]_1 ),
        .I4(\m00_axis_tdata[0]_2 ),
        .O(m00_axis_tdata[3]));
  LUT5 #(
    .INIT(32'h00000010)) 
    \m00_axis_tdata[4]_INST_0 
       (.I0(m00_axis_tdata_0_sn_1),
        .I1(\m00_axis_tdata[0]_0 ),
        .I2(DOUT0[4]),
        .I3(\m00_axis_tdata[0]_1 ),
        .I4(\m00_axis_tdata[0]_2 ),
        .O(m00_axis_tdata[4]));
  LUT5 #(
    .INIT(32'h00000010)) 
    \m00_axis_tdata[5]_INST_0 
       (.I0(m00_axis_tdata_0_sn_1),
        .I1(\m00_axis_tdata[0]_0 ),
        .I2(DOUT0[5]),
        .I3(\m00_axis_tdata[0]_1 ),
        .I4(\m00_axis_tdata[0]_2 ),
        .O(m00_axis_tdata[5]));
  LUT5 #(
    .INIT(32'h00000010)) 
    \m00_axis_tdata[6]_INST_0 
       (.I0(m00_axis_tdata_0_sn_1),
        .I1(\m00_axis_tdata[0]_0 ),
        .I2(DOUT0[6]),
        .I3(\m00_axis_tdata[0]_1 ),
        .I4(\m00_axis_tdata[0]_2 ),
        .O(m00_axis_tdata[6]));
  LUT5 #(
    .INIT(32'h00000010)) 
    \m00_axis_tdata[7]_INST_0 
       (.I0(m00_axis_tdata_0_sn_1),
        .I1(\m00_axis_tdata[0]_0 ),
        .I2(DOUT0[7]),
        .I3(\m00_axis_tdata[0]_1 ),
        .I4(\m00_axis_tdata[0]_2 ),
        .O(m00_axis_tdata[7]));
  LUT5 #(
    .INIT(32'h00000010)) 
    \m00_axis_tdata[8]_INST_0 
       (.I0(m00_axis_tdata_0_sn_1),
        .I1(\m00_axis_tdata[0]_0 ),
        .I2(DOUT0[8]),
        .I3(\m00_axis_tdata[0]_1 ),
        .I4(\m00_axis_tdata[0]_2 ),
        .O(m00_axis_tdata[8]));
  LUT5 #(
    .INIT(32'h00000010)) 
    \m00_axis_tdata[9]_INST_0 
       (.I0(m00_axis_tdata_0_sn_1),
        .I1(\m00_axis_tdata[0]_0 ),
        .I2(DOUT0[9]),
        .I3(\m00_axis_tdata[0]_1 ),
        .I4(\m00_axis_tdata[0]_2 ),
        .O(m00_axis_tdata[9]));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p2_d16" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "61472" *) 
  (* RTL_RAM_NAME = "ram" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "2047" *) 
  (* bram_slice_begin = "0" *) 
  (* bram_slice_end = "17" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "2047" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "17" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(18),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(18),
    .WRITE_WIDTH_B(0)) 
    ram_reg_0
       (.ADDRARDADDR({1'b1,addr,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CASCADEINA(1'b1),
        .CASCADEINB(1'b0),
        .CASCADEOUTA(NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED),
        .CASCADEOUTB(NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED),
        .CLKARDCLK(m00_axis_aclk),
        .CLKBWRCLK(1'b0),
        .DBITERR(NLW_ram_reg_0_DBITERR_UNCONNECTED),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,Q[15:0]}),
        .DIBDI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0,Q[17:16]}),
        .DIPBDIP({1'b1,1'b1,1'b1,1'b1}),
        .DOADO({NLW_ram_reg_0_DOADO_UNCONNECTED[31:16],DOUT0[15:0]}),
        .DOBDO(NLW_ram_reg_0_DOBDO_UNCONNECTED[31:0]),
        .DOPADOP({NLW_ram_reg_0_DOPADOP_UNCONNECTED[3:2],DOUT0[17:16]}),
        .DOPBDOP(NLW_ram_reg_0_DOPBDOP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_ram_reg_0_ECCPARITY_UNCONNECTED[7:0]),
        .ENARDEN(en),
        .ENBWREN(1'b0),
        .INJECTDBITERR(NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED),
        .INJECTSBITERR(NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED),
        .RDADDRECC(NLW_ram_reg_0_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_ram_reg_0_SBITERR_UNCONNECTED),
        .WEA({ram_reg_0_i_1_n_0,ram_reg_0_i_1_n_0,ram_reg_0_i_1_n_0,ram_reg_0_i_1_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
  LUT3 #(
    .INIT(8'h80)) 
    ram_reg_0_i_1
       (.I0(en),
        .I1(WE_r),
        .I2(square_r),
        .O(ram_reg_0_i_1_n_0));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d14" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "61472" *) 
  (* RTL_RAM_NAME = "ram" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "2047" *) 
  (* bram_slice_begin = "18" *) 
  (* bram_slice_end = "31" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "2047" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "18" *) 
  (* ram_slice_end = "31" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(18),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(18),
    .WRITE_WIDTH_B(0)) 
    ram_reg_1
       (.ADDRARDADDR({1'b1,addr,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CASCADEINA(1'b1),
        .CASCADEINB(1'b0),
        .CASCADEOUTA(NLW_ram_reg_1_CASCADEOUTA_UNCONNECTED),
        .CASCADEOUTB(NLW_ram_reg_1_CASCADEOUTB_UNCONNECTED),
        .CLKARDCLK(m00_axis_aclk),
        .CLKBWRCLK(1'b0),
        .DBITERR(NLW_ram_reg_1_DBITERR_UNCONNECTED),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,Q[31:18]}),
        .DIBDI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0,1'b0,1'b0}),
        .DIPBDIP({1'b1,1'b1,1'b1,1'b1}),
        .DOADO({NLW_ram_reg_1_DOADO_UNCONNECTED[31:14],DOUT0[31:18]}),
        .DOBDO(NLW_ram_reg_1_DOBDO_UNCONNECTED[31:0]),
        .DOPADOP(NLW_ram_reg_1_DOPADOP_UNCONNECTED[3:0]),
        .DOPBDOP(NLW_ram_reg_1_DOPBDOP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_ram_reg_1_ECCPARITY_UNCONNECTED[7:0]),
        .ENARDEN(en),
        .ENBWREN(1'b0),
        .INJECTDBITERR(NLW_ram_reg_1_INJECTDBITERR_UNCONNECTED),
        .INJECTSBITERR(NLW_ram_reg_1_INJECTSBITERR_UNCONNECTED),
        .RDADDRECC(NLW_ram_reg_1_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_ram_reg_1_SBITERR_UNCONNECTED),
        .WEA({ram_reg_0_i_1_n_0,ram_reg_0_i_1_n_0,ram_reg_0_i_1_n_0,ram_reg_0_i_1_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
