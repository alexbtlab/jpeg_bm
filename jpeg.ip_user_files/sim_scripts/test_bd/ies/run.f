-makelib ies_lib/xilinx_vip -sv \
  "/tools/Xilinx/Vivado/2019.1/data/xilinx_vip/hdl/axi4stream_vip_axi4streampc.sv" \
  "/tools/Xilinx/Vivado/2019.1/data/xilinx_vip/hdl/axi_vip_axi4pc.sv" \
  "/tools/Xilinx/Vivado/2019.1/data/xilinx_vip/hdl/xil_common_vip_pkg.sv" \
  "/tools/Xilinx/Vivado/2019.1/data/xilinx_vip/hdl/axi4stream_vip_pkg.sv" \
  "/tools/Xilinx/Vivado/2019.1/data/xilinx_vip/hdl/axi_vip_pkg.sv" \
  "/tools/Xilinx/Vivado/2019.1/data/xilinx_vip/hdl/axi4stream_vip_if.sv" \
  "/tools/Xilinx/Vivado/2019.1/data/xilinx_vip/hdl/axi_vip_if.sv" \
  "/tools/Xilinx/Vivado/2019.1/data/xilinx_vip/hdl/clk_vip_if.sv" \
  "/tools/Xilinx/Vivado/2019.1/data/xilinx_vip/hdl/rst_vip_if.sv" \
-endlib
-makelib ies_lib/xil_defaultlib -sv \
  "/tools/Xilinx/Vivado/2019.1/data/ip/xpm/xpm_cdc/hdl/xpm_cdc.sv" \
  "/tools/Xilinx/Vivado/2019.1/data/ip/xpm/xpm_fifo/hdl/xpm_fifo.sv" \
  "/tools/Xilinx/Vivado/2019.1/data/ip/xpm/xpm_memory/hdl/xpm_memory.sv" \
-endlib
-makelib ies_lib/xpm \
  "/tools/Xilinx/Vivado/2019.1/data/ip/xpm/xpm_VCOMP.vhd" \
-endlib
-makelib ies_lib/xil_defaultlib \
  "../../../bd/test_bd/ipshared/4180/sources/aq_axis_djpeg.v" \
  "../../../bd/test_bd/ipshared/4180/sources/aq_axis_djpeg_ctrl.v" \
  "../../../bd/test_bd/ipshared/4180/sources/aq_djpeg.v" \
  "../../../bd/test_bd/ipshared/4180/sources/aq_djpeg_dht.v" \
  "../../../bd/test_bd/ipshared/4180/sources/aq_djpeg_dqt.v" \
  "../../../bd/test_bd/ipshared/4180/sources/aq_djpeg_fsm.v" \
  "../../../bd/test_bd/ipshared/4180/sources/aq_djpeg_hm_decode.v" \
  "../../../bd/test_bd/ipshared/4180/sources/aq_djpeg_huffman.v" \
  "../../../bd/test_bd/ipshared/4180/sources/aq_djpeg_idct.v" \
  "../../../bd/test_bd/ipshared/4180/sources/aq_djpeg_idct_calc.v" \
  "../../../bd/test_bd/ipshared/4180/sources/aq_djpeg_idctb.v" \
  "../../../bd/test_bd/ipshared/4180/sources/aq_djpeg_regdata.v" \
  "../../../bd/test_bd/ipshared/4180/sources/aq_djpeg_ycbcr.v" \
  "../../../bd/test_bd/ipshared/4180/sources/aq_djpeg_ycbcr2rgb.v" \
  "../../../bd/test_bd/ipshared/4180/sources/aq_djpeg_ycbcr_mem.v" \
  "../../../bd/test_bd/ipshared/4180/sources/aq_djpeg_ziguzagu.v" \
  "../../../bd/test_bd/ip/test_bd_aq_axis_djpeg_0_0/sim/test_bd_aq_axis_djpeg_0_0.v" \
  "../../../bd/test_bd/ipshared/68d5/hdl/rams_sp_rf_rst.v" \
-endlib
-makelib ies_lib/xil_defaultlib -sv \
  "../../../bd/test_bd/ipshared/68d5/src/pixelReOrder_v1_0.sv" \
  "../../../bd/test_bd/ip/test_bd_pixelReOrder_0_0/sim/test_bd_pixelReOrder_0_0.sv" \
-endlib
-makelib ies_lib/axis_infrastructure_v1_1_0 \
  "../../../../jpeg.srcs/sources_1/bd/test_bd/ipshared/8713/hdl/axis_infrastructure_v1_1_vl_rfs.v" \
-endlib
-makelib ies_lib/axis_data_fifo_v2_0_1 \
  "../../../../jpeg.srcs/sources_1/bd/test_bd/ipshared/e1b1/hdl/axis_data_fifo_v2_0_vl_rfs.v" \
-endlib
-makelib ies_lib/xil_defaultlib \
  "../../../bd/test_bd/ip/test_bd_axis_data_fifo_0_0/sim/test_bd_axis_data_fifo_0_0.v" \
  "../../../bd/test_bd/sim/test_bd.v" \
-endlib
-makelib ies_lib/xil_defaultlib \
  glbl.v
-endlib

