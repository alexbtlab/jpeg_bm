vlib questa_lib/work
vlib questa_lib/msim

vlib questa_lib/msim/xilinx_vip
vlib questa_lib/msim/xil_defaultlib
vlib questa_lib/msim/xpm
vlib questa_lib/msim/axis_infrastructure_v1_1_0
vlib questa_lib/msim/axis_data_fifo_v2_0_1

vmap xilinx_vip questa_lib/msim/xilinx_vip
vmap xil_defaultlib questa_lib/msim/xil_defaultlib
vmap xpm questa_lib/msim/xpm
vmap axis_infrastructure_v1_1_0 questa_lib/msim/axis_infrastructure_v1_1_0
vmap axis_data_fifo_v2_0_1 questa_lib/msim/axis_data_fifo_v2_0_1

vlog -work xilinx_vip -64 -sv -L axi_vip_v1_1_5 -L processing_system7_vip_v1_0_7 -L xilinx_vip "+incdir+/tools/Xilinx/Vivado/2019.1/data/xilinx_vip/include" \
"/tools/Xilinx/Vivado/2019.1/data/xilinx_vip/hdl/axi4stream_vip_axi4streampc.sv" \
"/tools/Xilinx/Vivado/2019.1/data/xilinx_vip/hdl/axi_vip_axi4pc.sv" \
"/tools/Xilinx/Vivado/2019.1/data/xilinx_vip/hdl/xil_common_vip_pkg.sv" \
"/tools/Xilinx/Vivado/2019.1/data/xilinx_vip/hdl/axi4stream_vip_pkg.sv" \
"/tools/Xilinx/Vivado/2019.1/data/xilinx_vip/hdl/axi_vip_pkg.sv" \
"/tools/Xilinx/Vivado/2019.1/data/xilinx_vip/hdl/axi4stream_vip_if.sv" \
"/tools/Xilinx/Vivado/2019.1/data/xilinx_vip/hdl/axi_vip_if.sv" \
"/tools/Xilinx/Vivado/2019.1/data/xilinx_vip/hdl/clk_vip_if.sv" \
"/tools/Xilinx/Vivado/2019.1/data/xilinx_vip/hdl/rst_vip_if.sv" \

vlog -work xil_defaultlib -64 -sv -L axi_vip_v1_1_5 -L processing_system7_vip_v1_0_7 -L xilinx_vip "+incdir+../../../../jpeg.srcs/sources_1/bd/test_bd/ipshared/8713/hdl" "+incdir+/tools/Xilinx/Vivado/2019.1/data/xilinx_vip/include" \
"/tools/Xilinx/Vivado/2019.1/data/ip/xpm/xpm_cdc/hdl/xpm_cdc.sv" \
"/tools/Xilinx/Vivado/2019.1/data/ip/xpm/xpm_fifo/hdl/xpm_fifo.sv" \
"/tools/Xilinx/Vivado/2019.1/data/ip/xpm/xpm_memory/hdl/xpm_memory.sv" \

vcom -work xpm -64 -93 \
"/tools/Xilinx/Vivado/2019.1/data/ip/xpm/xpm_VCOMP.vhd" \

vlog -work xil_defaultlib -64 "+incdir+../../../../jpeg.srcs/sources_1/bd/test_bd/ipshared/8713/hdl" "+incdir+/tools/Xilinx/Vivado/2019.1/data/xilinx_vip/include" \
"../../../bd/test_bd/ipshared/4180/sources/aq_axis_djpeg.v" \
"../../../bd/test_bd/ipshared/4180/sources/aq_axis_djpeg_ctrl.v" \
"../../../bd/test_bd/ipshared/4180/sources/aq_djpeg.v" \
"../../../bd/test_bd/ipshared/4180/sources/aq_djpeg_dht.v" \
"../../../bd/test_bd/ipshared/4180/sources/aq_djpeg_dqt.v" \
"../../../bd/test_bd/ipshared/4180/sources/aq_djpeg_fsm.v" \
"../../../bd/test_bd/ipshared/4180/sources/aq_djpeg_hm_decode.v" \
"../../../bd/test_bd/ipshared/4180/sources/aq_djpeg_huffman.v" \
"../../../bd/test_bd/ipshared/4180/sources/aq_djpeg_idct.v" \
"../../../bd/test_bd/ipshared/4180/sources/aq_djpeg_idct_calc.v" \
"../../../bd/test_bd/ipshared/4180/sources/aq_djpeg_idctb.v" \
"../../../bd/test_bd/ipshared/4180/sources/aq_djpeg_regdata.v" \
"../../../bd/test_bd/ipshared/4180/sources/aq_djpeg_ycbcr.v" \
"../../../bd/test_bd/ipshared/4180/sources/aq_djpeg_ycbcr2rgb.v" \
"../../../bd/test_bd/ipshared/4180/sources/aq_djpeg_ycbcr_mem.v" \
"../../../bd/test_bd/ipshared/4180/sources/aq_djpeg_ziguzagu.v" \
"../../../bd/test_bd/ip/test_bd_aq_axis_djpeg_0_0/sim/test_bd_aq_axis_djpeg_0_0.v" \
"../../../bd/test_bd/ipshared/68d5/hdl/rams_sp_rf_rst.v" \

vlog -work xil_defaultlib -64 -sv -L axi_vip_v1_1_5 -L processing_system7_vip_v1_0_7 -L xilinx_vip "+incdir+../../../../jpeg.srcs/sources_1/bd/test_bd/ipshared/8713/hdl" "+incdir+/tools/Xilinx/Vivado/2019.1/data/xilinx_vip/include" \
"../../../bd/test_bd/ipshared/68d5/src/pixelReOrder_v1_0.sv" \
"../../../bd/test_bd/ip/test_bd_pixelReOrder_0_0/sim/test_bd_pixelReOrder_0_0.sv" \

vlog -work axis_infrastructure_v1_1_0 -64 "+incdir+../../../../jpeg.srcs/sources_1/bd/test_bd/ipshared/8713/hdl" "+incdir+/tools/Xilinx/Vivado/2019.1/data/xilinx_vip/include" \
"../../../../jpeg.srcs/sources_1/bd/test_bd/ipshared/8713/hdl/axis_infrastructure_v1_1_vl_rfs.v" \

vlog -work axis_data_fifo_v2_0_1 -64 "+incdir+../../../../jpeg.srcs/sources_1/bd/test_bd/ipshared/8713/hdl" "+incdir+/tools/Xilinx/Vivado/2019.1/data/xilinx_vip/include" \
"../../../../jpeg.srcs/sources_1/bd/test_bd/ipshared/e1b1/hdl/axis_data_fifo_v2_0_vl_rfs.v" \

vlog -work xil_defaultlib -64 "+incdir+../../../../jpeg.srcs/sources_1/bd/test_bd/ipshared/8713/hdl" "+incdir+/tools/Xilinx/Vivado/2019.1/data/xilinx_vip/include" \
"../../../bd/test_bd/ip/test_bd_axis_data_fifo_0_0/sim/test_bd_axis_data_fifo_0_0.v" \
"../../../bd/test_bd/sim/test_bd.v" \

vlog -work xil_defaultlib \
"glbl.v"

