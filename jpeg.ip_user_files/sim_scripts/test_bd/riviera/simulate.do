onbreak {quit -force}
onerror {quit -force}

asim -t 1ps +access +r +m+test_bd -L xilinx_vip -L xil_defaultlib -L xpm -L axis_infrastructure_v1_1_0 -L axis_data_fifo_v2_0_1 -L xilinx_vip -L unisims_ver -L unimacro_ver -L secureip -O5 xil_defaultlib.test_bd xil_defaultlib.glbl

do {wave.do}

view wave
view structure

do {test_bd.udo}

run -all

endsim

quit -force
