`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 10/15/2021 02:49:55 PM
// Design Name: 
// Module Name: tb_PixelReOrder_bd
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module tb_PixelReOrder_bd(   );
    
 wire [31:0] S_AXI_AWADDR;
  wire [3:0]  S_AXI_AWCACHE;
  wire [2:0]  S_AXI_AWPROT;
  wire        S_AXI_AWVALID;
  wire        S_AXI_AWREADY;

  // Write Data Channel
  wire [31:0] S_AXI_WDATA;
  wire [3:0]  S_AXI_WSTRB;
  wire        S_AXI_WVALID;
  wire        S_AXI_WREADY;

  // Write Response Channel
  wire        S_AXI_BVALID;
  wire        S_AXI_BREADY;
  wire [1:0]  S_AXI_BRESP;

  // Read Address Channel
  wire [31:0] S_AXI_ARADDR;
  wire [3:0]  S_AXI_ARCACHE;
  wire [2:0]  S_AXI_ARPROT;
  wire        S_AXI_ARVALID;
  wire        S_AXI_ARREADY;

  // Read Data Channel
  wire [31:0] S_AXI_RDATA;
  wire [1:0]  S_AXI_RRESP;
  wire        S_AXI_RVALID;
  wire        S_AXI_RREADY;
  
  wire        S_AXIS_TCLK;
  wire [31:0] S_AXIS_TDATA;
  wire        S_AXIS_TKEEP;
  wire        S_AXIS_TLAST;
  wire        S_AXIS_TREADY;
  wire [3:0]  S_AXIS_TSTRB;
  reg         S_AXIS_TVALID;

reg         hold_image;
	reg [23:0]	 rgb_mem [0:1920*1080-1];

    integer	DATA_COUNT;
    reg RST_N;
    reg CLK;
    reg clk_200MHz;
    parameter TIME10N = 2;

    always begin
        #(TIME10N/2) CLK = ~CLK;
    end
    always begin
        #(TIME10N) clk_200MHz = ~clk_200MHz;
    end

    initial begin
    // Initialize Inputs
    RST_N = 0;
    CLK   = 0;
    clk_200MHz = 0;
    
    // M_AXIS_TREADY = 1;
	 S_AXIS_TVALID = 0;
    #100;
    RST_N = 1;
  end


	integer	 count;
	
	initial begin
		count = 0;
		while(1) begin
		 @(posedge CLK);
		 count = count +1;
		end
	end

    wire M_AXIS_TLAST_0;
    integer fp;
    integer i;

//    `define FILENAME "/home/alexbtlab/project/aq_axis_djpeg_nekomona/model/sample.hex"
`define FILENAME "/home/alex/source/aq_axis_djpeg_nekomona-testbench/model/sample.hex"

    parameter FILETARGET = "/home/alexbtlab/project/aq_axis_djpeg_nekomona/model/id_15_10.dat";
     
      task_axilm u_task_axilm(
    // AXI4 Lite Interface
    .ARESETN      ( RST_N         ),
    .ACLK         ( CLK           ),
    // Write Address Channel
    .AXI_AWADDR   ( S_AXI_AWADDR  ),
    .AXI_AWCACHE  ( S_AXI_AWCACHE ),
    .AXI_AWPROT   ( S_AXI_AWPROT  ),
    .AXI_AWVALID  ( S_AXI_AWVALID ),
    .AXI_AWREADY  ( S_AXI_AWREADY ),
    // Write Data Channel
    .AXI_WDATA    ( S_AXI_WDATA   ),
    .AXI_WSTRB    ( S_AXI_WSTRB   ),
    .AXI_WVALID   ( S_AXI_WVALID  ),
    .AXI_WREADY   ( S_AXI_WREADY  ),
    // Write Response Channel
    .AXI_BVALID   ( S_AXI_BVALID  ),
    .AXI_BREADY   ( S_AXI_BREADY  ),
    .AXI_BRESP    ( S_AXI_BRESP   ),
    // Read Address Channel
    .AXI_ARADDR   ( S_AXI_ARADDR  ),
    .AXI_ARCACHE  ( S_AXI_ARCACHE ),
    .AXI_ARPROT   ( S_AXI_ARPROT  ),
    .AXI_ARVALID  ( S_AXI_ARVALID ),
    .AXI_ARREADY  ( S_AXI_ARREADY ),
    // Read Data Channel
    .AXI_RDATA    ( S_AXI_RDATA   ),
    .AXI_RRESP    ( S_AXI_RRESP   ),
    .AXI_RVALID   ( S_AXI_RVALID  ),
    .AXI_RREADY   ( S_AXI_RREADY  )
  );

	initial begin
		wait (RST_N);
		@(posedge CLK);
		$display(" Start Clock: %d",count);
    $display(" Start Clock: %d",count);
	    u_task_axilm.write(32'h0000_0000, 32'h8000_0000);
	    u_task_axilm.write(32'h0000_0000, 32'h0000_0000);
    
    $display(" Start Clock: %d",count);
        hold_image = 0;

		@(posedge CLK);
		@(posedge CLK);
		forever begin
		 if( S_AXIS_TREADY == 1'b1) begin
		 	S_AXIS_TVALID <= 1'b1;
		 end else begin
		 	S_AXIS_TVALID <= 1'b0;
		 end
		 @(posedge CLK);
		end
	end

    	initial begin
		forever begin 
		 @(posedge M_AXIS_TLAST_0)

		hold_image = 1;
		repeat(100) @(posedge CLK);
		hold_image = 0;
		
		@(posedge CLK);
		@(posedge CLK);
		@(posedge CLK);
		@(posedge CLK);
		@(posedge CLK);
		@(posedge CLK);
		@(posedge CLK);
		@(posedge CLK);

		$display(" End Clock %d",count);
		fp = $fopen(FILETARGET);
//		$fwrite(fp,"%0d\n",test_bd_wrapper_inst.u_axis_aq_djpeg.u_aq_djpeg.OutWidth);
//		$fwrite(fp,"%0d\n",test_bd_wrapper_inst.u_axis_aq_djpeg.u_aq_djpeg.OutHeight);

    $fwrite(fp,"%0d\n",test_bd_wrapper_inst.test_bd_i.aq_axis_djpeg_0.inst.OutHeight);
    $fwrite(fp,"%0d\n",test_bd_wrapper_inst.test_bd_i.aq_axis_djpeg_0.inst.OutWidth);
        
		for(i=0;i<test_bd_wrapper_inst.test_bd_i.aq_axis_djpeg_0.inst.OutWidth*test_bd_wrapper_inst.test_bd_i.aq_axis_djpeg_0.inst.OutHeight;i=i+1) begin
		 $fwrite(fp,"%06x\n",rgb_mem[i]);
		end
		$fclose(fp);

		// $coverage_save("sim.cov");
		$finish();
		
		
		$stop();
		end
	end

   
    reg [31:0] JPEG_MEM [0:1*1024*1024-1];
assign S_AXIS_TDATA = JPEG_MEM[DATA_COUNT];
	// Read JPEG File
	initial begin
		$readmemh(`FILENAME, JPEG_MEM);
	end

  	initial begin
		# 0;
		DATA_COUNT	<= 0;
		forever begin
			if(S_AXIS_TREADY & S_AXIS_TVALID) begin
				DATA_COUNT	<= DATA_COUNT +1;
			end
			 @(posedge CLK);
		end
	end
	assign S_AXIS_TDATA = JPEG_MEM[DATA_COUNT];


wire S_AXIS_1_tkeep;

test_bd_wrapper test_bd_wrapper_inst
   (
    .S_AXIS_1_tdata(S_AXIS_TDATA),
    .S_AXIS_1_tkeep(S_AXIS_1_tkeep),
    .S_AXIS_1_tlast(S_AXIS_TLAST),
    .S_AXIS_1_tready(S_AXIS_TREADY),
    .S_AXIS_1_tstrb(S_AXIS_TREADY),
    .S_AXIS_1_tvalid(S_AXIS_TVALID),
    .S_AXI_1_araddr(S_AXI_ARADDR),
    .S_AXI_1_arcache(S_AXI_ARCACHE),
    .S_AXI_1_arprot(S_AXI_ARPROT),
    .S_AXI_1_arready(S_AXI_ARREADY),
    .S_AXI_1_arvalid(S_AXI_ARVALID),
    .S_AXI_1_awaddr(S_AXI_AWADDR), 
    .S_AXI_1_awcache(S_AXI_AWCACHE),
    .S_AXI_1_awprot(S_AXI_AWPROT),
    .S_AXI_1_awready(S_AXI_AWREADY),
    .S_AXI_1_awvalid(S_AXI_AWVALID),
    .S_AXI_1_bready(S_AXI_BREADY),
    .S_AXI_1_bresp(S_AXI_BRESP),
    .S_AXI_1_bvalid(S_AXI_BVALID),
    .S_AXI_1_rdata(S_AXI_RDATA),
    .S_AXI_1_rready(S_AXI_RREADY),
    .S_AXI_1_rresp(S_AXI_RRESP),
    .S_AXI_1_rvalid(S_AXI_RVALID),
    .S_AXI_1_wdata(S_AXI_WDATA),
    .S_AXI_1_wready(S_AXI_WREADY),
    .S_AXI_1_wstrb(S_AXI_WSTRB),
    .S_AXI_1_wvalid(S_AXI_WVALID),
    .m00_axis_aclk_0(CLK),
    .s00_axis_aresetn_0(RST_N),

    .M_AXIS_TLAST_0(M_AXIS_TLAST_0),
    .m_axis_aclk_0(clk_200MHz)
    
    );
   

endmodule