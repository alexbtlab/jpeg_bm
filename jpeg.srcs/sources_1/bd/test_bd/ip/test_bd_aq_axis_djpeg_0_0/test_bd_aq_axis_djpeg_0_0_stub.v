// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (lin64) Build 2552052 Fri May 24 14:47:09 MDT 2019
// Date        : Fri Oct 15 15:38:18 2021
// Host        : alexbtlab-PC running 64-bit Ubuntu 18.04.5 LTS
// Command     : write_verilog -force -mode synth_stub
//               /home/alexbtlab/source/jpeg_bm/jpeg.srcs/sources_1/bd/test_bd/ip/test_bd_aq_axis_djpeg_0_0/test_bd_aq_axis_djpeg_0_0_stub.v
// Design      : test_bd_aq_axis_djpeg_0_0
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7z020clg484-1
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* X_CORE_INFO = "aq_axis_djpeg,Vivado 2019.1" *)
module test_bd_aq_axis_djpeg_0_0(ARESETN, ACLK, S_AXI_AWADDR, S_AXI_AWCACHE, 
  S_AXI_AWPROT, S_AXI_AWVALID, S_AXI_AWREADY, S_AXI_WDATA, S_AXI_WSTRB, S_AXI_WVALID, 
  S_AXI_WREADY, S_AXI_BVALID, S_AXI_BREADY, S_AXI_BRESP, S_AXI_ARADDR, S_AXI_ARCACHE, 
  S_AXI_ARPROT, S_AXI_ARVALID, S_AXI_ARREADY, S_AXI_RDATA, S_AXI_RRESP, S_AXI_RVALID, 
  S_AXI_RREADY, TCLK, S_AXIS_TDATA, S_AXIS_TKEEP, S_AXIS_TLAST, S_AXIS_TREADY, S_AXIS_TSTRB, 
  S_AXIS_TVALID, M_AXIS_TDATA, M_AXIS_TUSER, M_AXIS_TKEEP, M_AXIS_TLAST, M_AXIS_TREADY, 
  M_AXIS_TSTRB, M_AXIS_TVALID)
/* synthesis syn_black_box black_box_pad_pin="ARESETN,ACLK,S_AXI_AWADDR[31:0],S_AXI_AWCACHE[3:0],S_AXI_AWPROT[2:0],S_AXI_AWVALID,S_AXI_AWREADY,S_AXI_WDATA[31:0],S_AXI_WSTRB[3:0],S_AXI_WVALID,S_AXI_WREADY,S_AXI_BVALID,S_AXI_BREADY,S_AXI_BRESP[1:0],S_AXI_ARADDR[31:0],S_AXI_ARCACHE[3:0],S_AXI_ARPROT[2:0],S_AXI_ARVALID,S_AXI_ARREADY,S_AXI_RDATA[31:0],S_AXI_RRESP[1:0],S_AXI_RVALID,S_AXI_RREADY,TCLK,S_AXIS_TDATA[31:0],S_AXIS_TKEEP,S_AXIS_TLAST,S_AXIS_TREADY,S_AXIS_TSTRB[3:0],S_AXIS_TVALID,M_AXIS_TDATA[31:0],M_AXIS_TUSER[47:0],M_AXIS_TKEEP,M_AXIS_TLAST,M_AXIS_TREADY,M_AXIS_TSTRB[3:0],M_AXIS_TVALID" */;
  input ARESETN;
  input ACLK;
  input [31:0]S_AXI_AWADDR;
  input [3:0]S_AXI_AWCACHE;
  input [2:0]S_AXI_AWPROT;
  input S_AXI_AWVALID;
  output S_AXI_AWREADY;
  input [31:0]S_AXI_WDATA;
  input [3:0]S_AXI_WSTRB;
  input S_AXI_WVALID;
  output S_AXI_WREADY;
  output S_AXI_BVALID;
  input S_AXI_BREADY;
  output [1:0]S_AXI_BRESP;
  input [31:0]S_AXI_ARADDR;
  input [3:0]S_AXI_ARCACHE;
  input [2:0]S_AXI_ARPROT;
  input S_AXI_ARVALID;
  output S_AXI_ARREADY;
  output [31:0]S_AXI_RDATA;
  output [1:0]S_AXI_RRESP;
  output S_AXI_RVALID;
  input S_AXI_RREADY;
  input TCLK;
  input [31:0]S_AXIS_TDATA;
  input S_AXIS_TKEEP;
  input S_AXIS_TLAST;
  output S_AXIS_TREADY;
  input [3:0]S_AXIS_TSTRB;
  input S_AXIS_TVALID;
  output [31:0]M_AXIS_TDATA;
  output [47:0]M_AXIS_TUSER;
  output M_AXIS_TKEEP;
  output M_AXIS_TLAST;
  input M_AXIS_TREADY;
  output [3:0]M_AXIS_TSTRB;
  output M_AXIS_TVALID;
endmodule
