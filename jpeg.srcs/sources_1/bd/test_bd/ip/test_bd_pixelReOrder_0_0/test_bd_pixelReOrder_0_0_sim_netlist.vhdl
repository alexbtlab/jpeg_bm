-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1 (lin64) Build 2552052 Fri May 24 14:47:09 MDT 2019
-- Date        : Fri Oct 15 21:04:44 2021
-- Host        : alex-HP-Compaq-8200-Elite-CMT-PC running 64-bit Ubuntu 20.04.3 LTS
-- Command     : write_vhdl -force -mode funcsim
--               /home/alex/source/jpeg_bm/jpeg.srcs/sources_1/bd/test_bd/ip/test_bd_pixelReOrder_0_0/test_bd_pixelReOrder_0_0_sim_netlist.vhdl
-- Design      : test_bd_pixelReOrder_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7z020clg484-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity test_bd_pixelReOrder_0_0_multiplexer is
  port (
    Q : out STD_LOGIC_VECTOR ( 23 downto 0 );
    ctrl_mux_out_2 : in STD_LOGIC_VECTOR ( 4 downto 0 );
    D : in STD_LOGIC_VECTOR ( 23 downto 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of test_bd_pixelReOrder_0_0_multiplexer : entity is "multiplexer";
end test_bd_pixelReOrder_0_0_multiplexer;

architecture STRUCTURE of test_bd_pixelReOrder_0_0_multiplexer is
  signal \y_reg[23]_i_2__0_n_0\ : STD_LOGIC;
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of \y_reg[0]\ : label is "LD";
  attribute XILINX_LEGACY_PRIM of \y_reg[10]\ : label is "LD";
  attribute XILINX_LEGACY_PRIM of \y_reg[11]\ : label is "LD";
  attribute XILINX_LEGACY_PRIM of \y_reg[12]\ : label is "LD";
  attribute XILINX_LEGACY_PRIM of \y_reg[13]\ : label is "LD";
  attribute XILINX_LEGACY_PRIM of \y_reg[14]\ : label is "LD";
  attribute XILINX_LEGACY_PRIM of \y_reg[15]\ : label is "LD";
  attribute XILINX_LEGACY_PRIM of \y_reg[16]\ : label is "LD";
  attribute XILINX_LEGACY_PRIM of \y_reg[17]\ : label is "LD";
  attribute XILINX_LEGACY_PRIM of \y_reg[18]\ : label is "LD";
  attribute XILINX_LEGACY_PRIM of \y_reg[19]\ : label is "LD";
  attribute XILINX_LEGACY_PRIM of \y_reg[1]\ : label is "LD";
  attribute XILINX_LEGACY_PRIM of \y_reg[20]\ : label is "LD";
  attribute XILINX_LEGACY_PRIM of \y_reg[21]\ : label is "LD";
  attribute XILINX_LEGACY_PRIM of \y_reg[22]\ : label is "LD";
  attribute XILINX_LEGACY_PRIM of \y_reg[23]\ : label is "LD";
  attribute XILINX_LEGACY_PRIM of \y_reg[2]\ : label is "LD";
  attribute XILINX_LEGACY_PRIM of \y_reg[3]\ : label is "LD";
  attribute XILINX_LEGACY_PRIM of \y_reg[4]\ : label is "LD";
  attribute XILINX_LEGACY_PRIM of \y_reg[5]\ : label is "LD";
  attribute XILINX_LEGACY_PRIM of \y_reg[6]\ : label is "LD";
  attribute XILINX_LEGACY_PRIM of \y_reg[7]\ : label is "LD";
  attribute XILINX_LEGACY_PRIM of \y_reg[8]\ : label is "LD";
  attribute XILINX_LEGACY_PRIM of \y_reg[9]\ : label is "LD";
begin
\y_reg[0]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => D(0),
      G => \y_reg[23]_i_2__0_n_0\,
      GE => '1',
      Q => Q(0)
    );
\y_reg[10]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => D(10),
      G => \y_reg[23]_i_2__0_n_0\,
      GE => '1',
      Q => Q(10)
    );
\y_reg[11]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => D(11),
      G => \y_reg[23]_i_2__0_n_0\,
      GE => '1',
      Q => Q(11)
    );
\y_reg[12]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => D(12),
      G => \y_reg[23]_i_2__0_n_0\,
      GE => '1',
      Q => Q(12)
    );
\y_reg[13]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => D(13),
      G => \y_reg[23]_i_2__0_n_0\,
      GE => '1',
      Q => Q(13)
    );
\y_reg[14]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => D(14),
      G => \y_reg[23]_i_2__0_n_0\,
      GE => '1',
      Q => Q(14)
    );
\y_reg[15]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => D(15),
      G => \y_reg[23]_i_2__0_n_0\,
      GE => '1',
      Q => Q(15)
    );
\y_reg[16]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => D(16),
      G => \y_reg[23]_i_2__0_n_0\,
      GE => '1',
      Q => Q(16)
    );
\y_reg[17]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => D(17),
      G => \y_reg[23]_i_2__0_n_0\,
      GE => '1',
      Q => Q(17)
    );
\y_reg[18]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => D(18),
      G => \y_reg[23]_i_2__0_n_0\,
      GE => '1',
      Q => Q(18)
    );
\y_reg[19]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => D(19),
      G => \y_reg[23]_i_2__0_n_0\,
      GE => '1',
      Q => Q(19)
    );
\y_reg[1]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => D(1),
      G => \y_reg[23]_i_2__0_n_0\,
      GE => '1',
      Q => Q(1)
    );
\y_reg[20]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => D(20),
      G => \y_reg[23]_i_2__0_n_0\,
      GE => '1',
      Q => Q(20)
    );
\y_reg[21]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => D(21),
      G => \y_reg[23]_i_2__0_n_0\,
      GE => '1',
      Q => Q(21)
    );
\y_reg[22]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => D(22),
      G => \y_reg[23]_i_2__0_n_0\,
      GE => '1',
      Q => Q(22)
    );
\y_reg[23]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => D(23),
      G => \y_reg[23]_i_2__0_n_0\,
      GE => '1',
      Q => Q(23)
    );
\y_reg[23]_i_2__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"55555557"
    )
        port map (
      I0 => ctrl_mux_out_2(4),
      I1 => ctrl_mux_out_2(1),
      I2 => ctrl_mux_out_2(0),
      I3 => ctrl_mux_out_2(2),
      I4 => ctrl_mux_out_2(3),
      O => \y_reg[23]_i_2__0_n_0\
    );
\y_reg[2]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => D(2),
      G => \y_reg[23]_i_2__0_n_0\,
      GE => '1',
      Q => Q(2)
    );
\y_reg[3]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => D(3),
      G => \y_reg[23]_i_2__0_n_0\,
      GE => '1',
      Q => Q(3)
    );
\y_reg[4]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => D(4),
      G => \y_reg[23]_i_2__0_n_0\,
      GE => '1',
      Q => Q(4)
    );
\y_reg[5]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => D(5),
      G => \y_reg[23]_i_2__0_n_0\,
      GE => '1',
      Q => Q(5)
    );
\y_reg[6]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => D(6),
      G => \y_reg[23]_i_2__0_n_0\,
      GE => '1',
      Q => Q(6)
    );
\y_reg[7]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => D(7),
      G => \y_reg[23]_i_2__0_n_0\,
      GE => '1',
      Q => Q(7)
    );
\y_reg[8]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => D(8),
      G => \y_reg[23]_i_2__0_n_0\,
      GE => '1',
      Q => Q(8)
    );
\y_reg[9]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => D(9),
      G => \y_reg[23]_i_2__0_n_0\,
      GE => '1',
      Q => Q(9)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity test_bd_pixelReOrder_0_0_multiplexer_31 is
  port (
    m00_axis_tdata : out STD_LOGIC_VECTOR ( 23 downto 0 );
    ctrl_mux_out_1 : in STD_LOGIC_VECTOR ( 4 downto 0 );
    s00_axis_tuser : in STD_LOGIC_VECTOR ( 0 to 0 );
    Q : in STD_LOGIC_VECTOR ( 23 downto 0 );
    D : in STD_LOGIC_VECTOR ( 23 downto 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of test_bd_pixelReOrder_0_0_multiplexer_31 : entity is "multiplexer";
end test_bd_pixelReOrder_0_0_multiplexer_31;

architecture STRUCTURE of test_bd_pixelReOrder_0_0_multiplexer_31 is
  signal out_mux_1 : STD_LOGIC_VECTOR ( 23 downto 0 );
  signal \y_reg[23]_i_2_n_0\ : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \m00_axis_tdata[0]_INST_0\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \m00_axis_tdata[10]_INST_0\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \m00_axis_tdata[11]_INST_0\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \m00_axis_tdata[12]_INST_0\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \m00_axis_tdata[13]_INST_0\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \m00_axis_tdata[14]_INST_0\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \m00_axis_tdata[15]_INST_0\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \m00_axis_tdata[16]_INST_0\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \m00_axis_tdata[17]_INST_0\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \m00_axis_tdata[18]_INST_0\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \m00_axis_tdata[19]_INST_0\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \m00_axis_tdata[1]_INST_0\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \m00_axis_tdata[20]_INST_0\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \m00_axis_tdata[21]_INST_0\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \m00_axis_tdata[22]_INST_0\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \m00_axis_tdata[23]_INST_0\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \m00_axis_tdata[2]_INST_0\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \m00_axis_tdata[3]_INST_0\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \m00_axis_tdata[4]_INST_0\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \m00_axis_tdata[5]_INST_0\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \m00_axis_tdata[6]_INST_0\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \m00_axis_tdata[7]_INST_0\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \m00_axis_tdata[8]_INST_0\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \m00_axis_tdata[9]_INST_0\ : label is "soft_lutpair7";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of \y_reg[0]\ : label is "LD";
  attribute XILINX_LEGACY_PRIM of \y_reg[10]\ : label is "LD";
  attribute XILINX_LEGACY_PRIM of \y_reg[11]\ : label is "LD";
  attribute XILINX_LEGACY_PRIM of \y_reg[12]\ : label is "LD";
  attribute XILINX_LEGACY_PRIM of \y_reg[13]\ : label is "LD";
  attribute XILINX_LEGACY_PRIM of \y_reg[14]\ : label is "LD";
  attribute XILINX_LEGACY_PRIM of \y_reg[15]\ : label is "LD";
  attribute XILINX_LEGACY_PRIM of \y_reg[16]\ : label is "LD";
  attribute XILINX_LEGACY_PRIM of \y_reg[17]\ : label is "LD";
  attribute XILINX_LEGACY_PRIM of \y_reg[18]\ : label is "LD";
  attribute XILINX_LEGACY_PRIM of \y_reg[19]\ : label is "LD";
  attribute XILINX_LEGACY_PRIM of \y_reg[1]\ : label is "LD";
  attribute XILINX_LEGACY_PRIM of \y_reg[20]\ : label is "LD";
  attribute XILINX_LEGACY_PRIM of \y_reg[21]\ : label is "LD";
  attribute XILINX_LEGACY_PRIM of \y_reg[22]\ : label is "LD";
  attribute XILINX_LEGACY_PRIM of \y_reg[23]\ : label is "LD";
  attribute XILINX_LEGACY_PRIM of \y_reg[2]\ : label is "LD";
  attribute XILINX_LEGACY_PRIM of \y_reg[3]\ : label is "LD";
  attribute XILINX_LEGACY_PRIM of \y_reg[4]\ : label is "LD";
  attribute XILINX_LEGACY_PRIM of \y_reg[5]\ : label is "LD";
  attribute XILINX_LEGACY_PRIM of \y_reg[6]\ : label is "LD";
  attribute XILINX_LEGACY_PRIM of \y_reg[7]\ : label is "LD";
  attribute XILINX_LEGACY_PRIM of \y_reg[8]\ : label is "LD";
  attribute XILINX_LEGACY_PRIM of \y_reg[9]\ : label is "LD";
begin
\m00_axis_tdata[0]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => out_mux_1(0),
      I1 => s00_axis_tuser(0),
      I2 => Q(0),
      O => m00_axis_tdata(0)
    );
\m00_axis_tdata[10]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => out_mux_1(10),
      I1 => s00_axis_tuser(0),
      I2 => Q(10),
      O => m00_axis_tdata(10)
    );
\m00_axis_tdata[11]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => out_mux_1(11),
      I1 => s00_axis_tuser(0),
      I2 => Q(11),
      O => m00_axis_tdata(11)
    );
\m00_axis_tdata[12]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => out_mux_1(12),
      I1 => s00_axis_tuser(0),
      I2 => Q(12),
      O => m00_axis_tdata(12)
    );
\m00_axis_tdata[13]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => out_mux_1(13),
      I1 => s00_axis_tuser(0),
      I2 => Q(13),
      O => m00_axis_tdata(13)
    );
\m00_axis_tdata[14]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => out_mux_1(14),
      I1 => s00_axis_tuser(0),
      I2 => Q(14),
      O => m00_axis_tdata(14)
    );
\m00_axis_tdata[15]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => out_mux_1(15),
      I1 => s00_axis_tuser(0),
      I2 => Q(15),
      O => m00_axis_tdata(15)
    );
\m00_axis_tdata[16]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => out_mux_1(16),
      I1 => s00_axis_tuser(0),
      I2 => Q(16),
      O => m00_axis_tdata(16)
    );
\m00_axis_tdata[17]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => out_mux_1(17),
      I1 => s00_axis_tuser(0),
      I2 => Q(17),
      O => m00_axis_tdata(17)
    );
\m00_axis_tdata[18]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => out_mux_1(18),
      I1 => s00_axis_tuser(0),
      I2 => Q(18),
      O => m00_axis_tdata(18)
    );
\m00_axis_tdata[19]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => out_mux_1(19),
      I1 => s00_axis_tuser(0),
      I2 => Q(19),
      O => m00_axis_tdata(19)
    );
\m00_axis_tdata[1]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => out_mux_1(1),
      I1 => s00_axis_tuser(0),
      I2 => Q(1),
      O => m00_axis_tdata(1)
    );
\m00_axis_tdata[20]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => out_mux_1(20),
      I1 => s00_axis_tuser(0),
      I2 => Q(20),
      O => m00_axis_tdata(20)
    );
\m00_axis_tdata[21]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => out_mux_1(21),
      I1 => s00_axis_tuser(0),
      I2 => Q(21),
      O => m00_axis_tdata(21)
    );
\m00_axis_tdata[22]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => out_mux_1(22),
      I1 => s00_axis_tuser(0),
      I2 => Q(22),
      O => m00_axis_tdata(22)
    );
\m00_axis_tdata[23]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => out_mux_1(23),
      I1 => s00_axis_tuser(0),
      I2 => Q(23),
      O => m00_axis_tdata(23)
    );
\m00_axis_tdata[2]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => out_mux_1(2),
      I1 => s00_axis_tuser(0),
      I2 => Q(2),
      O => m00_axis_tdata(2)
    );
\m00_axis_tdata[3]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => out_mux_1(3),
      I1 => s00_axis_tuser(0),
      I2 => Q(3),
      O => m00_axis_tdata(3)
    );
\m00_axis_tdata[4]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => out_mux_1(4),
      I1 => s00_axis_tuser(0),
      I2 => Q(4),
      O => m00_axis_tdata(4)
    );
\m00_axis_tdata[5]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => out_mux_1(5),
      I1 => s00_axis_tuser(0),
      I2 => Q(5),
      O => m00_axis_tdata(5)
    );
\m00_axis_tdata[6]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => out_mux_1(6),
      I1 => s00_axis_tuser(0),
      I2 => Q(6),
      O => m00_axis_tdata(6)
    );
\m00_axis_tdata[7]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => out_mux_1(7),
      I1 => s00_axis_tuser(0),
      I2 => Q(7),
      O => m00_axis_tdata(7)
    );
\m00_axis_tdata[8]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => out_mux_1(8),
      I1 => s00_axis_tuser(0),
      I2 => Q(8),
      O => m00_axis_tdata(8)
    );
\m00_axis_tdata[9]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => out_mux_1(9),
      I1 => s00_axis_tuser(0),
      I2 => Q(9),
      O => m00_axis_tdata(9)
    );
\y_reg[0]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => D(0),
      G => \y_reg[23]_i_2_n_0\,
      GE => '1',
      Q => out_mux_1(0)
    );
\y_reg[10]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => D(10),
      G => \y_reg[23]_i_2_n_0\,
      GE => '1',
      Q => out_mux_1(10)
    );
\y_reg[11]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => D(11),
      G => \y_reg[23]_i_2_n_0\,
      GE => '1',
      Q => out_mux_1(11)
    );
\y_reg[12]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => D(12),
      G => \y_reg[23]_i_2_n_0\,
      GE => '1',
      Q => out_mux_1(12)
    );
\y_reg[13]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => D(13),
      G => \y_reg[23]_i_2_n_0\,
      GE => '1',
      Q => out_mux_1(13)
    );
\y_reg[14]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => D(14),
      G => \y_reg[23]_i_2_n_0\,
      GE => '1',
      Q => out_mux_1(14)
    );
\y_reg[15]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => D(15),
      G => \y_reg[23]_i_2_n_0\,
      GE => '1',
      Q => out_mux_1(15)
    );
\y_reg[16]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => D(16),
      G => \y_reg[23]_i_2_n_0\,
      GE => '1',
      Q => out_mux_1(16)
    );
\y_reg[17]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => D(17),
      G => \y_reg[23]_i_2_n_0\,
      GE => '1',
      Q => out_mux_1(17)
    );
\y_reg[18]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => D(18),
      G => \y_reg[23]_i_2_n_0\,
      GE => '1',
      Q => out_mux_1(18)
    );
\y_reg[19]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => D(19),
      G => \y_reg[23]_i_2_n_0\,
      GE => '1',
      Q => out_mux_1(19)
    );
\y_reg[1]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => D(1),
      G => \y_reg[23]_i_2_n_0\,
      GE => '1',
      Q => out_mux_1(1)
    );
\y_reg[20]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => D(20),
      G => \y_reg[23]_i_2_n_0\,
      GE => '1',
      Q => out_mux_1(20)
    );
\y_reg[21]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => D(21),
      G => \y_reg[23]_i_2_n_0\,
      GE => '1',
      Q => out_mux_1(21)
    );
\y_reg[22]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => D(22),
      G => \y_reg[23]_i_2_n_0\,
      GE => '1',
      Q => out_mux_1(22)
    );
\y_reg[23]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => D(23),
      G => \y_reg[23]_i_2_n_0\,
      GE => '1',
      Q => out_mux_1(23)
    );
\y_reg[23]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"55555557"
    )
        port map (
      I0 => ctrl_mux_out_1(4),
      I1 => ctrl_mux_out_1(1),
      I2 => ctrl_mux_out_1(0),
      I3 => ctrl_mux_out_1(2),
      I4 => ctrl_mux_out_1(3),
      O => \y_reg[23]_i_2_n_0\
    );
\y_reg[2]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => D(2),
      G => \y_reg[23]_i_2_n_0\,
      GE => '1',
      Q => out_mux_1(2)
    );
\y_reg[3]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => D(3),
      G => \y_reg[23]_i_2_n_0\,
      GE => '1',
      Q => out_mux_1(3)
    );
\y_reg[4]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => D(4),
      G => \y_reg[23]_i_2_n_0\,
      GE => '1',
      Q => out_mux_1(4)
    );
\y_reg[5]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => D(5),
      G => \y_reg[23]_i_2_n_0\,
      GE => '1',
      Q => out_mux_1(5)
    );
\y_reg[6]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => D(6),
      G => \y_reg[23]_i_2_n_0\,
      GE => '1',
      Q => out_mux_1(6)
    );
\y_reg[7]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => D(7),
      G => \y_reg[23]_i_2_n_0\,
      GE => '1',
      Q => out_mux_1(7)
    );
\y_reg[8]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => D(8),
      G => \y_reg[23]_i_2_n_0\,
      GE => '1',
      Q => out_mux_1(8)
    );
\y_reg[9]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => D(9),
      G => \y_reg[23]_i_2_n_0\,
      GE => '1',
      Q => out_mux_1(9)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity test_bd_pixelReOrder_0_0_rams_sp_rf_rst is
  port (
    dout0_out : out STD_LOGIC_VECTOR ( 23 downto 0 );
    m00_axis_aclk : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 0 to 0 );
    addr : in STD_LOGIC_VECTOR ( 10 downto 0 );
    s00_axis_tdata : in STD_LOGIC_VECTOR ( 23 downto 0 );
    s00_axis_tuser : in STD_LOGIC_VECTOR ( 0 to 0 );
    s00_axis_tvalid : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of test_bd_pixelReOrder_0_0_rams_sp_rf_rst : entity is "rams_sp_rf_rst";
end test_bd_pixelReOrder_0_0_rams_sp_rf_rst;

architecture STRUCTURE of test_bd_pixelReOrder_0_0_rams_sp_rf_rst is
  signal ram_reg_0_i_12_n_0 : STD_LOGIC;
  signal NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_DBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_SBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_DOADO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 16 );
  signal NLW_ram_reg_0_DOBDO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_ram_reg_0_DOPADOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal NLW_ram_reg_0_DOPBDOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_ram_reg_0_ECCPARITY_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal NLW_ram_reg_0_RDADDRECC_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_ram_reg_1_DOADO_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 6 );
  signal NLW_ram_reg_1_DOBDO_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal NLW_ram_reg_1_DOPADOP_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_ram_reg_1_DOPBDOP_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ : string;
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of ram_reg_0 : label is "p2_d16";
  attribute METHODOLOGY_DRC_VIOS : string;
  attribute METHODOLOGY_DRC_VIOS of ram_reg_0 : label is "{SYNTH-6 {cell *THIS*}}";
  attribute RTL_RAM_BITS : integer;
  attribute RTL_RAM_BITS of ram_reg_0 : label is 46080;
  attribute RTL_RAM_NAME : string;
  attribute RTL_RAM_NAME of ram_reg_0 : label is "ram";
  attribute bram_addr_begin : integer;
  attribute bram_addr_begin of ram_reg_0 : label is 0;
  attribute bram_addr_end : integer;
  attribute bram_addr_end of ram_reg_0 : label is 2047;
  attribute bram_slice_begin : integer;
  attribute bram_slice_begin of ram_reg_0 : label is 0;
  attribute bram_slice_end : integer;
  attribute bram_slice_end of ram_reg_0 : label is 17;
  attribute ram_addr_begin : integer;
  attribute ram_addr_begin of ram_reg_0 : label is 0;
  attribute ram_addr_end : integer;
  attribute ram_addr_end of ram_reg_0 : label is 2047;
  attribute ram_offset : integer;
  attribute ram_offset of ram_reg_0 : label is 0;
  attribute ram_slice_begin : integer;
  attribute ram_slice_begin of ram_reg_0 : label is 0;
  attribute ram_slice_end : integer;
  attribute ram_slice_end of ram_reg_0 : label is 17;
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of ram_reg_1 : label is "p0_d6";
  attribute METHODOLOGY_DRC_VIOS of ram_reg_1 : label is "{SYNTH-6 {cell *THIS*}}";
  attribute RTL_RAM_BITS of ram_reg_1 : label is 46080;
  attribute RTL_RAM_NAME of ram_reg_1 : label is "ram";
  attribute bram_addr_begin of ram_reg_1 : label is 0;
  attribute bram_addr_end of ram_reg_1 : label is 2047;
  attribute bram_slice_begin of ram_reg_1 : label is 18;
  attribute bram_slice_end of ram_reg_1 : label is 23;
  attribute ram_addr_begin of ram_reg_1 : label is 0;
  attribute ram_addr_end of ram_reg_1 : label is 2047;
  attribute ram_offset of ram_reg_1 : label is 0;
  attribute ram_slice_begin of ram_reg_1 : label is 18;
  attribute ram_slice_end of ram_reg_1 : label is 23;
begin
ram_reg_0: unisim.vcomponents.RAMB36E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      EN_ECC_READ => false,
      EN_ECC_WRITE => false,
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      RAM_EXTENSION_A => "NONE",
      RAM_EXTENSION_B => "NONE",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "PERFORMANCE",
      READ_WIDTH_A => 18,
      READ_WIDTH_B => 0,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "READ_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 18,
      WRITE_WIDTH_B => 0
    )
        port map (
      ADDRARDADDR(15) => '1',
      ADDRARDADDR(14 downto 4) => addr(10 downto 0),
      ADDRARDADDR(3 downto 0) => B"1111",
      ADDRBWRADDR(15 downto 0) => B"1111111111111111",
      CASCADEINA => '1',
      CASCADEINB => '0',
      CASCADEOUTA => NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED,
      CASCADEOUTB => NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED,
      CLKARDCLK => m00_axis_aclk,
      CLKBWRCLK => '0',
      DBITERR => NLW_ram_reg_0_DBITERR_UNCONNECTED,
      DIADI(31 downto 16) => B"0000000000000000",
      DIADI(15 downto 0) => s00_axis_tdata(15 downto 0),
      DIBDI(31 downto 0) => B"11111111111111111111111111111111",
      DIPADIP(3 downto 2) => B"00",
      DIPADIP(1 downto 0) => s00_axis_tdata(17 downto 16),
      DIPBDIP(3 downto 0) => B"1111",
      DOADO(31 downto 16) => NLW_ram_reg_0_DOADO_UNCONNECTED(31 downto 16),
      DOADO(15 downto 0) => dout0_out(15 downto 0),
      DOBDO(31 downto 0) => NLW_ram_reg_0_DOBDO_UNCONNECTED(31 downto 0),
      DOPADOP(3 downto 2) => NLW_ram_reg_0_DOPADOP_UNCONNECTED(3 downto 2),
      DOPADOP(1 downto 0) => dout0_out(17 downto 16),
      DOPBDOP(3 downto 0) => NLW_ram_reg_0_DOPBDOP_UNCONNECTED(3 downto 0),
      ECCPARITY(7 downto 0) => NLW_ram_reg_0_ECCPARITY_UNCONNECTED(7 downto 0),
      ENARDEN => Q(0),
      ENBWREN => '0',
      INJECTDBITERR => NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED,
      INJECTSBITERR => NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED,
      RDADDRECC(8 downto 0) => NLW_ram_reg_0_RDADDRECC_UNCONNECTED(8 downto 0),
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      SBITERR => NLW_ram_reg_0_SBITERR_UNCONNECTED,
      WEA(3) => ram_reg_0_i_12_n_0,
      WEA(2) => ram_reg_0_i_12_n_0,
      WEA(1) => ram_reg_0_i_12_n_0,
      WEA(0) => ram_reg_0_i_12_n_0,
      WEBWE(7 downto 0) => B"00000000"
    );
ram_reg_0_i_12: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => Q(0),
      I1 => s00_axis_tuser(0),
      I2 => s00_axis_tvalid,
      O => ram_reg_0_i_12_n_0
    );
ram_reg_1: unisim.vcomponents.RAMB18E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      INIT_A => X"00000",
      INIT_B => X"00000",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "PERFORMANCE",
      READ_WIDTH_A => 9,
      READ_WIDTH_B => 0,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"00000",
      SRVAL_B => X"00000",
      WRITE_MODE_A => "READ_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 9,
      WRITE_WIDTH_B => 0
    )
        port map (
      ADDRARDADDR(13 downto 3) => addr(10 downto 0),
      ADDRARDADDR(2 downto 0) => B"111",
      ADDRBWRADDR(13 downto 0) => B"11111111111111",
      CLKARDCLK => m00_axis_aclk,
      CLKBWRCLK => '0',
      DIADI(15 downto 6) => B"0000000000",
      DIADI(5 downto 0) => s00_axis_tdata(23 downto 18),
      DIBDI(15 downto 0) => B"1111111111111111",
      DIPADIP(1 downto 0) => B"00",
      DIPBDIP(1 downto 0) => B"11",
      DOADO(15 downto 6) => NLW_ram_reg_1_DOADO_UNCONNECTED(15 downto 6),
      DOADO(5 downto 0) => dout0_out(23 downto 18),
      DOBDO(15 downto 0) => NLW_ram_reg_1_DOBDO_UNCONNECTED(15 downto 0),
      DOPADOP(1 downto 0) => NLW_ram_reg_1_DOPADOP_UNCONNECTED(1 downto 0),
      DOPBDOP(1 downto 0) => NLW_ram_reg_1_DOPBDOP_UNCONNECTED(1 downto 0),
      ENARDEN => Q(0),
      ENBWREN => '0',
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      WEA(1) => ram_reg_0_i_12_n_0,
      WEA(0) => ram_reg_0_i_12_n_0,
      WEBWE(3 downto 0) => B"0000"
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity test_bd_pixelReOrder_0_0_rams_sp_rf_rst_0 is
  port (
    dout0_out : out STD_LOGIC_VECTOR ( 23 downto 0 );
    m00_axis_aclk : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 0 to 0 );
    addr : in STD_LOGIC_VECTOR ( 10 downto 0 );
    s00_axis_tdata : in STD_LOGIC_VECTOR ( 23 downto 0 );
    s00_axis_tuser : in STD_LOGIC_VECTOR ( 0 to 0 );
    s00_axis_tvalid : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of test_bd_pixelReOrder_0_0_rams_sp_rf_rst_0 : entity is "rams_sp_rf_rst";
end test_bd_pixelReOrder_0_0_rams_sp_rf_rst_0;

architecture STRUCTURE of test_bd_pixelReOrder_0_0_rams_sp_rf_rst_0 is
  signal \ram_reg_0_i_12__0_n_0\ : STD_LOGIC;
  signal NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_DBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_SBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_DOADO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 16 );
  signal NLW_ram_reg_0_DOBDO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_ram_reg_0_DOPADOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal NLW_ram_reg_0_DOPBDOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_ram_reg_0_ECCPARITY_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal NLW_ram_reg_0_RDADDRECC_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_ram_reg_1_DOADO_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 6 );
  signal NLW_ram_reg_1_DOBDO_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal NLW_ram_reg_1_DOPADOP_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_ram_reg_1_DOPBDOP_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ : string;
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of ram_reg_0 : label is "p2_d16";
  attribute METHODOLOGY_DRC_VIOS : string;
  attribute METHODOLOGY_DRC_VIOS of ram_reg_0 : label is "{SYNTH-6 {cell *THIS*}}";
  attribute RTL_RAM_BITS : integer;
  attribute RTL_RAM_BITS of ram_reg_0 : label is 46080;
  attribute RTL_RAM_NAME : string;
  attribute RTL_RAM_NAME of ram_reg_0 : label is "ram";
  attribute bram_addr_begin : integer;
  attribute bram_addr_begin of ram_reg_0 : label is 0;
  attribute bram_addr_end : integer;
  attribute bram_addr_end of ram_reg_0 : label is 2047;
  attribute bram_slice_begin : integer;
  attribute bram_slice_begin of ram_reg_0 : label is 0;
  attribute bram_slice_end : integer;
  attribute bram_slice_end of ram_reg_0 : label is 17;
  attribute ram_addr_begin : integer;
  attribute ram_addr_begin of ram_reg_0 : label is 0;
  attribute ram_addr_end : integer;
  attribute ram_addr_end of ram_reg_0 : label is 2047;
  attribute ram_offset : integer;
  attribute ram_offset of ram_reg_0 : label is 0;
  attribute ram_slice_begin : integer;
  attribute ram_slice_begin of ram_reg_0 : label is 0;
  attribute ram_slice_end : integer;
  attribute ram_slice_end of ram_reg_0 : label is 17;
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of ram_reg_1 : label is "p0_d6";
  attribute METHODOLOGY_DRC_VIOS of ram_reg_1 : label is "{SYNTH-6 {cell *THIS*}}";
  attribute RTL_RAM_BITS of ram_reg_1 : label is 46080;
  attribute RTL_RAM_NAME of ram_reg_1 : label is "ram";
  attribute bram_addr_begin of ram_reg_1 : label is 0;
  attribute bram_addr_end of ram_reg_1 : label is 2047;
  attribute bram_slice_begin of ram_reg_1 : label is 18;
  attribute bram_slice_end of ram_reg_1 : label is 23;
  attribute ram_addr_begin of ram_reg_1 : label is 0;
  attribute ram_addr_end of ram_reg_1 : label is 2047;
  attribute ram_offset of ram_reg_1 : label is 0;
  attribute ram_slice_begin of ram_reg_1 : label is 18;
  attribute ram_slice_end of ram_reg_1 : label is 23;
begin
ram_reg_0: unisim.vcomponents.RAMB36E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      EN_ECC_READ => false,
      EN_ECC_WRITE => false,
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      RAM_EXTENSION_A => "NONE",
      RAM_EXTENSION_B => "NONE",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "PERFORMANCE",
      READ_WIDTH_A => 18,
      READ_WIDTH_B => 0,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "READ_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 18,
      WRITE_WIDTH_B => 0
    )
        port map (
      ADDRARDADDR(15) => '1',
      ADDRARDADDR(14 downto 4) => addr(10 downto 0),
      ADDRARDADDR(3 downto 0) => B"1111",
      ADDRBWRADDR(15 downto 0) => B"1111111111111111",
      CASCADEINA => '1',
      CASCADEINB => '0',
      CASCADEOUTA => NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED,
      CASCADEOUTB => NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED,
      CLKARDCLK => m00_axis_aclk,
      CLKBWRCLK => '0',
      DBITERR => NLW_ram_reg_0_DBITERR_UNCONNECTED,
      DIADI(31 downto 16) => B"0000000000000000",
      DIADI(15 downto 0) => s00_axis_tdata(15 downto 0),
      DIBDI(31 downto 0) => B"11111111111111111111111111111111",
      DIPADIP(3 downto 2) => B"00",
      DIPADIP(1 downto 0) => s00_axis_tdata(17 downto 16),
      DIPBDIP(3 downto 0) => B"1111",
      DOADO(31 downto 16) => NLW_ram_reg_0_DOADO_UNCONNECTED(31 downto 16),
      DOADO(15 downto 0) => dout0_out(15 downto 0),
      DOBDO(31 downto 0) => NLW_ram_reg_0_DOBDO_UNCONNECTED(31 downto 0),
      DOPADOP(3 downto 2) => NLW_ram_reg_0_DOPADOP_UNCONNECTED(3 downto 2),
      DOPADOP(1 downto 0) => dout0_out(17 downto 16),
      DOPBDOP(3 downto 0) => NLW_ram_reg_0_DOPBDOP_UNCONNECTED(3 downto 0),
      ECCPARITY(7 downto 0) => NLW_ram_reg_0_ECCPARITY_UNCONNECTED(7 downto 0),
      ENARDEN => Q(0),
      ENBWREN => '0',
      INJECTDBITERR => NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED,
      INJECTSBITERR => NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED,
      RDADDRECC(8 downto 0) => NLW_ram_reg_0_RDADDRECC_UNCONNECTED(8 downto 0),
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      SBITERR => NLW_ram_reg_0_SBITERR_UNCONNECTED,
      WEA(3) => \ram_reg_0_i_12__0_n_0\,
      WEA(2) => \ram_reg_0_i_12__0_n_0\,
      WEA(1) => \ram_reg_0_i_12__0_n_0\,
      WEA(0) => \ram_reg_0_i_12__0_n_0\,
      WEBWE(7 downto 0) => B"00000000"
    );
\ram_reg_0_i_12__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => Q(0),
      I1 => s00_axis_tuser(0),
      I2 => s00_axis_tvalid,
      O => \ram_reg_0_i_12__0_n_0\
    );
ram_reg_1: unisim.vcomponents.RAMB18E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      INIT_A => X"00000",
      INIT_B => X"00000",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "PERFORMANCE",
      READ_WIDTH_A => 9,
      READ_WIDTH_B => 0,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"00000",
      SRVAL_B => X"00000",
      WRITE_MODE_A => "READ_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 9,
      WRITE_WIDTH_B => 0
    )
        port map (
      ADDRARDADDR(13 downto 3) => addr(10 downto 0),
      ADDRARDADDR(2 downto 0) => B"111",
      ADDRBWRADDR(13 downto 0) => B"11111111111111",
      CLKARDCLK => m00_axis_aclk,
      CLKBWRCLK => '0',
      DIADI(15 downto 6) => B"0000000000",
      DIADI(5 downto 0) => s00_axis_tdata(23 downto 18),
      DIBDI(15 downto 0) => B"1111111111111111",
      DIPADIP(1 downto 0) => B"00",
      DIPBDIP(1 downto 0) => B"11",
      DOADO(15 downto 6) => NLW_ram_reg_1_DOADO_UNCONNECTED(15 downto 6),
      DOADO(5 downto 0) => dout0_out(23 downto 18),
      DOBDO(15 downto 0) => NLW_ram_reg_1_DOBDO_UNCONNECTED(15 downto 0),
      DOPADOP(1 downto 0) => NLW_ram_reg_1_DOPADOP_UNCONNECTED(1 downto 0),
      DOPBDOP(1 downto 0) => NLW_ram_reg_1_DOPBDOP_UNCONNECTED(1 downto 0),
      ENARDEN => Q(0),
      ENBWREN => '0',
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      WEA(1) => \ram_reg_0_i_12__0_n_0\,
      WEA(0) => \ram_reg_0_i_12__0_n_0\,
      WEBWE(3 downto 0) => B"0000"
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity test_bd_pixelReOrder_0_0_rams_sp_rf_rst_1 is
  port (
    dout0_out : out STD_LOGIC_VECTOR ( 23 downto 0 );
    m00_axis_aclk : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 0 to 0 );
    addr : in STD_LOGIC_VECTOR ( 10 downto 0 );
    s00_axis_tdata : in STD_LOGIC_VECTOR ( 23 downto 0 );
    s00_axis_tuser : in STD_LOGIC_VECTOR ( 0 to 0 );
    s00_axis_tvalid : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of test_bd_pixelReOrder_0_0_rams_sp_rf_rst_1 : entity is "rams_sp_rf_rst";
end test_bd_pixelReOrder_0_0_rams_sp_rf_rst_1;

architecture STRUCTURE of test_bd_pixelReOrder_0_0_rams_sp_rf_rst_1 is
  signal \ram_reg_0_i_1__17_n_0\ : STD_LOGIC;
  signal NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_DBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_SBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_DOADO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 16 );
  signal NLW_ram_reg_0_DOBDO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_ram_reg_0_DOPADOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal NLW_ram_reg_0_DOPBDOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_ram_reg_0_ECCPARITY_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal NLW_ram_reg_0_RDADDRECC_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_ram_reg_1_DOADO_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 6 );
  signal NLW_ram_reg_1_DOBDO_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal NLW_ram_reg_1_DOPADOP_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_ram_reg_1_DOPBDOP_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ : string;
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of ram_reg_0 : label is "p2_d16";
  attribute METHODOLOGY_DRC_VIOS : string;
  attribute METHODOLOGY_DRC_VIOS of ram_reg_0 : label is "{SYNTH-6 {cell *THIS*}}";
  attribute RTL_RAM_BITS : integer;
  attribute RTL_RAM_BITS of ram_reg_0 : label is 46080;
  attribute RTL_RAM_NAME : string;
  attribute RTL_RAM_NAME of ram_reg_0 : label is "ram";
  attribute bram_addr_begin : integer;
  attribute bram_addr_begin of ram_reg_0 : label is 0;
  attribute bram_addr_end : integer;
  attribute bram_addr_end of ram_reg_0 : label is 2047;
  attribute bram_slice_begin : integer;
  attribute bram_slice_begin of ram_reg_0 : label is 0;
  attribute bram_slice_end : integer;
  attribute bram_slice_end of ram_reg_0 : label is 17;
  attribute ram_addr_begin : integer;
  attribute ram_addr_begin of ram_reg_0 : label is 0;
  attribute ram_addr_end : integer;
  attribute ram_addr_end of ram_reg_0 : label is 2047;
  attribute ram_offset : integer;
  attribute ram_offset of ram_reg_0 : label is 0;
  attribute ram_slice_begin : integer;
  attribute ram_slice_begin of ram_reg_0 : label is 0;
  attribute ram_slice_end : integer;
  attribute ram_slice_end of ram_reg_0 : label is 17;
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of ram_reg_1 : label is "p0_d6";
  attribute METHODOLOGY_DRC_VIOS of ram_reg_1 : label is "{SYNTH-6 {cell *THIS*}}";
  attribute RTL_RAM_BITS of ram_reg_1 : label is 46080;
  attribute RTL_RAM_NAME of ram_reg_1 : label is "ram";
  attribute bram_addr_begin of ram_reg_1 : label is 0;
  attribute bram_addr_end of ram_reg_1 : label is 2047;
  attribute bram_slice_begin of ram_reg_1 : label is 18;
  attribute bram_slice_end of ram_reg_1 : label is 23;
  attribute ram_addr_begin of ram_reg_1 : label is 0;
  attribute ram_addr_end of ram_reg_1 : label is 2047;
  attribute ram_offset of ram_reg_1 : label is 0;
  attribute ram_slice_begin of ram_reg_1 : label is 18;
  attribute ram_slice_end of ram_reg_1 : label is 23;
begin
ram_reg_0: unisim.vcomponents.RAMB36E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      EN_ECC_READ => false,
      EN_ECC_WRITE => false,
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      RAM_EXTENSION_A => "NONE",
      RAM_EXTENSION_B => "NONE",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "PERFORMANCE",
      READ_WIDTH_A => 18,
      READ_WIDTH_B => 0,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "READ_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 18,
      WRITE_WIDTH_B => 0
    )
        port map (
      ADDRARDADDR(15) => '1',
      ADDRARDADDR(14 downto 4) => addr(10 downto 0),
      ADDRARDADDR(3 downto 0) => B"1111",
      ADDRBWRADDR(15 downto 0) => B"1111111111111111",
      CASCADEINA => '1',
      CASCADEINB => '0',
      CASCADEOUTA => NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED,
      CASCADEOUTB => NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED,
      CLKARDCLK => m00_axis_aclk,
      CLKBWRCLK => '0',
      DBITERR => NLW_ram_reg_0_DBITERR_UNCONNECTED,
      DIADI(31 downto 16) => B"0000000000000000",
      DIADI(15 downto 0) => s00_axis_tdata(15 downto 0),
      DIBDI(31 downto 0) => B"11111111111111111111111111111111",
      DIPADIP(3 downto 2) => B"00",
      DIPADIP(1 downto 0) => s00_axis_tdata(17 downto 16),
      DIPBDIP(3 downto 0) => B"1111",
      DOADO(31 downto 16) => NLW_ram_reg_0_DOADO_UNCONNECTED(31 downto 16),
      DOADO(15 downto 0) => dout0_out(15 downto 0),
      DOBDO(31 downto 0) => NLW_ram_reg_0_DOBDO_UNCONNECTED(31 downto 0),
      DOPADOP(3 downto 2) => NLW_ram_reg_0_DOPADOP_UNCONNECTED(3 downto 2),
      DOPADOP(1 downto 0) => dout0_out(17 downto 16),
      DOPBDOP(3 downto 0) => NLW_ram_reg_0_DOPBDOP_UNCONNECTED(3 downto 0),
      ECCPARITY(7 downto 0) => NLW_ram_reg_0_ECCPARITY_UNCONNECTED(7 downto 0),
      ENARDEN => Q(0),
      ENBWREN => '0',
      INJECTDBITERR => NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED,
      INJECTSBITERR => NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED,
      RDADDRECC(8 downto 0) => NLW_ram_reg_0_RDADDRECC_UNCONNECTED(8 downto 0),
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      SBITERR => NLW_ram_reg_0_SBITERR_UNCONNECTED,
      WEA(3) => \ram_reg_0_i_1__17_n_0\,
      WEA(2) => \ram_reg_0_i_1__17_n_0\,
      WEA(1) => \ram_reg_0_i_1__17_n_0\,
      WEA(0) => \ram_reg_0_i_1__17_n_0\,
      WEBWE(7 downto 0) => B"00000000"
    );
\ram_reg_0_i_1__17\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => Q(0),
      I1 => s00_axis_tuser(0),
      I2 => s00_axis_tvalid,
      O => \ram_reg_0_i_1__17_n_0\
    );
ram_reg_1: unisim.vcomponents.RAMB18E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      INIT_A => X"00000",
      INIT_B => X"00000",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "PERFORMANCE",
      READ_WIDTH_A => 9,
      READ_WIDTH_B => 0,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"00000",
      SRVAL_B => X"00000",
      WRITE_MODE_A => "READ_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 9,
      WRITE_WIDTH_B => 0
    )
        port map (
      ADDRARDADDR(13 downto 3) => addr(10 downto 0),
      ADDRARDADDR(2 downto 0) => B"111",
      ADDRBWRADDR(13 downto 0) => B"11111111111111",
      CLKARDCLK => m00_axis_aclk,
      CLKBWRCLK => '0',
      DIADI(15 downto 6) => B"0000000000",
      DIADI(5 downto 0) => s00_axis_tdata(23 downto 18),
      DIBDI(15 downto 0) => B"1111111111111111",
      DIPADIP(1 downto 0) => B"00",
      DIPBDIP(1 downto 0) => B"11",
      DOADO(15 downto 6) => NLW_ram_reg_1_DOADO_UNCONNECTED(15 downto 6),
      DOADO(5 downto 0) => dout0_out(23 downto 18),
      DOBDO(15 downto 0) => NLW_ram_reg_1_DOBDO_UNCONNECTED(15 downto 0),
      DOPADOP(1 downto 0) => NLW_ram_reg_1_DOPADOP_UNCONNECTED(1 downto 0),
      DOPBDOP(1 downto 0) => NLW_ram_reg_1_DOPBDOP_UNCONNECTED(1 downto 0),
      ENARDEN => Q(0),
      ENBWREN => '0',
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      WEA(1) => \ram_reg_0_i_1__17_n_0\,
      WEA(0) => \ram_reg_0_i_1__17_n_0\,
      WEBWE(3 downto 0) => B"0000"
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity test_bd_pixelReOrder_0_0_rams_sp_rf_rst_10 is
  port (
    dout0_out : out STD_LOGIC_VECTOR ( 23 downto 0 );
    m00_axis_aclk : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 0 to 0 );
    addr : in STD_LOGIC_VECTOR ( 10 downto 0 );
    s00_axis_tdata : in STD_LOGIC_VECTOR ( 23 downto 0 );
    s00_axis_tuser : in STD_LOGIC_VECTOR ( 0 to 0 );
    s00_axis_tvalid : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of test_bd_pixelReOrder_0_0_rams_sp_rf_rst_10 : entity is "rams_sp_rf_rst";
end test_bd_pixelReOrder_0_0_rams_sp_rf_rst_10;

architecture STRUCTURE of test_bd_pixelReOrder_0_0_rams_sp_rf_rst_10 is
  signal \ram_reg_0_i_1__26_n_0\ : STD_LOGIC;
  signal NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_DBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_SBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_DOADO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 16 );
  signal NLW_ram_reg_0_DOBDO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_ram_reg_0_DOPADOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal NLW_ram_reg_0_DOPBDOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_ram_reg_0_ECCPARITY_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal NLW_ram_reg_0_RDADDRECC_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_ram_reg_1_DOADO_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 6 );
  signal NLW_ram_reg_1_DOBDO_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal NLW_ram_reg_1_DOPADOP_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_ram_reg_1_DOPBDOP_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ : string;
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of ram_reg_0 : label is "p2_d16";
  attribute METHODOLOGY_DRC_VIOS : string;
  attribute METHODOLOGY_DRC_VIOS of ram_reg_0 : label is "{SYNTH-6 {cell *THIS*}}";
  attribute RTL_RAM_BITS : integer;
  attribute RTL_RAM_BITS of ram_reg_0 : label is 46080;
  attribute RTL_RAM_NAME : string;
  attribute RTL_RAM_NAME of ram_reg_0 : label is "ram";
  attribute bram_addr_begin : integer;
  attribute bram_addr_begin of ram_reg_0 : label is 0;
  attribute bram_addr_end : integer;
  attribute bram_addr_end of ram_reg_0 : label is 2047;
  attribute bram_slice_begin : integer;
  attribute bram_slice_begin of ram_reg_0 : label is 0;
  attribute bram_slice_end : integer;
  attribute bram_slice_end of ram_reg_0 : label is 17;
  attribute ram_addr_begin : integer;
  attribute ram_addr_begin of ram_reg_0 : label is 0;
  attribute ram_addr_end : integer;
  attribute ram_addr_end of ram_reg_0 : label is 2047;
  attribute ram_offset : integer;
  attribute ram_offset of ram_reg_0 : label is 0;
  attribute ram_slice_begin : integer;
  attribute ram_slice_begin of ram_reg_0 : label is 0;
  attribute ram_slice_end : integer;
  attribute ram_slice_end of ram_reg_0 : label is 17;
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of ram_reg_1 : label is "p0_d6";
  attribute METHODOLOGY_DRC_VIOS of ram_reg_1 : label is "{SYNTH-6 {cell *THIS*}}";
  attribute RTL_RAM_BITS of ram_reg_1 : label is 46080;
  attribute RTL_RAM_NAME of ram_reg_1 : label is "ram";
  attribute bram_addr_begin of ram_reg_1 : label is 0;
  attribute bram_addr_end of ram_reg_1 : label is 2047;
  attribute bram_slice_begin of ram_reg_1 : label is 18;
  attribute bram_slice_end of ram_reg_1 : label is 23;
  attribute ram_addr_begin of ram_reg_1 : label is 0;
  attribute ram_addr_end of ram_reg_1 : label is 2047;
  attribute ram_offset of ram_reg_1 : label is 0;
  attribute ram_slice_begin of ram_reg_1 : label is 18;
  attribute ram_slice_end of ram_reg_1 : label is 23;
begin
ram_reg_0: unisim.vcomponents.RAMB36E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      EN_ECC_READ => false,
      EN_ECC_WRITE => false,
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      RAM_EXTENSION_A => "NONE",
      RAM_EXTENSION_B => "NONE",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "PERFORMANCE",
      READ_WIDTH_A => 18,
      READ_WIDTH_B => 0,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "READ_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 18,
      WRITE_WIDTH_B => 0
    )
        port map (
      ADDRARDADDR(15) => '1',
      ADDRARDADDR(14 downto 4) => addr(10 downto 0),
      ADDRARDADDR(3 downto 0) => B"1111",
      ADDRBWRADDR(15 downto 0) => B"1111111111111111",
      CASCADEINA => '1',
      CASCADEINB => '0',
      CASCADEOUTA => NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED,
      CASCADEOUTB => NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED,
      CLKARDCLK => m00_axis_aclk,
      CLKBWRCLK => '0',
      DBITERR => NLW_ram_reg_0_DBITERR_UNCONNECTED,
      DIADI(31 downto 16) => B"0000000000000000",
      DIADI(15 downto 0) => s00_axis_tdata(15 downto 0),
      DIBDI(31 downto 0) => B"11111111111111111111111111111111",
      DIPADIP(3 downto 2) => B"00",
      DIPADIP(1 downto 0) => s00_axis_tdata(17 downto 16),
      DIPBDIP(3 downto 0) => B"1111",
      DOADO(31 downto 16) => NLW_ram_reg_0_DOADO_UNCONNECTED(31 downto 16),
      DOADO(15 downto 0) => dout0_out(15 downto 0),
      DOBDO(31 downto 0) => NLW_ram_reg_0_DOBDO_UNCONNECTED(31 downto 0),
      DOPADOP(3 downto 2) => NLW_ram_reg_0_DOPADOP_UNCONNECTED(3 downto 2),
      DOPADOP(1 downto 0) => dout0_out(17 downto 16),
      DOPBDOP(3 downto 0) => NLW_ram_reg_0_DOPBDOP_UNCONNECTED(3 downto 0),
      ECCPARITY(7 downto 0) => NLW_ram_reg_0_ECCPARITY_UNCONNECTED(7 downto 0),
      ENARDEN => Q(0),
      ENBWREN => '0',
      INJECTDBITERR => NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED,
      INJECTSBITERR => NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED,
      RDADDRECC(8 downto 0) => NLW_ram_reg_0_RDADDRECC_UNCONNECTED(8 downto 0),
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      SBITERR => NLW_ram_reg_0_SBITERR_UNCONNECTED,
      WEA(3) => \ram_reg_0_i_1__26_n_0\,
      WEA(2) => \ram_reg_0_i_1__26_n_0\,
      WEA(1) => \ram_reg_0_i_1__26_n_0\,
      WEA(0) => \ram_reg_0_i_1__26_n_0\,
      WEBWE(7 downto 0) => B"00000000"
    );
\ram_reg_0_i_1__26\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => Q(0),
      I1 => s00_axis_tuser(0),
      I2 => s00_axis_tvalid,
      O => \ram_reg_0_i_1__26_n_0\
    );
ram_reg_1: unisim.vcomponents.RAMB18E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      INIT_A => X"00000",
      INIT_B => X"00000",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "PERFORMANCE",
      READ_WIDTH_A => 9,
      READ_WIDTH_B => 0,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"00000",
      SRVAL_B => X"00000",
      WRITE_MODE_A => "READ_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 9,
      WRITE_WIDTH_B => 0
    )
        port map (
      ADDRARDADDR(13 downto 3) => addr(10 downto 0),
      ADDRARDADDR(2 downto 0) => B"111",
      ADDRBWRADDR(13 downto 0) => B"11111111111111",
      CLKARDCLK => m00_axis_aclk,
      CLKBWRCLK => '0',
      DIADI(15 downto 6) => B"0000000000",
      DIADI(5 downto 0) => s00_axis_tdata(23 downto 18),
      DIBDI(15 downto 0) => B"1111111111111111",
      DIPADIP(1 downto 0) => B"00",
      DIPBDIP(1 downto 0) => B"11",
      DOADO(15 downto 6) => NLW_ram_reg_1_DOADO_UNCONNECTED(15 downto 6),
      DOADO(5 downto 0) => dout0_out(23 downto 18),
      DOBDO(15 downto 0) => NLW_ram_reg_1_DOBDO_UNCONNECTED(15 downto 0),
      DOPADOP(1 downto 0) => NLW_ram_reg_1_DOPADOP_UNCONNECTED(1 downto 0),
      DOPBDOP(1 downto 0) => NLW_ram_reg_1_DOPBDOP_UNCONNECTED(1 downto 0),
      ENARDEN => Q(0),
      ENBWREN => '0',
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      WEA(1) => \ram_reg_0_i_1__26_n_0\,
      WEA(0) => \ram_reg_0_i_1__26_n_0\,
      WEBWE(3 downto 0) => B"0000"
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity test_bd_pixelReOrder_0_0_rams_sp_rf_rst_11 is
  port (
    ram_reg_0_0 : out STD_LOGIC;
    ram_reg_0_1 : out STD_LOGIC;
    ram_reg_0_2 : out STD_LOGIC;
    ram_reg_0_3 : out STD_LOGIC;
    ram_reg_0_4 : out STD_LOGIC;
    ram_reg_0_5 : out STD_LOGIC;
    ram_reg_0_6 : out STD_LOGIC;
    ram_reg_0_7 : out STD_LOGIC;
    ram_reg_0_8 : out STD_LOGIC;
    ram_reg_0_9 : out STD_LOGIC;
    ram_reg_0_10 : out STD_LOGIC;
    ram_reg_0_11 : out STD_LOGIC;
    ram_reg_0_12 : out STD_LOGIC;
    ram_reg_0_13 : out STD_LOGIC;
    ram_reg_0_14 : out STD_LOGIC;
    ram_reg_0_15 : out STD_LOGIC;
    ram_reg_0_16 : out STD_LOGIC;
    ram_reg_0_17 : out STD_LOGIC;
    ram_reg_1_0 : out STD_LOGIC;
    ram_reg_1_1 : out STD_LOGIC;
    ram_reg_1_2 : out STD_LOGIC;
    ram_reg_1_3 : out STD_LOGIC;
    ram_reg_1_4 : out STD_LOGIC;
    ram_reg_1_5 : out STD_LOGIC;
    addr : out STD_LOGIC_VECTOR ( 10 downto 0 );
    dout0_out : in STD_LOGIC_VECTOR ( 23 downto 0 );
    ctrl_mux_out_1 : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \y_reg[23]_i_1\ : in STD_LOGIC_VECTOR ( 23 downto 0 );
    \y_reg[23]_i_1_0\ : in STD_LOGIC_VECTOR ( 23 downto 0 );
    Q : in STD_LOGIC_VECTOR ( 10 downto 0 );
    s00_axis_tuser : in STD_LOGIC_VECTOR ( 11 downto 0 );
    m00_axis_aclk : in STD_LOGIC;
    ram_reg_0_18 : in STD_LOGIC_VECTOR ( 0 to 0 );
    s00_axis_tdata : in STD_LOGIC_VECTOR ( 23 downto 0 );
    s00_axis_tvalid : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of test_bd_pixelReOrder_0_0_rams_sp_rf_rst_11 : entity is "rams_sp_rf_rst";
end test_bd_pixelReOrder_0_0_rams_sp_rf_rst_11;

architecture STRUCTURE of test_bd_pixelReOrder_0_0_rams_sp_rf_rst_11 is
  signal \_DOUT_1[15]_30\ : STD_LOGIC_VECTOR ( 23 downto 0 );
  signal \^addr\ : STD_LOGIC_VECTOR ( 10 downto 0 );
  signal \ram_reg_0_i_1__27_n_0\ : STD_LOGIC;
  signal NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_DBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_SBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_DOADO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 16 );
  signal NLW_ram_reg_0_DOBDO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_ram_reg_0_DOPADOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal NLW_ram_reg_0_DOPBDOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_ram_reg_0_ECCPARITY_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal NLW_ram_reg_0_RDADDRECC_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_ram_reg_1_DOADO_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 6 );
  signal NLW_ram_reg_1_DOBDO_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal NLW_ram_reg_1_DOPADOP_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_ram_reg_1_DOPBDOP_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ : string;
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of ram_reg_0 : label is "p2_d16";
  attribute METHODOLOGY_DRC_VIOS : string;
  attribute METHODOLOGY_DRC_VIOS of ram_reg_0 : label is "{SYNTH-6 {cell *THIS*}}";
  attribute RTL_RAM_BITS : integer;
  attribute RTL_RAM_BITS of ram_reg_0 : label is 46080;
  attribute RTL_RAM_NAME : string;
  attribute RTL_RAM_NAME of ram_reg_0 : label is "ram";
  attribute bram_addr_begin : integer;
  attribute bram_addr_begin of ram_reg_0 : label is 0;
  attribute bram_addr_end : integer;
  attribute bram_addr_end of ram_reg_0 : label is 2047;
  attribute bram_slice_begin : integer;
  attribute bram_slice_begin of ram_reg_0 : label is 0;
  attribute bram_slice_end : integer;
  attribute bram_slice_end of ram_reg_0 : label is 17;
  attribute ram_addr_begin : integer;
  attribute ram_addr_begin of ram_reg_0 : label is 0;
  attribute ram_addr_end : integer;
  attribute ram_addr_end of ram_reg_0 : label is 2047;
  attribute ram_offset : integer;
  attribute ram_offset of ram_reg_0 : label is 0;
  attribute ram_slice_begin : integer;
  attribute ram_slice_begin of ram_reg_0 : label is 0;
  attribute ram_slice_end : integer;
  attribute ram_slice_end of ram_reg_0 : label is 17;
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of ram_reg_1 : label is "p0_d6";
  attribute METHODOLOGY_DRC_VIOS of ram_reg_1 : label is "{SYNTH-6 {cell *THIS*}}";
  attribute RTL_RAM_BITS of ram_reg_1 : label is 46080;
  attribute RTL_RAM_NAME of ram_reg_1 : label is "ram";
  attribute bram_addr_begin of ram_reg_1 : label is 0;
  attribute bram_addr_end of ram_reg_1 : label is 2047;
  attribute bram_slice_begin of ram_reg_1 : label is 18;
  attribute bram_slice_end of ram_reg_1 : label is 23;
  attribute ram_addr_begin of ram_reg_1 : label is 0;
  attribute ram_addr_end of ram_reg_1 : label is 2047;
  attribute ram_offset of ram_reg_1 : label is 0;
  attribute ram_slice_begin of ram_reg_1 : label is 18;
  attribute ram_slice_end of ram_reg_1 : label is 23;
begin
  addr(10 downto 0) <= \^addr\(10 downto 0);
ram_reg_0: unisim.vcomponents.RAMB36E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      EN_ECC_READ => false,
      EN_ECC_WRITE => false,
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      RAM_EXTENSION_A => "NONE",
      RAM_EXTENSION_B => "NONE",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "PERFORMANCE",
      READ_WIDTH_A => 18,
      READ_WIDTH_B => 0,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "READ_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 18,
      WRITE_WIDTH_B => 0
    )
        port map (
      ADDRARDADDR(15) => '1',
      ADDRARDADDR(14 downto 4) => \^addr\(10 downto 0),
      ADDRARDADDR(3 downto 0) => B"1111",
      ADDRBWRADDR(15 downto 0) => B"1111111111111111",
      CASCADEINA => '1',
      CASCADEINB => '0',
      CASCADEOUTA => NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED,
      CASCADEOUTB => NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED,
      CLKARDCLK => m00_axis_aclk,
      CLKBWRCLK => '0',
      DBITERR => NLW_ram_reg_0_DBITERR_UNCONNECTED,
      DIADI(31 downto 16) => B"0000000000000000",
      DIADI(15 downto 0) => s00_axis_tdata(15 downto 0),
      DIBDI(31 downto 0) => B"11111111111111111111111111111111",
      DIPADIP(3 downto 2) => B"00",
      DIPADIP(1 downto 0) => s00_axis_tdata(17 downto 16),
      DIPBDIP(3 downto 0) => B"1111",
      DOADO(31 downto 16) => NLW_ram_reg_0_DOADO_UNCONNECTED(31 downto 16),
      DOADO(15 downto 0) => \_DOUT_1[15]_30\(15 downto 0),
      DOBDO(31 downto 0) => NLW_ram_reg_0_DOBDO_UNCONNECTED(31 downto 0),
      DOPADOP(3 downto 2) => NLW_ram_reg_0_DOPADOP_UNCONNECTED(3 downto 2),
      DOPADOP(1 downto 0) => \_DOUT_1[15]_30\(17 downto 16),
      DOPBDOP(3 downto 0) => NLW_ram_reg_0_DOPBDOP_UNCONNECTED(3 downto 0),
      ECCPARITY(7 downto 0) => NLW_ram_reg_0_ECCPARITY_UNCONNECTED(7 downto 0),
      ENARDEN => ram_reg_0_18(0),
      ENBWREN => '0',
      INJECTDBITERR => NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED,
      INJECTSBITERR => NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED,
      RDADDRECC(8 downto 0) => NLW_ram_reg_0_RDADDRECC_UNCONNECTED(8 downto 0),
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      SBITERR => NLW_ram_reg_0_SBITERR_UNCONNECTED,
      WEA(3) => \ram_reg_0_i_1__27_n_0\,
      WEA(2) => \ram_reg_0_i_1__27_n_0\,
      WEA(1) => \ram_reg_0_i_1__27_n_0\,
      WEA(0) => \ram_reg_0_i_1__27_n_0\,
      WEBWE(7 downto 0) => B"00000000"
    );
ram_reg_0_i_10: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => Q(1),
      I1 => s00_axis_tuser(11),
      I2 => s00_axis_tuser(1),
      O => \^addr\(1)
    );
ram_reg_0_i_11: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => Q(0),
      I1 => s00_axis_tuser(11),
      I2 => s00_axis_tuser(0),
      O => \^addr\(0)
    );
\ram_reg_0_i_1__27\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => ram_reg_0_18(0),
      I1 => s00_axis_tuser(11),
      I2 => s00_axis_tvalid,
      O => \ram_reg_0_i_1__27_n_0\
    );
\ram_reg_0_i_1__29\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => Q(10),
      I1 => s00_axis_tuser(11),
      I2 => s00_axis_tuser(10),
      O => \^addr\(10)
    );
ram_reg_0_i_2: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => Q(9),
      I1 => s00_axis_tuser(11),
      I2 => s00_axis_tuser(9),
      O => \^addr\(9)
    );
ram_reg_0_i_3: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => Q(8),
      I1 => s00_axis_tuser(11),
      I2 => s00_axis_tuser(8),
      O => \^addr\(8)
    );
ram_reg_0_i_4: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => Q(7),
      I1 => s00_axis_tuser(11),
      I2 => s00_axis_tuser(7),
      O => \^addr\(7)
    );
ram_reg_0_i_5: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => Q(6),
      I1 => s00_axis_tuser(11),
      I2 => s00_axis_tuser(6),
      O => \^addr\(6)
    );
ram_reg_0_i_6: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => Q(5),
      I1 => s00_axis_tuser(11),
      I2 => s00_axis_tuser(5),
      O => \^addr\(5)
    );
ram_reg_0_i_7: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => Q(4),
      I1 => s00_axis_tuser(11),
      I2 => s00_axis_tuser(4),
      O => \^addr\(4)
    );
ram_reg_0_i_8: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => Q(3),
      I1 => s00_axis_tuser(11),
      I2 => s00_axis_tuser(3),
      O => \^addr\(3)
    );
ram_reg_0_i_9: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => Q(2),
      I1 => s00_axis_tuser(11),
      I2 => s00_axis_tuser(2),
      O => \^addr\(2)
    );
ram_reg_1: unisim.vcomponents.RAMB18E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      INIT_A => X"00000",
      INIT_B => X"00000",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "PERFORMANCE",
      READ_WIDTH_A => 9,
      READ_WIDTH_B => 0,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"00000",
      SRVAL_B => X"00000",
      WRITE_MODE_A => "READ_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 9,
      WRITE_WIDTH_B => 0
    )
        port map (
      ADDRARDADDR(13 downto 3) => \^addr\(10 downto 0),
      ADDRARDADDR(2 downto 0) => B"111",
      ADDRBWRADDR(13 downto 0) => B"11111111111111",
      CLKARDCLK => m00_axis_aclk,
      CLKBWRCLK => '0',
      DIADI(15 downto 6) => B"0000000000",
      DIADI(5 downto 0) => s00_axis_tdata(23 downto 18),
      DIBDI(15 downto 0) => B"1111111111111111",
      DIPADIP(1 downto 0) => B"00",
      DIPBDIP(1 downto 0) => B"11",
      DOADO(15 downto 6) => NLW_ram_reg_1_DOADO_UNCONNECTED(15 downto 6),
      DOADO(5 downto 0) => \_DOUT_1[15]_30\(23 downto 18),
      DOBDO(15 downto 0) => NLW_ram_reg_1_DOBDO_UNCONNECTED(15 downto 0),
      DOPADOP(1 downto 0) => NLW_ram_reg_1_DOPADOP_UNCONNECTED(1 downto 0),
      DOPBDOP(1 downto 0) => NLW_ram_reg_1_DOPBDOP_UNCONNECTED(1 downto 0),
      ENARDEN => ram_reg_0_18(0),
      ENBWREN => '0',
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      WEA(1) => \ram_reg_0_i_1__27_n_0\,
      WEA(0) => \ram_reg_0_i_1__27_n_0\,
      WEBWE(3 downto 0) => B"0000"
    );
\y_reg[0]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_1[15]_30\(0),
      I1 => dout0_out(0),
      I2 => ctrl_mux_out_1(1),
      I3 => \y_reg[23]_i_1\(0),
      I4 => ctrl_mux_out_1(0),
      I5 => \y_reg[23]_i_1_0\(0),
      O => ram_reg_0_0
    );
\y_reg[10]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_1[15]_30\(10),
      I1 => dout0_out(10),
      I2 => ctrl_mux_out_1(1),
      I3 => \y_reg[23]_i_1\(10),
      I4 => ctrl_mux_out_1(0),
      I5 => \y_reg[23]_i_1_0\(10),
      O => ram_reg_0_10
    );
\y_reg[11]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_1[15]_30\(11),
      I1 => dout0_out(11),
      I2 => ctrl_mux_out_1(1),
      I3 => \y_reg[23]_i_1\(11),
      I4 => ctrl_mux_out_1(0),
      I5 => \y_reg[23]_i_1_0\(11),
      O => ram_reg_0_11
    );
\y_reg[12]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_1[15]_30\(12),
      I1 => dout0_out(12),
      I2 => ctrl_mux_out_1(1),
      I3 => \y_reg[23]_i_1\(12),
      I4 => ctrl_mux_out_1(0),
      I5 => \y_reg[23]_i_1_0\(12),
      O => ram_reg_0_12
    );
\y_reg[13]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_1[15]_30\(13),
      I1 => dout0_out(13),
      I2 => ctrl_mux_out_1(1),
      I3 => \y_reg[23]_i_1\(13),
      I4 => ctrl_mux_out_1(0),
      I5 => \y_reg[23]_i_1_0\(13),
      O => ram_reg_0_13
    );
\y_reg[14]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_1[15]_30\(14),
      I1 => dout0_out(14),
      I2 => ctrl_mux_out_1(1),
      I3 => \y_reg[23]_i_1\(14),
      I4 => ctrl_mux_out_1(0),
      I5 => \y_reg[23]_i_1_0\(14),
      O => ram_reg_0_14
    );
\y_reg[15]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_1[15]_30\(15),
      I1 => dout0_out(15),
      I2 => ctrl_mux_out_1(1),
      I3 => \y_reg[23]_i_1\(15),
      I4 => ctrl_mux_out_1(0),
      I5 => \y_reg[23]_i_1_0\(15),
      O => ram_reg_0_15
    );
\y_reg[16]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_1[15]_30\(16),
      I1 => dout0_out(16),
      I2 => ctrl_mux_out_1(1),
      I3 => \y_reg[23]_i_1\(16),
      I4 => ctrl_mux_out_1(0),
      I5 => \y_reg[23]_i_1_0\(16),
      O => ram_reg_0_16
    );
\y_reg[17]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_1[15]_30\(17),
      I1 => dout0_out(17),
      I2 => ctrl_mux_out_1(1),
      I3 => \y_reg[23]_i_1\(17),
      I4 => ctrl_mux_out_1(0),
      I5 => \y_reg[23]_i_1_0\(17),
      O => ram_reg_0_17
    );
\y_reg[18]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_1[15]_30\(18),
      I1 => dout0_out(18),
      I2 => ctrl_mux_out_1(1),
      I3 => \y_reg[23]_i_1\(18),
      I4 => ctrl_mux_out_1(0),
      I5 => \y_reg[23]_i_1_0\(18),
      O => ram_reg_1_0
    );
\y_reg[19]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_1[15]_30\(19),
      I1 => dout0_out(19),
      I2 => ctrl_mux_out_1(1),
      I3 => \y_reg[23]_i_1\(19),
      I4 => ctrl_mux_out_1(0),
      I5 => \y_reg[23]_i_1_0\(19),
      O => ram_reg_1_1
    );
\y_reg[1]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_1[15]_30\(1),
      I1 => dout0_out(1),
      I2 => ctrl_mux_out_1(1),
      I3 => \y_reg[23]_i_1\(1),
      I4 => ctrl_mux_out_1(0),
      I5 => \y_reg[23]_i_1_0\(1),
      O => ram_reg_0_1
    );
\y_reg[20]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_1[15]_30\(20),
      I1 => dout0_out(20),
      I2 => ctrl_mux_out_1(1),
      I3 => \y_reg[23]_i_1\(20),
      I4 => ctrl_mux_out_1(0),
      I5 => \y_reg[23]_i_1_0\(20),
      O => ram_reg_1_2
    );
\y_reg[21]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_1[15]_30\(21),
      I1 => dout0_out(21),
      I2 => ctrl_mux_out_1(1),
      I3 => \y_reg[23]_i_1\(21),
      I4 => ctrl_mux_out_1(0),
      I5 => \y_reg[23]_i_1_0\(21),
      O => ram_reg_1_3
    );
\y_reg[22]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_1[15]_30\(22),
      I1 => dout0_out(22),
      I2 => ctrl_mux_out_1(1),
      I3 => \y_reg[23]_i_1\(22),
      I4 => ctrl_mux_out_1(0),
      I5 => \y_reg[23]_i_1_0\(22),
      O => ram_reg_1_4
    );
\y_reg[23]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_1[15]_30\(23),
      I1 => dout0_out(23),
      I2 => ctrl_mux_out_1(1),
      I3 => \y_reg[23]_i_1\(23),
      I4 => ctrl_mux_out_1(0),
      I5 => \y_reg[23]_i_1_0\(23),
      O => ram_reg_1_5
    );
\y_reg[2]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_1[15]_30\(2),
      I1 => dout0_out(2),
      I2 => ctrl_mux_out_1(1),
      I3 => \y_reg[23]_i_1\(2),
      I4 => ctrl_mux_out_1(0),
      I5 => \y_reg[23]_i_1_0\(2),
      O => ram_reg_0_2
    );
\y_reg[3]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_1[15]_30\(3),
      I1 => dout0_out(3),
      I2 => ctrl_mux_out_1(1),
      I3 => \y_reg[23]_i_1\(3),
      I4 => ctrl_mux_out_1(0),
      I5 => \y_reg[23]_i_1_0\(3),
      O => ram_reg_0_3
    );
\y_reg[4]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_1[15]_30\(4),
      I1 => dout0_out(4),
      I2 => ctrl_mux_out_1(1),
      I3 => \y_reg[23]_i_1\(4),
      I4 => ctrl_mux_out_1(0),
      I5 => \y_reg[23]_i_1_0\(4),
      O => ram_reg_0_4
    );
\y_reg[5]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_1[15]_30\(5),
      I1 => dout0_out(5),
      I2 => ctrl_mux_out_1(1),
      I3 => \y_reg[23]_i_1\(5),
      I4 => ctrl_mux_out_1(0),
      I5 => \y_reg[23]_i_1_0\(5),
      O => ram_reg_0_5
    );
\y_reg[6]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_1[15]_30\(6),
      I1 => dout0_out(6),
      I2 => ctrl_mux_out_1(1),
      I3 => \y_reg[23]_i_1\(6),
      I4 => ctrl_mux_out_1(0),
      I5 => \y_reg[23]_i_1_0\(6),
      O => ram_reg_0_6
    );
\y_reg[7]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_1[15]_30\(7),
      I1 => dout0_out(7),
      I2 => ctrl_mux_out_1(1),
      I3 => \y_reg[23]_i_1\(7),
      I4 => ctrl_mux_out_1(0),
      I5 => \y_reg[23]_i_1_0\(7),
      O => ram_reg_0_7
    );
\y_reg[8]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_1[15]_30\(8),
      I1 => dout0_out(8),
      I2 => ctrl_mux_out_1(1),
      I3 => \y_reg[23]_i_1\(8),
      I4 => ctrl_mux_out_1(0),
      I5 => \y_reg[23]_i_1_0\(8),
      O => ram_reg_0_8
    );
\y_reg[9]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_1[15]_30\(9),
      I1 => dout0_out(9),
      I2 => ctrl_mux_out_1(1),
      I3 => \y_reg[23]_i_1\(9),
      I4 => ctrl_mux_out_1(0),
      I5 => \y_reg[23]_i_1_0\(9),
      O => ram_reg_0_9
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity test_bd_pixelReOrder_0_0_rams_sp_rf_rst_12 is
  port (
    ram_reg_0_0 : out STD_LOGIC;
    ram_reg_0_1 : out STD_LOGIC;
    ram_reg_0_2 : out STD_LOGIC;
    ram_reg_0_3 : out STD_LOGIC;
    ram_reg_0_4 : out STD_LOGIC;
    ram_reg_0_5 : out STD_LOGIC;
    ram_reg_0_6 : out STD_LOGIC;
    ram_reg_0_7 : out STD_LOGIC;
    ram_reg_0_8 : out STD_LOGIC;
    ram_reg_0_9 : out STD_LOGIC;
    ram_reg_0_10 : out STD_LOGIC;
    ram_reg_0_11 : out STD_LOGIC;
    ram_reg_0_12 : out STD_LOGIC;
    ram_reg_0_13 : out STD_LOGIC;
    ram_reg_0_14 : out STD_LOGIC;
    ram_reg_0_15 : out STD_LOGIC;
    ram_reg_0_16 : out STD_LOGIC;
    ram_reg_0_17 : out STD_LOGIC;
    ram_reg_1_0 : out STD_LOGIC;
    ram_reg_1_1 : out STD_LOGIC;
    ram_reg_1_2 : out STD_LOGIC;
    ram_reg_1_3 : out STD_LOGIC;
    ram_reg_1_4 : out STD_LOGIC;
    ram_reg_1_5 : out STD_LOGIC;
    addr : out STD_LOGIC_VECTOR ( 10 downto 0 );
    dout0_out : in STD_LOGIC_VECTOR ( 23 downto 0 );
    ctrl_mux_out_2 : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \y_reg[23]_i_1__0\ : in STD_LOGIC_VECTOR ( 23 downto 0 );
    \y_reg[23]_i_1__0_0\ : in STD_LOGIC_VECTOR ( 23 downto 0 );
    s00_axis_tuser : in STD_LOGIC_VECTOR ( 11 downto 0 );
    Q : in STD_LOGIC_VECTOR ( 10 downto 0 );
    m00_axis_aclk : in STD_LOGIC;
    ram_reg_0_18 : in STD_LOGIC_VECTOR ( 0 to 0 );
    s00_axis_tdata : in STD_LOGIC_VECTOR ( 23 downto 0 );
    s00_axis_tvalid : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of test_bd_pixelReOrder_0_0_rams_sp_rf_rst_12 : entity is "rams_sp_rf_rst";
end test_bd_pixelReOrder_0_0_rams_sp_rf_rst_12;

architecture STRUCTURE of test_bd_pixelReOrder_0_0_rams_sp_rf_rst_12 is
  signal \_DOUT_2[15]_31\ : STD_LOGIC_VECTOR ( 23 downto 0 );
  signal \^addr\ : STD_LOGIC_VECTOR ( 10 downto 0 );
  signal \ram_reg_0_i_1__28_n_0\ : STD_LOGIC;
  signal NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_DBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_SBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_DOADO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 16 );
  signal NLW_ram_reg_0_DOBDO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_ram_reg_0_DOPADOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal NLW_ram_reg_0_DOPBDOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_ram_reg_0_ECCPARITY_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal NLW_ram_reg_0_RDADDRECC_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_ram_reg_1_DOADO_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 6 );
  signal NLW_ram_reg_1_DOBDO_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal NLW_ram_reg_1_DOPADOP_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_ram_reg_1_DOPBDOP_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ : string;
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of ram_reg_0 : label is "p2_d16";
  attribute METHODOLOGY_DRC_VIOS : string;
  attribute METHODOLOGY_DRC_VIOS of ram_reg_0 : label is "{SYNTH-6 {cell *THIS*}}";
  attribute RTL_RAM_BITS : integer;
  attribute RTL_RAM_BITS of ram_reg_0 : label is 46080;
  attribute RTL_RAM_NAME : string;
  attribute RTL_RAM_NAME of ram_reg_0 : label is "ram";
  attribute bram_addr_begin : integer;
  attribute bram_addr_begin of ram_reg_0 : label is 0;
  attribute bram_addr_end : integer;
  attribute bram_addr_end of ram_reg_0 : label is 2047;
  attribute bram_slice_begin : integer;
  attribute bram_slice_begin of ram_reg_0 : label is 0;
  attribute bram_slice_end : integer;
  attribute bram_slice_end of ram_reg_0 : label is 17;
  attribute ram_addr_begin : integer;
  attribute ram_addr_begin of ram_reg_0 : label is 0;
  attribute ram_addr_end : integer;
  attribute ram_addr_end of ram_reg_0 : label is 2047;
  attribute ram_offset : integer;
  attribute ram_offset of ram_reg_0 : label is 0;
  attribute ram_slice_begin : integer;
  attribute ram_slice_begin of ram_reg_0 : label is 0;
  attribute ram_slice_end : integer;
  attribute ram_slice_end of ram_reg_0 : label is 17;
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of ram_reg_1 : label is "p0_d6";
  attribute METHODOLOGY_DRC_VIOS of ram_reg_1 : label is "{SYNTH-6 {cell *THIS*}}";
  attribute RTL_RAM_BITS of ram_reg_1 : label is 46080;
  attribute RTL_RAM_NAME of ram_reg_1 : label is "ram";
  attribute bram_addr_begin of ram_reg_1 : label is 0;
  attribute bram_addr_end of ram_reg_1 : label is 2047;
  attribute bram_slice_begin of ram_reg_1 : label is 18;
  attribute bram_slice_end of ram_reg_1 : label is 23;
  attribute ram_addr_begin of ram_reg_1 : label is 0;
  attribute ram_addr_end of ram_reg_1 : label is 2047;
  attribute ram_offset of ram_reg_1 : label is 0;
  attribute ram_slice_begin of ram_reg_1 : label is 18;
  attribute ram_slice_end of ram_reg_1 : label is 23;
begin
  addr(10 downto 0) <= \^addr\(10 downto 0);
ram_reg_0: unisim.vcomponents.RAMB36E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      EN_ECC_READ => false,
      EN_ECC_WRITE => false,
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      RAM_EXTENSION_A => "NONE",
      RAM_EXTENSION_B => "NONE",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "PERFORMANCE",
      READ_WIDTH_A => 18,
      READ_WIDTH_B => 0,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "READ_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 18,
      WRITE_WIDTH_B => 0
    )
        port map (
      ADDRARDADDR(15) => '1',
      ADDRARDADDR(14 downto 4) => \^addr\(10 downto 0),
      ADDRARDADDR(3 downto 0) => B"1111",
      ADDRBWRADDR(15 downto 0) => B"1111111111111111",
      CASCADEINA => '1',
      CASCADEINB => '0',
      CASCADEOUTA => NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED,
      CASCADEOUTB => NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED,
      CLKARDCLK => m00_axis_aclk,
      CLKBWRCLK => '0',
      DBITERR => NLW_ram_reg_0_DBITERR_UNCONNECTED,
      DIADI(31 downto 16) => B"0000000000000000",
      DIADI(15 downto 0) => s00_axis_tdata(15 downto 0),
      DIBDI(31 downto 0) => B"11111111111111111111111111111111",
      DIPADIP(3 downto 2) => B"00",
      DIPADIP(1 downto 0) => s00_axis_tdata(17 downto 16),
      DIPBDIP(3 downto 0) => B"1111",
      DOADO(31 downto 16) => NLW_ram_reg_0_DOADO_UNCONNECTED(31 downto 16),
      DOADO(15 downto 0) => \_DOUT_2[15]_31\(15 downto 0),
      DOBDO(31 downto 0) => NLW_ram_reg_0_DOBDO_UNCONNECTED(31 downto 0),
      DOPADOP(3 downto 2) => NLW_ram_reg_0_DOPADOP_UNCONNECTED(3 downto 2),
      DOPADOP(1 downto 0) => \_DOUT_2[15]_31\(17 downto 16),
      DOPBDOP(3 downto 0) => NLW_ram_reg_0_DOPBDOP_UNCONNECTED(3 downto 0),
      ECCPARITY(7 downto 0) => NLW_ram_reg_0_ECCPARITY_UNCONNECTED(7 downto 0),
      ENARDEN => ram_reg_0_18(0),
      ENBWREN => '0',
      INJECTDBITERR => NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED,
      INJECTSBITERR => NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED,
      RDADDRECC(8 downto 0) => NLW_ram_reg_0_RDADDRECC_UNCONNECTED(8 downto 0),
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      SBITERR => NLW_ram_reg_0_SBITERR_UNCONNECTED,
      WEA(3) => \ram_reg_0_i_1__28_n_0\,
      WEA(2) => \ram_reg_0_i_1__28_n_0\,
      WEA(1) => \ram_reg_0_i_1__28_n_0\,
      WEA(0) => \ram_reg_0_i_1__28_n_0\,
      WEBWE(7 downto 0) => B"00000000"
    );
\ram_reg_0_i_10__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s00_axis_tuser(1),
      I1 => s00_axis_tuser(11),
      I2 => Q(1),
      O => \^addr\(1)
    );
\ram_reg_0_i_11__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s00_axis_tuser(0),
      I1 => s00_axis_tuser(11),
      I2 => Q(0),
      O => \^addr\(0)
    );
\ram_reg_0_i_1__28\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => ram_reg_0_18(0),
      I1 => s00_axis_tuser(11),
      I2 => s00_axis_tvalid,
      O => \ram_reg_0_i_1__28_n_0\
    );
\ram_reg_0_i_1__30\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s00_axis_tuser(10),
      I1 => s00_axis_tuser(11),
      I2 => Q(10),
      O => \^addr\(10)
    );
\ram_reg_0_i_2__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s00_axis_tuser(9),
      I1 => s00_axis_tuser(11),
      I2 => Q(9),
      O => \^addr\(9)
    );
\ram_reg_0_i_3__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s00_axis_tuser(8),
      I1 => s00_axis_tuser(11),
      I2 => Q(8),
      O => \^addr\(8)
    );
\ram_reg_0_i_4__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s00_axis_tuser(7),
      I1 => s00_axis_tuser(11),
      I2 => Q(7),
      O => \^addr\(7)
    );
\ram_reg_0_i_5__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s00_axis_tuser(6),
      I1 => s00_axis_tuser(11),
      I2 => Q(6),
      O => \^addr\(6)
    );
\ram_reg_0_i_6__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s00_axis_tuser(5),
      I1 => s00_axis_tuser(11),
      I2 => Q(5),
      O => \^addr\(5)
    );
\ram_reg_0_i_7__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s00_axis_tuser(4),
      I1 => s00_axis_tuser(11),
      I2 => Q(4),
      O => \^addr\(4)
    );
\ram_reg_0_i_8__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s00_axis_tuser(3),
      I1 => s00_axis_tuser(11),
      I2 => Q(3),
      O => \^addr\(3)
    );
\ram_reg_0_i_9__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s00_axis_tuser(2),
      I1 => s00_axis_tuser(11),
      I2 => Q(2),
      O => \^addr\(2)
    );
ram_reg_1: unisim.vcomponents.RAMB18E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      INIT_A => X"00000",
      INIT_B => X"00000",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "PERFORMANCE",
      READ_WIDTH_A => 9,
      READ_WIDTH_B => 0,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"00000",
      SRVAL_B => X"00000",
      WRITE_MODE_A => "READ_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 9,
      WRITE_WIDTH_B => 0
    )
        port map (
      ADDRARDADDR(13 downto 3) => \^addr\(10 downto 0),
      ADDRARDADDR(2 downto 0) => B"111",
      ADDRBWRADDR(13 downto 0) => B"11111111111111",
      CLKARDCLK => m00_axis_aclk,
      CLKBWRCLK => '0',
      DIADI(15 downto 6) => B"0000000000",
      DIADI(5 downto 0) => s00_axis_tdata(23 downto 18),
      DIBDI(15 downto 0) => B"1111111111111111",
      DIPADIP(1 downto 0) => B"00",
      DIPBDIP(1 downto 0) => B"11",
      DOADO(15 downto 6) => NLW_ram_reg_1_DOADO_UNCONNECTED(15 downto 6),
      DOADO(5 downto 0) => \_DOUT_2[15]_31\(23 downto 18),
      DOBDO(15 downto 0) => NLW_ram_reg_1_DOBDO_UNCONNECTED(15 downto 0),
      DOPADOP(1 downto 0) => NLW_ram_reg_1_DOPADOP_UNCONNECTED(1 downto 0),
      DOPBDOP(1 downto 0) => NLW_ram_reg_1_DOPBDOP_UNCONNECTED(1 downto 0),
      ENARDEN => ram_reg_0_18(0),
      ENBWREN => '0',
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      WEA(1) => \ram_reg_0_i_1__28_n_0\,
      WEA(0) => \ram_reg_0_i_1__28_n_0\,
      WEBWE(3 downto 0) => B"0000"
    );
\y_reg[0]_i_4__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_2[15]_31\(0),
      I1 => dout0_out(0),
      I2 => ctrl_mux_out_2(1),
      I3 => \y_reg[23]_i_1__0\(0),
      I4 => ctrl_mux_out_2(0),
      I5 => \y_reg[23]_i_1__0_0\(0),
      O => ram_reg_0_0
    );
\y_reg[10]_i_4__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_2[15]_31\(10),
      I1 => dout0_out(10),
      I2 => ctrl_mux_out_2(1),
      I3 => \y_reg[23]_i_1__0\(10),
      I4 => ctrl_mux_out_2(0),
      I5 => \y_reg[23]_i_1__0_0\(10),
      O => ram_reg_0_10
    );
\y_reg[11]_i_4__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_2[15]_31\(11),
      I1 => dout0_out(11),
      I2 => ctrl_mux_out_2(1),
      I3 => \y_reg[23]_i_1__0\(11),
      I4 => ctrl_mux_out_2(0),
      I5 => \y_reg[23]_i_1__0_0\(11),
      O => ram_reg_0_11
    );
\y_reg[12]_i_4__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_2[15]_31\(12),
      I1 => dout0_out(12),
      I2 => ctrl_mux_out_2(1),
      I3 => \y_reg[23]_i_1__0\(12),
      I4 => ctrl_mux_out_2(0),
      I5 => \y_reg[23]_i_1__0_0\(12),
      O => ram_reg_0_12
    );
\y_reg[13]_i_4__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_2[15]_31\(13),
      I1 => dout0_out(13),
      I2 => ctrl_mux_out_2(1),
      I3 => \y_reg[23]_i_1__0\(13),
      I4 => ctrl_mux_out_2(0),
      I5 => \y_reg[23]_i_1__0_0\(13),
      O => ram_reg_0_13
    );
\y_reg[14]_i_4__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_2[15]_31\(14),
      I1 => dout0_out(14),
      I2 => ctrl_mux_out_2(1),
      I3 => \y_reg[23]_i_1__0\(14),
      I4 => ctrl_mux_out_2(0),
      I5 => \y_reg[23]_i_1__0_0\(14),
      O => ram_reg_0_14
    );
\y_reg[15]_i_4__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_2[15]_31\(15),
      I1 => dout0_out(15),
      I2 => ctrl_mux_out_2(1),
      I3 => \y_reg[23]_i_1__0\(15),
      I4 => ctrl_mux_out_2(0),
      I5 => \y_reg[23]_i_1__0_0\(15),
      O => ram_reg_0_15
    );
\y_reg[16]_i_4__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_2[15]_31\(16),
      I1 => dout0_out(16),
      I2 => ctrl_mux_out_2(1),
      I3 => \y_reg[23]_i_1__0\(16),
      I4 => ctrl_mux_out_2(0),
      I5 => \y_reg[23]_i_1__0_0\(16),
      O => ram_reg_0_16
    );
\y_reg[17]_i_4__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_2[15]_31\(17),
      I1 => dout0_out(17),
      I2 => ctrl_mux_out_2(1),
      I3 => \y_reg[23]_i_1__0\(17),
      I4 => ctrl_mux_out_2(0),
      I5 => \y_reg[23]_i_1__0_0\(17),
      O => ram_reg_0_17
    );
\y_reg[18]_i_4__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_2[15]_31\(18),
      I1 => dout0_out(18),
      I2 => ctrl_mux_out_2(1),
      I3 => \y_reg[23]_i_1__0\(18),
      I4 => ctrl_mux_out_2(0),
      I5 => \y_reg[23]_i_1__0_0\(18),
      O => ram_reg_1_0
    );
\y_reg[19]_i_4__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_2[15]_31\(19),
      I1 => dout0_out(19),
      I2 => ctrl_mux_out_2(1),
      I3 => \y_reg[23]_i_1__0\(19),
      I4 => ctrl_mux_out_2(0),
      I5 => \y_reg[23]_i_1__0_0\(19),
      O => ram_reg_1_1
    );
\y_reg[1]_i_4__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_2[15]_31\(1),
      I1 => dout0_out(1),
      I2 => ctrl_mux_out_2(1),
      I3 => \y_reg[23]_i_1__0\(1),
      I4 => ctrl_mux_out_2(0),
      I5 => \y_reg[23]_i_1__0_0\(1),
      O => ram_reg_0_1
    );
\y_reg[20]_i_4__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_2[15]_31\(20),
      I1 => dout0_out(20),
      I2 => ctrl_mux_out_2(1),
      I3 => \y_reg[23]_i_1__0\(20),
      I4 => ctrl_mux_out_2(0),
      I5 => \y_reg[23]_i_1__0_0\(20),
      O => ram_reg_1_2
    );
\y_reg[21]_i_4__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_2[15]_31\(21),
      I1 => dout0_out(21),
      I2 => ctrl_mux_out_2(1),
      I3 => \y_reg[23]_i_1__0\(21),
      I4 => ctrl_mux_out_2(0),
      I5 => \y_reg[23]_i_1__0_0\(21),
      O => ram_reg_1_3
    );
\y_reg[22]_i_4__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_2[15]_31\(22),
      I1 => dout0_out(22),
      I2 => ctrl_mux_out_2(1),
      I3 => \y_reg[23]_i_1__0\(22),
      I4 => ctrl_mux_out_2(0),
      I5 => \y_reg[23]_i_1__0_0\(22),
      O => ram_reg_1_4
    );
\y_reg[23]_i_5__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_2[15]_31\(23),
      I1 => dout0_out(23),
      I2 => ctrl_mux_out_2(1),
      I3 => \y_reg[23]_i_1__0\(23),
      I4 => ctrl_mux_out_2(0),
      I5 => \y_reg[23]_i_1__0_0\(23),
      O => ram_reg_1_5
    );
\y_reg[2]_i_4__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_2[15]_31\(2),
      I1 => dout0_out(2),
      I2 => ctrl_mux_out_2(1),
      I3 => \y_reg[23]_i_1__0\(2),
      I4 => ctrl_mux_out_2(0),
      I5 => \y_reg[23]_i_1__0_0\(2),
      O => ram_reg_0_2
    );
\y_reg[3]_i_4__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_2[15]_31\(3),
      I1 => dout0_out(3),
      I2 => ctrl_mux_out_2(1),
      I3 => \y_reg[23]_i_1__0\(3),
      I4 => ctrl_mux_out_2(0),
      I5 => \y_reg[23]_i_1__0_0\(3),
      O => ram_reg_0_3
    );
\y_reg[4]_i_4__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_2[15]_31\(4),
      I1 => dout0_out(4),
      I2 => ctrl_mux_out_2(1),
      I3 => \y_reg[23]_i_1__0\(4),
      I4 => ctrl_mux_out_2(0),
      I5 => \y_reg[23]_i_1__0_0\(4),
      O => ram_reg_0_4
    );
\y_reg[5]_i_4__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_2[15]_31\(5),
      I1 => dout0_out(5),
      I2 => ctrl_mux_out_2(1),
      I3 => \y_reg[23]_i_1__0\(5),
      I4 => ctrl_mux_out_2(0),
      I5 => \y_reg[23]_i_1__0_0\(5),
      O => ram_reg_0_5
    );
\y_reg[6]_i_4__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_2[15]_31\(6),
      I1 => dout0_out(6),
      I2 => ctrl_mux_out_2(1),
      I3 => \y_reg[23]_i_1__0\(6),
      I4 => ctrl_mux_out_2(0),
      I5 => \y_reg[23]_i_1__0_0\(6),
      O => ram_reg_0_6
    );
\y_reg[7]_i_4__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_2[15]_31\(7),
      I1 => dout0_out(7),
      I2 => ctrl_mux_out_2(1),
      I3 => \y_reg[23]_i_1__0\(7),
      I4 => ctrl_mux_out_2(0),
      I5 => \y_reg[23]_i_1__0_0\(7),
      O => ram_reg_0_7
    );
\y_reg[8]_i_4__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_2[15]_31\(8),
      I1 => dout0_out(8),
      I2 => ctrl_mux_out_2(1),
      I3 => \y_reg[23]_i_1__0\(8),
      I4 => ctrl_mux_out_2(0),
      I5 => \y_reg[23]_i_1__0_0\(8),
      O => ram_reg_0_8
    );
\y_reg[9]_i_4__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_2[15]_31\(9),
      I1 => dout0_out(9),
      I2 => ctrl_mux_out_2(1),
      I3 => \y_reg[23]_i_1__0\(9),
      I4 => ctrl_mux_out_2(0),
      I5 => \y_reg[23]_i_1__0_0\(9),
      O => ram_reg_0_9
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity test_bd_pixelReOrder_0_0_rams_sp_rf_rst_13 is
  port (
    dout0_out : out STD_LOGIC_VECTOR ( 23 downto 0 );
    m00_axis_aclk : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 0 to 0 );
    addr : in STD_LOGIC_VECTOR ( 10 downto 0 );
    s00_axis_tdata : in STD_LOGIC_VECTOR ( 23 downto 0 );
    s00_axis_tuser : in STD_LOGIC_VECTOR ( 0 to 0 );
    s00_axis_tvalid : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of test_bd_pixelReOrder_0_0_rams_sp_rf_rst_13 : entity is "rams_sp_rf_rst";
end test_bd_pixelReOrder_0_0_rams_sp_rf_rst_13;

architecture STRUCTURE of test_bd_pixelReOrder_0_0_rams_sp_rf_rst_13 is
  signal ram_reg_0_i_1_n_0 : STD_LOGIC;
  signal NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_DBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_SBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_DOADO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 16 );
  signal NLW_ram_reg_0_DOBDO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_ram_reg_0_DOPADOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal NLW_ram_reg_0_DOPBDOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_ram_reg_0_ECCPARITY_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal NLW_ram_reg_0_RDADDRECC_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_ram_reg_1_DOADO_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 6 );
  signal NLW_ram_reg_1_DOBDO_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal NLW_ram_reg_1_DOPADOP_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_ram_reg_1_DOPBDOP_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ : string;
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of ram_reg_0 : label is "p2_d16";
  attribute METHODOLOGY_DRC_VIOS : string;
  attribute METHODOLOGY_DRC_VIOS of ram_reg_0 : label is "{SYNTH-6 {cell *THIS*}}";
  attribute RTL_RAM_BITS : integer;
  attribute RTL_RAM_BITS of ram_reg_0 : label is 46080;
  attribute RTL_RAM_NAME : string;
  attribute RTL_RAM_NAME of ram_reg_0 : label is "ram";
  attribute bram_addr_begin : integer;
  attribute bram_addr_begin of ram_reg_0 : label is 0;
  attribute bram_addr_end : integer;
  attribute bram_addr_end of ram_reg_0 : label is 2047;
  attribute bram_slice_begin : integer;
  attribute bram_slice_begin of ram_reg_0 : label is 0;
  attribute bram_slice_end : integer;
  attribute bram_slice_end of ram_reg_0 : label is 17;
  attribute ram_addr_begin : integer;
  attribute ram_addr_begin of ram_reg_0 : label is 0;
  attribute ram_addr_end : integer;
  attribute ram_addr_end of ram_reg_0 : label is 2047;
  attribute ram_offset : integer;
  attribute ram_offset of ram_reg_0 : label is 0;
  attribute ram_slice_begin : integer;
  attribute ram_slice_begin of ram_reg_0 : label is 0;
  attribute ram_slice_end : integer;
  attribute ram_slice_end of ram_reg_0 : label is 17;
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of ram_reg_1 : label is "p0_d6";
  attribute METHODOLOGY_DRC_VIOS of ram_reg_1 : label is "{SYNTH-6 {cell *THIS*}}";
  attribute RTL_RAM_BITS of ram_reg_1 : label is 46080;
  attribute RTL_RAM_NAME of ram_reg_1 : label is "ram";
  attribute bram_addr_begin of ram_reg_1 : label is 0;
  attribute bram_addr_end of ram_reg_1 : label is 2047;
  attribute bram_slice_begin of ram_reg_1 : label is 18;
  attribute bram_slice_end of ram_reg_1 : label is 23;
  attribute ram_addr_begin of ram_reg_1 : label is 0;
  attribute ram_addr_end of ram_reg_1 : label is 2047;
  attribute ram_offset of ram_reg_1 : label is 0;
  attribute ram_slice_begin of ram_reg_1 : label is 18;
  attribute ram_slice_end of ram_reg_1 : label is 23;
begin
ram_reg_0: unisim.vcomponents.RAMB36E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      EN_ECC_READ => false,
      EN_ECC_WRITE => false,
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      RAM_EXTENSION_A => "NONE",
      RAM_EXTENSION_B => "NONE",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "PERFORMANCE",
      READ_WIDTH_A => 18,
      READ_WIDTH_B => 0,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "READ_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 18,
      WRITE_WIDTH_B => 0
    )
        port map (
      ADDRARDADDR(15) => '1',
      ADDRARDADDR(14 downto 4) => addr(10 downto 0),
      ADDRARDADDR(3 downto 0) => B"1111",
      ADDRBWRADDR(15 downto 0) => B"1111111111111111",
      CASCADEINA => '1',
      CASCADEINB => '0',
      CASCADEOUTA => NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED,
      CASCADEOUTB => NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED,
      CLKARDCLK => m00_axis_aclk,
      CLKBWRCLK => '0',
      DBITERR => NLW_ram_reg_0_DBITERR_UNCONNECTED,
      DIADI(31 downto 16) => B"0000000000000000",
      DIADI(15 downto 0) => s00_axis_tdata(15 downto 0),
      DIBDI(31 downto 0) => B"11111111111111111111111111111111",
      DIPADIP(3 downto 2) => B"00",
      DIPADIP(1 downto 0) => s00_axis_tdata(17 downto 16),
      DIPBDIP(3 downto 0) => B"1111",
      DOADO(31 downto 16) => NLW_ram_reg_0_DOADO_UNCONNECTED(31 downto 16),
      DOADO(15 downto 0) => dout0_out(15 downto 0),
      DOBDO(31 downto 0) => NLW_ram_reg_0_DOBDO_UNCONNECTED(31 downto 0),
      DOPADOP(3 downto 2) => NLW_ram_reg_0_DOPADOP_UNCONNECTED(3 downto 2),
      DOPADOP(1 downto 0) => dout0_out(17 downto 16),
      DOPBDOP(3 downto 0) => NLW_ram_reg_0_DOPBDOP_UNCONNECTED(3 downto 0),
      ECCPARITY(7 downto 0) => NLW_ram_reg_0_ECCPARITY_UNCONNECTED(7 downto 0),
      ENARDEN => Q(0),
      ENBWREN => '0',
      INJECTDBITERR => NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED,
      INJECTSBITERR => NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED,
      RDADDRECC(8 downto 0) => NLW_ram_reg_0_RDADDRECC_UNCONNECTED(8 downto 0),
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      SBITERR => NLW_ram_reg_0_SBITERR_UNCONNECTED,
      WEA(3) => ram_reg_0_i_1_n_0,
      WEA(2) => ram_reg_0_i_1_n_0,
      WEA(1) => ram_reg_0_i_1_n_0,
      WEA(0) => ram_reg_0_i_1_n_0,
      WEBWE(7 downto 0) => B"00000000"
    );
ram_reg_0_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => Q(0),
      I1 => s00_axis_tuser(0),
      I2 => s00_axis_tvalid,
      O => ram_reg_0_i_1_n_0
    );
ram_reg_1: unisim.vcomponents.RAMB18E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      INIT_A => X"00000",
      INIT_B => X"00000",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "PERFORMANCE",
      READ_WIDTH_A => 9,
      READ_WIDTH_B => 0,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"00000",
      SRVAL_B => X"00000",
      WRITE_MODE_A => "READ_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 9,
      WRITE_WIDTH_B => 0
    )
        port map (
      ADDRARDADDR(13 downto 3) => addr(10 downto 0),
      ADDRARDADDR(2 downto 0) => B"111",
      ADDRBWRADDR(13 downto 0) => B"11111111111111",
      CLKARDCLK => m00_axis_aclk,
      CLKBWRCLK => '0',
      DIADI(15 downto 6) => B"0000000000",
      DIADI(5 downto 0) => s00_axis_tdata(23 downto 18),
      DIBDI(15 downto 0) => B"1111111111111111",
      DIPADIP(1 downto 0) => B"00",
      DIPBDIP(1 downto 0) => B"11",
      DOADO(15 downto 6) => NLW_ram_reg_1_DOADO_UNCONNECTED(15 downto 6),
      DOADO(5 downto 0) => dout0_out(23 downto 18),
      DOBDO(15 downto 0) => NLW_ram_reg_1_DOBDO_UNCONNECTED(15 downto 0),
      DOPADOP(1 downto 0) => NLW_ram_reg_1_DOPADOP_UNCONNECTED(1 downto 0),
      DOPBDOP(1 downto 0) => NLW_ram_reg_1_DOPBDOP_UNCONNECTED(1 downto 0),
      ENARDEN => Q(0),
      ENBWREN => '0',
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      WEA(1) => ram_reg_0_i_1_n_0,
      WEA(0) => ram_reg_0_i_1_n_0,
      WEBWE(3 downto 0) => B"0000"
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity test_bd_pixelReOrder_0_0_rams_sp_rf_rst_14 is
  port (
    dout0_out : out STD_LOGIC_VECTOR ( 23 downto 0 );
    m00_axis_aclk : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 0 to 0 );
    addr : in STD_LOGIC_VECTOR ( 10 downto 0 );
    s00_axis_tdata : in STD_LOGIC_VECTOR ( 23 downto 0 );
    s00_axis_tuser : in STD_LOGIC_VECTOR ( 0 to 0 );
    s00_axis_tvalid : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of test_bd_pixelReOrder_0_0_rams_sp_rf_rst_14 : entity is "rams_sp_rf_rst";
end test_bd_pixelReOrder_0_0_rams_sp_rf_rst_14;

architecture STRUCTURE of test_bd_pixelReOrder_0_0_rams_sp_rf_rst_14 is
  signal \ram_reg_0_i_1__0_n_0\ : STD_LOGIC;
  signal NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_DBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_SBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_DOADO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 16 );
  signal NLW_ram_reg_0_DOBDO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_ram_reg_0_DOPADOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal NLW_ram_reg_0_DOPBDOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_ram_reg_0_ECCPARITY_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal NLW_ram_reg_0_RDADDRECC_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_ram_reg_1_DOADO_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 6 );
  signal NLW_ram_reg_1_DOBDO_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal NLW_ram_reg_1_DOPADOP_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_ram_reg_1_DOPBDOP_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ : string;
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of ram_reg_0 : label is "p2_d16";
  attribute METHODOLOGY_DRC_VIOS : string;
  attribute METHODOLOGY_DRC_VIOS of ram_reg_0 : label is "{SYNTH-6 {cell *THIS*}}";
  attribute RTL_RAM_BITS : integer;
  attribute RTL_RAM_BITS of ram_reg_0 : label is 46080;
  attribute RTL_RAM_NAME : string;
  attribute RTL_RAM_NAME of ram_reg_0 : label is "ram";
  attribute bram_addr_begin : integer;
  attribute bram_addr_begin of ram_reg_0 : label is 0;
  attribute bram_addr_end : integer;
  attribute bram_addr_end of ram_reg_0 : label is 2047;
  attribute bram_slice_begin : integer;
  attribute bram_slice_begin of ram_reg_0 : label is 0;
  attribute bram_slice_end : integer;
  attribute bram_slice_end of ram_reg_0 : label is 17;
  attribute ram_addr_begin : integer;
  attribute ram_addr_begin of ram_reg_0 : label is 0;
  attribute ram_addr_end : integer;
  attribute ram_addr_end of ram_reg_0 : label is 2047;
  attribute ram_offset : integer;
  attribute ram_offset of ram_reg_0 : label is 0;
  attribute ram_slice_begin : integer;
  attribute ram_slice_begin of ram_reg_0 : label is 0;
  attribute ram_slice_end : integer;
  attribute ram_slice_end of ram_reg_0 : label is 17;
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of ram_reg_1 : label is "p0_d6";
  attribute METHODOLOGY_DRC_VIOS of ram_reg_1 : label is "{SYNTH-6 {cell *THIS*}}";
  attribute RTL_RAM_BITS of ram_reg_1 : label is 46080;
  attribute RTL_RAM_NAME of ram_reg_1 : label is "ram";
  attribute bram_addr_begin of ram_reg_1 : label is 0;
  attribute bram_addr_end of ram_reg_1 : label is 2047;
  attribute bram_slice_begin of ram_reg_1 : label is 18;
  attribute bram_slice_end of ram_reg_1 : label is 23;
  attribute ram_addr_begin of ram_reg_1 : label is 0;
  attribute ram_addr_end of ram_reg_1 : label is 2047;
  attribute ram_offset of ram_reg_1 : label is 0;
  attribute ram_slice_begin of ram_reg_1 : label is 18;
  attribute ram_slice_end of ram_reg_1 : label is 23;
begin
ram_reg_0: unisim.vcomponents.RAMB36E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      EN_ECC_READ => false,
      EN_ECC_WRITE => false,
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      RAM_EXTENSION_A => "NONE",
      RAM_EXTENSION_B => "NONE",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "PERFORMANCE",
      READ_WIDTH_A => 18,
      READ_WIDTH_B => 0,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "READ_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 18,
      WRITE_WIDTH_B => 0
    )
        port map (
      ADDRARDADDR(15) => '1',
      ADDRARDADDR(14 downto 4) => addr(10 downto 0),
      ADDRARDADDR(3 downto 0) => B"1111",
      ADDRBWRADDR(15 downto 0) => B"1111111111111111",
      CASCADEINA => '1',
      CASCADEINB => '0',
      CASCADEOUTA => NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED,
      CASCADEOUTB => NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED,
      CLKARDCLK => m00_axis_aclk,
      CLKBWRCLK => '0',
      DBITERR => NLW_ram_reg_0_DBITERR_UNCONNECTED,
      DIADI(31 downto 16) => B"0000000000000000",
      DIADI(15 downto 0) => s00_axis_tdata(15 downto 0),
      DIBDI(31 downto 0) => B"11111111111111111111111111111111",
      DIPADIP(3 downto 2) => B"00",
      DIPADIP(1 downto 0) => s00_axis_tdata(17 downto 16),
      DIPBDIP(3 downto 0) => B"1111",
      DOADO(31 downto 16) => NLW_ram_reg_0_DOADO_UNCONNECTED(31 downto 16),
      DOADO(15 downto 0) => dout0_out(15 downto 0),
      DOBDO(31 downto 0) => NLW_ram_reg_0_DOBDO_UNCONNECTED(31 downto 0),
      DOPADOP(3 downto 2) => NLW_ram_reg_0_DOPADOP_UNCONNECTED(3 downto 2),
      DOPADOP(1 downto 0) => dout0_out(17 downto 16),
      DOPBDOP(3 downto 0) => NLW_ram_reg_0_DOPBDOP_UNCONNECTED(3 downto 0),
      ECCPARITY(7 downto 0) => NLW_ram_reg_0_ECCPARITY_UNCONNECTED(7 downto 0),
      ENARDEN => Q(0),
      ENBWREN => '0',
      INJECTDBITERR => NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED,
      INJECTSBITERR => NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED,
      RDADDRECC(8 downto 0) => NLW_ram_reg_0_RDADDRECC_UNCONNECTED(8 downto 0),
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      SBITERR => NLW_ram_reg_0_SBITERR_UNCONNECTED,
      WEA(3) => \ram_reg_0_i_1__0_n_0\,
      WEA(2) => \ram_reg_0_i_1__0_n_0\,
      WEA(1) => \ram_reg_0_i_1__0_n_0\,
      WEA(0) => \ram_reg_0_i_1__0_n_0\,
      WEBWE(7 downto 0) => B"00000000"
    );
\ram_reg_0_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => Q(0),
      I1 => s00_axis_tuser(0),
      I2 => s00_axis_tvalid,
      O => \ram_reg_0_i_1__0_n_0\
    );
ram_reg_1: unisim.vcomponents.RAMB18E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      INIT_A => X"00000",
      INIT_B => X"00000",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "PERFORMANCE",
      READ_WIDTH_A => 9,
      READ_WIDTH_B => 0,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"00000",
      SRVAL_B => X"00000",
      WRITE_MODE_A => "READ_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 9,
      WRITE_WIDTH_B => 0
    )
        port map (
      ADDRARDADDR(13 downto 3) => addr(10 downto 0),
      ADDRARDADDR(2 downto 0) => B"111",
      ADDRBWRADDR(13 downto 0) => B"11111111111111",
      CLKARDCLK => m00_axis_aclk,
      CLKBWRCLK => '0',
      DIADI(15 downto 6) => B"0000000000",
      DIADI(5 downto 0) => s00_axis_tdata(23 downto 18),
      DIBDI(15 downto 0) => B"1111111111111111",
      DIPADIP(1 downto 0) => B"00",
      DIPBDIP(1 downto 0) => B"11",
      DOADO(15 downto 6) => NLW_ram_reg_1_DOADO_UNCONNECTED(15 downto 6),
      DOADO(5 downto 0) => dout0_out(23 downto 18),
      DOBDO(15 downto 0) => NLW_ram_reg_1_DOBDO_UNCONNECTED(15 downto 0),
      DOPADOP(1 downto 0) => NLW_ram_reg_1_DOPADOP_UNCONNECTED(1 downto 0),
      DOPBDOP(1 downto 0) => NLW_ram_reg_1_DOPBDOP_UNCONNECTED(1 downto 0),
      ENARDEN => Q(0),
      ENBWREN => '0',
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      WEA(1) => \ram_reg_0_i_1__0_n_0\,
      WEA(0) => \ram_reg_0_i_1__0_n_0\,
      WEBWE(3 downto 0) => B"0000"
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity test_bd_pixelReOrder_0_0_rams_sp_rf_rst_15 is
  port (
    dout0_out : out STD_LOGIC_VECTOR ( 23 downto 0 );
    m00_axis_aclk : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 0 to 0 );
    addr : in STD_LOGIC_VECTOR ( 10 downto 0 );
    s00_axis_tdata : in STD_LOGIC_VECTOR ( 23 downto 0 );
    s00_axis_tuser : in STD_LOGIC_VECTOR ( 0 to 0 );
    s00_axis_tvalid : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of test_bd_pixelReOrder_0_0_rams_sp_rf_rst_15 : entity is "rams_sp_rf_rst";
end test_bd_pixelReOrder_0_0_rams_sp_rf_rst_15;

architecture STRUCTURE of test_bd_pixelReOrder_0_0_rams_sp_rf_rst_15 is
  signal \ram_reg_0_i_1__1_n_0\ : STD_LOGIC;
  signal NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_DBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_SBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_DOADO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 16 );
  signal NLW_ram_reg_0_DOBDO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_ram_reg_0_DOPADOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal NLW_ram_reg_0_DOPBDOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_ram_reg_0_ECCPARITY_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal NLW_ram_reg_0_RDADDRECC_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_ram_reg_1_DOADO_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 6 );
  signal NLW_ram_reg_1_DOBDO_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal NLW_ram_reg_1_DOPADOP_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_ram_reg_1_DOPBDOP_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ : string;
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of ram_reg_0 : label is "p2_d16";
  attribute METHODOLOGY_DRC_VIOS : string;
  attribute METHODOLOGY_DRC_VIOS of ram_reg_0 : label is "{SYNTH-6 {cell *THIS*}}";
  attribute RTL_RAM_BITS : integer;
  attribute RTL_RAM_BITS of ram_reg_0 : label is 46080;
  attribute RTL_RAM_NAME : string;
  attribute RTL_RAM_NAME of ram_reg_0 : label is "ram";
  attribute bram_addr_begin : integer;
  attribute bram_addr_begin of ram_reg_0 : label is 0;
  attribute bram_addr_end : integer;
  attribute bram_addr_end of ram_reg_0 : label is 2047;
  attribute bram_slice_begin : integer;
  attribute bram_slice_begin of ram_reg_0 : label is 0;
  attribute bram_slice_end : integer;
  attribute bram_slice_end of ram_reg_0 : label is 17;
  attribute ram_addr_begin : integer;
  attribute ram_addr_begin of ram_reg_0 : label is 0;
  attribute ram_addr_end : integer;
  attribute ram_addr_end of ram_reg_0 : label is 2047;
  attribute ram_offset : integer;
  attribute ram_offset of ram_reg_0 : label is 0;
  attribute ram_slice_begin : integer;
  attribute ram_slice_begin of ram_reg_0 : label is 0;
  attribute ram_slice_end : integer;
  attribute ram_slice_end of ram_reg_0 : label is 17;
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of ram_reg_1 : label is "p0_d6";
  attribute METHODOLOGY_DRC_VIOS of ram_reg_1 : label is "{SYNTH-6 {cell *THIS*}}";
  attribute RTL_RAM_BITS of ram_reg_1 : label is 46080;
  attribute RTL_RAM_NAME of ram_reg_1 : label is "ram";
  attribute bram_addr_begin of ram_reg_1 : label is 0;
  attribute bram_addr_end of ram_reg_1 : label is 2047;
  attribute bram_slice_begin of ram_reg_1 : label is 18;
  attribute bram_slice_end of ram_reg_1 : label is 23;
  attribute ram_addr_begin of ram_reg_1 : label is 0;
  attribute ram_addr_end of ram_reg_1 : label is 2047;
  attribute ram_offset of ram_reg_1 : label is 0;
  attribute ram_slice_begin of ram_reg_1 : label is 18;
  attribute ram_slice_end of ram_reg_1 : label is 23;
begin
ram_reg_0: unisim.vcomponents.RAMB36E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      EN_ECC_READ => false,
      EN_ECC_WRITE => false,
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      RAM_EXTENSION_A => "NONE",
      RAM_EXTENSION_B => "NONE",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "PERFORMANCE",
      READ_WIDTH_A => 18,
      READ_WIDTH_B => 0,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "READ_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 18,
      WRITE_WIDTH_B => 0
    )
        port map (
      ADDRARDADDR(15) => '1',
      ADDRARDADDR(14 downto 4) => addr(10 downto 0),
      ADDRARDADDR(3 downto 0) => B"1111",
      ADDRBWRADDR(15 downto 0) => B"1111111111111111",
      CASCADEINA => '1',
      CASCADEINB => '0',
      CASCADEOUTA => NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED,
      CASCADEOUTB => NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED,
      CLKARDCLK => m00_axis_aclk,
      CLKBWRCLK => '0',
      DBITERR => NLW_ram_reg_0_DBITERR_UNCONNECTED,
      DIADI(31 downto 16) => B"0000000000000000",
      DIADI(15 downto 0) => s00_axis_tdata(15 downto 0),
      DIBDI(31 downto 0) => B"11111111111111111111111111111111",
      DIPADIP(3 downto 2) => B"00",
      DIPADIP(1 downto 0) => s00_axis_tdata(17 downto 16),
      DIPBDIP(3 downto 0) => B"1111",
      DOADO(31 downto 16) => NLW_ram_reg_0_DOADO_UNCONNECTED(31 downto 16),
      DOADO(15 downto 0) => dout0_out(15 downto 0),
      DOBDO(31 downto 0) => NLW_ram_reg_0_DOBDO_UNCONNECTED(31 downto 0),
      DOPADOP(3 downto 2) => NLW_ram_reg_0_DOPADOP_UNCONNECTED(3 downto 2),
      DOPADOP(1 downto 0) => dout0_out(17 downto 16),
      DOPBDOP(3 downto 0) => NLW_ram_reg_0_DOPBDOP_UNCONNECTED(3 downto 0),
      ECCPARITY(7 downto 0) => NLW_ram_reg_0_ECCPARITY_UNCONNECTED(7 downto 0),
      ENARDEN => Q(0),
      ENBWREN => '0',
      INJECTDBITERR => NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED,
      INJECTSBITERR => NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED,
      RDADDRECC(8 downto 0) => NLW_ram_reg_0_RDADDRECC_UNCONNECTED(8 downto 0),
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      SBITERR => NLW_ram_reg_0_SBITERR_UNCONNECTED,
      WEA(3) => \ram_reg_0_i_1__1_n_0\,
      WEA(2) => \ram_reg_0_i_1__1_n_0\,
      WEA(1) => \ram_reg_0_i_1__1_n_0\,
      WEA(0) => \ram_reg_0_i_1__1_n_0\,
      WEBWE(7 downto 0) => B"00000000"
    );
\ram_reg_0_i_1__1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => Q(0),
      I1 => s00_axis_tuser(0),
      I2 => s00_axis_tvalid,
      O => \ram_reg_0_i_1__1_n_0\
    );
ram_reg_1: unisim.vcomponents.RAMB18E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      INIT_A => X"00000",
      INIT_B => X"00000",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "PERFORMANCE",
      READ_WIDTH_A => 9,
      READ_WIDTH_B => 0,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"00000",
      SRVAL_B => X"00000",
      WRITE_MODE_A => "READ_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 9,
      WRITE_WIDTH_B => 0
    )
        port map (
      ADDRARDADDR(13 downto 3) => addr(10 downto 0),
      ADDRARDADDR(2 downto 0) => B"111",
      ADDRBWRADDR(13 downto 0) => B"11111111111111",
      CLKARDCLK => m00_axis_aclk,
      CLKBWRCLK => '0',
      DIADI(15 downto 6) => B"0000000000",
      DIADI(5 downto 0) => s00_axis_tdata(23 downto 18),
      DIBDI(15 downto 0) => B"1111111111111111",
      DIPADIP(1 downto 0) => B"00",
      DIPBDIP(1 downto 0) => B"11",
      DOADO(15 downto 6) => NLW_ram_reg_1_DOADO_UNCONNECTED(15 downto 6),
      DOADO(5 downto 0) => dout0_out(23 downto 18),
      DOBDO(15 downto 0) => NLW_ram_reg_1_DOBDO_UNCONNECTED(15 downto 0),
      DOPADOP(1 downto 0) => NLW_ram_reg_1_DOPADOP_UNCONNECTED(1 downto 0),
      DOPBDOP(1 downto 0) => NLW_ram_reg_1_DOPBDOP_UNCONNECTED(1 downto 0),
      ENARDEN => Q(0),
      ENBWREN => '0',
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      WEA(1) => \ram_reg_0_i_1__1_n_0\,
      WEA(0) => \ram_reg_0_i_1__1_n_0\,
      WEBWE(3 downto 0) => B"0000"
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity test_bd_pixelReOrder_0_0_rams_sp_rf_rst_16 is
  port (
    dout0_out : out STD_LOGIC_VECTOR ( 23 downto 0 );
    m00_axis_aclk : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 0 to 0 );
    addr : in STD_LOGIC_VECTOR ( 10 downto 0 );
    s00_axis_tdata : in STD_LOGIC_VECTOR ( 23 downto 0 );
    s00_axis_tuser : in STD_LOGIC_VECTOR ( 0 to 0 );
    s00_axis_tvalid : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of test_bd_pixelReOrder_0_0_rams_sp_rf_rst_16 : entity is "rams_sp_rf_rst";
end test_bd_pixelReOrder_0_0_rams_sp_rf_rst_16;

architecture STRUCTURE of test_bd_pixelReOrder_0_0_rams_sp_rf_rst_16 is
  signal \ram_reg_0_i_1__2_n_0\ : STD_LOGIC;
  signal NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_DBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_SBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_DOADO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 16 );
  signal NLW_ram_reg_0_DOBDO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_ram_reg_0_DOPADOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal NLW_ram_reg_0_DOPBDOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_ram_reg_0_ECCPARITY_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal NLW_ram_reg_0_RDADDRECC_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_ram_reg_1_DOADO_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 6 );
  signal NLW_ram_reg_1_DOBDO_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal NLW_ram_reg_1_DOPADOP_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_ram_reg_1_DOPBDOP_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ : string;
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of ram_reg_0 : label is "p2_d16";
  attribute METHODOLOGY_DRC_VIOS : string;
  attribute METHODOLOGY_DRC_VIOS of ram_reg_0 : label is "{SYNTH-6 {cell *THIS*}}";
  attribute RTL_RAM_BITS : integer;
  attribute RTL_RAM_BITS of ram_reg_0 : label is 46080;
  attribute RTL_RAM_NAME : string;
  attribute RTL_RAM_NAME of ram_reg_0 : label is "ram";
  attribute bram_addr_begin : integer;
  attribute bram_addr_begin of ram_reg_0 : label is 0;
  attribute bram_addr_end : integer;
  attribute bram_addr_end of ram_reg_0 : label is 2047;
  attribute bram_slice_begin : integer;
  attribute bram_slice_begin of ram_reg_0 : label is 0;
  attribute bram_slice_end : integer;
  attribute bram_slice_end of ram_reg_0 : label is 17;
  attribute ram_addr_begin : integer;
  attribute ram_addr_begin of ram_reg_0 : label is 0;
  attribute ram_addr_end : integer;
  attribute ram_addr_end of ram_reg_0 : label is 2047;
  attribute ram_offset : integer;
  attribute ram_offset of ram_reg_0 : label is 0;
  attribute ram_slice_begin : integer;
  attribute ram_slice_begin of ram_reg_0 : label is 0;
  attribute ram_slice_end : integer;
  attribute ram_slice_end of ram_reg_0 : label is 17;
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of ram_reg_1 : label is "p0_d6";
  attribute METHODOLOGY_DRC_VIOS of ram_reg_1 : label is "{SYNTH-6 {cell *THIS*}}";
  attribute RTL_RAM_BITS of ram_reg_1 : label is 46080;
  attribute RTL_RAM_NAME of ram_reg_1 : label is "ram";
  attribute bram_addr_begin of ram_reg_1 : label is 0;
  attribute bram_addr_end of ram_reg_1 : label is 2047;
  attribute bram_slice_begin of ram_reg_1 : label is 18;
  attribute bram_slice_end of ram_reg_1 : label is 23;
  attribute ram_addr_begin of ram_reg_1 : label is 0;
  attribute ram_addr_end of ram_reg_1 : label is 2047;
  attribute ram_offset of ram_reg_1 : label is 0;
  attribute ram_slice_begin of ram_reg_1 : label is 18;
  attribute ram_slice_end of ram_reg_1 : label is 23;
begin
ram_reg_0: unisim.vcomponents.RAMB36E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      EN_ECC_READ => false,
      EN_ECC_WRITE => false,
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      RAM_EXTENSION_A => "NONE",
      RAM_EXTENSION_B => "NONE",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "PERFORMANCE",
      READ_WIDTH_A => 18,
      READ_WIDTH_B => 0,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "READ_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 18,
      WRITE_WIDTH_B => 0
    )
        port map (
      ADDRARDADDR(15) => '1',
      ADDRARDADDR(14 downto 4) => addr(10 downto 0),
      ADDRARDADDR(3 downto 0) => B"1111",
      ADDRBWRADDR(15 downto 0) => B"1111111111111111",
      CASCADEINA => '1',
      CASCADEINB => '0',
      CASCADEOUTA => NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED,
      CASCADEOUTB => NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED,
      CLKARDCLK => m00_axis_aclk,
      CLKBWRCLK => '0',
      DBITERR => NLW_ram_reg_0_DBITERR_UNCONNECTED,
      DIADI(31 downto 16) => B"0000000000000000",
      DIADI(15 downto 0) => s00_axis_tdata(15 downto 0),
      DIBDI(31 downto 0) => B"11111111111111111111111111111111",
      DIPADIP(3 downto 2) => B"00",
      DIPADIP(1 downto 0) => s00_axis_tdata(17 downto 16),
      DIPBDIP(3 downto 0) => B"1111",
      DOADO(31 downto 16) => NLW_ram_reg_0_DOADO_UNCONNECTED(31 downto 16),
      DOADO(15 downto 0) => dout0_out(15 downto 0),
      DOBDO(31 downto 0) => NLW_ram_reg_0_DOBDO_UNCONNECTED(31 downto 0),
      DOPADOP(3 downto 2) => NLW_ram_reg_0_DOPADOP_UNCONNECTED(3 downto 2),
      DOPADOP(1 downto 0) => dout0_out(17 downto 16),
      DOPBDOP(3 downto 0) => NLW_ram_reg_0_DOPBDOP_UNCONNECTED(3 downto 0),
      ECCPARITY(7 downto 0) => NLW_ram_reg_0_ECCPARITY_UNCONNECTED(7 downto 0),
      ENARDEN => Q(0),
      ENBWREN => '0',
      INJECTDBITERR => NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED,
      INJECTSBITERR => NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED,
      RDADDRECC(8 downto 0) => NLW_ram_reg_0_RDADDRECC_UNCONNECTED(8 downto 0),
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      SBITERR => NLW_ram_reg_0_SBITERR_UNCONNECTED,
      WEA(3) => \ram_reg_0_i_1__2_n_0\,
      WEA(2) => \ram_reg_0_i_1__2_n_0\,
      WEA(1) => \ram_reg_0_i_1__2_n_0\,
      WEA(0) => \ram_reg_0_i_1__2_n_0\,
      WEBWE(7 downto 0) => B"00000000"
    );
\ram_reg_0_i_1__2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => Q(0),
      I1 => s00_axis_tuser(0),
      I2 => s00_axis_tvalid,
      O => \ram_reg_0_i_1__2_n_0\
    );
ram_reg_1: unisim.vcomponents.RAMB18E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      INIT_A => X"00000",
      INIT_B => X"00000",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "PERFORMANCE",
      READ_WIDTH_A => 9,
      READ_WIDTH_B => 0,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"00000",
      SRVAL_B => X"00000",
      WRITE_MODE_A => "READ_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 9,
      WRITE_WIDTH_B => 0
    )
        port map (
      ADDRARDADDR(13 downto 3) => addr(10 downto 0),
      ADDRARDADDR(2 downto 0) => B"111",
      ADDRBWRADDR(13 downto 0) => B"11111111111111",
      CLKARDCLK => m00_axis_aclk,
      CLKBWRCLK => '0',
      DIADI(15 downto 6) => B"0000000000",
      DIADI(5 downto 0) => s00_axis_tdata(23 downto 18),
      DIBDI(15 downto 0) => B"1111111111111111",
      DIPADIP(1 downto 0) => B"00",
      DIPBDIP(1 downto 0) => B"11",
      DOADO(15 downto 6) => NLW_ram_reg_1_DOADO_UNCONNECTED(15 downto 6),
      DOADO(5 downto 0) => dout0_out(23 downto 18),
      DOBDO(15 downto 0) => NLW_ram_reg_1_DOBDO_UNCONNECTED(15 downto 0),
      DOPADOP(1 downto 0) => NLW_ram_reg_1_DOPADOP_UNCONNECTED(1 downto 0),
      DOPBDOP(1 downto 0) => NLW_ram_reg_1_DOPBDOP_UNCONNECTED(1 downto 0),
      ENARDEN => Q(0),
      ENBWREN => '0',
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      WEA(1) => \ram_reg_0_i_1__2_n_0\,
      WEA(0) => \ram_reg_0_i_1__2_n_0\,
      WEBWE(3 downto 0) => B"0000"
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity test_bd_pixelReOrder_0_0_rams_sp_rf_rst_17 is
  port (
    D : out STD_LOGIC_VECTOR ( 23 downto 0 );
    ctrl_mux_out_1 : in STD_LOGIC_VECTOR ( 4 downto 0 );
    \y_reg[0]\ : in STD_LOGIC;
    \y_reg[0]_0\ : in STD_LOGIC;
    \y_reg[1]\ : in STD_LOGIC;
    \y_reg[1]_0\ : in STD_LOGIC;
    \y_reg[2]\ : in STD_LOGIC;
    \y_reg[2]_0\ : in STD_LOGIC;
    \y_reg[3]\ : in STD_LOGIC;
    \y_reg[3]_0\ : in STD_LOGIC;
    \y_reg[4]\ : in STD_LOGIC;
    \y_reg[4]_0\ : in STD_LOGIC;
    \y_reg[5]\ : in STD_LOGIC;
    \y_reg[5]_0\ : in STD_LOGIC;
    \y_reg[6]\ : in STD_LOGIC;
    \y_reg[6]_0\ : in STD_LOGIC;
    \y_reg[7]\ : in STD_LOGIC;
    \y_reg[7]_0\ : in STD_LOGIC;
    \y_reg[8]\ : in STD_LOGIC;
    \y_reg[8]_0\ : in STD_LOGIC;
    \y_reg[9]\ : in STD_LOGIC;
    \y_reg[9]_0\ : in STD_LOGIC;
    \y_reg[10]\ : in STD_LOGIC;
    \y_reg[10]_0\ : in STD_LOGIC;
    \y_reg[11]\ : in STD_LOGIC;
    \y_reg[11]_0\ : in STD_LOGIC;
    \y_reg[12]\ : in STD_LOGIC;
    \y_reg[12]_0\ : in STD_LOGIC;
    \y_reg[13]\ : in STD_LOGIC;
    \y_reg[13]_0\ : in STD_LOGIC;
    \y_reg[14]\ : in STD_LOGIC;
    \y_reg[14]_0\ : in STD_LOGIC;
    \y_reg[15]\ : in STD_LOGIC;
    \y_reg[15]_0\ : in STD_LOGIC;
    \y_reg[16]\ : in STD_LOGIC;
    \y_reg[16]_0\ : in STD_LOGIC;
    \y_reg[17]\ : in STD_LOGIC;
    \y_reg[17]_0\ : in STD_LOGIC;
    \y_reg[18]\ : in STD_LOGIC;
    \y_reg[18]_0\ : in STD_LOGIC;
    \y_reg[19]\ : in STD_LOGIC;
    \y_reg[19]_0\ : in STD_LOGIC;
    \y_reg[20]\ : in STD_LOGIC;
    \y_reg[20]_0\ : in STD_LOGIC;
    \y_reg[21]\ : in STD_LOGIC;
    \y_reg[21]_0\ : in STD_LOGIC;
    \y_reg[22]\ : in STD_LOGIC;
    \y_reg[22]_0\ : in STD_LOGIC;
    \y_reg[23]\ : in STD_LOGIC;
    \y_reg[23]_0\ : in STD_LOGIC;
    \y_reg[0]_i_1_0\ : in STD_LOGIC;
    dout0_out : in STD_LOGIC_VECTOR ( 23 downto 0 );
    \y_reg[23]_i_3_0\ : in STD_LOGIC_VECTOR ( 23 downto 0 );
    \y_reg[23]_i_3_1\ : in STD_LOGIC_VECTOR ( 23 downto 0 );
    \y_reg[1]_i_1_0\ : in STD_LOGIC;
    \y_reg[2]_i_1_0\ : in STD_LOGIC;
    \y_reg[3]_i_1_0\ : in STD_LOGIC;
    \y_reg[4]_i_1_0\ : in STD_LOGIC;
    \y_reg[5]_i_1_0\ : in STD_LOGIC;
    \y_reg[6]_i_1_0\ : in STD_LOGIC;
    \y_reg[7]_i_1_0\ : in STD_LOGIC;
    \y_reg[8]_i_1_0\ : in STD_LOGIC;
    \y_reg[9]_i_1_0\ : in STD_LOGIC;
    \y_reg[10]_i_1_0\ : in STD_LOGIC;
    \y_reg[11]_i_1_0\ : in STD_LOGIC;
    \y_reg[12]_i_1_0\ : in STD_LOGIC;
    \y_reg[13]_i_1_0\ : in STD_LOGIC;
    \y_reg[14]_i_1_0\ : in STD_LOGIC;
    \y_reg[15]_i_1_0\ : in STD_LOGIC;
    \y_reg[16]_i_1_0\ : in STD_LOGIC;
    \y_reg[17]_i_1_0\ : in STD_LOGIC;
    \y_reg[18]_i_1_0\ : in STD_LOGIC;
    \y_reg[19]_i_1_0\ : in STD_LOGIC;
    \y_reg[20]_i_1_0\ : in STD_LOGIC;
    \y_reg[21]_i_1_0\ : in STD_LOGIC;
    \y_reg[22]_i_1_0\ : in STD_LOGIC;
    \y_reg[23]_i_1_0\ : in STD_LOGIC;
    m00_axis_aclk : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 0 to 0 );
    addr : in STD_LOGIC_VECTOR ( 10 downto 0 );
    s00_axis_tdata : in STD_LOGIC_VECTOR ( 23 downto 0 );
    s00_axis_tuser : in STD_LOGIC_VECTOR ( 0 to 0 );
    s00_axis_tvalid : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of test_bd_pixelReOrder_0_0_rams_sp_rf_rst_17 : entity is "rams_sp_rf_rst";
end test_bd_pixelReOrder_0_0_rams_sp_rf_rst_17;

architecture STRUCTURE of test_bd_pixelReOrder_0_0_rams_sp_rf_rst_17 is
  signal \_DOUT_1[3]_6\ : STD_LOGIC_VECTOR ( 23 downto 0 );
  signal \ram_reg_0_i_1__3_n_0\ : STD_LOGIC;
  signal \y_reg[0]_i_2_n_0\ : STD_LOGIC;
  signal \y_reg[0]_i_5_n_0\ : STD_LOGIC;
  signal \y_reg[10]_i_2_n_0\ : STD_LOGIC;
  signal \y_reg[10]_i_5_n_0\ : STD_LOGIC;
  signal \y_reg[11]_i_2_n_0\ : STD_LOGIC;
  signal \y_reg[11]_i_5_n_0\ : STD_LOGIC;
  signal \y_reg[12]_i_2_n_0\ : STD_LOGIC;
  signal \y_reg[12]_i_5_n_0\ : STD_LOGIC;
  signal \y_reg[13]_i_2_n_0\ : STD_LOGIC;
  signal \y_reg[13]_i_5_n_0\ : STD_LOGIC;
  signal \y_reg[14]_i_2_n_0\ : STD_LOGIC;
  signal \y_reg[14]_i_5_n_0\ : STD_LOGIC;
  signal \y_reg[15]_i_2_n_0\ : STD_LOGIC;
  signal \y_reg[15]_i_5_n_0\ : STD_LOGIC;
  signal \y_reg[16]_i_2_n_0\ : STD_LOGIC;
  signal \y_reg[16]_i_5_n_0\ : STD_LOGIC;
  signal \y_reg[17]_i_2_n_0\ : STD_LOGIC;
  signal \y_reg[17]_i_5_n_0\ : STD_LOGIC;
  signal \y_reg[18]_i_2_n_0\ : STD_LOGIC;
  signal \y_reg[18]_i_5_n_0\ : STD_LOGIC;
  signal \y_reg[19]_i_2_n_0\ : STD_LOGIC;
  signal \y_reg[19]_i_5_n_0\ : STD_LOGIC;
  signal \y_reg[1]_i_2_n_0\ : STD_LOGIC;
  signal \y_reg[1]_i_5_n_0\ : STD_LOGIC;
  signal \y_reg[20]_i_2_n_0\ : STD_LOGIC;
  signal \y_reg[20]_i_5_n_0\ : STD_LOGIC;
  signal \y_reg[21]_i_2_n_0\ : STD_LOGIC;
  signal \y_reg[21]_i_5_n_0\ : STD_LOGIC;
  signal \y_reg[22]_i_2_n_0\ : STD_LOGIC;
  signal \y_reg[22]_i_5_n_0\ : STD_LOGIC;
  signal \y_reg[23]_i_3_n_0\ : STD_LOGIC;
  signal \y_reg[23]_i_6_n_0\ : STD_LOGIC;
  signal \y_reg[2]_i_2_n_0\ : STD_LOGIC;
  signal \y_reg[2]_i_5_n_0\ : STD_LOGIC;
  signal \y_reg[3]_i_2_n_0\ : STD_LOGIC;
  signal \y_reg[3]_i_5_n_0\ : STD_LOGIC;
  signal \y_reg[4]_i_2_n_0\ : STD_LOGIC;
  signal \y_reg[4]_i_5_n_0\ : STD_LOGIC;
  signal \y_reg[5]_i_2_n_0\ : STD_LOGIC;
  signal \y_reg[5]_i_5_n_0\ : STD_LOGIC;
  signal \y_reg[6]_i_2_n_0\ : STD_LOGIC;
  signal \y_reg[6]_i_5_n_0\ : STD_LOGIC;
  signal \y_reg[7]_i_2_n_0\ : STD_LOGIC;
  signal \y_reg[7]_i_5_n_0\ : STD_LOGIC;
  signal \y_reg[8]_i_2_n_0\ : STD_LOGIC;
  signal \y_reg[8]_i_5_n_0\ : STD_LOGIC;
  signal \y_reg[9]_i_2_n_0\ : STD_LOGIC;
  signal \y_reg[9]_i_5_n_0\ : STD_LOGIC;
  signal NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_DBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_SBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_DOADO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 16 );
  signal NLW_ram_reg_0_DOBDO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_ram_reg_0_DOPADOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal NLW_ram_reg_0_DOPBDOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_ram_reg_0_ECCPARITY_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal NLW_ram_reg_0_RDADDRECC_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_ram_reg_1_DOADO_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 6 );
  signal NLW_ram_reg_1_DOBDO_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal NLW_ram_reg_1_DOPADOP_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_ram_reg_1_DOPBDOP_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ : string;
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of ram_reg_0 : label is "p2_d16";
  attribute METHODOLOGY_DRC_VIOS : string;
  attribute METHODOLOGY_DRC_VIOS of ram_reg_0 : label is "{SYNTH-6 {cell *THIS*}}";
  attribute RTL_RAM_BITS : integer;
  attribute RTL_RAM_BITS of ram_reg_0 : label is 46080;
  attribute RTL_RAM_NAME : string;
  attribute RTL_RAM_NAME of ram_reg_0 : label is "ram";
  attribute bram_addr_begin : integer;
  attribute bram_addr_begin of ram_reg_0 : label is 0;
  attribute bram_addr_end : integer;
  attribute bram_addr_end of ram_reg_0 : label is 2047;
  attribute bram_slice_begin : integer;
  attribute bram_slice_begin of ram_reg_0 : label is 0;
  attribute bram_slice_end : integer;
  attribute bram_slice_end of ram_reg_0 : label is 17;
  attribute ram_addr_begin : integer;
  attribute ram_addr_begin of ram_reg_0 : label is 0;
  attribute ram_addr_end : integer;
  attribute ram_addr_end of ram_reg_0 : label is 2047;
  attribute ram_offset : integer;
  attribute ram_offset of ram_reg_0 : label is 0;
  attribute ram_slice_begin : integer;
  attribute ram_slice_begin of ram_reg_0 : label is 0;
  attribute ram_slice_end : integer;
  attribute ram_slice_end of ram_reg_0 : label is 17;
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of ram_reg_1 : label is "p0_d6";
  attribute METHODOLOGY_DRC_VIOS of ram_reg_1 : label is "{SYNTH-6 {cell *THIS*}}";
  attribute RTL_RAM_BITS of ram_reg_1 : label is 46080;
  attribute RTL_RAM_NAME of ram_reg_1 : label is "ram";
  attribute bram_addr_begin of ram_reg_1 : label is 0;
  attribute bram_addr_end of ram_reg_1 : label is 2047;
  attribute bram_slice_begin of ram_reg_1 : label is 18;
  attribute bram_slice_end of ram_reg_1 : label is 23;
  attribute ram_addr_begin of ram_reg_1 : label is 0;
  attribute ram_addr_end of ram_reg_1 : label is 2047;
  attribute ram_offset of ram_reg_1 : label is 0;
  attribute ram_slice_begin of ram_reg_1 : label is 18;
  attribute ram_slice_end of ram_reg_1 : label is 23;
begin
ram_reg_0: unisim.vcomponents.RAMB36E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      EN_ECC_READ => false,
      EN_ECC_WRITE => false,
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      RAM_EXTENSION_A => "NONE",
      RAM_EXTENSION_B => "NONE",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "PERFORMANCE",
      READ_WIDTH_A => 18,
      READ_WIDTH_B => 0,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "READ_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 18,
      WRITE_WIDTH_B => 0
    )
        port map (
      ADDRARDADDR(15) => '1',
      ADDRARDADDR(14 downto 4) => addr(10 downto 0),
      ADDRARDADDR(3 downto 0) => B"1111",
      ADDRBWRADDR(15 downto 0) => B"1111111111111111",
      CASCADEINA => '1',
      CASCADEINB => '0',
      CASCADEOUTA => NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED,
      CASCADEOUTB => NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED,
      CLKARDCLK => m00_axis_aclk,
      CLKBWRCLK => '0',
      DBITERR => NLW_ram_reg_0_DBITERR_UNCONNECTED,
      DIADI(31 downto 16) => B"0000000000000000",
      DIADI(15 downto 0) => s00_axis_tdata(15 downto 0),
      DIBDI(31 downto 0) => B"11111111111111111111111111111111",
      DIPADIP(3 downto 2) => B"00",
      DIPADIP(1 downto 0) => s00_axis_tdata(17 downto 16),
      DIPBDIP(3 downto 0) => B"1111",
      DOADO(31 downto 16) => NLW_ram_reg_0_DOADO_UNCONNECTED(31 downto 16),
      DOADO(15 downto 0) => \_DOUT_1[3]_6\(15 downto 0),
      DOBDO(31 downto 0) => NLW_ram_reg_0_DOBDO_UNCONNECTED(31 downto 0),
      DOPADOP(3 downto 2) => NLW_ram_reg_0_DOPADOP_UNCONNECTED(3 downto 2),
      DOPADOP(1 downto 0) => \_DOUT_1[3]_6\(17 downto 16),
      DOPBDOP(3 downto 0) => NLW_ram_reg_0_DOPBDOP_UNCONNECTED(3 downto 0),
      ECCPARITY(7 downto 0) => NLW_ram_reg_0_ECCPARITY_UNCONNECTED(7 downto 0),
      ENARDEN => Q(0),
      ENBWREN => '0',
      INJECTDBITERR => NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED,
      INJECTSBITERR => NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED,
      RDADDRECC(8 downto 0) => NLW_ram_reg_0_RDADDRECC_UNCONNECTED(8 downto 0),
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      SBITERR => NLW_ram_reg_0_SBITERR_UNCONNECTED,
      WEA(3) => \ram_reg_0_i_1__3_n_0\,
      WEA(2) => \ram_reg_0_i_1__3_n_0\,
      WEA(1) => \ram_reg_0_i_1__3_n_0\,
      WEA(0) => \ram_reg_0_i_1__3_n_0\,
      WEBWE(7 downto 0) => B"00000000"
    );
\ram_reg_0_i_1__3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => Q(0),
      I1 => s00_axis_tuser(0),
      I2 => s00_axis_tvalid,
      O => \ram_reg_0_i_1__3_n_0\
    );
ram_reg_1: unisim.vcomponents.RAMB18E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      INIT_A => X"00000",
      INIT_B => X"00000",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "PERFORMANCE",
      READ_WIDTH_A => 9,
      READ_WIDTH_B => 0,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"00000",
      SRVAL_B => X"00000",
      WRITE_MODE_A => "READ_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 9,
      WRITE_WIDTH_B => 0
    )
        port map (
      ADDRARDADDR(13 downto 3) => addr(10 downto 0),
      ADDRARDADDR(2 downto 0) => B"111",
      ADDRBWRADDR(13 downto 0) => B"11111111111111",
      CLKARDCLK => m00_axis_aclk,
      CLKBWRCLK => '0',
      DIADI(15 downto 6) => B"0000000000",
      DIADI(5 downto 0) => s00_axis_tdata(23 downto 18),
      DIBDI(15 downto 0) => B"1111111111111111",
      DIPADIP(1 downto 0) => B"00",
      DIPBDIP(1 downto 0) => B"11",
      DOADO(15 downto 6) => NLW_ram_reg_1_DOADO_UNCONNECTED(15 downto 6),
      DOADO(5 downto 0) => \_DOUT_1[3]_6\(23 downto 18),
      DOBDO(15 downto 0) => NLW_ram_reg_1_DOBDO_UNCONNECTED(15 downto 0),
      DOPADOP(1 downto 0) => NLW_ram_reg_1_DOPADOP_UNCONNECTED(1 downto 0),
      DOPBDOP(1 downto 0) => NLW_ram_reg_1_DOPBDOP_UNCONNECTED(1 downto 0),
      ENARDEN => Q(0),
      ENBWREN => '0',
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      WEA(1) => \ram_reg_0_i_1__3_n_0\,
      WEA(0) => \ram_reg_0_i_1__3_n_0\,
      WEBWE(3 downto 0) => B"0000"
    );
\y_reg[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000EEE222E2"
    )
        port map (
      I0 => \y_reg[0]_i_2_n_0\,
      I1 => ctrl_mux_out_1(3),
      I2 => \y_reg[0]\,
      I3 => ctrl_mux_out_1(2),
      I4 => \y_reg[0]_0\,
      I5 => ctrl_mux_out_1(4),
      O => D(0)
    );
\y_reg[0]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \y_reg[0]_i_5_n_0\,
      I1 => \y_reg[0]_i_1_0\,
      O => \y_reg[0]_i_2_n_0\,
      S => ctrl_mux_out_1(2)
    );
\y_reg[0]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_1[3]_6\(0),
      I1 => dout0_out(0),
      I2 => ctrl_mux_out_1(1),
      I3 => \y_reg[23]_i_3_0\(0),
      I4 => ctrl_mux_out_1(0),
      I5 => \y_reg[23]_i_3_1\(0),
      O => \y_reg[0]_i_5_n_0\
    );
\y_reg[10]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000EEE222E2"
    )
        port map (
      I0 => \y_reg[10]_i_2_n_0\,
      I1 => ctrl_mux_out_1(3),
      I2 => \y_reg[10]\,
      I3 => ctrl_mux_out_1(2),
      I4 => \y_reg[10]_0\,
      I5 => ctrl_mux_out_1(4),
      O => D(10)
    );
\y_reg[10]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \y_reg[10]_i_5_n_0\,
      I1 => \y_reg[10]_i_1_0\,
      O => \y_reg[10]_i_2_n_0\,
      S => ctrl_mux_out_1(2)
    );
\y_reg[10]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_1[3]_6\(10),
      I1 => dout0_out(10),
      I2 => ctrl_mux_out_1(1),
      I3 => \y_reg[23]_i_3_0\(10),
      I4 => ctrl_mux_out_1(0),
      I5 => \y_reg[23]_i_3_1\(10),
      O => \y_reg[10]_i_5_n_0\
    );
\y_reg[11]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000EEE222E2"
    )
        port map (
      I0 => \y_reg[11]_i_2_n_0\,
      I1 => ctrl_mux_out_1(3),
      I2 => \y_reg[11]\,
      I3 => ctrl_mux_out_1(2),
      I4 => \y_reg[11]_0\,
      I5 => ctrl_mux_out_1(4),
      O => D(11)
    );
\y_reg[11]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \y_reg[11]_i_5_n_0\,
      I1 => \y_reg[11]_i_1_0\,
      O => \y_reg[11]_i_2_n_0\,
      S => ctrl_mux_out_1(2)
    );
\y_reg[11]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_1[3]_6\(11),
      I1 => dout0_out(11),
      I2 => ctrl_mux_out_1(1),
      I3 => \y_reg[23]_i_3_0\(11),
      I4 => ctrl_mux_out_1(0),
      I5 => \y_reg[23]_i_3_1\(11),
      O => \y_reg[11]_i_5_n_0\
    );
\y_reg[12]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000EEE222E2"
    )
        port map (
      I0 => \y_reg[12]_i_2_n_0\,
      I1 => ctrl_mux_out_1(3),
      I2 => \y_reg[12]\,
      I3 => ctrl_mux_out_1(2),
      I4 => \y_reg[12]_0\,
      I5 => ctrl_mux_out_1(4),
      O => D(12)
    );
\y_reg[12]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \y_reg[12]_i_5_n_0\,
      I1 => \y_reg[12]_i_1_0\,
      O => \y_reg[12]_i_2_n_0\,
      S => ctrl_mux_out_1(2)
    );
\y_reg[12]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_1[3]_6\(12),
      I1 => dout0_out(12),
      I2 => ctrl_mux_out_1(1),
      I3 => \y_reg[23]_i_3_0\(12),
      I4 => ctrl_mux_out_1(0),
      I5 => \y_reg[23]_i_3_1\(12),
      O => \y_reg[12]_i_5_n_0\
    );
\y_reg[13]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000EEE222E2"
    )
        port map (
      I0 => \y_reg[13]_i_2_n_0\,
      I1 => ctrl_mux_out_1(3),
      I2 => \y_reg[13]\,
      I3 => ctrl_mux_out_1(2),
      I4 => \y_reg[13]_0\,
      I5 => ctrl_mux_out_1(4),
      O => D(13)
    );
\y_reg[13]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \y_reg[13]_i_5_n_0\,
      I1 => \y_reg[13]_i_1_0\,
      O => \y_reg[13]_i_2_n_0\,
      S => ctrl_mux_out_1(2)
    );
\y_reg[13]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_1[3]_6\(13),
      I1 => dout0_out(13),
      I2 => ctrl_mux_out_1(1),
      I3 => \y_reg[23]_i_3_0\(13),
      I4 => ctrl_mux_out_1(0),
      I5 => \y_reg[23]_i_3_1\(13),
      O => \y_reg[13]_i_5_n_0\
    );
\y_reg[14]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000EEE222E2"
    )
        port map (
      I0 => \y_reg[14]_i_2_n_0\,
      I1 => ctrl_mux_out_1(3),
      I2 => \y_reg[14]\,
      I3 => ctrl_mux_out_1(2),
      I4 => \y_reg[14]_0\,
      I5 => ctrl_mux_out_1(4),
      O => D(14)
    );
\y_reg[14]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \y_reg[14]_i_5_n_0\,
      I1 => \y_reg[14]_i_1_0\,
      O => \y_reg[14]_i_2_n_0\,
      S => ctrl_mux_out_1(2)
    );
\y_reg[14]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_1[3]_6\(14),
      I1 => dout0_out(14),
      I2 => ctrl_mux_out_1(1),
      I3 => \y_reg[23]_i_3_0\(14),
      I4 => ctrl_mux_out_1(0),
      I5 => \y_reg[23]_i_3_1\(14),
      O => \y_reg[14]_i_5_n_0\
    );
\y_reg[15]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000EEE222E2"
    )
        port map (
      I0 => \y_reg[15]_i_2_n_0\,
      I1 => ctrl_mux_out_1(3),
      I2 => \y_reg[15]\,
      I3 => ctrl_mux_out_1(2),
      I4 => \y_reg[15]_0\,
      I5 => ctrl_mux_out_1(4),
      O => D(15)
    );
\y_reg[15]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \y_reg[15]_i_5_n_0\,
      I1 => \y_reg[15]_i_1_0\,
      O => \y_reg[15]_i_2_n_0\,
      S => ctrl_mux_out_1(2)
    );
\y_reg[15]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_1[3]_6\(15),
      I1 => dout0_out(15),
      I2 => ctrl_mux_out_1(1),
      I3 => \y_reg[23]_i_3_0\(15),
      I4 => ctrl_mux_out_1(0),
      I5 => \y_reg[23]_i_3_1\(15),
      O => \y_reg[15]_i_5_n_0\
    );
\y_reg[16]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000EEE222E2"
    )
        port map (
      I0 => \y_reg[16]_i_2_n_0\,
      I1 => ctrl_mux_out_1(3),
      I2 => \y_reg[16]\,
      I3 => ctrl_mux_out_1(2),
      I4 => \y_reg[16]_0\,
      I5 => ctrl_mux_out_1(4),
      O => D(16)
    );
\y_reg[16]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \y_reg[16]_i_5_n_0\,
      I1 => \y_reg[16]_i_1_0\,
      O => \y_reg[16]_i_2_n_0\,
      S => ctrl_mux_out_1(2)
    );
\y_reg[16]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_1[3]_6\(16),
      I1 => dout0_out(16),
      I2 => ctrl_mux_out_1(1),
      I3 => \y_reg[23]_i_3_0\(16),
      I4 => ctrl_mux_out_1(0),
      I5 => \y_reg[23]_i_3_1\(16),
      O => \y_reg[16]_i_5_n_0\
    );
\y_reg[17]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000EEE222E2"
    )
        port map (
      I0 => \y_reg[17]_i_2_n_0\,
      I1 => ctrl_mux_out_1(3),
      I2 => \y_reg[17]\,
      I3 => ctrl_mux_out_1(2),
      I4 => \y_reg[17]_0\,
      I5 => ctrl_mux_out_1(4),
      O => D(17)
    );
\y_reg[17]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \y_reg[17]_i_5_n_0\,
      I1 => \y_reg[17]_i_1_0\,
      O => \y_reg[17]_i_2_n_0\,
      S => ctrl_mux_out_1(2)
    );
\y_reg[17]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_1[3]_6\(17),
      I1 => dout0_out(17),
      I2 => ctrl_mux_out_1(1),
      I3 => \y_reg[23]_i_3_0\(17),
      I4 => ctrl_mux_out_1(0),
      I5 => \y_reg[23]_i_3_1\(17),
      O => \y_reg[17]_i_5_n_0\
    );
\y_reg[18]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000EEE222E2"
    )
        port map (
      I0 => \y_reg[18]_i_2_n_0\,
      I1 => ctrl_mux_out_1(3),
      I2 => \y_reg[18]\,
      I3 => ctrl_mux_out_1(2),
      I4 => \y_reg[18]_0\,
      I5 => ctrl_mux_out_1(4),
      O => D(18)
    );
\y_reg[18]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \y_reg[18]_i_5_n_0\,
      I1 => \y_reg[18]_i_1_0\,
      O => \y_reg[18]_i_2_n_0\,
      S => ctrl_mux_out_1(2)
    );
\y_reg[18]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_1[3]_6\(18),
      I1 => dout0_out(18),
      I2 => ctrl_mux_out_1(1),
      I3 => \y_reg[23]_i_3_0\(18),
      I4 => ctrl_mux_out_1(0),
      I5 => \y_reg[23]_i_3_1\(18),
      O => \y_reg[18]_i_5_n_0\
    );
\y_reg[19]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000EEE222E2"
    )
        port map (
      I0 => \y_reg[19]_i_2_n_0\,
      I1 => ctrl_mux_out_1(3),
      I2 => \y_reg[19]\,
      I3 => ctrl_mux_out_1(2),
      I4 => \y_reg[19]_0\,
      I5 => ctrl_mux_out_1(4),
      O => D(19)
    );
\y_reg[19]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \y_reg[19]_i_5_n_0\,
      I1 => \y_reg[19]_i_1_0\,
      O => \y_reg[19]_i_2_n_0\,
      S => ctrl_mux_out_1(2)
    );
\y_reg[19]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_1[3]_6\(19),
      I1 => dout0_out(19),
      I2 => ctrl_mux_out_1(1),
      I3 => \y_reg[23]_i_3_0\(19),
      I4 => ctrl_mux_out_1(0),
      I5 => \y_reg[23]_i_3_1\(19),
      O => \y_reg[19]_i_5_n_0\
    );
\y_reg[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000EEE222E2"
    )
        port map (
      I0 => \y_reg[1]_i_2_n_0\,
      I1 => ctrl_mux_out_1(3),
      I2 => \y_reg[1]\,
      I3 => ctrl_mux_out_1(2),
      I4 => \y_reg[1]_0\,
      I5 => ctrl_mux_out_1(4),
      O => D(1)
    );
\y_reg[1]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \y_reg[1]_i_5_n_0\,
      I1 => \y_reg[1]_i_1_0\,
      O => \y_reg[1]_i_2_n_0\,
      S => ctrl_mux_out_1(2)
    );
\y_reg[1]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_1[3]_6\(1),
      I1 => dout0_out(1),
      I2 => ctrl_mux_out_1(1),
      I3 => \y_reg[23]_i_3_0\(1),
      I4 => ctrl_mux_out_1(0),
      I5 => \y_reg[23]_i_3_1\(1),
      O => \y_reg[1]_i_5_n_0\
    );
\y_reg[20]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000EEE222E2"
    )
        port map (
      I0 => \y_reg[20]_i_2_n_0\,
      I1 => ctrl_mux_out_1(3),
      I2 => \y_reg[20]\,
      I3 => ctrl_mux_out_1(2),
      I4 => \y_reg[20]_0\,
      I5 => ctrl_mux_out_1(4),
      O => D(20)
    );
\y_reg[20]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \y_reg[20]_i_5_n_0\,
      I1 => \y_reg[20]_i_1_0\,
      O => \y_reg[20]_i_2_n_0\,
      S => ctrl_mux_out_1(2)
    );
\y_reg[20]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_1[3]_6\(20),
      I1 => dout0_out(20),
      I2 => ctrl_mux_out_1(1),
      I3 => \y_reg[23]_i_3_0\(20),
      I4 => ctrl_mux_out_1(0),
      I5 => \y_reg[23]_i_3_1\(20),
      O => \y_reg[20]_i_5_n_0\
    );
\y_reg[21]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000EEE222E2"
    )
        port map (
      I0 => \y_reg[21]_i_2_n_0\,
      I1 => ctrl_mux_out_1(3),
      I2 => \y_reg[21]\,
      I3 => ctrl_mux_out_1(2),
      I4 => \y_reg[21]_0\,
      I5 => ctrl_mux_out_1(4),
      O => D(21)
    );
\y_reg[21]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \y_reg[21]_i_5_n_0\,
      I1 => \y_reg[21]_i_1_0\,
      O => \y_reg[21]_i_2_n_0\,
      S => ctrl_mux_out_1(2)
    );
\y_reg[21]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_1[3]_6\(21),
      I1 => dout0_out(21),
      I2 => ctrl_mux_out_1(1),
      I3 => \y_reg[23]_i_3_0\(21),
      I4 => ctrl_mux_out_1(0),
      I5 => \y_reg[23]_i_3_1\(21),
      O => \y_reg[21]_i_5_n_0\
    );
\y_reg[22]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000EEE222E2"
    )
        port map (
      I0 => \y_reg[22]_i_2_n_0\,
      I1 => ctrl_mux_out_1(3),
      I2 => \y_reg[22]\,
      I3 => ctrl_mux_out_1(2),
      I4 => \y_reg[22]_0\,
      I5 => ctrl_mux_out_1(4),
      O => D(22)
    );
\y_reg[22]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \y_reg[22]_i_5_n_0\,
      I1 => \y_reg[22]_i_1_0\,
      O => \y_reg[22]_i_2_n_0\,
      S => ctrl_mux_out_1(2)
    );
\y_reg[22]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_1[3]_6\(22),
      I1 => dout0_out(22),
      I2 => ctrl_mux_out_1(1),
      I3 => \y_reg[23]_i_3_0\(22),
      I4 => ctrl_mux_out_1(0),
      I5 => \y_reg[23]_i_3_1\(22),
      O => \y_reg[22]_i_5_n_0\
    );
\y_reg[23]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000EEE222E2"
    )
        port map (
      I0 => \y_reg[23]_i_3_n_0\,
      I1 => ctrl_mux_out_1(3),
      I2 => \y_reg[23]\,
      I3 => ctrl_mux_out_1(2),
      I4 => \y_reg[23]_0\,
      I5 => ctrl_mux_out_1(4),
      O => D(23)
    );
\y_reg[23]_i_3\: unisim.vcomponents.MUXF7
     port map (
      I0 => \y_reg[23]_i_6_n_0\,
      I1 => \y_reg[23]_i_1_0\,
      O => \y_reg[23]_i_3_n_0\,
      S => ctrl_mux_out_1(2)
    );
\y_reg[23]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_1[3]_6\(23),
      I1 => dout0_out(23),
      I2 => ctrl_mux_out_1(1),
      I3 => \y_reg[23]_i_3_0\(23),
      I4 => ctrl_mux_out_1(0),
      I5 => \y_reg[23]_i_3_1\(23),
      O => \y_reg[23]_i_6_n_0\
    );
\y_reg[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000EEE222E2"
    )
        port map (
      I0 => \y_reg[2]_i_2_n_0\,
      I1 => ctrl_mux_out_1(3),
      I2 => \y_reg[2]\,
      I3 => ctrl_mux_out_1(2),
      I4 => \y_reg[2]_0\,
      I5 => ctrl_mux_out_1(4),
      O => D(2)
    );
\y_reg[2]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \y_reg[2]_i_5_n_0\,
      I1 => \y_reg[2]_i_1_0\,
      O => \y_reg[2]_i_2_n_0\,
      S => ctrl_mux_out_1(2)
    );
\y_reg[2]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_1[3]_6\(2),
      I1 => dout0_out(2),
      I2 => ctrl_mux_out_1(1),
      I3 => \y_reg[23]_i_3_0\(2),
      I4 => ctrl_mux_out_1(0),
      I5 => \y_reg[23]_i_3_1\(2),
      O => \y_reg[2]_i_5_n_0\
    );
\y_reg[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000EEE222E2"
    )
        port map (
      I0 => \y_reg[3]_i_2_n_0\,
      I1 => ctrl_mux_out_1(3),
      I2 => \y_reg[3]\,
      I3 => ctrl_mux_out_1(2),
      I4 => \y_reg[3]_0\,
      I5 => ctrl_mux_out_1(4),
      O => D(3)
    );
\y_reg[3]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \y_reg[3]_i_5_n_0\,
      I1 => \y_reg[3]_i_1_0\,
      O => \y_reg[3]_i_2_n_0\,
      S => ctrl_mux_out_1(2)
    );
\y_reg[3]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_1[3]_6\(3),
      I1 => dout0_out(3),
      I2 => ctrl_mux_out_1(1),
      I3 => \y_reg[23]_i_3_0\(3),
      I4 => ctrl_mux_out_1(0),
      I5 => \y_reg[23]_i_3_1\(3),
      O => \y_reg[3]_i_5_n_0\
    );
\y_reg[4]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000EEE222E2"
    )
        port map (
      I0 => \y_reg[4]_i_2_n_0\,
      I1 => ctrl_mux_out_1(3),
      I2 => \y_reg[4]\,
      I3 => ctrl_mux_out_1(2),
      I4 => \y_reg[4]_0\,
      I5 => ctrl_mux_out_1(4),
      O => D(4)
    );
\y_reg[4]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \y_reg[4]_i_5_n_0\,
      I1 => \y_reg[4]_i_1_0\,
      O => \y_reg[4]_i_2_n_0\,
      S => ctrl_mux_out_1(2)
    );
\y_reg[4]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_1[3]_6\(4),
      I1 => dout0_out(4),
      I2 => ctrl_mux_out_1(1),
      I3 => \y_reg[23]_i_3_0\(4),
      I4 => ctrl_mux_out_1(0),
      I5 => \y_reg[23]_i_3_1\(4),
      O => \y_reg[4]_i_5_n_0\
    );
\y_reg[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000EEE222E2"
    )
        port map (
      I0 => \y_reg[5]_i_2_n_0\,
      I1 => ctrl_mux_out_1(3),
      I2 => \y_reg[5]\,
      I3 => ctrl_mux_out_1(2),
      I4 => \y_reg[5]_0\,
      I5 => ctrl_mux_out_1(4),
      O => D(5)
    );
\y_reg[5]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \y_reg[5]_i_5_n_0\,
      I1 => \y_reg[5]_i_1_0\,
      O => \y_reg[5]_i_2_n_0\,
      S => ctrl_mux_out_1(2)
    );
\y_reg[5]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_1[3]_6\(5),
      I1 => dout0_out(5),
      I2 => ctrl_mux_out_1(1),
      I3 => \y_reg[23]_i_3_0\(5),
      I4 => ctrl_mux_out_1(0),
      I5 => \y_reg[23]_i_3_1\(5),
      O => \y_reg[5]_i_5_n_0\
    );
\y_reg[6]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000EEE222E2"
    )
        port map (
      I0 => \y_reg[6]_i_2_n_0\,
      I1 => ctrl_mux_out_1(3),
      I2 => \y_reg[6]\,
      I3 => ctrl_mux_out_1(2),
      I4 => \y_reg[6]_0\,
      I5 => ctrl_mux_out_1(4),
      O => D(6)
    );
\y_reg[6]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \y_reg[6]_i_5_n_0\,
      I1 => \y_reg[6]_i_1_0\,
      O => \y_reg[6]_i_2_n_0\,
      S => ctrl_mux_out_1(2)
    );
\y_reg[6]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_1[3]_6\(6),
      I1 => dout0_out(6),
      I2 => ctrl_mux_out_1(1),
      I3 => \y_reg[23]_i_3_0\(6),
      I4 => ctrl_mux_out_1(0),
      I5 => \y_reg[23]_i_3_1\(6),
      O => \y_reg[6]_i_5_n_0\
    );
\y_reg[7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000EEE222E2"
    )
        port map (
      I0 => \y_reg[7]_i_2_n_0\,
      I1 => ctrl_mux_out_1(3),
      I2 => \y_reg[7]\,
      I3 => ctrl_mux_out_1(2),
      I4 => \y_reg[7]_0\,
      I5 => ctrl_mux_out_1(4),
      O => D(7)
    );
\y_reg[7]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \y_reg[7]_i_5_n_0\,
      I1 => \y_reg[7]_i_1_0\,
      O => \y_reg[7]_i_2_n_0\,
      S => ctrl_mux_out_1(2)
    );
\y_reg[7]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_1[3]_6\(7),
      I1 => dout0_out(7),
      I2 => ctrl_mux_out_1(1),
      I3 => \y_reg[23]_i_3_0\(7),
      I4 => ctrl_mux_out_1(0),
      I5 => \y_reg[23]_i_3_1\(7),
      O => \y_reg[7]_i_5_n_0\
    );
\y_reg[8]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000EEE222E2"
    )
        port map (
      I0 => \y_reg[8]_i_2_n_0\,
      I1 => ctrl_mux_out_1(3),
      I2 => \y_reg[8]\,
      I3 => ctrl_mux_out_1(2),
      I4 => \y_reg[8]_0\,
      I5 => ctrl_mux_out_1(4),
      O => D(8)
    );
\y_reg[8]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \y_reg[8]_i_5_n_0\,
      I1 => \y_reg[8]_i_1_0\,
      O => \y_reg[8]_i_2_n_0\,
      S => ctrl_mux_out_1(2)
    );
\y_reg[8]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_1[3]_6\(8),
      I1 => dout0_out(8),
      I2 => ctrl_mux_out_1(1),
      I3 => \y_reg[23]_i_3_0\(8),
      I4 => ctrl_mux_out_1(0),
      I5 => \y_reg[23]_i_3_1\(8),
      O => \y_reg[8]_i_5_n_0\
    );
\y_reg[9]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000EEE222E2"
    )
        port map (
      I0 => \y_reg[9]_i_2_n_0\,
      I1 => ctrl_mux_out_1(3),
      I2 => \y_reg[9]\,
      I3 => ctrl_mux_out_1(2),
      I4 => \y_reg[9]_0\,
      I5 => ctrl_mux_out_1(4),
      O => D(9)
    );
\y_reg[9]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \y_reg[9]_i_5_n_0\,
      I1 => \y_reg[9]_i_1_0\,
      O => \y_reg[9]_i_2_n_0\,
      S => ctrl_mux_out_1(2)
    );
\y_reg[9]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_1[3]_6\(9),
      I1 => dout0_out(9),
      I2 => ctrl_mux_out_1(1),
      I3 => \y_reg[23]_i_3_0\(9),
      I4 => ctrl_mux_out_1(0),
      I5 => \y_reg[23]_i_3_1\(9),
      O => \y_reg[9]_i_5_n_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity test_bd_pixelReOrder_0_0_rams_sp_rf_rst_18 is
  port (
    D : out STD_LOGIC_VECTOR ( 23 downto 0 );
    ctrl_mux_out_2 : in STD_LOGIC_VECTOR ( 4 downto 0 );
    \y_reg[0]\ : in STD_LOGIC;
    \y_reg[0]_0\ : in STD_LOGIC;
    \y_reg[1]\ : in STD_LOGIC;
    \y_reg[1]_0\ : in STD_LOGIC;
    \y_reg[2]\ : in STD_LOGIC;
    \y_reg[2]_0\ : in STD_LOGIC;
    \y_reg[3]\ : in STD_LOGIC;
    \y_reg[3]_0\ : in STD_LOGIC;
    \y_reg[4]\ : in STD_LOGIC;
    \y_reg[4]_0\ : in STD_LOGIC;
    \y_reg[5]\ : in STD_LOGIC;
    \y_reg[5]_0\ : in STD_LOGIC;
    \y_reg[6]\ : in STD_LOGIC;
    \y_reg[6]_0\ : in STD_LOGIC;
    \y_reg[7]\ : in STD_LOGIC;
    \y_reg[7]_0\ : in STD_LOGIC;
    \y_reg[8]\ : in STD_LOGIC;
    \y_reg[8]_0\ : in STD_LOGIC;
    \y_reg[9]\ : in STD_LOGIC;
    \y_reg[9]_0\ : in STD_LOGIC;
    \y_reg[10]\ : in STD_LOGIC;
    \y_reg[10]_0\ : in STD_LOGIC;
    \y_reg[11]\ : in STD_LOGIC;
    \y_reg[11]_0\ : in STD_LOGIC;
    \y_reg[12]\ : in STD_LOGIC;
    \y_reg[12]_0\ : in STD_LOGIC;
    \y_reg[13]\ : in STD_LOGIC;
    \y_reg[13]_0\ : in STD_LOGIC;
    \y_reg[14]\ : in STD_LOGIC;
    \y_reg[14]_0\ : in STD_LOGIC;
    \y_reg[15]\ : in STD_LOGIC;
    \y_reg[15]_0\ : in STD_LOGIC;
    \y_reg[16]\ : in STD_LOGIC;
    \y_reg[16]_0\ : in STD_LOGIC;
    \y_reg[17]\ : in STD_LOGIC;
    \y_reg[17]_0\ : in STD_LOGIC;
    \y_reg[18]\ : in STD_LOGIC;
    \y_reg[18]_0\ : in STD_LOGIC;
    \y_reg[19]\ : in STD_LOGIC;
    \y_reg[19]_0\ : in STD_LOGIC;
    \y_reg[20]\ : in STD_LOGIC;
    \y_reg[20]_0\ : in STD_LOGIC;
    \y_reg[21]\ : in STD_LOGIC;
    \y_reg[21]_0\ : in STD_LOGIC;
    \y_reg[22]\ : in STD_LOGIC;
    \y_reg[22]_0\ : in STD_LOGIC;
    \y_reg[23]\ : in STD_LOGIC;
    \y_reg[23]_0\ : in STD_LOGIC;
    \y_reg[0]_i_1__0_0\ : in STD_LOGIC;
    dout0_out : in STD_LOGIC_VECTOR ( 23 downto 0 );
    \y_reg[23]_i_3__0_0\ : in STD_LOGIC_VECTOR ( 23 downto 0 );
    \y_reg[23]_i_3__0_1\ : in STD_LOGIC_VECTOR ( 23 downto 0 );
    \y_reg[1]_i_1__0_0\ : in STD_LOGIC;
    \y_reg[2]_i_1__0_0\ : in STD_LOGIC;
    \y_reg[3]_i_1__0_0\ : in STD_LOGIC;
    \y_reg[4]_i_1__0_0\ : in STD_LOGIC;
    \y_reg[5]_i_1__0_0\ : in STD_LOGIC;
    \y_reg[6]_i_1__0_0\ : in STD_LOGIC;
    \y_reg[7]_i_1__0_0\ : in STD_LOGIC;
    \y_reg[8]_i_1__0_0\ : in STD_LOGIC;
    \y_reg[9]_i_1__0_0\ : in STD_LOGIC;
    \y_reg[10]_i_1__0_0\ : in STD_LOGIC;
    \y_reg[11]_i_1__0_0\ : in STD_LOGIC;
    \y_reg[12]_i_1__0_0\ : in STD_LOGIC;
    \y_reg[13]_i_1__0_0\ : in STD_LOGIC;
    \y_reg[14]_i_1__0_0\ : in STD_LOGIC;
    \y_reg[15]_i_1__0_0\ : in STD_LOGIC;
    \y_reg[16]_i_1__0_0\ : in STD_LOGIC;
    \y_reg[17]_i_1__0_0\ : in STD_LOGIC;
    \y_reg[18]_i_1__0_0\ : in STD_LOGIC;
    \y_reg[19]_i_1__0_0\ : in STD_LOGIC;
    \y_reg[20]_i_1__0_0\ : in STD_LOGIC;
    \y_reg[21]_i_1__0_0\ : in STD_LOGIC;
    \y_reg[22]_i_1__0_0\ : in STD_LOGIC;
    \y_reg[23]_i_1__0_0\ : in STD_LOGIC;
    m00_axis_aclk : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 0 to 0 );
    addr : in STD_LOGIC_VECTOR ( 10 downto 0 );
    s00_axis_tdata : in STD_LOGIC_VECTOR ( 23 downto 0 );
    s00_axis_tuser : in STD_LOGIC_VECTOR ( 0 to 0 );
    s00_axis_tvalid : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of test_bd_pixelReOrder_0_0_rams_sp_rf_rst_18 : entity is "rams_sp_rf_rst";
end test_bd_pixelReOrder_0_0_rams_sp_rf_rst_18;

architecture STRUCTURE of test_bd_pixelReOrder_0_0_rams_sp_rf_rst_18 is
  signal \_DOUT_2[3]_7\ : STD_LOGIC_VECTOR ( 23 downto 0 );
  signal \ram_reg_0_i_1__4_n_0\ : STD_LOGIC;
  signal \y_reg[0]_i_2__0_n_0\ : STD_LOGIC;
  signal \y_reg[0]_i_5__0_n_0\ : STD_LOGIC;
  signal \y_reg[10]_i_2__0_n_0\ : STD_LOGIC;
  signal \y_reg[10]_i_5__0_n_0\ : STD_LOGIC;
  signal \y_reg[11]_i_2__0_n_0\ : STD_LOGIC;
  signal \y_reg[11]_i_5__0_n_0\ : STD_LOGIC;
  signal \y_reg[12]_i_2__0_n_0\ : STD_LOGIC;
  signal \y_reg[12]_i_5__0_n_0\ : STD_LOGIC;
  signal \y_reg[13]_i_2__0_n_0\ : STD_LOGIC;
  signal \y_reg[13]_i_5__0_n_0\ : STD_LOGIC;
  signal \y_reg[14]_i_2__0_n_0\ : STD_LOGIC;
  signal \y_reg[14]_i_5__0_n_0\ : STD_LOGIC;
  signal \y_reg[15]_i_2__0_n_0\ : STD_LOGIC;
  signal \y_reg[15]_i_5__0_n_0\ : STD_LOGIC;
  signal \y_reg[16]_i_2__0_n_0\ : STD_LOGIC;
  signal \y_reg[16]_i_5__0_n_0\ : STD_LOGIC;
  signal \y_reg[17]_i_2__0_n_0\ : STD_LOGIC;
  signal \y_reg[17]_i_5__0_n_0\ : STD_LOGIC;
  signal \y_reg[18]_i_2__0_n_0\ : STD_LOGIC;
  signal \y_reg[18]_i_5__0_n_0\ : STD_LOGIC;
  signal \y_reg[19]_i_2__0_n_0\ : STD_LOGIC;
  signal \y_reg[19]_i_5__0_n_0\ : STD_LOGIC;
  signal \y_reg[1]_i_2__0_n_0\ : STD_LOGIC;
  signal \y_reg[1]_i_5__0_n_0\ : STD_LOGIC;
  signal \y_reg[20]_i_2__0_n_0\ : STD_LOGIC;
  signal \y_reg[20]_i_5__0_n_0\ : STD_LOGIC;
  signal \y_reg[21]_i_2__0_n_0\ : STD_LOGIC;
  signal \y_reg[21]_i_5__0_n_0\ : STD_LOGIC;
  signal \y_reg[22]_i_2__0_n_0\ : STD_LOGIC;
  signal \y_reg[22]_i_5__0_n_0\ : STD_LOGIC;
  signal \y_reg[23]_i_3__0_n_0\ : STD_LOGIC;
  signal \y_reg[23]_i_6__0_n_0\ : STD_LOGIC;
  signal \y_reg[2]_i_2__0_n_0\ : STD_LOGIC;
  signal \y_reg[2]_i_5__0_n_0\ : STD_LOGIC;
  signal \y_reg[3]_i_2__0_n_0\ : STD_LOGIC;
  signal \y_reg[3]_i_5__0_n_0\ : STD_LOGIC;
  signal \y_reg[4]_i_2__0_n_0\ : STD_LOGIC;
  signal \y_reg[4]_i_5__0_n_0\ : STD_LOGIC;
  signal \y_reg[5]_i_2__0_n_0\ : STD_LOGIC;
  signal \y_reg[5]_i_5__0_n_0\ : STD_LOGIC;
  signal \y_reg[6]_i_2__0_n_0\ : STD_LOGIC;
  signal \y_reg[6]_i_5__0_n_0\ : STD_LOGIC;
  signal \y_reg[7]_i_2__0_n_0\ : STD_LOGIC;
  signal \y_reg[7]_i_5__0_n_0\ : STD_LOGIC;
  signal \y_reg[8]_i_2__0_n_0\ : STD_LOGIC;
  signal \y_reg[8]_i_5__0_n_0\ : STD_LOGIC;
  signal \y_reg[9]_i_2__0_n_0\ : STD_LOGIC;
  signal \y_reg[9]_i_5__0_n_0\ : STD_LOGIC;
  signal NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_DBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_SBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_DOADO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 16 );
  signal NLW_ram_reg_0_DOBDO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_ram_reg_0_DOPADOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal NLW_ram_reg_0_DOPBDOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_ram_reg_0_ECCPARITY_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal NLW_ram_reg_0_RDADDRECC_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_ram_reg_1_DOADO_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 6 );
  signal NLW_ram_reg_1_DOBDO_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal NLW_ram_reg_1_DOPADOP_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_ram_reg_1_DOPBDOP_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ : string;
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of ram_reg_0 : label is "p2_d16";
  attribute METHODOLOGY_DRC_VIOS : string;
  attribute METHODOLOGY_DRC_VIOS of ram_reg_0 : label is "{SYNTH-6 {cell *THIS*}}";
  attribute RTL_RAM_BITS : integer;
  attribute RTL_RAM_BITS of ram_reg_0 : label is 46080;
  attribute RTL_RAM_NAME : string;
  attribute RTL_RAM_NAME of ram_reg_0 : label is "ram";
  attribute bram_addr_begin : integer;
  attribute bram_addr_begin of ram_reg_0 : label is 0;
  attribute bram_addr_end : integer;
  attribute bram_addr_end of ram_reg_0 : label is 2047;
  attribute bram_slice_begin : integer;
  attribute bram_slice_begin of ram_reg_0 : label is 0;
  attribute bram_slice_end : integer;
  attribute bram_slice_end of ram_reg_0 : label is 17;
  attribute ram_addr_begin : integer;
  attribute ram_addr_begin of ram_reg_0 : label is 0;
  attribute ram_addr_end : integer;
  attribute ram_addr_end of ram_reg_0 : label is 2047;
  attribute ram_offset : integer;
  attribute ram_offset of ram_reg_0 : label is 0;
  attribute ram_slice_begin : integer;
  attribute ram_slice_begin of ram_reg_0 : label is 0;
  attribute ram_slice_end : integer;
  attribute ram_slice_end of ram_reg_0 : label is 17;
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of ram_reg_1 : label is "p0_d6";
  attribute METHODOLOGY_DRC_VIOS of ram_reg_1 : label is "{SYNTH-6 {cell *THIS*}}";
  attribute RTL_RAM_BITS of ram_reg_1 : label is 46080;
  attribute RTL_RAM_NAME of ram_reg_1 : label is "ram";
  attribute bram_addr_begin of ram_reg_1 : label is 0;
  attribute bram_addr_end of ram_reg_1 : label is 2047;
  attribute bram_slice_begin of ram_reg_1 : label is 18;
  attribute bram_slice_end of ram_reg_1 : label is 23;
  attribute ram_addr_begin of ram_reg_1 : label is 0;
  attribute ram_addr_end of ram_reg_1 : label is 2047;
  attribute ram_offset of ram_reg_1 : label is 0;
  attribute ram_slice_begin of ram_reg_1 : label is 18;
  attribute ram_slice_end of ram_reg_1 : label is 23;
begin
ram_reg_0: unisim.vcomponents.RAMB36E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      EN_ECC_READ => false,
      EN_ECC_WRITE => false,
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      RAM_EXTENSION_A => "NONE",
      RAM_EXTENSION_B => "NONE",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "PERFORMANCE",
      READ_WIDTH_A => 18,
      READ_WIDTH_B => 0,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "READ_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 18,
      WRITE_WIDTH_B => 0
    )
        port map (
      ADDRARDADDR(15) => '1',
      ADDRARDADDR(14 downto 4) => addr(10 downto 0),
      ADDRARDADDR(3 downto 0) => B"1111",
      ADDRBWRADDR(15 downto 0) => B"1111111111111111",
      CASCADEINA => '1',
      CASCADEINB => '0',
      CASCADEOUTA => NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED,
      CASCADEOUTB => NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED,
      CLKARDCLK => m00_axis_aclk,
      CLKBWRCLK => '0',
      DBITERR => NLW_ram_reg_0_DBITERR_UNCONNECTED,
      DIADI(31 downto 16) => B"0000000000000000",
      DIADI(15 downto 0) => s00_axis_tdata(15 downto 0),
      DIBDI(31 downto 0) => B"11111111111111111111111111111111",
      DIPADIP(3 downto 2) => B"00",
      DIPADIP(1 downto 0) => s00_axis_tdata(17 downto 16),
      DIPBDIP(3 downto 0) => B"1111",
      DOADO(31 downto 16) => NLW_ram_reg_0_DOADO_UNCONNECTED(31 downto 16),
      DOADO(15 downto 0) => \_DOUT_2[3]_7\(15 downto 0),
      DOBDO(31 downto 0) => NLW_ram_reg_0_DOBDO_UNCONNECTED(31 downto 0),
      DOPADOP(3 downto 2) => NLW_ram_reg_0_DOPADOP_UNCONNECTED(3 downto 2),
      DOPADOP(1 downto 0) => \_DOUT_2[3]_7\(17 downto 16),
      DOPBDOP(3 downto 0) => NLW_ram_reg_0_DOPBDOP_UNCONNECTED(3 downto 0),
      ECCPARITY(7 downto 0) => NLW_ram_reg_0_ECCPARITY_UNCONNECTED(7 downto 0),
      ENARDEN => Q(0),
      ENBWREN => '0',
      INJECTDBITERR => NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED,
      INJECTSBITERR => NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED,
      RDADDRECC(8 downto 0) => NLW_ram_reg_0_RDADDRECC_UNCONNECTED(8 downto 0),
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      SBITERR => NLW_ram_reg_0_SBITERR_UNCONNECTED,
      WEA(3) => \ram_reg_0_i_1__4_n_0\,
      WEA(2) => \ram_reg_0_i_1__4_n_0\,
      WEA(1) => \ram_reg_0_i_1__4_n_0\,
      WEA(0) => \ram_reg_0_i_1__4_n_0\,
      WEBWE(7 downto 0) => B"00000000"
    );
\ram_reg_0_i_1__4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => Q(0),
      I1 => s00_axis_tuser(0),
      I2 => s00_axis_tvalid,
      O => \ram_reg_0_i_1__4_n_0\
    );
ram_reg_1: unisim.vcomponents.RAMB18E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      INIT_A => X"00000",
      INIT_B => X"00000",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "PERFORMANCE",
      READ_WIDTH_A => 9,
      READ_WIDTH_B => 0,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"00000",
      SRVAL_B => X"00000",
      WRITE_MODE_A => "READ_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 9,
      WRITE_WIDTH_B => 0
    )
        port map (
      ADDRARDADDR(13 downto 3) => addr(10 downto 0),
      ADDRARDADDR(2 downto 0) => B"111",
      ADDRBWRADDR(13 downto 0) => B"11111111111111",
      CLKARDCLK => m00_axis_aclk,
      CLKBWRCLK => '0',
      DIADI(15 downto 6) => B"0000000000",
      DIADI(5 downto 0) => s00_axis_tdata(23 downto 18),
      DIBDI(15 downto 0) => B"1111111111111111",
      DIPADIP(1 downto 0) => B"00",
      DIPBDIP(1 downto 0) => B"11",
      DOADO(15 downto 6) => NLW_ram_reg_1_DOADO_UNCONNECTED(15 downto 6),
      DOADO(5 downto 0) => \_DOUT_2[3]_7\(23 downto 18),
      DOBDO(15 downto 0) => NLW_ram_reg_1_DOBDO_UNCONNECTED(15 downto 0),
      DOPADOP(1 downto 0) => NLW_ram_reg_1_DOPADOP_UNCONNECTED(1 downto 0),
      DOPBDOP(1 downto 0) => NLW_ram_reg_1_DOPBDOP_UNCONNECTED(1 downto 0),
      ENARDEN => Q(0),
      ENBWREN => '0',
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      WEA(1) => \ram_reg_0_i_1__4_n_0\,
      WEA(0) => \ram_reg_0_i_1__4_n_0\,
      WEBWE(3 downto 0) => B"0000"
    );
\y_reg[0]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000EEE222E2"
    )
        port map (
      I0 => \y_reg[0]_i_2__0_n_0\,
      I1 => ctrl_mux_out_2(3),
      I2 => \y_reg[0]\,
      I3 => ctrl_mux_out_2(2),
      I4 => \y_reg[0]_0\,
      I5 => ctrl_mux_out_2(4),
      O => D(0)
    );
\y_reg[0]_i_2__0\: unisim.vcomponents.MUXF7
     port map (
      I0 => \y_reg[0]_i_5__0_n_0\,
      I1 => \y_reg[0]_i_1__0_0\,
      O => \y_reg[0]_i_2__0_n_0\,
      S => ctrl_mux_out_2(2)
    );
\y_reg[0]_i_5__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_2[3]_7\(0),
      I1 => dout0_out(0),
      I2 => ctrl_mux_out_2(1),
      I3 => \y_reg[23]_i_3__0_0\(0),
      I4 => ctrl_mux_out_2(0),
      I5 => \y_reg[23]_i_3__0_1\(0),
      O => \y_reg[0]_i_5__0_n_0\
    );
\y_reg[10]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000EEE222E2"
    )
        port map (
      I0 => \y_reg[10]_i_2__0_n_0\,
      I1 => ctrl_mux_out_2(3),
      I2 => \y_reg[10]\,
      I3 => ctrl_mux_out_2(2),
      I4 => \y_reg[10]_0\,
      I5 => ctrl_mux_out_2(4),
      O => D(10)
    );
\y_reg[10]_i_2__0\: unisim.vcomponents.MUXF7
     port map (
      I0 => \y_reg[10]_i_5__0_n_0\,
      I1 => \y_reg[10]_i_1__0_0\,
      O => \y_reg[10]_i_2__0_n_0\,
      S => ctrl_mux_out_2(2)
    );
\y_reg[10]_i_5__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_2[3]_7\(10),
      I1 => dout0_out(10),
      I2 => ctrl_mux_out_2(1),
      I3 => \y_reg[23]_i_3__0_0\(10),
      I4 => ctrl_mux_out_2(0),
      I5 => \y_reg[23]_i_3__0_1\(10),
      O => \y_reg[10]_i_5__0_n_0\
    );
\y_reg[11]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000EEE222E2"
    )
        port map (
      I0 => \y_reg[11]_i_2__0_n_0\,
      I1 => ctrl_mux_out_2(3),
      I2 => \y_reg[11]\,
      I3 => ctrl_mux_out_2(2),
      I4 => \y_reg[11]_0\,
      I5 => ctrl_mux_out_2(4),
      O => D(11)
    );
\y_reg[11]_i_2__0\: unisim.vcomponents.MUXF7
     port map (
      I0 => \y_reg[11]_i_5__0_n_0\,
      I1 => \y_reg[11]_i_1__0_0\,
      O => \y_reg[11]_i_2__0_n_0\,
      S => ctrl_mux_out_2(2)
    );
\y_reg[11]_i_5__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_2[3]_7\(11),
      I1 => dout0_out(11),
      I2 => ctrl_mux_out_2(1),
      I3 => \y_reg[23]_i_3__0_0\(11),
      I4 => ctrl_mux_out_2(0),
      I5 => \y_reg[23]_i_3__0_1\(11),
      O => \y_reg[11]_i_5__0_n_0\
    );
\y_reg[12]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000EEE222E2"
    )
        port map (
      I0 => \y_reg[12]_i_2__0_n_0\,
      I1 => ctrl_mux_out_2(3),
      I2 => \y_reg[12]\,
      I3 => ctrl_mux_out_2(2),
      I4 => \y_reg[12]_0\,
      I5 => ctrl_mux_out_2(4),
      O => D(12)
    );
\y_reg[12]_i_2__0\: unisim.vcomponents.MUXF7
     port map (
      I0 => \y_reg[12]_i_5__0_n_0\,
      I1 => \y_reg[12]_i_1__0_0\,
      O => \y_reg[12]_i_2__0_n_0\,
      S => ctrl_mux_out_2(2)
    );
\y_reg[12]_i_5__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_2[3]_7\(12),
      I1 => dout0_out(12),
      I2 => ctrl_mux_out_2(1),
      I3 => \y_reg[23]_i_3__0_0\(12),
      I4 => ctrl_mux_out_2(0),
      I5 => \y_reg[23]_i_3__0_1\(12),
      O => \y_reg[12]_i_5__0_n_0\
    );
\y_reg[13]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000EEE222E2"
    )
        port map (
      I0 => \y_reg[13]_i_2__0_n_0\,
      I1 => ctrl_mux_out_2(3),
      I2 => \y_reg[13]\,
      I3 => ctrl_mux_out_2(2),
      I4 => \y_reg[13]_0\,
      I5 => ctrl_mux_out_2(4),
      O => D(13)
    );
\y_reg[13]_i_2__0\: unisim.vcomponents.MUXF7
     port map (
      I0 => \y_reg[13]_i_5__0_n_0\,
      I1 => \y_reg[13]_i_1__0_0\,
      O => \y_reg[13]_i_2__0_n_0\,
      S => ctrl_mux_out_2(2)
    );
\y_reg[13]_i_5__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_2[3]_7\(13),
      I1 => dout0_out(13),
      I2 => ctrl_mux_out_2(1),
      I3 => \y_reg[23]_i_3__0_0\(13),
      I4 => ctrl_mux_out_2(0),
      I5 => \y_reg[23]_i_3__0_1\(13),
      O => \y_reg[13]_i_5__0_n_0\
    );
\y_reg[14]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000EEE222E2"
    )
        port map (
      I0 => \y_reg[14]_i_2__0_n_0\,
      I1 => ctrl_mux_out_2(3),
      I2 => \y_reg[14]\,
      I3 => ctrl_mux_out_2(2),
      I4 => \y_reg[14]_0\,
      I5 => ctrl_mux_out_2(4),
      O => D(14)
    );
\y_reg[14]_i_2__0\: unisim.vcomponents.MUXF7
     port map (
      I0 => \y_reg[14]_i_5__0_n_0\,
      I1 => \y_reg[14]_i_1__0_0\,
      O => \y_reg[14]_i_2__0_n_0\,
      S => ctrl_mux_out_2(2)
    );
\y_reg[14]_i_5__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_2[3]_7\(14),
      I1 => dout0_out(14),
      I2 => ctrl_mux_out_2(1),
      I3 => \y_reg[23]_i_3__0_0\(14),
      I4 => ctrl_mux_out_2(0),
      I5 => \y_reg[23]_i_3__0_1\(14),
      O => \y_reg[14]_i_5__0_n_0\
    );
\y_reg[15]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000EEE222E2"
    )
        port map (
      I0 => \y_reg[15]_i_2__0_n_0\,
      I1 => ctrl_mux_out_2(3),
      I2 => \y_reg[15]\,
      I3 => ctrl_mux_out_2(2),
      I4 => \y_reg[15]_0\,
      I5 => ctrl_mux_out_2(4),
      O => D(15)
    );
\y_reg[15]_i_2__0\: unisim.vcomponents.MUXF7
     port map (
      I0 => \y_reg[15]_i_5__0_n_0\,
      I1 => \y_reg[15]_i_1__0_0\,
      O => \y_reg[15]_i_2__0_n_0\,
      S => ctrl_mux_out_2(2)
    );
\y_reg[15]_i_5__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_2[3]_7\(15),
      I1 => dout0_out(15),
      I2 => ctrl_mux_out_2(1),
      I3 => \y_reg[23]_i_3__0_0\(15),
      I4 => ctrl_mux_out_2(0),
      I5 => \y_reg[23]_i_3__0_1\(15),
      O => \y_reg[15]_i_5__0_n_0\
    );
\y_reg[16]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000EEE222E2"
    )
        port map (
      I0 => \y_reg[16]_i_2__0_n_0\,
      I1 => ctrl_mux_out_2(3),
      I2 => \y_reg[16]\,
      I3 => ctrl_mux_out_2(2),
      I4 => \y_reg[16]_0\,
      I5 => ctrl_mux_out_2(4),
      O => D(16)
    );
\y_reg[16]_i_2__0\: unisim.vcomponents.MUXF7
     port map (
      I0 => \y_reg[16]_i_5__0_n_0\,
      I1 => \y_reg[16]_i_1__0_0\,
      O => \y_reg[16]_i_2__0_n_0\,
      S => ctrl_mux_out_2(2)
    );
\y_reg[16]_i_5__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_2[3]_7\(16),
      I1 => dout0_out(16),
      I2 => ctrl_mux_out_2(1),
      I3 => \y_reg[23]_i_3__0_0\(16),
      I4 => ctrl_mux_out_2(0),
      I5 => \y_reg[23]_i_3__0_1\(16),
      O => \y_reg[16]_i_5__0_n_0\
    );
\y_reg[17]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000EEE222E2"
    )
        port map (
      I0 => \y_reg[17]_i_2__0_n_0\,
      I1 => ctrl_mux_out_2(3),
      I2 => \y_reg[17]\,
      I3 => ctrl_mux_out_2(2),
      I4 => \y_reg[17]_0\,
      I5 => ctrl_mux_out_2(4),
      O => D(17)
    );
\y_reg[17]_i_2__0\: unisim.vcomponents.MUXF7
     port map (
      I0 => \y_reg[17]_i_5__0_n_0\,
      I1 => \y_reg[17]_i_1__0_0\,
      O => \y_reg[17]_i_2__0_n_0\,
      S => ctrl_mux_out_2(2)
    );
\y_reg[17]_i_5__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_2[3]_7\(17),
      I1 => dout0_out(17),
      I2 => ctrl_mux_out_2(1),
      I3 => \y_reg[23]_i_3__0_0\(17),
      I4 => ctrl_mux_out_2(0),
      I5 => \y_reg[23]_i_3__0_1\(17),
      O => \y_reg[17]_i_5__0_n_0\
    );
\y_reg[18]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000EEE222E2"
    )
        port map (
      I0 => \y_reg[18]_i_2__0_n_0\,
      I1 => ctrl_mux_out_2(3),
      I2 => \y_reg[18]\,
      I3 => ctrl_mux_out_2(2),
      I4 => \y_reg[18]_0\,
      I5 => ctrl_mux_out_2(4),
      O => D(18)
    );
\y_reg[18]_i_2__0\: unisim.vcomponents.MUXF7
     port map (
      I0 => \y_reg[18]_i_5__0_n_0\,
      I1 => \y_reg[18]_i_1__0_0\,
      O => \y_reg[18]_i_2__0_n_0\,
      S => ctrl_mux_out_2(2)
    );
\y_reg[18]_i_5__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_2[3]_7\(18),
      I1 => dout0_out(18),
      I2 => ctrl_mux_out_2(1),
      I3 => \y_reg[23]_i_3__0_0\(18),
      I4 => ctrl_mux_out_2(0),
      I5 => \y_reg[23]_i_3__0_1\(18),
      O => \y_reg[18]_i_5__0_n_0\
    );
\y_reg[19]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000EEE222E2"
    )
        port map (
      I0 => \y_reg[19]_i_2__0_n_0\,
      I1 => ctrl_mux_out_2(3),
      I2 => \y_reg[19]\,
      I3 => ctrl_mux_out_2(2),
      I4 => \y_reg[19]_0\,
      I5 => ctrl_mux_out_2(4),
      O => D(19)
    );
\y_reg[19]_i_2__0\: unisim.vcomponents.MUXF7
     port map (
      I0 => \y_reg[19]_i_5__0_n_0\,
      I1 => \y_reg[19]_i_1__0_0\,
      O => \y_reg[19]_i_2__0_n_0\,
      S => ctrl_mux_out_2(2)
    );
\y_reg[19]_i_5__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_2[3]_7\(19),
      I1 => dout0_out(19),
      I2 => ctrl_mux_out_2(1),
      I3 => \y_reg[23]_i_3__0_0\(19),
      I4 => ctrl_mux_out_2(0),
      I5 => \y_reg[23]_i_3__0_1\(19),
      O => \y_reg[19]_i_5__0_n_0\
    );
\y_reg[1]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000EEE222E2"
    )
        port map (
      I0 => \y_reg[1]_i_2__0_n_0\,
      I1 => ctrl_mux_out_2(3),
      I2 => \y_reg[1]\,
      I3 => ctrl_mux_out_2(2),
      I4 => \y_reg[1]_0\,
      I5 => ctrl_mux_out_2(4),
      O => D(1)
    );
\y_reg[1]_i_2__0\: unisim.vcomponents.MUXF7
     port map (
      I0 => \y_reg[1]_i_5__0_n_0\,
      I1 => \y_reg[1]_i_1__0_0\,
      O => \y_reg[1]_i_2__0_n_0\,
      S => ctrl_mux_out_2(2)
    );
\y_reg[1]_i_5__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_2[3]_7\(1),
      I1 => dout0_out(1),
      I2 => ctrl_mux_out_2(1),
      I3 => \y_reg[23]_i_3__0_0\(1),
      I4 => ctrl_mux_out_2(0),
      I5 => \y_reg[23]_i_3__0_1\(1),
      O => \y_reg[1]_i_5__0_n_0\
    );
\y_reg[20]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000EEE222E2"
    )
        port map (
      I0 => \y_reg[20]_i_2__0_n_0\,
      I1 => ctrl_mux_out_2(3),
      I2 => \y_reg[20]\,
      I3 => ctrl_mux_out_2(2),
      I4 => \y_reg[20]_0\,
      I5 => ctrl_mux_out_2(4),
      O => D(20)
    );
\y_reg[20]_i_2__0\: unisim.vcomponents.MUXF7
     port map (
      I0 => \y_reg[20]_i_5__0_n_0\,
      I1 => \y_reg[20]_i_1__0_0\,
      O => \y_reg[20]_i_2__0_n_0\,
      S => ctrl_mux_out_2(2)
    );
\y_reg[20]_i_5__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_2[3]_7\(20),
      I1 => dout0_out(20),
      I2 => ctrl_mux_out_2(1),
      I3 => \y_reg[23]_i_3__0_0\(20),
      I4 => ctrl_mux_out_2(0),
      I5 => \y_reg[23]_i_3__0_1\(20),
      O => \y_reg[20]_i_5__0_n_0\
    );
\y_reg[21]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000EEE222E2"
    )
        port map (
      I0 => \y_reg[21]_i_2__0_n_0\,
      I1 => ctrl_mux_out_2(3),
      I2 => \y_reg[21]\,
      I3 => ctrl_mux_out_2(2),
      I4 => \y_reg[21]_0\,
      I5 => ctrl_mux_out_2(4),
      O => D(21)
    );
\y_reg[21]_i_2__0\: unisim.vcomponents.MUXF7
     port map (
      I0 => \y_reg[21]_i_5__0_n_0\,
      I1 => \y_reg[21]_i_1__0_0\,
      O => \y_reg[21]_i_2__0_n_0\,
      S => ctrl_mux_out_2(2)
    );
\y_reg[21]_i_5__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_2[3]_7\(21),
      I1 => dout0_out(21),
      I2 => ctrl_mux_out_2(1),
      I3 => \y_reg[23]_i_3__0_0\(21),
      I4 => ctrl_mux_out_2(0),
      I5 => \y_reg[23]_i_3__0_1\(21),
      O => \y_reg[21]_i_5__0_n_0\
    );
\y_reg[22]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000EEE222E2"
    )
        port map (
      I0 => \y_reg[22]_i_2__0_n_0\,
      I1 => ctrl_mux_out_2(3),
      I2 => \y_reg[22]\,
      I3 => ctrl_mux_out_2(2),
      I4 => \y_reg[22]_0\,
      I5 => ctrl_mux_out_2(4),
      O => D(22)
    );
\y_reg[22]_i_2__0\: unisim.vcomponents.MUXF7
     port map (
      I0 => \y_reg[22]_i_5__0_n_0\,
      I1 => \y_reg[22]_i_1__0_0\,
      O => \y_reg[22]_i_2__0_n_0\,
      S => ctrl_mux_out_2(2)
    );
\y_reg[22]_i_5__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_2[3]_7\(22),
      I1 => dout0_out(22),
      I2 => ctrl_mux_out_2(1),
      I3 => \y_reg[23]_i_3__0_0\(22),
      I4 => ctrl_mux_out_2(0),
      I5 => \y_reg[23]_i_3__0_1\(22),
      O => \y_reg[22]_i_5__0_n_0\
    );
\y_reg[23]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000EEE222E2"
    )
        port map (
      I0 => \y_reg[23]_i_3__0_n_0\,
      I1 => ctrl_mux_out_2(3),
      I2 => \y_reg[23]\,
      I3 => ctrl_mux_out_2(2),
      I4 => \y_reg[23]_0\,
      I5 => ctrl_mux_out_2(4),
      O => D(23)
    );
\y_reg[23]_i_3__0\: unisim.vcomponents.MUXF7
     port map (
      I0 => \y_reg[23]_i_6__0_n_0\,
      I1 => \y_reg[23]_i_1__0_0\,
      O => \y_reg[23]_i_3__0_n_0\,
      S => ctrl_mux_out_2(2)
    );
\y_reg[23]_i_6__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_2[3]_7\(23),
      I1 => dout0_out(23),
      I2 => ctrl_mux_out_2(1),
      I3 => \y_reg[23]_i_3__0_0\(23),
      I4 => ctrl_mux_out_2(0),
      I5 => \y_reg[23]_i_3__0_1\(23),
      O => \y_reg[23]_i_6__0_n_0\
    );
\y_reg[2]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000EEE222E2"
    )
        port map (
      I0 => \y_reg[2]_i_2__0_n_0\,
      I1 => ctrl_mux_out_2(3),
      I2 => \y_reg[2]\,
      I3 => ctrl_mux_out_2(2),
      I4 => \y_reg[2]_0\,
      I5 => ctrl_mux_out_2(4),
      O => D(2)
    );
\y_reg[2]_i_2__0\: unisim.vcomponents.MUXF7
     port map (
      I0 => \y_reg[2]_i_5__0_n_0\,
      I1 => \y_reg[2]_i_1__0_0\,
      O => \y_reg[2]_i_2__0_n_0\,
      S => ctrl_mux_out_2(2)
    );
\y_reg[2]_i_5__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_2[3]_7\(2),
      I1 => dout0_out(2),
      I2 => ctrl_mux_out_2(1),
      I3 => \y_reg[23]_i_3__0_0\(2),
      I4 => ctrl_mux_out_2(0),
      I5 => \y_reg[23]_i_3__0_1\(2),
      O => \y_reg[2]_i_5__0_n_0\
    );
\y_reg[3]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000EEE222E2"
    )
        port map (
      I0 => \y_reg[3]_i_2__0_n_0\,
      I1 => ctrl_mux_out_2(3),
      I2 => \y_reg[3]\,
      I3 => ctrl_mux_out_2(2),
      I4 => \y_reg[3]_0\,
      I5 => ctrl_mux_out_2(4),
      O => D(3)
    );
\y_reg[3]_i_2__0\: unisim.vcomponents.MUXF7
     port map (
      I0 => \y_reg[3]_i_5__0_n_0\,
      I1 => \y_reg[3]_i_1__0_0\,
      O => \y_reg[3]_i_2__0_n_0\,
      S => ctrl_mux_out_2(2)
    );
\y_reg[3]_i_5__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_2[3]_7\(3),
      I1 => dout0_out(3),
      I2 => ctrl_mux_out_2(1),
      I3 => \y_reg[23]_i_3__0_0\(3),
      I4 => ctrl_mux_out_2(0),
      I5 => \y_reg[23]_i_3__0_1\(3),
      O => \y_reg[3]_i_5__0_n_0\
    );
\y_reg[4]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000EEE222E2"
    )
        port map (
      I0 => \y_reg[4]_i_2__0_n_0\,
      I1 => ctrl_mux_out_2(3),
      I2 => \y_reg[4]\,
      I3 => ctrl_mux_out_2(2),
      I4 => \y_reg[4]_0\,
      I5 => ctrl_mux_out_2(4),
      O => D(4)
    );
\y_reg[4]_i_2__0\: unisim.vcomponents.MUXF7
     port map (
      I0 => \y_reg[4]_i_5__0_n_0\,
      I1 => \y_reg[4]_i_1__0_0\,
      O => \y_reg[4]_i_2__0_n_0\,
      S => ctrl_mux_out_2(2)
    );
\y_reg[4]_i_5__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_2[3]_7\(4),
      I1 => dout0_out(4),
      I2 => ctrl_mux_out_2(1),
      I3 => \y_reg[23]_i_3__0_0\(4),
      I4 => ctrl_mux_out_2(0),
      I5 => \y_reg[23]_i_3__0_1\(4),
      O => \y_reg[4]_i_5__0_n_0\
    );
\y_reg[5]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000EEE222E2"
    )
        port map (
      I0 => \y_reg[5]_i_2__0_n_0\,
      I1 => ctrl_mux_out_2(3),
      I2 => \y_reg[5]\,
      I3 => ctrl_mux_out_2(2),
      I4 => \y_reg[5]_0\,
      I5 => ctrl_mux_out_2(4),
      O => D(5)
    );
\y_reg[5]_i_2__0\: unisim.vcomponents.MUXF7
     port map (
      I0 => \y_reg[5]_i_5__0_n_0\,
      I1 => \y_reg[5]_i_1__0_0\,
      O => \y_reg[5]_i_2__0_n_0\,
      S => ctrl_mux_out_2(2)
    );
\y_reg[5]_i_5__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_2[3]_7\(5),
      I1 => dout0_out(5),
      I2 => ctrl_mux_out_2(1),
      I3 => \y_reg[23]_i_3__0_0\(5),
      I4 => ctrl_mux_out_2(0),
      I5 => \y_reg[23]_i_3__0_1\(5),
      O => \y_reg[5]_i_5__0_n_0\
    );
\y_reg[6]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000EEE222E2"
    )
        port map (
      I0 => \y_reg[6]_i_2__0_n_0\,
      I1 => ctrl_mux_out_2(3),
      I2 => \y_reg[6]\,
      I3 => ctrl_mux_out_2(2),
      I4 => \y_reg[6]_0\,
      I5 => ctrl_mux_out_2(4),
      O => D(6)
    );
\y_reg[6]_i_2__0\: unisim.vcomponents.MUXF7
     port map (
      I0 => \y_reg[6]_i_5__0_n_0\,
      I1 => \y_reg[6]_i_1__0_0\,
      O => \y_reg[6]_i_2__0_n_0\,
      S => ctrl_mux_out_2(2)
    );
\y_reg[6]_i_5__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_2[3]_7\(6),
      I1 => dout0_out(6),
      I2 => ctrl_mux_out_2(1),
      I3 => \y_reg[23]_i_3__0_0\(6),
      I4 => ctrl_mux_out_2(0),
      I5 => \y_reg[23]_i_3__0_1\(6),
      O => \y_reg[6]_i_5__0_n_0\
    );
\y_reg[7]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000EEE222E2"
    )
        port map (
      I0 => \y_reg[7]_i_2__0_n_0\,
      I1 => ctrl_mux_out_2(3),
      I2 => \y_reg[7]\,
      I3 => ctrl_mux_out_2(2),
      I4 => \y_reg[7]_0\,
      I5 => ctrl_mux_out_2(4),
      O => D(7)
    );
\y_reg[7]_i_2__0\: unisim.vcomponents.MUXF7
     port map (
      I0 => \y_reg[7]_i_5__0_n_0\,
      I1 => \y_reg[7]_i_1__0_0\,
      O => \y_reg[7]_i_2__0_n_0\,
      S => ctrl_mux_out_2(2)
    );
\y_reg[7]_i_5__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_2[3]_7\(7),
      I1 => dout0_out(7),
      I2 => ctrl_mux_out_2(1),
      I3 => \y_reg[23]_i_3__0_0\(7),
      I4 => ctrl_mux_out_2(0),
      I5 => \y_reg[23]_i_3__0_1\(7),
      O => \y_reg[7]_i_5__0_n_0\
    );
\y_reg[8]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000EEE222E2"
    )
        port map (
      I0 => \y_reg[8]_i_2__0_n_0\,
      I1 => ctrl_mux_out_2(3),
      I2 => \y_reg[8]\,
      I3 => ctrl_mux_out_2(2),
      I4 => \y_reg[8]_0\,
      I5 => ctrl_mux_out_2(4),
      O => D(8)
    );
\y_reg[8]_i_2__0\: unisim.vcomponents.MUXF7
     port map (
      I0 => \y_reg[8]_i_5__0_n_0\,
      I1 => \y_reg[8]_i_1__0_0\,
      O => \y_reg[8]_i_2__0_n_0\,
      S => ctrl_mux_out_2(2)
    );
\y_reg[8]_i_5__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_2[3]_7\(8),
      I1 => dout0_out(8),
      I2 => ctrl_mux_out_2(1),
      I3 => \y_reg[23]_i_3__0_0\(8),
      I4 => ctrl_mux_out_2(0),
      I5 => \y_reg[23]_i_3__0_1\(8),
      O => \y_reg[8]_i_5__0_n_0\
    );
\y_reg[9]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000EEE222E2"
    )
        port map (
      I0 => \y_reg[9]_i_2__0_n_0\,
      I1 => ctrl_mux_out_2(3),
      I2 => \y_reg[9]\,
      I3 => ctrl_mux_out_2(2),
      I4 => \y_reg[9]_0\,
      I5 => ctrl_mux_out_2(4),
      O => D(9)
    );
\y_reg[9]_i_2__0\: unisim.vcomponents.MUXF7
     port map (
      I0 => \y_reg[9]_i_5__0_n_0\,
      I1 => \y_reg[9]_i_1__0_0\,
      O => \y_reg[9]_i_2__0_n_0\,
      S => ctrl_mux_out_2(2)
    );
\y_reg[9]_i_5__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_2[3]_7\(9),
      I1 => dout0_out(9),
      I2 => ctrl_mux_out_2(1),
      I3 => \y_reg[23]_i_3__0_0\(9),
      I4 => ctrl_mux_out_2(0),
      I5 => \y_reg[23]_i_3__0_1\(9),
      O => \y_reg[9]_i_5__0_n_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity test_bd_pixelReOrder_0_0_rams_sp_rf_rst_19 is
  port (
    dout0_out : out STD_LOGIC_VECTOR ( 23 downto 0 );
    m00_axis_aclk : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 0 to 0 );
    addr : in STD_LOGIC_VECTOR ( 10 downto 0 );
    s00_axis_tdata : in STD_LOGIC_VECTOR ( 23 downto 0 );
    s00_axis_tuser : in STD_LOGIC_VECTOR ( 0 to 0 );
    s00_axis_tvalid : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of test_bd_pixelReOrder_0_0_rams_sp_rf_rst_19 : entity is "rams_sp_rf_rst";
end test_bd_pixelReOrder_0_0_rams_sp_rf_rst_19;

architecture STRUCTURE of test_bd_pixelReOrder_0_0_rams_sp_rf_rst_19 is
  signal \ram_reg_0_i_1__5_n_0\ : STD_LOGIC;
  signal NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_DBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_SBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_DOADO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 16 );
  signal NLW_ram_reg_0_DOBDO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_ram_reg_0_DOPADOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal NLW_ram_reg_0_DOPBDOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_ram_reg_0_ECCPARITY_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal NLW_ram_reg_0_RDADDRECC_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_ram_reg_1_DOADO_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 6 );
  signal NLW_ram_reg_1_DOBDO_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal NLW_ram_reg_1_DOPADOP_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_ram_reg_1_DOPBDOP_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ : string;
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of ram_reg_0 : label is "p2_d16";
  attribute METHODOLOGY_DRC_VIOS : string;
  attribute METHODOLOGY_DRC_VIOS of ram_reg_0 : label is "{SYNTH-6 {cell *THIS*}}";
  attribute RTL_RAM_BITS : integer;
  attribute RTL_RAM_BITS of ram_reg_0 : label is 46080;
  attribute RTL_RAM_NAME : string;
  attribute RTL_RAM_NAME of ram_reg_0 : label is "ram";
  attribute bram_addr_begin : integer;
  attribute bram_addr_begin of ram_reg_0 : label is 0;
  attribute bram_addr_end : integer;
  attribute bram_addr_end of ram_reg_0 : label is 2047;
  attribute bram_slice_begin : integer;
  attribute bram_slice_begin of ram_reg_0 : label is 0;
  attribute bram_slice_end : integer;
  attribute bram_slice_end of ram_reg_0 : label is 17;
  attribute ram_addr_begin : integer;
  attribute ram_addr_begin of ram_reg_0 : label is 0;
  attribute ram_addr_end : integer;
  attribute ram_addr_end of ram_reg_0 : label is 2047;
  attribute ram_offset : integer;
  attribute ram_offset of ram_reg_0 : label is 0;
  attribute ram_slice_begin : integer;
  attribute ram_slice_begin of ram_reg_0 : label is 0;
  attribute ram_slice_end : integer;
  attribute ram_slice_end of ram_reg_0 : label is 17;
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of ram_reg_1 : label is "p0_d6";
  attribute METHODOLOGY_DRC_VIOS of ram_reg_1 : label is "{SYNTH-6 {cell *THIS*}}";
  attribute RTL_RAM_BITS of ram_reg_1 : label is 46080;
  attribute RTL_RAM_NAME of ram_reg_1 : label is "ram";
  attribute bram_addr_begin of ram_reg_1 : label is 0;
  attribute bram_addr_end of ram_reg_1 : label is 2047;
  attribute bram_slice_begin of ram_reg_1 : label is 18;
  attribute bram_slice_end of ram_reg_1 : label is 23;
  attribute ram_addr_begin of ram_reg_1 : label is 0;
  attribute ram_addr_end of ram_reg_1 : label is 2047;
  attribute ram_offset of ram_reg_1 : label is 0;
  attribute ram_slice_begin of ram_reg_1 : label is 18;
  attribute ram_slice_end of ram_reg_1 : label is 23;
begin
ram_reg_0: unisim.vcomponents.RAMB36E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      EN_ECC_READ => false,
      EN_ECC_WRITE => false,
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      RAM_EXTENSION_A => "NONE",
      RAM_EXTENSION_B => "NONE",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "PERFORMANCE",
      READ_WIDTH_A => 18,
      READ_WIDTH_B => 0,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "READ_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 18,
      WRITE_WIDTH_B => 0
    )
        port map (
      ADDRARDADDR(15) => '1',
      ADDRARDADDR(14 downto 4) => addr(10 downto 0),
      ADDRARDADDR(3 downto 0) => B"1111",
      ADDRBWRADDR(15 downto 0) => B"1111111111111111",
      CASCADEINA => '1',
      CASCADEINB => '0',
      CASCADEOUTA => NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED,
      CASCADEOUTB => NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED,
      CLKARDCLK => m00_axis_aclk,
      CLKBWRCLK => '0',
      DBITERR => NLW_ram_reg_0_DBITERR_UNCONNECTED,
      DIADI(31 downto 16) => B"0000000000000000",
      DIADI(15 downto 0) => s00_axis_tdata(15 downto 0),
      DIBDI(31 downto 0) => B"11111111111111111111111111111111",
      DIPADIP(3 downto 2) => B"00",
      DIPADIP(1 downto 0) => s00_axis_tdata(17 downto 16),
      DIPBDIP(3 downto 0) => B"1111",
      DOADO(31 downto 16) => NLW_ram_reg_0_DOADO_UNCONNECTED(31 downto 16),
      DOADO(15 downto 0) => dout0_out(15 downto 0),
      DOBDO(31 downto 0) => NLW_ram_reg_0_DOBDO_UNCONNECTED(31 downto 0),
      DOPADOP(3 downto 2) => NLW_ram_reg_0_DOPADOP_UNCONNECTED(3 downto 2),
      DOPADOP(1 downto 0) => dout0_out(17 downto 16),
      DOPBDOP(3 downto 0) => NLW_ram_reg_0_DOPBDOP_UNCONNECTED(3 downto 0),
      ECCPARITY(7 downto 0) => NLW_ram_reg_0_ECCPARITY_UNCONNECTED(7 downto 0),
      ENARDEN => Q(0),
      ENBWREN => '0',
      INJECTDBITERR => NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED,
      INJECTSBITERR => NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED,
      RDADDRECC(8 downto 0) => NLW_ram_reg_0_RDADDRECC_UNCONNECTED(8 downto 0),
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      SBITERR => NLW_ram_reg_0_SBITERR_UNCONNECTED,
      WEA(3) => \ram_reg_0_i_1__5_n_0\,
      WEA(2) => \ram_reg_0_i_1__5_n_0\,
      WEA(1) => \ram_reg_0_i_1__5_n_0\,
      WEA(0) => \ram_reg_0_i_1__5_n_0\,
      WEBWE(7 downto 0) => B"00000000"
    );
\ram_reg_0_i_1__5\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => Q(0),
      I1 => s00_axis_tuser(0),
      I2 => s00_axis_tvalid,
      O => \ram_reg_0_i_1__5_n_0\
    );
ram_reg_1: unisim.vcomponents.RAMB18E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      INIT_A => X"00000",
      INIT_B => X"00000",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "PERFORMANCE",
      READ_WIDTH_A => 9,
      READ_WIDTH_B => 0,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"00000",
      SRVAL_B => X"00000",
      WRITE_MODE_A => "READ_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 9,
      WRITE_WIDTH_B => 0
    )
        port map (
      ADDRARDADDR(13 downto 3) => addr(10 downto 0),
      ADDRARDADDR(2 downto 0) => B"111",
      ADDRBWRADDR(13 downto 0) => B"11111111111111",
      CLKARDCLK => m00_axis_aclk,
      CLKBWRCLK => '0',
      DIADI(15 downto 6) => B"0000000000",
      DIADI(5 downto 0) => s00_axis_tdata(23 downto 18),
      DIBDI(15 downto 0) => B"1111111111111111",
      DIPADIP(1 downto 0) => B"00",
      DIPBDIP(1 downto 0) => B"11",
      DOADO(15 downto 6) => NLW_ram_reg_1_DOADO_UNCONNECTED(15 downto 6),
      DOADO(5 downto 0) => dout0_out(23 downto 18),
      DOBDO(15 downto 0) => NLW_ram_reg_1_DOBDO_UNCONNECTED(15 downto 0),
      DOPADOP(1 downto 0) => NLW_ram_reg_1_DOPADOP_UNCONNECTED(1 downto 0),
      DOPBDOP(1 downto 0) => NLW_ram_reg_1_DOPBDOP_UNCONNECTED(1 downto 0),
      ENARDEN => Q(0),
      ENBWREN => '0',
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      WEA(1) => \ram_reg_0_i_1__5_n_0\,
      WEA(0) => \ram_reg_0_i_1__5_n_0\,
      WEBWE(3 downto 0) => B"0000"
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity test_bd_pixelReOrder_0_0_rams_sp_rf_rst_2 is
  port (
    dout0_out : out STD_LOGIC_VECTOR ( 23 downto 0 );
    m00_axis_aclk : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 0 to 0 );
    addr : in STD_LOGIC_VECTOR ( 10 downto 0 );
    s00_axis_tdata : in STD_LOGIC_VECTOR ( 23 downto 0 );
    s00_axis_tuser : in STD_LOGIC_VECTOR ( 0 to 0 );
    s00_axis_tvalid : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of test_bd_pixelReOrder_0_0_rams_sp_rf_rst_2 : entity is "rams_sp_rf_rst";
end test_bd_pixelReOrder_0_0_rams_sp_rf_rst_2;

architecture STRUCTURE of test_bd_pixelReOrder_0_0_rams_sp_rf_rst_2 is
  signal \ram_reg_0_i_1__18_n_0\ : STD_LOGIC;
  signal NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_DBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_SBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_DOADO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 16 );
  signal NLW_ram_reg_0_DOBDO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_ram_reg_0_DOPADOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal NLW_ram_reg_0_DOPBDOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_ram_reg_0_ECCPARITY_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal NLW_ram_reg_0_RDADDRECC_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_ram_reg_1_DOADO_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 6 );
  signal NLW_ram_reg_1_DOBDO_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal NLW_ram_reg_1_DOPADOP_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_ram_reg_1_DOPBDOP_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ : string;
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of ram_reg_0 : label is "p2_d16";
  attribute METHODOLOGY_DRC_VIOS : string;
  attribute METHODOLOGY_DRC_VIOS of ram_reg_0 : label is "{SYNTH-6 {cell *THIS*}}";
  attribute RTL_RAM_BITS : integer;
  attribute RTL_RAM_BITS of ram_reg_0 : label is 46080;
  attribute RTL_RAM_NAME : string;
  attribute RTL_RAM_NAME of ram_reg_0 : label is "ram";
  attribute bram_addr_begin : integer;
  attribute bram_addr_begin of ram_reg_0 : label is 0;
  attribute bram_addr_end : integer;
  attribute bram_addr_end of ram_reg_0 : label is 2047;
  attribute bram_slice_begin : integer;
  attribute bram_slice_begin of ram_reg_0 : label is 0;
  attribute bram_slice_end : integer;
  attribute bram_slice_end of ram_reg_0 : label is 17;
  attribute ram_addr_begin : integer;
  attribute ram_addr_begin of ram_reg_0 : label is 0;
  attribute ram_addr_end : integer;
  attribute ram_addr_end of ram_reg_0 : label is 2047;
  attribute ram_offset : integer;
  attribute ram_offset of ram_reg_0 : label is 0;
  attribute ram_slice_begin : integer;
  attribute ram_slice_begin of ram_reg_0 : label is 0;
  attribute ram_slice_end : integer;
  attribute ram_slice_end of ram_reg_0 : label is 17;
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of ram_reg_1 : label is "p0_d6";
  attribute METHODOLOGY_DRC_VIOS of ram_reg_1 : label is "{SYNTH-6 {cell *THIS*}}";
  attribute RTL_RAM_BITS of ram_reg_1 : label is 46080;
  attribute RTL_RAM_NAME of ram_reg_1 : label is "ram";
  attribute bram_addr_begin of ram_reg_1 : label is 0;
  attribute bram_addr_end of ram_reg_1 : label is 2047;
  attribute bram_slice_begin of ram_reg_1 : label is 18;
  attribute bram_slice_end of ram_reg_1 : label is 23;
  attribute ram_addr_begin of ram_reg_1 : label is 0;
  attribute ram_addr_end of ram_reg_1 : label is 2047;
  attribute ram_offset of ram_reg_1 : label is 0;
  attribute ram_slice_begin of ram_reg_1 : label is 18;
  attribute ram_slice_end of ram_reg_1 : label is 23;
begin
ram_reg_0: unisim.vcomponents.RAMB36E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      EN_ECC_READ => false,
      EN_ECC_WRITE => false,
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      RAM_EXTENSION_A => "NONE",
      RAM_EXTENSION_B => "NONE",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "PERFORMANCE",
      READ_WIDTH_A => 18,
      READ_WIDTH_B => 0,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "READ_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 18,
      WRITE_WIDTH_B => 0
    )
        port map (
      ADDRARDADDR(15) => '1',
      ADDRARDADDR(14 downto 4) => addr(10 downto 0),
      ADDRARDADDR(3 downto 0) => B"1111",
      ADDRBWRADDR(15 downto 0) => B"1111111111111111",
      CASCADEINA => '1',
      CASCADEINB => '0',
      CASCADEOUTA => NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED,
      CASCADEOUTB => NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED,
      CLKARDCLK => m00_axis_aclk,
      CLKBWRCLK => '0',
      DBITERR => NLW_ram_reg_0_DBITERR_UNCONNECTED,
      DIADI(31 downto 16) => B"0000000000000000",
      DIADI(15 downto 0) => s00_axis_tdata(15 downto 0),
      DIBDI(31 downto 0) => B"11111111111111111111111111111111",
      DIPADIP(3 downto 2) => B"00",
      DIPADIP(1 downto 0) => s00_axis_tdata(17 downto 16),
      DIPBDIP(3 downto 0) => B"1111",
      DOADO(31 downto 16) => NLW_ram_reg_0_DOADO_UNCONNECTED(31 downto 16),
      DOADO(15 downto 0) => dout0_out(15 downto 0),
      DOBDO(31 downto 0) => NLW_ram_reg_0_DOBDO_UNCONNECTED(31 downto 0),
      DOPADOP(3 downto 2) => NLW_ram_reg_0_DOPADOP_UNCONNECTED(3 downto 2),
      DOPADOP(1 downto 0) => dout0_out(17 downto 16),
      DOPBDOP(3 downto 0) => NLW_ram_reg_0_DOPBDOP_UNCONNECTED(3 downto 0),
      ECCPARITY(7 downto 0) => NLW_ram_reg_0_ECCPARITY_UNCONNECTED(7 downto 0),
      ENARDEN => Q(0),
      ENBWREN => '0',
      INJECTDBITERR => NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED,
      INJECTSBITERR => NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED,
      RDADDRECC(8 downto 0) => NLW_ram_reg_0_RDADDRECC_UNCONNECTED(8 downto 0),
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      SBITERR => NLW_ram_reg_0_SBITERR_UNCONNECTED,
      WEA(3) => \ram_reg_0_i_1__18_n_0\,
      WEA(2) => \ram_reg_0_i_1__18_n_0\,
      WEA(1) => \ram_reg_0_i_1__18_n_0\,
      WEA(0) => \ram_reg_0_i_1__18_n_0\,
      WEBWE(7 downto 0) => B"00000000"
    );
\ram_reg_0_i_1__18\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => Q(0),
      I1 => s00_axis_tuser(0),
      I2 => s00_axis_tvalid,
      O => \ram_reg_0_i_1__18_n_0\
    );
ram_reg_1: unisim.vcomponents.RAMB18E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      INIT_A => X"00000",
      INIT_B => X"00000",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "PERFORMANCE",
      READ_WIDTH_A => 9,
      READ_WIDTH_B => 0,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"00000",
      SRVAL_B => X"00000",
      WRITE_MODE_A => "READ_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 9,
      WRITE_WIDTH_B => 0
    )
        port map (
      ADDRARDADDR(13 downto 3) => addr(10 downto 0),
      ADDRARDADDR(2 downto 0) => B"111",
      ADDRBWRADDR(13 downto 0) => B"11111111111111",
      CLKARDCLK => m00_axis_aclk,
      CLKBWRCLK => '0',
      DIADI(15 downto 6) => B"0000000000",
      DIADI(5 downto 0) => s00_axis_tdata(23 downto 18),
      DIBDI(15 downto 0) => B"1111111111111111",
      DIPADIP(1 downto 0) => B"00",
      DIPBDIP(1 downto 0) => B"11",
      DOADO(15 downto 6) => NLW_ram_reg_1_DOADO_UNCONNECTED(15 downto 6),
      DOADO(5 downto 0) => dout0_out(23 downto 18),
      DOBDO(15 downto 0) => NLW_ram_reg_1_DOBDO_UNCONNECTED(15 downto 0),
      DOPADOP(1 downto 0) => NLW_ram_reg_1_DOPADOP_UNCONNECTED(1 downto 0),
      DOPBDOP(1 downto 0) => NLW_ram_reg_1_DOPBDOP_UNCONNECTED(1 downto 0),
      ENARDEN => Q(0),
      ENBWREN => '0',
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      WEA(1) => \ram_reg_0_i_1__18_n_0\,
      WEA(0) => \ram_reg_0_i_1__18_n_0\,
      WEBWE(3 downto 0) => B"0000"
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity test_bd_pixelReOrder_0_0_rams_sp_rf_rst_20 is
  port (
    dout0_out : out STD_LOGIC_VECTOR ( 23 downto 0 );
    m00_axis_aclk : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 0 to 0 );
    addr : in STD_LOGIC_VECTOR ( 10 downto 0 );
    s00_axis_tdata : in STD_LOGIC_VECTOR ( 23 downto 0 );
    s00_axis_tuser : in STD_LOGIC_VECTOR ( 0 to 0 );
    s00_axis_tvalid : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of test_bd_pixelReOrder_0_0_rams_sp_rf_rst_20 : entity is "rams_sp_rf_rst";
end test_bd_pixelReOrder_0_0_rams_sp_rf_rst_20;

architecture STRUCTURE of test_bd_pixelReOrder_0_0_rams_sp_rf_rst_20 is
  signal \ram_reg_0_i_1__6_n_0\ : STD_LOGIC;
  signal NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_DBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_SBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_DOADO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 16 );
  signal NLW_ram_reg_0_DOBDO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_ram_reg_0_DOPADOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal NLW_ram_reg_0_DOPBDOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_ram_reg_0_ECCPARITY_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal NLW_ram_reg_0_RDADDRECC_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_ram_reg_1_DOADO_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 6 );
  signal NLW_ram_reg_1_DOBDO_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal NLW_ram_reg_1_DOPADOP_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_ram_reg_1_DOPBDOP_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ : string;
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of ram_reg_0 : label is "p2_d16";
  attribute METHODOLOGY_DRC_VIOS : string;
  attribute METHODOLOGY_DRC_VIOS of ram_reg_0 : label is "{SYNTH-6 {cell *THIS*}}";
  attribute RTL_RAM_BITS : integer;
  attribute RTL_RAM_BITS of ram_reg_0 : label is 46080;
  attribute RTL_RAM_NAME : string;
  attribute RTL_RAM_NAME of ram_reg_0 : label is "ram";
  attribute bram_addr_begin : integer;
  attribute bram_addr_begin of ram_reg_0 : label is 0;
  attribute bram_addr_end : integer;
  attribute bram_addr_end of ram_reg_0 : label is 2047;
  attribute bram_slice_begin : integer;
  attribute bram_slice_begin of ram_reg_0 : label is 0;
  attribute bram_slice_end : integer;
  attribute bram_slice_end of ram_reg_0 : label is 17;
  attribute ram_addr_begin : integer;
  attribute ram_addr_begin of ram_reg_0 : label is 0;
  attribute ram_addr_end : integer;
  attribute ram_addr_end of ram_reg_0 : label is 2047;
  attribute ram_offset : integer;
  attribute ram_offset of ram_reg_0 : label is 0;
  attribute ram_slice_begin : integer;
  attribute ram_slice_begin of ram_reg_0 : label is 0;
  attribute ram_slice_end : integer;
  attribute ram_slice_end of ram_reg_0 : label is 17;
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of ram_reg_1 : label is "p0_d6";
  attribute METHODOLOGY_DRC_VIOS of ram_reg_1 : label is "{SYNTH-6 {cell *THIS*}}";
  attribute RTL_RAM_BITS of ram_reg_1 : label is 46080;
  attribute RTL_RAM_NAME of ram_reg_1 : label is "ram";
  attribute bram_addr_begin of ram_reg_1 : label is 0;
  attribute bram_addr_end of ram_reg_1 : label is 2047;
  attribute bram_slice_begin of ram_reg_1 : label is 18;
  attribute bram_slice_end of ram_reg_1 : label is 23;
  attribute ram_addr_begin of ram_reg_1 : label is 0;
  attribute ram_addr_end of ram_reg_1 : label is 2047;
  attribute ram_offset of ram_reg_1 : label is 0;
  attribute ram_slice_begin of ram_reg_1 : label is 18;
  attribute ram_slice_end of ram_reg_1 : label is 23;
begin
ram_reg_0: unisim.vcomponents.RAMB36E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      EN_ECC_READ => false,
      EN_ECC_WRITE => false,
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      RAM_EXTENSION_A => "NONE",
      RAM_EXTENSION_B => "NONE",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "PERFORMANCE",
      READ_WIDTH_A => 18,
      READ_WIDTH_B => 0,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "READ_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 18,
      WRITE_WIDTH_B => 0
    )
        port map (
      ADDRARDADDR(15) => '1',
      ADDRARDADDR(14 downto 4) => addr(10 downto 0),
      ADDRARDADDR(3 downto 0) => B"1111",
      ADDRBWRADDR(15 downto 0) => B"1111111111111111",
      CASCADEINA => '1',
      CASCADEINB => '0',
      CASCADEOUTA => NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED,
      CASCADEOUTB => NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED,
      CLKARDCLK => m00_axis_aclk,
      CLKBWRCLK => '0',
      DBITERR => NLW_ram_reg_0_DBITERR_UNCONNECTED,
      DIADI(31 downto 16) => B"0000000000000000",
      DIADI(15 downto 0) => s00_axis_tdata(15 downto 0),
      DIBDI(31 downto 0) => B"11111111111111111111111111111111",
      DIPADIP(3 downto 2) => B"00",
      DIPADIP(1 downto 0) => s00_axis_tdata(17 downto 16),
      DIPBDIP(3 downto 0) => B"1111",
      DOADO(31 downto 16) => NLW_ram_reg_0_DOADO_UNCONNECTED(31 downto 16),
      DOADO(15 downto 0) => dout0_out(15 downto 0),
      DOBDO(31 downto 0) => NLW_ram_reg_0_DOBDO_UNCONNECTED(31 downto 0),
      DOPADOP(3 downto 2) => NLW_ram_reg_0_DOPADOP_UNCONNECTED(3 downto 2),
      DOPADOP(1 downto 0) => dout0_out(17 downto 16),
      DOPBDOP(3 downto 0) => NLW_ram_reg_0_DOPBDOP_UNCONNECTED(3 downto 0),
      ECCPARITY(7 downto 0) => NLW_ram_reg_0_ECCPARITY_UNCONNECTED(7 downto 0),
      ENARDEN => Q(0),
      ENBWREN => '0',
      INJECTDBITERR => NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED,
      INJECTSBITERR => NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED,
      RDADDRECC(8 downto 0) => NLW_ram_reg_0_RDADDRECC_UNCONNECTED(8 downto 0),
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      SBITERR => NLW_ram_reg_0_SBITERR_UNCONNECTED,
      WEA(3) => \ram_reg_0_i_1__6_n_0\,
      WEA(2) => \ram_reg_0_i_1__6_n_0\,
      WEA(1) => \ram_reg_0_i_1__6_n_0\,
      WEA(0) => \ram_reg_0_i_1__6_n_0\,
      WEBWE(7 downto 0) => B"00000000"
    );
\ram_reg_0_i_1__6\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => Q(0),
      I1 => s00_axis_tuser(0),
      I2 => s00_axis_tvalid,
      O => \ram_reg_0_i_1__6_n_0\
    );
ram_reg_1: unisim.vcomponents.RAMB18E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      INIT_A => X"00000",
      INIT_B => X"00000",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "PERFORMANCE",
      READ_WIDTH_A => 9,
      READ_WIDTH_B => 0,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"00000",
      SRVAL_B => X"00000",
      WRITE_MODE_A => "READ_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 9,
      WRITE_WIDTH_B => 0
    )
        port map (
      ADDRARDADDR(13 downto 3) => addr(10 downto 0),
      ADDRARDADDR(2 downto 0) => B"111",
      ADDRBWRADDR(13 downto 0) => B"11111111111111",
      CLKARDCLK => m00_axis_aclk,
      CLKBWRCLK => '0',
      DIADI(15 downto 6) => B"0000000000",
      DIADI(5 downto 0) => s00_axis_tdata(23 downto 18),
      DIBDI(15 downto 0) => B"1111111111111111",
      DIPADIP(1 downto 0) => B"00",
      DIPBDIP(1 downto 0) => B"11",
      DOADO(15 downto 6) => NLW_ram_reg_1_DOADO_UNCONNECTED(15 downto 6),
      DOADO(5 downto 0) => dout0_out(23 downto 18),
      DOBDO(15 downto 0) => NLW_ram_reg_1_DOBDO_UNCONNECTED(15 downto 0),
      DOPADOP(1 downto 0) => NLW_ram_reg_1_DOPADOP_UNCONNECTED(1 downto 0),
      DOPBDOP(1 downto 0) => NLW_ram_reg_1_DOPBDOP_UNCONNECTED(1 downto 0),
      ENARDEN => Q(0),
      ENBWREN => '0',
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      WEA(1) => \ram_reg_0_i_1__6_n_0\,
      WEA(0) => \ram_reg_0_i_1__6_n_0\,
      WEBWE(3 downto 0) => B"0000"
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity test_bd_pixelReOrder_0_0_rams_sp_rf_rst_21 is
  port (
    dout0_out : out STD_LOGIC_VECTOR ( 23 downto 0 );
    m00_axis_aclk : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 0 to 0 );
    addr : in STD_LOGIC_VECTOR ( 10 downto 0 );
    s00_axis_tdata : in STD_LOGIC_VECTOR ( 23 downto 0 );
    s00_axis_tuser : in STD_LOGIC_VECTOR ( 0 to 0 );
    s00_axis_tvalid : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of test_bd_pixelReOrder_0_0_rams_sp_rf_rst_21 : entity is "rams_sp_rf_rst";
end test_bd_pixelReOrder_0_0_rams_sp_rf_rst_21;

architecture STRUCTURE of test_bd_pixelReOrder_0_0_rams_sp_rf_rst_21 is
  signal \ram_reg_0_i_1__7_n_0\ : STD_LOGIC;
  signal NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_DBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_SBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_DOADO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 16 );
  signal NLW_ram_reg_0_DOBDO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_ram_reg_0_DOPADOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal NLW_ram_reg_0_DOPBDOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_ram_reg_0_ECCPARITY_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal NLW_ram_reg_0_RDADDRECC_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_ram_reg_1_DOADO_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 6 );
  signal NLW_ram_reg_1_DOBDO_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal NLW_ram_reg_1_DOPADOP_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_ram_reg_1_DOPBDOP_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ : string;
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of ram_reg_0 : label is "p2_d16";
  attribute METHODOLOGY_DRC_VIOS : string;
  attribute METHODOLOGY_DRC_VIOS of ram_reg_0 : label is "{SYNTH-6 {cell *THIS*}}";
  attribute RTL_RAM_BITS : integer;
  attribute RTL_RAM_BITS of ram_reg_0 : label is 46080;
  attribute RTL_RAM_NAME : string;
  attribute RTL_RAM_NAME of ram_reg_0 : label is "ram";
  attribute bram_addr_begin : integer;
  attribute bram_addr_begin of ram_reg_0 : label is 0;
  attribute bram_addr_end : integer;
  attribute bram_addr_end of ram_reg_0 : label is 2047;
  attribute bram_slice_begin : integer;
  attribute bram_slice_begin of ram_reg_0 : label is 0;
  attribute bram_slice_end : integer;
  attribute bram_slice_end of ram_reg_0 : label is 17;
  attribute ram_addr_begin : integer;
  attribute ram_addr_begin of ram_reg_0 : label is 0;
  attribute ram_addr_end : integer;
  attribute ram_addr_end of ram_reg_0 : label is 2047;
  attribute ram_offset : integer;
  attribute ram_offset of ram_reg_0 : label is 0;
  attribute ram_slice_begin : integer;
  attribute ram_slice_begin of ram_reg_0 : label is 0;
  attribute ram_slice_end : integer;
  attribute ram_slice_end of ram_reg_0 : label is 17;
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of ram_reg_1 : label is "p0_d6";
  attribute METHODOLOGY_DRC_VIOS of ram_reg_1 : label is "{SYNTH-6 {cell *THIS*}}";
  attribute RTL_RAM_BITS of ram_reg_1 : label is 46080;
  attribute RTL_RAM_NAME of ram_reg_1 : label is "ram";
  attribute bram_addr_begin of ram_reg_1 : label is 0;
  attribute bram_addr_end of ram_reg_1 : label is 2047;
  attribute bram_slice_begin of ram_reg_1 : label is 18;
  attribute bram_slice_end of ram_reg_1 : label is 23;
  attribute ram_addr_begin of ram_reg_1 : label is 0;
  attribute ram_addr_end of ram_reg_1 : label is 2047;
  attribute ram_offset of ram_reg_1 : label is 0;
  attribute ram_slice_begin of ram_reg_1 : label is 18;
  attribute ram_slice_end of ram_reg_1 : label is 23;
begin
ram_reg_0: unisim.vcomponents.RAMB36E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      EN_ECC_READ => false,
      EN_ECC_WRITE => false,
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      RAM_EXTENSION_A => "NONE",
      RAM_EXTENSION_B => "NONE",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "PERFORMANCE",
      READ_WIDTH_A => 18,
      READ_WIDTH_B => 0,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "READ_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 18,
      WRITE_WIDTH_B => 0
    )
        port map (
      ADDRARDADDR(15) => '1',
      ADDRARDADDR(14 downto 4) => addr(10 downto 0),
      ADDRARDADDR(3 downto 0) => B"1111",
      ADDRBWRADDR(15 downto 0) => B"1111111111111111",
      CASCADEINA => '1',
      CASCADEINB => '0',
      CASCADEOUTA => NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED,
      CASCADEOUTB => NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED,
      CLKARDCLK => m00_axis_aclk,
      CLKBWRCLK => '0',
      DBITERR => NLW_ram_reg_0_DBITERR_UNCONNECTED,
      DIADI(31 downto 16) => B"0000000000000000",
      DIADI(15 downto 0) => s00_axis_tdata(15 downto 0),
      DIBDI(31 downto 0) => B"11111111111111111111111111111111",
      DIPADIP(3 downto 2) => B"00",
      DIPADIP(1 downto 0) => s00_axis_tdata(17 downto 16),
      DIPBDIP(3 downto 0) => B"1111",
      DOADO(31 downto 16) => NLW_ram_reg_0_DOADO_UNCONNECTED(31 downto 16),
      DOADO(15 downto 0) => dout0_out(15 downto 0),
      DOBDO(31 downto 0) => NLW_ram_reg_0_DOBDO_UNCONNECTED(31 downto 0),
      DOPADOP(3 downto 2) => NLW_ram_reg_0_DOPADOP_UNCONNECTED(3 downto 2),
      DOPADOP(1 downto 0) => dout0_out(17 downto 16),
      DOPBDOP(3 downto 0) => NLW_ram_reg_0_DOPBDOP_UNCONNECTED(3 downto 0),
      ECCPARITY(7 downto 0) => NLW_ram_reg_0_ECCPARITY_UNCONNECTED(7 downto 0),
      ENARDEN => Q(0),
      ENBWREN => '0',
      INJECTDBITERR => NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED,
      INJECTSBITERR => NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED,
      RDADDRECC(8 downto 0) => NLW_ram_reg_0_RDADDRECC_UNCONNECTED(8 downto 0),
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      SBITERR => NLW_ram_reg_0_SBITERR_UNCONNECTED,
      WEA(3) => \ram_reg_0_i_1__7_n_0\,
      WEA(2) => \ram_reg_0_i_1__7_n_0\,
      WEA(1) => \ram_reg_0_i_1__7_n_0\,
      WEA(0) => \ram_reg_0_i_1__7_n_0\,
      WEBWE(7 downto 0) => B"00000000"
    );
\ram_reg_0_i_1__7\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => Q(0),
      I1 => s00_axis_tuser(0),
      I2 => s00_axis_tvalid,
      O => \ram_reg_0_i_1__7_n_0\
    );
ram_reg_1: unisim.vcomponents.RAMB18E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      INIT_A => X"00000",
      INIT_B => X"00000",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "PERFORMANCE",
      READ_WIDTH_A => 9,
      READ_WIDTH_B => 0,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"00000",
      SRVAL_B => X"00000",
      WRITE_MODE_A => "READ_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 9,
      WRITE_WIDTH_B => 0
    )
        port map (
      ADDRARDADDR(13 downto 3) => addr(10 downto 0),
      ADDRARDADDR(2 downto 0) => B"111",
      ADDRBWRADDR(13 downto 0) => B"11111111111111",
      CLKARDCLK => m00_axis_aclk,
      CLKBWRCLK => '0',
      DIADI(15 downto 6) => B"0000000000",
      DIADI(5 downto 0) => s00_axis_tdata(23 downto 18),
      DIBDI(15 downto 0) => B"1111111111111111",
      DIPADIP(1 downto 0) => B"00",
      DIPBDIP(1 downto 0) => B"11",
      DOADO(15 downto 6) => NLW_ram_reg_1_DOADO_UNCONNECTED(15 downto 6),
      DOADO(5 downto 0) => dout0_out(23 downto 18),
      DOBDO(15 downto 0) => NLW_ram_reg_1_DOBDO_UNCONNECTED(15 downto 0),
      DOPADOP(1 downto 0) => NLW_ram_reg_1_DOPADOP_UNCONNECTED(1 downto 0),
      DOPBDOP(1 downto 0) => NLW_ram_reg_1_DOPBDOP_UNCONNECTED(1 downto 0),
      ENARDEN => Q(0),
      ENBWREN => '0',
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      WEA(1) => \ram_reg_0_i_1__7_n_0\,
      WEA(0) => \ram_reg_0_i_1__7_n_0\,
      WEBWE(3 downto 0) => B"0000"
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity test_bd_pixelReOrder_0_0_rams_sp_rf_rst_22 is
  port (
    dout0_out : out STD_LOGIC_VECTOR ( 23 downto 0 );
    m00_axis_aclk : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 0 to 0 );
    addr : in STD_LOGIC_VECTOR ( 10 downto 0 );
    s00_axis_tdata : in STD_LOGIC_VECTOR ( 23 downto 0 );
    s00_axis_tuser : in STD_LOGIC_VECTOR ( 0 to 0 );
    s00_axis_tvalid : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of test_bd_pixelReOrder_0_0_rams_sp_rf_rst_22 : entity is "rams_sp_rf_rst";
end test_bd_pixelReOrder_0_0_rams_sp_rf_rst_22;

architecture STRUCTURE of test_bd_pixelReOrder_0_0_rams_sp_rf_rst_22 is
  signal \ram_reg_0_i_1__8_n_0\ : STD_LOGIC;
  signal NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_DBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_SBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_DOADO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 16 );
  signal NLW_ram_reg_0_DOBDO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_ram_reg_0_DOPADOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal NLW_ram_reg_0_DOPBDOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_ram_reg_0_ECCPARITY_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal NLW_ram_reg_0_RDADDRECC_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_ram_reg_1_DOADO_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 6 );
  signal NLW_ram_reg_1_DOBDO_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal NLW_ram_reg_1_DOPADOP_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_ram_reg_1_DOPBDOP_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ : string;
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of ram_reg_0 : label is "p2_d16";
  attribute METHODOLOGY_DRC_VIOS : string;
  attribute METHODOLOGY_DRC_VIOS of ram_reg_0 : label is "{SYNTH-6 {cell *THIS*}}";
  attribute RTL_RAM_BITS : integer;
  attribute RTL_RAM_BITS of ram_reg_0 : label is 46080;
  attribute RTL_RAM_NAME : string;
  attribute RTL_RAM_NAME of ram_reg_0 : label is "ram";
  attribute bram_addr_begin : integer;
  attribute bram_addr_begin of ram_reg_0 : label is 0;
  attribute bram_addr_end : integer;
  attribute bram_addr_end of ram_reg_0 : label is 2047;
  attribute bram_slice_begin : integer;
  attribute bram_slice_begin of ram_reg_0 : label is 0;
  attribute bram_slice_end : integer;
  attribute bram_slice_end of ram_reg_0 : label is 17;
  attribute ram_addr_begin : integer;
  attribute ram_addr_begin of ram_reg_0 : label is 0;
  attribute ram_addr_end : integer;
  attribute ram_addr_end of ram_reg_0 : label is 2047;
  attribute ram_offset : integer;
  attribute ram_offset of ram_reg_0 : label is 0;
  attribute ram_slice_begin : integer;
  attribute ram_slice_begin of ram_reg_0 : label is 0;
  attribute ram_slice_end : integer;
  attribute ram_slice_end of ram_reg_0 : label is 17;
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of ram_reg_1 : label is "p0_d6";
  attribute METHODOLOGY_DRC_VIOS of ram_reg_1 : label is "{SYNTH-6 {cell *THIS*}}";
  attribute RTL_RAM_BITS of ram_reg_1 : label is 46080;
  attribute RTL_RAM_NAME of ram_reg_1 : label is "ram";
  attribute bram_addr_begin of ram_reg_1 : label is 0;
  attribute bram_addr_end of ram_reg_1 : label is 2047;
  attribute bram_slice_begin of ram_reg_1 : label is 18;
  attribute bram_slice_end of ram_reg_1 : label is 23;
  attribute ram_addr_begin of ram_reg_1 : label is 0;
  attribute ram_addr_end of ram_reg_1 : label is 2047;
  attribute ram_offset of ram_reg_1 : label is 0;
  attribute ram_slice_begin of ram_reg_1 : label is 18;
  attribute ram_slice_end of ram_reg_1 : label is 23;
begin
ram_reg_0: unisim.vcomponents.RAMB36E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      EN_ECC_READ => false,
      EN_ECC_WRITE => false,
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      RAM_EXTENSION_A => "NONE",
      RAM_EXTENSION_B => "NONE",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "PERFORMANCE",
      READ_WIDTH_A => 18,
      READ_WIDTH_B => 0,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "READ_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 18,
      WRITE_WIDTH_B => 0
    )
        port map (
      ADDRARDADDR(15) => '1',
      ADDRARDADDR(14 downto 4) => addr(10 downto 0),
      ADDRARDADDR(3 downto 0) => B"1111",
      ADDRBWRADDR(15 downto 0) => B"1111111111111111",
      CASCADEINA => '1',
      CASCADEINB => '0',
      CASCADEOUTA => NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED,
      CASCADEOUTB => NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED,
      CLKARDCLK => m00_axis_aclk,
      CLKBWRCLK => '0',
      DBITERR => NLW_ram_reg_0_DBITERR_UNCONNECTED,
      DIADI(31 downto 16) => B"0000000000000000",
      DIADI(15 downto 0) => s00_axis_tdata(15 downto 0),
      DIBDI(31 downto 0) => B"11111111111111111111111111111111",
      DIPADIP(3 downto 2) => B"00",
      DIPADIP(1 downto 0) => s00_axis_tdata(17 downto 16),
      DIPBDIP(3 downto 0) => B"1111",
      DOADO(31 downto 16) => NLW_ram_reg_0_DOADO_UNCONNECTED(31 downto 16),
      DOADO(15 downto 0) => dout0_out(15 downto 0),
      DOBDO(31 downto 0) => NLW_ram_reg_0_DOBDO_UNCONNECTED(31 downto 0),
      DOPADOP(3 downto 2) => NLW_ram_reg_0_DOPADOP_UNCONNECTED(3 downto 2),
      DOPADOP(1 downto 0) => dout0_out(17 downto 16),
      DOPBDOP(3 downto 0) => NLW_ram_reg_0_DOPBDOP_UNCONNECTED(3 downto 0),
      ECCPARITY(7 downto 0) => NLW_ram_reg_0_ECCPARITY_UNCONNECTED(7 downto 0),
      ENARDEN => Q(0),
      ENBWREN => '0',
      INJECTDBITERR => NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED,
      INJECTSBITERR => NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED,
      RDADDRECC(8 downto 0) => NLW_ram_reg_0_RDADDRECC_UNCONNECTED(8 downto 0),
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      SBITERR => NLW_ram_reg_0_SBITERR_UNCONNECTED,
      WEA(3) => \ram_reg_0_i_1__8_n_0\,
      WEA(2) => \ram_reg_0_i_1__8_n_0\,
      WEA(1) => \ram_reg_0_i_1__8_n_0\,
      WEA(0) => \ram_reg_0_i_1__8_n_0\,
      WEBWE(7 downto 0) => B"00000000"
    );
\ram_reg_0_i_1__8\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => Q(0),
      I1 => s00_axis_tuser(0),
      I2 => s00_axis_tvalid,
      O => \ram_reg_0_i_1__8_n_0\
    );
ram_reg_1: unisim.vcomponents.RAMB18E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      INIT_A => X"00000",
      INIT_B => X"00000",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "PERFORMANCE",
      READ_WIDTH_A => 9,
      READ_WIDTH_B => 0,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"00000",
      SRVAL_B => X"00000",
      WRITE_MODE_A => "READ_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 9,
      WRITE_WIDTH_B => 0
    )
        port map (
      ADDRARDADDR(13 downto 3) => addr(10 downto 0),
      ADDRARDADDR(2 downto 0) => B"111",
      ADDRBWRADDR(13 downto 0) => B"11111111111111",
      CLKARDCLK => m00_axis_aclk,
      CLKBWRCLK => '0',
      DIADI(15 downto 6) => B"0000000000",
      DIADI(5 downto 0) => s00_axis_tdata(23 downto 18),
      DIBDI(15 downto 0) => B"1111111111111111",
      DIPADIP(1 downto 0) => B"00",
      DIPBDIP(1 downto 0) => B"11",
      DOADO(15 downto 6) => NLW_ram_reg_1_DOADO_UNCONNECTED(15 downto 6),
      DOADO(5 downto 0) => dout0_out(23 downto 18),
      DOBDO(15 downto 0) => NLW_ram_reg_1_DOBDO_UNCONNECTED(15 downto 0),
      DOPADOP(1 downto 0) => NLW_ram_reg_1_DOPADOP_UNCONNECTED(1 downto 0),
      DOPBDOP(1 downto 0) => NLW_ram_reg_1_DOPBDOP_UNCONNECTED(1 downto 0),
      ENARDEN => Q(0),
      ENBWREN => '0',
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      WEA(1) => \ram_reg_0_i_1__8_n_0\,
      WEA(0) => \ram_reg_0_i_1__8_n_0\,
      WEBWE(3 downto 0) => B"0000"
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity test_bd_pixelReOrder_0_0_rams_sp_rf_rst_23 is
  port (
    dout0_out : out STD_LOGIC_VECTOR ( 23 downto 0 );
    m00_axis_aclk : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 0 to 0 );
    addr : in STD_LOGIC_VECTOR ( 10 downto 0 );
    s00_axis_tdata : in STD_LOGIC_VECTOR ( 23 downto 0 );
    s00_axis_tuser : in STD_LOGIC_VECTOR ( 0 to 0 );
    s00_axis_tvalid : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of test_bd_pixelReOrder_0_0_rams_sp_rf_rst_23 : entity is "rams_sp_rf_rst";
end test_bd_pixelReOrder_0_0_rams_sp_rf_rst_23;

architecture STRUCTURE of test_bd_pixelReOrder_0_0_rams_sp_rf_rst_23 is
  signal \ram_reg_0_i_1__9_n_0\ : STD_LOGIC;
  signal NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_DBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_SBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_DOADO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 16 );
  signal NLW_ram_reg_0_DOBDO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_ram_reg_0_DOPADOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal NLW_ram_reg_0_DOPBDOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_ram_reg_0_ECCPARITY_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal NLW_ram_reg_0_RDADDRECC_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_ram_reg_1_DOADO_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 6 );
  signal NLW_ram_reg_1_DOBDO_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal NLW_ram_reg_1_DOPADOP_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_ram_reg_1_DOPBDOP_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ : string;
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of ram_reg_0 : label is "p2_d16";
  attribute METHODOLOGY_DRC_VIOS : string;
  attribute METHODOLOGY_DRC_VIOS of ram_reg_0 : label is "{SYNTH-6 {cell *THIS*}}";
  attribute RTL_RAM_BITS : integer;
  attribute RTL_RAM_BITS of ram_reg_0 : label is 46080;
  attribute RTL_RAM_NAME : string;
  attribute RTL_RAM_NAME of ram_reg_0 : label is "ram";
  attribute bram_addr_begin : integer;
  attribute bram_addr_begin of ram_reg_0 : label is 0;
  attribute bram_addr_end : integer;
  attribute bram_addr_end of ram_reg_0 : label is 2047;
  attribute bram_slice_begin : integer;
  attribute bram_slice_begin of ram_reg_0 : label is 0;
  attribute bram_slice_end : integer;
  attribute bram_slice_end of ram_reg_0 : label is 17;
  attribute ram_addr_begin : integer;
  attribute ram_addr_begin of ram_reg_0 : label is 0;
  attribute ram_addr_end : integer;
  attribute ram_addr_end of ram_reg_0 : label is 2047;
  attribute ram_offset : integer;
  attribute ram_offset of ram_reg_0 : label is 0;
  attribute ram_slice_begin : integer;
  attribute ram_slice_begin of ram_reg_0 : label is 0;
  attribute ram_slice_end : integer;
  attribute ram_slice_end of ram_reg_0 : label is 17;
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of ram_reg_1 : label is "p0_d6";
  attribute METHODOLOGY_DRC_VIOS of ram_reg_1 : label is "{SYNTH-6 {cell *THIS*}}";
  attribute RTL_RAM_BITS of ram_reg_1 : label is 46080;
  attribute RTL_RAM_NAME of ram_reg_1 : label is "ram";
  attribute bram_addr_begin of ram_reg_1 : label is 0;
  attribute bram_addr_end of ram_reg_1 : label is 2047;
  attribute bram_slice_begin of ram_reg_1 : label is 18;
  attribute bram_slice_end of ram_reg_1 : label is 23;
  attribute ram_addr_begin of ram_reg_1 : label is 0;
  attribute ram_addr_end of ram_reg_1 : label is 2047;
  attribute ram_offset of ram_reg_1 : label is 0;
  attribute ram_slice_begin of ram_reg_1 : label is 18;
  attribute ram_slice_end of ram_reg_1 : label is 23;
begin
ram_reg_0: unisim.vcomponents.RAMB36E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      EN_ECC_READ => false,
      EN_ECC_WRITE => false,
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      RAM_EXTENSION_A => "NONE",
      RAM_EXTENSION_B => "NONE",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "PERFORMANCE",
      READ_WIDTH_A => 18,
      READ_WIDTH_B => 0,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "READ_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 18,
      WRITE_WIDTH_B => 0
    )
        port map (
      ADDRARDADDR(15) => '1',
      ADDRARDADDR(14 downto 4) => addr(10 downto 0),
      ADDRARDADDR(3 downto 0) => B"1111",
      ADDRBWRADDR(15 downto 0) => B"1111111111111111",
      CASCADEINA => '1',
      CASCADEINB => '0',
      CASCADEOUTA => NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED,
      CASCADEOUTB => NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED,
      CLKARDCLK => m00_axis_aclk,
      CLKBWRCLK => '0',
      DBITERR => NLW_ram_reg_0_DBITERR_UNCONNECTED,
      DIADI(31 downto 16) => B"0000000000000000",
      DIADI(15 downto 0) => s00_axis_tdata(15 downto 0),
      DIBDI(31 downto 0) => B"11111111111111111111111111111111",
      DIPADIP(3 downto 2) => B"00",
      DIPADIP(1 downto 0) => s00_axis_tdata(17 downto 16),
      DIPBDIP(3 downto 0) => B"1111",
      DOADO(31 downto 16) => NLW_ram_reg_0_DOADO_UNCONNECTED(31 downto 16),
      DOADO(15 downto 0) => dout0_out(15 downto 0),
      DOBDO(31 downto 0) => NLW_ram_reg_0_DOBDO_UNCONNECTED(31 downto 0),
      DOPADOP(3 downto 2) => NLW_ram_reg_0_DOPADOP_UNCONNECTED(3 downto 2),
      DOPADOP(1 downto 0) => dout0_out(17 downto 16),
      DOPBDOP(3 downto 0) => NLW_ram_reg_0_DOPBDOP_UNCONNECTED(3 downto 0),
      ECCPARITY(7 downto 0) => NLW_ram_reg_0_ECCPARITY_UNCONNECTED(7 downto 0),
      ENARDEN => Q(0),
      ENBWREN => '0',
      INJECTDBITERR => NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED,
      INJECTSBITERR => NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED,
      RDADDRECC(8 downto 0) => NLW_ram_reg_0_RDADDRECC_UNCONNECTED(8 downto 0),
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      SBITERR => NLW_ram_reg_0_SBITERR_UNCONNECTED,
      WEA(3) => \ram_reg_0_i_1__9_n_0\,
      WEA(2) => \ram_reg_0_i_1__9_n_0\,
      WEA(1) => \ram_reg_0_i_1__9_n_0\,
      WEA(0) => \ram_reg_0_i_1__9_n_0\,
      WEBWE(7 downto 0) => B"00000000"
    );
\ram_reg_0_i_1__9\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => Q(0),
      I1 => s00_axis_tuser(0),
      I2 => s00_axis_tvalid,
      O => \ram_reg_0_i_1__9_n_0\
    );
ram_reg_1: unisim.vcomponents.RAMB18E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      INIT_A => X"00000",
      INIT_B => X"00000",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "PERFORMANCE",
      READ_WIDTH_A => 9,
      READ_WIDTH_B => 0,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"00000",
      SRVAL_B => X"00000",
      WRITE_MODE_A => "READ_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 9,
      WRITE_WIDTH_B => 0
    )
        port map (
      ADDRARDADDR(13 downto 3) => addr(10 downto 0),
      ADDRARDADDR(2 downto 0) => B"111",
      ADDRBWRADDR(13 downto 0) => B"11111111111111",
      CLKARDCLK => m00_axis_aclk,
      CLKBWRCLK => '0',
      DIADI(15 downto 6) => B"0000000000",
      DIADI(5 downto 0) => s00_axis_tdata(23 downto 18),
      DIBDI(15 downto 0) => B"1111111111111111",
      DIPADIP(1 downto 0) => B"00",
      DIPBDIP(1 downto 0) => B"11",
      DOADO(15 downto 6) => NLW_ram_reg_1_DOADO_UNCONNECTED(15 downto 6),
      DOADO(5 downto 0) => dout0_out(23 downto 18),
      DOBDO(15 downto 0) => NLW_ram_reg_1_DOBDO_UNCONNECTED(15 downto 0),
      DOPADOP(1 downto 0) => NLW_ram_reg_1_DOPADOP_UNCONNECTED(1 downto 0),
      DOPBDOP(1 downto 0) => NLW_ram_reg_1_DOPBDOP_UNCONNECTED(1 downto 0),
      ENARDEN => Q(0),
      ENBWREN => '0',
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      WEA(1) => \ram_reg_0_i_1__9_n_0\,
      WEA(0) => \ram_reg_0_i_1__9_n_0\,
      WEBWE(3 downto 0) => B"0000"
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity test_bd_pixelReOrder_0_0_rams_sp_rf_rst_24 is
  port (
    dout0_out : out STD_LOGIC_VECTOR ( 23 downto 0 );
    m00_axis_aclk : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 0 to 0 );
    addr : in STD_LOGIC_VECTOR ( 10 downto 0 );
    s00_axis_tdata : in STD_LOGIC_VECTOR ( 23 downto 0 );
    s00_axis_tuser : in STD_LOGIC_VECTOR ( 0 to 0 );
    s00_axis_tvalid : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of test_bd_pixelReOrder_0_0_rams_sp_rf_rst_24 : entity is "rams_sp_rf_rst";
end test_bd_pixelReOrder_0_0_rams_sp_rf_rst_24;

architecture STRUCTURE of test_bd_pixelReOrder_0_0_rams_sp_rf_rst_24 is
  signal \ram_reg_0_i_1__10_n_0\ : STD_LOGIC;
  signal NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_DBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_SBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_DOADO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 16 );
  signal NLW_ram_reg_0_DOBDO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_ram_reg_0_DOPADOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal NLW_ram_reg_0_DOPBDOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_ram_reg_0_ECCPARITY_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal NLW_ram_reg_0_RDADDRECC_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_ram_reg_1_DOADO_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 6 );
  signal NLW_ram_reg_1_DOBDO_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal NLW_ram_reg_1_DOPADOP_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_ram_reg_1_DOPBDOP_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ : string;
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of ram_reg_0 : label is "p2_d16";
  attribute METHODOLOGY_DRC_VIOS : string;
  attribute METHODOLOGY_DRC_VIOS of ram_reg_0 : label is "{SYNTH-6 {cell *THIS*}}";
  attribute RTL_RAM_BITS : integer;
  attribute RTL_RAM_BITS of ram_reg_0 : label is 46080;
  attribute RTL_RAM_NAME : string;
  attribute RTL_RAM_NAME of ram_reg_0 : label is "ram";
  attribute bram_addr_begin : integer;
  attribute bram_addr_begin of ram_reg_0 : label is 0;
  attribute bram_addr_end : integer;
  attribute bram_addr_end of ram_reg_0 : label is 2047;
  attribute bram_slice_begin : integer;
  attribute bram_slice_begin of ram_reg_0 : label is 0;
  attribute bram_slice_end : integer;
  attribute bram_slice_end of ram_reg_0 : label is 17;
  attribute ram_addr_begin : integer;
  attribute ram_addr_begin of ram_reg_0 : label is 0;
  attribute ram_addr_end : integer;
  attribute ram_addr_end of ram_reg_0 : label is 2047;
  attribute ram_offset : integer;
  attribute ram_offset of ram_reg_0 : label is 0;
  attribute ram_slice_begin : integer;
  attribute ram_slice_begin of ram_reg_0 : label is 0;
  attribute ram_slice_end : integer;
  attribute ram_slice_end of ram_reg_0 : label is 17;
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of ram_reg_1 : label is "p0_d6";
  attribute METHODOLOGY_DRC_VIOS of ram_reg_1 : label is "{SYNTH-6 {cell *THIS*}}";
  attribute RTL_RAM_BITS of ram_reg_1 : label is 46080;
  attribute RTL_RAM_NAME of ram_reg_1 : label is "ram";
  attribute bram_addr_begin of ram_reg_1 : label is 0;
  attribute bram_addr_end of ram_reg_1 : label is 2047;
  attribute bram_slice_begin of ram_reg_1 : label is 18;
  attribute bram_slice_end of ram_reg_1 : label is 23;
  attribute ram_addr_begin of ram_reg_1 : label is 0;
  attribute ram_addr_end of ram_reg_1 : label is 2047;
  attribute ram_offset of ram_reg_1 : label is 0;
  attribute ram_slice_begin of ram_reg_1 : label is 18;
  attribute ram_slice_end of ram_reg_1 : label is 23;
begin
ram_reg_0: unisim.vcomponents.RAMB36E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      EN_ECC_READ => false,
      EN_ECC_WRITE => false,
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      RAM_EXTENSION_A => "NONE",
      RAM_EXTENSION_B => "NONE",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "PERFORMANCE",
      READ_WIDTH_A => 18,
      READ_WIDTH_B => 0,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "READ_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 18,
      WRITE_WIDTH_B => 0
    )
        port map (
      ADDRARDADDR(15) => '1',
      ADDRARDADDR(14 downto 4) => addr(10 downto 0),
      ADDRARDADDR(3 downto 0) => B"1111",
      ADDRBWRADDR(15 downto 0) => B"1111111111111111",
      CASCADEINA => '1',
      CASCADEINB => '0',
      CASCADEOUTA => NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED,
      CASCADEOUTB => NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED,
      CLKARDCLK => m00_axis_aclk,
      CLKBWRCLK => '0',
      DBITERR => NLW_ram_reg_0_DBITERR_UNCONNECTED,
      DIADI(31 downto 16) => B"0000000000000000",
      DIADI(15 downto 0) => s00_axis_tdata(15 downto 0),
      DIBDI(31 downto 0) => B"11111111111111111111111111111111",
      DIPADIP(3 downto 2) => B"00",
      DIPADIP(1 downto 0) => s00_axis_tdata(17 downto 16),
      DIPBDIP(3 downto 0) => B"1111",
      DOADO(31 downto 16) => NLW_ram_reg_0_DOADO_UNCONNECTED(31 downto 16),
      DOADO(15 downto 0) => dout0_out(15 downto 0),
      DOBDO(31 downto 0) => NLW_ram_reg_0_DOBDO_UNCONNECTED(31 downto 0),
      DOPADOP(3 downto 2) => NLW_ram_reg_0_DOPADOP_UNCONNECTED(3 downto 2),
      DOPADOP(1 downto 0) => dout0_out(17 downto 16),
      DOPBDOP(3 downto 0) => NLW_ram_reg_0_DOPBDOP_UNCONNECTED(3 downto 0),
      ECCPARITY(7 downto 0) => NLW_ram_reg_0_ECCPARITY_UNCONNECTED(7 downto 0),
      ENARDEN => Q(0),
      ENBWREN => '0',
      INJECTDBITERR => NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED,
      INJECTSBITERR => NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED,
      RDADDRECC(8 downto 0) => NLW_ram_reg_0_RDADDRECC_UNCONNECTED(8 downto 0),
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      SBITERR => NLW_ram_reg_0_SBITERR_UNCONNECTED,
      WEA(3) => \ram_reg_0_i_1__10_n_0\,
      WEA(2) => \ram_reg_0_i_1__10_n_0\,
      WEA(1) => \ram_reg_0_i_1__10_n_0\,
      WEA(0) => \ram_reg_0_i_1__10_n_0\,
      WEBWE(7 downto 0) => B"00000000"
    );
\ram_reg_0_i_1__10\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => Q(0),
      I1 => s00_axis_tuser(0),
      I2 => s00_axis_tvalid,
      O => \ram_reg_0_i_1__10_n_0\
    );
ram_reg_1: unisim.vcomponents.RAMB18E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      INIT_A => X"00000",
      INIT_B => X"00000",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "PERFORMANCE",
      READ_WIDTH_A => 9,
      READ_WIDTH_B => 0,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"00000",
      SRVAL_B => X"00000",
      WRITE_MODE_A => "READ_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 9,
      WRITE_WIDTH_B => 0
    )
        port map (
      ADDRARDADDR(13 downto 3) => addr(10 downto 0),
      ADDRARDADDR(2 downto 0) => B"111",
      ADDRBWRADDR(13 downto 0) => B"11111111111111",
      CLKARDCLK => m00_axis_aclk,
      CLKBWRCLK => '0',
      DIADI(15 downto 6) => B"0000000000",
      DIADI(5 downto 0) => s00_axis_tdata(23 downto 18),
      DIBDI(15 downto 0) => B"1111111111111111",
      DIPADIP(1 downto 0) => B"00",
      DIPBDIP(1 downto 0) => B"11",
      DOADO(15 downto 6) => NLW_ram_reg_1_DOADO_UNCONNECTED(15 downto 6),
      DOADO(5 downto 0) => dout0_out(23 downto 18),
      DOBDO(15 downto 0) => NLW_ram_reg_1_DOBDO_UNCONNECTED(15 downto 0),
      DOPADOP(1 downto 0) => NLW_ram_reg_1_DOPADOP_UNCONNECTED(1 downto 0),
      DOPBDOP(1 downto 0) => NLW_ram_reg_1_DOPBDOP_UNCONNECTED(1 downto 0),
      ENARDEN => Q(0),
      ENBWREN => '0',
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      WEA(1) => \ram_reg_0_i_1__10_n_0\,
      WEA(0) => \ram_reg_0_i_1__10_n_0\,
      WEBWE(3 downto 0) => B"0000"
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity test_bd_pixelReOrder_0_0_rams_sp_rf_rst_25 is
  port (
    ram_reg_0_0 : out STD_LOGIC;
    ram_reg_0_1 : out STD_LOGIC;
    ram_reg_0_2 : out STD_LOGIC;
    ram_reg_0_3 : out STD_LOGIC;
    ram_reg_0_4 : out STD_LOGIC;
    ram_reg_0_5 : out STD_LOGIC;
    ram_reg_0_6 : out STD_LOGIC;
    ram_reg_0_7 : out STD_LOGIC;
    ram_reg_0_8 : out STD_LOGIC;
    ram_reg_0_9 : out STD_LOGIC;
    ram_reg_0_10 : out STD_LOGIC;
    ram_reg_0_11 : out STD_LOGIC;
    ram_reg_0_12 : out STD_LOGIC;
    ram_reg_0_13 : out STD_LOGIC;
    ram_reg_0_14 : out STD_LOGIC;
    ram_reg_0_15 : out STD_LOGIC;
    ram_reg_0_16 : out STD_LOGIC;
    ram_reg_0_17 : out STD_LOGIC;
    ram_reg_1_0 : out STD_LOGIC;
    ram_reg_1_1 : out STD_LOGIC;
    ram_reg_1_2 : out STD_LOGIC;
    ram_reg_1_3 : out STD_LOGIC;
    ram_reg_1_4 : out STD_LOGIC;
    ram_reg_1_5 : out STD_LOGIC;
    dout0_out : in STD_LOGIC_VECTOR ( 23 downto 0 );
    ctrl_mux_out_1 : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \y_reg[23]_i_3\ : in STD_LOGIC_VECTOR ( 23 downto 0 );
    \y_reg[23]_i_3_0\ : in STD_LOGIC_VECTOR ( 23 downto 0 );
    m00_axis_aclk : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 0 to 0 );
    addr : in STD_LOGIC_VECTOR ( 10 downto 0 );
    s00_axis_tdata : in STD_LOGIC_VECTOR ( 23 downto 0 );
    s00_axis_tuser : in STD_LOGIC_VECTOR ( 0 to 0 );
    s00_axis_tvalid : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of test_bd_pixelReOrder_0_0_rams_sp_rf_rst_25 : entity is "rams_sp_rf_rst";
end test_bd_pixelReOrder_0_0_rams_sp_rf_rst_25;

architecture STRUCTURE of test_bd_pixelReOrder_0_0_rams_sp_rf_rst_25 is
  signal \_DOUT_1[7]_14\ : STD_LOGIC_VECTOR ( 23 downto 0 );
  signal \ram_reg_0_i_1__11_n_0\ : STD_LOGIC;
  signal NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_DBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_SBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_DOADO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 16 );
  signal NLW_ram_reg_0_DOBDO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_ram_reg_0_DOPADOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal NLW_ram_reg_0_DOPBDOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_ram_reg_0_ECCPARITY_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal NLW_ram_reg_0_RDADDRECC_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_ram_reg_1_DOADO_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 6 );
  signal NLW_ram_reg_1_DOBDO_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal NLW_ram_reg_1_DOPADOP_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_ram_reg_1_DOPBDOP_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ : string;
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of ram_reg_0 : label is "p2_d16";
  attribute METHODOLOGY_DRC_VIOS : string;
  attribute METHODOLOGY_DRC_VIOS of ram_reg_0 : label is "{SYNTH-6 {cell *THIS*}}";
  attribute RTL_RAM_BITS : integer;
  attribute RTL_RAM_BITS of ram_reg_0 : label is 46080;
  attribute RTL_RAM_NAME : string;
  attribute RTL_RAM_NAME of ram_reg_0 : label is "ram";
  attribute bram_addr_begin : integer;
  attribute bram_addr_begin of ram_reg_0 : label is 0;
  attribute bram_addr_end : integer;
  attribute bram_addr_end of ram_reg_0 : label is 2047;
  attribute bram_slice_begin : integer;
  attribute bram_slice_begin of ram_reg_0 : label is 0;
  attribute bram_slice_end : integer;
  attribute bram_slice_end of ram_reg_0 : label is 17;
  attribute ram_addr_begin : integer;
  attribute ram_addr_begin of ram_reg_0 : label is 0;
  attribute ram_addr_end : integer;
  attribute ram_addr_end of ram_reg_0 : label is 2047;
  attribute ram_offset : integer;
  attribute ram_offset of ram_reg_0 : label is 0;
  attribute ram_slice_begin : integer;
  attribute ram_slice_begin of ram_reg_0 : label is 0;
  attribute ram_slice_end : integer;
  attribute ram_slice_end of ram_reg_0 : label is 17;
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of ram_reg_1 : label is "p0_d6";
  attribute METHODOLOGY_DRC_VIOS of ram_reg_1 : label is "{SYNTH-6 {cell *THIS*}}";
  attribute RTL_RAM_BITS of ram_reg_1 : label is 46080;
  attribute RTL_RAM_NAME of ram_reg_1 : label is "ram";
  attribute bram_addr_begin of ram_reg_1 : label is 0;
  attribute bram_addr_end of ram_reg_1 : label is 2047;
  attribute bram_slice_begin of ram_reg_1 : label is 18;
  attribute bram_slice_end of ram_reg_1 : label is 23;
  attribute ram_addr_begin of ram_reg_1 : label is 0;
  attribute ram_addr_end of ram_reg_1 : label is 2047;
  attribute ram_offset of ram_reg_1 : label is 0;
  attribute ram_slice_begin of ram_reg_1 : label is 18;
  attribute ram_slice_end of ram_reg_1 : label is 23;
begin
ram_reg_0: unisim.vcomponents.RAMB36E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      EN_ECC_READ => false,
      EN_ECC_WRITE => false,
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      RAM_EXTENSION_A => "NONE",
      RAM_EXTENSION_B => "NONE",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "PERFORMANCE",
      READ_WIDTH_A => 18,
      READ_WIDTH_B => 0,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "READ_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 18,
      WRITE_WIDTH_B => 0
    )
        port map (
      ADDRARDADDR(15) => '1',
      ADDRARDADDR(14 downto 4) => addr(10 downto 0),
      ADDRARDADDR(3 downto 0) => B"1111",
      ADDRBWRADDR(15 downto 0) => B"1111111111111111",
      CASCADEINA => '1',
      CASCADEINB => '0',
      CASCADEOUTA => NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED,
      CASCADEOUTB => NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED,
      CLKARDCLK => m00_axis_aclk,
      CLKBWRCLK => '0',
      DBITERR => NLW_ram_reg_0_DBITERR_UNCONNECTED,
      DIADI(31 downto 16) => B"0000000000000000",
      DIADI(15 downto 0) => s00_axis_tdata(15 downto 0),
      DIBDI(31 downto 0) => B"11111111111111111111111111111111",
      DIPADIP(3 downto 2) => B"00",
      DIPADIP(1 downto 0) => s00_axis_tdata(17 downto 16),
      DIPBDIP(3 downto 0) => B"1111",
      DOADO(31 downto 16) => NLW_ram_reg_0_DOADO_UNCONNECTED(31 downto 16),
      DOADO(15 downto 0) => \_DOUT_1[7]_14\(15 downto 0),
      DOBDO(31 downto 0) => NLW_ram_reg_0_DOBDO_UNCONNECTED(31 downto 0),
      DOPADOP(3 downto 2) => NLW_ram_reg_0_DOPADOP_UNCONNECTED(3 downto 2),
      DOPADOP(1 downto 0) => \_DOUT_1[7]_14\(17 downto 16),
      DOPBDOP(3 downto 0) => NLW_ram_reg_0_DOPBDOP_UNCONNECTED(3 downto 0),
      ECCPARITY(7 downto 0) => NLW_ram_reg_0_ECCPARITY_UNCONNECTED(7 downto 0),
      ENARDEN => Q(0),
      ENBWREN => '0',
      INJECTDBITERR => NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED,
      INJECTSBITERR => NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED,
      RDADDRECC(8 downto 0) => NLW_ram_reg_0_RDADDRECC_UNCONNECTED(8 downto 0),
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      SBITERR => NLW_ram_reg_0_SBITERR_UNCONNECTED,
      WEA(3) => \ram_reg_0_i_1__11_n_0\,
      WEA(2) => \ram_reg_0_i_1__11_n_0\,
      WEA(1) => \ram_reg_0_i_1__11_n_0\,
      WEA(0) => \ram_reg_0_i_1__11_n_0\,
      WEBWE(7 downto 0) => B"00000000"
    );
\ram_reg_0_i_1__11\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => Q(0),
      I1 => s00_axis_tuser(0),
      I2 => s00_axis_tvalid,
      O => \ram_reg_0_i_1__11_n_0\
    );
ram_reg_1: unisim.vcomponents.RAMB18E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      INIT_A => X"00000",
      INIT_B => X"00000",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "PERFORMANCE",
      READ_WIDTH_A => 9,
      READ_WIDTH_B => 0,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"00000",
      SRVAL_B => X"00000",
      WRITE_MODE_A => "READ_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 9,
      WRITE_WIDTH_B => 0
    )
        port map (
      ADDRARDADDR(13 downto 3) => addr(10 downto 0),
      ADDRARDADDR(2 downto 0) => B"111",
      ADDRBWRADDR(13 downto 0) => B"11111111111111",
      CLKARDCLK => m00_axis_aclk,
      CLKBWRCLK => '0',
      DIADI(15 downto 6) => B"0000000000",
      DIADI(5 downto 0) => s00_axis_tdata(23 downto 18),
      DIBDI(15 downto 0) => B"1111111111111111",
      DIPADIP(1 downto 0) => B"00",
      DIPBDIP(1 downto 0) => B"11",
      DOADO(15 downto 6) => NLW_ram_reg_1_DOADO_UNCONNECTED(15 downto 6),
      DOADO(5 downto 0) => \_DOUT_1[7]_14\(23 downto 18),
      DOBDO(15 downto 0) => NLW_ram_reg_1_DOBDO_UNCONNECTED(15 downto 0),
      DOPADOP(1 downto 0) => NLW_ram_reg_1_DOPADOP_UNCONNECTED(1 downto 0),
      DOPBDOP(1 downto 0) => NLW_ram_reg_1_DOPBDOP_UNCONNECTED(1 downto 0),
      ENARDEN => Q(0),
      ENBWREN => '0',
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      WEA(1) => \ram_reg_0_i_1__11_n_0\,
      WEA(0) => \ram_reg_0_i_1__11_n_0\,
      WEBWE(3 downto 0) => B"0000"
    );
\y_reg[0]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_1[7]_14\(0),
      I1 => dout0_out(0),
      I2 => ctrl_mux_out_1(1),
      I3 => \y_reg[23]_i_3\(0),
      I4 => ctrl_mux_out_1(0),
      I5 => \y_reg[23]_i_3_0\(0),
      O => ram_reg_0_0
    );
\y_reg[10]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_1[7]_14\(10),
      I1 => dout0_out(10),
      I2 => ctrl_mux_out_1(1),
      I3 => \y_reg[23]_i_3\(10),
      I4 => ctrl_mux_out_1(0),
      I5 => \y_reg[23]_i_3_0\(10),
      O => ram_reg_0_10
    );
\y_reg[11]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_1[7]_14\(11),
      I1 => dout0_out(11),
      I2 => ctrl_mux_out_1(1),
      I3 => \y_reg[23]_i_3\(11),
      I4 => ctrl_mux_out_1(0),
      I5 => \y_reg[23]_i_3_0\(11),
      O => ram_reg_0_11
    );
\y_reg[12]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_1[7]_14\(12),
      I1 => dout0_out(12),
      I2 => ctrl_mux_out_1(1),
      I3 => \y_reg[23]_i_3\(12),
      I4 => ctrl_mux_out_1(0),
      I5 => \y_reg[23]_i_3_0\(12),
      O => ram_reg_0_12
    );
\y_reg[13]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_1[7]_14\(13),
      I1 => dout0_out(13),
      I2 => ctrl_mux_out_1(1),
      I3 => \y_reg[23]_i_3\(13),
      I4 => ctrl_mux_out_1(0),
      I5 => \y_reg[23]_i_3_0\(13),
      O => ram_reg_0_13
    );
\y_reg[14]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_1[7]_14\(14),
      I1 => dout0_out(14),
      I2 => ctrl_mux_out_1(1),
      I3 => \y_reg[23]_i_3\(14),
      I4 => ctrl_mux_out_1(0),
      I5 => \y_reg[23]_i_3_0\(14),
      O => ram_reg_0_14
    );
\y_reg[15]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_1[7]_14\(15),
      I1 => dout0_out(15),
      I2 => ctrl_mux_out_1(1),
      I3 => \y_reg[23]_i_3\(15),
      I4 => ctrl_mux_out_1(0),
      I5 => \y_reg[23]_i_3_0\(15),
      O => ram_reg_0_15
    );
\y_reg[16]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_1[7]_14\(16),
      I1 => dout0_out(16),
      I2 => ctrl_mux_out_1(1),
      I3 => \y_reg[23]_i_3\(16),
      I4 => ctrl_mux_out_1(0),
      I5 => \y_reg[23]_i_3_0\(16),
      O => ram_reg_0_16
    );
\y_reg[17]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_1[7]_14\(17),
      I1 => dout0_out(17),
      I2 => ctrl_mux_out_1(1),
      I3 => \y_reg[23]_i_3\(17),
      I4 => ctrl_mux_out_1(0),
      I5 => \y_reg[23]_i_3_0\(17),
      O => ram_reg_0_17
    );
\y_reg[18]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_1[7]_14\(18),
      I1 => dout0_out(18),
      I2 => ctrl_mux_out_1(1),
      I3 => \y_reg[23]_i_3\(18),
      I4 => ctrl_mux_out_1(0),
      I5 => \y_reg[23]_i_3_0\(18),
      O => ram_reg_1_0
    );
\y_reg[19]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_1[7]_14\(19),
      I1 => dout0_out(19),
      I2 => ctrl_mux_out_1(1),
      I3 => \y_reg[23]_i_3\(19),
      I4 => ctrl_mux_out_1(0),
      I5 => \y_reg[23]_i_3_0\(19),
      O => ram_reg_1_1
    );
\y_reg[1]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_1[7]_14\(1),
      I1 => dout0_out(1),
      I2 => ctrl_mux_out_1(1),
      I3 => \y_reg[23]_i_3\(1),
      I4 => ctrl_mux_out_1(0),
      I5 => \y_reg[23]_i_3_0\(1),
      O => ram_reg_0_1
    );
\y_reg[20]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_1[7]_14\(20),
      I1 => dout0_out(20),
      I2 => ctrl_mux_out_1(1),
      I3 => \y_reg[23]_i_3\(20),
      I4 => ctrl_mux_out_1(0),
      I5 => \y_reg[23]_i_3_0\(20),
      O => ram_reg_1_2
    );
\y_reg[21]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_1[7]_14\(21),
      I1 => dout0_out(21),
      I2 => ctrl_mux_out_1(1),
      I3 => \y_reg[23]_i_3\(21),
      I4 => ctrl_mux_out_1(0),
      I5 => \y_reg[23]_i_3_0\(21),
      O => ram_reg_1_3
    );
\y_reg[22]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_1[7]_14\(22),
      I1 => dout0_out(22),
      I2 => ctrl_mux_out_1(1),
      I3 => \y_reg[23]_i_3\(22),
      I4 => ctrl_mux_out_1(0),
      I5 => \y_reg[23]_i_3_0\(22),
      O => ram_reg_1_4
    );
\y_reg[23]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_1[7]_14\(23),
      I1 => dout0_out(23),
      I2 => ctrl_mux_out_1(1),
      I3 => \y_reg[23]_i_3\(23),
      I4 => ctrl_mux_out_1(0),
      I5 => \y_reg[23]_i_3_0\(23),
      O => ram_reg_1_5
    );
\y_reg[2]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_1[7]_14\(2),
      I1 => dout0_out(2),
      I2 => ctrl_mux_out_1(1),
      I3 => \y_reg[23]_i_3\(2),
      I4 => ctrl_mux_out_1(0),
      I5 => \y_reg[23]_i_3_0\(2),
      O => ram_reg_0_2
    );
\y_reg[3]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_1[7]_14\(3),
      I1 => dout0_out(3),
      I2 => ctrl_mux_out_1(1),
      I3 => \y_reg[23]_i_3\(3),
      I4 => ctrl_mux_out_1(0),
      I5 => \y_reg[23]_i_3_0\(3),
      O => ram_reg_0_3
    );
\y_reg[4]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_1[7]_14\(4),
      I1 => dout0_out(4),
      I2 => ctrl_mux_out_1(1),
      I3 => \y_reg[23]_i_3\(4),
      I4 => ctrl_mux_out_1(0),
      I5 => \y_reg[23]_i_3_0\(4),
      O => ram_reg_0_4
    );
\y_reg[5]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_1[7]_14\(5),
      I1 => dout0_out(5),
      I2 => ctrl_mux_out_1(1),
      I3 => \y_reg[23]_i_3\(5),
      I4 => ctrl_mux_out_1(0),
      I5 => \y_reg[23]_i_3_0\(5),
      O => ram_reg_0_5
    );
\y_reg[6]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_1[7]_14\(6),
      I1 => dout0_out(6),
      I2 => ctrl_mux_out_1(1),
      I3 => \y_reg[23]_i_3\(6),
      I4 => ctrl_mux_out_1(0),
      I5 => \y_reg[23]_i_3_0\(6),
      O => ram_reg_0_6
    );
\y_reg[7]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_1[7]_14\(7),
      I1 => dout0_out(7),
      I2 => ctrl_mux_out_1(1),
      I3 => \y_reg[23]_i_3\(7),
      I4 => ctrl_mux_out_1(0),
      I5 => \y_reg[23]_i_3_0\(7),
      O => ram_reg_0_7
    );
\y_reg[8]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_1[7]_14\(8),
      I1 => dout0_out(8),
      I2 => ctrl_mux_out_1(1),
      I3 => \y_reg[23]_i_3\(8),
      I4 => ctrl_mux_out_1(0),
      I5 => \y_reg[23]_i_3_0\(8),
      O => ram_reg_0_8
    );
\y_reg[9]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_1[7]_14\(9),
      I1 => dout0_out(9),
      I2 => ctrl_mux_out_1(1),
      I3 => \y_reg[23]_i_3\(9),
      I4 => ctrl_mux_out_1(0),
      I5 => \y_reg[23]_i_3_0\(9),
      O => ram_reg_0_9
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity test_bd_pixelReOrder_0_0_rams_sp_rf_rst_26 is
  port (
    ram_reg_0_0 : out STD_LOGIC;
    ram_reg_0_1 : out STD_LOGIC;
    ram_reg_0_2 : out STD_LOGIC;
    ram_reg_0_3 : out STD_LOGIC;
    ram_reg_0_4 : out STD_LOGIC;
    ram_reg_0_5 : out STD_LOGIC;
    ram_reg_0_6 : out STD_LOGIC;
    ram_reg_0_7 : out STD_LOGIC;
    ram_reg_0_8 : out STD_LOGIC;
    ram_reg_0_9 : out STD_LOGIC;
    ram_reg_0_10 : out STD_LOGIC;
    ram_reg_0_11 : out STD_LOGIC;
    ram_reg_0_12 : out STD_LOGIC;
    ram_reg_0_13 : out STD_LOGIC;
    ram_reg_0_14 : out STD_LOGIC;
    ram_reg_0_15 : out STD_LOGIC;
    ram_reg_0_16 : out STD_LOGIC;
    ram_reg_0_17 : out STD_LOGIC;
    ram_reg_1_0 : out STD_LOGIC;
    ram_reg_1_1 : out STD_LOGIC;
    ram_reg_1_2 : out STD_LOGIC;
    ram_reg_1_3 : out STD_LOGIC;
    ram_reg_1_4 : out STD_LOGIC;
    ram_reg_1_5 : out STD_LOGIC;
    dout0_out : in STD_LOGIC_VECTOR ( 23 downto 0 );
    ctrl_mux_out_2 : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \y_reg[23]_i_3__0\ : in STD_LOGIC_VECTOR ( 23 downto 0 );
    \y_reg[23]_i_3__0_0\ : in STD_LOGIC_VECTOR ( 23 downto 0 );
    m00_axis_aclk : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 0 to 0 );
    addr : in STD_LOGIC_VECTOR ( 10 downto 0 );
    s00_axis_tdata : in STD_LOGIC_VECTOR ( 23 downto 0 );
    s00_axis_tuser : in STD_LOGIC_VECTOR ( 0 to 0 );
    s00_axis_tvalid : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of test_bd_pixelReOrder_0_0_rams_sp_rf_rst_26 : entity is "rams_sp_rf_rst";
end test_bd_pixelReOrder_0_0_rams_sp_rf_rst_26;

architecture STRUCTURE of test_bd_pixelReOrder_0_0_rams_sp_rf_rst_26 is
  signal \_DOUT_2[7]_15\ : STD_LOGIC_VECTOR ( 23 downto 0 );
  signal \ram_reg_0_i_1__12_n_0\ : STD_LOGIC;
  signal NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_DBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_SBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_DOADO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 16 );
  signal NLW_ram_reg_0_DOBDO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_ram_reg_0_DOPADOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal NLW_ram_reg_0_DOPBDOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_ram_reg_0_ECCPARITY_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal NLW_ram_reg_0_RDADDRECC_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_ram_reg_1_DOADO_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 6 );
  signal NLW_ram_reg_1_DOBDO_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal NLW_ram_reg_1_DOPADOP_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_ram_reg_1_DOPBDOP_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ : string;
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of ram_reg_0 : label is "p2_d16";
  attribute METHODOLOGY_DRC_VIOS : string;
  attribute METHODOLOGY_DRC_VIOS of ram_reg_0 : label is "{SYNTH-6 {cell *THIS*}}";
  attribute RTL_RAM_BITS : integer;
  attribute RTL_RAM_BITS of ram_reg_0 : label is 46080;
  attribute RTL_RAM_NAME : string;
  attribute RTL_RAM_NAME of ram_reg_0 : label is "ram";
  attribute bram_addr_begin : integer;
  attribute bram_addr_begin of ram_reg_0 : label is 0;
  attribute bram_addr_end : integer;
  attribute bram_addr_end of ram_reg_0 : label is 2047;
  attribute bram_slice_begin : integer;
  attribute bram_slice_begin of ram_reg_0 : label is 0;
  attribute bram_slice_end : integer;
  attribute bram_slice_end of ram_reg_0 : label is 17;
  attribute ram_addr_begin : integer;
  attribute ram_addr_begin of ram_reg_0 : label is 0;
  attribute ram_addr_end : integer;
  attribute ram_addr_end of ram_reg_0 : label is 2047;
  attribute ram_offset : integer;
  attribute ram_offset of ram_reg_0 : label is 0;
  attribute ram_slice_begin : integer;
  attribute ram_slice_begin of ram_reg_0 : label is 0;
  attribute ram_slice_end : integer;
  attribute ram_slice_end of ram_reg_0 : label is 17;
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of ram_reg_1 : label is "p0_d6";
  attribute METHODOLOGY_DRC_VIOS of ram_reg_1 : label is "{SYNTH-6 {cell *THIS*}}";
  attribute RTL_RAM_BITS of ram_reg_1 : label is 46080;
  attribute RTL_RAM_NAME of ram_reg_1 : label is "ram";
  attribute bram_addr_begin of ram_reg_1 : label is 0;
  attribute bram_addr_end of ram_reg_1 : label is 2047;
  attribute bram_slice_begin of ram_reg_1 : label is 18;
  attribute bram_slice_end of ram_reg_1 : label is 23;
  attribute ram_addr_begin of ram_reg_1 : label is 0;
  attribute ram_addr_end of ram_reg_1 : label is 2047;
  attribute ram_offset of ram_reg_1 : label is 0;
  attribute ram_slice_begin of ram_reg_1 : label is 18;
  attribute ram_slice_end of ram_reg_1 : label is 23;
begin
ram_reg_0: unisim.vcomponents.RAMB36E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      EN_ECC_READ => false,
      EN_ECC_WRITE => false,
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      RAM_EXTENSION_A => "NONE",
      RAM_EXTENSION_B => "NONE",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "PERFORMANCE",
      READ_WIDTH_A => 18,
      READ_WIDTH_B => 0,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "READ_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 18,
      WRITE_WIDTH_B => 0
    )
        port map (
      ADDRARDADDR(15) => '1',
      ADDRARDADDR(14 downto 4) => addr(10 downto 0),
      ADDRARDADDR(3 downto 0) => B"1111",
      ADDRBWRADDR(15 downto 0) => B"1111111111111111",
      CASCADEINA => '1',
      CASCADEINB => '0',
      CASCADEOUTA => NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED,
      CASCADEOUTB => NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED,
      CLKARDCLK => m00_axis_aclk,
      CLKBWRCLK => '0',
      DBITERR => NLW_ram_reg_0_DBITERR_UNCONNECTED,
      DIADI(31 downto 16) => B"0000000000000000",
      DIADI(15 downto 0) => s00_axis_tdata(15 downto 0),
      DIBDI(31 downto 0) => B"11111111111111111111111111111111",
      DIPADIP(3 downto 2) => B"00",
      DIPADIP(1 downto 0) => s00_axis_tdata(17 downto 16),
      DIPBDIP(3 downto 0) => B"1111",
      DOADO(31 downto 16) => NLW_ram_reg_0_DOADO_UNCONNECTED(31 downto 16),
      DOADO(15 downto 0) => \_DOUT_2[7]_15\(15 downto 0),
      DOBDO(31 downto 0) => NLW_ram_reg_0_DOBDO_UNCONNECTED(31 downto 0),
      DOPADOP(3 downto 2) => NLW_ram_reg_0_DOPADOP_UNCONNECTED(3 downto 2),
      DOPADOP(1 downto 0) => \_DOUT_2[7]_15\(17 downto 16),
      DOPBDOP(3 downto 0) => NLW_ram_reg_0_DOPBDOP_UNCONNECTED(3 downto 0),
      ECCPARITY(7 downto 0) => NLW_ram_reg_0_ECCPARITY_UNCONNECTED(7 downto 0),
      ENARDEN => Q(0),
      ENBWREN => '0',
      INJECTDBITERR => NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED,
      INJECTSBITERR => NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED,
      RDADDRECC(8 downto 0) => NLW_ram_reg_0_RDADDRECC_UNCONNECTED(8 downto 0),
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      SBITERR => NLW_ram_reg_0_SBITERR_UNCONNECTED,
      WEA(3) => \ram_reg_0_i_1__12_n_0\,
      WEA(2) => \ram_reg_0_i_1__12_n_0\,
      WEA(1) => \ram_reg_0_i_1__12_n_0\,
      WEA(0) => \ram_reg_0_i_1__12_n_0\,
      WEBWE(7 downto 0) => B"00000000"
    );
\ram_reg_0_i_1__12\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => Q(0),
      I1 => s00_axis_tuser(0),
      I2 => s00_axis_tvalid,
      O => \ram_reg_0_i_1__12_n_0\
    );
ram_reg_1: unisim.vcomponents.RAMB18E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      INIT_A => X"00000",
      INIT_B => X"00000",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "PERFORMANCE",
      READ_WIDTH_A => 9,
      READ_WIDTH_B => 0,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"00000",
      SRVAL_B => X"00000",
      WRITE_MODE_A => "READ_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 9,
      WRITE_WIDTH_B => 0
    )
        port map (
      ADDRARDADDR(13 downto 3) => addr(10 downto 0),
      ADDRARDADDR(2 downto 0) => B"111",
      ADDRBWRADDR(13 downto 0) => B"11111111111111",
      CLKARDCLK => m00_axis_aclk,
      CLKBWRCLK => '0',
      DIADI(15 downto 6) => B"0000000000",
      DIADI(5 downto 0) => s00_axis_tdata(23 downto 18),
      DIBDI(15 downto 0) => B"1111111111111111",
      DIPADIP(1 downto 0) => B"00",
      DIPBDIP(1 downto 0) => B"11",
      DOADO(15 downto 6) => NLW_ram_reg_1_DOADO_UNCONNECTED(15 downto 6),
      DOADO(5 downto 0) => \_DOUT_2[7]_15\(23 downto 18),
      DOBDO(15 downto 0) => NLW_ram_reg_1_DOBDO_UNCONNECTED(15 downto 0),
      DOPADOP(1 downto 0) => NLW_ram_reg_1_DOPADOP_UNCONNECTED(1 downto 0),
      DOPBDOP(1 downto 0) => NLW_ram_reg_1_DOPBDOP_UNCONNECTED(1 downto 0),
      ENARDEN => Q(0),
      ENBWREN => '0',
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      WEA(1) => \ram_reg_0_i_1__12_n_0\,
      WEA(0) => \ram_reg_0_i_1__12_n_0\,
      WEBWE(3 downto 0) => B"0000"
    );
\y_reg[0]_i_6__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_2[7]_15\(0),
      I1 => dout0_out(0),
      I2 => ctrl_mux_out_2(1),
      I3 => \y_reg[23]_i_3__0\(0),
      I4 => ctrl_mux_out_2(0),
      I5 => \y_reg[23]_i_3__0_0\(0),
      O => ram_reg_0_0
    );
\y_reg[10]_i_6__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_2[7]_15\(10),
      I1 => dout0_out(10),
      I2 => ctrl_mux_out_2(1),
      I3 => \y_reg[23]_i_3__0\(10),
      I4 => ctrl_mux_out_2(0),
      I5 => \y_reg[23]_i_3__0_0\(10),
      O => ram_reg_0_10
    );
\y_reg[11]_i_6__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_2[7]_15\(11),
      I1 => dout0_out(11),
      I2 => ctrl_mux_out_2(1),
      I3 => \y_reg[23]_i_3__0\(11),
      I4 => ctrl_mux_out_2(0),
      I5 => \y_reg[23]_i_3__0_0\(11),
      O => ram_reg_0_11
    );
\y_reg[12]_i_6__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_2[7]_15\(12),
      I1 => dout0_out(12),
      I2 => ctrl_mux_out_2(1),
      I3 => \y_reg[23]_i_3__0\(12),
      I4 => ctrl_mux_out_2(0),
      I5 => \y_reg[23]_i_3__0_0\(12),
      O => ram_reg_0_12
    );
\y_reg[13]_i_6__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_2[7]_15\(13),
      I1 => dout0_out(13),
      I2 => ctrl_mux_out_2(1),
      I3 => \y_reg[23]_i_3__0\(13),
      I4 => ctrl_mux_out_2(0),
      I5 => \y_reg[23]_i_3__0_0\(13),
      O => ram_reg_0_13
    );
\y_reg[14]_i_6__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_2[7]_15\(14),
      I1 => dout0_out(14),
      I2 => ctrl_mux_out_2(1),
      I3 => \y_reg[23]_i_3__0\(14),
      I4 => ctrl_mux_out_2(0),
      I5 => \y_reg[23]_i_3__0_0\(14),
      O => ram_reg_0_14
    );
\y_reg[15]_i_6__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_2[7]_15\(15),
      I1 => dout0_out(15),
      I2 => ctrl_mux_out_2(1),
      I3 => \y_reg[23]_i_3__0\(15),
      I4 => ctrl_mux_out_2(0),
      I5 => \y_reg[23]_i_3__0_0\(15),
      O => ram_reg_0_15
    );
\y_reg[16]_i_6__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_2[7]_15\(16),
      I1 => dout0_out(16),
      I2 => ctrl_mux_out_2(1),
      I3 => \y_reg[23]_i_3__0\(16),
      I4 => ctrl_mux_out_2(0),
      I5 => \y_reg[23]_i_3__0_0\(16),
      O => ram_reg_0_16
    );
\y_reg[17]_i_6__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_2[7]_15\(17),
      I1 => dout0_out(17),
      I2 => ctrl_mux_out_2(1),
      I3 => \y_reg[23]_i_3__0\(17),
      I4 => ctrl_mux_out_2(0),
      I5 => \y_reg[23]_i_3__0_0\(17),
      O => ram_reg_0_17
    );
\y_reg[18]_i_6__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_2[7]_15\(18),
      I1 => dout0_out(18),
      I2 => ctrl_mux_out_2(1),
      I3 => \y_reg[23]_i_3__0\(18),
      I4 => ctrl_mux_out_2(0),
      I5 => \y_reg[23]_i_3__0_0\(18),
      O => ram_reg_1_0
    );
\y_reg[19]_i_6__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_2[7]_15\(19),
      I1 => dout0_out(19),
      I2 => ctrl_mux_out_2(1),
      I3 => \y_reg[23]_i_3__0\(19),
      I4 => ctrl_mux_out_2(0),
      I5 => \y_reg[23]_i_3__0_0\(19),
      O => ram_reg_1_1
    );
\y_reg[1]_i_6__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_2[7]_15\(1),
      I1 => dout0_out(1),
      I2 => ctrl_mux_out_2(1),
      I3 => \y_reg[23]_i_3__0\(1),
      I4 => ctrl_mux_out_2(0),
      I5 => \y_reg[23]_i_3__0_0\(1),
      O => ram_reg_0_1
    );
\y_reg[20]_i_6__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_2[7]_15\(20),
      I1 => dout0_out(20),
      I2 => ctrl_mux_out_2(1),
      I3 => \y_reg[23]_i_3__0\(20),
      I4 => ctrl_mux_out_2(0),
      I5 => \y_reg[23]_i_3__0_0\(20),
      O => ram_reg_1_2
    );
\y_reg[21]_i_6__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_2[7]_15\(21),
      I1 => dout0_out(21),
      I2 => ctrl_mux_out_2(1),
      I3 => \y_reg[23]_i_3__0\(21),
      I4 => ctrl_mux_out_2(0),
      I5 => \y_reg[23]_i_3__0_0\(21),
      O => ram_reg_1_3
    );
\y_reg[22]_i_6__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_2[7]_15\(22),
      I1 => dout0_out(22),
      I2 => ctrl_mux_out_2(1),
      I3 => \y_reg[23]_i_3__0\(22),
      I4 => ctrl_mux_out_2(0),
      I5 => \y_reg[23]_i_3__0_0\(22),
      O => ram_reg_1_4
    );
\y_reg[23]_i_7__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_2[7]_15\(23),
      I1 => dout0_out(23),
      I2 => ctrl_mux_out_2(1),
      I3 => \y_reg[23]_i_3__0\(23),
      I4 => ctrl_mux_out_2(0),
      I5 => \y_reg[23]_i_3__0_0\(23),
      O => ram_reg_1_5
    );
\y_reg[2]_i_6__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_2[7]_15\(2),
      I1 => dout0_out(2),
      I2 => ctrl_mux_out_2(1),
      I3 => \y_reg[23]_i_3__0\(2),
      I4 => ctrl_mux_out_2(0),
      I5 => \y_reg[23]_i_3__0_0\(2),
      O => ram_reg_0_2
    );
\y_reg[3]_i_6__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_2[7]_15\(3),
      I1 => dout0_out(3),
      I2 => ctrl_mux_out_2(1),
      I3 => \y_reg[23]_i_3__0\(3),
      I4 => ctrl_mux_out_2(0),
      I5 => \y_reg[23]_i_3__0_0\(3),
      O => ram_reg_0_3
    );
\y_reg[4]_i_6__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_2[7]_15\(4),
      I1 => dout0_out(4),
      I2 => ctrl_mux_out_2(1),
      I3 => \y_reg[23]_i_3__0\(4),
      I4 => ctrl_mux_out_2(0),
      I5 => \y_reg[23]_i_3__0_0\(4),
      O => ram_reg_0_4
    );
\y_reg[5]_i_6__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_2[7]_15\(5),
      I1 => dout0_out(5),
      I2 => ctrl_mux_out_2(1),
      I3 => \y_reg[23]_i_3__0\(5),
      I4 => ctrl_mux_out_2(0),
      I5 => \y_reg[23]_i_3__0_0\(5),
      O => ram_reg_0_5
    );
\y_reg[6]_i_6__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_2[7]_15\(6),
      I1 => dout0_out(6),
      I2 => ctrl_mux_out_2(1),
      I3 => \y_reg[23]_i_3__0\(6),
      I4 => ctrl_mux_out_2(0),
      I5 => \y_reg[23]_i_3__0_0\(6),
      O => ram_reg_0_6
    );
\y_reg[7]_i_6__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_2[7]_15\(7),
      I1 => dout0_out(7),
      I2 => ctrl_mux_out_2(1),
      I3 => \y_reg[23]_i_3__0\(7),
      I4 => ctrl_mux_out_2(0),
      I5 => \y_reg[23]_i_3__0_0\(7),
      O => ram_reg_0_7
    );
\y_reg[8]_i_6__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_2[7]_15\(8),
      I1 => dout0_out(8),
      I2 => ctrl_mux_out_2(1),
      I3 => \y_reg[23]_i_3__0\(8),
      I4 => ctrl_mux_out_2(0),
      I5 => \y_reg[23]_i_3__0_0\(8),
      O => ram_reg_0_8
    );
\y_reg[9]_i_6__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_2[7]_15\(9),
      I1 => dout0_out(9),
      I2 => ctrl_mux_out_2(1),
      I3 => \y_reg[23]_i_3__0\(9),
      I4 => ctrl_mux_out_2(0),
      I5 => \y_reg[23]_i_3__0_0\(9),
      O => ram_reg_0_9
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity test_bd_pixelReOrder_0_0_rams_sp_rf_rst_27 is
  port (
    dout0_out : out STD_LOGIC_VECTOR ( 23 downto 0 );
    m00_axis_aclk : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 0 to 0 );
    addr : in STD_LOGIC_VECTOR ( 10 downto 0 );
    s00_axis_tdata : in STD_LOGIC_VECTOR ( 23 downto 0 );
    s00_axis_tuser : in STD_LOGIC_VECTOR ( 0 to 0 );
    s00_axis_tvalid : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of test_bd_pixelReOrder_0_0_rams_sp_rf_rst_27 : entity is "rams_sp_rf_rst";
end test_bd_pixelReOrder_0_0_rams_sp_rf_rst_27;

architecture STRUCTURE of test_bd_pixelReOrder_0_0_rams_sp_rf_rst_27 is
  signal \ram_reg_0_i_1__13_n_0\ : STD_LOGIC;
  signal NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_DBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_SBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_DOADO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 16 );
  signal NLW_ram_reg_0_DOBDO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_ram_reg_0_DOPADOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal NLW_ram_reg_0_DOPBDOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_ram_reg_0_ECCPARITY_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal NLW_ram_reg_0_RDADDRECC_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_ram_reg_1_DOADO_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 6 );
  signal NLW_ram_reg_1_DOBDO_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal NLW_ram_reg_1_DOPADOP_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_ram_reg_1_DOPBDOP_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ : string;
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of ram_reg_0 : label is "p2_d16";
  attribute METHODOLOGY_DRC_VIOS : string;
  attribute METHODOLOGY_DRC_VIOS of ram_reg_0 : label is "{SYNTH-6 {cell *THIS*}}";
  attribute RTL_RAM_BITS : integer;
  attribute RTL_RAM_BITS of ram_reg_0 : label is 46080;
  attribute RTL_RAM_NAME : string;
  attribute RTL_RAM_NAME of ram_reg_0 : label is "ram";
  attribute bram_addr_begin : integer;
  attribute bram_addr_begin of ram_reg_0 : label is 0;
  attribute bram_addr_end : integer;
  attribute bram_addr_end of ram_reg_0 : label is 2047;
  attribute bram_slice_begin : integer;
  attribute bram_slice_begin of ram_reg_0 : label is 0;
  attribute bram_slice_end : integer;
  attribute bram_slice_end of ram_reg_0 : label is 17;
  attribute ram_addr_begin : integer;
  attribute ram_addr_begin of ram_reg_0 : label is 0;
  attribute ram_addr_end : integer;
  attribute ram_addr_end of ram_reg_0 : label is 2047;
  attribute ram_offset : integer;
  attribute ram_offset of ram_reg_0 : label is 0;
  attribute ram_slice_begin : integer;
  attribute ram_slice_begin of ram_reg_0 : label is 0;
  attribute ram_slice_end : integer;
  attribute ram_slice_end of ram_reg_0 : label is 17;
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of ram_reg_1 : label is "p0_d6";
  attribute METHODOLOGY_DRC_VIOS of ram_reg_1 : label is "{SYNTH-6 {cell *THIS*}}";
  attribute RTL_RAM_BITS of ram_reg_1 : label is 46080;
  attribute RTL_RAM_NAME of ram_reg_1 : label is "ram";
  attribute bram_addr_begin of ram_reg_1 : label is 0;
  attribute bram_addr_end of ram_reg_1 : label is 2047;
  attribute bram_slice_begin of ram_reg_1 : label is 18;
  attribute bram_slice_end of ram_reg_1 : label is 23;
  attribute ram_addr_begin of ram_reg_1 : label is 0;
  attribute ram_addr_end of ram_reg_1 : label is 2047;
  attribute ram_offset of ram_reg_1 : label is 0;
  attribute ram_slice_begin of ram_reg_1 : label is 18;
  attribute ram_slice_end of ram_reg_1 : label is 23;
begin
ram_reg_0: unisim.vcomponents.RAMB36E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      EN_ECC_READ => false,
      EN_ECC_WRITE => false,
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      RAM_EXTENSION_A => "NONE",
      RAM_EXTENSION_B => "NONE",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "PERFORMANCE",
      READ_WIDTH_A => 18,
      READ_WIDTH_B => 0,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "READ_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 18,
      WRITE_WIDTH_B => 0
    )
        port map (
      ADDRARDADDR(15) => '1',
      ADDRARDADDR(14 downto 4) => addr(10 downto 0),
      ADDRARDADDR(3 downto 0) => B"1111",
      ADDRBWRADDR(15 downto 0) => B"1111111111111111",
      CASCADEINA => '1',
      CASCADEINB => '0',
      CASCADEOUTA => NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED,
      CASCADEOUTB => NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED,
      CLKARDCLK => m00_axis_aclk,
      CLKBWRCLK => '0',
      DBITERR => NLW_ram_reg_0_DBITERR_UNCONNECTED,
      DIADI(31 downto 16) => B"0000000000000000",
      DIADI(15 downto 0) => s00_axis_tdata(15 downto 0),
      DIBDI(31 downto 0) => B"11111111111111111111111111111111",
      DIPADIP(3 downto 2) => B"00",
      DIPADIP(1 downto 0) => s00_axis_tdata(17 downto 16),
      DIPBDIP(3 downto 0) => B"1111",
      DOADO(31 downto 16) => NLW_ram_reg_0_DOADO_UNCONNECTED(31 downto 16),
      DOADO(15 downto 0) => dout0_out(15 downto 0),
      DOBDO(31 downto 0) => NLW_ram_reg_0_DOBDO_UNCONNECTED(31 downto 0),
      DOPADOP(3 downto 2) => NLW_ram_reg_0_DOPADOP_UNCONNECTED(3 downto 2),
      DOPADOP(1 downto 0) => dout0_out(17 downto 16),
      DOPBDOP(3 downto 0) => NLW_ram_reg_0_DOPBDOP_UNCONNECTED(3 downto 0),
      ECCPARITY(7 downto 0) => NLW_ram_reg_0_ECCPARITY_UNCONNECTED(7 downto 0),
      ENARDEN => Q(0),
      ENBWREN => '0',
      INJECTDBITERR => NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED,
      INJECTSBITERR => NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED,
      RDADDRECC(8 downto 0) => NLW_ram_reg_0_RDADDRECC_UNCONNECTED(8 downto 0),
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      SBITERR => NLW_ram_reg_0_SBITERR_UNCONNECTED,
      WEA(3) => \ram_reg_0_i_1__13_n_0\,
      WEA(2) => \ram_reg_0_i_1__13_n_0\,
      WEA(1) => \ram_reg_0_i_1__13_n_0\,
      WEA(0) => \ram_reg_0_i_1__13_n_0\,
      WEBWE(7 downto 0) => B"00000000"
    );
\ram_reg_0_i_1__13\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => Q(0),
      I1 => s00_axis_tuser(0),
      I2 => s00_axis_tvalid,
      O => \ram_reg_0_i_1__13_n_0\
    );
ram_reg_1: unisim.vcomponents.RAMB18E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      INIT_A => X"00000",
      INIT_B => X"00000",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "PERFORMANCE",
      READ_WIDTH_A => 9,
      READ_WIDTH_B => 0,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"00000",
      SRVAL_B => X"00000",
      WRITE_MODE_A => "READ_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 9,
      WRITE_WIDTH_B => 0
    )
        port map (
      ADDRARDADDR(13 downto 3) => addr(10 downto 0),
      ADDRARDADDR(2 downto 0) => B"111",
      ADDRBWRADDR(13 downto 0) => B"11111111111111",
      CLKARDCLK => m00_axis_aclk,
      CLKBWRCLK => '0',
      DIADI(15 downto 6) => B"0000000000",
      DIADI(5 downto 0) => s00_axis_tdata(23 downto 18),
      DIBDI(15 downto 0) => B"1111111111111111",
      DIPADIP(1 downto 0) => B"00",
      DIPBDIP(1 downto 0) => B"11",
      DOADO(15 downto 6) => NLW_ram_reg_1_DOADO_UNCONNECTED(15 downto 6),
      DOADO(5 downto 0) => dout0_out(23 downto 18),
      DOBDO(15 downto 0) => NLW_ram_reg_1_DOBDO_UNCONNECTED(15 downto 0),
      DOPADOP(1 downto 0) => NLW_ram_reg_1_DOPADOP_UNCONNECTED(1 downto 0),
      DOPBDOP(1 downto 0) => NLW_ram_reg_1_DOPBDOP_UNCONNECTED(1 downto 0),
      ENARDEN => Q(0),
      ENBWREN => '0',
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      WEA(1) => \ram_reg_0_i_1__13_n_0\,
      WEA(0) => \ram_reg_0_i_1__13_n_0\,
      WEBWE(3 downto 0) => B"0000"
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity test_bd_pixelReOrder_0_0_rams_sp_rf_rst_28 is
  port (
    dout0_out : out STD_LOGIC_VECTOR ( 23 downto 0 );
    m00_axis_aclk : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 0 to 0 );
    addr : in STD_LOGIC_VECTOR ( 10 downto 0 );
    s00_axis_tdata : in STD_LOGIC_VECTOR ( 23 downto 0 );
    s00_axis_tuser : in STD_LOGIC_VECTOR ( 0 to 0 );
    s00_axis_tvalid : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of test_bd_pixelReOrder_0_0_rams_sp_rf_rst_28 : entity is "rams_sp_rf_rst";
end test_bd_pixelReOrder_0_0_rams_sp_rf_rst_28;

architecture STRUCTURE of test_bd_pixelReOrder_0_0_rams_sp_rf_rst_28 is
  signal \ram_reg_0_i_1__14_n_0\ : STD_LOGIC;
  signal NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_DBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_SBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_DOADO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 16 );
  signal NLW_ram_reg_0_DOBDO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_ram_reg_0_DOPADOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal NLW_ram_reg_0_DOPBDOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_ram_reg_0_ECCPARITY_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal NLW_ram_reg_0_RDADDRECC_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_ram_reg_1_DOADO_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 6 );
  signal NLW_ram_reg_1_DOBDO_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal NLW_ram_reg_1_DOPADOP_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_ram_reg_1_DOPBDOP_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ : string;
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of ram_reg_0 : label is "p2_d16";
  attribute METHODOLOGY_DRC_VIOS : string;
  attribute METHODOLOGY_DRC_VIOS of ram_reg_0 : label is "{SYNTH-6 {cell *THIS*}}";
  attribute RTL_RAM_BITS : integer;
  attribute RTL_RAM_BITS of ram_reg_0 : label is 46080;
  attribute RTL_RAM_NAME : string;
  attribute RTL_RAM_NAME of ram_reg_0 : label is "ram";
  attribute bram_addr_begin : integer;
  attribute bram_addr_begin of ram_reg_0 : label is 0;
  attribute bram_addr_end : integer;
  attribute bram_addr_end of ram_reg_0 : label is 2047;
  attribute bram_slice_begin : integer;
  attribute bram_slice_begin of ram_reg_0 : label is 0;
  attribute bram_slice_end : integer;
  attribute bram_slice_end of ram_reg_0 : label is 17;
  attribute ram_addr_begin : integer;
  attribute ram_addr_begin of ram_reg_0 : label is 0;
  attribute ram_addr_end : integer;
  attribute ram_addr_end of ram_reg_0 : label is 2047;
  attribute ram_offset : integer;
  attribute ram_offset of ram_reg_0 : label is 0;
  attribute ram_slice_begin : integer;
  attribute ram_slice_begin of ram_reg_0 : label is 0;
  attribute ram_slice_end : integer;
  attribute ram_slice_end of ram_reg_0 : label is 17;
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of ram_reg_1 : label is "p0_d6";
  attribute METHODOLOGY_DRC_VIOS of ram_reg_1 : label is "{SYNTH-6 {cell *THIS*}}";
  attribute RTL_RAM_BITS of ram_reg_1 : label is 46080;
  attribute RTL_RAM_NAME of ram_reg_1 : label is "ram";
  attribute bram_addr_begin of ram_reg_1 : label is 0;
  attribute bram_addr_end of ram_reg_1 : label is 2047;
  attribute bram_slice_begin of ram_reg_1 : label is 18;
  attribute bram_slice_end of ram_reg_1 : label is 23;
  attribute ram_addr_begin of ram_reg_1 : label is 0;
  attribute ram_addr_end of ram_reg_1 : label is 2047;
  attribute ram_offset of ram_reg_1 : label is 0;
  attribute ram_slice_begin of ram_reg_1 : label is 18;
  attribute ram_slice_end of ram_reg_1 : label is 23;
begin
ram_reg_0: unisim.vcomponents.RAMB36E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      EN_ECC_READ => false,
      EN_ECC_WRITE => false,
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      RAM_EXTENSION_A => "NONE",
      RAM_EXTENSION_B => "NONE",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "PERFORMANCE",
      READ_WIDTH_A => 18,
      READ_WIDTH_B => 0,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "READ_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 18,
      WRITE_WIDTH_B => 0
    )
        port map (
      ADDRARDADDR(15) => '1',
      ADDRARDADDR(14 downto 4) => addr(10 downto 0),
      ADDRARDADDR(3 downto 0) => B"1111",
      ADDRBWRADDR(15 downto 0) => B"1111111111111111",
      CASCADEINA => '1',
      CASCADEINB => '0',
      CASCADEOUTA => NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED,
      CASCADEOUTB => NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED,
      CLKARDCLK => m00_axis_aclk,
      CLKBWRCLK => '0',
      DBITERR => NLW_ram_reg_0_DBITERR_UNCONNECTED,
      DIADI(31 downto 16) => B"0000000000000000",
      DIADI(15 downto 0) => s00_axis_tdata(15 downto 0),
      DIBDI(31 downto 0) => B"11111111111111111111111111111111",
      DIPADIP(3 downto 2) => B"00",
      DIPADIP(1 downto 0) => s00_axis_tdata(17 downto 16),
      DIPBDIP(3 downto 0) => B"1111",
      DOADO(31 downto 16) => NLW_ram_reg_0_DOADO_UNCONNECTED(31 downto 16),
      DOADO(15 downto 0) => dout0_out(15 downto 0),
      DOBDO(31 downto 0) => NLW_ram_reg_0_DOBDO_UNCONNECTED(31 downto 0),
      DOPADOP(3 downto 2) => NLW_ram_reg_0_DOPADOP_UNCONNECTED(3 downto 2),
      DOPADOP(1 downto 0) => dout0_out(17 downto 16),
      DOPBDOP(3 downto 0) => NLW_ram_reg_0_DOPBDOP_UNCONNECTED(3 downto 0),
      ECCPARITY(7 downto 0) => NLW_ram_reg_0_ECCPARITY_UNCONNECTED(7 downto 0),
      ENARDEN => Q(0),
      ENBWREN => '0',
      INJECTDBITERR => NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED,
      INJECTSBITERR => NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED,
      RDADDRECC(8 downto 0) => NLW_ram_reg_0_RDADDRECC_UNCONNECTED(8 downto 0),
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      SBITERR => NLW_ram_reg_0_SBITERR_UNCONNECTED,
      WEA(3) => \ram_reg_0_i_1__14_n_0\,
      WEA(2) => \ram_reg_0_i_1__14_n_0\,
      WEA(1) => \ram_reg_0_i_1__14_n_0\,
      WEA(0) => \ram_reg_0_i_1__14_n_0\,
      WEBWE(7 downto 0) => B"00000000"
    );
\ram_reg_0_i_1__14\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => Q(0),
      I1 => s00_axis_tuser(0),
      I2 => s00_axis_tvalid,
      O => \ram_reg_0_i_1__14_n_0\
    );
ram_reg_1: unisim.vcomponents.RAMB18E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      INIT_A => X"00000",
      INIT_B => X"00000",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "PERFORMANCE",
      READ_WIDTH_A => 9,
      READ_WIDTH_B => 0,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"00000",
      SRVAL_B => X"00000",
      WRITE_MODE_A => "READ_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 9,
      WRITE_WIDTH_B => 0
    )
        port map (
      ADDRARDADDR(13 downto 3) => addr(10 downto 0),
      ADDRARDADDR(2 downto 0) => B"111",
      ADDRBWRADDR(13 downto 0) => B"11111111111111",
      CLKARDCLK => m00_axis_aclk,
      CLKBWRCLK => '0',
      DIADI(15 downto 6) => B"0000000000",
      DIADI(5 downto 0) => s00_axis_tdata(23 downto 18),
      DIBDI(15 downto 0) => B"1111111111111111",
      DIPADIP(1 downto 0) => B"00",
      DIPBDIP(1 downto 0) => B"11",
      DOADO(15 downto 6) => NLW_ram_reg_1_DOADO_UNCONNECTED(15 downto 6),
      DOADO(5 downto 0) => dout0_out(23 downto 18),
      DOBDO(15 downto 0) => NLW_ram_reg_1_DOBDO_UNCONNECTED(15 downto 0),
      DOPADOP(1 downto 0) => NLW_ram_reg_1_DOPADOP_UNCONNECTED(1 downto 0),
      DOPBDOP(1 downto 0) => NLW_ram_reg_1_DOPBDOP_UNCONNECTED(1 downto 0),
      ENARDEN => Q(0),
      ENBWREN => '0',
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      WEA(1) => \ram_reg_0_i_1__14_n_0\,
      WEA(0) => \ram_reg_0_i_1__14_n_0\,
      WEBWE(3 downto 0) => B"0000"
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity test_bd_pixelReOrder_0_0_rams_sp_rf_rst_29 is
  port (
    dout0_out : out STD_LOGIC_VECTOR ( 23 downto 0 );
    m00_axis_aclk : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 0 to 0 );
    addr : in STD_LOGIC_VECTOR ( 10 downto 0 );
    s00_axis_tdata : in STD_LOGIC_VECTOR ( 23 downto 0 );
    s00_axis_tuser : in STD_LOGIC_VECTOR ( 0 to 0 );
    s00_axis_tvalid : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of test_bd_pixelReOrder_0_0_rams_sp_rf_rst_29 : entity is "rams_sp_rf_rst";
end test_bd_pixelReOrder_0_0_rams_sp_rf_rst_29;

architecture STRUCTURE of test_bd_pixelReOrder_0_0_rams_sp_rf_rst_29 is
  signal \ram_reg_0_i_1__15_n_0\ : STD_LOGIC;
  signal NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_DBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_SBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_DOADO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 16 );
  signal NLW_ram_reg_0_DOBDO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_ram_reg_0_DOPADOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal NLW_ram_reg_0_DOPBDOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_ram_reg_0_ECCPARITY_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal NLW_ram_reg_0_RDADDRECC_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_ram_reg_1_DOADO_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 6 );
  signal NLW_ram_reg_1_DOBDO_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal NLW_ram_reg_1_DOPADOP_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_ram_reg_1_DOPBDOP_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ : string;
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of ram_reg_0 : label is "p2_d16";
  attribute METHODOLOGY_DRC_VIOS : string;
  attribute METHODOLOGY_DRC_VIOS of ram_reg_0 : label is "{SYNTH-6 {cell *THIS*}}";
  attribute RTL_RAM_BITS : integer;
  attribute RTL_RAM_BITS of ram_reg_0 : label is 46080;
  attribute RTL_RAM_NAME : string;
  attribute RTL_RAM_NAME of ram_reg_0 : label is "ram";
  attribute bram_addr_begin : integer;
  attribute bram_addr_begin of ram_reg_0 : label is 0;
  attribute bram_addr_end : integer;
  attribute bram_addr_end of ram_reg_0 : label is 2047;
  attribute bram_slice_begin : integer;
  attribute bram_slice_begin of ram_reg_0 : label is 0;
  attribute bram_slice_end : integer;
  attribute bram_slice_end of ram_reg_0 : label is 17;
  attribute ram_addr_begin : integer;
  attribute ram_addr_begin of ram_reg_0 : label is 0;
  attribute ram_addr_end : integer;
  attribute ram_addr_end of ram_reg_0 : label is 2047;
  attribute ram_offset : integer;
  attribute ram_offset of ram_reg_0 : label is 0;
  attribute ram_slice_begin : integer;
  attribute ram_slice_begin of ram_reg_0 : label is 0;
  attribute ram_slice_end : integer;
  attribute ram_slice_end of ram_reg_0 : label is 17;
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of ram_reg_1 : label is "p0_d6";
  attribute METHODOLOGY_DRC_VIOS of ram_reg_1 : label is "{SYNTH-6 {cell *THIS*}}";
  attribute RTL_RAM_BITS of ram_reg_1 : label is 46080;
  attribute RTL_RAM_NAME of ram_reg_1 : label is "ram";
  attribute bram_addr_begin of ram_reg_1 : label is 0;
  attribute bram_addr_end of ram_reg_1 : label is 2047;
  attribute bram_slice_begin of ram_reg_1 : label is 18;
  attribute bram_slice_end of ram_reg_1 : label is 23;
  attribute ram_addr_begin of ram_reg_1 : label is 0;
  attribute ram_addr_end of ram_reg_1 : label is 2047;
  attribute ram_offset of ram_reg_1 : label is 0;
  attribute ram_slice_begin of ram_reg_1 : label is 18;
  attribute ram_slice_end of ram_reg_1 : label is 23;
begin
ram_reg_0: unisim.vcomponents.RAMB36E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      EN_ECC_READ => false,
      EN_ECC_WRITE => false,
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      RAM_EXTENSION_A => "NONE",
      RAM_EXTENSION_B => "NONE",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "PERFORMANCE",
      READ_WIDTH_A => 18,
      READ_WIDTH_B => 0,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "READ_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 18,
      WRITE_WIDTH_B => 0
    )
        port map (
      ADDRARDADDR(15) => '1',
      ADDRARDADDR(14 downto 4) => addr(10 downto 0),
      ADDRARDADDR(3 downto 0) => B"1111",
      ADDRBWRADDR(15 downto 0) => B"1111111111111111",
      CASCADEINA => '1',
      CASCADEINB => '0',
      CASCADEOUTA => NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED,
      CASCADEOUTB => NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED,
      CLKARDCLK => m00_axis_aclk,
      CLKBWRCLK => '0',
      DBITERR => NLW_ram_reg_0_DBITERR_UNCONNECTED,
      DIADI(31 downto 16) => B"0000000000000000",
      DIADI(15 downto 0) => s00_axis_tdata(15 downto 0),
      DIBDI(31 downto 0) => B"11111111111111111111111111111111",
      DIPADIP(3 downto 2) => B"00",
      DIPADIP(1 downto 0) => s00_axis_tdata(17 downto 16),
      DIPBDIP(3 downto 0) => B"1111",
      DOADO(31 downto 16) => NLW_ram_reg_0_DOADO_UNCONNECTED(31 downto 16),
      DOADO(15 downto 0) => dout0_out(15 downto 0),
      DOBDO(31 downto 0) => NLW_ram_reg_0_DOBDO_UNCONNECTED(31 downto 0),
      DOPADOP(3 downto 2) => NLW_ram_reg_0_DOPADOP_UNCONNECTED(3 downto 2),
      DOPADOP(1 downto 0) => dout0_out(17 downto 16),
      DOPBDOP(3 downto 0) => NLW_ram_reg_0_DOPBDOP_UNCONNECTED(3 downto 0),
      ECCPARITY(7 downto 0) => NLW_ram_reg_0_ECCPARITY_UNCONNECTED(7 downto 0),
      ENARDEN => Q(0),
      ENBWREN => '0',
      INJECTDBITERR => NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED,
      INJECTSBITERR => NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED,
      RDADDRECC(8 downto 0) => NLW_ram_reg_0_RDADDRECC_UNCONNECTED(8 downto 0),
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      SBITERR => NLW_ram_reg_0_SBITERR_UNCONNECTED,
      WEA(3) => \ram_reg_0_i_1__15_n_0\,
      WEA(2) => \ram_reg_0_i_1__15_n_0\,
      WEA(1) => \ram_reg_0_i_1__15_n_0\,
      WEA(0) => \ram_reg_0_i_1__15_n_0\,
      WEBWE(7 downto 0) => B"00000000"
    );
\ram_reg_0_i_1__15\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => Q(0),
      I1 => s00_axis_tuser(0),
      I2 => s00_axis_tvalid,
      O => \ram_reg_0_i_1__15_n_0\
    );
ram_reg_1: unisim.vcomponents.RAMB18E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      INIT_A => X"00000",
      INIT_B => X"00000",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "PERFORMANCE",
      READ_WIDTH_A => 9,
      READ_WIDTH_B => 0,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"00000",
      SRVAL_B => X"00000",
      WRITE_MODE_A => "READ_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 9,
      WRITE_WIDTH_B => 0
    )
        port map (
      ADDRARDADDR(13 downto 3) => addr(10 downto 0),
      ADDRARDADDR(2 downto 0) => B"111",
      ADDRBWRADDR(13 downto 0) => B"11111111111111",
      CLKARDCLK => m00_axis_aclk,
      CLKBWRCLK => '0',
      DIADI(15 downto 6) => B"0000000000",
      DIADI(5 downto 0) => s00_axis_tdata(23 downto 18),
      DIBDI(15 downto 0) => B"1111111111111111",
      DIPADIP(1 downto 0) => B"00",
      DIPBDIP(1 downto 0) => B"11",
      DOADO(15 downto 6) => NLW_ram_reg_1_DOADO_UNCONNECTED(15 downto 6),
      DOADO(5 downto 0) => dout0_out(23 downto 18),
      DOBDO(15 downto 0) => NLW_ram_reg_1_DOBDO_UNCONNECTED(15 downto 0),
      DOPADOP(1 downto 0) => NLW_ram_reg_1_DOPADOP_UNCONNECTED(1 downto 0),
      DOPBDOP(1 downto 0) => NLW_ram_reg_1_DOPBDOP_UNCONNECTED(1 downto 0),
      ENARDEN => Q(0),
      ENBWREN => '0',
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      WEA(1) => \ram_reg_0_i_1__15_n_0\,
      WEA(0) => \ram_reg_0_i_1__15_n_0\,
      WEBWE(3 downto 0) => B"0000"
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity test_bd_pixelReOrder_0_0_rams_sp_rf_rst_3 is
  port (
    ram_reg_0_0 : out STD_LOGIC;
    ram_reg_0_1 : out STD_LOGIC;
    ram_reg_0_2 : out STD_LOGIC;
    ram_reg_0_3 : out STD_LOGIC;
    ram_reg_0_4 : out STD_LOGIC;
    ram_reg_0_5 : out STD_LOGIC;
    ram_reg_0_6 : out STD_LOGIC;
    ram_reg_0_7 : out STD_LOGIC;
    ram_reg_0_8 : out STD_LOGIC;
    ram_reg_0_9 : out STD_LOGIC;
    ram_reg_0_10 : out STD_LOGIC;
    ram_reg_0_11 : out STD_LOGIC;
    ram_reg_0_12 : out STD_LOGIC;
    ram_reg_0_13 : out STD_LOGIC;
    ram_reg_0_14 : out STD_LOGIC;
    ram_reg_0_15 : out STD_LOGIC;
    ram_reg_0_16 : out STD_LOGIC;
    ram_reg_0_17 : out STD_LOGIC;
    ram_reg_1_0 : out STD_LOGIC;
    ram_reg_1_1 : out STD_LOGIC;
    ram_reg_1_2 : out STD_LOGIC;
    ram_reg_1_3 : out STD_LOGIC;
    ram_reg_1_4 : out STD_LOGIC;
    ram_reg_1_5 : out STD_LOGIC;
    dout0_out : in STD_LOGIC_VECTOR ( 23 downto 0 );
    ctrl_mux_out_1 : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \y_reg[23]_i_1\ : in STD_LOGIC_VECTOR ( 23 downto 0 );
    \y_reg[23]_i_1_0\ : in STD_LOGIC_VECTOR ( 23 downto 0 );
    m00_axis_aclk : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 0 to 0 );
    addr : in STD_LOGIC_VECTOR ( 10 downto 0 );
    s00_axis_tdata : in STD_LOGIC_VECTOR ( 23 downto 0 );
    s00_axis_tuser : in STD_LOGIC_VECTOR ( 0 to 0 );
    s00_axis_tvalid : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of test_bd_pixelReOrder_0_0_rams_sp_rf_rst_3 : entity is "rams_sp_rf_rst";
end test_bd_pixelReOrder_0_0_rams_sp_rf_rst_3;

architecture STRUCTURE of test_bd_pixelReOrder_0_0_rams_sp_rf_rst_3 is
  signal \_DOUT_1[11]_22\ : STD_LOGIC_VECTOR ( 23 downto 0 );
  signal \ram_reg_0_i_1__19_n_0\ : STD_LOGIC;
  signal NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_DBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_SBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_DOADO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 16 );
  signal NLW_ram_reg_0_DOBDO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_ram_reg_0_DOPADOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal NLW_ram_reg_0_DOPBDOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_ram_reg_0_ECCPARITY_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal NLW_ram_reg_0_RDADDRECC_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_ram_reg_1_DOADO_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 6 );
  signal NLW_ram_reg_1_DOBDO_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal NLW_ram_reg_1_DOPADOP_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_ram_reg_1_DOPBDOP_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ : string;
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of ram_reg_0 : label is "p2_d16";
  attribute METHODOLOGY_DRC_VIOS : string;
  attribute METHODOLOGY_DRC_VIOS of ram_reg_0 : label is "{SYNTH-6 {cell *THIS*}}";
  attribute RTL_RAM_BITS : integer;
  attribute RTL_RAM_BITS of ram_reg_0 : label is 46080;
  attribute RTL_RAM_NAME : string;
  attribute RTL_RAM_NAME of ram_reg_0 : label is "ram";
  attribute bram_addr_begin : integer;
  attribute bram_addr_begin of ram_reg_0 : label is 0;
  attribute bram_addr_end : integer;
  attribute bram_addr_end of ram_reg_0 : label is 2047;
  attribute bram_slice_begin : integer;
  attribute bram_slice_begin of ram_reg_0 : label is 0;
  attribute bram_slice_end : integer;
  attribute bram_slice_end of ram_reg_0 : label is 17;
  attribute ram_addr_begin : integer;
  attribute ram_addr_begin of ram_reg_0 : label is 0;
  attribute ram_addr_end : integer;
  attribute ram_addr_end of ram_reg_0 : label is 2047;
  attribute ram_offset : integer;
  attribute ram_offset of ram_reg_0 : label is 0;
  attribute ram_slice_begin : integer;
  attribute ram_slice_begin of ram_reg_0 : label is 0;
  attribute ram_slice_end : integer;
  attribute ram_slice_end of ram_reg_0 : label is 17;
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of ram_reg_1 : label is "p0_d6";
  attribute METHODOLOGY_DRC_VIOS of ram_reg_1 : label is "{SYNTH-6 {cell *THIS*}}";
  attribute RTL_RAM_BITS of ram_reg_1 : label is 46080;
  attribute RTL_RAM_NAME of ram_reg_1 : label is "ram";
  attribute bram_addr_begin of ram_reg_1 : label is 0;
  attribute bram_addr_end of ram_reg_1 : label is 2047;
  attribute bram_slice_begin of ram_reg_1 : label is 18;
  attribute bram_slice_end of ram_reg_1 : label is 23;
  attribute ram_addr_begin of ram_reg_1 : label is 0;
  attribute ram_addr_end of ram_reg_1 : label is 2047;
  attribute ram_offset of ram_reg_1 : label is 0;
  attribute ram_slice_begin of ram_reg_1 : label is 18;
  attribute ram_slice_end of ram_reg_1 : label is 23;
begin
ram_reg_0: unisim.vcomponents.RAMB36E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      EN_ECC_READ => false,
      EN_ECC_WRITE => false,
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      RAM_EXTENSION_A => "NONE",
      RAM_EXTENSION_B => "NONE",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "PERFORMANCE",
      READ_WIDTH_A => 18,
      READ_WIDTH_B => 0,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "READ_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 18,
      WRITE_WIDTH_B => 0
    )
        port map (
      ADDRARDADDR(15) => '1',
      ADDRARDADDR(14 downto 4) => addr(10 downto 0),
      ADDRARDADDR(3 downto 0) => B"1111",
      ADDRBWRADDR(15 downto 0) => B"1111111111111111",
      CASCADEINA => '1',
      CASCADEINB => '0',
      CASCADEOUTA => NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED,
      CASCADEOUTB => NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED,
      CLKARDCLK => m00_axis_aclk,
      CLKBWRCLK => '0',
      DBITERR => NLW_ram_reg_0_DBITERR_UNCONNECTED,
      DIADI(31 downto 16) => B"0000000000000000",
      DIADI(15 downto 0) => s00_axis_tdata(15 downto 0),
      DIBDI(31 downto 0) => B"11111111111111111111111111111111",
      DIPADIP(3 downto 2) => B"00",
      DIPADIP(1 downto 0) => s00_axis_tdata(17 downto 16),
      DIPBDIP(3 downto 0) => B"1111",
      DOADO(31 downto 16) => NLW_ram_reg_0_DOADO_UNCONNECTED(31 downto 16),
      DOADO(15 downto 0) => \_DOUT_1[11]_22\(15 downto 0),
      DOBDO(31 downto 0) => NLW_ram_reg_0_DOBDO_UNCONNECTED(31 downto 0),
      DOPADOP(3 downto 2) => NLW_ram_reg_0_DOPADOP_UNCONNECTED(3 downto 2),
      DOPADOP(1 downto 0) => \_DOUT_1[11]_22\(17 downto 16),
      DOPBDOP(3 downto 0) => NLW_ram_reg_0_DOPBDOP_UNCONNECTED(3 downto 0),
      ECCPARITY(7 downto 0) => NLW_ram_reg_0_ECCPARITY_UNCONNECTED(7 downto 0),
      ENARDEN => Q(0),
      ENBWREN => '0',
      INJECTDBITERR => NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED,
      INJECTSBITERR => NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED,
      RDADDRECC(8 downto 0) => NLW_ram_reg_0_RDADDRECC_UNCONNECTED(8 downto 0),
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      SBITERR => NLW_ram_reg_0_SBITERR_UNCONNECTED,
      WEA(3) => \ram_reg_0_i_1__19_n_0\,
      WEA(2) => \ram_reg_0_i_1__19_n_0\,
      WEA(1) => \ram_reg_0_i_1__19_n_0\,
      WEA(0) => \ram_reg_0_i_1__19_n_0\,
      WEBWE(7 downto 0) => B"00000000"
    );
\ram_reg_0_i_1__19\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => Q(0),
      I1 => s00_axis_tuser(0),
      I2 => s00_axis_tvalid,
      O => \ram_reg_0_i_1__19_n_0\
    );
ram_reg_1: unisim.vcomponents.RAMB18E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      INIT_A => X"00000",
      INIT_B => X"00000",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "PERFORMANCE",
      READ_WIDTH_A => 9,
      READ_WIDTH_B => 0,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"00000",
      SRVAL_B => X"00000",
      WRITE_MODE_A => "READ_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 9,
      WRITE_WIDTH_B => 0
    )
        port map (
      ADDRARDADDR(13 downto 3) => addr(10 downto 0),
      ADDRARDADDR(2 downto 0) => B"111",
      ADDRBWRADDR(13 downto 0) => B"11111111111111",
      CLKARDCLK => m00_axis_aclk,
      CLKBWRCLK => '0',
      DIADI(15 downto 6) => B"0000000000",
      DIADI(5 downto 0) => s00_axis_tdata(23 downto 18),
      DIBDI(15 downto 0) => B"1111111111111111",
      DIPADIP(1 downto 0) => B"00",
      DIPBDIP(1 downto 0) => B"11",
      DOADO(15 downto 6) => NLW_ram_reg_1_DOADO_UNCONNECTED(15 downto 6),
      DOADO(5 downto 0) => \_DOUT_1[11]_22\(23 downto 18),
      DOBDO(15 downto 0) => NLW_ram_reg_1_DOBDO_UNCONNECTED(15 downto 0),
      DOPADOP(1 downto 0) => NLW_ram_reg_1_DOPADOP_UNCONNECTED(1 downto 0),
      DOPBDOP(1 downto 0) => NLW_ram_reg_1_DOPBDOP_UNCONNECTED(1 downto 0),
      ENARDEN => Q(0),
      ENBWREN => '0',
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      WEA(1) => \ram_reg_0_i_1__19_n_0\,
      WEA(0) => \ram_reg_0_i_1__19_n_0\,
      WEBWE(3 downto 0) => B"0000"
    );
\y_reg[0]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_1[11]_22\(0),
      I1 => dout0_out(0),
      I2 => ctrl_mux_out_1(1),
      I3 => \y_reg[23]_i_1\(0),
      I4 => ctrl_mux_out_1(0),
      I5 => \y_reg[23]_i_1_0\(0),
      O => ram_reg_0_0
    );
\y_reg[10]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_1[11]_22\(10),
      I1 => dout0_out(10),
      I2 => ctrl_mux_out_1(1),
      I3 => \y_reg[23]_i_1\(10),
      I4 => ctrl_mux_out_1(0),
      I5 => \y_reg[23]_i_1_0\(10),
      O => ram_reg_0_10
    );
\y_reg[11]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_1[11]_22\(11),
      I1 => dout0_out(11),
      I2 => ctrl_mux_out_1(1),
      I3 => \y_reg[23]_i_1\(11),
      I4 => ctrl_mux_out_1(0),
      I5 => \y_reg[23]_i_1_0\(11),
      O => ram_reg_0_11
    );
\y_reg[12]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_1[11]_22\(12),
      I1 => dout0_out(12),
      I2 => ctrl_mux_out_1(1),
      I3 => \y_reg[23]_i_1\(12),
      I4 => ctrl_mux_out_1(0),
      I5 => \y_reg[23]_i_1_0\(12),
      O => ram_reg_0_12
    );
\y_reg[13]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_1[11]_22\(13),
      I1 => dout0_out(13),
      I2 => ctrl_mux_out_1(1),
      I3 => \y_reg[23]_i_1\(13),
      I4 => ctrl_mux_out_1(0),
      I5 => \y_reg[23]_i_1_0\(13),
      O => ram_reg_0_13
    );
\y_reg[14]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_1[11]_22\(14),
      I1 => dout0_out(14),
      I2 => ctrl_mux_out_1(1),
      I3 => \y_reg[23]_i_1\(14),
      I4 => ctrl_mux_out_1(0),
      I5 => \y_reg[23]_i_1_0\(14),
      O => ram_reg_0_14
    );
\y_reg[15]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_1[11]_22\(15),
      I1 => dout0_out(15),
      I2 => ctrl_mux_out_1(1),
      I3 => \y_reg[23]_i_1\(15),
      I4 => ctrl_mux_out_1(0),
      I5 => \y_reg[23]_i_1_0\(15),
      O => ram_reg_0_15
    );
\y_reg[16]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_1[11]_22\(16),
      I1 => dout0_out(16),
      I2 => ctrl_mux_out_1(1),
      I3 => \y_reg[23]_i_1\(16),
      I4 => ctrl_mux_out_1(0),
      I5 => \y_reg[23]_i_1_0\(16),
      O => ram_reg_0_16
    );
\y_reg[17]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_1[11]_22\(17),
      I1 => dout0_out(17),
      I2 => ctrl_mux_out_1(1),
      I3 => \y_reg[23]_i_1\(17),
      I4 => ctrl_mux_out_1(0),
      I5 => \y_reg[23]_i_1_0\(17),
      O => ram_reg_0_17
    );
\y_reg[18]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_1[11]_22\(18),
      I1 => dout0_out(18),
      I2 => ctrl_mux_out_1(1),
      I3 => \y_reg[23]_i_1\(18),
      I4 => ctrl_mux_out_1(0),
      I5 => \y_reg[23]_i_1_0\(18),
      O => ram_reg_1_0
    );
\y_reg[19]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_1[11]_22\(19),
      I1 => dout0_out(19),
      I2 => ctrl_mux_out_1(1),
      I3 => \y_reg[23]_i_1\(19),
      I4 => ctrl_mux_out_1(0),
      I5 => \y_reg[23]_i_1_0\(19),
      O => ram_reg_1_1
    );
\y_reg[1]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_1[11]_22\(1),
      I1 => dout0_out(1),
      I2 => ctrl_mux_out_1(1),
      I3 => \y_reg[23]_i_1\(1),
      I4 => ctrl_mux_out_1(0),
      I5 => \y_reg[23]_i_1_0\(1),
      O => ram_reg_0_1
    );
\y_reg[20]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_1[11]_22\(20),
      I1 => dout0_out(20),
      I2 => ctrl_mux_out_1(1),
      I3 => \y_reg[23]_i_1\(20),
      I4 => ctrl_mux_out_1(0),
      I5 => \y_reg[23]_i_1_0\(20),
      O => ram_reg_1_2
    );
\y_reg[21]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_1[11]_22\(21),
      I1 => dout0_out(21),
      I2 => ctrl_mux_out_1(1),
      I3 => \y_reg[23]_i_1\(21),
      I4 => ctrl_mux_out_1(0),
      I5 => \y_reg[23]_i_1_0\(21),
      O => ram_reg_1_3
    );
\y_reg[22]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_1[11]_22\(22),
      I1 => dout0_out(22),
      I2 => ctrl_mux_out_1(1),
      I3 => \y_reg[23]_i_1\(22),
      I4 => ctrl_mux_out_1(0),
      I5 => \y_reg[23]_i_1_0\(22),
      O => ram_reg_1_4
    );
\y_reg[23]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_1[11]_22\(23),
      I1 => dout0_out(23),
      I2 => ctrl_mux_out_1(1),
      I3 => \y_reg[23]_i_1\(23),
      I4 => ctrl_mux_out_1(0),
      I5 => \y_reg[23]_i_1_0\(23),
      O => ram_reg_1_5
    );
\y_reg[2]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_1[11]_22\(2),
      I1 => dout0_out(2),
      I2 => ctrl_mux_out_1(1),
      I3 => \y_reg[23]_i_1\(2),
      I4 => ctrl_mux_out_1(0),
      I5 => \y_reg[23]_i_1_0\(2),
      O => ram_reg_0_2
    );
\y_reg[3]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_1[11]_22\(3),
      I1 => dout0_out(3),
      I2 => ctrl_mux_out_1(1),
      I3 => \y_reg[23]_i_1\(3),
      I4 => ctrl_mux_out_1(0),
      I5 => \y_reg[23]_i_1_0\(3),
      O => ram_reg_0_3
    );
\y_reg[4]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_1[11]_22\(4),
      I1 => dout0_out(4),
      I2 => ctrl_mux_out_1(1),
      I3 => \y_reg[23]_i_1\(4),
      I4 => ctrl_mux_out_1(0),
      I5 => \y_reg[23]_i_1_0\(4),
      O => ram_reg_0_4
    );
\y_reg[5]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_1[11]_22\(5),
      I1 => dout0_out(5),
      I2 => ctrl_mux_out_1(1),
      I3 => \y_reg[23]_i_1\(5),
      I4 => ctrl_mux_out_1(0),
      I5 => \y_reg[23]_i_1_0\(5),
      O => ram_reg_0_5
    );
\y_reg[6]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_1[11]_22\(6),
      I1 => dout0_out(6),
      I2 => ctrl_mux_out_1(1),
      I3 => \y_reg[23]_i_1\(6),
      I4 => ctrl_mux_out_1(0),
      I5 => \y_reg[23]_i_1_0\(6),
      O => ram_reg_0_6
    );
\y_reg[7]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_1[11]_22\(7),
      I1 => dout0_out(7),
      I2 => ctrl_mux_out_1(1),
      I3 => \y_reg[23]_i_1\(7),
      I4 => ctrl_mux_out_1(0),
      I5 => \y_reg[23]_i_1_0\(7),
      O => ram_reg_0_7
    );
\y_reg[8]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_1[11]_22\(8),
      I1 => dout0_out(8),
      I2 => ctrl_mux_out_1(1),
      I3 => \y_reg[23]_i_1\(8),
      I4 => ctrl_mux_out_1(0),
      I5 => \y_reg[23]_i_1_0\(8),
      O => ram_reg_0_8
    );
\y_reg[9]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_1[11]_22\(9),
      I1 => dout0_out(9),
      I2 => ctrl_mux_out_1(1),
      I3 => \y_reg[23]_i_1\(9),
      I4 => ctrl_mux_out_1(0),
      I5 => \y_reg[23]_i_1_0\(9),
      O => ram_reg_0_9
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity test_bd_pixelReOrder_0_0_rams_sp_rf_rst_30 is
  port (
    dout0_out : out STD_LOGIC_VECTOR ( 23 downto 0 );
    m00_axis_aclk : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 0 to 0 );
    addr : in STD_LOGIC_VECTOR ( 10 downto 0 );
    s00_axis_tdata : in STD_LOGIC_VECTOR ( 23 downto 0 );
    s00_axis_tuser : in STD_LOGIC_VECTOR ( 0 to 0 );
    s00_axis_tvalid : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of test_bd_pixelReOrder_0_0_rams_sp_rf_rst_30 : entity is "rams_sp_rf_rst";
end test_bd_pixelReOrder_0_0_rams_sp_rf_rst_30;

architecture STRUCTURE of test_bd_pixelReOrder_0_0_rams_sp_rf_rst_30 is
  signal \ram_reg_0_i_1__16_n_0\ : STD_LOGIC;
  signal NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_DBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_SBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_DOADO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 16 );
  signal NLW_ram_reg_0_DOBDO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_ram_reg_0_DOPADOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal NLW_ram_reg_0_DOPBDOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_ram_reg_0_ECCPARITY_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal NLW_ram_reg_0_RDADDRECC_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_ram_reg_1_DOADO_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 6 );
  signal NLW_ram_reg_1_DOBDO_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal NLW_ram_reg_1_DOPADOP_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_ram_reg_1_DOPBDOP_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ : string;
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of ram_reg_0 : label is "p2_d16";
  attribute METHODOLOGY_DRC_VIOS : string;
  attribute METHODOLOGY_DRC_VIOS of ram_reg_0 : label is "{SYNTH-6 {cell *THIS*}}";
  attribute RTL_RAM_BITS : integer;
  attribute RTL_RAM_BITS of ram_reg_0 : label is 46080;
  attribute RTL_RAM_NAME : string;
  attribute RTL_RAM_NAME of ram_reg_0 : label is "ram";
  attribute bram_addr_begin : integer;
  attribute bram_addr_begin of ram_reg_0 : label is 0;
  attribute bram_addr_end : integer;
  attribute bram_addr_end of ram_reg_0 : label is 2047;
  attribute bram_slice_begin : integer;
  attribute bram_slice_begin of ram_reg_0 : label is 0;
  attribute bram_slice_end : integer;
  attribute bram_slice_end of ram_reg_0 : label is 17;
  attribute ram_addr_begin : integer;
  attribute ram_addr_begin of ram_reg_0 : label is 0;
  attribute ram_addr_end : integer;
  attribute ram_addr_end of ram_reg_0 : label is 2047;
  attribute ram_offset : integer;
  attribute ram_offset of ram_reg_0 : label is 0;
  attribute ram_slice_begin : integer;
  attribute ram_slice_begin of ram_reg_0 : label is 0;
  attribute ram_slice_end : integer;
  attribute ram_slice_end of ram_reg_0 : label is 17;
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of ram_reg_1 : label is "p0_d6";
  attribute METHODOLOGY_DRC_VIOS of ram_reg_1 : label is "{SYNTH-6 {cell *THIS*}}";
  attribute RTL_RAM_BITS of ram_reg_1 : label is 46080;
  attribute RTL_RAM_NAME of ram_reg_1 : label is "ram";
  attribute bram_addr_begin of ram_reg_1 : label is 0;
  attribute bram_addr_end of ram_reg_1 : label is 2047;
  attribute bram_slice_begin of ram_reg_1 : label is 18;
  attribute bram_slice_end of ram_reg_1 : label is 23;
  attribute ram_addr_begin of ram_reg_1 : label is 0;
  attribute ram_addr_end of ram_reg_1 : label is 2047;
  attribute ram_offset of ram_reg_1 : label is 0;
  attribute ram_slice_begin of ram_reg_1 : label is 18;
  attribute ram_slice_end of ram_reg_1 : label is 23;
begin
ram_reg_0: unisim.vcomponents.RAMB36E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      EN_ECC_READ => false,
      EN_ECC_WRITE => false,
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      RAM_EXTENSION_A => "NONE",
      RAM_EXTENSION_B => "NONE",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "PERFORMANCE",
      READ_WIDTH_A => 18,
      READ_WIDTH_B => 0,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "READ_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 18,
      WRITE_WIDTH_B => 0
    )
        port map (
      ADDRARDADDR(15) => '1',
      ADDRARDADDR(14 downto 4) => addr(10 downto 0),
      ADDRARDADDR(3 downto 0) => B"1111",
      ADDRBWRADDR(15 downto 0) => B"1111111111111111",
      CASCADEINA => '1',
      CASCADEINB => '0',
      CASCADEOUTA => NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED,
      CASCADEOUTB => NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED,
      CLKARDCLK => m00_axis_aclk,
      CLKBWRCLK => '0',
      DBITERR => NLW_ram_reg_0_DBITERR_UNCONNECTED,
      DIADI(31 downto 16) => B"0000000000000000",
      DIADI(15 downto 0) => s00_axis_tdata(15 downto 0),
      DIBDI(31 downto 0) => B"11111111111111111111111111111111",
      DIPADIP(3 downto 2) => B"00",
      DIPADIP(1 downto 0) => s00_axis_tdata(17 downto 16),
      DIPBDIP(3 downto 0) => B"1111",
      DOADO(31 downto 16) => NLW_ram_reg_0_DOADO_UNCONNECTED(31 downto 16),
      DOADO(15 downto 0) => dout0_out(15 downto 0),
      DOBDO(31 downto 0) => NLW_ram_reg_0_DOBDO_UNCONNECTED(31 downto 0),
      DOPADOP(3 downto 2) => NLW_ram_reg_0_DOPADOP_UNCONNECTED(3 downto 2),
      DOPADOP(1 downto 0) => dout0_out(17 downto 16),
      DOPBDOP(3 downto 0) => NLW_ram_reg_0_DOPBDOP_UNCONNECTED(3 downto 0),
      ECCPARITY(7 downto 0) => NLW_ram_reg_0_ECCPARITY_UNCONNECTED(7 downto 0),
      ENARDEN => Q(0),
      ENBWREN => '0',
      INJECTDBITERR => NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED,
      INJECTSBITERR => NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED,
      RDADDRECC(8 downto 0) => NLW_ram_reg_0_RDADDRECC_UNCONNECTED(8 downto 0),
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      SBITERR => NLW_ram_reg_0_SBITERR_UNCONNECTED,
      WEA(3) => \ram_reg_0_i_1__16_n_0\,
      WEA(2) => \ram_reg_0_i_1__16_n_0\,
      WEA(1) => \ram_reg_0_i_1__16_n_0\,
      WEA(0) => \ram_reg_0_i_1__16_n_0\,
      WEBWE(7 downto 0) => B"00000000"
    );
\ram_reg_0_i_1__16\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => Q(0),
      I1 => s00_axis_tuser(0),
      I2 => s00_axis_tvalid,
      O => \ram_reg_0_i_1__16_n_0\
    );
ram_reg_1: unisim.vcomponents.RAMB18E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      INIT_A => X"00000",
      INIT_B => X"00000",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "PERFORMANCE",
      READ_WIDTH_A => 9,
      READ_WIDTH_B => 0,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"00000",
      SRVAL_B => X"00000",
      WRITE_MODE_A => "READ_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 9,
      WRITE_WIDTH_B => 0
    )
        port map (
      ADDRARDADDR(13 downto 3) => addr(10 downto 0),
      ADDRARDADDR(2 downto 0) => B"111",
      ADDRBWRADDR(13 downto 0) => B"11111111111111",
      CLKARDCLK => m00_axis_aclk,
      CLKBWRCLK => '0',
      DIADI(15 downto 6) => B"0000000000",
      DIADI(5 downto 0) => s00_axis_tdata(23 downto 18),
      DIBDI(15 downto 0) => B"1111111111111111",
      DIPADIP(1 downto 0) => B"00",
      DIPBDIP(1 downto 0) => B"11",
      DOADO(15 downto 6) => NLW_ram_reg_1_DOADO_UNCONNECTED(15 downto 6),
      DOADO(5 downto 0) => dout0_out(23 downto 18),
      DOBDO(15 downto 0) => NLW_ram_reg_1_DOBDO_UNCONNECTED(15 downto 0),
      DOPADOP(1 downto 0) => NLW_ram_reg_1_DOPADOP_UNCONNECTED(1 downto 0),
      DOPBDOP(1 downto 0) => NLW_ram_reg_1_DOPBDOP_UNCONNECTED(1 downto 0),
      ENARDEN => Q(0),
      ENBWREN => '0',
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      WEA(1) => \ram_reg_0_i_1__16_n_0\,
      WEA(0) => \ram_reg_0_i_1__16_n_0\,
      WEBWE(3 downto 0) => B"0000"
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity test_bd_pixelReOrder_0_0_rams_sp_rf_rst_4 is
  port (
    ram_reg_0_0 : out STD_LOGIC;
    ram_reg_0_1 : out STD_LOGIC;
    ram_reg_0_2 : out STD_LOGIC;
    ram_reg_0_3 : out STD_LOGIC;
    ram_reg_0_4 : out STD_LOGIC;
    ram_reg_0_5 : out STD_LOGIC;
    ram_reg_0_6 : out STD_LOGIC;
    ram_reg_0_7 : out STD_LOGIC;
    ram_reg_0_8 : out STD_LOGIC;
    ram_reg_0_9 : out STD_LOGIC;
    ram_reg_0_10 : out STD_LOGIC;
    ram_reg_0_11 : out STD_LOGIC;
    ram_reg_0_12 : out STD_LOGIC;
    ram_reg_0_13 : out STD_LOGIC;
    ram_reg_0_14 : out STD_LOGIC;
    ram_reg_0_15 : out STD_LOGIC;
    ram_reg_0_16 : out STD_LOGIC;
    ram_reg_0_17 : out STD_LOGIC;
    ram_reg_1_0 : out STD_LOGIC;
    ram_reg_1_1 : out STD_LOGIC;
    ram_reg_1_2 : out STD_LOGIC;
    ram_reg_1_3 : out STD_LOGIC;
    ram_reg_1_4 : out STD_LOGIC;
    ram_reg_1_5 : out STD_LOGIC;
    dout0_out : in STD_LOGIC_VECTOR ( 23 downto 0 );
    ctrl_mux_out_2 : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \y_reg[23]_i_1__0\ : in STD_LOGIC_VECTOR ( 23 downto 0 );
    \y_reg[23]_i_1__0_0\ : in STD_LOGIC_VECTOR ( 23 downto 0 );
    m00_axis_aclk : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 0 to 0 );
    addr : in STD_LOGIC_VECTOR ( 10 downto 0 );
    s00_axis_tdata : in STD_LOGIC_VECTOR ( 23 downto 0 );
    s00_axis_tuser : in STD_LOGIC_VECTOR ( 0 to 0 );
    s00_axis_tvalid : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of test_bd_pixelReOrder_0_0_rams_sp_rf_rst_4 : entity is "rams_sp_rf_rst";
end test_bd_pixelReOrder_0_0_rams_sp_rf_rst_4;

architecture STRUCTURE of test_bd_pixelReOrder_0_0_rams_sp_rf_rst_4 is
  signal \_DOUT_2[11]_23\ : STD_LOGIC_VECTOR ( 23 downto 0 );
  signal \ram_reg_0_i_1__20_n_0\ : STD_LOGIC;
  signal NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_DBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_SBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_DOADO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 16 );
  signal NLW_ram_reg_0_DOBDO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_ram_reg_0_DOPADOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal NLW_ram_reg_0_DOPBDOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_ram_reg_0_ECCPARITY_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal NLW_ram_reg_0_RDADDRECC_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_ram_reg_1_DOADO_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 6 );
  signal NLW_ram_reg_1_DOBDO_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal NLW_ram_reg_1_DOPADOP_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_ram_reg_1_DOPBDOP_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ : string;
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of ram_reg_0 : label is "p2_d16";
  attribute METHODOLOGY_DRC_VIOS : string;
  attribute METHODOLOGY_DRC_VIOS of ram_reg_0 : label is "{SYNTH-6 {cell *THIS*}}";
  attribute RTL_RAM_BITS : integer;
  attribute RTL_RAM_BITS of ram_reg_0 : label is 46080;
  attribute RTL_RAM_NAME : string;
  attribute RTL_RAM_NAME of ram_reg_0 : label is "ram";
  attribute bram_addr_begin : integer;
  attribute bram_addr_begin of ram_reg_0 : label is 0;
  attribute bram_addr_end : integer;
  attribute bram_addr_end of ram_reg_0 : label is 2047;
  attribute bram_slice_begin : integer;
  attribute bram_slice_begin of ram_reg_0 : label is 0;
  attribute bram_slice_end : integer;
  attribute bram_slice_end of ram_reg_0 : label is 17;
  attribute ram_addr_begin : integer;
  attribute ram_addr_begin of ram_reg_0 : label is 0;
  attribute ram_addr_end : integer;
  attribute ram_addr_end of ram_reg_0 : label is 2047;
  attribute ram_offset : integer;
  attribute ram_offset of ram_reg_0 : label is 0;
  attribute ram_slice_begin : integer;
  attribute ram_slice_begin of ram_reg_0 : label is 0;
  attribute ram_slice_end : integer;
  attribute ram_slice_end of ram_reg_0 : label is 17;
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of ram_reg_1 : label is "p0_d6";
  attribute METHODOLOGY_DRC_VIOS of ram_reg_1 : label is "{SYNTH-6 {cell *THIS*}}";
  attribute RTL_RAM_BITS of ram_reg_1 : label is 46080;
  attribute RTL_RAM_NAME of ram_reg_1 : label is "ram";
  attribute bram_addr_begin of ram_reg_1 : label is 0;
  attribute bram_addr_end of ram_reg_1 : label is 2047;
  attribute bram_slice_begin of ram_reg_1 : label is 18;
  attribute bram_slice_end of ram_reg_1 : label is 23;
  attribute ram_addr_begin of ram_reg_1 : label is 0;
  attribute ram_addr_end of ram_reg_1 : label is 2047;
  attribute ram_offset of ram_reg_1 : label is 0;
  attribute ram_slice_begin of ram_reg_1 : label is 18;
  attribute ram_slice_end of ram_reg_1 : label is 23;
begin
ram_reg_0: unisim.vcomponents.RAMB36E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      EN_ECC_READ => false,
      EN_ECC_WRITE => false,
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      RAM_EXTENSION_A => "NONE",
      RAM_EXTENSION_B => "NONE",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "PERFORMANCE",
      READ_WIDTH_A => 18,
      READ_WIDTH_B => 0,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "READ_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 18,
      WRITE_WIDTH_B => 0
    )
        port map (
      ADDRARDADDR(15) => '1',
      ADDRARDADDR(14 downto 4) => addr(10 downto 0),
      ADDRARDADDR(3 downto 0) => B"1111",
      ADDRBWRADDR(15 downto 0) => B"1111111111111111",
      CASCADEINA => '1',
      CASCADEINB => '0',
      CASCADEOUTA => NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED,
      CASCADEOUTB => NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED,
      CLKARDCLK => m00_axis_aclk,
      CLKBWRCLK => '0',
      DBITERR => NLW_ram_reg_0_DBITERR_UNCONNECTED,
      DIADI(31 downto 16) => B"0000000000000000",
      DIADI(15 downto 0) => s00_axis_tdata(15 downto 0),
      DIBDI(31 downto 0) => B"11111111111111111111111111111111",
      DIPADIP(3 downto 2) => B"00",
      DIPADIP(1 downto 0) => s00_axis_tdata(17 downto 16),
      DIPBDIP(3 downto 0) => B"1111",
      DOADO(31 downto 16) => NLW_ram_reg_0_DOADO_UNCONNECTED(31 downto 16),
      DOADO(15 downto 0) => \_DOUT_2[11]_23\(15 downto 0),
      DOBDO(31 downto 0) => NLW_ram_reg_0_DOBDO_UNCONNECTED(31 downto 0),
      DOPADOP(3 downto 2) => NLW_ram_reg_0_DOPADOP_UNCONNECTED(3 downto 2),
      DOPADOP(1 downto 0) => \_DOUT_2[11]_23\(17 downto 16),
      DOPBDOP(3 downto 0) => NLW_ram_reg_0_DOPBDOP_UNCONNECTED(3 downto 0),
      ECCPARITY(7 downto 0) => NLW_ram_reg_0_ECCPARITY_UNCONNECTED(7 downto 0),
      ENARDEN => Q(0),
      ENBWREN => '0',
      INJECTDBITERR => NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED,
      INJECTSBITERR => NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED,
      RDADDRECC(8 downto 0) => NLW_ram_reg_0_RDADDRECC_UNCONNECTED(8 downto 0),
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      SBITERR => NLW_ram_reg_0_SBITERR_UNCONNECTED,
      WEA(3) => \ram_reg_0_i_1__20_n_0\,
      WEA(2) => \ram_reg_0_i_1__20_n_0\,
      WEA(1) => \ram_reg_0_i_1__20_n_0\,
      WEA(0) => \ram_reg_0_i_1__20_n_0\,
      WEBWE(7 downto 0) => B"00000000"
    );
\ram_reg_0_i_1__20\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => Q(0),
      I1 => s00_axis_tuser(0),
      I2 => s00_axis_tvalid,
      O => \ram_reg_0_i_1__20_n_0\
    );
ram_reg_1: unisim.vcomponents.RAMB18E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      INIT_A => X"00000",
      INIT_B => X"00000",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "PERFORMANCE",
      READ_WIDTH_A => 9,
      READ_WIDTH_B => 0,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"00000",
      SRVAL_B => X"00000",
      WRITE_MODE_A => "READ_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 9,
      WRITE_WIDTH_B => 0
    )
        port map (
      ADDRARDADDR(13 downto 3) => addr(10 downto 0),
      ADDRARDADDR(2 downto 0) => B"111",
      ADDRBWRADDR(13 downto 0) => B"11111111111111",
      CLKARDCLK => m00_axis_aclk,
      CLKBWRCLK => '0',
      DIADI(15 downto 6) => B"0000000000",
      DIADI(5 downto 0) => s00_axis_tdata(23 downto 18),
      DIBDI(15 downto 0) => B"1111111111111111",
      DIPADIP(1 downto 0) => B"00",
      DIPBDIP(1 downto 0) => B"11",
      DOADO(15 downto 6) => NLW_ram_reg_1_DOADO_UNCONNECTED(15 downto 6),
      DOADO(5 downto 0) => \_DOUT_2[11]_23\(23 downto 18),
      DOBDO(15 downto 0) => NLW_ram_reg_1_DOBDO_UNCONNECTED(15 downto 0),
      DOPADOP(1 downto 0) => NLW_ram_reg_1_DOPADOP_UNCONNECTED(1 downto 0),
      DOPBDOP(1 downto 0) => NLW_ram_reg_1_DOPBDOP_UNCONNECTED(1 downto 0),
      ENARDEN => Q(0),
      ENBWREN => '0',
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      WEA(1) => \ram_reg_0_i_1__20_n_0\,
      WEA(0) => \ram_reg_0_i_1__20_n_0\,
      WEBWE(3 downto 0) => B"0000"
    );
\y_reg[0]_i_3__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_2[11]_23\(0),
      I1 => dout0_out(0),
      I2 => ctrl_mux_out_2(1),
      I3 => \y_reg[23]_i_1__0\(0),
      I4 => ctrl_mux_out_2(0),
      I5 => \y_reg[23]_i_1__0_0\(0),
      O => ram_reg_0_0
    );
\y_reg[10]_i_3__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_2[11]_23\(10),
      I1 => dout0_out(10),
      I2 => ctrl_mux_out_2(1),
      I3 => \y_reg[23]_i_1__0\(10),
      I4 => ctrl_mux_out_2(0),
      I5 => \y_reg[23]_i_1__0_0\(10),
      O => ram_reg_0_10
    );
\y_reg[11]_i_3__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_2[11]_23\(11),
      I1 => dout0_out(11),
      I2 => ctrl_mux_out_2(1),
      I3 => \y_reg[23]_i_1__0\(11),
      I4 => ctrl_mux_out_2(0),
      I5 => \y_reg[23]_i_1__0_0\(11),
      O => ram_reg_0_11
    );
\y_reg[12]_i_3__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_2[11]_23\(12),
      I1 => dout0_out(12),
      I2 => ctrl_mux_out_2(1),
      I3 => \y_reg[23]_i_1__0\(12),
      I4 => ctrl_mux_out_2(0),
      I5 => \y_reg[23]_i_1__0_0\(12),
      O => ram_reg_0_12
    );
\y_reg[13]_i_3__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_2[11]_23\(13),
      I1 => dout0_out(13),
      I2 => ctrl_mux_out_2(1),
      I3 => \y_reg[23]_i_1__0\(13),
      I4 => ctrl_mux_out_2(0),
      I5 => \y_reg[23]_i_1__0_0\(13),
      O => ram_reg_0_13
    );
\y_reg[14]_i_3__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_2[11]_23\(14),
      I1 => dout0_out(14),
      I2 => ctrl_mux_out_2(1),
      I3 => \y_reg[23]_i_1__0\(14),
      I4 => ctrl_mux_out_2(0),
      I5 => \y_reg[23]_i_1__0_0\(14),
      O => ram_reg_0_14
    );
\y_reg[15]_i_3__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_2[11]_23\(15),
      I1 => dout0_out(15),
      I2 => ctrl_mux_out_2(1),
      I3 => \y_reg[23]_i_1__0\(15),
      I4 => ctrl_mux_out_2(0),
      I5 => \y_reg[23]_i_1__0_0\(15),
      O => ram_reg_0_15
    );
\y_reg[16]_i_3__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_2[11]_23\(16),
      I1 => dout0_out(16),
      I2 => ctrl_mux_out_2(1),
      I3 => \y_reg[23]_i_1__0\(16),
      I4 => ctrl_mux_out_2(0),
      I5 => \y_reg[23]_i_1__0_0\(16),
      O => ram_reg_0_16
    );
\y_reg[17]_i_3__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_2[11]_23\(17),
      I1 => dout0_out(17),
      I2 => ctrl_mux_out_2(1),
      I3 => \y_reg[23]_i_1__0\(17),
      I4 => ctrl_mux_out_2(0),
      I5 => \y_reg[23]_i_1__0_0\(17),
      O => ram_reg_0_17
    );
\y_reg[18]_i_3__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_2[11]_23\(18),
      I1 => dout0_out(18),
      I2 => ctrl_mux_out_2(1),
      I3 => \y_reg[23]_i_1__0\(18),
      I4 => ctrl_mux_out_2(0),
      I5 => \y_reg[23]_i_1__0_0\(18),
      O => ram_reg_1_0
    );
\y_reg[19]_i_3__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_2[11]_23\(19),
      I1 => dout0_out(19),
      I2 => ctrl_mux_out_2(1),
      I3 => \y_reg[23]_i_1__0\(19),
      I4 => ctrl_mux_out_2(0),
      I5 => \y_reg[23]_i_1__0_0\(19),
      O => ram_reg_1_1
    );
\y_reg[1]_i_3__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_2[11]_23\(1),
      I1 => dout0_out(1),
      I2 => ctrl_mux_out_2(1),
      I3 => \y_reg[23]_i_1__0\(1),
      I4 => ctrl_mux_out_2(0),
      I5 => \y_reg[23]_i_1__0_0\(1),
      O => ram_reg_0_1
    );
\y_reg[20]_i_3__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_2[11]_23\(20),
      I1 => dout0_out(20),
      I2 => ctrl_mux_out_2(1),
      I3 => \y_reg[23]_i_1__0\(20),
      I4 => ctrl_mux_out_2(0),
      I5 => \y_reg[23]_i_1__0_0\(20),
      O => ram_reg_1_2
    );
\y_reg[21]_i_3__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_2[11]_23\(21),
      I1 => dout0_out(21),
      I2 => ctrl_mux_out_2(1),
      I3 => \y_reg[23]_i_1__0\(21),
      I4 => ctrl_mux_out_2(0),
      I5 => \y_reg[23]_i_1__0_0\(21),
      O => ram_reg_1_3
    );
\y_reg[22]_i_3__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_2[11]_23\(22),
      I1 => dout0_out(22),
      I2 => ctrl_mux_out_2(1),
      I3 => \y_reg[23]_i_1__0\(22),
      I4 => ctrl_mux_out_2(0),
      I5 => \y_reg[23]_i_1__0_0\(22),
      O => ram_reg_1_4
    );
\y_reg[23]_i_4__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_2[11]_23\(23),
      I1 => dout0_out(23),
      I2 => ctrl_mux_out_2(1),
      I3 => \y_reg[23]_i_1__0\(23),
      I4 => ctrl_mux_out_2(0),
      I5 => \y_reg[23]_i_1__0_0\(23),
      O => ram_reg_1_5
    );
\y_reg[2]_i_3__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_2[11]_23\(2),
      I1 => dout0_out(2),
      I2 => ctrl_mux_out_2(1),
      I3 => \y_reg[23]_i_1__0\(2),
      I4 => ctrl_mux_out_2(0),
      I5 => \y_reg[23]_i_1__0_0\(2),
      O => ram_reg_0_2
    );
\y_reg[3]_i_3__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_2[11]_23\(3),
      I1 => dout0_out(3),
      I2 => ctrl_mux_out_2(1),
      I3 => \y_reg[23]_i_1__0\(3),
      I4 => ctrl_mux_out_2(0),
      I5 => \y_reg[23]_i_1__0_0\(3),
      O => ram_reg_0_3
    );
\y_reg[4]_i_3__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_2[11]_23\(4),
      I1 => dout0_out(4),
      I2 => ctrl_mux_out_2(1),
      I3 => \y_reg[23]_i_1__0\(4),
      I4 => ctrl_mux_out_2(0),
      I5 => \y_reg[23]_i_1__0_0\(4),
      O => ram_reg_0_4
    );
\y_reg[5]_i_3__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_2[11]_23\(5),
      I1 => dout0_out(5),
      I2 => ctrl_mux_out_2(1),
      I3 => \y_reg[23]_i_1__0\(5),
      I4 => ctrl_mux_out_2(0),
      I5 => \y_reg[23]_i_1__0_0\(5),
      O => ram_reg_0_5
    );
\y_reg[6]_i_3__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_2[11]_23\(6),
      I1 => dout0_out(6),
      I2 => ctrl_mux_out_2(1),
      I3 => \y_reg[23]_i_1__0\(6),
      I4 => ctrl_mux_out_2(0),
      I5 => \y_reg[23]_i_1__0_0\(6),
      O => ram_reg_0_6
    );
\y_reg[7]_i_3__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_2[11]_23\(7),
      I1 => dout0_out(7),
      I2 => ctrl_mux_out_2(1),
      I3 => \y_reg[23]_i_1__0\(7),
      I4 => ctrl_mux_out_2(0),
      I5 => \y_reg[23]_i_1__0_0\(7),
      O => ram_reg_0_7
    );
\y_reg[8]_i_3__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_2[11]_23\(8),
      I1 => dout0_out(8),
      I2 => ctrl_mux_out_2(1),
      I3 => \y_reg[23]_i_1__0\(8),
      I4 => ctrl_mux_out_2(0),
      I5 => \y_reg[23]_i_1__0_0\(8),
      O => ram_reg_0_8
    );
\y_reg[9]_i_3__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \_DOUT_2[11]_23\(9),
      I1 => dout0_out(9),
      I2 => ctrl_mux_out_2(1),
      I3 => \y_reg[23]_i_1__0\(9),
      I4 => ctrl_mux_out_2(0),
      I5 => \y_reg[23]_i_1__0_0\(9),
      O => ram_reg_0_9
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity test_bd_pixelReOrder_0_0_rams_sp_rf_rst_5 is
  port (
    dout0_out : out STD_LOGIC_VECTOR ( 23 downto 0 );
    m00_axis_aclk : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 0 to 0 );
    addr : in STD_LOGIC_VECTOR ( 10 downto 0 );
    s00_axis_tdata : in STD_LOGIC_VECTOR ( 23 downto 0 );
    s00_axis_tuser : in STD_LOGIC_VECTOR ( 0 to 0 );
    s00_axis_tvalid : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of test_bd_pixelReOrder_0_0_rams_sp_rf_rst_5 : entity is "rams_sp_rf_rst";
end test_bd_pixelReOrder_0_0_rams_sp_rf_rst_5;

architecture STRUCTURE of test_bd_pixelReOrder_0_0_rams_sp_rf_rst_5 is
  signal \ram_reg_0_i_1__21_n_0\ : STD_LOGIC;
  signal NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_DBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_SBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_DOADO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 16 );
  signal NLW_ram_reg_0_DOBDO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_ram_reg_0_DOPADOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal NLW_ram_reg_0_DOPBDOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_ram_reg_0_ECCPARITY_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal NLW_ram_reg_0_RDADDRECC_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_ram_reg_1_DOADO_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 6 );
  signal NLW_ram_reg_1_DOBDO_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal NLW_ram_reg_1_DOPADOP_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_ram_reg_1_DOPBDOP_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ : string;
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of ram_reg_0 : label is "p2_d16";
  attribute METHODOLOGY_DRC_VIOS : string;
  attribute METHODOLOGY_DRC_VIOS of ram_reg_0 : label is "{SYNTH-6 {cell *THIS*}}";
  attribute RTL_RAM_BITS : integer;
  attribute RTL_RAM_BITS of ram_reg_0 : label is 46080;
  attribute RTL_RAM_NAME : string;
  attribute RTL_RAM_NAME of ram_reg_0 : label is "ram";
  attribute bram_addr_begin : integer;
  attribute bram_addr_begin of ram_reg_0 : label is 0;
  attribute bram_addr_end : integer;
  attribute bram_addr_end of ram_reg_0 : label is 2047;
  attribute bram_slice_begin : integer;
  attribute bram_slice_begin of ram_reg_0 : label is 0;
  attribute bram_slice_end : integer;
  attribute bram_slice_end of ram_reg_0 : label is 17;
  attribute ram_addr_begin : integer;
  attribute ram_addr_begin of ram_reg_0 : label is 0;
  attribute ram_addr_end : integer;
  attribute ram_addr_end of ram_reg_0 : label is 2047;
  attribute ram_offset : integer;
  attribute ram_offset of ram_reg_0 : label is 0;
  attribute ram_slice_begin : integer;
  attribute ram_slice_begin of ram_reg_0 : label is 0;
  attribute ram_slice_end : integer;
  attribute ram_slice_end of ram_reg_0 : label is 17;
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of ram_reg_1 : label is "p0_d6";
  attribute METHODOLOGY_DRC_VIOS of ram_reg_1 : label is "{SYNTH-6 {cell *THIS*}}";
  attribute RTL_RAM_BITS of ram_reg_1 : label is 46080;
  attribute RTL_RAM_NAME of ram_reg_1 : label is "ram";
  attribute bram_addr_begin of ram_reg_1 : label is 0;
  attribute bram_addr_end of ram_reg_1 : label is 2047;
  attribute bram_slice_begin of ram_reg_1 : label is 18;
  attribute bram_slice_end of ram_reg_1 : label is 23;
  attribute ram_addr_begin of ram_reg_1 : label is 0;
  attribute ram_addr_end of ram_reg_1 : label is 2047;
  attribute ram_offset of ram_reg_1 : label is 0;
  attribute ram_slice_begin of ram_reg_1 : label is 18;
  attribute ram_slice_end of ram_reg_1 : label is 23;
begin
ram_reg_0: unisim.vcomponents.RAMB36E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      EN_ECC_READ => false,
      EN_ECC_WRITE => false,
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      RAM_EXTENSION_A => "NONE",
      RAM_EXTENSION_B => "NONE",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "PERFORMANCE",
      READ_WIDTH_A => 18,
      READ_WIDTH_B => 0,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "READ_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 18,
      WRITE_WIDTH_B => 0
    )
        port map (
      ADDRARDADDR(15) => '1',
      ADDRARDADDR(14 downto 4) => addr(10 downto 0),
      ADDRARDADDR(3 downto 0) => B"1111",
      ADDRBWRADDR(15 downto 0) => B"1111111111111111",
      CASCADEINA => '1',
      CASCADEINB => '0',
      CASCADEOUTA => NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED,
      CASCADEOUTB => NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED,
      CLKARDCLK => m00_axis_aclk,
      CLKBWRCLK => '0',
      DBITERR => NLW_ram_reg_0_DBITERR_UNCONNECTED,
      DIADI(31 downto 16) => B"0000000000000000",
      DIADI(15 downto 0) => s00_axis_tdata(15 downto 0),
      DIBDI(31 downto 0) => B"11111111111111111111111111111111",
      DIPADIP(3 downto 2) => B"00",
      DIPADIP(1 downto 0) => s00_axis_tdata(17 downto 16),
      DIPBDIP(3 downto 0) => B"1111",
      DOADO(31 downto 16) => NLW_ram_reg_0_DOADO_UNCONNECTED(31 downto 16),
      DOADO(15 downto 0) => dout0_out(15 downto 0),
      DOBDO(31 downto 0) => NLW_ram_reg_0_DOBDO_UNCONNECTED(31 downto 0),
      DOPADOP(3 downto 2) => NLW_ram_reg_0_DOPADOP_UNCONNECTED(3 downto 2),
      DOPADOP(1 downto 0) => dout0_out(17 downto 16),
      DOPBDOP(3 downto 0) => NLW_ram_reg_0_DOPBDOP_UNCONNECTED(3 downto 0),
      ECCPARITY(7 downto 0) => NLW_ram_reg_0_ECCPARITY_UNCONNECTED(7 downto 0),
      ENARDEN => Q(0),
      ENBWREN => '0',
      INJECTDBITERR => NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED,
      INJECTSBITERR => NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED,
      RDADDRECC(8 downto 0) => NLW_ram_reg_0_RDADDRECC_UNCONNECTED(8 downto 0),
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      SBITERR => NLW_ram_reg_0_SBITERR_UNCONNECTED,
      WEA(3) => \ram_reg_0_i_1__21_n_0\,
      WEA(2) => \ram_reg_0_i_1__21_n_0\,
      WEA(1) => \ram_reg_0_i_1__21_n_0\,
      WEA(0) => \ram_reg_0_i_1__21_n_0\,
      WEBWE(7 downto 0) => B"00000000"
    );
\ram_reg_0_i_1__21\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => Q(0),
      I1 => s00_axis_tuser(0),
      I2 => s00_axis_tvalid,
      O => \ram_reg_0_i_1__21_n_0\
    );
ram_reg_1: unisim.vcomponents.RAMB18E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      INIT_A => X"00000",
      INIT_B => X"00000",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "PERFORMANCE",
      READ_WIDTH_A => 9,
      READ_WIDTH_B => 0,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"00000",
      SRVAL_B => X"00000",
      WRITE_MODE_A => "READ_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 9,
      WRITE_WIDTH_B => 0
    )
        port map (
      ADDRARDADDR(13 downto 3) => addr(10 downto 0),
      ADDRARDADDR(2 downto 0) => B"111",
      ADDRBWRADDR(13 downto 0) => B"11111111111111",
      CLKARDCLK => m00_axis_aclk,
      CLKBWRCLK => '0',
      DIADI(15 downto 6) => B"0000000000",
      DIADI(5 downto 0) => s00_axis_tdata(23 downto 18),
      DIBDI(15 downto 0) => B"1111111111111111",
      DIPADIP(1 downto 0) => B"00",
      DIPBDIP(1 downto 0) => B"11",
      DOADO(15 downto 6) => NLW_ram_reg_1_DOADO_UNCONNECTED(15 downto 6),
      DOADO(5 downto 0) => dout0_out(23 downto 18),
      DOBDO(15 downto 0) => NLW_ram_reg_1_DOBDO_UNCONNECTED(15 downto 0),
      DOPADOP(1 downto 0) => NLW_ram_reg_1_DOPADOP_UNCONNECTED(1 downto 0),
      DOPBDOP(1 downto 0) => NLW_ram_reg_1_DOPBDOP_UNCONNECTED(1 downto 0),
      ENARDEN => Q(0),
      ENBWREN => '0',
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      WEA(1) => \ram_reg_0_i_1__21_n_0\,
      WEA(0) => \ram_reg_0_i_1__21_n_0\,
      WEBWE(3 downto 0) => B"0000"
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity test_bd_pixelReOrder_0_0_rams_sp_rf_rst_6 is
  port (
    dout0_out : out STD_LOGIC_VECTOR ( 23 downto 0 );
    m00_axis_aclk : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 0 to 0 );
    addr : in STD_LOGIC_VECTOR ( 10 downto 0 );
    s00_axis_tdata : in STD_LOGIC_VECTOR ( 23 downto 0 );
    s00_axis_tuser : in STD_LOGIC_VECTOR ( 0 to 0 );
    s00_axis_tvalid : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of test_bd_pixelReOrder_0_0_rams_sp_rf_rst_6 : entity is "rams_sp_rf_rst";
end test_bd_pixelReOrder_0_0_rams_sp_rf_rst_6;

architecture STRUCTURE of test_bd_pixelReOrder_0_0_rams_sp_rf_rst_6 is
  signal \ram_reg_0_i_1__22_n_0\ : STD_LOGIC;
  signal NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_DBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_SBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_DOADO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 16 );
  signal NLW_ram_reg_0_DOBDO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_ram_reg_0_DOPADOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal NLW_ram_reg_0_DOPBDOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_ram_reg_0_ECCPARITY_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal NLW_ram_reg_0_RDADDRECC_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_ram_reg_1_DOADO_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 6 );
  signal NLW_ram_reg_1_DOBDO_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal NLW_ram_reg_1_DOPADOP_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_ram_reg_1_DOPBDOP_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ : string;
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of ram_reg_0 : label is "p2_d16";
  attribute METHODOLOGY_DRC_VIOS : string;
  attribute METHODOLOGY_DRC_VIOS of ram_reg_0 : label is "{SYNTH-6 {cell *THIS*}}";
  attribute RTL_RAM_BITS : integer;
  attribute RTL_RAM_BITS of ram_reg_0 : label is 46080;
  attribute RTL_RAM_NAME : string;
  attribute RTL_RAM_NAME of ram_reg_0 : label is "ram";
  attribute bram_addr_begin : integer;
  attribute bram_addr_begin of ram_reg_0 : label is 0;
  attribute bram_addr_end : integer;
  attribute bram_addr_end of ram_reg_0 : label is 2047;
  attribute bram_slice_begin : integer;
  attribute bram_slice_begin of ram_reg_0 : label is 0;
  attribute bram_slice_end : integer;
  attribute bram_slice_end of ram_reg_0 : label is 17;
  attribute ram_addr_begin : integer;
  attribute ram_addr_begin of ram_reg_0 : label is 0;
  attribute ram_addr_end : integer;
  attribute ram_addr_end of ram_reg_0 : label is 2047;
  attribute ram_offset : integer;
  attribute ram_offset of ram_reg_0 : label is 0;
  attribute ram_slice_begin : integer;
  attribute ram_slice_begin of ram_reg_0 : label is 0;
  attribute ram_slice_end : integer;
  attribute ram_slice_end of ram_reg_0 : label is 17;
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of ram_reg_1 : label is "p0_d6";
  attribute METHODOLOGY_DRC_VIOS of ram_reg_1 : label is "{SYNTH-6 {cell *THIS*}}";
  attribute RTL_RAM_BITS of ram_reg_1 : label is 46080;
  attribute RTL_RAM_NAME of ram_reg_1 : label is "ram";
  attribute bram_addr_begin of ram_reg_1 : label is 0;
  attribute bram_addr_end of ram_reg_1 : label is 2047;
  attribute bram_slice_begin of ram_reg_1 : label is 18;
  attribute bram_slice_end of ram_reg_1 : label is 23;
  attribute ram_addr_begin of ram_reg_1 : label is 0;
  attribute ram_addr_end of ram_reg_1 : label is 2047;
  attribute ram_offset of ram_reg_1 : label is 0;
  attribute ram_slice_begin of ram_reg_1 : label is 18;
  attribute ram_slice_end of ram_reg_1 : label is 23;
begin
ram_reg_0: unisim.vcomponents.RAMB36E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      EN_ECC_READ => false,
      EN_ECC_WRITE => false,
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      RAM_EXTENSION_A => "NONE",
      RAM_EXTENSION_B => "NONE",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "PERFORMANCE",
      READ_WIDTH_A => 18,
      READ_WIDTH_B => 0,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "READ_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 18,
      WRITE_WIDTH_B => 0
    )
        port map (
      ADDRARDADDR(15) => '1',
      ADDRARDADDR(14 downto 4) => addr(10 downto 0),
      ADDRARDADDR(3 downto 0) => B"1111",
      ADDRBWRADDR(15 downto 0) => B"1111111111111111",
      CASCADEINA => '1',
      CASCADEINB => '0',
      CASCADEOUTA => NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED,
      CASCADEOUTB => NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED,
      CLKARDCLK => m00_axis_aclk,
      CLKBWRCLK => '0',
      DBITERR => NLW_ram_reg_0_DBITERR_UNCONNECTED,
      DIADI(31 downto 16) => B"0000000000000000",
      DIADI(15 downto 0) => s00_axis_tdata(15 downto 0),
      DIBDI(31 downto 0) => B"11111111111111111111111111111111",
      DIPADIP(3 downto 2) => B"00",
      DIPADIP(1 downto 0) => s00_axis_tdata(17 downto 16),
      DIPBDIP(3 downto 0) => B"1111",
      DOADO(31 downto 16) => NLW_ram_reg_0_DOADO_UNCONNECTED(31 downto 16),
      DOADO(15 downto 0) => dout0_out(15 downto 0),
      DOBDO(31 downto 0) => NLW_ram_reg_0_DOBDO_UNCONNECTED(31 downto 0),
      DOPADOP(3 downto 2) => NLW_ram_reg_0_DOPADOP_UNCONNECTED(3 downto 2),
      DOPADOP(1 downto 0) => dout0_out(17 downto 16),
      DOPBDOP(3 downto 0) => NLW_ram_reg_0_DOPBDOP_UNCONNECTED(3 downto 0),
      ECCPARITY(7 downto 0) => NLW_ram_reg_0_ECCPARITY_UNCONNECTED(7 downto 0),
      ENARDEN => Q(0),
      ENBWREN => '0',
      INJECTDBITERR => NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED,
      INJECTSBITERR => NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED,
      RDADDRECC(8 downto 0) => NLW_ram_reg_0_RDADDRECC_UNCONNECTED(8 downto 0),
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      SBITERR => NLW_ram_reg_0_SBITERR_UNCONNECTED,
      WEA(3) => \ram_reg_0_i_1__22_n_0\,
      WEA(2) => \ram_reg_0_i_1__22_n_0\,
      WEA(1) => \ram_reg_0_i_1__22_n_0\,
      WEA(0) => \ram_reg_0_i_1__22_n_0\,
      WEBWE(7 downto 0) => B"00000000"
    );
\ram_reg_0_i_1__22\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => Q(0),
      I1 => s00_axis_tuser(0),
      I2 => s00_axis_tvalid,
      O => \ram_reg_0_i_1__22_n_0\
    );
ram_reg_1: unisim.vcomponents.RAMB18E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      INIT_A => X"00000",
      INIT_B => X"00000",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "PERFORMANCE",
      READ_WIDTH_A => 9,
      READ_WIDTH_B => 0,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"00000",
      SRVAL_B => X"00000",
      WRITE_MODE_A => "READ_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 9,
      WRITE_WIDTH_B => 0
    )
        port map (
      ADDRARDADDR(13 downto 3) => addr(10 downto 0),
      ADDRARDADDR(2 downto 0) => B"111",
      ADDRBWRADDR(13 downto 0) => B"11111111111111",
      CLKARDCLK => m00_axis_aclk,
      CLKBWRCLK => '0',
      DIADI(15 downto 6) => B"0000000000",
      DIADI(5 downto 0) => s00_axis_tdata(23 downto 18),
      DIBDI(15 downto 0) => B"1111111111111111",
      DIPADIP(1 downto 0) => B"00",
      DIPBDIP(1 downto 0) => B"11",
      DOADO(15 downto 6) => NLW_ram_reg_1_DOADO_UNCONNECTED(15 downto 6),
      DOADO(5 downto 0) => dout0_out(23 downto 18),
      DOBDO(15 downto 0) => NLW_ram_reg_1_DOBDO_UNCONNECTED(15 downto 0),
      DOPADOP(1 downto 0) => NLW_ram_reg_1_DOPADOP_UNCONNECTED(1 downto 0),
      DOPBDOP(1 downto 0) => NLW_ram_reg_1_DOPBDOP_UNCONNECTED(1 downto 0),
      ENARDEN => Q(0),
      ENBWREN => '0',
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      WEA(1) => \ram_reg_0_i_1__22_n_0\,
      WEA(0) => \ram_reg_0_i_1__22_n_0\,
      WEBWE(3 downto 0) => B"0000"
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity test_bd_pixelReOrder_0_0_rams_sp_rf_rst_7 is
  port (
    dout0_out : out STD_LOGIC_VECTOR ( 23 downto 0 );
    m00_axis_aclk : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 0 to 0 );
    addr : in STD_LOGIC_VECTOR ( 10 downto 0 );
    s00_axis_tdata : in STD_LOGIC_VECTOR ( 23 downto 0 );
    s00_axis_tuser : in STD_LOGIC_VECTOR ( 0 to 0 );
    s00_axis_tvalid : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of test_bd_pixelReOrder_0_0_rams_sp_rf_rst_7 : entity is "rams_sp_rf_rst";
end test_bd_pixelReOrder_0_0_rams_sp_rf_rst_7;

architecture STRUCTURE of test_bd_pixelReOrder_0_0_rams_sp_rf_rst_7 is
  signal \ram_reg_0_i_1__23_n_0\ : STD_LOGIC;
  signal NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_DBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_SBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_DOADO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 16 );
  signal NLW_ram_reg_0_DOBDO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_ram_reg_0_DOPADOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal NLW_ram_reg_0_DOPBDOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_ram_reg_0_ECCPARITY_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal NLW_ram_reg_0_RDADDRECC_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_ram_reg_1_DOADO_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 6 );
  signal NLW_ram_reg_1_DOBDO_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal NLW_ram_reg_1_DOPADOP_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_ram_reg_1_DOPBDOP_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ : string;
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of ram_reg_0 : label is "p2_d16";
  attribute METHODOLOGY_DRC_VIOS : string;
  attribute METHODOLOGY_DRC_VIOS of ram_reg_0 : label is "{SYNTH-6 {cell *THIS*}}";
  attribute RTL_RAM_BITS : integer;
  attribute RTL_RAM_BITS of ram_reg_0 : label is 46080;
  attribute RTL_RAM_NAME : string;
  attribute RTL_RAM_NAME of ram_reg_0 : label is "ram";
  attribute bram_addr_begin : integer;
  attribute bram_addr_begin of ram_reg_0 : label is 0;
  attribute bram_addr_end : integer;
  attribute bram_addr_end of ram_reg_0 : label is 2047;
  attribute bram_slice_begin : integer;
  attribute bram_slice_begin of ram_reg_0 : label is 0;
  attribute bram_slice_end : integer;
  attribute bram_slice_end of ram_reg_0 : label is 17;
  attribute ram_addr_begin : integer;
  attribute ram_addr_begin of ram_reg_0 : label is 0;
  attribute ram_addr_end : integer;
  attribute ram_addr_end of ram_reg_0 : label is 2047;
  attribute ram_offset : integer;
  attribute ram_offset of ram_reg_0 : label is 0;
  attribute ram_slice_begin : integer;
  attribute ram_slice_begin of ram_reg_0 : label is 0;
  attribute ram_slice_end : integer;
  attribute ram_slice_end of ram_reg_0 : label is 17;
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of ram_reg_1 : label is "p0_d6";
  attribute METHODOLOGY_DRC_VIOS of ram_reg_1 : label is "{SYNTH-6 {cell *THIS*}}";
  attribute RTL_RAM_BITS of ram_reg_1 : label is 46080;
  attribute RTL_RAM_NAME of ram_reg_1 : label is "ram";
  attribute bram_addr_begin of ram_reg_1 : label is 0;
  attribute bram_addr_end of ram_reg_1 : label is 2047;
  attribute bram_slice_begin of ram_reg_1 : label is 18;
  attribute bram_slice_end of ram_reg_1 : label is 23;
  attribute ram_addr_begin of ram_reg_1 : label is 0;
  attribute ram_addr_end of ram_reg_1 : label is 2047;
  attribute ram_offset of ram_reg_1 : label is 0;
  attribute ram_slice_begin of ram_reg_1 : label is 18;
  attribute ram_slice_end of ram_reg_1 : label is 23;
begin
ram_reg_0: unisim.vcomponents.RAMB36E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      EN_ECC_READ => false,
      EN_ECC_WRITE => false,
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      RAM_EXTENSION_A => "NONE",
      RAM_EXTENSION_B => "NONE",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "PERFORMANCE",
      READ_WIDTH_A => 18,
      READ_WIDTH_B => 0,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "READ_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 18,
      WRITE_WIDTH_B => 0
    )
        port map (
      ADDRARDADDR(15) => '1',
      ADDRARDADDR(14 downto 4) => addr(10 downto 0),
      ADDRARDADDR(3 downto 0) => B"1111",
      ADDRBWRADDR(15 downto 0) => B"1111111111111111",
      CASCADEINA => '1',
      CASCADEINB => '0',
      CASCADEOUTA => NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED,
      CASCADEOUTB => NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED,
      CLKARDCLK => m00_axis_aclk,
      CLKBWRCLK => '0',
      DBITERR => NLW_ram_reg_0_DBITERR_UNCONNECTED,
      DIADI(31 downto 16) => B"0000000000000000",
      DIADI(15 downto 0) => s00_axis_tdata(15 downto 0),
      DIBDI(31 downto 0) => B"11111111111111111111111111111111",
      DIPADIP(3 downto 2) => B"00",
      DIPADIP(1 downto 0) => s00_axis_tdata(17 downto 16),
      DIPBDIP(3 downto 0) => B"1111",
      DOADO(31 downto 16) => NLW_ram_reg_0_DOADO_UNCONNECTED(31 downto 16),
      DOADO(15 downto 0) => dout0_out(15 downto 0),
      DOBDO(31 downto 0) => NLW_ram_reg_0_DOBDO_UNCONNECTED(31 downto 0),
      DOPADOP(3 downto 2) => NLW_ram_reg_0_DOPADOP_UNCONNECTED(3 downto 2),
      DOPADOP(1 downto 0) => dout0_out(17 downto 16),
      DOPBDOP(3 downto 0) => NLW_ram_reg_0_DOPBDOP_UNCONNECTED(3 downto 0),
      ECCPARITY(7 downto 0) => NLW_ram_reg_0_ECCPARITY_UNCONNECTED(7 downto 0),
      ENARDEN => Q(0),
      ENBWREN => '0',
      INJECTDBITERR => NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED,
      INJECTSBITERR => NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED,
      RDADDRECC(8 downto 0) => NLW_ram_reg_0_RDADDRECC_UNCONNECTED(8 downto 0),
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      SBITERR => NLW_ram_reg_0_SBITERR_UNCONNECTED,
      WEA(3) => \ram_reg_0_i_1__23_n_0\,
      WEA(2) => \ram_reg_0_i_1__23_n_0\,
      WEA(1) => \ram_reg_0_i_1__23_n_0\,
      WEA(0) => \ram_reg_0_i_1__23_n_0\,
      WEBWE(7 downto 0) => B"00000000"
    );
\ram_reg_0_i_1__23\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => Q(0),
      I1 => s00_axis_tuser(0),
      I2 => s00_axis_tvalid,
      O => \ram_reg_0_i_1__23_n_0\
    );
ram_reg_1: unisim.vcomponents.RAMB18E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      INIT_A => X"00000",
      INIT_B => X"00000",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "PERFORMANCE",
      READ_WIDTH_A => 9,
      READ_WIDTH_B => 0,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"00000",
      SRVAL_B => X"00000",
      WRITE_MODE_A => "READ_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 9,
      WRITE_WIDTH_B => 0
    )
        port map (
      ADDRARDADDR(13 downto 3) => addr(10 downto 0),
      ADDRARDADDR(2 downto 0) => B"111",
      ADDRBWRADDR(13 downto 0) => B"11111111111111",
      CLKARDCLK => m00_axis_aclk,
      CLKBWRCLK => '0',
      DIADI(15 downto 6) => B"0000000000",
      DIADI(5 downto 0) => s00_axis_tdata(23 downto 18),
      DIBDI(15 downto 0) => B"1111111111111111",
      DIPADIP(1 downto 0) => B"00",
      DIPBDIP(1 downto 0) => B"11",
      DOADO(15 downto 6) => NLW_ram_reg_1_DOADO_UNCONNECTED(15 downto 6),
      DOADO(5 downto 0) => dout0_out(23 downto 18),
      DOBDO(15 downto 0) => NLW_ram_reg_1_DOBDO_UNCONNECTED(15 downto 0),
      DOPADOP(1 downto 0) => NLW_ram_reg_1_DOPADOP_UNCONNECTED(1 downto 0),
      DOPBDOP(1 downto 0) => NLW_ram_reg_1_DOPBDOP_UNCONNECTED(1 downto 0),
      ENARDEN => Q(0),
      ENBWREN => '0',
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      WEA(1) => \ram_reg_0_i_1__23_n_0\,
      WEA(0) => \ram_reg_0_i_1__23_n_0\,
      WEBWE(3 downto 0) => B"0000"
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity test_bd_pixelReOrder_0_0_rams_sp_rf_rst_8 is
  port (
    dout0_out : out STD_LOGIC_VECTOR ( 23 downto 0 );
    m00_axis_aclk : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 0 to 0 );
    addr : in STD_LOGIC_VECTOR ( 10 downto 0 );
    s00_axis_tdata : in STD_LOGIC_VECTOR ( 23 downto 0 );
    s00_axis_tuser : in STD_LOGIC_VECTOR ( 0 to 0 );
    s00_axis_tvalid : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of test_bd_pixelReOrder_0_0_rams_sp_rf_rst_8 : entity is "rams_sp_rf_rst";
end test_bd_pixelReOrder_0_0_rams_sp_rf_rst_8;

architecture STRUCTURE of test_bd_pixelReOrder_0_0_rams_sp_rf_rst_8 is
  signal \ram_reg_0_i_1__24_n_0\ : STD_LOGIC;
  signal NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_DBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_SBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_DOADO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 16 );
  signal NLW_ram_reg_0_DOBDO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_ram_reg_0_DOPADOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal NLW_ram_reg_0_DOPBDOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_ram_reg_0_ECCPARITY_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal NLW_ram_reg_0_RDADDRECC_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_ram_reg_1_DOADO_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 6 );
  signal NLW_ram_reg_1_DOBDO_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal NLW_ram_reg_1_DOPADOP_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_ram_reg_1_DOPBDOP_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ : string;
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of ram_reg_0 : label is "p2_d16";
  attribute METHODOLOGY_DRC_VIOS : string;
  attribute METHODOLOGY_DRC_VIOS of ram_reg_0 : label is "{SYNTH-6 {cell *THIS*}}";
  attribute RTL_RAM_BITS : integer;
  attribute RTL_RAM_BITS of ram_reg_0 : label is 46080;
  attribute RTL_RAM_NAME : string;
  attribute RTL_RAM_NAME of ram_reg_0 : label is "ram";
  attribute bram_addr_begin : integer;
  attribute bram_addr_begin of ram_reg_0 : label is 0;
  attribute bram_addr_end : integer;
  attribute bram_addr_end of ram_reg_0 : label is 2047;
  attribute bram_slice_begin : integer;
  attribute bram_slice_begin of ram_reg_0 : label is 0;
  attribute bram_slice_end : integer;
  attribute bram_slice_end of ram_reg_0 : label is 17;
  attribute ram_addr_begin : integer;
  attribute ram_addr_begin of ram_reg_0 : label is 0;
  attribute ram_addr_end : integer;
  attribute ram_addr_end of ram_reg_0 : label is 2047;
  attribute ram_offset : integer;
  attribute ram_offset of ram_reg_0 : label is 0;
  attribute ram_slice_begin : integer;
  attribute ram_slice_begin of ram_reg_0 : label is 0;
  attribute ram_slice_end : integer;
  attribute ram_slice_end of ram_reg_0 : label is 17;
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of ram_reg_1 : label is "p0_d6";
  attribute METHODOLOGY_DRC_VIOS of ram_reg_1 : label is "{SYNTH-6 {cell *THIS*}}";
  attribute RTL_RAM_BITS of ram_reg_1 : label is 46080;
  attribute RTL_RAM_NAME of ram_reg_1 : label is "ram";
  attribute bram_addr_begin of ram_reg_1 : label is 0;
  attribute bram_addr_end of ram_reg_1 : label is 2047;
  attribute bram_slice_begin of ram_reg_1 : label is 18;
  attribute bram_slice_end of ram_reg_1 : label is 23;
  attribute ram_addr_begin of ram_reg_1 : label is 0;
  attribute ram_addr_end of ram_reg_1 : label is 2047;
  attribute ram_offset of ram_reg_1 : label is 0;
  attribute ram_slice_begin of ram_reg_1 : label is 18;
  attribute ram_slice_end of ram_reg_1 : label is 23;
begin
ram_reg_0: unisim.vcomponents.RAMB36E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      EN_ECC_READ => false,
      EN_ECC_WRITE => false,
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      RAM_EXTENSION_A => "NONE",
      RAM_EXTENSION_B => "NONE",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "PERFORMANCE",
      READ_WIDTH_A => 18,
      READ_WIDTH_B => 0,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "READ_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 18,
      WRITE_WIDTH_B => 0
    )
        port map (
      ADDRARDADDR(15) => '1',
      ADDRARDADDR(14 downto 4) => addr(10 downto 0),
      ADDRARDADDR(3 downto 0) => B"1111",
      ADDRBWRADDR(15 downto 0) => B"1111111111111111",
      CASCADEINA => '1',
      CASCADEINB => '0',
      CASCADEOUTA => NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED,
      CASCADEOUTB => NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED,
      CLKARDCLK => m00_axis_aclk,
      CLKBWRCLK => '0',
      DBITERR => NLW_ram_reg_0_DBITERR_UNCONNECTED,
      DIADI(31 downto 16) => B"0000000000000000",
      DIADI(15 downto 0) => s00_axis_tdata(15 downto 0),
      DIBDI(31 downto 0) => B"11111111111111111111111111111111",
      DIPADIP(3 downto 2) => B"00",
      DIPADIP(1 downto 0) => s00_axis_tdata(17 downto 16),
      DIPBDIP(3 downto 0) => B"1111",
      DOADO(31 downto 16) => NLW_ram_reg_0_DOADO_UNCONNECTED(31 downto 16),
      DOADO(15 downto 0) => dout0_out(15 downto 0),
      DOBDO(31 downto 0) => NLW_ram_reg_0_DOBDO_UNCONNECTED(31 downto 0),
      DOPADOP(3 downto 2) => NLW_ram_reg_0_DOPADOP_UNCONNECTED(3 downto 2),
      DOPADOP(1 downto 0) => dout0_out(17 downto 16),
      DOPBDOP(3 downto 0) => NLW_ram_reg_0_DOPBDOP_UNCONNECTED(3 downto 0),
      ECCPARITY(7 downto 0) => NLW_ram_reg_0_ECCPARITY_UNCONNECTED(7 downto 0),
      ENARDEN => Q(0),
      ENBWREN => '0',
      INJECTDBITERR => NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED,
      INJECTSBITERR => NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED,
      RDADDRECC(8 downto 0) => NLW_ram_reg_0_RDADDRECC_UNCONNECTED(8 downto 0),
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      SBITERR => NLW_ram_reg_0_SBITERR_UNCONNECTED,
      WEA(3) => \ram_reg_0_i_1__24_n_0\,
      WEA(2) => \ram_reg_0_i_1__24_n_0\,
      WEA(1) => \ram_reg_0_i_1__24_n_0\,
      WEA(0) => \ram_reg_0_i_1__24_n_0\,
      WEBWE(7 downto 0) => B"00000000"
    );
\ram_reg_0_i_1__24\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => Q(0),
      I1 => s00_axis_tuser(0),
      I2 => s00_axis_tvalid,
      O => \ram_reg_0_i_1__24_n_0\
    );
ram_reg_1: unisim.vcomponents.RAMB18E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      INIT_A => X"00000",
      INIT_B => X"00000",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "PERFORMANCE",
      READ_WIDTH_A => 9,
      READ_WIDTH_B => 0,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"00000",
      SRVAL_B => X"00000",
      WRITE_MODE_A => "READ_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 9,
      WRITE_WIDTH_B => 0
    )
        port map (
      ADDRARDADDR(13 downto 3) => addr(10 downto 0),
      ADDRARDADDR(2 downto 0) => B"111",
      ADDRBWRADDR(13 downto 0) => B"11111111111111",
      CLKARDCLK => m00_axis_aclk,
      CLKBWRCLK => '0',
      DIADI(15 downto 6) => B"0000000000",
      DIADI(5 downto 0) => s00_axis_tdata(23 downto 18),
      DIBDI(15 downto 0) => B"1111111111111111",
      DIPADIP(1 downto 0) => B"00",
      DIPBDIP(1 downto 0) => B"11",
      DOADO(15 downto 6) => NLW_ram_reg_1_DOADO_UNCONNECTED(15 downto 6),
      DOADO(5 downto 0) => dout0_out(23 downto 18),
      DOBDO(15 downto 0) => NLW_ram_reg_1_DOBDO_UNCONNECTED(15 downto 0),
      DOPADOP(1 downto 0) => NLW_ram_reg_1_DOPADOP_UNCONNECTED(1 downto 0),
      DOPBDOP(1 downto 0) => NLW_ram_reg_1_DOPBDOP_UNCONNECTED(1 downto 0),
      ENARDEN => Q(0),
      ENBWREN => '0',
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      WEA(1) => \ram_reg_0_i_1__24_n_0\,
      WEA(0) => \ram_reg_0_i_1__24_n_0\,
      WEBWE(3 downto 0) => B"0000"
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity test_bd_pixelReOrder_0_0_rams_sp_rf_rst_9 is
  port (
    dout0_out : out STD_LOGIC_VECTOR ( 23 downto 0 );
    m00_axis_aclk : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 0 to 0 );
    addr : in STD_LOGIC_VECTOR ( 10 downto 0 );
    s00_axis_tdata : in STD_LOGIC_VECTOR ( 23 downto 0 );
    s00_axis_tuser : in STD_LOGIC_VECTOR ( 0 to 0 );
    s00_axis_tvalid : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of test_bd_pixelReOrder_0_0_rams_sp_rf_rst_9 : entity is "rams_sp_rf_rst";
end test_bd_pixelReOrder_0_0_rams_sp_rf_rst_9;

architecture STRUCTURE of test_bd_pixelReOrder_0_0_rams_sp_rf_rst_9 is
  signal \ram_reg_0_i_1__25_n_0\ : STD_LOGIC;
  signal NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_DBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_SBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_ram_reg_0_DOADO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 16 );
  signal NLW_ram_reg_0_DOBDO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_ram_reg_0_DOPADOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal NLW_ram_reg_0_DOPBDOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_ram_reg_0_ECCPARITY_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal NLW_ram_reg_0_RDADDRECC_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_ram_reg_1_DOADO_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 6 );
  signal NLW_ram_reg_1_DOBDO_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal NLW_ram_reg_1_DOPADOP_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_ram_reg_1_DOPBDOP_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ : string;
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of ram_reg_0 : label is "p2_d16";
  attribute METHODOLOGY_DRC_VIOS : string;
  attribute METHODOLOGY_DRC_VIOS of ram_reg_0 : label is "{SYNTH-6 {cell *THIS*}}";
  attribute RTL_RAM_BITS : integer;
  attribute RTL_RAM_BITS of ram_reg_0 : label is 46080;
  attribute RTL_RAM_NAME : string;
  attribute RTL_RAM_NAME of ram_reg_0 : label is "ram";
  attribute bram_addr_begin : integer;
  attribute bram_addr_begin of ram_reg_0 : label is 0;
  attribute bram_addr_end : integer;
  attribute bram_addr_end of ram_reg_0 : label is 2047;
  attribute bram_slice_begin : integer;
  attribute bram_slice_begin of ram_reg_0 : label is 0;
  attribute bram_slice_end : integer;
  attribute bram_slice_end of ram_reg_0 : label is 17;
  attribute ram_addr_begin : integer;
  attribute ram_addr_begin of ram_reg_0 : label is 0;
  attribute ram_addr_end : integer;
  attribute ram_addr_end of ram_reg_0 : label is 2047;
  attribute ram_offset : integer;
  attribute ram_offset of ram_reg_0 : label is 0;
  attribute ram_slice_begin : integer;
  attribute ram_slice_begin of ram_reg_0 : label is 0;
  attribute ram_slice_end : integer;
  attribute ram_slice_end of ram_reg_0 : label is 17;
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of ram_reg_1 : label is "p0_d6";
  attribute METHODOLOGY_DRC_VIOS of ram_reg_1 : label is "{SYNTH-6 {cell *THIS*}}";
  attribute RTL_RAM_BITS of ram_reg_1 : label is 46080;
  attribute RTL_RAM_NAME of ram_reg_1 : label is "ram";
  attribute bram_addr_begin of ram_reg_1 : label is 0;
  attribute bram_addr_end of ram_reg_1 : label is 2047;
  attribute bram_slice_begin of ram_reg_1 : label is 18;
  attribute bram_slice_end of ram_reg_1 : label is 23;
  attribute ram_addr_begin of ram_reg_1 : label is 0;
  attribute ram_addr_end of ram_reg_1 : label is 2047;
  attribute ram_offset of ram_reg_1 : label is 0;
  attribute ram_slice_begin of ram_reg_1 : label is 18;
  attribute ram_slice_end of ram_reg_1 : label is 23;
begin
ram_reg_0: unisim.vcomponents.RAMB36E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      EN_ECC_READ => false,
      EN_ECC_WRITE => false,
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      RAM_EXTENSION_A => "NONE",
      RAM_EXTENSION_B => "NONE",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "PERFORMANCE",
      READ_WIDTH_A => 18,
      READ_WIDTH_B => 0,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "READ_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 18,
      WRITE_WIDTH_B => 0
    )
        port map (
      ADDRARDADDR(15) => '1',
      ADDRARDADDR(14 downto 4) => addr(10 downto 0),
      ADDRARDADDR(3 downto 0) => B"1111",
      ADDRBWRADDR(15 downto 0) => B"1111111111111111",
      CASCADEINA => '1',
      CASCADEINB => '0',
      CASCADEOUTA => NLW_ram_reg_0_CASCADEOUTA_UNCONNECTED,
      CASCADEOUTB => NLW_ram_reg_0_CASCADEOUTB_UNCONNECTED,
      CLKARDCLK => m00_axis_aclk,
      CLKBWRCLK => '0',
      DBITERR => NLW_ram_reg_0_DBITERR_UNCONNECTED,
      DIADI(31 downto 16) => B"0000000000000000",
      DIADI(15 downto 0) => s00_axis_tdata(15 downto 0),
      DIBDI(31 downto 0) => B"11111111111111111111111111111111",
      DIPADIP(3 downto 2) => B"00",
      DIPADIP(1 downto 0) => s00_axis_tdata(17 downto 16),
      DIPBDIP(3 downto 0) => B"1111",
      DOADO(31 downto 16) => NLW_ram_reg_0_DOADO_UNCONNECTED(31 downto 16),
      DOADO(15 downto 0) => dout0_out(15 downto 0),
      DOBDO(31 downto 0) => NLW_ram_reg_0_DOBDO_UNCONNECTED(31 downto 0),
      DOPADOP(3 downto 2) => NLW_ram_reg_0_DOPADOP_UNCONNECTED(3 downto 2),
      DOPADOP(1 downto 0) => dout0_out(17 downto 16),
      DOPBDOP(3 downto 0) => NLW_ram_reg_0_DOPBDOP_UNCONNECTED(3 downto 0),
      ECCPARITY(7 downto 0) => NLW_ram_reg_0_ECCPARITY_UNCONNECTED(7 downto 0),
      ENARDEN => Q(0),
      ENBWREN => '0',
      INJECTDBITERR => NLW_ram_reg_0_INJECTDBITERR_UNCONNECTED,
      INJECTSBITERR => NLW_ram_reg_0_INJECTSBITERR_UNCONNECTED,
      RDADDRECC(8 downto 0) => NLW_ram_reg_0_RDADDRECC_UNCONNECTED(8 downto 0),
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      SBITERR => NLW_ram_reg_0_SBITERR_UNCONNECTED,
      WEA(3) => \ram_reg_0_i_1__25_n_0\,
      WEA(2) => \ram_reg_0_i_1__25_n_0\,
      WEA(1) => \ram_reg_0_i_1__25_n_0\,
      WEA(0) => \ram_reg_0_i_1__25_n_0\,
      WEBWE(7 downto 0) => B"00000000"
    );
\ram_reg_0_i_1__25\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => Q(0),
      I1 => s00_axis_tuser(0),
      I2 => s00_axis_tvalid,
      O => \ram_reg_0_i_1__25_n_0\
    );
ram_reg_1: unisim.vcomponents.RAMB18E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      INIT_A => X"00000",
      INIT_B => X"00000",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "PERFORMANCE",
      READ_WIDTH_A => 9,
      READ_WIDTH_B => 0,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"00000",
      SRVAL_B => X"00000",
      WRITE_MODE_A => "READ_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 9,
      WRITE_WIDTH_B => 0
    )
        port map (
      ADDRARDADDR(13 downto 3) => addr(10 downto 0),
      ADDRARDADDR(2 downto 0) => B"111",
      ADDRBWRADDR(13 downto 0) => B"11111111111111",
      CLKARDCLK => m00_axis_aclk,
      CLKBWRCLK => '0',
      DIADI(15 downto 6) => B"0000000000",
      DIADI(5 downto 0) => s00_axis_tdata(23 downto 18),
      DIBDI(15 downto 0) => B"1111111111111111",
      DIPADIP(1 downto 0) => B"00",
      DIPBDIP(1 downto 0) => B"11",
      DOADO(15 downto 6) => NLW_ram_reg_1_DOADO_UNCONNECTED(15 downto 6),
      DOADO(5 downto 0) => dout0_out(23 downto 18),
      DOBDO(15 downto 0) => NLW_ram_reg_1_DOBDO_UNCONNECTED(15 downto 0),
      DOPADOP(1 downto 0) => NLW_ram_reg_1_DOPADOP_UNCONNECTED(1 downto 0),
      DOPBDOP(1 downto 0) => NLW_ram_reg_1_DOPBDOP_UNCONNECTED(1 downto 0),
      ENARDEN => Q(0),
      ENBWREN => '0',
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      WEA(1) => \ram_reg_0_i_1__25_n_0\,
      WEA(0) => \ram_reg_0_i_1__25_n_0\,
      WEBWE(3 downto 0) => B"0000"
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity test_bd_pixelReOrder_0_0_pixelReOrder_v1_0 is
  port (
    m00_axis_tlast : out STD_LOGIC;
    m00_axis_tvalid : out STD_LOGIC;
    m00_axis_tdata : out STD_LOGIC_VECTOR ( 23 downto 0 );
    s00_axis_tuser : in STD_LOGIC_VECTOR ( 14 downto 0 );
    m00_axis_aclk : in STD_LOGIC;
    s00_axis_tdata : in STD_LOGIC_VECTOR ( 23 downto 0 );
    s00_axis_tvalid : in STD_LOGIC;
    s00_axis_aresetn : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of test_bd_pixelReOrder_0_0_pixelReOrder_v1_0 : entity is "pixelReOrder_v1_0";
end test_bd_pixelReOrder_0_0_pixelReOrder_v1_0;

architecture STRUCTURE of test_bd_pixelReOrder_0_0_pixelReOrder_v1_0 is
  signal ADDR2 : STD_LOGIC_VECTOR ( 10 downto 0 );
  signal \EN1[0]_i_1_n_0\ : STD_LOGIC;
  signal \EN1[0]_i_2_n_0\ : STD_LOGIC;
  signal \EN1[10]_i_1_n_0\ : STD_LOGIC;
  signal \EN1[11]_i_1_n_0\ : STD_LOGIC;
  signal \EN1[12]_i_1_n_0\ : STD_LOGIC;
  signal \EN1[13]_i_1_n_0\ : STD_LOGIC;
  signal \EN1[14]_i_1_n_0\ : STD_LOGIC;
  signal \EN1[14]_i_2_n_0\ : STD_LOGIC;
  signal \EN1[15]_i_1_n_0\ : STD_LOGIC;
  signal \EN1[15]_i_2_n_0\ : STD_LOGIC;
  signal \EN1[15]_i_3_n_0\ : STD_LOGIC;
  signal \EN1[15]_i_4_n_0\ : STD_LOGIC;
  signal \EN1[15]_i_5_n_0\ : STD_LOGIC;
  signal \EN1[15]_i_6_n_0\ : STD_LOGIC;
  signal \EN1[15]_i_7_n_0\ : STD_LOGIC;
  signal \EN1[1]_i_1_n_0\ : STD_LOGIC;
  signal \EN1[2]_i_1_n_0\ : STD_LOGIC;
  signal \EN1[3]_i_1_n_0\ : STD_LOGIC;
  signal \EN1[3]_i_2_n_0\ : STD_LOGIC;
  signal \EN1[4]_i_1_n_0\ : STD_LOGIC;
  signal \EN1[4]_i_2_n_0\ : STD_LOGIC;
  signal \EN1[5]_i_1_n_0\ : STD_LOGIC;
  signal \EN1[6]_i_1_n_0\ : STD_LOGIC;
  signal \EN1[6]_i_2_n_0\ : STD_LOGIC;
  signal \EN1[7]_i_1_n_0\ : STD_LOGIC;
  signal \EN1[7]_i_2_n_0\ : STD_LOGIC;
  signal \EN1[7]_i_3_n_0\ : STD_LOGIC;
  signal \EN1[8]_i_1_n_0\ : STD_LOGIC;
  signal \EN1[8]_i_2_n_0\ : STD_LOGIC;
  signal \EN1[9]_i_1_n_0\ : STD_LOGIC;
  signal \EN1_reg_n_0_[0]\ : STD_LOGIC;
  signal \EN1_reg_n_0_[10]\ : STD_LOGIC;
  signal \EN1_reg_n_0_[11]\ : STD_LOGIC;
  signal \EN1_reg_n_0_[12]\ : STD_LOGIC;
  signal \EN1_reg_n_0_[13]\ : STD_LOGIC;
  signal \EN1_reg_n_0_[14]\ : STD_LOGIC;
  signal \EN1_reg_n_0_[15]\ : STD_LOGIC;
  signal \EN1_reg_n_0_[1]\ : STD_LOGIC;
  signal \EN1_reg_n_0_[2]\ : STD_LOGIC;
  signal \EN1_reg_n_0_[3]\ : STD_LOGIC;
  signal \EN1_reg_n_0_[4]\ : STD_LOGIC;
  signal \EN1_reg_n_0_[5]\ : STD_LOGIC;
  signal \EN1_reg_n_0_[6]\ : STD_LOGIC;
  signal \EN1_reg_n_0_[7]\ : STD_LOGIC;
  signal \EN1_reg_n_0_[8]\ : STD_LOGIC;
  signal \EN1_reg_n_0_[9]\ : STD_LOGIC;
  signal \EN2[0]_i_1_n_0\ : STD_LOGIC;
  signal \EN2[0]_i_2_n_0\ : STD_LOGIC;
  signal \EN2[0]_i_3_n_0\ : STD_LOGIC;
  signal \EN2[10]_i_1_n_0\ : STD_LOGIC;
  signal \EN2[11]_i_1_n_0\ : STD_LOGIC;
  signal \EN2[12]_i_1_n_0\ : STD_LOGIC;
  signal \EN2[12]_i_2_n_0\ : STD_LOGIC;
  signal \EN2[12]_i_3_n_0\ : STD_LOGIC;
  signal \EN2[13]_i_1_n_0\ : STD_LOGIC;
  signal \EN2[13]_i_2_n_0\ : STD_LOGIC;
  signal \EN2[14]_i_1_n_0\ : STD_LOGIC;
  signal \EN2[14]_i_2_n_0\ : STD_LOGIC;
  signal \EN2[14]_i_3_n_0\ : STD_LOGIC;
  signal \EN2[15]_i_10_n_0\ : STD_LOGIC;
  signal \EN2[15]_i_1_n_0\ : STD_LOGIC;
  signal \EN2[15]_i_2_n_0\ : STD_LOGIC;
  signal \EN2[15]_i_3_n_0\ : STD_LOGIC;
  signal \EN2[15]_i_4_n_0\ : STD_LOGIC;
  signal \EN2[15]_i_5_n_0\ : STD_LOGIC;
  signal \EN2[15]_i_6_n_0\ : STD_LOGIC;
  signal \EN2[15]_i_7_n_0\ : STD_LOGIC;
  signal \EN2[15]_i_8_n_0\ : STD_LOGIC;
  signal \EN2[15]_i_9_n_0\ : STD_LOGIC;
  signal \EN2[1]_i_1_n_0\ : STD_LOGIC;
  signal \EN2[2]_i_1_n_0\ : STD_LOGIC;
  signal \EN2[3]_i_1_n_0\ : STD_LOGIC;
  signal \EN2[3]_i_2_n_0\ : STD_LOGIC;
  signal \EN2[4]_i_1_n_0\ : STD_LOGIC;
  signal \EN2[4]_i_2_n_0\ : STD_LOGIC;
  signal \EN2[5]_i_1_n_0\ : STD_LOGIC;
  signal \EN2[6]_i_1_n_0\ : STD_LOGIC;
  signal \EN2[6]_i_2_n_0\ : STD_LOGIC;
  signal \EN2[7]_i_1_n_0\ : STD_LOGIC;
  signal \EN2[7]_i_2_n_0\ : STD_LOGIC;
  signal \EN2[7]_i_3_n_0\ : STD_LOGIC;
  signal \EN2[8]_i_1_n_0\ : STD_LOGIC;
  signal \EN2[8]_i_2_n_0\ : STD_LOGIC;
  signal \EN2[9]_i_1_n_0\ : STD_LOGIC;
  signal \EN2_reg_n_0_[0]\ : STD_LOGIC;
  signal \EN2_reg_n_0_[10]\ : STD_LOGIC;
  signal \EN2_reg_n_0_[11]\ : STD_LOGIC;
  signal \EN2_reg_n_0_[12]\ : STD_LOGIC;
  signal \EN2_reg_n_0_[13]\ : STD_LOGIC;
  signal \EN2_reg_n_0_[14]\ : STD_LOGIC;
  signal \EN2_reg_n_0_[15]\ : STD_LOGIC;
  signal \EN2_reg_n_0_[1]\ : STD_LOGIC;
  signal \EN2_reg_n_0_[2]\ : STD_LOGIC;
  signal \EN2_reg_n_0_[3]\ : STD_LOGIC;
  signal \EN2_reg_n_0_[4]\ : STD_LOGIC;
  signal \EN2_reg_n_0_[5]\ : STD_LOGIC;
  signal \EN2_reg_n_0_[6]\ : STD_LOGIC;
  signal \EN2_reg_n_0_[7]\ : STD_LOGIC;
  signal \EN2_reg_n_0_[8]\ : STD_LOGIC;
  signal \EN2_reg_n_0_[9]\ : STD_LOGIC;
  signal \_DOUT_1[0]_0\ : STD_LOGIC_VECTOR ( 23 downto 0 );
  signal \_DOUT_1[10]_20\ : STD_LOGIC_VECTOR ( 23 downto 0 );
  signal \_DOUT_1[12]_24\ : STD_LOGIC_VECTOR ( 23 downto 0 );
  signal \_DOUT_1[13]_26\ : STD_LOGIC_VECTOR ( 23 downto 0 );
  signal \_DOUT_1[14]_28\ : STD_LOGIC_VECTOR ( 23 downto 0 );
  signal \_DOUT_1[1]_2\ : STD_LOGIC_VECTOR ( 23 downto 0 );
  signal \_DOUT_1[2]_4\ : STD_LOGIC_VECTOR ( 23 downto 0 );
  signal \_DOUT_1[4]_8\ : STD_LOGIC_VECTOR ( 23 downto 0 );
  signal \_DOUT_1[5]_10\ : STD_LOGIC_VECTOR ( 23 downto 0 );
  signal \_DOUT_1[6]_12\ : STD_LOGIC_VECTOR ( 23 downto 0 );
  signal \_DOUT_1[8]_16\ : STD_LOGIC_VECTOR ( 23 downto 0 );
  signal \_DOUT_1[9]_18\ : STD_LOGIC_VECTOR ( 23 downto 0 );
  signal \_DOUT_2[0]_1\ : STD_LOGIC_VECTOR ( 23 downto 0 );
  signal \_DOUT_2[10]_21\ : STD_LOGIC_VECTOR ( 23 downto 0 );
  signal \_DOUT_2[12]_25\ : STD_LOGIC_VECTOR ( 23 downto 0 );
  signal \_DOUT_2[13]_27\ : STD_LOGIC_VECTOR ( 23 downto 0 );
  signal \_DOUT_2[14]_29\ : STD_LOGIC_VECTOR ( 23 downto 0 );
  signal \_DOUT_2[1]_3\ : STD_LOGIC_VECTOR ( 23 downto 0 );
  signal \_DOUT_2[2]_5\ : STD_LOGIC_VECTOR ( 23 downto 0 );
  signal \_DOUT_2[4]_9\ : STD_LOGIC_VECTOR ( 23 downto 0 );
  signal \_DOUT_2[5]_11\ : STD_LOGIC_VECTOR ( 23 downto 0 );
  signal \_DOUT_2[6]_13\ : STD_LOGIC_VECTOR ( 23 downto 0 );
  signal \_DOUT_2[8]_17\ : STD_LOGIC_VECTOR ( 23 downto 0 );
  signal \_DOUT_2[9]_19\ : STD_LOGIC_VECTOR ( 23 downto 0 );
  signal \active_mem1_in_read_state[0]_i_1_n_0\ : STD_LOGIC;
  signal \active_mem1_in_read_state[10]_i_1_n_0\ : STD_LOGIC;
  signal \active_mem1_in_read_state[11]_i_1_n_0\ : STD_LOGIC;
  signal \active_mem1_in_read_state[12]_i_1_n_0\ : STD_LOGIC;
  signal \active_mem1_in_read_state[13]_i_1_n_0\ : STD_LOGIC;
  signal \active_mem1_in_read_state[14]_i_1_n_0\ : STD_LOGIC;
  signal \active_mem1_in_read_state[15]_i_1_n_0\ : STD_LOGIC;
  signal \active_mem1_in_read_state[15]_i_2_n_0\ : STD_LOGIC;
  signal \active_mem1_in_read_state[1]_i_1_n_0\ : STD_LOGIC;
  signal \active_mem1_in_read_state[2]_i_1_n_0\ : STD_LOGIC;
  signal \active_mem1_in_read_state[3]_i_1_n_0\ : STD_LOGIC;
  signal \active_mem1_in_read_state[4]_i_1_n_0\ : STD_LOGIC;
  signal \active_mem1_in_read_state[5]_i_1_n_0\ : STD_LOGIC;
  signal \active_mem1_in_read_state[6]_i_1_n_0\ : STD_LOGIC;
  signal \active_mem1_in_read_state[7]_i_1_n_0\ : STD_LOGIC;
  signal \active_mem1_in_read_state[8]_i_1_n_0\ : STD_LOGIC;
  signal \active_mem1_in_read_state[9]_i_1_n_0\ : STD_LOGIC;
  signal \active_mem1_in_read_state_reg[12]_i_2_n_0\ : STD_LOGIC;
  signal \active_mem1_in_read_state_reg[12]_i_2_n_1\ : STD_LOGIC;
  signal \active_mem1_in_read_state_reg[12]_i_2_n_2\ : STD_LOGIC;
  signal \active_mem1_in_read_state_reg[12]_i_2_n_3\ : STD_LOGIC;
  signal \active_mem1_in_read_state_reg[12]_i_2_n_4\ : STD_LOGIC;
  signal \active_mem1_in_read_state_reg[12]_i_2_n_5\ : STD_LOGIC;
  signal \active_mem1_in_read_state_reg[12]_i_2_n_6\ : STD_LOGIC;
  signal \active_mem1_in_read_state_reg[12]_i_2_n_7\ : STD_LOGIC;
  signal \active_mem1_in_read_state_reg[15]_i_3_n_2\ : STD_LOGIC;
  signal \active_mem1_in_read_state_reg[15]_i_3_n_3\ : STD_LOGIC;
  signal \active_mem1_in_read_state_reg[15]_i_3_n_5\ : STD_LOGIC;
  signal \active_mem1_in_read_state_reg[15]_i_3_n_6\ : STD_LOGIC;
  signal \active_mem1_in_read_state_reg[15]_i_3_n_7\ : STD_LOGIC;
  signal \active_mem1_in_read_state_reg[4]_i_2_n_0\ : STD_LOGIC;
  signal \active_mem1_in_read_state_reg[4]_i_2_n_1\ : STD_LOGIC;
  signal \active_mem1_in_read_state_reg[4]_i_2_n_2\ : STD_LOGIC;
  signal \active_mem1_in_read_state_reg[4]_i_2_n_3\ : STD_LOGIC;
  signal \active_mem1_in_read_state_reg[4]_i_2_n_4\ : STD_LOGIC;
  signal \active_mem1_in_read_state_reg[4]_i_2_n_5\ : STD_LOGIC;
  signal \active_mem1_in_read_state_reg[4]_i_2_n_6\ : STD_LOGIC;
  signal \active_mem1_in_read_state_reg[4]_i_2_n_7\ : STD_LOGIC;
  signal \active_mem1_in_read_state_reg[8]_i_2_n_0\ : STD_LOGIC;
  signal \active_mem1_in_read_state_reg[8]_i_2_n_1\ : STD_LOGIC;
  signal \active_mem1_in_read_state_reg[8]_i_2_n_2\ : STD_LOGIC;
  signal \active_mem1_in_read_state_reg[8]_i_2_n_3\ : STD_LOGIC;
  signal \active_mem1_in_read_state_reg[8]_i_2_n_4\ : STD_LOGIC;
  signal \active_mem1_in_read_state_reg[8]_i_2_n_5\ : STD_LOGIC;
  signal \active_mem1_in_read_state_reg[8]_i_2_n_6\ : STD_LOGIC;
  signal \active_mem1_in_read_state_reg[8]_i_2_n_7\ : STD_LOGIC;
  signal \active_mem1_in_read_state_reg_n_0_[0]\ : STD_LOGIC;
  signal \active_mem1_in_read_state_reg_n_0_[10]\ : STD_LOGIC;
  signal \active_mem1_in_read_state_reg_n_0_[11]\ : STD_LOGIC;
  signal \active_mem1_in_read_state_reg_n_0_[12]\ : STD_LOGIC;
  signal \active_mem1_in_read_state_reg_n_0_[13]\ : STD_LOGIC;
  signal \active_mem1_in_read_state_reg_n_0_[14]\ : STD_LOGIC;
  signal \active_mem1_in_read_state_reg_n_0_[15]\ : STD_LOGIC;
  signal \active_mem1_in_read_state_reg_n_0_[1]\ : STD_LOGIC;
  signal \active_mem1_in_read_state_reg_n_0_[2]\ : STD_LOGIC;
  signal \active_mem1_in_read_state_reg_n_0_[3]\ : STD_LOGIC;
  signal \active_mem1_in_read_state_reg_n_0_[4]\ : STD_LOGIC;
  signal \active_mem1_in_read_state_reg_n_0_[5]\ : STD_LOGIC;
  signal \active_mem1_in_read_state_reg_n_0_[6]\ : STD_LOGIC;
  signal \active_mem1_in_read_state_reg_n_0_[7]\ : STD_LOGIC;
  signal \active_mem1_in_read_state_reg_n_0_[8]\ : STD_LOGIC;
  signal \active_mem1_in_read_state_reg_n_0_[9]\ : STD_LOGIC;
  signal \active_mem2_in_read_state[0]_i_1_n_0\ : STD_LOGIC;
  signal \active_mem2_in_read_state[0]_i_3_n_0\ : STD_LOGIC;
  signal active_mem2_in_read_state_reg : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal \active_mem2_in_read_state_reg[0]_i_2_n_0\ : STD_LOGIC;
  signal \active_mem2_in_read_state_reg[0]_i_2_n_1\ : STD_LOGIC;
  signal \active_mem2_in_read_state_reg[0]_i_2_n_2\ : STD_LOGIC;
  signal \active_mem2_in_read_state_reg[0]_i_2_n_3\ : STD_LOGIC;
  signal \active_mem2_in_read_state_reg[0]_i_2_n_4\ : STD_LOGIC;
  signal \active_mem2_in_read_state_reg[0]_i_2_n_5\ : STD_LOGIC;
  signal \active_mem2_in_read_state_reg[0]_i_2_n_6\ : STD_LOGIC;
  signal \active_mem2_in_read_state_reg[0]_i_2_n_7\ : STD_LOGIC;
  signal \active_mem2_in_read_state_reg[12]_i_1_n_1\ : STD_LOGIC;
  signal \active_mem2_in_read_state_reg[12]_i_1_n_2\ : STD_LOGIC;
  signal \active_mem2_in_read_state_reg[12]_i_1_n_3\ : STD_LOGIC;
  signal \active_mem2_in_read_state_reg[12]_i_1_n_4\ : STD_LOGIC;
  signal \active_mem2_in_read_state_reg[12]_i_1_n_5\ : STD_LOGIC;
  signal \active_mem2_in_read_state_reg[12]_i_1_n_6\ : STD_LOGIC;
  signal \active_mem2_in_read_state_reg[12]_i_1_n_7\ : STD_LOGIC;
  signal \active_mem2_in_read_state_reg[4]_i_1_n_0\ : STD_LOGIC;
  signal \active_mem2_in_read_state_reg[4]_i_1_n_1\ : STD_LOGIC;
  signal \active_mem2_in_read_state_reg[4]_i_1_n_2\ : STD_LOGIC;
  signal \active_mem2_in_read_state_reg[4]_i_1_n_3\ : STD_LOGIC;
  signal \active_mem2_in_read_state_reg[4]_i_1_n_4\ : STD_LOGIC;
  signal \active_mem2_in_read_state_reg[4]_i_1_n_5\ : STD_LOGIC;
  signal \active_mem2_in_read_state_reg[4]_i_1_n_6\ : STD_LOGIC;
  signal \active_mem2_in_read_state_reg[4]_i_1_n_7\ : STD_LOGIC;
  signal \active_mem2_in_read_state_reg[8]_i_1_n_0\ : STD_LOGIC;
  signal \active_mem2_in_read_state_reg[8]_i_1_n_1\ : STD_LOGIC;
  signal \active_mem2_in_read_state_reg[8]_i_1_n_2\ : STD_LOGIC;
  signal \active_mem2_in_read_state_reg[8]_i_1_n_3\ : STD_LOGIC;
  signal \active_mem2_in_read_state_reg[8]_i_1_n_4\ : STD_LOGIC;
  signal \active_mem2_in_read_state_reg[8]_i_1_n_5\ : STD_LOGIC;
  signal \active_mem2_in_read_state_reg[8]_i_1_n_6\ : STD_LOGIC;
  signal \active_mem2_in_read_state_reg[8]_i_1_n_7\ : STD_LOGIC;
  signal \adr_read_reorder_data_mem1[0]_i_1_n_0\ : STD_LOGIC;
  signal \adr_read_reorder_data_mem1[10]_i_1_n_0\ : STD_LOGIC;
  signal \adr_read_reorder_data_mem1[10]_i_2_n_0\ : STD_LOGIC;
  signal \adr_read_reorder_data_mem1[10]_i_3_n_0\ : STD_LOGIC;
  signal \adr_read_reorder_data_mem1[10]_i_4_n_0\ : STD_LOGIC;
  signal \adr_read_reorder_data_mem1[1]_i_1_n_0\ : STD_LOGIC;
  signal \adr_read_reorder_data_mem1[2]_i_1_n_0\ : STD_LOGIC;
  signal \adr_read_reorder_data_mem1[3]_i_1_n_0\ : STD_LOGIC;
  signal \adr_read_reorder_data_mem1[3]_i_2_n_0\ : STD_LOGIC;
  signal \adr_read_reorder_data_mem1[4]_i_1_n_0\ : STD_LOGIC;
  signal \adr_read_reorder_data_mem1[4]_i_2_n_0\ : STD_LOGIC;
  signal \adr_read_reorder_data_mem1[5]_i_1_n_0\ : STD_LOGIC;
  signal \adr_read_reorder_data_mem1[5]_i_2_n_0\ : STD_LOGIC;
  signal \adr_read_reorder_data_mem1[5]_i_3_n_0\ : STD_LOGIC;
  signal \adr_read_reorder_data_mem1[6]_i_1_n_0\ : STD_LOGIC;
  signal \adr_read_reorder_data_mem1[6]_i_2_n_0\ : STD_LOGIC;
  signal \adr_read_reorder_data_mem1[6]_i_3_n_0\ : STD_LOGIC;
  signal \adr_read_reorder_data_mem1[6]_i_4_n_0\ : STD_LOGIC;
  signal \adr_read_reorder_data_mem1[7]_i_1_n_0\ : STD_LOGIC;
  signal \adr_read_reorder_data_mem1[8]_i_1_n_0\ : STD_LOGIC;
  signal \adr_read_reorder_data_mem1[9]_i_1_n_0\ : STD_LOGIC;
  signal \adr_read_reorder_data_mem1_reg_n_0_[0]\ : STD_LOGIC;
  signal \adr_read_reorder_data_mem1_reg_n_0_[10]\ : STD_LOGIC;
  signal \adr_read_reorder_data_mem1_reg_n_0_[1]\ : STD_LOGIC;
  signal \adr_read_reorder_data_mem1_reg_n_0_[2]\ : STD_LOGIC;
  signal \adr_read_reorder_data_mem1_reg_n_0_[3]\ : STD_LOGIC;
  signal \adr_read_reorder_data_mem1_reg_n_0_[4]\ : STD_LOGIC;
  signal \adr_read_reorder_data_mem1_reg_n_0_[5]\ : STD_LOGIC;
  signal \adr_read_reorder_data_mem1_reg_n_0_[6]\ : STD_LOGIC;
  signal \adr_read_reorder_data_mem1_reg_n_0_[7]\ : STD_LOGIC;
  signal \adr_read_reorder_data_mem1_reg_n_0_[8]\ : STD_LOGIC;
  signal \adr_read_reorder_data_mem1_reg_n_0_[9]\ : STD_LOGIC;
  signal \adr_read_reorder_data_mem2[10]_i_1_n_0\ : STD_LOGIC;
  signal \adr_read_reorder_data_mem2[10]_i_2_n_0\ : STD_LOGIC;
  signal \adr_read_reorder_data_mem2[10]_i_4_n_0\ : STD_LOGIC;
  signal \adr_read_reorder_data_mem2[1]_i_2_n_0\ : STD_LOGIC;
  signal \adr_read_reorder_data_mem2[1]_i_3_n_0\ : STD_LOGIC;
  signal \adr_read_reorder_data_mem2[2]_i_2_n_0\ : STD_LOGIC;
  signal \adr_read_reorder_data_mem2[3]_i_2_n_0\ : STD_LOGIC;
  signal \adr_read_reorder_data_mem2[4]_i_2_n_0\ : STD_LOGIC;
  signal \adr_read_reorder_data_mem2[5]_i_2_n_0\ : STD_LOGIC;
  signal \adr_read_reorder_data_mem2[5]_i_3_n_0\ : STD_LOGIC;
  signal \adr_read_reorder_data_mem2[6]_i_2_n_0\ : STD_LOGIC;
  signal \adr_read_reorder_data_mem2[6]_i_3_n_0\ : STD_LOGIC;
  signal \adr_read_reorder_data_mem2[6]_i_4_n_0\ : STD_LOGIC;
  signal \adr_read_reorder_data_mem2[6]_i_5_n_0\ : STD_LOGIC;
  signal \adr_read_reorder_data_mem2_reg_n_0_[0]\ : STD_LOGIC;
  signal \adr_read_reorder_data_mem2_reg_n_0_[10]\ : STD_LOGIC;
  signal \adr_read_reorder_data_mem2_reg_n_0_[1]\ : STD_LOGIC;
  signal \adr_read_reorder_data_mem2_reg_n_0_[2]\ : STD_LOGIC;
  signal \adr_read_reorder_data_mem2_reg_n_0_[3]\ : STD_LOGIC;
  signal \adr_read_reorder_data_mem2_reg_n_0_[4]\ : STD_LOGIC;
  signal \adr_read_reorder_data_mem2_reg_n_0_[5]\ : STD_LOGIC;
  signal \adr_read_reorder_data_mem2_reg_n_0_[6]\ : STD_LOGIC;
  signal \adr_read_reorder_data_mem2_reg_n_0_[7]\ : STD_LOGIC;
  signal \adr_read_reorder_data_mem2_reg_n_0_[8]\ : STD_LOGIC;
  signal \adr_read_reorder_data_mem2_reg_n_0_[9]\ : STD_LOGIC;
  signal cnt_inner_valid : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal \cnt_inner_valid0_carry__0_n_0\ : STD_LOGIC;
  signal \cnt_inner_valid0_carry__0_n_1\ : STD_LOGIC;
  signal \cnt_inner_valid0_carry__0_n_2\ : STD_LOGIC;
  signal \cnt_inner_valid0_carry__0_n_3\ : STD_LOGIC;
  signal \cnt_inner_valid0_carry__0_n_4\ : STD_LOGIC;
  signal \cnt_inner_valid0_carry__0_n_5\ : STD_LOGIC;
  signal \cnt_inner_valid0_carry__0_n_6\ : STD_LOGIC;
  signal \cnt_inner_valid0_carry__0_n_7\ : STD_LOGIC;
  signal \cnt_inner_valid0_carry__1_n_0\ : STD_LOGIC;
  signal \cnt_inner_valid0_carry__1_n_1\ : STD_LOGIC;
  signal \cnt_inner_valid0_carry__1_n_2\ : STD_LOGIC;
  signal \cnt_inner_valid0_carry__1_n_3\ : STD_LOGIC;
  signal \cnt_inner_valid0_carry__1_n_4\ : STD_LOGIC;
  signal \cnt_inner_valid0_carry__1_n_5\ : STD_LOGIC;
  signal \cnt_inner_valid0_carry__1_n_6\ : STD_LOGIC;
  signal \cnt_inner_valid0_carry__1_n_7\ : STD_LOGIC;
  signal \cnt_inner_valid0_carry__2_n_2\ : STD_LOGIC;
  signal \cnt_inner_valid0_carry__2_n_3\ : STD_LOGIC;
  signal \cnt_inner_valid0_carry__2_n_5\ : STD_LOGIC;
  signal \cnt_inner_valid0_carry__2_n_6\ : STD_LOGIC;
  signal \cnt_inner_valid0_carry__2_n_7\ : STD_LOGIC;
  signal cnt_inner_valid0_carry_n_0 : STD_LOGIC;
  signal cnt_inner_valid0_carry_n_1 : STD_LOGIC;
  signal cnt_inner_valid0_carry_n_2 : STD_LOGIC;
  signal cnt_inner_valid0_carry_n_3 : STD_LOGIC;
  signal cnt_inner_valid0_carry_n_4 : STD_LOGIC;
  signal cnt_inner_valid0_carry_n_5 : STD_LOGIC;
  signal cnt_inner_valid0_carry_n_6 : STD_LOGIC;
  signal cnt_inner_valid0_carry_n_7 : STD_LOGIC;
  signal \cnt_inner_valid[15]_i_1_n_0\ : STD_LOGIC;
  signal \cnt_inner_valid[15]_i_3_n_0\ : STD_LOGIC;
  signal \cnt_inner_valid[15]_i_4_n_0\ : STD_LOGIC;
  signal \cnt_inner_valid[15]_i_5_n_0\ : STD_LOGIC;
  signal \cnt_inner_valid_reg_n_0_[0]\ : STD_LOGIC;
  signal \cnt_inner_valid_reg_n_0_[10]\ : STD_LOGIC;
  signal \cnt_inner_valid_reg_n_0_[11]\ : STD_LOGIC;
  signal \cnt_inner_valid_reg_n_0_[12]\ : STD_LOGIC;
  signal \cnt_inner_valid_reg_n_0_[13]\ : STD_LOGIC;
  signal \cnt_inner_valid_reg_n_0_[14]\ : STD_LOGIC;
  signal \cnt_inner_valid_reg_n_0_[15]\ : STD_LOGIC;
  signal \cnt_inner_valid_reg_n_0_[1]\ : STD_LOGIC;
  signal \cnt_inner_valid_reg_n_0_[2]\ : STD_LOGIC;
  signal \cnt_inner_valid_reg_n_0_[3]\ : STD_LOGIC;
  signal \cnt_inner_valid_reg_n_0_[4]\ : STD_LOGIC;
  signal \cnt_inner_valid_reg_n_0_[5]\ : STD_LOGIC;
  signal \cnt_inner_valid_reg_n_0_[6]\ : STD_LOGIC;
  signal \cnt_inner_valid_reg_n_0_[7]\ : STD_LOGIC;
  signal \cnt_inner_valid_reg_n_0_[8]\ : STD_LOGIC;
  signal \cnt_inner_valid_reg_n_0_[9]\ : STD_LOGIC;
  signal \cnt_last[0]_i_3_n_0\ : STD_LOGIC;
  signal \cnt_last[0]_i_4_n_0\ : STD_LOGIC;
  signal cnt_last_reg : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal \cnt_last_reg[0]_i_1_n_0\ : STD_LOGIC;
  signal \cnt_last_reg[0]_i_1_n_1\ : STD_LOGIC;
  signal \cnt_last_reg[0]_i_1_n_2\ : STD_LOGIC;
  signal \cnt_last_reg[0]_i_1_n_3\ : STD_LOGIC;
  signal \cnt_last_reg[0]_i_1_n_4\ : STD_LOGIC;
  signal \cnt_last_reg[0]_i_1_n_5\ : STD_LOGIC;
  signal \cnt_last_reg[0]_i_1_n_6\ : STD_LOGIC;
  signal \cnt_last_reg[0]_i_1_n_7\ : STD_LOGIC;
  signal \cnt_last_reg[12]_i_1_n_1\ : STD_LOGIC;
  signal \cnt_last_reg[12]_i_1_n_2\ : STD_LOGIC;
  signal \cnt_last_reg[12]_i_1_n_3\ : STD_LOGIC;
  signal \cnt_last_reg[12]_i_1_n_4\ : STD_LOGIC;
  signal \cnt_last_reg[12]_i_1_n_5\ : STD_LOGIC;
  signal \cnt_last_reg[12]_i_1_n_6\ : STD_LOGIC;
  signal \cnt_last_reg[12]_i_1_n_7\ : STD_LOGIC;
  signal \cnt_last_reg[4]_i_1_n_0\ : STD_LOGIC;
  signal \cnt_last_reg[4]_i_1_n_1\ : STD_LOGIC;
  signal \cnt_last_reg[4]_i_1_n_2\ : STD_LOGIC;
  signal \cnt_last_reg[4]_i_1_n_3\ : STD_LOGIC;
  signal \cnt_last_reg[4]_i_1_n_4\ : STD_LOGIC;
  signal \cnt_last_reg[4]_i_1_n_5\ : STD_LOGIC;
  signal \cnt_last_reg[4]_i_1_n_6\ : STD_LOGIC;
  signal \cnt_last_reg[4]_i_1_n_7\ : STD_LOGIC;
  signal \cnt_last_reg[8]_i_1_n_0\ : STD_LOGIC;
  signal \cnt_last_reg[8]_i_1_n_1\ : STD_LOGIC;
  signal \cnt_last_reg[8]_i_1_n_2\ : STD_LOGIC;
  signal \cnt_last_reg[8]_i_1_n_3\ : STD_LOGIC;
  signal \cnt_last_reg[8]_i_1_n_4\ : STD_LOGIC;
  signal \cnt_last_reg[8]_i_1_n_5\ : STD_LOGIC;
  signal \cnt_last_reg[8]_i_1_n_6\ : STD_LOGIC;
  signal \cnt_last_reg[8]_i_1_n_7\ : STD_LOGIC;
  signal ctrl_mux_out_1 : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal \ctrl_mux_out_1[3]_i_1_n_0\ : STD_LOGIC;
  signal \ctrl_mux_out_1[4]_i_1_n_0\ : STD_LOGIC;
  signal ctrl_mux_out_2 : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal \ctrl_mux_out_2[4]_i_1_n_0\ : STD_LOGIC;
  signal \genblk1[11].rams_sp_rf_rst_inst0_n_0\ : STD_LOGIC;
  signal \genblk1[11].rams_sp_rf_rst_inst0_n_1\ : STD_LOGIC;
  signal \genblk1[11].rams_sp_rf_rst_inst0_n_10\ : STD_LOGIC;
  signal \genblk1[11].rams_sp_rf_rst_inst0_n_11\ : STD_LOGIC;
  signal \genblk1[11].rams_sp_rf_rst_inst0_n_12\ : STD_LOGIC;
  signal \genblk1[11].rams_sp_rf_rst_inst0_n_13\ : STD_LOGIC;
  signal \genblk1[11].rams_sp_rf_rst_inst0_n_14\ : STD_LOGIC;
  signal \genblk1[11].rams_sp_rf_rst_inst0_n_15\ : STD_LOGIC;
  signal \genblk1[11].rams_sp_rf_rst_inst0_n_16\ : STD_LOGIC;
  signal \genblk1[11].rams_sp_rf_rst_inst0_n_17\ : STD_LOGIC;
  signal \genblk1[11].rams_sp_rf_rst_inst0_n_18\ : STD_LOGIC;
  signal \genblk1[11].rams_sp_rf_rst_inst0_n_19\ : STD_LOGIC;
  signal \genblk1[11].rams_sp_rf_rst_inst0_n_2\ : STD_LOGIC;
  signal \genblk1[11].rams_sp_rf_rst_inst0_n_20\ : STD_LOGIC;
  signal \genblk1[11].rams_sp_rf_rst_inst0_n_21\ : STD_LOGIC;
  signal \genblk1[11].rams_sp_rf_rst_inst0_n_22\ : STD_LOGIC;
  signal \genblk1[11].rams_sp_rf_rst_inst0_n_23\ : STD_LOGIC;
  signal \genblk1[11].rams_sp_rf_rst_inst0_n_3\ : STD_LOGIC;
  signal \genblk1[11].rams_sp_rf_rst_inst0_n_4\ : STD_LOGIC;
  signal \genblk1[11].rams_sp_rf_rst_inst0_n_5\ : STD_LOGIC;
  signal \genblk1[11].rams_sp_rf_rst_inst0_n_6\ : STD_LOGIC;
  signal \genblk1[11].rams_sp_rf_rst_inst0_n_7\ : STD_LOGIC;
  signal \genblk1[11].rams_sp_rf_rst_inst0_n_8\ : STD_LOGIC;
  signal \genblk1[11].rams_sp_rf_rst_inst0_n_9\ : STD_LOGIC;
  signal \genblk1[11].rams_sp_rf_rst_inst1_n_0\ : STD_LOGIC;
  signal \genblk1[11].rams_sp_rf_rst_inst1_n_1\ : STD_LOGIC;
  signal \genblk1[11].rams_sp_rf_rst_inst1_n_10\ : STD_LOGIC;
  signal \genblk1[11].rams_sp_rf_rst_inst1_n_11\ : STD_LOGIC;
  signal \genblk1[11].rams_sp_rf_rst_inst1_n_12\ : STD_LOGIC;
  signal \genblk1[11].rams_sp_rf_rst_inst1_n_13\ : STD_LOGIC;
  signal \genblk1[11].rams_sp_rf_rst_inst1_n_14\ : STD_LOGIC;
  signal \genblk1[11].rams_sp_rf_rst_inst1_n_15\ : STD_LOGIC;
  signal \genblk1[11].rams_sp_rf_rst_inst1_n_16\ : STD_LOGIC;
  signal \genblk1[11].rams_sp_rf_rst_inst1_n_17\ : STD_LOGIC;
  signal \genblk1[11].rams_sp_rf_rst_inst1_n_18\ : STD_LOGIC;
  signal \genblk1[11].rams_sp_rf_rst_inst1_n_19\ : STD_LOGIC;
  signal \genblk1[11].rams_sp_rf_rst_inst1_n_2\ : STD_LOGIC;
  signal \genblk1[11].rams_sp_rf_rst_inst1_n_20\ : STD_LOGIC;
  signal \genblk1[11].rams_sp_rf_rst_inst1_n_21\ : STD_LOGIC;
  signal \genblk1[11].rams_sp_rf_rst_inst1_n_22\ : STD_LOGIC;
  signal \genblk1[11].rams_sp_rf_rst_inst1_n_23\ : STD_LOGIC;
  signal \genblk1[11].rams_sp_rf_rst_inst1_n_3\ : STD_LOGIC;
  signal \genblk1[11].rams_sp_rf_rst_inst1_n_4\ : STD_LOGIC;
  signal \genblk1[11].rams_sp_rf_rst_inst1_n_5\ : STD_LOGIC;
  signal \genblk1[11].rams_sp_rf_rst_inst1_n_6\ : STD_LOGIC;
  signal \genblk1[11].rams_sp_rf_rst_inst1_n_7\ : STD_LOGIC;
  signal \genblk1[11].rams_sp_rf_rst_inst1_n_8\ : STD_LOGIC;
  signal \genblk1[11].rams_sp_rf_rst_inst1_n_9\ : STD_LOGIC;
  signal \genblk1[15].rams_sp_rf_rst_inst0_n_0\ : STD_LOGIC;
  signal \genblk1[15].rams_sp_rf_rst_inst0_n_1\ : STD_LOGIC;
  signal \genblk1[15].rams_sp_rf_rst_inst0_n_10\ : STD_LOGIC;
  signal \genblk1[15].rams_sp_rf_rst_inst0_n_11\ : STD_LOGIC;
  signal \genblk1[15].rams_sp_rf_rst_inst0_n_12\ : STD_LOGIC;
  signal \genblk1[15].rams_sp_rf_rst_inst0_n_13\ : STD_LOGIC;
  signal \genblk1[15].rams_sp_rf_rst_inst0_n_14\ : STD_LOGIC;
  signal \genblk1[15].rams_sp_rf_rst_inst0_n_15\ : STD_LOGIC;
  signal \genblk1[15].rams_sp_rf_rst_inst0_n_16\ : STD_LOGIC;
  signal \genblk1[15].rams_sp_rf_rst_inst0_n_17\ : STD_LOGIC;
  signal \genblk1[15].rams_sp_rf_rst_inst0_n_18\ : STD_LOGIC;
  signal \genblk1[15].rams_sp_rf_rst_inst0_n_19\ : STD_LOGIC;
  signal \genblk1[15].rams_sp_rf_rst_inst0_n_2\ : STD_LOGIC;
  signal \genblk1[15].rams_sp_rf_rst_inst0_n_20\ : STD_LOGIC;
  signal \genblk1[15].rams_sp_rf_rst_inst0_n_21\ : STD_LOGIC;
  signal \genblk1[15].rams_sp_rf_rst_inst0_n_22\ : STD_LOGIC;
  signal \genblk1[15].rams_sp_rf_rst_inst0_n_23\ : STD_LOGIC;
  signal \genblk1[15].rams_sp_rf_rst_inst0_n_24\ : STD_LOGIC;
  signal \genblk1[15].rams_sp_rf_rst_inst0_n_25\ : STD_LOGIC;
  signal \genblk1[15].rams_sp_rf_rst_inst0_n_26\ : STD_LOGIC;
  signal \genblk1[15].rams_sp_rf_rst_inst0_n_27\ : STD_LOGIC;
  signal \genblk1[15].rams_sp_rf_rst_inst0_n_28\ : STD_LOGIC;
  signal \genblk1[15].rams_sp_rf_rst_inst0_n_29\ : STD_LOGIC;
  signal \genblk1[15].rams_sp_rf_rst_inst0_n_3\ : STD_LOGIC;
  signal \genblk1[15].rams_sp_rf_rst_inst0_n_30\ : STD_LOGIC;
  signal \genblk1[15].rams_sp_rf_rst_inst0_n_31\ : STD_LOGIC;
  signal \genblk1[15].rams_sp_rf_rst_inst0_n_32\ : STD_LOGIC;
  signal \genblk1[15].rams_sp_rf_rst_inst0_n_33\ : STD_LOGIC;
  signal \genblk1[15].rams_sp_rf_rst_inst0_n_34\ : STD_LOGIC;
  signal \genblk1[15].rams_sp_rf_rst_inst0_n_4\ : STD_LOGIC;
  signal \genblk1[15].rams_sp_rf_rst_inst0_n_5\ : STD_LOGIC;
  signal \genblk1[15].rams_sp_rf_rst_inst0_n_6\ : STD_LOGIC;
  signal \genblk1[15].rams_sp_rf_rst_inst0_n_7\ : STD_LOGIC;
  signal \genblk1[15].rams_sp_rf_rst_inst0_n_8\ : STD_LOGIC;
  signal \genblk1[15].rams_sp_rf_rst_inst0_n_9\ : STD_LOGIC;
  signal \genblk1[15].rams_sp_rf_rst_inst1_n_0\ : STD_LOGIC;
  signal \genblk1[15].rams_sp_rf_rst_inst1_n_1\ : STD_LOGIC;
  signal \genblk1[15].rams_sp_rf_rst_inst1_n_10\ : STD_LOGIC;
  signal \genblk1[15].rams_sp_rf_rst_inst1_n_11\ : STD_LOGIC;
  signal \genblk1[15].rams_sp_rf_rst_inst1_n_12\ : STD_LOGIC;
  signal \genblk1[15].rams_sp_rf_rst_inst1_n_13\ : STD_LOGIC;
  signal \genblk1[15].rams_sp_rf_rst_inst1_n_14\ : STD_LOGIC;
  signal \genblk1[15].rams_sp_rf_rst_inst1_n_15\ : STD_LOGIC;
  signal \genblk1[15].rams_sp_rf_rst_inst1_n_16\ : STD_LOGIC;
  signal \genblk1[15].rams_sp_rf_rst_inst1_n_17\ : STD_LOGIC;
  signal \genblk1[15].rams_sp_rf_rst_inst1_n_18\ : STD_LOGIC;
  signal \genblk1[15].rams_sp_rf_rst_inst1_n_19\ : STD_LOGIC;
  signal \genblk1[15].rams_sp_rf_rst_inst1_n_2\ : STD_LOGIC;
  signal \genblk1[15].rams_sp_rf_rst_inst1_n_20\ : STD_LOGIC;
  signal \genblk1[15].rams_sp_rf_rst_inst1_n_21\ : STD_LOGIC;
  signal \genblk1[15].rams_sp_rf_rst_inst1_n_22\ : STD_LOGIC;
  signal \genblk1[15].rams_sp_rf_rst_inst1_n_23\ : STD_LOGIC;
  signal \genblk1[15].rams_sp_rf_rst_inst1_n_3\ : STD_LOGIC;
  signal \genblk1[15].rams_sp_rf_rst_inst1_n_4\ : STD_LOGIC;
  signal \genblk1[15].rams_sp_rf_rst_inst1_n_5\ : STD_LOGIC;
  signal \genblk1[15].rams_sp_rf_rst_inst1_n_6\ : STD_LOGIC;
  signal \genblk1[15].rams_sp_rf_rst_inst1_n_7\ : STD_LOGIC;
  signal \genblk1[15].rams_sp_rf_rst_inst1_n_8\ : STD_LOGIC;
  signal \genblk1[15].rams_sp_rf_rst_inst1_n_9\ : STD_LOGIC;
  signal \genblk1[3].rams_sp_rf_rst_inst0_n_0\ : STD_LOGIC;
  signal \genblk1[3].rams_sp_rf_rst_inst0_n_1\ : STD_LOGIC;
  signal \genblk1[3].rams_sp_rf_rst_inst0_n_10\ : STD_LOGIC;
  signal \genblk1[3].rams_sp_rf_rst_inst0_n_11\ : STD_LOGIC;
  signal \genblk1[3].rams_sp_rf_rst_inst0_n_12\ : STD_LOGIC;
  signal \genblk1[3].rams_sp_rf_rst_inst0_n_13\ : STD_LOGIC;
  signal \genblk1[3].rams_sp_rf_rst_inst0_n_14\ : STD_LOGIC;
  signal \genblk1[3].rams_sp_rf_rst_inst0_n_15\ : STD_LOGIC;
  signal \genblk1[3].rams_sp_rf_rst_inst0_n_16\ : STD_LOGIC;
  signal \genblk1[3].rams_sp_rf_rst_inst0_n_17\ : STD_LOGIC;
  signal \genblk1[3].rams_sp_rf_rst_inst0_n_18\ : STD_LOGIC;
  signal \genblk1[3].rams_sp_rf_rst_inst0_n_19\ : STD_LOGIC;
  signal \genblk1[3].rams_sp_rf_rst_inst0_n_2\ : STD_LOGIC;
  signal \genblk1[3].rams_sp_rf_rst_inst0_n_20\ : STD_LOGIC;
  signal \genblk1[3].rams_sp_rf_rst_inst0_n_21\ : STD_LOGIC;
  signal \genblk1[3].rams_sp_rf_rst_inst0_n_22\ : STD_LOGIC;
  signal \genblk1[3].rams_sp_rf_rst_inst0_n_23\ : STD_LOGIC;
  signal \genblk1[3].rams_sp_rf_rst_inst0_n_3\ : STD_LOGIC;
  signal \genblk1[3].rams_sp_rf_rst_inst0_n_4\ : STD_LOGIC;
  signal \genblk1[3].rams_sp_rf_rst_inst0_n_5\ : STD_LOGIC;
  signal \genblk1[3].rams_sp_rf_rst_inst0_n_6\ : STD_LOGIC;
  signal \genblk1[3].rams_sp_rf_rst_inst0_n_7\ : STD_LOGIC;
  signal \genblk1[3].rams_sp_rf_rst_inst0_n_8\ : STD_LOGIC;
  signal \genblk1[3].rams_sp_rf_rst_inst0_n_9\ : STD_LOGIC;
  signal \genblk1[3].rams_sp_rf_rst_inst1_n_0\ : STD_LOGIC;
  signal \genblk1[3].rams_sp_rf_rst_inst1_n_1\ : STD_LOGIC;
  signal \genblk1[3].rams_sp_rf_rst_inst1_n_10\ : STD_LOGIC;
  signal \genblk1[3].rams_sp_rf_rst_inst1_n_11\ : STD_LOGIC;
  signal \genblk1[3].rams_sp_rf_rst_inst1_n_12\ : STD_LOGIC;
  signal \genblk1[3].rams_sp_rf_rst_inst1_n_13\ : STD_LOGIC;
  signal \genblk1[3].rams_sp_rf_rst_inst1_n_14\ : STD_LOGIC;
  signal \genblk1[3].rams_sp_rf_rst_inst1_n_15\ : STD_LOGIC;
  signal \genblk1[3].rams_sp_rf_rst_inst1_n_16\ : STD_LOGIC;
  signal \genblk1[3].rams_sp_rf_rst_inst1_n_17\ : STD_LOGIC;
  signal \genblk1[3].rams_sp_rf_rst_inst1_n_18\ : STD_LOGIC;
  signal \genblk1[3].rams_sp_rf_rst_inst1_n_19\ : STD_LOGIC;
  signal \genblk1[3].rams_sp_rf_rst_inst1_n_2\ : STD_LOGIC;
  signal \genblk1[3].rams_sp_rf_rst_inst1_n_20\ : STD_LOGIC;
  signal \genblk1[3].rams_sp_rf_rst_inst1_n_21\ : STD_LOGIC;
  signal \genblk1[3].rams_sp_rf_rst_inst1_n_22\ : STD_LOGIC;
  signal \genblk1[3].rams_sp_rf_rst_inst1_n_23\ : STD_LOGIC;
  signal \genblk1[3].rams_sp_rf_rst_inst1_n_3\ : STD_LOGIC;
  signal \genblk1[3].rams_sp_rf_rst_inst1_n_4\ : STD_LOGIC;
  signal \genblk1[3].rams_sp_rf_rst_inst1_n_5\ : STD_LOGIC;
  signal \genblk1[3].rams_sp_rf_rst_inst1_n_6\ : STD_LOGIC;
  signal \genblk1[3].rams_sp_rf_rst_inst1_n_7\ : STD_LOGIC;
  signal \genblk1[3].rams_sp_rf_rst_inst1_n_8\ : STD_LOGIC;
  signal \genblk1[3].rams_sp_rf_rst_inst1_n_9\ : STD_LOGIC;
  signal \genblk1[7].rams_sp_rf_rst_inst0_n_0\ : STD_LOGIC;
  signal \genblk1[7].rams_sp_rf_rst_inst0_n_1\ : STD_LOGIC;
  signal \genblk1[7].rams_sp_rf_rst_inst0_n_10\ : STD_LOGIC;
  signal \genblk1[7].rams_sp_rf_rst_inst0_n_11\ : STD_LOGIC;
  signal \genblk1[7].rams_sp_rf_rst_inst0_n_12\ : STD_LOGIC;
  signal \genblk1[7].rams_sp_rf_rst_inst0_n_13\ : STD_LOGIC;
  signal \genblk1[7].rams_sp_rf_rst_inst0_n_14\ : STD_LOGIC;
  signal \genblk1[7].rams_sp_rf_rst_inst0_n_15\ : STD_LOGIC;
  signal \genblk1[7].rams_sp_rf_rst_inst0_n_16\ : STD_LOGIC;
  signal \genblk1[7].rams_sp_rf_rst_inst0_n_17\ : STD_LOGIC;
  signal \genblk1[7].rams_sp_rf_rst_inst0_n_18\ : STD_LOGIC;
  signal \genblk1[7].rams_sp_rf_rst_inst0_n_19\ : STD_LOGIC;
  signal \genblk1[7].rams_sp_rf_rst_inst0_n_2\ : STD_LOGIC;
  signal \genblk1[7].rams_sp_rf_rst_inst0_n_20\ : STD_LOGIC;
  signal \genblk1[7].rams_sp_rf_rst_inst0_n_21\ : STD_LOGIC;
  signal \genblk1[7].rams_sp_rf_rst_inst0_n_22\ : STD_LOGIC;
  signal \genblk1[7].rams_sp_rf_rst_inst0_n_23\ : STD_LOGIC;
  signal \genblk1[7].rams_sp_rf_rst_inst0_n_3\ : STD_LOGIC;
  signal \genblk1[7].rams_sp_rf_rst_inst0_n_4\ : STD_LOGIC;
  signal \genblk1[7].rams_sp_rf_rst_inst0_n_5\ : STD_LOGIC;
  signal \genblk1[7].rams_sp_rf_rst_inst0_n_6\ : STD_LOGIC;
  signal \genblk1[7].rams_sp_rf_rst_inst0_n_7\ : STD_LOGIC;
  signal \genblk1[7].rams_sp_rf_rst_inst0_n_8\ : STD_LOGIC;
  signal \genblk1[7].rams_sp_rf_rst_inst0_n_9\ : STD_LOGIC;
  signal \genblk1[7].rams_sp_rf_rst_inst1_n_0\ : STD_LOGIC;
  signal \genblk1[7].rams_sp_rf_rst_inst1_n_1\ : STD_LOGIC;
  signal \genblk1[7].rams_sp_rf_rst_inst1_n_10\ : STD_LOGIC;
  signal \genblk1[7].rams_sp_rf_rst_inst1_n_11\ : STD_LOGIC;
  signal \genblk1[7].rams_sp_rf_rst_inst1_n_12\ : STD_LOGIC;
  signal \genblk1[7].rams_sp_rf_rst_inst1_n_13\ : STD_LOGIC;
  signal \genblk1[7].rams_sp_rf_rst_inst1_n_14\ : STD_LOGIC;
  signal \genblk1[7].rams_sp_rf_rst_inst1_n_15\ : STD_LOGIC;
  signal \genblk1[7].rams_sp_rf_rst_inst1_n_16\ : STD_LOGIC;
  signal \genblk1[7].rams_sp_rf_rst_inst1_n_17\ : STD_LOGIC;
  signal \genblk1[7].rams_sp_rf_rst_inst1_n_18\ : STD_LOGIC;
  signal \genblk1[7].rams_sp_rf_rst_inst1_n_19\ : STD_LOGIC;
  signal \genblk1[7].rams_sp_rf_rst_inst1_n_2\ : STD_LOGIC;
  signal \genblk1[7].rams_sp_rf_rst_inst1_n_20\ : STD_LOGIC;
  signal \genblk1[7].rams_sp_rf_rst_inst1_n_21\ : STD_LOGIC;
  signal \genblk1[7].rams_sp_rf_rst_inst1_n_22\ : STD_LOGIC;
  signal \genblk1[7].rams_sp_rf_rst_inst1_n_23\ : STD_LOGIC;
  signal \genblk1[7].rams_sp_rf_rst_inst1_n_3\ : STD_LOGIC;
  signal \genblk1[7].rams_sp_rf_rst_inst1_n_4\ : STD_LOGIC;
  signal \genblk1[7].rams_sp_rf_rst_inst1_n_5\ : STD_LOGIC;
  signal \genblk1[7].rams_sp_rf_rst_inst1_n_6\ : STD_LOGIC;
  signal \genblk1[7].rams_sp_rf_rst_inst1_n_7\ : STD_LOGIC;
  signal \genblk1[7].rams_sp_rf_rst_inst1_n_8\ : STD_LOGIC;
  signal \genblk1[7].rams_sp_rf_rst_inst1_n_9\ : STD_LOGIC;
  signal last : STD_LOGIC;
  signal locked_EN1_i_1_n_0 : STD_LOGIC;
  signal locked_EN1_i_2_n_0 : STD_LOGIC;
  signal locked_EN1_reg_n_0 : STD_LOGIC;
  signal locked_EN2_i_1_n_0 : STD_LOGIC;
  signal locked_EN2_i_2_n_0 : STD_LOGIC;
  signal locked_EN2_reg_n_0 : STD_LOGIC;
  signal \^m00_axis_tlast\ : STD_LOGIC;
  signal m00_axis_tlast_INST_0_i_1_n_0 : STD_LOGIC;
  signal m00_axis_tlast_INST_0_i_2_n_0 : STD_LOGIC;
  signal \^m00_axis_tvalid\ : STD_LOGIC;
  signal m00_axis_tvalid_r_i_1_n_0 : STD_LOGIC;
  signal m00_axis_tvalid_r_i_2_n_0 : STD_LOGIC;
  signal m00_axis_tvalid_r_i_3_n_0 : STD_LOGIC;
  signal m00_axis_tvalid_r_i_4_n_0 : STD_LOGIC;
  signal out_mux_2 : STD_LOGIC_VECTOR ( 23 downto 0 );
  signal p_1_in : STD_LOGIC_VECTOR ( 10 downto 0 );
  signal \NLW_active_mem1_in_read_state_reg[15]_i_3_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_active_mem1_in_read_state_reg[15]_i_3_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_active_mem2_in_read_state_reg[12]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_cnt_inner_valid0_carry__2_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_cnt_inner_valid0_carry__2_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_cnt_last_reg[12]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \EN1[0]_i_2\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \EN1[14]_i_2\ : label is "soft_lutpair22";
  attribute SOFT_HLUTNM of \EN1[15]_i_3\ : label is "soft_lutpair23";
  attribute SOFT_HLUTNM of \EN1[15]_i_4\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \EN1[6]_i_1\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \EN1[6]_i_2\ : label is "soft_lutpair22";
  attribute SOFT_HLUTNM of \EN1[7]_i_2\ : label is "soft_lutpair23";
  attribute SOFT_HLUTNM of \EN2[0]_i_3\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \EN2[14]_i_3\ : label is "soft_lutpair25";
  attribute SOFT_HLUTNM of \EN2[15]_i_3\ : label is "soft_lutpair36";
  attribute SOFT_HLUTNM of \EN2[15]_i_5\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \EN2[15]_i_6\ : label is "soft_lutpair27";
  attribute SOFT_HLUTNM of \EN2[1]_i_1\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \EN2[6]_i_2\ : label is "soft_lutpair25";
  attribute SOFT_HLUTNM of \EN2[7]_i_3\ : label is "soft_lutpair27";
  attribute SOFT_HLUTNM of \active_mem1_in_read_state[0]_i_1\ : label is "soft_lutpair35";
  attribute SOFT_HLUTNM of \active_mem1_in_read_state[10]_i_1\ : label is "soft_lutpair29";
  attribute SOFT_HLUTNM of \active_mem1_in_read_state[11]_i_1\ : label is "soft_lutpair28";
  attribute SOFT_HLUTNM of \active_mem1_in_read_state[12]_i_1\ : label is "soft_lutpair31";
  attribute SOFT_HLUTNM of \active_mem1_in_read_state[13]_i_1\ : label is "soft_lutpair30";
  attribute SOFT_HLUTNM of \active_mem1_in_read_state[14]_i_1\ : label is "soft_lutpair29";
  attribute SOFT_HLUTNM of \active_mem1_in_read_state[15]_i_2\ : label is "soft_lutpair28";
  attribute SOFT_HLUTNM of \active_mem1_in_read_state[1]_i_1\ : label is "soft_lutpair35";
  attribute SOFT_HLUTNM of \active_mem1_in_read_state[2]_i_1\ : label is "soft_lutpair34";
  attribute SOFT_HLUTNM of \active_mem1_in_read_state[3]_i_1\ : label is "soft_lutpair33";
  attribute SOFT_HLUTNM of \active_mem1_in_read_state[4]_i_1\ : label is "soft_lutpair32";
  attribute SOFT_HLUTNM of \active_mem1_in_read_state[5]_i_1\ : label is "soft_lutpair34";
  attribute SOFT_HLUTNM of \active_mem1_in_read_state[6]_i_1\ : label is "soft_lutpair33";
  attribute SOFT_HLUTNM of \active_mem1_in_read_state[7]_i_1\ : label is "soft_lutpair32";
  attribute SOFT_HLUTNM of \active_mem1_in_read_state[8]_i_1\ : label is "soft_lutpair31";
  attribute SOFT_HLUTNM of \active_mem1_in_read_state[9]_i_1\ : label is "soft_lutpair30";
  attribute SOFT_HLUTNM of \adr_read_reorder_data_mem1[3]_i_2\ : label is "soft_lutpair26";
  attribute SOFT_HLUTNM of \adr_read_reorder_data_mem1[5]_i_2\ : label is "soft_lutpair17";
  attribute SOFT_HLUTNM of \adr_read_reorder_data_mem1[5]_i_3\ : label is "soft_lutpair18";
  attribute SOFT_HLUTNM of \adr_read_reorder_data_mem1[6]_i_2\ : label is "soft_lutpair26";
  attribute SOFT_HLUTNM of \adr_read_reorder_data_mem1[6]_i_3\ : label is "soft_lutpair18";
  attribute SOFT_HLUTNM of \adr_read_reorder_data_mem1[7]_i_1\ : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of \adr_read_reorder_data_mem1[8]_i_1\ : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of \adr_read_reorder_data_mem2[10]_i_4\ : label is "soft_lutpair37";
  attribute SOFT_HLUTNM of \adr_read_reorder_data_mem2[1]_i_2\ : label is "soft_lutpair20";
  attribute SOFT_HLUTNM of \adr_read_reorder_data_mem2[1]_i_3\ : label is "soft_lutpair21";
  attribute SOFT_HLUTNM of \adr_read_reorder_data_mem2[2]_i_2\ : label is "soft_lutpair24";
  attribute SOFT_HLUTNM of \adr_read_reorder_data_mem2[3]_i_2\ : label is "soft_lutpair24";
  attribute SOFT_HLUTNM of \adr_read_reorder_data_mem2[4]_i_2\ : label is "soft_lutpair19";
  attribute SOFT_HLUTNM of \adr_read_reorder_data_mem2[5]_i_2\ : label is "soft_lutpair19";
  attribute SOFT_HLUTNM of \adr_read_reorder_data_mem2[5]_i_3\ : label is "soft_lutpair37";
  attribute SOFT_HLUTNM of \adr_read_reorder_data_mem2[6]_i_2\ : label is "soft_lutpair20";
  attribute SOFT_HLUTNM of \adr_read_reorder_data_mem2[7]_i_1\ : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of \adr_read_reorder_data_mem2[8]_i_1\ : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of \cnt_inner_valid[15]_i_3\ : label is "soft_lutpair36";
  attribute SOFT_HLUTNM of m00_axis_tvalid_r_i_2 : label is "soft_lutpair17";
  attribute SOFT_HLUTNM of m00_axis_tvalid_r_i_3 : label is "soft_lutpair21";
begin
  m00_axis_tlast <= \^m00_axis_tlast\;
  m00_axis_tvalid <= \^m00_axis_tvalid\;
\EN1[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"888888888888F888"
    )
        port map (
      I0 => \EN1[0]_i_2_n_0\,
      I1 => \EN1[6]_i_2_n_0\,
      I2 => \EN1[3]_i_2_n_0\,
      I3 => \EN2[0]_i_2_n_0\,
      I4 => \cnt_inner_valid_reg_n_0_[5]\,
      I5 => \cnt_inner_valid_reg_n_0_[4]\,
      O => \EN1[0]_i_1_n_0\
    );
\EN1[0]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \active_mem1_in_read_state_reg_n_0_[1]\,
      I1 => \active_mem1_in_read_state_reg_n_0_[2]\,
      O => \EN1[0]_i_2_n_0\
    );
\EN1[10]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"20FF202020202020"
    )
        port map (
      I0 => \active_mem1_in_read_state_reg_n_0_[1]\,
      I1 => \active_mem1_in_read_state_reg_n_0_[2]\,
      I2 => \EN1[14]_i_2_n_0\,
      I3 => \cnt_inner_valid[15]_i_3_n_0\,
      I4 => \EN2[14]_i_2_n_0\,
      I5 => \EN1[15]_i_4_n_0\,
      O => \EN1[10]_i_1_n_0\
    );
\EN1[11]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"08FF080808080808"
    )
        port map (
      I0 => \EN1[15]_i_3_n_0\,
      I1 => \active_mem1_in_read_state_reg_n_0_[1]\,
      I2 => \active_mem1_in_read_state_reg_n_0_[2]\,
      I3 => \cnt_inner_valid[15]_i_3_n_0\,
      I4 => \EN2[15]_i_4_n_0\,
      I5 => \EN1[15]_i_4_n_0\,
      O => \EN1[11]_i_1_n_0\
    );
\EN1[12]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF20202020202020"
    )
        port map (
      I0 => \active_mem1_in_read_state_reg_n_0_[2]\,
      I1 => \active_mem1_in_read_state_reg_n_0_[1]\,
      I2 => \EN1[14]_i_2_n_0\,
      I3 => \EN2[12]_i_2_n_0\,
      I4 => \EN1[15]_i_4_n_0\,
      I5 => \cnt_inner_valid_reg_n_0_[7]\,
      O => \EN1[12]_i_1_n_0\
    );
\EN1[13]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF08080808080808"
    )
        port map (
      I0 => \EN1[15]_i_3_n_0\,
      I1 => \active_mem1_in_read_state_reg_n_0_[2]\,
      I2 => \active_mem1_in_read_state_reg_n_0_[1]\,
      I3 => \EN2[15]_i_3_n_0\,
      I4 => \EN2[13]_i_2_n_0\,
      I5 => \EN1[15]_i_4_n_0\,
      O => \EN1[13]_i_1_n_0\
    );
\EN1[14]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF80808080808080"
    )
        port map (
      I0 => \active_mem1_in_read_state_reg_n_0_[1]\,
      I1 => \active_mem1_in_read_state_reg_n_0_[2]\,
      I2 => \EN1[14]_i_2_n_0\,
      I3 => \EN2[15]_i_3_n_0\,
      I4 => \EN2[14]_i_2_n_0\,
      I5 => \EN1[15]_i_4_n_0\,
      O => \EN1[14]_i_1_n_0\
    );
\EN1[14]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => \active_mem1_in_read_state_reg_n_0_[0]\,
      I1 => \EN1[15]_i_5_n_0\,
      I2 => \active_mem1_in_read_state_reg_n_0_[3]\,
      O => \EN1[14]_i_2_n_0\
    );
\EN1[15]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => locked_EN1_reg_n_0,
      I1 => s00_axis_tuser(11),
      I2 => s00_axis_aresetn,
      O => \EN1[15]_i_1_n_0\
    );
\EN1[15]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF80808080808080"
    )
        port map (
      I0 => \active_mem1_in_read_state_reg_n_0_[1]\,
      I1 => \active_mem1_in_read_state_reg_n_0_[2]\,
      I2 => \EN1[15]_i_3_n_0\,
      I3 => \EN2[15]_i_3_n_0\,
      I4 => \EN2[15]_i_4_n_0\,
      I5 => \EN1[15]_i_4_n_0\,
      O => \EN1[15]_i_2_n_0\
    );
\EN1[15]_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => \active_mem1_in_read_state_reg_n_0_[0]\,
      I1 => \EN1[15]_i_5_n_0\,
      I2 => \active_mem1_in_read_state_reg_n_0_[3]\,
      O => \EN1[15]_i_3_n_0\
    );
\EN1[15]_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00010101"
    )
        port map (
      I0 => \cnt_inner_valid_reg_n_0_[10]\,
      I1 => \cnt_inner_valid_reg_n_0_[8]\,
      I2 => \cnt_inner_valid_reg_n_0_[9]\,
      I3 => s00_axis_tuser(11),
      I4 => s00_axis_aresetn,
      O => \EN1[15]_i_4_n_0\
    );
\EN1[15]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000200000000"
    )
        port map (
      I0 => \EN1[15]_i_6_n_0\,
      I1 => \active_mem1_in_read_state_reg_n_0_[10]\,
      I2 => \active_mem1_in_read_state_reg_n_0_[9]\,
      I3 => \active_mem1_in_read_state_reg_n_0_[8]\,
      I4 => \active_mem1_in_read_state_reg_n_0_[7]\,
      I5 => \EN1[15]_i_7_n_0\,
      O => \EN1[15]_i_5_n_0\
    );
\EN1[15]_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0001"
    )
        port map (
      I0 => \active_mem1_in_read_state_reg_n_0_[14]\,
      I1 => \active_mem1_in_read_state_reg_n_0_[13]\,
      I2 => \active_mem1_in_read_state_reg_n_0_[12]\,
      I3 => \active_mem1_in_read_state_reg_n_0_[11]\,
      O => \EN1[15]_i_6_n_0\
    );
\EN1[15]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000040"
    )
        port map (
      I0 => \active_mem1_in_read_state_reg_n_0_[15]\,
      I1 => s00_axis_tuser(11),
      I2 => s00_axis_aresetn,
      I3 => \active_mem1_in_read_state_reg_n_0_[4]\,
      I4 => \active_mem1_in_read_state_reg_n_0_[6]\,
      I5 => \active_mem1_in_read_state_reg_n_0_[5]\,
      O => \EN1[15]_i_7_n_0\
    );
\EN1[1]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FF101010"
    )
        port map (
      I0 => \active_mem1_in_read_state_reg_n_0_[1]\,
      I1 => \active_mem1_in_read_state_reg_n_0_[2]\,
      I2 => \EN1[7]_i_2_n_0\,
      I3 => \EN2[13]_i_2_n_0\,
      I4 => \EN1[3]_i_2_n_0\,
      O => \EN1[1]_i_1_n_0\
    );
\EN1[2]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FF202020"
    )
        port map (
      I0 => \active_mem1_in_read_state_reg_n_0_[1]\,
      I1 => \active_mem1_in_read_state_reg_n_0_[2]\,
      I2 => \EN1[6]_i_2_n_0\,
      I3 => \EN2[14]_i_2_n_0\,
      I4 => \EN1[3]_i_2_n_0\,
      O => \EN1[2]_i_1_n_0\
    );
\EN1[3]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FF202020"
    )
        port map (
      I0 => \active_mem1_in_read_state_reg_n_0_[1]\,
      I1 => \active_mem1_in_read_state_reg_n_0_[2]\,
      I2 => \EN1[7]_i_2_n_0\,
      I3 => \EN2[15]_i_4_n_0\,
      I4 => \EN1[3]_i_2_n_0\,
      O => \EN1[3]_i_1_n_0\
    );
\EN1[3]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000002"
    )
        port map (
      I0 => \adr_read_reorder_data_mem2[10]_i_2_n_0\,
      I1 => \cnt_inner_valid_reg_n_0_[10]\,
      I2 => \cnt_inner_valid_reg_n_0_[8]\,
      I3 => \cnt_inner_valid_reg_n_0_[9]\,
      I4 => \cnt_inner_valid_reg_n_0_[7]\,
      I5 => \cnt_inner_valid[15]_i_5_n_0\,
      O => \EN1[3]_i_2_n_0\
    );
\EN1[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FF202020"
    )
        port map (
      I0 => \active_mem1_in_read_state_reg_n_0_[2]\,
      I1 => \active_mem1_in_read_state_reg_n_0_[1]\,
      I2 => \EN1[6]_i_2_n_0\,
      I3 => \EN2[12]_i_2_n_0\,
      I4 => \EN1[4]_i_2_n_0\,
      O => \EN1[4]_i_1_n_0\
    );
\EN1[4]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000100010001"
    )
        port map (
      I0 => \cnt_inner_valid_reg_n_0_[7]\,
      I1 => \cnt_inner_valid_reg_n_0_[9]\,
      I2 => \cnt_inner_valid_reg_n_0_[8]\,
      I3 => \cnt_inner_valid_reg_n_0_[10]\,
      I4 => s00_axis_tuser(11),
      I5 => s00_axis_aresetn,
      O => \EN1[4]_i_2_n_0\
    );
\EN1[5]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FF202020"
    )
        port map (
      I0 => \active_mem1_in_read_state_reg_n_0_[2]\,
      I1 => \active_mem1_in_read_state_reg_n_0_[1]\,
      I2 => \EN1[7]_i_2_n_0\,
      I3 => \EN2[13]_i_2_n_0\,
      I4 => \EN1[7]_i_3_n_0\,
      O => \EN1[5]_i_1_n_0\
    );
\EN1[6]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FF808080"
    )
        port map (
      I0 => \active_mem1_in_read_state_reg_n_0_[1]\,
      I1 => \active_mem1_in_read_state_reg_n_0_[2]\,
      I2 => \EN1[6]_i_2_n_0\,
      I3 => \EN2[14]_i_2_n_0\,
      I4 => \EN1[7]_i_3_n_0\,
      O => \EN1[6]_i_1_n_0\
    );
\EN1[6]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"04"
    )
        port map (
      I0 => \active_mem1_in_read_state_reg_n_0_[0]\,
      I1 => \EN1[15]_i_5_n_0\,
      I2 => \active_mem1_in_read_state_reg_n_0_[3]\,
      O => \EN1[6]_i_2_n_0\
    );
\EN1[7]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FF808080"
    )
        port map (
      I0 => \active_mem1_in_read_state_reg_n_0_[1]\,
      I1 => \active_mem1_in_read_state_reg_n_0_[2]\,
      I2 => \EN1[7]_i_2_n_0\,
      I3 => \EN2[15]_i_4_n_0\,
      I4 => \EN1[7]_i_3_n_0\,
      O => \EN1[7]_i_1_n_0\
    );
\EN1[7]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => \active_mem1_in_read_state_reg_n_0_[0]\,
      I1 => \EN1[15]_i_5_n_0\,
      I2 => \active_mem1_in_read_state_reg_n_0_[3]\,
      O => \EN1[7]_i_2_n_0\
    );
\EN1[7]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000200000000"
    )
        port map (
      I0 => \adr_read_reorder_data_mem2[10]_i_2_n_0\,
      I1 => \cnt_inner_valid_reg_n_0_[10]\,
      I2 => \cnt_inner_valid_reg_n_0_[8]\,
      I3 => \cnt_inner_valid_reg_n_0_[9]\,
      I4 => \cnt_inner_valid_reg_n_0_[7]\,
      I5 => \EN2[15]_i_7_n_0\,
      O => \EN1[7]_i_3_n_0\
    );
\EN1[8]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7777777700070000"
    )
        port map (
      I0 => locked_EN1_reg_n_0,
      I1 => s00_axis_tuser(11),
      I2 => \active_mem1_in_read_state_reg_n_0_[1]\,
      I3 => \active_mem1_in_read_state_reg_n_0_[2]\,
      I4 => \EN1[14]_i_2_n_0\,
      I5 => \EN1[8]_i_2_n_0\,
      O => \EN1[8]_i_1_n_0\
    );
\EN1[8]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000044F0000000"
    )
        port map (
      I0 => \cnt_inner_valid[15]_i_3_n_0\,
      I1 => \EN1[15]_i_4_n_0\,
      I2 => \EN1[7]_i_3_n_0\,
      I3 => \cnt_inner_valid_reg_n_0_[5]\,
      I4 => \cnt_inner_valid_reg_n_0_[4]\,
      I5 => \EN2[0]_i_2_n_0\,
      O => \EN1[8]_i_2_n_0\
    );
\EN1[9]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"02FF020202020202"
    )
        port map (
      I0 => \EN1[15]_i_3_n_0\,
      I1 => \active_mem1_in_read_state_reg_n_0_[1]\,
      I2 => \active_mem1_in_read_state_reg_n_0_[2]\,
      I3 => \cnt_inner_valid[15]_i_3_n_0\,
      I4 => \EN2[13]_i_2_n_0\,
      I5 => \EN1[15]_i_4_n_0\,
      O => \EN1[9]_i_1_n_0\
    );
\EN1_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => s00_axis_aresetn,
      D => \EN1[0]_i_1_n_0\,
      Q => \EN1_reg_n_0_[0]\,
      R => \EN1[15]_i_1_n_0\
    );
\EN1_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => s00_axis_aresetn,
      D => \EN1[10]_i_1_n_0\,
      Q => \EN1_reg_n_0_[10]\,
      R => \EN1[15]_i_1_n_0\
    );
\EN1_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => s00_axis_aresetn,
      D => \EN1[11]_i_1_n_0\,
      Q => \EN1_reg_n_0_[11]\,
      R => \EN1[15]_i_1_n_0\
    );
\EN1_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => s00_axis_aresetn,
      D => \EN1[12]_i_1_n_0\,
      Q => \EN1_reg_n_0_[12]\,
      R => \EN1[15]_i_1_n_0\
    );
\EN1_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => s00_axis_aresetn,
      D => \EN1[13]_i_1_n_0\,
      Q => \EN1_reg_n_0_[13]\,
      R => \EN1[15]_i_1_n_0\
    );
\EN1_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => s00_axis_aresetn,
      D => \EN1[14]_i_1_n_0\,
      Q => \EN1_reg_n_0_[14]\,
      R => \EN1[15]_i_1_n_0\
    );
\EN1_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => s00_axis_aresetn,
      D => \EN1[15]_i_2_n_0\,
      Q => \EN1_reg_n_0_[15]\,
      R => \EN1[15]_i_1_n_0\
    );
\EN1_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => s00_axis_aresetn,
      D => \EN1[1]_i_1_n_0\,
      Q => \EN1_reg_n_0_[1]\,
      R => \EN1[15]_i_1_n_0\
    );
\EN1_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => s00_axis_aresetn,
      D => \EN1[2]_i_1_n_0\,
      Q => \EN1_reg_n_0_[2]\,
      R => \EN1[15]_i_1_n_0\
    );
\EN1_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => s00_axis_aresetn,
      D => \EN1[3]_i_1_n_0\,
      Q => \EN1_reg_n_0_[3]\,
      R => \EN1[15]_i_1_n_0\
    );
\EN1_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => s00_axis_aresetn,
      D => \EN1[4]_i_1_n_0\,
      Q => \EN1_reg_n_0_[4]\,
      R => \EN1[15]_i_1_n_0\
    );
\EN1_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => s00_axis_aresetn,
      D => \EN1[5]_i_1_n_0\,
      Q => \EN1_reg_n_0_[5]\,
      R => \EN1[15]_i_1_n_0\
    );
\EN1_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => s00_axis_aresetn,
      D => \EN1[6]_i_1_n_0\,
      Q => \EN1_reg_n_0_[6]\,
      R => \EN1[15]_i_1_n_0\
    );
\EN1_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => s00_axis_aresetn,
      D => \EN1[7]_i_1_n_0\,
      Q => \EN1_reg_n_0_[7]\,
      R => \EN1[15]_i_1_n_0\
    );
\EN1_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => s00_axis_aresetn,
      D => \EN1[8]_i_1_n_0\,
      Q => \EN1_reg_n_0_[8]\,
      R => '0'
    );
\EN1_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => s00_axis_aresetn,
      D => \EN1[9]_i_1_n_0\,
      Q => \EN1_reg_n_0_[9]\,
      R => \EN1[15]_i_1_n_0\
    );
\EN2[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFF000800080008"
    )
        port map (
      I0 => \EN2[3]_i_2_n_0\,
      I1 => \EN2[0]_i_2_n_0\,
      I2 => \cnt_inner_valid_reg_n_0_[5]\,
      I3 => \cnt_inner_valid_reg_n_0_[4]\,
      I4 => \EN2[0]_i_3_n_0\,
      I5 => \EN2[6]_i_2_n_0\,
      O => \EN2[0]_i_1_n_0\
    );
\EN2[0]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7FFF"
    )
        port map (
      I0 => \cnt_inner_valid_reg_n_0_[1]\,
      I1 => \cnt_inner_valid_reg_n_0_[0]\,
      I2 => \cnt_inner_valid_reg_n_0_[3]\,
      I3 => \cnt_inner_valid_reg_n_0_[2]\,
      O => \EN2[0]_i_2_n_0\
    );
\EN2[0]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => active_mem2_in_read_state_reg(1),
      I1 => active_mem2_in_read_state_reg(2),
      O => \EN2[0]_i_3_n_0\
    );
\EN2[10]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4040FF4040404040"
    )
        port map (
      I0 => \cnt_inner_valid[15]_i_3_n_0\,
      I1 => \EN2[14]_i_2_n_0\,
      I2 => \EN2[15]_i_5_n_0\,
      I3 => active_mem2_in_read_state_reg(1),
      I4 => active_mem2_in_read_state_reg(2),
      I5 => \EN2[14]_i_3_n_0\,
      O => \EN2[10]_i_1_n_0\
    );
\EN2[11]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"40404040FF404040"
    )
        port map (
      I0 => \cnt_inner_valid[15]_i_3_n_0\,
      I1 => \EN2[15]_i_4_n_0\,
      I2 => \EN2[15]_i_5_n_0\,
      I3 => \EN2[15]_i_6_n_0\,
      I4 => active_mem2_in_read_state_reg(1),
      I5 => active_mem2_in_read_state_reg(2),
      O => \EN2[11]_i_1_n_0\
    );
\EN2[12]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8080FF8080808080"
    )
        port map (
      I0 => \EN2[12]_i_2_n_0\,
      I1 => \EN2[15]_i_5_n_0\,
      I2 => \cnt_inner_valid_reg_n_0_[7]\,
      I3 => active_mem2_in_read_state_reg(2),
      I4 => active_mem2_in_read_state_reg(1),
      I5 => \EN2[14]_i_3_n_0\,
      O => \EN2[12]_i_1_n_0\
    );
\EN2[12]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00041000"
    )
        port map (
      I0 => \EN2[12]_i_3_n_0\,
      I1 => \cnt_inner_valid_reg_n_0_[6]\,
      I2 => \cnt_inner_valid_reg_n_0_[5]\,
      I3 => \cnt_inner_valid_reg_n_0_[4]\,
      I4 => \EN2[0]_i_2_n_0\,
      O => \EN2[12]_i_2_n_0\
    );
\EN2[12]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => \cnt_inner_valid_reg_n_0_[11]\,
      I1 => \cnt_inner_valid_reg_n_0_[14]\,
      I2 => \cnt_inner_valid_reg_n_0_[15]\,
      I3 => \cnt_inner_valid_reg_n_0_[13]\,
      I4 => \cnt_inner_valid_reg_n_0_[12]\,
      O => \EN2[12]_i_3_n_0\
    );
\EN2[13]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"80808080FF808080"
    )
        port map (
      I0 => \EN2[15]_i_3_n_0\,
      I1 => \EN2[13]_i_2_n_0\,
      I2 => \EN2[15]_i_5_n_0\,
      I3 => \EN2[15]_i_6_n_0\,
      I4 => active_mem2_in_read_state_reg(2),
      I5 => active_mem2_in_read_state_reg(1),
      O => \EN2[13]_i_1_n_0\
    );
\EN2[13]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"000000006AAAAAAA"
    )
        port map (
      I0 => \cnt_inner_valid_reg_n_0_[4]\,
      I1 => \cnt_inner_valid_reg_n_0_[1]\,
      I2 => \cnt_inner_valid_reg_n_0_[0]\,
      I3 => \cnt_inner_valid_reg_n_0_[3]\,
      I4 => \cnt_inner_valid_reg_n_0_[2]\,
      I5 => \cnt_inner_valid_reg_n_0_[5]\,
      O => \EN2[13]_i_2_n_0\
    );
\EN2[14]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF80808080808080"
    )
        port map (
      I0 => \EN2[15]_i_3_n_0\,
      I1 => \EN2[14]_i_2_n_0\,
      I2 => \EN2[15]_i_5_n_0\,
      I3 => active_mem2_in_read_state_reg(1),
      I4 => active_mem2_in_read_state_reg(2),
      I5 => \EN2[14]_i_3_n_0\,
      O => \EN2[14]_i_1_n_0\
    );
\EN2[14]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00007FFF80000000"
    )
        port map (
      I0 => \cnt_inner_valid_reg_n_0_[1]\,
      I1 => \cnt_inner_valid_reg_n_0_[0]\,
      I2 => \cnt_inner_valid_reg_n_0_[3]\,
      I3 => \cnt_inner_valid_reg_n_0_[2]\,
      I4 => \cnt_inner_valid_reg_n_0_[4]\,
      I5 => \cnt_inner_valid_reg_n_0_[5]\,
      O => \EN2[14]_i_2_n_0\
    );
\EN2[14]_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => active_mem2_in_read_state_reg(0),
      I1 => \EN2[15]_i_8_n_0\,
      I2 => active_mem2_in_read_state_reg(3),
      O => \EN2[14]_i_3_n_0\
    );
\EN2[15]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => locked_EN2_reg_n_0,
      I1 => s00_axis_aresetn,
      I2 => s00_axis_tuser(11),
      O => \EN2[15]_i_1_n_0\
    );
\EN2[15]_i_10\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000004"
    )
        port map (
      I0 => active_mem2_in_read_state_reg(15),
      I1 => s00_axis_aresetn,
      I2 => s00_axis_tuser(11),
      I3 => active_mem2_in_read_state_reg(4),
      I4 => active_mem2_in_read_state_reg(6),
      I5 => active_mem2_in_read_state_reg(5),
      O => \EN2[15]_i_10_n_0\
    );
\EN2[15]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF80808080808080"
    )
        port map (
      I0 => \EN2[15]_i_3_n_0\,
      I1 => \EN2[15]_i_4_n_0\,
      I2 => \EN2[15]_i_5_n_0\,
      I3 => active_mem2_in_read_state_reg(1),
      I4 => active_mem2_in_read_state_reg(2),
      I5 => \EN2[15]_i_6_n_0\,
      O => \EN2[15]_i_2_n_0\
    );
\EN2[15]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \EN2[15]_i_7_n_0\,
      I1 => \cnt_inner_valid_reg_n_0_[7]\,
      O => \EN2[15]_i_3_n_0\
    );
\EN2[15]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6AAAAAAA00000000"
    )
        port map (
      I0 => \cnt_inner_valid_reg_n_0_[4]\,
      I1 => \cnt_inner_valid_reg_n_0_[1]\,
      I2 => \cnt_inner_valid_reg_n_0_[0]\,
      I3 => \cnt_inner_valid_reg_n_0_[3]\,
      I4 => \cnt_inner_valid_reg_n_0_[2]\,
      I5 => \cnt_inner_valid_reg_n_0_[5]\,
      O => \EN2[15]_i_4_n_0\
    );
\EN2[15]_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"01010001"
    )
        port map (
      I0 => \cnt_inner_valid_reg_n_0_[10]\,
      I1 => \cnt_inner_valid_reg_n_0_[8]\,
      I2 => \cnt_inner_valid_reg_n_0_[9]\,
      I3 => s00_axis_aresetn,
      I4 => s00_axis_tuser(11),
      O => \EN2[15]_i_5_n_0\
    );
\EN2[15]_i_6\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => active_mem2_in_read_state_reg(0),
      I1 => \EN2[15]_i_8_n_0\,
      I2 => active_mem2_in_read_state_reg(3),
      O => \EN2[15]_i_6_n_0\
    );
\EN2[15]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000002"
    )
        port map (
      I0 => \cnt_inner_valid_reg_n_0_[6]\,
      I1 => \cnt_inner_valid_reg_n_0_[12]\,
      I2 => \cnt_inner_valid_reg_n_0_[13]\,
      I3 => \cnt_inner_valid_reg_n_0_[15]\,
      I4 => \cnt_inner_valid_reg_n_0_[14]\,
      I5 => \cnt_inner_valid_reg_n_0_[11]\,
      O => \EN2[15]_i_7_n_0\
    );
\EN2[15]_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000200000000"
    )
        port map (
      I0 => \EN2[15]_i_9_n_0\,
      I1 => active_mem2_in_read_state_reg(10),
      I2 => active_mem2_in_read_state_reg(9),
      I3 => active_mem2_in_read_state_reg(8),
      I4 => active_mem2_in_read_state_reg(7),
      I5 => \EN2[15]_i_10_n_0\,
      O => \EN2[15]_i_8_n_0\
    );
\EN2[15]_i_9\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0001"
    )
        port map (
      I0 => active_mem2_in_read_state_reg(14),
      I1 => active_mem2_in_read_state_reg(13),
      I2 => active_mem2_in_read_state_reg(12),
      I3 => active_mem2_in_read_state_reg(11),
      O => \EN2[15]_i_9_n_0\
    );
\EN2[1]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"888F8888"
    )
        port map (
      I0 => \EN2[13]_i_2_n_0\,
      I1 => \EN2[3]_i_2_n_0\,
      I2 => active_mem2_in_read_state_reg(1),
      I3 => active_mem2_in_read_state_reg(2),
      I4 => \EN2[7]_i_3_n_0\,
      O => \EN2[1]_i_1_n_0\
    );
\EN2[2]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"88F88888"
    )
        port map (
      I0 => \EN2[14]_i_2_n_0\,
      I1 => \EN2[3]_i_2_n_0\,
      I2 => active_mem2_in_read_state_reg(1),
      I3 => active_mem2_in_read_state_reg(2),
      I4 => \EN2[6]_i_2_n_0\,
      O => \EN2[2]_i_1_n_0\
    );
\EN2[3]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"88F88888"
    )
        port map (
      I0 => \EN2[15]_i_4_n_0\,
      I1 => \EN2[3]_i_2_n_0\,
      I2 => active_mem2_in_read_state_reg(1),
      I3 => active_mem2_in_read_state_reg(2),
      I4 => \EN2[7]_i_3_n_0\,
      O => \EN2[3]_i_1_n_0\
    );
\EN2[3]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000002"
    )
        port map (
      I0 => \adr_read_reorder_data_mem1[10]_i_2_n_0\,
      I1 => \cnt_inner_valid_reg_n_0_[10]\,
      I2 => \cnt_inner_valid_reg_n_0_[8]\,
      I3 => \cnt_inner_valid_reg_n_0_[9]\,
      I4 => \cnt_inner_valid_reg_n_0_[7]\,
      I5 => \cnt_inner_valid[15]_i_5_n_0\,
      O => \EN2[3]_i_2_n_0\
    );
\EN2[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"88F88888"
    )
        port map (
      I0 => \EN2[12]_i_2_n_0\,
      I1 => \EN2[4]_i_2_n_0\,
      I2 => active_mem2_in_read_state_reg(2),
      I3 => active_mem2_in_read_state_reg(1),
      I4 => \EN2[6]_i_2_n_0\,
      O => \EN2[4]_i_1_n_0\
    );
\EN2[4]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0001000100000001"
    )
        port map (
      I0 => \cnt_inner_valid_reg_n_0_[7]\,
      I1 => \cnt_inner_valid_reg_n_0_[9]\,
      I2 => \cnt_inner_valid_reg_n_0_[8]\,
      I3 => \cnt_inner_valid_reg_n_0_[10]\,
      I4 => s00_axis_aresetn,
      I5 => s00_axis_tuser(11),
      O => \EN2[4]_i_2_n_0\
    );
\EN2[5]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"88F88888"
    )
        port map (
      I0 => \EN2[13]_i_2_n_0\,
      I1 => \EN2[7]_i_2_n_0\,
      I2 => active_mem2_in_read_state_reg(2),
      I3 => active_mem2_in_read_state_reg(1),
      I4 => \EN2[7]_i_3_n_0\,
      O => \EN2[5]_i_1_n_0\
    );
\EN2[6]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F8888888"
    )
        port map (
      I0 => \EN2[14]_i_2_n_0\,
      I1 => \EN2[7]_i_2_n_0\,
      I2 => active_mem2_in_read_state_reg(1),
      I3 => active_mem2_in_read_state_reg(2),
      I4 => \EN2[6]_i_2_n_0\,
      O => \EN2[6]_i_1_n_0\
    );
\EN2[6]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"04"
    )
        port map (
      I0 => active_mem2_in_read_state_reg(0),
      I1 => \EN2[15]_i_8_n_0\,
      I2 => active_mem2_in_read_state_reg(3),
      O => \EN2[6]_i_2_n_0\
    );
\EN2[7]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F8888888"
    )
        port map (
      I0 => \EN2[15]_i_4_n_0\,
      I1 => \EN2[7]_i_2_n_0\,
      I2 => active_mem2_in_read_state_reg(1),
      I3 => active_mem2_in_read_state_reg(2),
      I4 => \EN2[7]_i_3_n_0\,
      O => \EN2[7]_i_1_n_0\
    );
\EN2[7]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000200000000"
    )
        port map (
      I0 => \adr_read_reorder_data_mem1[10]_i_2_n_0\,
      I1 => \cnt_inner_valid_reg_n_0_[10]\,
      I2 => \cnt_inner_valid_reg_n_0_[8]\,
      I3 => \cnt_inner_valid_reg_n_0_[9]\,
      I4 => \cnt_inner_valid_reg_n_0_[7]\,
      I5 => \EN2[15]_i_7_n_0\,
      O => \EN2[7]_i_2_n_0\
    );
\EN2[7]_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => active_mem2_in_read_state_reg(0),
      I1 => \EN2[15]_i_8_n_0\,
      I2 => active_mem2_in_read_state_reg(3),
      O => \EN2[7]_i_3_n_0\
    );
\EN2[8]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"DDDDDDDD000D0000"
    )
        port map (
      I0 => locked_EN2_reg_n_0,
      I1 => s00_axis_tuser(11),
      I2 => active_mem2_in_read_state_reg(1),
      I3 => active_mem2_in_read_state_reg(2),
      I4 => \EN2[14]_i_3_n_0\,
      I5 => \EN2[8]_i_2_n_0\,
      O => \EN2[8]_i_1_n_0\
    );
\EN2[8]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000044F0000000"
    )
        port map (
      I0 => \cnt_inner_valid[15]_i_3_n_0\,
      I1 => \EN2[15]_i_5_n_0\,
      I2 => \EN2[7]_i_2_n_0\,
      I3 => \cnt_inner_valid_reg_n_0_[5]\,
      I4 => \cnt_inner_valid_reg_n_0_[4]\,
      I5 => \EN2[0]_i_2_n_0\,
      O => \EN2[8]_i_2_n_0\
    );
\EN2[9]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"404040404040FF40"
    )
        port map (
      I0 => \cnt_inner_valid[15]_i_3_n_0\,
      I1 => \EN2[13]_i_2_n_0\,
      I2 => \EN2[15]_i_5_n_0\,
      I3 => \EN2[15]_i_6_n_0\,
      I4 => active_mem2_in_read_state_reg(1),
      I5 => active_mem2_in_read_state_reg(2),
      O => \EN2[9]_i_1_n_0\
    );
\EN2_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => s00_axis_aresetn,
      D => \EN2[0]_i_1_n_0\,
      Q => \EN2_reg_n_0_[0]\,
      R => \EN2[15]_i_1_n_0\
    );
\EN2_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => s00_axis_aresetn,
      D => \EN2[10]_i_1_n_0\,
      Q => \EN2_reg_n_0_[10]\,
      R => \EN2[15]_i_1_n_0\
    );
\EN2_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => s00_axis_aresetn,
      D => \EN2[11]_i_1_n_0\,
      Q => \EN2_reg_n_0_[11]\,
      R => \EN2[15]_i_1_n_0\
    );
\EN2_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => s00_axis_aresetn,
      D => \EN2[12]_i_1_n_0\,
      Q => \EN2_reg_n_0_[12]\,
      R => \EN2[15]_i_1_n_0\
    );
\EN2_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => s00_axis_aresetn,
      D => \EN2[13]_i_1_n_0\,
      Q => \EN2_reg_n_0_[13]\,
      R => \EN2[15]_i_1_n_0\
    );
\EN2_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => s00_axis_aresetn,
      D => \EN2[14]_i_1_n_0\,
      Q => \EN2_reg_n_0_[14]\,
      R => \EN2[15]_i_1_n_0\
    );
\EN2_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => s00_axis_aresetn,
      D => \EN2[15]_i_2_n_0\,
      Q => \EN2_reg_n_0_[15]\,
      R => \EN2[15]_i_1_n_0\
    );
\EN2_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => s00_axis_aresetn,
      D => \EN2[1]_i_1_n_0\,
      Q => \EN2_reg_n_0_[1]\,
      R => \EN2[15]_i_1_n_0\
    );
\EN2_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => s00_axis_aresetn,
      D => \EN2[2]_i_1_n_0\,
      Q => \EN2_reg_n_0_[2]\,
      R => \EN2[15]_i_1_n_0\
    );
\EN2_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => s00_axis_aresetn,
      D => \EN2[3]_i_1_n_0\,
      Q => \EN2_reg_n_0_[3]\,
      R => \EN2[15]_i_1_n_0\
    );
\EN2_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => s00_axis_aresetn,
      D => \EN2[4]_i_1_n_0\,
      Q => \EN2_reg_n_0_[4]\,
      R => \EN2[15]_i_1_n_0\
    );
\EN2_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => s00_axis_aresetn,
      D => \EN2[5]_i_1_n_0\,
      Q => \EN2_reg_n_0_[5]\,
      R => \EN2[15]_i_1_n_0\
    );
\EN2_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => s00_axis_aresetn,
      D => \EN2[6]_i_1_n_0\,
      Q => \EN2_reg_n_0_[6]\,
      R => \EN2[15]_i_1_n_0\
    );
\EN2_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => s00_axis_aresetn,
      D => \EN2[7]_i_1_n_0\,
      Q => \EN2_reg_n_0_[7]\,
      R => \EN2[15]_i_1_n_0\
    );
\EN2_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => s00_axis_aresetn,
      D => \EN2[8]_i_1_n_0\,
      Q => \EN2_reg_n_0_[8]\,
      R => '0'
    );
\EN2_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => s00_axis_aresetn,
      D => \EN2[9]_i_1_n_0\,
      Q => \EN2_reg_n_0_[9]\,
      R => \EN2[15]_i_1_n_0\
    );
\active_mem1_in_read_state[0]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => s00_axis_tuser(11),
      I1 => \active_mem1_in_read_state_reg_n_0_[0]\,
      O => \active_mem1_in_read_state[0]_i_1_n_0\
    );
\active_mem1_in_read_state[10]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \active_mem1_in_read_state_reg[12]_i_2_n_6\,
      I1 => s00_axis_tuser(11),
      O => \active_mem1_in_read_state[10]_i_1_n_0\
    );
\active_mem1_in_read_state[11]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \active_mem1_in_read_state_reg[12]_i_2_n_5\,
      I1 => s00_axis_tuser(11),
      O => \active_mem1_in_read_state[11]_i_1_n_0\
    );
\active_mem1_in_read_state[12]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \active_mem1_in_read_state_reg[12]_i_2_n_4\,
      I1 => s00_axis_tuser(11),
      O => \active_mem1_in_read_state[12]_i_1_n_0\
    );
\active_mem1_in_read_state[13]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \active_mem1_in_read_state_reg[15]_i_3_n_7\,
      I1 => s00_axis_tuser(11),
      O => \active_mem1_in_read_state[13]_i_1_n_0\
    );
\active_mem1_in_read_state[14]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \active_mem1_in_read_state_reg[15]_i_3_n_6\,
      I1 => s00_axis_tuser(11),
      O => \active_mem1_in_read_state[14]_i_1_n_0\
    );
\active_mem1_in_read_state[15]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00010000FFFF0000"
    )
        port map (
      I0 => \adr_read_reorder_data_mem1[5]_i_2_n_0\,
      I1 => \adr_read_reorder_data_mem1_reg_n_0_[4]\,
      I2 => \adr_read_reorder_data_mem1_reg_n_0_[5]\,
      I3 => \adr_read_reorder_data_mem1[6]_i_3_n_0\,
      I4 => s00_axis_aresetn,
      I5 => s00_axis_tuser(11),
      O => \active_mem1_in_read_state[15]_i_1_n_0\
    );
\active_mem1_in_read_state[15]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \active_mem1_in_read_state_reg[15]_i_3_n_5\,
      I1 => s00_axis_tuser(11),
      O => \active_mem1_in_read_state[15]_i_2_n_0\
    );
\active_mem1_in_read_state[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \active_mem1_in_read_state_reg[4]_i_2_n_7\,
      I1 => s00_axis_tuser(11),
      O => \active_mem1_in_read_state[1]_i_1_n_0\
    );
\active_mem1_in_read_state[2]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \active_mem1_in_read_state_reg[4]_i_2_n_6\,
      I1 => s00_axis_tuser(11),
      O => \active_mem1_in_read_state[2]_i_1_n_0\
    );
\active_mem1_in_read_state[3]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \active_mem1_in_read_state_reg[4]_i_2_n_5\,
      I1 => s00_axis_tuser(11),
      O => \active_mem1_in_read_state[3]_i_1_n_0\
    );
\active_mem1_in_read_state[4]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \active_mem1_in_read_state_reg[4]_i_2_n_4\,
      I1 => s00_axis_tuser(11),
      O => \active_mem1_in_read_state[4]_i_1_n_0\
    );
\active_mem1_in_read_state[5]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \active_mem1_in_read_state_reg[8]_i_2_n_7\,
      I1 => s00_axis_tuser(11),
      O => \active_mem1_in_read_state[5]_i_1_n_0\
    );
\active_mem1_in_read_state[6]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \active_mem1_in_read_state_reg[8]_i_2_n_6\,
      I1 => s00_axis_tuser(11),
      O => \active_mem1_in_read_state[6]_i_1_n_0\
    );
\active_mem1_in_read_state[7]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \active_mem1_in_read_state_reg[8]_i_2_n_5\,
      I1 => s00_axis_tuser(11),
      O => \active_mem1_in_read_state[7]_i_1_n_0\
    );
\active_mem1_in_read_state[8]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \active_mem1_in_read_state_reg[8]_i_2_n_4\,
      I1 => s00_axis_tuser(11),
      O => \active_mem1_in_read_state[8]_i_1_n_0\
    );
\active_mem1_in_read_state[9]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \active_mem1_in_read_state_reg[12]_i_2_n_7\,
      I1 => s00_axis_tuser(11),
      O => \active_mem1_in_read_state[9]_i_1_n_0\
    );
\active_mem1_in_read_state_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \active_mem1_in_read_state[15]_i_1_n_0\,
      D => \active_mem1_in_read_state[0]_i_1_n_0\,
      Q => \active_mem1_in_read_state_reg_n_0_[0]\,
      R => '0'
    );
\active_mem1_in_read_state_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \active_mem1_in_read_state[15]_i_1_n_0\,
      D => \active_mem1_in_read_state[10]_i_1_n_0\,
      Q => \active_mem1_in_read_state_reg_n_0_[10]\,
      R => '0'
    );
\active_mem1_in_read_state_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \active_mem1_in_read_state[15]_i_1_n_0\,
      D => \active_mem1_in_read_state[11]_i_1_n_0\,
      Q => \active_mem1_in_read_state_reg_n_0_[11]\,
      R => '0'
    );
\active_mem1_in_read_state_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \active_mem1_in_read_state[15]_i_1_n_0\,
      D => \active_mem1_in_read_state[12]_i_1_n_0\,
      Q => \active_mem1_in_read_state_reg_n_0_[12]\,
      R => '0'
    );
\active_mem1_in_read_state_reg[12]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => \active_mem1_in_read_state_reg[8]_i_2_n_0\,
      CO(3) => \active_mem1_in_read_state_reg[12]_i_2_n_0\,
      CO(2) => \active_mem1_in_read_state_reg[12]_i_2_n_1\,
      CO(1) => \active_mem1_in_read_state_reg[12]_i_2_n_2\,
      CO(0) => \active_mem1_in_read_state_reg[12]_i_2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \active_mem1_in_read_state_reg[12]_i_2_n_4\,
      O(2) => \active_mem1_in_read_state_reg[12]_i_2_n_5\,
      O(1) => \active_mem1_in_read_state_reg[12]_i_2_n_6\,
      O(0) => \active_mem1_in_read_state_reg[12]_i_2_n_7\,
      S(3) => \active_mem1_in_read_state_reg_n_0_[12]\,
      S(2) => \active_mem1_in_read_state_reg_n_0_[11]\,
      S(1) => \active_mem1_in_read_state_reg_n_0_[10]\,
      S(0) => \active_mem1_in_read_state_reg_n_0_[9]\
    );
\active_mem1_in_read_state_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \active_mem1_in_read_state[15]_i_1_n_0\,
      D => \active_mem1_in_read_state[13]_i_1_n_0\,
      Q => \active_mem1_in_read_state_reg_n_0_[13]\,
      R => '0'
    );
\active_mem1_in_read_state_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \active_mem1_in_read_state[15]_i_1_n_0\,
      D => \active_mem1_in_read_state[14]_i_1_n_0\,
      Q => \active_mem1_in_read_state_reg_n_0_[14]\,
      R => '0'
    );
\active_mem1_in_read_state_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \active_mem1_in_read_state[15]_i_1_n_0\,
      D => \active_mem1_in_read_state[15]_i_2_n_0\,
      Q => \active_mem1_in_read_state_reg_n_0_[15]\,
      R => '0'
    );
\active_mem1_in_read_state_reg[15]_i_3\: unisim.vcomponents.CARRY4
     port map (
      CI => \active_mem1_in_read_state_reg[12]_i_2_n_0\,
      CO(3 downto 2) => \NLW_active_mem1_in_read_state_reg[15]_i_3_CO_UNCONNECTED\(3 downto 2),
      CO(1) => \active_mem1_in_read_state_reg[15]_i_3_n_2\,
      CO(0) => \active_mem1_in_read_state_reg[15]_i_3_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \NLW_active_mem1_in_read_state_reg[15]_i_3_O_UNCONNECTED\(3),
      O(2) => \active_mem1_in_read_state_reg[15]_i_3_n_5\,
      O(1) => \active_mem1_in_read_state_reg[15]_i_3_n_6\,
      O(0) => \active_mem1_in_read_state_reg[15]_i_3_n_7\,
      S(3) => '0',
      S(2) => \active_mem1_in_read_state_reg_n_0_[15]\,
      S(1) => \active_mem1_in_read_state_reg_n_0_[14]\,
      S(0) => \active_mem1_in_read_state_reg_n_0_[13]\
    );
\active_mem1_in_read_state_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \active_mem1_in_read_state[15]_i_1_n_0\,
      D => \active_mem1_in_read_state[1]_i_1_n_0\,
      Q => \active_mem1_in_read_state_reg_n_0_[1]\,
      R => '0'
    );
\active_mem1_in_read_state_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \active_mem1_in_read_state[15]_i_1_n_0\,
      D => \active_mem1_in_read_state[2]_i_1_n_0\,
      Q => \active_mem1_in_read_state_reg_n_0_[2]\,
      R => '0'
    );
\active_mem1_in_read_state_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \active_mem1_in_read_state[15]_i_1_n_0\,
      D => \active_mem1_in_read_state[3]_i_1_n_0\,
      Q => \active_mem1_in_read_state_reg_n_0_[3]\,
      R => '0'
    );
\active_mem1_in_read_state_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \active_mem1_in_read_state[15]_i_1_n_0\,
      D => \active_mem1_in_read_state[4]_i_1_n_0\,
      Q => \active_mem1_in_read_state_reg_n_0_[4]\,
      R => '0'
    );
\active_mem1_in_read_state_reg[4]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \active_mem1_in_read_state_reg[4]_i_2_n_0\,
      CO(2) => \active_mem1_in_read_state_reg[4]_i_2_n_1\,
      CO(1) => \active_mem1_in_read_state_reg[4]_i_2_n_2\,
      CO(0) => \active_mem1_in_read_state_reg[4]_i_2_n_3\,
      CYINIT => \active_mem1_in_read_state_reg_n_0_[0]\,
      DI(3 downto 0) => B"0000",
      O(3) => \active_mem1_in_read_state_reg[4]_i_2_n_4\,
      O(2) => \active_mem1_in_read_state_reg[4]_i_2_n_5\,
      O(1) => \active_mem1_in_read_state_reg[4]_i_2_n_6\,
      O(0) => \active_mem1_in_read_state_reg[4]_i_2_n_7\,
      S(3) => \active_mem1_in_read_state_reg_n_0_[4]\,
      S(2) => \active_mem1_in_read_state_reg_n_0_[3]\,
      S(1) => \active_mem1_in_read_state_reg_n_0_[2]\,
      S(0) => \active_mem1_in_read_state_reg_n_0_[1]\
    );
\active_mem1_in_read_state_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \active_mem1_in_read_state[15]_i_1_n_0\,
      D => \active_mem1_in_read_state[5]_i_1_n_0\,
      Q => \active_mem1_in_read_state_reg_n_0_[5]\,
      R => '0'
    );
\active_mem1_in_read_state_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \active_mem1_in_read_state[15]_i_1_n_0\,
      D => \active_mem1_in_read_state[6]_i_1_n_0\,
      Q => \active_mem1_in_read_state_reg_n_0_[6]\,
      R => '0'
    );
\active_mem1_in_read_state_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \active_mem1_in_read_state[15]_i_1_n_0\,
      D => \active_mem1_in_read_state[7]_i_1_n_0\,
      Q => \active_mem1_in_read_state_reg_n_0_[7]\,
      R => '0'
    );
\active_mem1_in_read_state_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \active_mem1_in_read_state[15]_i_1_n_0\,
      D => \active_mem1_in_read_state[8]_i_1_n_0\,
      Q => \active_mem1_in_read_state_reg_n_0_[8]\,
      R => '0'
    );
\active_mem1_in_read_state_reg[8]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => \active_mem1_in_read_state_reg[4]_i_2_n_0\,
      CO(3) => \active_mem1_in_read_state_reg[8]_i_2_n_0\,
      CO(2) => \active_mem1_in_read_state_reg[8]_i_2_n_1\,
      CO(1) => \active_mem1_in_read_state_reg[8]_i_2_n_2\,
      CO(0) => \active_mem1_in_read_state_reg[8]_i_2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \active_mem1_in_read_state_reg[8]_i_2_n_4\,
      O(2) => \active_mem1_in_read_state_reg[8]_i_2_n_5\,
      O(1) => \active_mem1_in_read_state_reg[8]_i_2_n_6\,
      O(0) => \active_mem1_in_read_state_reg[8]_i_2_n_7\,
      S(3) => \active_mem1_in_read_state_reg_n_0_[8]\,
      S(2) => \active_mem1_in_read_state_reg_n_0_[7]\,
      S(1) => \active_mem1_in_read_state_reg_n_0_[6]\,
      S(0) => \active_mem1_in_read_state_reg_n_0_[5]\
    );
\active_mem1_in_read_state_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \active_mem1_in_read_state[15]_i_1_n_0\,
      D => \active_mem1_in_read_state[9]_i_1_n_0\,
      Q => \active_mem1_in_read_state_reg_n_0_[9]\,
      R => '0'
    );
\active_mem2_in_read_state[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000100000000"
    )
        port map (
      I0 => \adr_read_reorder_data_mem2_reg_n_0_[4]\,
      I1 => \adr_read_reorder_data_mem2_reg_n_0_[5]\,
      I2 => \adr_read_reorder_data_mem2_reg_n_0_[3]\,
      I3 => \adr_read_reorder_data_mem2_reg_n_0_[2]\,
      I4 => \adr_read_reorder_data_mem1[10]_i_2_n_0\,
      I5 => \adr_read_reorder_data_mem2[5]_i_3_n_0\,
      O => \active_mem2_in_read_state[0]_i_1_n_0\
    );
\active_mem2_in_read_state[0]_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => active_mem2_in_read_state_reg(0),
      O => \active_mem2_in_read_state[0]_i_3_n_0\
    );
\active_mem2_in_read_state_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \active_mem2_in_read_state[0]_i_1_n_0\,
      D => \active_mem2_in_read_state_reg[0]_i_2_n_7\,
      Q => active_mem2_in_read_state_reg(0),
      R => \adr_read_reorder_data_mem2[10]_i_1_n_0\
    );
\active_mem2_in_read_state_reg[0]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \active_mem2_in_read_state_reg[0]_i_2_n_0\,
      CO(2) => \active_mem2_in_read_state_reg[0]_i_2_n_1\,
      CO(1) => \active_mem2_in_read_state_reg[0]_i_2_n_2\,
      CO(0) => \active_mem2_in_read_state_reg[0]_i_2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0001",
      O(3) => \active_mem2_in_read_state_reg[0]_i_2_n_4\,
      O(2) => \active_mem2_in_read_state_reg[0]_i_2_n_5\,
      O(1) => \active_mem2_in_read_state_reg[0]_i_2_n_6\,
      O(0) => \active_mem2_in_read_state_reg[0]_i_2_n_7\,
      S(3 downto 1) => active_mem2_in_read_state_reg(3 downto 1),
      S(0) => \active_mem2_in_read_state[0]_i_3_n_0\
    );
\active_mem2_in_read_state_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \active_mem2_in_read_state[0]_i_1_n_0\,
      D => \active_mem2_in_read_state_reg[8]_i_1_n_5\,
      Q => active_mem2_in_read_state_reg(10),
      R => \adr_read_reorder_data_mem2[10]_i_1_n_0\
    );
\active_mem2_in_read_state_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \active_mem2_in_read_state[0]_i_1_n_0\,
      D => \active_mem2_in_read_state_reg[8]_i_1_n_4\,
      Q => active_mem2_in_read_state_reg(11),
      R => \adr_read_reorder_data_mem2[10]_i_1_n_0\
    );
\active_mem2_in_read_state_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \active_mem2_in_read_state[0]_i_1_n_0\,
      D => \active_mem2_in_read_state_reg[12]_i_1_n_7\,
      Q => active_mem2_in_read_state_reg(12),
      R => \adr_read_reorder_data_mem2[10]_i_1_n_0\
    );
\active_mem2_in_read_state_reg[12]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \active_mem2_in_read_state_reg[8]_i_1_n_0\,
      CO(3) => \NLW_active_mem2_in_read_state_reg[12]_i_1_CO_UNCONNECTED\(3),
      CO(2) => \active_mem2_in_read_state_reg[12]_i_1_n_1\,
      CO(1) => \active_mem2_in_read_state_reg[12]_i_1_n_2\,
      CO(0) => \active_mem2_in_read_state_reg[12]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \active_mem2_in_read_state_reg[12]_i_1_n_4\,
      O(2) => \active_mem2_in_read_state_reg[12]_i_1_n_5\,
      O(1) => \active_mem2_in_read_state_reg[12]_i_1_n_6\,
      O(0) => \active_mem2_in_read_state_reg[12]_i_1_n_7\,
      S(3 downto 0) => active_mem2_in_read_state_reg(15 downto 12)
    );
\active_mem2_in_read_state_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \active_mem2_in_read_state[0]_i_1_n_0\,
      D => \active_mem2_in_read_state_reg[12]_i_1_n_6\,
      Q => active_mem2_in_read_state_reg(13),
      R => \adr_read_reorder_data_mem2[10]_i_1_n_0\
    );
\active_mem2_in_read_state_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \active_mem2_in_read_state[0]_i_1_n_0\,
      D => \active_mem2_in_read_state_reg[12]_i_1_n_5\,
      Q => active_mem2_in_read_state_reg(14),
      R => \adr_read_reorder_data_mem2[10]_i_1_n_0\
    );
\active_mem2_in_read_state_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \active_mem2_in_read_state[0]_i_1_n_0\,
      D => \active_mem2_in_read_state_reg[12]_i_1_n_4\,
      Q => active_mem2_in_read_state_reg(15),
      R => \adr_read_reorder_data_mem2[10]_i_1_n_0\
    );
\active_mem2_in_read_state_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \active_mem2_in_read_state[0]_i_1_n_0\,
      D => \active_mem2_in_read_state_reg[0]_i_2_n_6\,
      Q => active_mem2_in_read_state_reg(1),
      R => \adr_read_reorder_data_mem2[10]_i_1_n_0\
    );
\active_mem2_in_read_state_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \active_mem2_in_read_state[0]_i_1_n_0\,
      D => \active_mem2_in_read_state_reg[0]_i_2_n_5\,
      Q => active_mem2_in_read_state_reg(2),
      R => \adr_read_reorder_data_mem2[10]_i_1_n_0\
    );
\active_mem2_in_read_state_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \active_mem2_in_read_state[0]_i_1_n_0\,
      D => \active_mem2_in_read_state_reg[0]_i_2_n_4\,
      Q => active_mem2_in_read_state_reg(3),
      R => \adr_read_reorder_data_mem2[10]_i_1_n_0\
    );
\active_mem2_in_read_state_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \active_mem2_in_read_state[0]_i_1_n_0\,
      D => \active_mem2_in_read_state_reg[4]_i_1_n_7\,
      Q => active_mem2_in_read_state_reg(4),
      R => \adr_read_reorder_data_mem2[10]_i_1_n_0\
    );
\active_mem2_in_read_state_reg[4]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \active_mem2_in_read_state_reg[0]_i_2_n_0\,
      CO(3) => \active_mem2_in_read_state_reg[4]_i_1_n_0\,
      CO(2) => \active_mem2_in_read_state_reg[4]_i_1_n_1\,
      CO(1) => \active_mem2_in_read_state_reg[4]_i_1_n_2\,
      CO(0) => \active_mem2_in_read_state_reg[4]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \active_mem2_in_read_state_reg[4]_i_1_n_4\,
      O(2) => \active_mem2_in_read_state_reg[4]_i_1_n_5\,
      O(1) => \active_mem2_in_read_state_reg[4]_i_1_n_6\,
      O(0) => \active_mem2_in_read_state_reg[4]_i_1_n_7\,
      S(3 downto 0) => active_mem2_in_read_state_reg(7 downto 4)
    );
\active_mem2_in_read_state_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \active_mem2_in_read_state[0]_i_1_n_0\,
      D => \active_mem2_in_read_state_reg[4]_i_1_n_6\,
      Q => active_mem2_in_read_state_reg(5),
      R => \adr_read_reorder_data_mem2[10]_i_1_n_0\
    );
\active_mem2_in_read_state_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \active_mem2_in_read_state[0]_i_1_n_0\,
      D => \active_mem2_in_read_state_reg[4]_i_1_n_5\,
      Q => active_mem2_in_read_state_reg(6),
      R => \adr_read_reorder_data_mem2[10]_i_1_n_0\
    );
\active_mem2_in_read_state_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \active_mem2_in_read_state[0]_i_1_n_0\,
      D => \active_mem2_in_read_state_reg[4]_i_1_n_4\,
      Q => active_mem2_in_read_state_reg(7),
      R => \adr_read_reorder_data_mem2[10]_i_1_n_0\
    );
\active_mem2_in_read_state_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \active_mem2_in_read_state[0]_i_1_n_0\,
      D => \active_mem2_in_read_state_reg[8]_i_1_n_7\,
      Q => active_mem2_in_read_state_reg(8),
      R => \adr_read_reorder_data_mem2[10]_i_1_n_0\
    );
\active_mem2_in_read_state_reg[8]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \active_mem2_in_read_state_reg[4]_i_1_n_0\,
      CO(3) => \active_mem2_in_read_state_reg[8]_i_1_n_0\,
      CO(2) => \active_mem2_in_read_state_reg[8]_i_1_n_1\,
      CO(1) => \active_mem2_in_read_state_reg[8]_i_1_n_2\,
      CO(0) => \active_mem2_in_read_state_reg[8]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \active_mem2_in_read_state_reg[8]_i_1_n_4\,
      O(2) => \active_mem2_in_read_state_reg[8]_i_1_n_5\,
      O(1) => \active_mem2_in_read_state_reg[8]_i_1_n_6\,
      O(0) => \active_mem2_in_read_state_reg[8]_i_1_n_7\,
      S(3 downto 0) => active_mem2_in_read_state_reg(11 downto 8)
    );
\active_mem2_in_read_state_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \active_mem2_in_read_state[0]_i_1_n_0\,
      D => \active_mem2_in_read_state_reg[8]_i_1_n_6\,
      Q => active_mem2_in_read_state_reg(9),
      R => \adr_read_reorder_data_mem2[10]_i_1_n_0\
    );
\adr_read_reorder_data_mem1[0]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"7F"
    )
        port map (
      I0 => s00_axis_tuser(11),
      I1 => s00_axis_aresetn,
      I2 => \adr_read_reorder_data_mem1_reg_n_0_[0]\,
      O => \adr_read_reorder_data_mem1[0]_i_1_n_0\
    );
\adr_read_reorder_data_mem1[10]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => s00_axis_aresetn,
      I1 => s00_axis_tuser(11),
      O => \adr_read_reorder_data_mem1[10]_i_1_n_0\
    );
\adr_read_reorder_data_mem1[10]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => s00_axis_tuser(11),
      I1 => s00_axis_aresetn,
      O => \adr_read_reorder_data_mem1[10]_i_2_n_0\
    );
\adr_read_reorder_data_mem1[10]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFF9AAAAAAA"
    )
        port map (
      I0 => \adr_read_reorder_data_mem1_reg_n_0_[10]\,
      I1 => \adr_read_reorder_data_mem1[10]_i_4_n_0\,
      I2 => \adr_read_reorder_data_mem1_reg_n_0_[9]\,
      I3 => \adr_read_reorder_data_mem1_reg_n_0_[7]\,
      I4 => \adr_read_reorder_data_mem1_reg_n_0_[8]\,
      I5 => \adr_read_reorder_data_mem2[10]_i_2_n_0\,
      O => \adr_read_reorder_data_mem1[10]_i_3_n_0\
    );
\adr_read_reorder_data_mem1[10]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => \adr_read_reorder_data_mem1[6]_i_4_n_0\,
      I1 => \adr_read_reorder_data_mem1_reg_n_0_[6]\,
      O => \adr_read_reorder_data_mem1[10]_i_4_n_0\
    );
\adr_read_reorder_data_mem1[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BBBBEEEEBBBBEEEF"
    )
        port map (
      I0 => \adr_read_reorder_data_mem2[10]_i_2_n_0\,
      I1 => \adr_read_reorder_data_mem1_reg_n_0_[0]\,
      I2 => \adr_read_reorder_data_mem1[3]_i_2_n_0\,
      I3 => \adr_read_reorder_data_mem1_reg_n_0_[3]\,
      I4 => \adr_read_reorder_data_mem1_reg_n_0_[1]\,
      I5 => \adr_read_reorder_data_mem1_reg_n_0_[2]\,
      O => \adr_read_reorder_data_mem1[1]_i_1_n_0\
    );
\adr_read_reorder_data_mem1[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BBBBEEEEEEEEEEEF"
    )
        port map (
      I0 => \adr_read_reorder_data_mem2[10]_i_2_n_0\,
      I1 => \adr_read_reorder_data_mem1_reg_n_0_[2]\,
      I2 => \adr_read_reorder_data_mem1[3]_i_2_n_0\,
      I3 => \adr_read_reorder_data_mem1_reg_n_0_[3]\,
      I4 => \adr_read_reorder_data_mem1_reg_n_0_[1]\,
      I5 => \adr_read_reorder_data_mem1_reg_n_0_[0]\,
      O => \adr_read_reorder_data_mem1[2]_i_1_n_0\
    );
\adr_read_reorder_data_mem1[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BBEEEEEEEEEEEEEF"
    )
        port map (
      I0 => \adr_read_reorder_data_mem2[10]_i_2_n_0\,
      I1 => \adr_read_reorder_data_mem1_reg_n_0_[3]\,
      I2 => \adr_read_reorder_data_mem1[3]_i_2_n_0\,
      I3 => \adr_read_reorder_data_mem1_reg_n_0_[2]\,
      I4 => \adr_read_reorder_data_mem1_reg_n_0_[0]\,
      I5 => \adr_read_reorder_data_mem1_reg_n_0_[1]\,
      O => \adr_read_reorder_data_mem1[3]_i_1_n_0\
    );
\adr_read_reorder_data_mem1[3]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FE"
    )
        port map (
      I0 => \adr_read_reorder_data_mem1_reg_n_0_[5]\,
      I1 => \adr_read_reorder_data_mem1_reg_n_0_[4]\,
      I2 => \adr_read_reorder_data_mem1[5]_i_2_n_0\,
      O => \adr_read_reorder_data_mem1[3]_i_2_n_0\
    );
\adr_read_reorder_data_mem1[4]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EBEBEBEBEBEBEBFF"
    )
        port map (
      I0 => \adr_read_reorder_data_mem2[10]_i_2_n_0\,
      I1 => \adr_read_reorder_data_mem1[4]_i_2_n_0\,
      I2 => \adr_read_reorder_data_mem1_reg_n_0_[4]\,
      I3 => \adr_read_reorder_data_mem1[5]_i_2_n_0\,
      I4 => \adr_read_reorder_data_mem1[6]_i_3_n_0\,
      I5 => \adr_read_reorder_data_mem1_reg_n_0_[5]\,
      O => \adr_read_reorder_data_mem1[4]_i_1_n_0\
    );
\adr_read_reorder_data_mem1[4]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7FFF"
    )
        port map (
      I0 => \adr_read_reorder_data_mem1_reg_n_0_[2]\,
      I1 => \adr_read_reorder_data_mem1_reg_n_0_[0]\,
      I2 => \adr_read_reorder_data_mem1_reg_n_0_[1]\,
      I3 => \adr_read_reorder_data_mem1_reg_n_0_[3]\,
      O => \adr_read_reorder_data_mem1[4]_i_2_n_0\
    );
\adr_read_reorder_data_mem1[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFAAABAAABFFFF"
    )
        port map (
      I0 => \adr_read_reorder_data_mem2[10]_i_2_n_0\,
      I1 => \adr_read_reorder_data_mem1_reg_n_0_[4]\,
      I2 => \adr_read_reorder_data_mem1[6]_i_3_n_0\,
      I3 => \adr_read_reorder_data_mem1[5]_i_2_n_0\,
      I4 => \adr_read_reorder_data_mem1_reg_n_0_[5]\,
      I5 => \adr_read_reorder_data_mem1[5]_i_3_n_0\,
      O => \adr_read_reorder_data_mem1[5]_i_1_n_0\
    );
\adr_read_reorder_data_mem1[5]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF7FFF"
    )
        port map (
      I0 => \adr_read_reorder_data_mem1_reg_n_0_[10]\,
      I1 => \adr_read_reorder_data_mem1_reg_n_0_[8]\,
      I2 => \adr_read_reorder_data_mem1_reg_n_0_[7]\,
      I3 => \adr_read_reorder_data_mem1_reg_n_0_[9]\,
      I4 => \adr_read_reorder_data_mem1_reg_n_0_[6]\,
      O => \adr_read_reorder_data_mem1[5]_i_2_n_0\
    );
\adr_read_reorder_data_mem1[5]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFFFFFF"
    )
        port map (
      I0 => \adr_read_reorder_data_mem1_reg_n_0_[3]\,
      I1 => \adr_read_reorder_data_mem1_reg_n_0_[1]\,
      I2 => \adr_read_reorder_data_mem1_reg_n_0_[0]\,
      I3 => \adr_read_reorder_data_mem1_reg_n_0_[2]\,
      I4 => \adr_read_reorder_data_mem1_reg_n_0_[4]\,
      O => \adr_read_reorder_data_mem1[5]_i_3_n_0\
    );
\adr_read_reorder_data_mem1[6]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFAAABAAABFFFF"
    )
        port map (
      I0 => \adr_read_reorder_data_mem2[10]_i_2_n_0\,
      I1 => \adr_read_reorder_data_mem1[6]_i_2_n_0\,
      I2 => \adr_read_reorder_data_mem1[6]_i_3_n_0\,
      I3 => m00_axis_tvalid_r_i_2_n_0,
      I4 => \adr_read_reorder_data_mem1_reg_n_0_[6]\,
      I5 => \adr_read_reorder_data_mem1[6]_i_4_n_0\,
      O => \adr_read_reorder_data_mem1[6]_i_1_n_0\
    );
\adr_read_reorder_data_mem1[6]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => \adr_read_reorder_data_mem1_reg_n_0_[4]\,
      I1 => \adr_read_reorder_data_mem1_reg_n_0_[5]\,
      O => \adr_read_reorder_data_mem1[6]_i_2_n_0\
    );
\adr_read_reorder_data_mem1[6]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => \adr_read_reorder_data_mem1_reg_n_0_[2]\,
      I1 => \adr_read_reorder_data_mem1_reg_n_0_[0]\,
      I2 => \adr_read_reorder_data_mem1_reg_n_0_[3]\,
      I3 => \adr_read_reorder_data_mem1_reg_n_0_[1]\,
      O => \adr_read_reorder_data_mem1[6]_i_3_n_0\
    );
\adr_read_reorder_data_mem1[6]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFFFFFFFFFF"
    )
        port map (
      I0 => \adr_read_reorder_data_mem1_reg_n_0_[4]\,
      I1 => \adr_read_reorder_data_mem1_reg_n_0_[2]\,
      I2 => \adr_read_reorder_data_mem1_reg_n_0_[0]\,
      I3 => \adr_read_reorder_data_mem1_reg_n_0_[1]\,
      I4 => \adr_read_reorder_data_mem1_reg_n_0_[3]\,
      I5 => \adr_read_reorder_data_mem1_reg_n_0_[5]\,
      O => \adr_read_reorder_data_mem1[6]_i_4_n_0\
    );
\adr_read_reorder_data_mem1[7]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9FFF"
    )
        port map (
      I0 => \adr_read_reorder_data_mem1_reg_n_0_[7]\,
      I1 => \adr_read_reorder_data_mem1[10]_i_4_n_0\,
      I2 => s00_axis_tuser(11),
      I3 => s00_axis_aresetn,
      O => \adr_read_reorder_data_mem1[7]_i_1_n_0\
    );
\adr_read_reorder_data_mem1[8]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"9AFFFFFF"
    )
        port map (
      I0 => \adr_read_reorder_data_mem1_reg_n_0_[8]\,
      I1 => \adr_read_reorder_data_mem1[10]_i_4_n_0\,
      I2 => \adr_read_reorder_data_mem1_reg_n_0_[7]\,
      I3 => s00_axis_tuser(11),
      I4 => s00_axis_aresetn,
      O => \adr_read_reorder_data_mem1[8]_i_1_n_0\
    );
\adr_read_reorder_data_mem1[9]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9AAAFFFFFFFFFFFF"
    )
        port map (
      I0 => \adr_read_reorder_data_mem1_reg_n_0_[9]\,
      I1 => \adr_read_reorder_data_mem1[10]_i_4_n_0\,
      I2 => \adr_read_reorder_data_mem1_reg_n_0_[8]\,
      I3 => \adr_read_reorder_data_mem1_reg_n_0_[7]\,
      I4 => s00_axis_tuser(11),
      I5 => s00_axis_aresetn,
      O => \adr_read_reorder_data_mem1[9]_i_1_n_0\
    );
\adr_read_reorder_data_mem1_reg[0]\: unisim.vcomponents.FDSE
     port map (
      C => m00_axis_aclk,
      CE => \adr_read_reorder_data_mem1[10]_i_2_n_0\,
      D => \adr_read_reorder_data_mem1[0]_i_1_n_0\,
      Q => \adr_read_reorder_data_mem1_reg_n_0_[0]\,
      S => \adr_read_reorder_data_mem1[10]_i_1_n_0\
    );
\adr_read_reorder_data_mem1_reg[10]\: unisim.vcomponents.FDSE
     port map (
      C => m00_axis_aclk,
      CE => \adr_read_reorder_data_mem1[10]_i_2_n_0\,
      D => \adr_read_reorder_data_mem1[10]_i_3_n_0\,
      Q => \adr_read_reorder_data_mem1_reg_n_0_[10]\,
      S => \adr_read_reorder_data_mem1[10]_i_1_n_0\
    );
\adr_read_reorder_data_mem1_reg[1]\: unisim.vcomponents.FDSE
     port map (
      C => m00_axis_aclk,
      CE => \adr_read_reorder_data_mem1[10]_i_2_n_0\,
      D => \adr_read_reorder_data_mem1[1]_i_1_n_0\,
      Q => \adr_read_reorder_data_mem1_reg_n_0_[1]\,
      S => \adr_read_reorder_data_mem1[10]_i_1_n_0\
    );
\adr_read_reorder_data_mem1_reg[2]\: unisim.vcomponents.FDSE
     port map (
      C => m00_axis_aclk,
      CE => \adr_read_reorder_data_mem1[10]_i_2_n_0\,
      D => \adr_read_reorder_data_mem1[2]_i_1_n_0\,
      Q => \adr_read_reorder_data_mem1_reg_n_0_[2]\,
      S => \adr_read_reorder_data_mem1[10]_i_1_n_0\
    );
\adr_read_reorder_data_mem1_reg[3]\: unisim.vcomponents.FDSE
     port map (
      C => m00_axis_aclk,
      CE => \adr_read_reorder_data_mem1[10]_i_2_n_0\,
      D => \adr_read_reorder_data_mem1[3]_i_1_n_0\,
      Q => \adr_read_reorder_data_mem1_reg_n_0_[3]\,
      S => \adr_read_reorder_data_mem1[10]_i_1_n_0\
    );
\adr_read_reorder_data_mem1_reg[4]\: unisim.vcomponents.FDSE
     port map (
      C => m00_axis_aclk,
      CE => \adr_read_reorder_data_mem1[10]_i_2_n_0\,
      D => \adr_read_reorder_data_mem1[4]_i_1_n_0\,
      Q => \adr_read_reorder_data_mem1_reg_n_0_[4]\,
      S => \adr_read_reorder_data_mem1[10]_i_1_n_0\
    );
\adr_read_reorder_data_mem1_reg[5]\: unisim.vcomponents.FDSE
     port map (
      C => m00_axis_aclk,
      CE => \adr_read_reorder_data_mem1[10]_i_2_n_0\,
      D => \adr_read_reorder_data_mem1[5]_i_1_n_0\,
      Q => \adr_read_reorder_data_mem1_reg_n_0_[5]\,
      S => \adr_read_reorder_data_mem1[10]_i_1_n_0\
    );
\adr_read_reorder_data_mem1_reg[6]\: unisim.vcomponents.FDSE
     port map (
      C => m00_axis_aclk,
      CE => \adr_read_reorder_data_mem1[10]_i_2_n_0\,
      D => \adr_read_reorder_data_mem1[6]_i_1_n_0\,
      Q => \adr_read_reorder_data_mem1_reg_n_0_[6]\,
      S => \adr_read_reorder_data_mem1[10]_i_1_n_0\
    );
\adr_read_reorder_data_mem1_reg[7]\: unisim.vcomponents.FDSE
     port map (
      C => m00_axis_aclk,
      CE => \adr_read_reorder_data_mem1[10]_i_2_n_0\,
      D => \adr_read_reorder_data_mem1[7]_i_1_n_0\,
      Q => \adr_read_reorder_data_mem1_reg_n_0_[7]\,
      S => \adr_read_reorder_data_mem1[10]_i_1_n_0\
    );
\adr_read_reorder_data_mem1_reg[8]\: unisim.vcomponents.FDSE
     port map (
      C => m00_axis_aclk,
      CE => \adr_read_reorder_data_mem1[10]_i_2_n_0\,
      D => \adr_read_reorder_data_mem1[8]_i_1_n_0\,
      Q => \adr_read_reorder_data_mem1_reg_n_0_[8]\,
      S => \adr_read_reorder_data_mem1[10]_i_1_n_0\
    );
\adr_read_reorder_data_mem1_reg[9]\: unisim.vcomponents.FDSE
     port map (
      C => m00_axis_aclk,
      CE => \adr_read_reorder_data_mem1[10]_i_2_n_0\,
      D => \adr_read_reorder_data_mem1[9]_i_1_n_0\,
      Q => \adr_read_reorder_data_mem1_reg_n_0_[9]\,
      S => \adr_read_reorder_data_mem1[10]_i_1_n_0\
    );
\adr_read_reorder_data_mem2[0]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"DF"
    )
        port map (
      I0 => s00_axis_aresetn,
      I1 => s00_axis_tuser(11),
      I2 => \adr_read_reorder_data_mem2_reg_n_0_[0]\,
      O => p_1_in(0)
    );
\adr_read_reorder_data_mem2[10]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => s00_axis_tuser(11),
      I1 => s00_axis_aresetn,
      O => \adr_read_reorder_data_mem2[10]_i_1_n_0\
    );
\adr_read_reorder_data_mem2[10]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => s00_axis_aresetn,
      I1 => s00_axis_tuser(11),
      O => \adr_read_reorder_data_mem2[10]_i_2_n_0\
    );
\adr_read_reorder_data_mem2[10]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFF9AAAAAAA"
    )
        port map (
      I0 => \adr_read_reorder_data_mem2_reg_n_0_[10]\,
      I1 => \adr_read_reorder_data_mem2[10]_i_4_n_0\,
      I2 => \adr_read_reorder_data_mem2_reg_n_0_[9]\,
      I3 => \adr_read_reorder_data_mem2_reg_n_0_[7]\,
      I4 => \adr_read_reorder_data_mem2_reg_n_0_[8]\,
      I5 => \adr_read_reorder_data_mem1[10]_i_2_n_0\,
      O => p_1_in(10)
    );
\adr_read_reorder_data_mem2[10]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => \adr_read_reorder_data_mem2[6]_i_5_n_0\,
      I1 => \adr_read_reorder_data_mem2_reg_n_0_[6]\,
      O => \adr_read_reorder_data_mem2[10]_i_4_n_0\
    );
\adr_read_reorder_data_mem2[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFF8FF8FFFF"
    )
        port map (
      I0 => \adr_read_reorder_data_mem2[1]_i_2_n_0\,
      I1 => \adr_read_reorder_data_mem2[1]_i_3_n_0\,
      I2 => \adr_read_reorder_data_mem2_reg_n_0_[0]\,
      I3 => \adr_read_reorder_data_mem2_reg_n_0_[1]\,
      I4 => s00_axis_aresetn,
      I5 => s00_axis_tuser(11),
      O => p_1_in(1)
    );
\adr_read_reorder_data_mem2[1]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000001"
    )
        port map (
      I0 => \adr_read_reorder_data_mem2_reg_n_0_[6]\,
      I1 => \adr_read_reorder_data_mem2_reg_n_0_[2]\,
      I2 => \adr_read_reorder_data_mem2_reg_n_0_[3]\,
      I3 => \adr_read_reorder_data_mem2_reg_n_0_[5]\,
      I4 => \adr_read_reorder_data_mem2_reg_n_0_[4]\,
      O => \adr_read_reorder_data_mem2[1]_i_2_n_0\
    );
\adr_read_reorder_data_mem2[1]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00008000"
    )
        port map (
      I0 => \adr_read_reorder_data_mem2_reg_n_0_[10]\,
      I1 => \adr_read_reorder_data_mem2_reg_n_0_[8]\,
      I2 => \adr_read_reorder_data_mem2_reg_n_0_[7]\,
      I3 => \adr_read_reorder_data_mem2_reg_n_0_[9]\,
      I4 => \adr_read_reorder_data_mem2_reg_n_0_[0]\,
      O => \adr_read_reorder_data_mem2[1]_i_3_n_0\
    );
\adr_read_reorder_data_mem2[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EBEBEBEBFFEBEBEB"
    )
        port map (
      I0 => \adr_read_reorder_data_mem1[10]_i_2_n_0\,
      I1 => \adr_read_reorder_data_mem2[2]_i_2_n_0\,
      I2 => \adr_read_reorder_data_mem2_reg_n_0_[2]\,
      I3 => \adr_read_reorder_data_mem2[5]_i_3_n_0\,
      I4 => \adr_read_reorder_data_mem2[6]_i_3_n_0\,
      I5 => \adr_read_reorder_data_mem2_reg_n_0_[3]\,
      O => p_1_in(2)
    );
\adr_read_reorder_data_mem2[2]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => \adr_read_reorder_data_mem2_reg_n_0_[0]\,
      I1 => \adr_read_reorder_data_mem2_reg_n_0_[1]\,
      O => \adr_read_reorder_data_mem2[2]_i_2_n_0\
    );
\adr_read_reorder_data_mem2[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EBEBEBEBFFEBEBEB"
    )
        port map (
      I0 => \adr_read_reorder_data_mem1[10]_i_2_n_0\,
      I1 => \adr_read_reorder_data_mem2[3]_i_2_n_0\,
      I2 => \adr_read_reorder_data_mem2_reg_n_0_[3]\,
      I3 => \adr_read_reorder_data_mem2[5]_i_3_n_0\,
      I4 => \adr_read_reorder_data_mem2[6]_i_3_n_0\,
      I5 => \adr_read_reorder_data_mem2_reg_n_0_[2]\,
      O => p_1_in(3)
    );
\adr_read_reorder_data_mem2[3]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"7F"
    )
        port map (
      I0 => \adr_read_reorder_data_mem2_reg_n_0_[1]\,
      I1 => \adr_read_reorder_data_mem2_reg_n_0_[0]\,
      I2 => \adr_read_reorder_data_mem2_reg_n_0_[2]\,
      O => \adr_read_reorder_data_mem2[3]_i_2_n_0\
    );
\adr_read_reorder_data_mem2[4]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EBEBEBEBFFEBEBEB"
    )
        port map (
      I0 => \adr_read_reorder_data_mem1[10]_i_2_n_0\,
      I1 => \adr_read_reorder_data_mem2[4]_i_2_n_0\,
      I2 => \adr_read_reorder_data_mem2_reg_n_0_[4]\,
      I3 => \adr_read_reorder_data_mem2[5]_i_3_n_0\,
      I4 => \adr_read_reorder_data_mem2[6]_i_2_n_0\,
      I5 => \adr_read_reorder_data_mem2_reg_n_0_[5]\,
      O => p_1_in(4)
    );
\adr_read_reorder_data_mem2[4]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7FFF"
    )
        port map (
      I0 => \adr_read_reorder_data_mem2_reg_n_0_[2]\,
      I1 => \adr_read_reorder_data_mem2_reg_n_0_[0]\,
      I2 => \adr_read_reorder_data_mem2_reg_n_0_[1]\,
      I3 => \adr_read_reorder_data_mem2_reg_n_0_[3]\,
      O => \adr_read_reorder_data_mem2[4]_i_2_n_0\
    );
\adr_read_reorder_data_mem2[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EBEBEBEBFFEBEBEB"
    )
        port map (
      I0 => \adr_read_reorder_data_mem1[10]_i_2_n_0\,
      I1 => \adr_read_reorder_data_mem2[5]_i_2_n_0\,
      I2 => \adr_read_reorder_data_mem2_reg_n_0_[5]\,
      I3 => \adr_read_reorder_data_mem2[5]_i_3_n_0\,
      I4 => \adr_read_reorder_data_mem2[6]_i_2_n_0\,
      I5 => \adr_read_reorder_data_mem2_reg_n_0_[4]\,
      O => p_1_in(5)
    );
\adr_read_reorder_data_mem2[5]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFFFFFF"
    )
        port map (
      I0 => \adr_read_reorder_data_mem2_reg_n_0_[3]\,
      I1 => \adr_read_reorder_data_mem2_reg_n_0_[1]\,
      I2 => \adr_read_reorder_data_mem2_reg_n_0_[0]\,
      I3 => \adr_read_reorder_data_mem2_reg_n_0_[2]\,
      I4 => \adr_read_reorder_data_mem2_reg_n_0_[4]\,
      O => \adr_read_reorder_data_mem2[5]_i_2_n_0\
    );
\adr_read_reorder_data_mem2[5]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \adr_read_reorder_data_mem2[6]_i_4_n_0\,
      I1 => \adr_read_reorder_data_mem2_reg_n_0_[6]\,
      O => \adr_read_reorder_data_mem2[5]_i_3_n_0\
    );
\adr_read_reorder_data_mem2[6]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFEAAAEAAAFFFF"
    )
        port map (
      I0 => \adr_read_reorder_data_mem1[10]_i_2_n_0\,
      I1 => \adr_read_reorder_data_mem2[6]_i_2_n_0\,
      I2 => \adr_read_reorder_data_mem2[6]_i_3_n_0\,
      I3 => \adr_read_reorder_data_mem2[6]_i_4_n_0\,
      I4 => \adr_read_reorder_data_mem2_reg_n_0_[6]\,
      I5 => \adr_read_reorder_data_mem2[6]_i_5_n_0\,
      O => p_1_in(6)
    );
\adr_read_reorder_data_mem2[6]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \adr_read_reorder_data_mem2_reg_n_0_[2]\,
      I1 => \adr_read_reorder_data_mem2_reg_n_0_[3]\,
      O => \adr_read_reorder_data_mem2[6]_i_2_n_0\
    );
\adr_read_reorder_data_mem2[6]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \adr_read_reorder_data_mem2_reg_n_0_[4]\,
      I1 => \adr_read_reorder_data_mem2_reg_n_0_[5]\,
      O => \adr_read_reorder_data_mem2[6]_i_3_n_0\
    );
\adr_read_reorder_data_mem2[6]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000040000000"
    )
        port map (
      I0 => \adr_read_reorder_data_mem2_reg_n_0_[0]\,
      I1 => \adr_read_reorder_data_mem2_reg_n_0_[9]\,
      I2 => \adr_read_reorder_data_mem2_reg_n_0_[7]\,
      I3 => \adr_read_reorder_data_mem2_reg_n_0_[8]\,
      I4 => \adr_read_reorder_data_mem2_reg_n_0_[10]\,
      I5 => \adr_read_reorder_data_mem2_reg_n_0_[1]\,
      O => \adr_read_reorder_data_mem2[6]_i_4_n_0\
    );
\adr_read_reorder_data_mem2[6]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFFFFFFFFFF"
    )
        port map (
      I0 => \adr_read_reorder_data_mem2_reg_n_0_[4]\,
      I1 => \adr_read_reorder_data_mem2_reg_n_0_[2]\,
      I2 => \adr_read_reorder_data_mem2_reg_n_0_[0]\,
      I3 => \adr_read_reorder_data_mem2_reg_n_0_[1]\,
      I4 => \adr_read_reorder_data_mem2_reg_n_0_[3]\,
      I5 => \adr_read_reorder_data_mem2_reg_n_0_[5]\,
      O => \adr_read_reorder_data_mem2[6]_i_5_n_0\
    );
\adr_read_reorder_data_mem2[7]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FF9F"
    )
        port map (
      I0 => \adr_read_reorder_data_mem2_reg_n_0_[7]\,
      I1 => \adr_read_reorder_data_mem2[10]_i_4_n_0\,
      I2 => s00_axis_aresetn,
      I3 => s00_axis_tuser(11),
      O => p_1_in(7)
    );
\adr_read_reorder_data_mem2[8]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF9AFF"
    )
        port map (
      I0 => \adr_read_reorder_data_mem2_reg_n_0_[8]\,
      I1 => \adr_read_reorder_data_mem2[10]_i_4_n_0\,
      I2 => \adr_read_reorder_data_mem2_reg_n_0_[7]\,
      I3 => s00_axis_aresetn,
      I4 => s00_axis_tuser(11),
      O => p_1_in(8)
    );
\adr_read_reorder_data_mem2[9]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFF9AAAFFFF"
    )
        port map (
      I0 => \adr_read_reorder_data_mem2_reg_n_0_[9]\,
      I1 => \adr_read_reorder_data_mem2[10]_i_4_n_0\,
      I2 => \adr_read_reorder_data_mem2_reg_n_0_[8]\,
      I3 => \adr_read_reorder_data_mem2_reg_n_0_[7]\,
      I4 => s00_axis_aresetn,
      I5 => s00_axis_tuser(11),
      O => p_1_in(9)
    );
\adr_read_reorder_data_mem2_reg[0]\: unisim.vcomponents.FDSE
     port map (
      C => m00_axis_aclk,
      CE => \adr_read_reorder_data_mem2[10]_i_2_n_0\,
      D => p_1_in(0),
      Q => \adr_read_reorder_data_mem2_reg_n_0_[0]\,
      S => \adr_read_reorder_data_mem2[10]_i_1_n_0\
    );
\adr_read_reorder_data_mem2_reg[10]\: unisim.vcomponents.FDSE
     port map (
      C => m00_axis_aclk,
      CE => \adr_read_reorder_data_mem2[10]_i_2_n_0\,
      D => p_1_in(10),
      Q => \adr_read_reorder_data_mem2_reg_n_0_[10]\,
      S => \adr_read_reorder_data_mem2[10]_i_1_n_0\
    );
\adr_read_reorder_data_mem2_reg[1]\: unisim.vcomponents.FDSE
     port map (
      C => m00_axis_aclk,
      CE => \adr_read_reorder_data_mem2[10]_i_2_n_0\,
      D => p_1_in(1),
      Q => \adr_read_reorder_data_mem2_reg_n_0_[1]\,
      S => \adr_read_reorder_data_mem2[10]_i_1_n_0\
    );
\adr_read_reorder_data_mem2_reg[2]\: unisim.vcomponents.FDSE
     port map (
      C => m00_axis_aclk,
      CE => \adr_read_reorder_data_mem2[10]_i_2_n_0\,
      D => p_1_in(2),
      Q => \adr_read_reorder_data_mem2_reg_n_0_[2]\,
      S => \adr_read_reorder_data_mem2[10]_i_1_n_0\
    );
\adr_read_reorder_data_mem2_reg[3]\: unisim.vcomponents.FDSE
     port map (
      C => m00_axis_aclk,
      CE => \adr_read_reorder_data_mem2[10]_i_2_n_0\,
      D => p_1_in(3),
      Q => \adr_read_reorder_data_mem2_reg_n_0_[3]\,
      S => \adr_read_reorder_data_mem2[10]_i_1_n_0\
    );
\adr_read_reorder_data_mem2_reg[4]\: unisim.vcomponents.FDSE
     port map (
      C => m00_axis_aclk,
      CE => \adr_read_reorder_data_mem2[10]_i_2_n_0\,
      D => p_1_in(4),
      Q => \adr_read_reorder_data_mem2_reg_n_0_[4]\,
      S => \adr_read_reorder_data_mem2[10]_i_1_n_0\
    );
\adr_read_reorder_data_mem2_reg[5]\: unisim.vcomponents.FDSE
     port map (
      C => m00_axis_aclk,
      CE => \adr_read_reorder_data_mem2[10]_i_2_n_0\,
      D => p_1_in(5),
      Q => \adr_read_reorder_data_mem2_reg_n_0_[5]\,
      S => \adr_read_reorder_data_mem2[10]_i_1_n_0\
    );
\adr_read_reorder_data_mem2_reg[6]\: unisim.vcomponents.FDSE
     port map (
      C => m00_axis_aclk,
      CE => \adr_read_reorder_data_mem2[10]_i_2_n_0\,
      D => p_1_in(6),
      Q => \adr_read_reorder_data_mem2_reg_n_0_[6]\,
      S => \adr_read_reorder_data_mem2[10]_i_1_n_0\
    );
\adr_read_reorder_data_mem2_reg[7]\: unisim.vcomponents.FDSE
     port map (
      C => m00_axis_aclk,
      CE => \adr_read_reorder_data_mem2[10]_i_2_n_0\,
      D => p_1_in(7),
      Q => \adr_read_reorder_data_mem2_reg_n_0_[7]\,
      S => \adr_read_reorder_data_mem2[10]_i_1_n_0\
    );
\adr_read_reorder_data_mem2_reg[8]\: unisim.vcomponents.FDSE
     port map (
      C => m00_axis_aclk,
      CE => \adr_read_reorder_data_mem2[10]_i_2_n_0\,
      D => p_1_in(8),
      Q => \adr_read_reorder_data_mem2_reg_n_0_[8]\,
      S => \adr_read_reorder_data_mem2[10]_i_1_n_0\
    );
\adr_read_reorder_data_mem2_reg[9]\: unisim.vcomponents.FDSE
     port map (
      C => m00_axis_aclk,
      CE => \adr_read_reorder_data_mem2[10]_i_2_n_0\,
      D => p_1_in(9),
      Q => \adr_read_reorder_data_mem2_reg_n_0_[9]\,
      S => \adr_read_reorder_data_mem2[10]_i_1_n_0\
    );
cnt_inner_valid0_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => cnt_inner_valid0_carry_n_0,
      CO(2) => cnt_inner_valid0_carry_n_1,
      CO(1) => cnt_inner_valid0_carry_n_2,
      CO(0) => cnt_inner_valid0_carry_n_3,
      CYINIT => \cnt_inner_valid_reg_n_0_[0]\,
      DI(3 downto 0) => B"0000",
      O(3) => cnt_inner_valid0_carry_n_4,
      O(2) => cnt_inner_valid0_carry_n_5,
      O(1) => cnt_inner_valid0_carry_n_6,
      O(0) => cnt_inner_valid0_carry_n_7,
      S(3) => \cnt_inner_valid_reg_n_0_[4]\,
      S(2) => \cnt_inner_valid_reg_n_0_[3]\,
      S(1) => \cnt_inner_valid_reg_n_0_[2]\,
      S(0) => \cnt_inner_valid_reg_n_0_[1]\
    );
\cnt_inner_valid0_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => cnt_inner_valid0_carry_n_0,
      CO(3) => \cnt_inner_valid0_carry__0_n_0\,
      CO(2) => \cnt_inner_valid0_carry__0_n_1\,
      CO(1) => \cnt_inner_valid0_carry__0_n_2\,
      CO(0) => \cnt_inner_valid0_carry__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \cnt_inner_valid0_carry__0_n_4\,
      O(2) => \cnt_inner_valid0_carry__0_n_5\,
      O(1) => \cnt_inner_valid0_carry__0_n_6\,
      O(0) => \cnt_inner_valid0_carry__0_n_7\,
      S(3) => \cnt_inner_valid_reg_n_0_[8]\,
      S(2) => \cnt_inner_valid_reg_n_0_[7]\,
      S(1) => \cnt_inner_valid_reg_n_0_[6]\,
      S(0) => \cnt_inner_valid_reg_n_0_[5]\
    );
\cnt_inner_valid0_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \cnt_inner_valid0_carry__0_n_0\,
      CO(3) => \cnt_inner_valid0_carry__1_n_0\,
      CO(2) => \cnt_inner_valid0_carry__1_n_1\,
      CO(1) => \cnt_inner_valid0_carry__1_n_2\,
      CO(0) => \cnt_inner_valid0_carry__1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \cnt_inner_valid0_carry__1_n_4\,
      O(2) => \cnt_inner_valid0_carry__1_n_5\,
      O(1) => \cnt_inner_valid0_carry__1_n_6\,
      O(0) => \cnt_inner_valid0_carry__1_n_7\,
      S(3) => \cnt_inner_valid_reg_n_0_[12]\,
      S(2) => \cnt_inner_valid_reg_n_0_[11]\,
      S(1) => \cnt_inner_valid_reg_n_0_[10]\,
      S(0) => \cnt_inner_valid_reg_n_0_[9]\
    );
\cnt_inner_valid0_carry__2\: unisim.vcomponents.CARRY4
     port map (
      CI => \cnt_inner_valid0_carry__1_n_0\,
      CO(3 downto 2) => \NLW_cnt_inner_valid0_carry__2_CO_UNCONNECTED\(3 downto 2),
      CO(1) => \cnt_inner_valid0_carry__2_n_2\,
      CO(0) => \cnt_inner_valid0_carry__2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \NLW_cnt_inner_valid0_carry__2_O_UNCONNECTED\(3),
      O(2) => \cnt_inner_valid0_carry__2_n_5\,
      O(1) => \cnt_inner_valid0_carry__2_n_6\,
      O(0) => \cnt_inner_valid0_carry__2_n_7\,
      S(3) => '0',
      S(2) => \cnt_inner_valid_reg_n_0_[15]\,
      S(1) => \cnt_inner_valid_reg_n_0_[14]\,
      S(0) => \cnt_inner_valid_reg_n_0_[13]\
    );
\cnt_inner_valid[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0000FFFE"
    )
        port map (
      I0 => \cnt_inner_valid[15]_i_4_n_0\,
      I1 => \cnt_inner_valid[15]_i_3_n_0\,
      I2 => \cnt_inner_valid_reg_n_0_[4]\,
      I3 => \cnt_inner_valid_reg_n_0_[5]\,
      I4 => \cnt_inner_valid_reg_n_0_[0]\,
      O => cnt_inner_valid(0)
    );
\cnt_inner_valid[10]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFE00000000"
    )
        port map (
      I0 => \cnt_inner_valid_reg_n_0_[5]\,
      I1 => \cnt_inner_valid_reg_n_0_[4]\,
      I2 => \cnt_inner_valid[15]_i_3_n_0\,
      I3 => \cnt_inner_valid[15]_i_4_n_0\,
      I4 => \cnt_inner_valid_reg_n_0_[0]\,
      I5 => \cnt_inner_valid0_carry__1_n_6\,
      O => cnt_inner_valid(10)
    );
\cnt_inner_valid[11]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFE00000000"
    )
        port map (
      I0 => \cnt_inner_valid_reg_n_0_[5]\,
      I1 => \cnt_inner_valid_reg_n_0_[4]\,
      I2 => \cnt_inner_valid[15]_i_3_n_0\,
      I3 => \cnt_inner_valid[15]_i_4_n_0\,
      I4 => \cnt_inner_valid_reg_n_0_[0]\,
      I5 => \cnt_inner_valid0_carry__1_n_5\,
      O => cnt_inner_valid(11)
    );
\cnt_inner_valid[12]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFE00000000"
    )
        port map (
      I0 => \cnt_inner_valid_reg_n_0_[5]\,
      I1 => \cnt_inner_valid_reg_n_0_[4]\,
      I2 => \cnt_inner_valid[15]_i_3_n_0\,
      I3 => \cnt_inner_valid[15]_i_4_n_0\,
      I4 => \cnt_inner_valid_reg_n_0_[0]\,
      I5 => \cnt_inner_valid0_carry__1_n_4\,
      O => cnt_inner_valid(12)
    );
\cnt_inner_valid[13]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFE00000000"
    )
        port map (
      I0 => \cnt_inner_valid_reg_n_0_[5]\,
      I1 => \cnt_inner_valid_reg_n_0_[4]\,
      I2 => \cnt_inner_valid[15]_i_3_n_0\,
      I3 => \cnt_inner_valid[15]_i_4_n_0\,
      I4 => \cnt_inner_valid_reg_n_0_[0]\,
      I5 => \cnt_inner_valid0_carry__2_n_7\,
      O => cnt_inner_valid(13)
    );
\cnt_inner_valid[14]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFE00000000"
    )
        port map (
      I0 => \cnt_inner_valid_reg_n_0_[5]\,
      I1 => \cnt_inner_valid_reg_n_0_[4]\,
      I2 => \cnt_inner_valid[15]_i_3_n_0\,
      I3 => \cnt_inner_valid[15]_i_4_n_0\,
      I4 => \cnt_inner_valid_reg_n_0_[0]\,
      I5 => \cnt_inner_valid0_carry__2_n_6\,
      O => cnt_inner_valid(14)
    );
\cnt_inner_valid[15]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => s00_axis_tvalid,
      I1 => s00_axis_aresetn,
      O => \cnt_inner_valid[15]_i_1_n_0\
    );
\cnt_inner_valid[15]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFE00000000"
    )
        port map (
      I0 => \cnt_inner_valid_reg_n_0_[5]\,
      I1 => \cnt_inner_valid_reg_n_0_[4]\,
      I2 => \cnt_inner_valid[15]_i_3_n_0\,
      I3 => \cnt_inner_valid[15]_i_4_n_0\,
      I4 => \cnt_inner_valid_reg_n_0_[0]\,
      I5 => \cnt_inner_valid0_carry__2_n_5\,
      O => cnt_inner_valid(15)
    );
\cnt_inner_valid[15]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => \cnt_inner_valid[15]_i_5_n_0\,
      I1 => \cnt_inner_valid_reg_n_0_[7]\,
      O => \cnt_inner_valid[15]_i_3_n_0\
    );
\cnt_inner_valid[15]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFF7FF"
    )
        port map (
      I0 => \cnt_inner_valid_reg_n_0_[9]\,
      I1 => \cnt_inner_valid_reg_n_0_[10]\,
      I2 => \cnt_inner_valid_reg_n_0_[3]\,
      I3 => \cnt_inner_valid_reg_n_0_[8]\,
      I4 => \cnt_inner_valid_reg_n_0_[2]\,
      I5 => \cnt_inner_valid_reg_n_0_[1]\,
      O => \cnt_inner_valid[15]_i_4_n_0\
    );
\cnt_inner_valid[15]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => \cnt_inner_valid_reg_n_0_[12]\,
      I1 => \cnt_inner_valid_reg_n_0_[13]\,
      I2 => \cnt_inner_valid_reg_n_0_[15]\,
      I3 => \cnt_inner_valid_reg_n_0_[14]\,
      I4 => \cnt_inner_valid_reg_n_0_[11]\,
      I5 => \cnt_inner_valid_reg_n_0_[6]\,
      O => \cnt_inner_valid[15]_i_5_n_0\
    );
\cnt_inner_valid[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFE00000000"
    )
        port map (
      I0 => \cnt_inner_valid_reg_n_0_[5]\,
      I1 => \cnt_inner_valid_reg_n_0_[4]\,
      I2 => \cnt_inner_valid[15]_i_3_n_0\,
      I3 => \cnt_inner_valid[15]_i_4_n_0\,
      I4 => \cnt_inner_valid_reg_n_0_[0]\,
      I5 => cnt_inner_valid0_carry_n_7,
      O => cnt_inner_valid(1)
    );
\cnt_inner_valid[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFE00000000"
    )
        port map (
      I0 => \cnt_inner_valid_reg_n_0_[5]\,
      I1 => \cnt_inner_valid_reg_n_0_[4]\,
      I2 => \cnt_inner_valid[15]_i_3_n_0\,
      I3 => \cnt_inner_valid[15]_i_4_n_0\,
      I4 => \cnt_inner_valid_reg_n_0_[0]\,
      I5 => cnt_inner_valid0_carry_n_6,
      O => cnt_inner_valid(2)
    );
\cnt_inner_valid[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFE00000000"
    )
        port map (
      I0 => \cnt_inner_valid_reg_n_0_[5]\,
      I1 => \cnt_inner_valid_reg_n_0_[4]\,
      I2 => \cnt_inner_valid[15]_i_3_n_0\,
      I3 => \cnt_inner_valid[15]_i_4_n_0\,
      I4 => \cnt_inner_valid_reg_n_0_[0]\,
      I5 => cnt_inner_valid0_carry_n_5,
      O => cnt_inner_valid(3)
    );
\cnt_inner_valid[4]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFE00000000"
    )
        port map (
      I0 => \cnt_inner_valid_reg_n_0_[5]\,
      I1 => \cnt_inner_valid_reg_n_0_[4]\,
      I2 => \cnt_inner_valid[15]_i_3_n_0\,
      I3 => \cnt_inner_valid[15]_i_4_n_0\,
      I4 => \cnt_inner_valid_reg_n_0_[0]\,
      I5 => cnt_inner_valid0_carry_n_4,
      O => cnt_inner_valid(4)
    );
\cnt_inner_valid[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFE00000000"
    )
        port map (
      I0 => \cnt_inner_valid_reg_n_0_[5]\,
      I1 => \cnt_inner_valid_reg_n_0_[4]\,
      I2 => \cnt_inner_valid[15]_i_3_n_0\,
      I3 => \cnt_inner_valid[15]_i_4_n_0\,
      I4 => \cnt_inner_valid_reg_n_0_[0]\,
      I5 => \cnt_inner_valid0_carry__0_n_7\,
      O => cnt_inner_valid(5)
    );
\cnt_inner_valid[6]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFE00000000"
    )
        port map (
      I0 => \cnt_inner_valid_reg_n_0_[5]\,
      I1 => \cnt_inner_valid_reg_n_0_[4]\,
      I2 => \cnt_inner_valid[15]_i_3_n_0\,
      I3 => \cnt_inner_valid[15]_i_4_n_0\,
      I4 => \cnt_inner_valid_reg_n_0_[0]\,
      I5 => \cnt_inner_valid0_carry__0_n_6\,
      O => cnt_inner_valid(6)
    );
\cnt_inner_valid[7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFE00000000"
    )
        port map (
      I0 => \cnt_inner_valid_reg_n_0_[5]\,
      I1 => \cnt_inner_valid_reg_n_0_[4]\,
      I2 => \cnt_inner_valid[15]_i_3_n_0\,
      I3 => \cnt_inner_valid[15]_i_4_n_0\,
      I4 => \cnt_inner_valid_reg_n_0_[0]\,
      I5 => \cnt_inner_valid0_carry__0_n_5\,
      O => cnt_inner_valid(7)
    );
\cnt_inner_valid[8]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFE00000000"
    )
        port map (
      I0 => \cnt_inner_valid_reg_n_0_[5]\,
      I1 => \cnt_inner_valid_reg_n_0_[4]\,
      I2 => \cnt_inner_valid[15]_i_3_n_0\,
      I3 => \cnt_inner_valid[15]_i_4_n_0\,
      I4 => \cnt_inner_valid_reg_n_0_[0]\,
      I5 => \cnt_inner_valid0_carry__0_n_4\,
      O => cnt_inner_valid(8)
    );
\cnt_inner_valid[9]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFE00000000"
    )
        port map (
      I0 => \cnt_inner_valid_reg_n_0_[5]\,
      I1 => \cnt_inner_valid_reg_n_0_[4]\,
      I2 => \cnt_inner_valid[15]_i_3_n_0\,
      I3 => \cnt_inner_valid[15]_i_4_n_0\,
      I4 => \cnt_inner_valid_reg_n_0_[0]\,
      I5 => \cnt_inner_valid0_carry__1_n_7\,
      O => cnt_inner_valid(9)
    );
\cnt_inner_valid_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => cnt_inner_valid(0),
      Q => \cnt_inner_valid_reg_n_0_[0]\,
      R => \cnt_inner_valid[15]_i_1_n_0\
    );
\cnt_inner_valid_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => cnt_inner_valid(10),
      Q => \cnt_inner_valid_reg_n_0_[10]\,
      R => \cnt_inner_valid[15]_i_1_n_0\
    );
\cnt_inner_valid_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => cnt_inner_valid(11),
      Q => \cnt_inner_valid_reg_n_0_[11]\,
      R => \cnt_inner_valid[15]_i_1_n_0\
    );
\cnt_inner_valid_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => cnt_inner_valid(12),
      Q => \cnt_inner_valid_reg_n_0_[12]\,
      R => \cnt_inner_valid[15]_i_1_n_0\
    );
\cnt_inner_valid_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => cnt_inner_valid(13),
      Q => \cnt_inner_valid_reg_n_0_[13]\,
      R => \cnt_inner_valid[15]_i_1_n_0\
    );
\cnt_inner_valid_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => cnt_inner_valid(14),
      Q => \cnt_inner_valid_reg_n_0_[14]\,
      R => \cnt_inner_valid[15]_i_1_n_0\
    );
\cnt_inner_valid_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => cnt_inner_valid(15),
      Q => \cnt_inner_valid_reg_n_0_[15]\,
      R => \cnt_inner_valid[15]_i_1_n_0\
    );
\cnt_inner_valid_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => cnt_inner_valid(1),
      Q => \cnt_inner_valid_reg_n_0_[1]\,
      R => \cnt_inner_valid[15]_i_1_n_0\
    );
\cnt_inner_valid_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => cnt_inner_valid(2),
      Q => \cnt_inner_valid_reg_n_0_[2]\,
      R => \cnt_inner_valid[15]_i_1_n_0\
    );
\cnt_inner_valid_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => cnt_inner_valid(3),
      Q => \cnt_inner_valid_reg_n_0_[3]\,
      R => \cnt_inner_valid[15]_i_1_n_0\
    );
\cnt_inner_valid_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => cnt_inner_valid(4),
      Q => \cnt_inner_valid_reg_n_0_[4]\,
      R => \cnt_inner_valid[15]_i_1_n_0\
    );
\cnt_inner_valid_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => cnt_inner_valid(5),
      Q => \cnt_inner_valid_reg_n_0_[5]\,
      R => \cnt_inner_valid[15]_i_1_n_0\
    );
\cnt_inner_valid_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => cnt_inner_valid(6),
      Q => \cnt_inner_valid_reg_n_0_[6]\,
      R => \cnt_inner_valid[15]_i_1_n_0\
    );
\cnt_inner_valid_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => cnt_inner_valid(7),
      Q => \cnt_inner_valid_reg_n_0_[7]\,
      R => \cnt_inner_valid[15]_i_1_n_0\
    );
\cnt_inner_valid_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => cnt_inner_valid(8),
      Q => \cnt_inner_valid_reg_n_0_[8]\,
      R => \cnt_inner_valid[15]_i_1_n_0\
    );
\cnt_inner_valid_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => cnt_inner_valid(9),
      Q => \cnt_inner_valid_reg_n_0_[9]\,
      R => \cnt_inner_valid[15]_i_1_n_0\
    );
\cnt_last[0]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FF40"
    )
        port map (
      I0 => \adr_read_reorder_data_mem1[6]_i_3_n_0\,
      I1 => \^m00_axis_tvalid\,
      I2 => s00_axis_tuser(11),
      I3 => \cnt_last[0]_i_4_n_0\,
      O => last
    );
\cnt_last[0]_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => cnt_last_reg(0),
      O => \cnt_last[0]_i_3_n_0\
    );
\cnt_last[0]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000010"
    )
        port map (
      I0 => s00_axis_tuser(1),
      I1 => s00_axis_tuser(2),
      I2 => \^m00_axis_tvalid\,
      I3 => s00_axis_tuser(0),
      I4 => s00_axis_tuser(11),
      I5 => s00_axis_tuser(3),
      O => \cnt_last[0]_i_4_n_0\
    );
\cnt_last_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => last,
      CE => '1',
      D => \cnt_last_reg[0]_i_1_n_7\,
      Q => cnt_last_reg(0),
      R => \^m00_axis_tlast\
    );
\cnt_last_reg[0]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \cnt_last_reg[0]_i_1_n_0\,
      CO(2) => \cnt_last_reg[0]_i_1_n_1\,
      CO(1) => \cnt_last_reg[0]_i_1_n_2\,
      CO(0) => \cnt_last_reg[0]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0001",
      O(3) => \cnt_last_reg[0]_i_1_n_4\,
      O(2) => \cnt_last_reg[0]_i_1_n_5\,
      O(1) => \cnt_last_reg[0]_i_1_n_6\,
      O(0) => \cnt_last_reg[0]_i_1_n_7\,
      S(3 downto 1) => cnt_last_reg(3 downto 1),
      S(0) => \cnt_last[0]_i_3_n_0\
    );
\cnt_last_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => last,
      CE => '1',
      D => \cnt_last_reg[8]_i_1_n_5\,
      Q => cnt_last_reg(10),
      R => \^m00_axis_tlast\
    );
\cnt_last_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => last,
      CE => '1',
      D => \cnt_last_reg[8]_i_1_n_4\,
      Q => cnt_last_reg(11),
      R => \^m00_axis_tlast\
    );
\cnt_last_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => last,
      CE => '1',
      D => \cnt_last_reg[12]_i_1_n_7\,
      Q => cnt_last_reg(12),
      R => \^m00_axis_tlast\
    );
\cnt_last_reg[12]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \cnt_last_reg[8]_i_1_n_0\,
      CO(3) => \NLW_cnt_last_reg[12]_i_1_CO_UNCONNECTED\(3),
      CO(2) => \cnt_last_reg[12]_i_1_n_1\,
      CO(1) => \cnt_last_reg[12]_i_1_n_2\,
      CO(0) => \cnt_last_reg[12]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \cnt_last_reg[12]_i_1_n_4\,
      O(2) => \cnt_last_reg[12]_i_1_n_5\,
      O(1) => \cnt_last_reg[12]_i_1_n_6\,
      O(0) => \cnt_last_reg[12]_i_1_n_7\,
      S(3 downto 0) => cnt_last_reg(15 downto 12)
    );
\cnt_last_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => last,
      CE => '1',
      D => \cnt_last_reg[12]_i_1_n_6\,
      Q => cnt_last_reg(13),
      R => \^m00_axis_tlast\
    );
\cnt_last_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => last,
      CE => '1',
      D => \cnt_last_reg[12]_i_1_n_5\,
      Q => cnt_last_reg(14),
      R => \^m00_axis_tlast\
    );
\cnt_last_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => last,
      CE => '1',
      D => \cnt_last_reg[12]_i_1_n_4\,
      Q => cnt_last_reg(15),
      R => \^m00_axis_tlast\
    );
\cnt_last_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => last,
      CE => '1',
      D => \cnt_last_reg[0]_i_1_n_6\,
      Q => cnt_last_reg(1),
      R => \^m00_axis_tlast\
    );
\cnt_last_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => last,
      CE => '1',
      D => \cnt_last_reg[0]_i_1_n_5\,
      Q => cnt_last_reg(2),
      R => \^m00_axis_tlast\
    );
\cnt_last_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => last,
      CE => '1',
      D => \cnt_last_reg[0]_i_1_n_4\,
      Q => cnt_last_reg(3),
      R => \^m00_axis_tlast\
    );
\cnt_last_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => last,
      CE => '1',
      D => \cnt_last_reg[4]_i_1_n_7\,
      Q => cnt_last_reg(4),
      R => \^m00_axis_tlast\
    );
\cnt_last_reg[4]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \cnt_last_reg[0]_i_1_n_0\,
      CO(3) => \cnt_last_reg[4]_i_1_n_0\,
      CO(2) => \cnt_last_reg[4]_i_1_n_1\,
      CO(1) => \cnt_last_reg[4]_i_1_n_2\,
      CO(0) => \cnt_last_reg[4]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \cnt_last_reg[4]_i_1_n_4\,
      O(2) => \cnt_last_reg[4]_i_1_n_5\,
      O(1) => \cnt_last_reg[4]_i_1_n_6\,
      O(0) => \cnt_last_reg[4]_i_1_n_7\,
      S(3 downto 0) => cnt_last_reg(7 downto 4)
    );
\cnt_last_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => last,
      CE => '1',
      D => \cnt_last_reg[4]_i_1_n_6\,
      Q => cnt_last_reg(5),
      R => \^m00_axis_tlast\
    );
\cnt_last_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => last,
      CE => '1',
      D => \cnt_last_reg[4]_i_1_n_5\,
      Q => cnt_last_reg(6),
      R => \^m00_axis_tlast\
    );
\cnt_last_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => last,
      CE => '1',
      D => \cnt_last_reg[4]_i_1_n_4\,
      Q => cnt_last_reg(7),
      R => \^m00_axis_tlast\
    );
\cnt_last_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => last,
      CE => '1',
      D => \cnt_last_reg[8]_i_1_n_7\,
      Q => cnt_last_reg(8),
      R => \^m00_axis_tlast\
    );
\cnt_last_reg[8]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \cnt_last_reg[4]_i_1_n_0\,
      CO(3) => \cnt_last_reg[8]_i_1_n_0\,
      CO(2) => \cnt_last_reg[8]_i_1_n_1\,
      CO(1) => \cnt_last_reg[8]_i_1_n_2\,
      CO(0) => \cnt_last_reg[8]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \cnt_last_reg[8]_i_1_n_4\,
      O(2) => \cnt_last_reg[8]_i_1_n_5\,
      O(1) => \cnt_last_reg[8]_i_1_n_6\,
      O(0) => \cnt_last_reg[8]_i_1_n_7\,
      S(3 downto 0) => cnt_last_reg(11 downto 8)
    );
\cnt_last_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => last,
      CE => '1',
      D => \cnt_last_reg[8]_i_1_n_6\,
      Q => cnt_last_reg(9),
      R => \^m00_axis_tlast\
    );
\ctrl_mux_out_1[3]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => s00_axis_tuser(11),
      O => \ctrl_mux_out_1[3]_i_1_n_0\
    );
\ctrl_mux_out_1[4]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BF8F"
    )
        port map (
      I0 => \active_mem1_in_read_state_reg_n_0_[4]\,
      I1 => s00_axis_aresetn,
      I2 => s00_axis_tuser(11),
      I3 => ctrl_mux_out_1(4),
      O => \ctrl_mux_out_1[4]_i_1_n_0\
    );
\ctrl_mux_out_1_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => s00_axis_aresetn,
      D => \active_mem1_in_read_state_reg_n_0_[0]\,
      Q => ctrl_mux_out_1(0),
      R => \ctrl_mux_out_1[3]_i_1_n_0\
    );
\ctrl_mux_out_1_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => s00_axis_aresetn,
      D => \active_mem1_in_read_state_reg_n_0_[1]\,
      Q => ctrl_mux_out_1(1),
      R => \ctrl_mux_out_1[3]_i_1_n_0\
    );
\ctrl_mux_out_1_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => s00_axis_aresetn,
      D => \active_mem1_in_read_state_reg_n_0_[2]\,
      Q => ctrl_mux_out_1(2),
      R => \ctrl_mux_out_1[3]_i_1_n_0\
    );
\ctrl_mux_out_1_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => s00_axis_aresetn,
      D => \active_mem1_in_read_state_reg_n_0_[3]\,
      Q => ctrl_mux_out_1(3),
      R => \ctrl_mux_out_1[3]_i_1_n_0\
    );
\ctrl_mux_out_1_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => \ctrl_mux_out_1[4]_i_1_n_0\,
      Q => ctrl_mux_out_1(4),
      R => '0'
    );
\ctrl_mux_out_2[4]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FBF8"
    )
        port map (
      I0 => active_mem2_in_read_state_reg(4),
      I1 => s00_axis_aresetn,
      I2 => s00_axis_tuser(11),
      I3 => ctrl_mux_out_2(4),
      O => \ctrl_mux_out_2[4]_i_1_n_0\
    );
\ctrl_mux_out_2_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => s00_axis_aresetn,
      D => active_mem2_in_read_state_reg(0),
      Q => ctrl_mux_out_2(0),
      R => s00_axis_tuser(11)
    );
\ctrl_mux_out_2_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => s00_axis_aresetn,
      D => active_mem2_in_read_state_reg(1),
      Q => ctrl_mux_out_2(1),
      R => s00_axis_tuser(11)
    );
\ctrl_mux_out_2_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => s00_axis_aresetn,
      D => active_mem2_in_read_state_reg(2),
      Q => ctrl_mux_out_2(2),
      R => s00_axis_tuser(11)
    );
\ctrl_mux_out_2_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => s00_axis_aresetn,
      D => active_mem2_in_read_state_reg(3),
      Q => ctrl_mux_out_2(3),
      R => s00_axis_tuser(11)
    );
\ctrl_mux_out_2_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => \ctrl_mux_out_2[4]_i_1_n_0\,
      Q => ctrl_mux_out_2(4),
      R => '0'
    );
\genblk1[0].rams_sp_rf_rst_inst0\: entity work.test_bd_pixelReOrder_0_0_rams_sp_rf_rst
     port map (
      Q(0) => \EN1_reg_n_0_[0]\,
      addr(10) => \genblk1[15].rams_sp_rf_rst_inst0_n_24\,
      addr(9) => \genblk1[15].rams_sp_rf_rst_inst0_n_25\,
      addr(8) => \genblk1[15].rams_sp_rf_rst_inst0_n_26\,
      addr(7) => \genblk1[15].rams_sp_rf_rst_inst0_n_27\,
      addr(6) => \genblk1[15].rams_sp_rf_rst_inst0_n_28\,
      addr(5) => \genblk1[15].rams_sp_rf_rst_inst0_n_29\,
      addr(4) => \genblk1[15].rams_sp_rf_rst_inst0_n_30\,
      addr(3) => \genblk1[15].rams_sp_rf_rst_inst0_n_31\,
      addr(2) => \genblk1[15].rams_sp_rf_rst_inst0_n_32\,
      addr(1) => \genblk1[15].rams_sp_rf_rst_inst0_n_33\,
      addr(0) => \genblk1[15].rams_sp_rf_rst_inst0_n_34\,
      dout0_out(23 downto 0) => \_DOUT_1[0]_0\(23 downto 0),
      m00_axis_aclk => m00_axis_aclk,
      s00_axis_tdata(23 downto 0) => s00_axis_tdata(23 downto 0),
      s00_axis_tuser(0) => s00_axis_tuser(11),
      s00_axis_tvalid => s00_axis_tvalid
    );
\genblk1[0].rams_sp_rf_rst_inst1\: entity work.test_bd_pixelReOrder_0_0_rams_sp_rf_rst_0
     port map (
      Q(0) => \EN2_reg_n_0_[0]\,
      addr(10 downto 0) => ADDR2(10 downto 0),
      dout0_out(23 downto 0) => \_DOUT_2[0]_1\(23 downto 0),
      m00_axis_aclk => m00_axis_aclk,
      s00_axis_tdata(23 downto 0) => s00_axis_tdata(23 downto 0),
      s00_axis_tuser(0) => s00_axis_tuser(11),
      s00_axis_tvalid => s00_axis_tvalid
    );
\genblk1[10].rams_sp_rf_rst_inst0\: entity work.test_bd_pixelReOrder_0_0_rams_sp_rf_rst_1
     port map (
      Q(0) => \EN1_reg_n_0_[10]\,
      addr(10) => \genblk1[15].rams_sp_rf_rst_inst0_n_24\,
      addr(9) => \genblk1[15].rams_sp_rf_rst_inst0_n_25\,
      addr(8) => \genblk1[15].rams_sp_rf_rst_inst0_n_26\,
      addr(7) => \genblk1[15].rams_sp_rf_rst_inst0_n_27\,
      addr(6) => \genblk1[15].rams_sp_rf_rst_inst0_n_28\,
      addr(5) => \genblk1[15].rams_sp_rf_rst_inst0_n_29\,
      addr(4) => \genblk1[15].rams_sp_rf_rst_inst0_n_30\,
      addr(3) => \genblk1[15].rams_sp_rf_rst_inst0_n_31\,
      addr(2) => \genblk1[15].rams_sp_rf_rst_inst0_n_32\,
      addr(1) => \genblk1[15].rams_sp_rf_rst_inst0_n_33\,
      addr(0) => \genblk1[15].rams_sp_rf_rst_inst0_n_34\,
      dout0_out(23 downto 0) => \_DOUT_1[10]_20\(23 downto 0),
      m00_axis_aclk => m00_axis_aclk,
      s00_axis_tdata(23 downto 0) => s00_axis_tdata(23 downto 0),
      s00_axis_tuser(0) => s00_axis_tuser(11),
      s00_axis_tvalid => s00_axis_tvalid
    );
\genblk1[10].rams_sp_rf_rst_inst1\: entity work.test_bd_pixelReOrder_0_0_rams_sp_rf_rst_2
     port map (
      Q(0) => \EN2_reg_n_0_[10]\,
      addr(10 downto 0) => ADDR2(10 downto 0),
      dout0_out(23 downto 0) => \_DOUT_2[10]_21\(23 downto 0),
      m00_axis_aclk => m00_axis_aclk,
      s00_axis_tdata(23 downto 0) => s00_axis_tdata(23 downto 0),
      s00_axis_tuser(0) => s00_axis_tuser(11),
      s00_axis_tvalid => s00_axis_tvalid
    );
\genblk1[11].rams_sp_rf_rst_inst0\: entity work.test_bd_pixelReOrder_0_0_rams_sp_rf_rst_3
     port map (
      Q(0) => \EN1_reg_n_0_[11]\,
      addr(10) => \genblk1[15].rams_sp_rf_rst_inst0_n_24\,
      addr(9) => \genblk1[15].rams_sp_rf_rst_inst0_n_25\,
      addr(8) => \genblk1[15].rams_sp_rf_rst_inst0_n_26\,
      addr(7) => \genblk1[15].rams_sp_rf_rst_inst0_n_27\,
      addr(6) => \genblk1[15].rams_sp_rf_rst_inst0_n_28\,
      addr(5) => \genblk1[15].rams_sp_rf_rst_inst0_n_29\,
      addr(4) => \genblk1[15].rams_sp_rf_rst_inst0_n_30\,
      addr(3) => \genblk1[15].rams_sp_rf_rst_inst0_n_31\,
      addr(2) => \genblk1[15].rams_sp_rf_rst_inst0_n_32\,
      addr(1) => \genblk1[15].rams_sp_rf_rst_inst0_n_33\,
      addr(0) => \genblk1[15].rams_sp_rf_rst_inst0_n_34\,
      ctrl_mux_out_1(1 downto 0) => ctrl_mux_out_1(1 downto 0),
      dout0_out(23 downto 0) => \_DOUT_1[10]_20\(23 downto 0),
      m00_axis_aclk => m00_axis_aclk,
      ram_reg_0_0 => \genblk1[11].rams_sp_rf_rst_inst0_n_0\,
      ram_reg_0_1 => \genblk1[11].rams_sp_rf_rst_inst0_n_1\,
      ram_reg_0_10 => \genblk1[11].rams_sp_rf_rst_inst0_n_10\,
      ram_reg_0_11 => \genblk1[11].rams_sp_rf_rst_inst0_n_11\,
      ram_reg_0_12 => \genblk1[11].rams_sp_rf_rst_inst0_n_12\,
      ram_reg_0_13 => \genblk1[11].rams_sp_rf_rst_inst0_n_13\,
      ram_reg_0_14 => \genblk1[11].rams_sp_rf_rst_inst0_n_14\,
      ram_reg_0_15 => \genblk1[11].rams_sp_rf_rst_inst0_n_15\,
      ram_reg_0_16 => \genblk1[11].rams_sp_rf_rst_inst0_n_16\,
      ram_reg_0_17 => \genblk1[11].rams_sp_rf_rst_inst0_n_17\,
      ram_reg_0_2 => \genblk1[11].rams_sp_rf_rst_inst0_n_2\,
      ram_reg_0_3 => \genblk1[11].rams_sp_rf_rst_inst0_n_3\,
      ram_reg_0_4 => \genblk1[11].rams_sp_rf_rst_inst0_n_4\,
      ram_reg_0_5 => \genblk1[11].rams_sp_rf_rst_inst0_n_5\,
      ram_reg_0_6 => \genblk1[11].rams_sp_rf_rst_inst0_n_6\,
      ram_reg_0_7 => \genblk1[11].rams_sp_rf_rst_inst0_n_7\,
      ram_reg_0_8 => \genblk1[11].rams_sp_rf_rst_inst0_n_8\,
      ram_reg_0_9 => \genblk1[11].rams_sp_rf_rst_inst0_n_9\,
      ram_reg_1_0 => \genblk1[11].rams_sp_rf_rst_inst0_n_18\,
      ram_reg_1_1 => \genblk1[11].rams_sp_rf_rst_inst0_n_19\,
      ram_reg_1_2 => \genblk1[11].rams_sp_rf_rst_inst0_n_20\,
      ram_reg_1_3 => \genblk1[11].rams_sp_rf_rst_inst0_n_21\,
      ram_reg_1_4 => \genblk1[11].rams_sp_rf_rst_inst0_n_22\,
      ram_reg_1_5 => \genblk1[11].rams_sp_rf_rst_inst0_n_23\,
      s00_axis_tdata(23 downto 0) => s00_axis_tdata(23 downto 0),
      s00_axis_tuser(0) => s00_axis_tuser(11),
      s00_axis_tvalid => s00_axis_tvalid,
      \y_reg[23]_i_1\(23 downto 0) => \_DOUT_1[9]_18\(23 downto 0),
      \y_reg[23]_i_1_0\(23 downto 0) => \_DOUT_1[8]_16\(23 downto 0)
    );
\genblk1[11].rams_sp_rf_rst_inst1\: entity work.test_bd_pixelReOrder_0_0_rams_sp_rf_rst_4
     port map (
      Q(0) => \EN2_reg_n_0_[11]\,
      addr(10 downto 0) => ADDR2(10 downto 0),
      ctrl_mux_out_2(1 downto 0) => ctrl_mux_out_2(1 downto 0),
      dout0_out(23 downto 0) => \_DOUT_2[10]_21\(23 downto 0),
      m00_axis_aclk => m00_axis_aclk,
      ram_reg_0_0 => \genblk1[11].rams_sp_rf_rst_inst1_n_0\,
      ram_reg_0_1 => \genblk1[11].rams_sp_rf_rst_inst1_n_1\,
      ram_reg_0_10 => \genblk1[11].rams_sp_rf_rst_inst1_n_10\,
      ram_reg_0_11 => \genblk1[11].rams_sp_rf_rst_inst1_n_11\,
      ram_reg_0_12 => \genblk1[11].rams_sp_rf_rst_inst1_n_12\,
      ram_reg_0_13 => \genblk1[11].rams_sp_rf_rst_inst1_n_13\,
      ram_reg_0_14 => \genblk1[11].rams_sp_rf_rst_inst1_n_14\,
      ram_reg_0_15 => \genblk1[11].rams_sp_rf_rst_inst1_n_15\,
      ram_reg_0_16 => \genblk1[11].rams_sp_rf_rst_inst1_n_16\,
      ram_reg_0_17 => \genblk1[11].rams_sp_rf_rst_inst1_n_17\,
      ram_reg_0_2 => \genblk1[11].rams_sp_rf_rst_inst1_n_2\,
      ram_reg_0_3 => \genblk1[11].rams_sp_rf_rst_inst1_n_3\,
      ram_reg_0_4 => \genblk1[11].rams_sp_rf_rst_inst1_n_4\,
      ram_reg_0_5 => \genblk1[11].rams_sp_rf_rst_inst1_n_5\,
      ram_reg_0_6 => \genblk1[11].rams_sp_rf_rst_inst1_n_6\,
      ram_reg_0_7 => \genblk1[11].rams_sp_rf_rst_inst1_n_7\,
      ram_reg_0_8 => \genblk1[11].rams_sp_rf_rst_inst1_n_8\,
      ram_reg_0_9 => \genblk1[11].rams_sp_rf_rst_inst1_n_9\,
      ram_reg_1_0 => \genblk1[11].rams_sp_rf_rst_inst1_n_18\,
      ram_reg_1_1 => \genblk1[11].rams_sp_rf_rst_inst1_n_19\,
      ram_reg_1_2 => \genblk1[11].rams_sp_rf_rst_inst1_n_20\,
      ram_reg_1_3 => \genblk1[11].rams_sp_rf_rst_inst1_n_21\,
      ram_reg_1_4 => \genblk1[11].rams_sp_rf_rst_inst1_n_22\,
      ram_reg_1_5 => \genblk1[11].rams_sp_rf_rst_inst1_n_23\,
      s00_axis_tdata(23 downto 0) => s00_axis_tdata(23 downto 0),
      s00_axis_tuser(0) => s00_axis_tuser(11),
      s00_axis_tvalid => s00_axis_tvalid,
      \y_reg[23]_i_1__0\(23 downto 0) => \_DOUT_2[9]_19\(23 downto 0),
      \y_reg[23]_i_1__0_0\(23 downto 0) => \_DOUT_2[8]_17\(23 downto 0)
    );
\genblk1[12].rams_sp_rf_rst_inst0\: entity work.test_bd_pixelReOrder_0_0_rams_sp_rf_rst_5
     port map (
      Q(0) => \EN1_reg_n_0_[12]\,
      addr(10) => \genblk1[15].rams_sp_rf_rst_inst0_n_24\,
      addr(9) => \genblk1[15].rams_sp_rf_rst_inst0_n_25\,
      addr(8) => \genblk1[15].rams_sp_rf_rst_inst0_n_26\,
      addr(7) => \genblk1[15].rams_sp_rf_rst_inst0_n_27\,
      addr(6) => \genblk1[15].rams_sp_rf_rst_inst0_n_28\,
      addr(5) => \genblk1[15].rams_sp_rf_rst_inst0_n_29\,
      addr(4) => \genblk1[15].rams_sp_rf_rst_inst0_n_30\,
      addr(3) => \genblk1[15].rams_sp_rf_rst_inst0_n_31\,
      addr(2) => \genblk1[15].rams_sp_rf_rst_inst0_n_32\,
      addr(1) => \genblk1[15].rams_sp_rf_rst_inst0_n_33\,
      addr(0) => \genblk1[15].rams_sp_rf_rst_inst0_n_34\,
      dout0_out(23 downto 0) => \_DOUT_1[12]_24\(23 downto 0),
      m00_axis_aclk => m00_axis_aclk,
      s00_axis_tdata(23 downto 0) => s00_axis_tdata(23 downto 0),
      s00_axis_tuser(0) => s00_axis_tuser(11),
      s00_axis_tvalid => s00_axis_tvalid
    );
\genblk1[12].rams_sp_rf_rst_inst1\: entity work.test_bd_pixelReOrder_0_0_rams_sp_rf_rst_6
     port map (
      Q(0) => \EN2_reg_n_0_[12]\,
      addr(10 downto 0) => ADDR2(10 downto 0),
      dout0_out(23 downto 0) => \_DOUT_2[12]_25\(23 downto 0),
      m00_axis_aclk => m00_axis_aclk,
      s00_axis_tdata(23 downto 0) => s00_axis_tdata(23 downto 0),
      s00_axis_tuser(0) => s00_axis_tuser(11),
      s00_axis_tvalid => s00_axis_tvalid
    );
\genblk1[13].rams_sp_rf_rst_inst0\: entity work.test_bd_pixelReOrder_0_0_rams_sp_rf_rst_7
     port map (
      Q(0) => \EN1_reg_n_0_[13]\,
      addr(10) => \genblk1[15].rams_sp_rf_rst_inst0_n_24\,
      addr(9) => \genblk1[15].rams_sp_rf_rst_inst0_n_25\,
      addr(8) => \genblk1[15].rams_sp_rf_rst_inst0_n_26\,
      addr(7) => \genblk1[15].rams_sp_rf_rst_inst0_n_27\,
      addr(6) => \genblk1[15].rams_sp_rf_rst_inst0_n_28\,
      addr(5) => \genblk1[15].rams_sp_rf_rst_inst0_n_29\,
      addr(4) => \genblk1[15].rams_sp_rf_rst_inst0_n_30\,
      addr(3) => \genblk1[15].rams_sp_rf_rst_inst0_n_31\,
      addr(2) => \genblk1[15].rams_sp_rf_rst_inst0_n_32\,
      addr(1) => \genblk1[15].rams_sp_rf_rst_inst0_n_33\,
      addr(0) => \genblk1[15].rams_sp_rf_rst_inst0_n_34\,
      dout0_out(23 downto 0) => \_DOUT_1[13]_26\(23 downto 0),
      m00_axis_aclk => m00_axis_aclk,
      s00_axis_tdata(23 downto 0) => s00_axis_tdata(23 downto 0),
      s00_axis_tuser(0) => s00_axis_tuser(11),
      s00_axis_tvalid => s00_axis_tvalid
    );
\genblk1[13].rams_sp_rf_rst_inst1\: entity work.test_bd_pixelReOrder_0_0_rams_sp_rf_rst_8
     port map (
      Q(0) => \EN2_reg_n_0_[13]\,
      addr(10 downto 0) => ADDR2(10 downto 0),
      dout0_out(23 downto 0) => \_DOUT_2[13]_27\(23 downto 0),
      m00_axis_aclk => m00_axis_aclk,
      s00_axis_tdata(23 downto 0) => s00_axis_tdata(23 downto 0),
      s00_axis_tuser(0) => s00_axis_tuser(11),
      s00_axis_tvalid => s00_axis_tvalid
    );
\genblk1[14].rams_sp_rf_rst_inst0\: entity work.test_bd_pixelReOrder_0_0_rams_sp_rf_rst_9
     port map (
      Q(0) => \EN1_reg_n_0_[14]\,
      addr(10) => \genblk1[15].rams_sp_rf_rst_inst0_n_24\,
      addr(9) => \genblk1[15].rams_sp_rf_rst_inst0_n_25\,
      addr(8) => \genblk1[15].rams_sp_rf_rst_inst0_n_26\,
      addr(7) => \genblk1[15].rams_sp_rf_rst_inst0_n_27\,
      addr(6) => \genblk1[15].rams_sp_rf_rst_inst0_n_28\,
      addr(5) => \genblk1[15].rams_sp_rf_rst_inst0_n_29\,
      addr(4) => \genblk1[15].rams_sp_rf_rst_inst0_n_30\,
      addr(3) => \genblk1[15].rams_sp_rf_rst_inst0_n_31\,
      addr(2) => \genblk1[15].rams_sp_rf_rst_inst0_n_32\,
      addr(1) => \genblk1[15].rams_sp_rf_rst_inst0_n_33\,
      addr(0) => \genblk1[15].rams_sp_rf_rst_inst0_n_34\,
      dout0_out(23 downto 0) => \_DOUT_1[14]_28\(23 downto 0),
      m00_axis_aclk => m00_axis_aclk,
      s00_axis_tdata(23 downto 0) => s00_axis_tdata(23 downto 0),
      s00_axis_tuser(0) => s00_axis_tuser(11),
      s00_axis_tvalid => s00_axis_tvalid
    );
\genblk1[14].rams_sp_rf_rst_inst1\: entity work.test_bd_pixelReOrder_0_0_rams_sp_rf_rst_10
     port map (
      Q(0) => \EN2_reg_n_0_[14]\,
      addr(10 downto 0) => ADDR2(10 downto 0),
      dout0_out(23 downto 0) => \_DOUT_2[14]_29\(23 downto 0),
      m00_axis_aclk => m00_axis_aclk,
      s00_axis_tdata(23 downto 0) => s00_axis_tdata(23 downto 0),
      s00_axis_tuser(0) => s00_axis_tuser(11),
      s00_axis_tvalid => s00_axis_tvalid
    );
\genblk1[15].rams_sp_rf_rst_inst0\: entity work.test_bd_pixelReOrder_0_0_rams_sp_rf_rst_11
     port map (
      Q(10) => \adr_read_reorder_data_mem1_reg_n_0_[10]\,
      Q(9) => \adr_read_reorder_data_mem1_reg_n_0_[9]\,
      Q(8) => \adr_read_reorder_data_mem1_reg_n_0_[8]\,
      Q(7) => \adr_read_reorder_data_mem1_reg_n_0_[7]\,
      Q(6) => \adr_read_reorder_data_mem1_reg_n_0_[6]\,
      Q(5) => \adr_read_reorder_data_mem1_reg_n_0_[5]\,
      Q(4) => \adr_read_reorder_data_mem1_reg_n_0_[4]\,
      Q(3) => \adr_read_reorder_data_mem1_reg_n_0_[3]\,
      Q(2) => \adr_read_reorder_data_mem1_reg_n_0_[2]\,
      Q(1) => \adr_read_reorder_data_mem1_reg_n_0_[1]\,
      Q(0) => \adr_read_reorder_data_mem1_reg_n_0_[0]\,
      addr(10) => \genblk1[15].rams_sp_rf_rst_inst0_n_24\,
      addr(9) => \genblk1[15].rams_sp_rf_rst_inst0_n_25\,
      addr(8) => \genblk1[15].rams_sp_rf_rst_inst0_n_26\,
      addr(7) => \genblk1[15].rams_sp_rf_rst_inst0_n_27\,
      addr(6) => \genblk1[15].rams_sp_rf_rst_inst0_n_28\,
      addr(5) => \genblk1[15].rams_sp_rf_rst_inst0_n_29\,
      addr(4) => \genblk1[15].rams_sp_rf_rst_inst0_n_30\,
      addr(3) => \genblk1[15].rams_sp_rf_rst_inst0_n_31\,
      addr(2) => \genblk1[15].rams_sp_rf_rst_inst0_n_32\,
      addr(1) => \genblk1[15].rams_sp_rf_rst_inst0_n_33\,
      addr(0) => \genblk1[15].rams_sp_rf_rst_inst0_n_34\,
      ctrl_mux_out_1(1 downto 0) => ctrl_mux_out_1(1 downto 0),
      dout0_out(23 downto 0) => \_DOUT_1[14]_28\(23 downto 0),
      m00_axis_aclk => m00_axis_aclk,
      ram_reg_0_0 => \genblk1[15].rams_sp_rf_rst_inst0_n_0\,
      ram_reg_0_1 => \genblk1[15].rams_sp_rf_rst_inst0_n_1\,
      ram_reg_0_10 => \genblk1[15].rams_sp_rf_rst_inst0_n_10\,
      ram_reg_0_11 => \genblk1[15].rams_sp_rf_rst_inst0_n_11\,
      ram_reg_0_12 => \genblk1[15].rams_sp_rf_rst_inst0_n_12\,
      ram_reg_0_13 => \genblk1[15].rams_sp_rf_rst_inst0_n_13\,
      ram_reg_0_14 => \genblk1[15].rams_sp_rf_rst_inst0_n_14\,
      ram_reg_0_15 => \genblk1[15].rams_sp_rf_rst_inst0_n_15\,
      ram_reg_0_16 => \genblk1[15].rams_sp_rf_rst_inst0_n_16\,
      ram_reg_0_17 => \genblk1[15].rams_sp_rf_rst_inst0_n_17\,
      ram_reg_0_18(0) => \EN1_reg_n_0_[15]\,
      ram_reg_0_2 => \genblk1[15].rams_sp_rf_rst_inst0_n_2\,
      ram_reg_0_3 => \genblk1[15].rams_sp_rf_rst_inst0_n_3\,
      ram_reg_0_4 => \genblk1[15].rams_sp_rf_rst_inst0_n_4\,
      ram_reg_0_5 => \genblk1[15].rams_sp_rf_rst_inst0_n_5\,
      ram_reg_0_6 => \genblk1[15].rams_sp_rf_rst_inst0_n_6\,
      ram_reg_0_7 => \genblk1[15].rams_sp_rf_rst_inst0_n_7\,
      ram_reg_0_8 => \genblk1[15].rams_sp_rf_rst_inst0_n_8\,
      ram_reg_0_9 => \genblk1[15].rams_sp_rf_rst_inst0_n_9\,
      ram_reg_1_0 => \genblk1[15].rams_sp_rf_rst_inst0_n_18\,
      ram_reg_1_1 => \genblk1[15].rams_sp_rf_rst_inst0_n_19\,
      ram_reg_1_2 => \genblk1[15].rams_sp_rf_rst_inst0_n_20\,
      ram_reg_1_3 => \genblk1[15].rams_sp_rf_rst_inst0_n_21\,
      ram_reg_1_4 => \genblk1[15].rams_sp_rf_rst_inst0_n_22\,
      ram_reg_1_5 => \genblk1[15].rams_sp_rf_rst_inst0_n_23\,
      s00_axis_tdata(23 downto 0) => s00_axis_tdata(23 downto 0),
      s00_axis_tuser(11 downto 0) => s00_axis_tuser(11 downto 0),
      s00_axis_tvalid => s00_axis_tvalid,
      \y_reg[23]_i_1\(23 downto 0) => \_DOUT_1[13]_26\(23 downto 0),
      \y_reg[23]_i_1_0\(23 downto 0) => \_DOUT_1[12]_24\(23 downto 0)
    );
\genblk1[15].rams_sp_rf_rst_inst1\: entity work.test_bd_pixelReOrder_0_0_rams_sp_rf_rst_12
     port map (
      Q(10) => \adr_read_reorder_data_mem2_reg_n_0_[10]\,
      Q(9) => \adr_read_reorder_data_mem2_reg_n_0_[9]\,
      Q(8) => \adr_read_reorder_data_mem2_reg_n_0_[8]\,
      Q(7) => \adr_read_reorder_data_mem2_reg_n_0_[7]\,
      Q(6) => \adr_read_reorder_data_mem2_reg_n_0_[6]\,
      Q(5) => \adr_read_reorder_data_mem2_reg_n_0_[5]\,
      Q(4) => \adr_read_reorder_data_mem2_reg_n_0_[4]\,
      Q(3) => \adr_read_reorder_data_mem2_reg_n_0_[3]\,
      Q(2) => \adr_read_reorder_data_mem2_reg_n_0_[2]\,
      Q(1) => \adr_read_reorder_data_mem2_reg_n_0_[1]\,
      Q(0) => \adr_read_reorder_data_mem2_reg_n_0_[0]\,
      addr(10 downto 0) => ADDR2(10 downto 0),
      ctrl_mux_out_2(1 downto 0) => ctrl_mux_out_2(1 downto 0),
      dout0_out(23 downto 0) => \_DOUT_2[14]_29\(23 downto 0),
      m00_axis_aclk => m00_axis_aclk,
      ram_reg_0_0 => \genblk1[15].rams_sp_rf_rst_inst1_n_0\,
      ram_reg_0_1 => \genblk1[15].rams_sp_rf_rst_inst1_n_1\,
      ram_reg_0_10 => \genblk1[15].rams_sp_rf_rst_inst1_n_10\,
      ram_reg_0_11 => \genblk1[15].rams_sp_rf_rst_inst1_n_11\,
      ram_reg_0_12 => \genblk1[15].rams_sp_rf_rst_inst1_n_12\,
      ram_reg_0_13 => \genblk1[15].rams_sp_rf_rst_inst1_n_13\,
      ram_reg_0_14 => \genblk1[15].rams_sp_rf_rst_inst1_n_14\,
      ram_reg_0_15 => \genblk1[15].rams_sp_rf_rst_inst1_n_15\,
      ram_reg_0_16 => \genblk1[15].rams_sp_rf_rst_inst1_n_16\,
      ram_reg_0_17 => \genblk1[15].rams_sp_rf_rst_inst1_n_17\,
      ram_reg_0_18(0) => \EN2_reg_n_0_[15]\,
      ram_reg_0_2 => \genblk1[15].rams_sp_rf_rst_inst1_n_2\,
      ram_reg_0_3 => \genblk1[15].rams_sp_rf_rst_inst1_n_3\,
      ram_reg_0_4 => \genblk1[15].rams_sp_rf_rst_inst1_n_4\,
      ram_reg_0_5 => \genblk1[15].rams_sp_rf_rst_inst1_n_5\,
      ram_reg_0_6 => \genblk1[15].rams_sp_rf_rst_inst1_n_6\,
      ram_reg_0_7 => \genblk1[15].rams_sp_rf_rst_inst1_n_7\,
      ram_reg_0_8 => \genblk1[15].rams_sp_rf_rst_inst1_n_8\,
      ram_reg_0_9 => \genblk1[15].rams_sp_rf_rst_inst1_n_9\,
      ram_reg_1_0 => \genblk1[15].rams_sp_rf_rst_inst1_n_18\,
      ram_reg_1_1 => \genblk1[15].rams_sp_rf_rst_inst1_n_19\,
      ram_reg_1_2 => \genblk1[15].rams_sp_rf_rst_inst1_n_20\,
      ram_reg_1_3 => \genblk1[15].rams_sp_rf_rst_inst1_n_21\,
      ram_reg_1_4 => \genblk1[15].rams_sp_rf_rst_inst1_n_22\,
      ram_reg_1_5 => \genblk1[15].rams_sp_rf_rst_inst1_n_23\,
      s00_axis_tdata(23 downto 0) => s00_axis_tdata(23 downto 0),
      s00_axis_tuser(11 downto 0) => s00_axis_tuser(11 downto 0),
      s00_axis_tvalid => s00_axis_tvalid,
      \y_reg[23]_i_1__0\(23 downto 0) => \_DOUT_2[13]_27\(23 downto 0),
      \y_reg[23]_i_1__0_0\(23 downto 0) => \_DOUT_2[12]_25\(23 downto 0)
    );
\genblk1[1].rams_sp_rf_rst_inst0\: entity work.test_bd_pixelReOrder_0_0_rams_sp_rf_rst_13
     port map (
      Q(0) => \EN1_reg_n_0_[1]\,
      addr(10) => \genblk1[15].rams_sp_rf_rst_inst0_n_24\,
      addr(9) => \genblk1[15].rams_sp_rf_rst_inst0_n_25\,
      addr(8) => \genblk1[15].rams_sp_rf_rst_inst0_n_26\,
      addr(7) => \genblk1[15].rams_sp_rf_rst_inst0_n_27\,
      addr(6) => \genblk1[15].rams_sp_rf_rst_inst0_n_28\,
      addr(5) => \genblk1[15].rams_sp_rf_rst_inst0_n_29\,
      addr(4) => \genblk1[15].rams_sp_rf_rst_inst0_n_30\,
      addr(3) => \genblk1[15].rams_sp_rf_rst_inst0_n_31\,
      addr(2) => \genblk1[15].rams_sp_rf_rst_inst0_n_32\,
      addr(1) => \genblk1[15].rams_sp_rf_rst_inst0_n_33\,
      addr(0) => \genblk1[15].rams_sp_rf_rst_inst0_n_34\,
      dout0_out(23 downto 0) => \_DOUT_1[1]_2\(23 downto 0),
      m00_axis_aclk => m00_axis_aclk,
      s00_axis_tdata(23 downto 0) => s00_axis_tdata(23 downto 0),
      s00_axis_tuser(0) => s00_axis_tuser(11),
      s00_axis_tvalid => s00_axis_tvalid
    );
\genblk1[1].rams_sp_rf_rst_inst1\: entity work.test_bd_pixelReOrder_0_0_rams_sp_rf_rst_14
     port map (
      Q(0) => \EN2_reg_n_0_[1]\,
      addr(10 downto 0) => ADDR2(10 downto 0),
      dout0_out(23 downto 0) => \_DOUT_2[1]_3\(23 downto 0),
      m00_axis_aclk => m00_axis_aclk,
      s00_axis_tdata(23 downto 0) => s00_axis_tdata(23 downto 0),
      s00_axis_tuser(0) => s00_axis_tuser(11),
      s00_axis_tvalid => s00_axis_tvalid
    );
\genblk1[2].rams_sp_rf_rst_inst0\: entity work.test_bd_pixelReOrder_0_0_rams_sp_rf_rst_15
     port map (
      Q(0) => \EN1_reg_n_0_[2]\,
      addr(10) => \genblk1[15].rams_sp_rf_rst_inst0_n_24\,
      addr(9) => \genblk1[15].rams_sp_rf_rst_inst0_n_25\,
      addr(8) => \genblk1[15].rams_sp_rf_rst_inst0_n_26\,
      addr(7) => \genblk1[15].rams_sp_rf_rst_inst0_n_27\,
      addr(6) => \genblk1[15].rams_sp_rf_rst_inst0_n_28\,
      addr(5) => \genblk1[15].rams_sp_rf_rst_inst0_n_29\,
      addr(4) => \genblk1[15].rams_sp_rf_rst_inst0_n_30\,
      addr(3) => \genblk1[15].rams_sp_rf_rst_inst0_n_31\,
      addr(2) => \genblk1[15].rams_sp_rf_rst_inst0_n_32\,
      addr(1) => \genblk1[15].rams_sp_rf_rst_inst0_n_33\,
      addr(0) => \genblk1[15].rams_sp_rf_rst_inst0_n_34\,
      dout0_out(23 downto 0) => \_DOUT_1[2]_4\(23 downto 0),
      m00_axis_aclk => m00_axis_aclk,
      s00_axis_tdata(23 downto 0) => s00_axis_tdata(23 downto 0),
      s00_axis_tuser(0) => s00_axis_tuser(11),
      s00_axis_tvalid => s00_axis_tvalid
    );
\genblk1[2].rams_sp_rf_rst_inst1\: entity work.test_bd_pixelReOrder_0_0_rams_sp_rf_rst_16
     port map (
      Q(0) => \EN2_reg_n_0_[2]\,
      addr(10 downto 0) => ADDR2(10 downto 0),
      dout0_out(23 downto 0) => \_DOUT_2[2]_5\(23 downto 0),
      m00_axis_aclk => m00_axis_aclk,
      s00_axis_tdata(23 downto 0) => s00_axis_tdata(23 downto 0),
      s00_axis_tuser(0) => s00_axis_tuser(11),
      s00_axis_tvalid => s00_axis_tvalid
    );
\genblk1[3].rams_sp_rf_rst_inst0\: entity work.test_bd_pixelReOrder_0_0_rams_sp_rf_rst_17
     port map (
      D(23) => \genblk1[3].rams_sp_rf_rst_inst0_n_0\,
      D(22) => \genblk1[3].rams_sp_rf_rst_inst0_n_1\,
      D(21) => \genblk1[3].rams_sp_rf_rst_inst0_n_2\,
      D(20) => \genblk1[3].rams_sp_rf_rst_inst0_n_3\,
      D(19) => \genblk1[3].rams_sp_rf_rst_inst0_n_4\,
      D(18) => \genblk1[3].rams_sp_rf_rst_inst0_n_5\,
      D(17) => \genblk1[3].rams_sp_rf_rst_inst0_n_6\,
      D(16) => \genblk1[3].rams_sp_rf_rst_inst0_n_7\,
      D(15) => \genblk1[3].rams_sp_rf_rst_inst0_n_8\,
      D(14) => \genblk1[3].rams_sp_rf_rst_inst0_n_9\,
      D(13) => \genblk1[3].rams_sp_rf_rst_inst0_n_10\,
      D(12) => \genblk1[3].rams_sp_rf_rst_inst0_n_11\,
      D(11) => \genblk1[3].rams_sp_rf_rst_inst0_n_12\,
      D(10) => \genblk1[3].rams_sp_rf_rst_inst0_n_13\,
      D(9) => \genblk1[3].rams_sp_rf_rst_inst0_n_14\,
      D(8) => \genblk1[3].rams_sp_rf_rst_inst0_n_15\,
      D(7) => \genblk1[3].rams_sp_rf_rst_inst0_n_16\,
      D(6) => \genblk1[3].rams_sp_rf_rst_inst0_n_17\,
      D(5) => \genblk1[3].rams_sp_rf_rst_inst0_n_18\,
      D(4) => \genblk1[3].rams_sp_rf_rst_inst0_n_19\,
      D(3) => \genblk1[3].rams_sp_rf_rst_inst0_n_20\,
      D(2) => \genblk1[3].rams_sp_rf_rst_inst0_n_21\,
      D(1) => \genblk1[3].rams_sp_rf_rst_inst0_n_22\,
      D(0) => \genblk1[3].rams_sp_rf_rst_inst0_n_23\,
      Q(0) => \EN1_reg_n_0_[3]\,
      addr(10) => \genblk1[15].rams_sp_rf_rst_inst0_n_24\,
      addr(9) => \genblk1[15].rams_sp_rf_rst_inst0_n_25\,
      addr(8) => \genblk1[15].rams_sp_rf_rst_inst0_n_26\,
      addr(7) => \genblk1[15].rams_sp_rf_rst_inst0_n_27\,
      addr(6) => \genblk1[15].rams_sp_rf_rst_inst0_n_28\,
      addr(5) => \genblk1[15].rams_sp_rf_rst_inst0_n_29\,
      addr(4) => \genblk1[15].rams_sp_rf_rst_inst0_n_30\,
      addr(3) => \genblk1[15].rams_sp_rf_rst_inst0_n_31\,
      addr(2) => \genblk1[15].rams_sp_rf_rst_inst0_n_32\,
      addr(1) => \genblk1[15].rams_sp_rf_rst_inst0_n_33\,
      addr(0) => \genblk1[15].rams_sp_rf_rst_inst0_n_34\,
      ctrl_mux_out_1(4 downto 0) => ctrl_mux_out_1(4 downto 0),
      dout0_out(23 downto 0) => \_DOUT_1[2]_4\(23 downto 0),
      m00_axis_aclk => m00_axis_aclk,
      s00_axis_tdata(23 downto 0) => s00_axis_tdata(23 downto 0),
      s00_axis_tuser(0) => s00_axis_tuser(11),
      s00_axis_tvalid => s00_axis_tvalid,
      \y_reg[0]\ => \genblk1[11].rams_sp_rf_rst_inst0_n_0\,
      \y_reg[0]_0\ => \genblk1[15].rams_sp_rf_rst_inst0_n_0\,
      \y_reg[0]_i_1_0\ => \genblk1[7].rams_sp_rf_rst_inst0_n_0\,
      \y_reg[10]\ => \genblk1[11].rams_sp_rf_rst_inst0_n_10\,
      \y_reg[10]_0\ => \genblk1[15].rams_sp_rf_rst_inst0_n_10\,
      \y_reg[10]_i_1_0\ => \genblk1[7].rams_sp_rf_rst_inst0_n_10\,
      \y_reg[11]\ => \genblk1[11].rams_sp_rf_rst_inst0_n_11\,
      \y_reg[11]_0\ => \genblk1[15].rams_sp_rf_rst_inst0_n_11\,
      \y_reg[11]_i_1_0\ => \genblk1[7].rams_sp_rf_rst_inst0_n_11\,
      \y_reg[12]\ => \genblk1[11].rams_sp_rf_rst_inst0_n_12\,
      \y_reg[12]_0\ => \genblk1[15].rams_sp_rf_rst_inst0_n_12\,
      \y_reg[12]_i_1_0\ => \genblk1[7].rams_sp_rf_rst_inst0_n_12\,
      \y_reg[13]\ => \genblk1[11].rams_sp_rf_rst_inst0_n_13\,
      \y_reg[13]_0\ => \genblk1[15].rams_sp_rf_rst_inst0_n_13\,
      \y_reg[13]_i_1_0\ => \genblk1[7].rams_sp_rf_rst_inst0_n_13\,
      \y_reg[14]\ => \genblk1[11].rams_sp_rf_rst_inst0_n_14\,
      \y_reg[14]_0\ => \genblk1[15].rams_sp_rf_rst_inst0_n_14\,
      \y_reg[14]_i_1_0\ => \genblk1[7].rams_sp_rf_rst_inst0_n_14\,
      \y_reg[15]\ => \genblk1[11].rams_sp_rf_rst_inst0_n_15\,
      \y_reg[15]_0\ => \genblk1[15].rams_sp_rf_rst_inst0_n_15\,
      \y_reg[15]_i_1_0\ => \genblk1[7].rams_sp_rf_rst_inst0_n_15\,
      \y_reg[16]\ => \genblk1[11].rams_sp_rf_rst_inst0_n_16\,
      \y_reg[16]_0\ => \genblk1[15].rams_sp_rf_rst_inst0_n_16\,
      \y_reg[16]_i_1_0\ => \genblk1[7].rams_sp_rf_rst_inst0_n_16\,
      \y_reg[17]\ => \genblk1[11].rams_sp_rf_rst_inst0_n_17\,
      \y_reg[17]_0\ => \genblk1[15].rams_sp_rf_rst_inst0_n_17\,
      \y_reg[17]_i_1_0\ => \genblk1[7].rams_sp_rf_rst_inst0_n_17\,
      \y_reg[18]\ => \genblk1[11].rams_sp_rf_rst_inst0_n_18\,
      \y_reg[18]_0\ => \genblk1[15].rams_sp_rf_rst_inst0_n_18\,
      \y_reg[18]_i_1_0\ => \genblk1[7].rams_sp_rf_rst_inst0_n_18\,
      \y_reg[19]\ => \genblk1[11].rams_sp_rf_rst_inst0_n_19\,
      \y_reg[19]_0\ => \genblk1[15].rams_sp_rf_rst_inst0_n_19\,
      \y_reg[19]_i_1_0\ => \genblk1[7].rams_sp_rf_rst_inst0_n_19\,
      \y_reg[1]\ => \genblk1[11].rams_sp_rf_rst_inst0_n_1\,
      \y_reg[1]_0\ => \genblk1[15].rams_sp_rf_rst_inst0_n_1\,
      \y_reg[1]_i_1_0\ => \genblk1[7].rams_sp_rf_rst_inst0_n_1\,
      \y_reg[20]\ => \genblk1[11].rams_sp_rf_rst_inst0_n_20\,
      \y_reg[20]_0\ => \genblk1[15].rams_sp_rf_rst_inst0_n_20\,
      \y_reg[20]_i_1_0\ => \genblk1[7].rams_sp_rf_rst_inst0_n_20\,
      \y_reg[21]\ => \genblk1[11].rams_sp_rf_rst_inst0_n_21\,
      \y_reg[21]_0\ => \genblk1[15].rams_sp_rf_rst_inst0_n_21\,
      \y_reg[21]_i_1_0\ => \genblk1[7].rams_sp_rf_rst_inst0_n_21\,
      \y_reg[22]\ => \genblk1[11].rams_sp_rf_rst_inst0_n_22\,
      \y_reg[22]_0\ => \genblk1[15].rams_sp_rf_rst_inst0_n_22\,
      \y_reg[22]_i_1_0\ => \genblk1[7].rams_sp_rf_rst_inst0_n_22\,
      \y_reg[23]\ => \genblk1[11].rams_sp_rf_rst_inst0_n_23\,
      \y_reg[23]_0\ => \genblk1[15].rams_sp_rf_rst_inst0_n_23\,
      \y_reg[23]_i_1_0\ => \genblk1[7].rams_sp_rf_rst_inst0_n_23\,
      \y_reg[23]_i_3_0\(23 downto 0) => \_DOUT_1[1]_2\(23 downto 0),
      \y_reg[23]_i_3_1\(23 downto 0) => \_DOUT_1[0]_0\(23 downto 0),
      \y_reg[2]\ => \genblk1[11].rams_sp_rf_rst_inst0_n_2\,
      \y_reg[2]_0\ => \genblk1[15].rams_sp_rf_rst_inst0_n_2\,
      \y_reg[2]_i_1_0\ => \genblk1[7].rams_sp_rf_rst_inst0_n_2\,
      \y_reg[3]\ => \genblk1[11].rams_sp_rf_rst_inst0_n_3\,
      \y_reg[3]_0\ => \genblk1[15].rams_sp_rf_rst_inst0_n_3\,
      \y_reg[3]_i_1_0\ => \genblk1[7].rams_sp_rf_rst_inst0_n_3\,
      \y_reg[4]\ => \genblk1[11].rams_sp_rf_rst_inst0_n_4\,
      \y_reg[4]_0\ => \genblk1[15].rams_sp_rf_rst_inst0_n_4\,
      \y_reg[4]_i_1_0\ => \genblk1[7].rams_sp_rf_rst_inst0_n_4\,
      \y_reg[5]\ => \genblk1[11].rams_sp_rf_rst_inst0_n_5\,
      \y_reg[5]_0\ => \genblk1[15].rams_sp_rf_rst_inst0_n_5\,
      \y_reg[5]_i_1_0\ => \genblk1[7].rams_sp_rf_rst_inst0_n_5\,
      \y_reg[6]\ => \genblk1[11].rams_sp_rf_rst_inst0_n_6\,
      \y_reg[6]_0\ => \genblk1[15].rams_sp_rf_rst_inst0_n_6\,
      \y_reg[6]_i_1_0\ => \genblk1[7].rams_sp_rf_rst_inst0_n_6\,
      \y_reg[7]\ => \genblk1[11].rams_sp_rf_rst_inst0_n_7\,
      \y_reg[7]_0\ => \genblk1[15].rams_sp_rf_rst_inst0_n_7\,
      \y_reg[7]_i_1_0\ => \genblk1[7].rams_sp_rf_rst_inst0_n_7\,
      \y_reg[8]\ => \genblk1[11].rams_sp_rf_rst_inst0_n_8\,
      \y_reg[8]_0\ => \genblk1[15].rams_sp_rf_rst_inst0_n_8\,
      \y_reg[8]_i_1_0\ => \genblk1[7].rams_sp_rf_rst_inst0_n_8\,
      \y_reg[9]\ => \genblk1[11].rams_sp_rf_rst_inst0_n_9\,
      \y_reg[9]_0\ => \genblk1[15].rams_sp_rf_rst_inst0_n_9\,
      \y_reg[9]_i_1_0\ => \genblk1[7].rams_sp_rf_rst_inst0_n_9\
    );
\genblk1[3].rams_sp_rf_rst_inst1\: entity work.test_bd_pixelReOrder_0_0_rams_sp_rf_rst_18
     port map (
      D(23) => \genblk1[3].rams_sp_rf_rst_inst1_n_0\,
      D(22) => \genblk1[3].rams_sp_rf_rst_inst1_n_1\,
      D(21) => \genblk1[3].rams_sp_rf_rst_inst1_n_2\,
      D(20) => \genblk1[3].rams_sp_rf_rst_inst1_n_3\,
      D(19) => \genblk1[3].rams_sp_rf_rst_inst1_n_4\,
      D(18) => \genblk1[3].rams_sp_rf_rst_inst1_n_5\,
      D(17) => \genblk1[3].rams_sp_rf_rst_inst1_n_6\,
      D(16) => \genblk1[3].rams_sp_rf_rst_inst1_n_7\,
      D(15) => \genblk1[3].rams_sp_rf_rst_inst1_n_8\,
      D(14) => \genblk1[3].rams_sp_rf_rst_inst1_n_9\,
      D(13) => \genblk1[3].rams_sp_rf_rst_inst1_n_10\,
      D(12) => \genblk1[3].rams_sp_rf_rst_inst1_n_11\,
      D(11) => \genblk1[3].rams_sp_rf_rst_inst1_n_12\,
      D(10) => \genblk1[3].rams_sp_rf_rst_inst1_n_13\,
      D(9) => \genblk1[3].rams_sp_rf_rst_inst1_n_14\,
      D(8) => \genblk1[3].rams_sp_rf_rst_inst1_n_15\,
      D(7) => \genblk1[3].rams_sp_rf_rst_inst1_n_16\,
      D(6) => \genblk1[3].rams_sp_rf_rst_inst1_n_17\,
      D(5) => \genblk1[3].rams_sp_rf_rst_inst1_n_18\,
      D(4) => \genblk1[3].rams_sp_rf_rst_inst1_n_19\,
      D(3) => \genblk1[3].rams_sp_rf_rst_inst1_n_20\,
      D(2) => \genblk1[3].rams_sp_rf_rst_inst1_n_21\,
      D(1) => \genblk1[3].rams_sp_rf_rst_inst1_n_22\,
      D(0) => \genblk1[3].rams_sp_rf_rst_inst1_n_23\,
      Q(0) => \EN2_reg_n_0_[3]\,
      addr(10 downto 0) => ADDR2(10 downto 0),
      ctrl_mux_out_2(4 downto 0) => ctrl_mux_out_2(4 downto 0),
      dout0_out(23 downto 0) => \_DOUT_2[2]_5\(23 downto 0),
      m00_axis_aclk => m00_axis_aclk,
      s00_axis_tdata(23 downto 0) => s00_axis_tdata(23 downto 0),
      s00_axis_tuser(0) => s00_axis_tuser(11),
      s00_axis_tvalid => s00_axis_tvalid,
      \y_reg[0]\ => \genblk1[11].rams_sp_rf_rst_inst1_n_0\,
      \y_reg[0]_0\ => \genblk1[15].rams_sp_rf_rst_inst1_n_0\,
      \y_reg[0]_i_1__0_0\ => \genblk1[7].rams_sp_rf_rst_inst1_n_0\,
      \y_reg[10]\ => \genblk1[11].rams_sp_rf_rst_inst1_n_10\,
      \y_reg[10]_0\ => \genblk1[15].rams_sp_rf_rst_inst1_n_10\,
      \y_reg[10]_i_1__0_0\ => \genblk1[7].rams_sp_rf_rst_inst1_n_10\,
      \y_reg[11]\ => \genblk1[11].rams_sp_rf_rst_inst1_n_11\,
      \y_reg[11]_0\ => \genblk1[15].rams_sp_rf_rst_inst1_n_11\,
      \y_reg[11]_i_1__0_0\ => \genblk1[7].rams_sp_rf_rst_inst1_n_11\,
      \y_reg[12]\ => \genblk1[11].rams_sp_rf_rst_inst1_n_12\,
      \y_reg[12]_0\ => \genblk1[15].rams_sp_rf_rst_inst1_n_12\,
      \y_reg[12]_i_1__0_0\ => \genblk1[7].rams_sp_rf_rst_inst1_n_12\,
      \y_reg[13]\ => \genblk1[11].rams_sp_rf_rst_inst1_n_13\,
      \y_reg[13]_0\ => \genblk1[15].rams_sp_rf_rst_inst1_n_13\,
      \y_reg[13]_i_1__0_0\ => \genblk1[7].rams_sp_rf_rst_inst1_n_13\,
      \y_reg[14]\ => \genblk1[11].rams_sp_rf_rst_inst1_n_14\,
      \y_reg[14]_0\ => \genblk1[15].rams_sp_rf_rst_inst1_n_14\,
      \y_reg[14]_i_1__0_0\ => \genblk1[7].rams_sp_rf_rst_inst1_n_14\,
      \y_reg[15]\ => \genblk1[11].rams_sp_rf_rst_inst1_n_15\,
      \y_reg[15]_0\ => \genblk1[15].rams_sp_rf_rst_inst1_n_15\,
      \y_reg[15]_i_1__0_0\ => \genblk1[7].rams_sp_rf_rst_inst1_n_15\,
      \y_reg[16]\ => \genblk1[11].rams_sp_rf_rst_inst1_n_16\,
      \y_reg[16]_0\ => \genblk1[15].rams_sp_rf_rst_inst1_n_16\,
      \y_reg[16]_i_1__0_0\ => \genblk1[7].rams_sp_rf_rst_inst1_n_16\,
      \y_reg[17]\ => \genblk1[11].rams_sp_rf_rst_inst1_n_17\,
      \y_reg[17]_0\ => \genblk1[15].rams_sp_rf_rst_inst1_n_17\,
      \y_reg[17]_i_1__0_0\ => \genblk1[7].rams_sp_rf_rst_inst1_n_17\,
      \y_reg[18]\ => \genblk1[11].rams_sp_rf_rst_inst1_n_18\,
      \y_reg[18]_0\ => \genblk1[15].rams_sp_rf_rst_inst1_n_18\,
      \y_reg[18]_i_1__0_0\ => \genblk1[7].rams_sp_rf_rst_inst1_n_18\,
      \y_reg[19]\ => \genblk1[11].rams_sp_rf_rst_inst1_n_19\,
      \y_reg[19]_0\ => \genblk1[15].rams_sp_rf_rst_inst1_n_19\,
      \y_reg[19]_i_1__0_0\ => \genblk1[7].rams_sp_rf_rst_inst1_n_19\,
      \y_reg[1]\ => \genblk1[11].rams_sp_rf_rst_inst1_n_1\,
      \y_reg[1]_0\ => \genblk1[15].rams_sp_rf_rst_inst1_n_1\,
      \y_reg[1]_i_1__0_0\ => \genblk1[7].rams_sp_rf_rst_inst1_n_1\,
      \y_reg[20]\ => \genblk1[11].rams_sp_rf_rst_inst1_n_20\,
      \y_reg[20]_0\ => \genblk1[15].rams_sp_rf_rst_inst1_n_20\,
      \y_reg[20]_i_1__0_0\ => \genblk1[7].rams_sp_rf_rst_inst1_n_20\,
      \y_reg[21]\ => \genblk1[11].rams_sp_rf_rst_inst1_n_21\,
      \y_reg[21]_0\ => \genblk1[15].rams_sp_rf_rst_inst1_n_21\,
      \y_reg[21]_i_1__0_0\ => \genblk1[7].rams_sp_rf_rst_inst1_n_21\,
      \y_reg[22]\ => \genblk1[11].rams_sp_rf_rst_inst1_n_22\,
      \y_reg[22]_0\ => \genblk1[15].rams_sp_rf_rst_inst1_n_22\,
      \y_reg[22]_i_1__0_0\ => \genblk1[7].rams_sp_rf_rst_inst1_n_22\,
      \y_reg[23]\ => \genblk1[11].rams_sp_rf_rst_inst1_n_23\,
      \y_reg[23]_0\ => \genblk1[15].rams_sp_rf_rst_inst1_n_23\,
      \y_reg[23]_i_1__0_0\ => \genblk1[7].rams_sp_rf_rst_inst1_n_23\,
      \y_reg[23]_i_3__0_0\(23 downto 0) => \_DOUT_2[1]_3\(23 downto 0),
      \y_reg[23]_i_3__0_1\(23 downto 0) => \_DOUT_2[0]_1\(23 downto 0),
      \y_reg[2]\ => \genblk1[11].rams_sp_rf_rst_inst1_n_2\,
      \y_reg[2]_0\ => \genblk1[15].rams_sp_rf_rst_inst1_n_2\,
      \y_reg[2]_i_1__0_0\ => \genblk1[7].rams_sp_rf_rst_inst1_n_2\,
      \y_reg[3]\ => \genblk1[11].rams_sp_rf_rst_inst1_n_3\,
      \y_reg[3]_0\ => \genblk1[15].rams_sp_rf_rst_inst1_n_3\,
      \y_reg[3]_i_1__0_0\ => \genblk1[7].rams_sp_rf_rst_inst1_n_3\,
      \y_reg[4]\ => \genblk1[11].rams_sp_rf_rst_inst1_n_4\,
      \y_reg[4]_0\ => \genblk1[15].rams_sp_rf_rst_inst1_n_4\,
      \y_reg[4]_i_1__0_0\ => \genblk1[7].rams_sp_rf_rst_inst1_n_4\,
      \y_reg[5]\ => \genblk1[11].rams_sp_rf_rst_inst1_n_5\,
      \y_reg[5]_0\ => \genblk1[15].rams_sp_rf_rst_inst1_n_5\,
      \y_reg[5]_i_1__0_0\ => \genblk1[7].rams_sp_rf_rst_inst1_n_5\,
      \y_reg[6]\ => \genblk1[11].rams_sp_rf_rst_inst1_n_6\,
      \y_reg[6]_0\ => \genblk1[15].rams_sp_rf_rst_inst1_n_6\,
      \y_reg[6]_i_1__0_0\ => \genblk1[7].rams_sp_rf_rst_inst1_n_6\,
      \y_reg[7]\ => \genblk1[11].rams_sp_rf_rst_inst1_n_7\,
      \y_reg[7]_0\ => \genblk1[15].rams_sp_rf_rst_inst1_n_7\,
      \y_reg[7]_i_1__0_0\ => \genblk1[7].rams_sp_rf_rst_inst1_n_7\,
      \y_reg[8]\ => \genblk1[11].rams_sp_rf_rst_inst1_n_8\,
      \y_reg[8]_0\ => \genblk1[15].rams_sp_rf_rst_inst1_n_8\,
      \y_reg[8]_i_1__0_0\ => \genblk1[7].rams_sp_rf_rst_inst1_n_8\,
      \y_reg[9]\ => \genblk1[11].rams_sp_rf_rst_inst1_n_9\,
      \y_reg[9]_0\ => \genblk1[15].rams_sp_rf_rst_inst1_n_9\,
      \y_reg[9]_i_1__0_0\ => \genblk1[7].rams_sp_rf_rst_inst1_n_9\
    );
\genblk1[4].rams_sp_rf_rst_inst0\: entity work.test_bd_pixelReOrder_0_0_rams_sp_rf_rst_19
     port map (
      Q(0) => \EN1_reg_n_0_[4]\,
      addr(10) => \genblk1[15].rams_sp_rf_rst_inst0_n_24\,
      addr(9) => \genblk1[15].rams_sp_rf_rst_inst0_n_25\,
      addr(8) => \genblk1[15].rams_sp_rf_rst_inst0_n_26\,
      addr(7) => \genblk1[15].rams_sp_rf_rst_inst0_n_27\,
      addr(6) => \genblk1[15].rams_sp_rf_rst_inst0_n_28\,
      addr(5) => \genblk1[15].rams_sp_rf_rst_inst0_n_29\,
      addr(4) => \genblk1[15].rams_sp_rf_rst_inst0_n_30\,
      addr(3) => \genblk1[15].rams_sp_rf_rst_inst0_n_31\,
      addr(2) => \genblk1[15].rams_sp_rf_rst_inst0_n_32\,
      addr(1) => \genblk1[15].rams_sp_rf_rst_inst0_n_33\,
      addr(0) => \genblk1[15].rams_sp_rf_rst_inst0_n_34\,
      dout0_out(23 downto 0) => \_DOUT_1[4]_8\(23 downto 0),
      m00_axis_aclk => m00_axis_aclk,
      s00_axis_tdata(23 downto 0) => s00_axis_tdata(23 downto 0),
      s00_axis_tuser(0) => s00_axis_tuser(11),
      s00_axis_tvalid => s00_axis_tvalid
    );
\genblk1[4].rams_sp_rf_rst_inst1\: entity work.test_bd_pixelReOrder_0_0_rams_sp_rf_rst_20
     port map (
      Q(0) => \EN2_reg_n_0_[4]\,
      addr(10 downto 0) => ADDR2(10 downto 0),
      dout0_out(23 downto 0) => \_DOUT_2[4]_9\(23 downto 0),
      m00_axis_aclk => m00_axis_aclk,
      s00_axis_tdata(23 downto 0) => s00_axis_tdata(23 downto 0),
      s00_axis_tuser(0) => s00_axis_tuser(11),
      s00_axis_tvalid => s00_axis_tvalid
    );
\genblk1[5].rams_sp_rf_rst_inst0\: entity work.test_bd_pixelReOrder_0_0_rams_sp_rf_rst_21
     port map (
      Q(0) => \EN1_reg_n_0_[5]\,
      addr(10) => \genblk1[15].rams_sp_rf_rst_inst0_n_24\,
      addr(9) => \genblk1[15].rams_sp_rf_rst_inst0_n_25\,
      addr(8) => \genblk1[15].rams_sp_rf_rst_inst0_n_26\,
      addr(7) => \genblk1[15].rams_sp_rf_rst_inst0_n_27\,
      addr(6) => \genblk1[15].rams_sp_rf_rst_inst0_n_28\,
      addr(5) => \genblk1[15].rams_sp_rf_rst_inst0_n_29\,
      addr(4) => \genblk1[15].rams_sp_rf_rst_inst0_n_30\,
      addr(3) => \genblk1[15].rams_sp_rf_rst_inst0_n_31\,
      addr(2) => \genblk1[15].rams_sp_rf_rst_inst0_n_32\,
      addr(1) => \genblk1[15].rams_sp_rf_rst_inst0_n_33\,
      addr(0) => \genblk1[15].rams_sp_rf_rst_inst0_n_34\,
      dout0_out(23 downto 0) => \_DOUT_1[5]_10\(23 downto 0),
      m00_axis_aclk => m00_axis_aclk,
      s00_axis_tdata(23 downto 0) => s00_axis_tdata(23 downto 0),
      s00_axis_tuser(0) => s00_axis_tuser(11),
      s00_axis_tvalid => s00_axis_tvalid
    );
\genblk1[5].rams_sp_rf_rst_inst1\: entity work.test_bd_pixelReOrder_0_0_rams_sp_rf_rst_22
     port map (
      Q(0) => \EN2_reg_n_0_[5]\,
      addr(10 downto 0) => ADDR2(10 downto 0),
      dout0_out(23 downto 0) => \_DOUT_2[5]_11\(23 downto 0),
      m00_axis_aclk => m00_axis_aclk,
      s00_axis_tdata(23 downto 0) => s00_axis_tdata(23 downto 0),
      s00_axis_tuser(0) => s00_axis_tuser(11),
      s00_axis_tvalid => s00_axis_tvalid
    );
\genblk1[6].rams_sp_rf_rst_inst0\: entity work.test_bd_pixelReOrder_0_0_rams_sp_rf_rst_23
     port map (
      Q(0) => \EN1_reg_n_0_[6]\,
      addr(10) => \genblk1[15].rams_sp_rf_rst_inst0_n_24\,
      addr(9) => \genblk1[15].rams_sp_rf_rst_inst0_n_25\,
      addr(8) => \genblk1[15].rams_sp_rf_rst_inst0_n_26\,
      addr(7) => \genblk1[15].rams_sp_rf_rst_inst0_n_27\,
      addr(6) => \genblk1[15].rams_sp_rf_rst_inst0_n_28\,
      addr(5) => \genblk1[15].rams_sp_rf_rst_inst0_n_29\,
      addr(4) => \genblk1[15].rams_sp_rf_rst_inst0_n_30\,
      addr(3) => \genblk1[15].rams_sp_rf_rst_inst0_n_31\,
      addr(2) => \genblk1[15].rams_sp_rf_rst_inst0_n_32\,
      addr(1) => \genblk1[15].rams_sp_rf_rst_inst0_n_33\,
      addr(0) => \genblk1[15].rams_sp_rf_rst_inst0_n_34\,
      dout0_out(23 downto 0) => \_DOUT_1[6]_12\(23 downto 0),
      m00_axis_aclk => m00_axis_aclk,
      s00_axis_tdata(23 downto 0) => s00_axis_tdata(23 downto 0),
      s00_axis_tuser(0) => s00_axis_tuser(11),
      s00_axis_tvalid => s00_axis_tvalid
    );
\genblk1[6].rams_sp_rf_rst_inst1\: entity work.test_bd_pixelReOrder_0_0_rams_sp_rf_rst_24
     port map (
      Q(0) => \EN2_reg_n_0_[6]\,
      addr(10 downto 0) => ADDR2(10 downto 0),
      dout0_out(23 downto 0) => \_DOUT_2[6]_13\(23 downto 0),
      m00_axis_aclk => m00_axis_aclk,
      s00_axis_tdata(23 downto 0) => s00_axis_tdata(23 downto 0),
      s00_axis_tuser(0) => s00_axis_tuser(11),
      s00_axis_tvalid => s00_axis_tvalid
    );
\genblk1[7].rams_sp_rf_rst_inst0\: entity work.test_bd_pixelReOrder_0_0_rams_sp_rf_rst_25
     port map (
      Q(0) => \EN1_reg_n_0_[7]\,
      addr(10) => \genblk1[15].rams_sp_rf_rst_inst0_n_24\,
      addr(9) => \genblk1[15].rams_sp_rf_rst_inst0_n_25\,
      addr(8) => \genblk1[15].rams_sp_rf_rst_inst0_n_26\,
      addr(7) => \genblk1[15].rams_sp_rf_rst_inst0_n_27\,
      addr(6) => \genblk1[15].rams_sp_rf_rst_inst0_n_28\,
      addr(5) => \genblk1[15].rams_sp_rf_rst_inst0_n_29\,
      addr(4) => \genblk1[15].rams_sp_rf_rst_inst0_n_30\,
      addr(3) => \genblk1[15].rams_sp_rf_rst_inst0_n_31\,
      addr(2) => \genblk1[15].rams_sp_rf_rst_inst0_n_32\,
      addr(1) => \genblk1[15].rams_sp_rf_rst_inst0_n_33\,
      addr(0) => \genblk1[15].rams_sp_rf_rst_inst0_n_34\,
      ctrl_mux_out_1(1 downto 0) => ctrl_mux_out_1(1 downto 0),
      dout0_out(23 downto 0) => \_DOUT_1[6]_12\(23 downto 0),
      m00_axis_aclk => m00_axis_aclk,
      ram_reg_0_0 => \genblk1[7].rams_sp_rf_rst_inst0_n_0\,
      ram_reg_0_1 => \genblk1[7].rams_sp_rf_rst_inst0_n_1\,
      ram_reg_0_10 => \genblk1[7].rams_sp_rf_rst_inst0_n_10\,
      ram_reg_0_11 => \genblk1[7].rams_sp_rf_rst_inst0_n_11\,
      ram_reg_0_12 => \genblk1[7].rams_sp_rf_rst_inst0_n_12\,
      ram_reg_0_13 => \genblk1[7].rams_sp_rf_rst_inst0_n_13\,
      ram_reg_0_14 => \genblk1[7].rams_sp_rf_rst_inst0_n_14\,
      ram_reg_0_15 => \genblk1[7].rams_sp_rf_rst_inst0_n_15\,
      ram_reg_0_16 => \genblk1[7].rams_sp_rf_rst_inst0_n_16\,
      ram_reg_0_17 => \genblk1[7].rams_sp_rf_rst_inst0_n_17\,
      ram_reg_0_2 => \genblk1[7].rams_sp_rf_rst_inst0_n_2\,
      ram_reg_0_3 => \genblk1[7].rams_sp_rf_rst_inst0_n_3\,
      ram_reg_0_4 => \genblk1[7].rams_sp_rf_rst_inst0_n_4\,
      ram_reg_0_5 => \genblk1[7].rams_sp_rf_rst_inst0_n_5\,
      ram_reg_0_6 => \genblk1[7].rams_sp_rf_rst_inst0_n_6\,
      ram_reg_0_7 => \genblk1[7].rams_sp_rf_rst_inst0_n_7\,
      ram_reg_0_8 => \genblk1[7].rams_sp_rf_rst_inst0_n_8\,
      ram_reg_0_9 => \genblk1[7].rams_sp_rf_rst_inst0_n_9\,
      ram_reg_1_0 => \genblk1[7].rams_sp_rf_rst_inst0_n_18\,
      ram_reg_1_1 => \genblk1[7].rams_sp_rf_rst_inst0_n_19\,
      ram_reg_1_2 => \genblk1[7].rams_sp_rf_rst_inst0_n_20\,
      ram_reg_1_3 => \genblk1[7].rams_sp_rf_rst_inst0_n_21\,
      ram_reg_1_4 => \genblk1[7].rams_sp_rf_rst_inst0_n_22\,
      ram_reg_1_5 => \genblk1[7].rams_sp_rf_rst_inst0_n_23\,
      s00_axis_tdata(23 downto 0) => s00_axis_tdata(23 downto 0),
      s00_axis_tuser(0) => s00_axis_tuser(11),
      s00_axis_tvalid => s00_axis_tvalid,
      \y_reg[23]_i_3\(23 downto 0) => \_DOUT_1[5]_10\(23 downto 0),
      \y_reg[23]_i_3_0\(23 downto 0) => \_DOUT_1[4]_8\(23 downto 0)
    );
\genblk1[7].rams_sp_rf_rst_inst1\: entity work.test_bd_pixelReOrder_0_0_rams_sp_rf_rst_26
     port map (
      Q(0) => \EN2_reg_n_0_[7]\,
      addr(10 downto 0) => ADDR2(10 downto 0),
      ctrl_mux_out_2(1 downto 0) => ctrl_mux_out_2(1 downto 0),
      dout0_out(23 downto 0) => \_DOUT_2[6]_13\(23 downto 0),
      m00_axis_aclk => m00_axis_aclk,
      ram_reg_0_0 => \genblk1[7].rams_sp_rf_rst_inst1_n_0\,
      ram_reg_0_1 => \genblk1[7].rams_sp_rf_rst_inst1_n_1\,
      ram_reg_0_10 => \genblk1[7].rams_sp_rf_rst_inst1_n_10\,
      ram_reg_0_11 => \genblk1[7].rams_sp_rf_rst_inst1_n_11\,
      ram_reg_0_12 => \genblk1[7].rams_sp_rf_rst_inst1_n_12\,
      ram_reg_0_13 => \genblk1[7].rams_sp_rf_rst_inst1_n_13\,
      ram_reg_0_14 => \genblk1[7].rams_sp_rf_rst_inst1_n_14\,
      ram_reg_0_15 => \genblk1[7].rams_sp_rf_rst_inst1_n_15\,
      ram_reg_0_16 => \genblk1[7].rams_sp_rf_rst_inst1_n_16\,
      ram_reg_0_17 => \genblk1[7].rams_sp_rf_rst_inst1_n_17\,
      ram_reg_0_2 => \genblk1[7].rams_sp_rf_rst_inst1_n_2\,
      ram_reg_0_3 => \genblk1[7].rams_sp_rf_rst_inst1_n_3\,
      ram_reg_0_4 => \genblk1[7].rams_sp_rf_rst_inst1_n_4\,
      ram_reg_0_5 => \genblk1[7].rams_sp_rf_rst_inst1_n_5\,
      ram_reg_0_6 => \genblk1[7].rams_sp_rf_rst_inst1_n_6\,
      ram_reg_0_7 => \genblk1[7].rams_sp_rf_rst_inst1_n_7\,
      ram_reg_0_8 => \genblk1[7].rams_sp_rf_rst_inst1_n_8\,
      ram_reg_0_9 => \genblk1[7].rams_sp_rf_rst_inst1_n_9\,
      ram_reg_1_0 => \genblk1[7].rams_sp_rf_rst_inst1_n_18\,
      ram_reg_1_1 => \genblk1[7].rams_sp_rf_rst_inst1_n_19\,
      ram_reg_1_2 => \genblk1[7].rams_sp_rf_rst_inst1_n_20\,
      ram_reg_1_3 => \genblk1[7].rams_sp_rf_rst_inst1_n_21\,
      ram_reg_1_4 => \genblk1[7].rams_sp_rf_rst_inst1_n_22\,
      ram_reg_1_5 => \genblk1[7].rams_sp_rf_rst_inst1_n_23\,
      s00_axis_tdata(23 downto 0) => s00_axis_tdata(23 downto 0),
      s00_axis_tuser(0) => s00_axis_tuser(11),
      s00_axis_tvalid => s00_axis_tvalid,
      \y_reg[23]_i_3__0\(23 downto 0) => \_DOUT_2[5]_11\(23 downto 0),
      \y_reg[23]_i_3__0_0\(23 downto 0) => \_DOUT_2[4]_9\(23 downto 0)
    );
\genblk1[8].rams_sp_rf_rst_inst0\: entity work.test_bd_pixelReOrder_0_0_rams_sp_rf_rst_27
     port map (
      Q(0) => \EN1_reg_n_0_[8]\,
      addr(10) => \genblk1[15].rams_sp_rf_rst_inst0_n_24\,
      addr(9) => \genblk1[15].rams_sp_rf_rst_inst0_n_25\,
      addr(8) => \genblk1[15].rams_sp_rf_rst_inst0_n_26\,
      addr(7) => \genblk1[15].rams_sp_rf_rst_inst0_n_27\,
      addr(6) => \genblk1[15].rams_sp_rf_rst_inst0_n_28\,
      addr(5) => \genblk1[15].rams_sp_rf_rst_inst0_n_29\,
      addr(4) => \genblk1[15].rams_sp_rf_rst_inst0_n_30\,
      addr(3) => \genblk1[15].rams_sp_rf_rst_inst0_n_31\,
      addr(2) => \genblk1[15].rams_sp_rf_rst_inst0_n_32\,
      addr(1) => \genblk1[15].rams_sp_rf_rst_inst0_n_33\,
      addr(0) => \genblk1[15].rams_sp_rf_rst_inst0_n_34\,
      dout0_out(23 downto 0) => \_DOUT_1[8]_16\(23 downto 0),
      m00_axis_aclk => m00_axis_aclk,
      s00_axis_tdata(23 downto 0) => s00_axis_tdata(23 downto 0),
      s00_axis_tuser(0) => s00_axis_tuser(11),
      s00_axis_tvalid => s00_axis_tvalid
    );
\genblk1[8].rams_sp_rf_rst_inst1\: entity work.test_bd_pixelReOrder_0_0_rams_sp_rf_rst_28
     port map (
      Q(0) => \EN2_reg_n_0_[8]\,
      addr(10 downto 0) => ADDR2(10 downto 0),
      dout0_out(23 downto 0) => \_DOUT_2[8]_17\(23 downto 0),
      m00_axis_aclk => m00_axis_aclk,
      s00_axis_tdata(23 downto 0) => s00_axis_tdata(23 downto 0),
      s00_axis_tuser(0) => s00_axis_tuser(11),
      s00_axis_tvalid => s00_axis_tvalid
    );
\genblk1[9].rams_sp_rf_rst_inst0\: entity work.test_bd_pixelReOrder_0_0_rams_sp_rf_rst_29
     port map (
      Q(0) => \EN1_reg_n_0_[9]\,
      addr(10) => \genblk1[15].rams_sp_rf_rst_inst0_n_24\,
      addr(9) => \genblk1[15].rams_sp_rf_rst_inst0_n_25\,
      addr(8) => \genblk1[15].rams_sp_rf_rst_inst0_n_26\,
      addr(7) => \genblk1[15].rams_sp_rf_rst_inst0_n_27\,
      addr(6) => \genblk1[15].rams_sp_rf_rst_inst0_n_28\,
      addr(5) => \genblk1[15].rams_sp_rf_rst_inst0_n_29\,
      addr(4) => \genblk1[15].rams_sp_rf_rst_inst0_n_30\,
      addr(3) => \genblk1[15].rams_sp_rf_rst_inst0_n_31\,
      addr(2) => \genblk1[15].rams_sp_rf_rst_inst0_n_32\,
      addr(1) => \genblk1[15].rams_sp_rf_rst_inst0_n_33\,
      addr(0) => \genblk1[15].rams_sp_rf_rst_inst0_n_34\,
      dout0_out(23 downto 0) => \_DOUT_1[9]_18\(23 downto 0),
      m00_axis_aclk => m00_axis_aclk,
      s00_axis_tdata(23 downto 0) => s00_axis_tdata(23 downto 0),
      s00_axis_tuser(0) => s00_axis_tuser(11),
      s00_axis_tvalid => s00_axis_tvalid
    );
\genblk1[9].rams_sp_rf_rst_inst1\: entity work.test_bd_pixelReOrder_0_0_rams_sp_rf_rst_30
     port map (
      Q(0) => \EN2_reg_n_0_[9]\,
      addr(10 downto 0) => ADDR2(10 downto 0),
      dout0_out(23 downto 0) => \_DOUT_2[9]_19\(23 downto 0),
      m00_axis_aclk => m00_axis_aclk,
      s00_axis_tdata(23 downto 0) => s00_axis_tdata(23 downto 0),
      s00_axis_tuser(0) => s00_axis_tuser(11),
      s00_axis_tvalid => s00_axis_tvalid
    );
locked_EN1_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF08080808080808"
    )
        port map (
      I0 => \EN1[15]_i_3_n_0\,
      I1 => locked_EN1_i_2_n_0,
      I2 => \adr_read_reorder_data_mem1[10]_i_4_n_0\,
      I3 => locked_EN1_reg_n_0,
      I4 => s00_axis_tuser(11),
      I5 => s00_axis_aresetn,
      O => locked_EN1_i_1_n_0
    );
locked_EN1_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0080000000000000"
    )
        port map (
      I0 => \adr_read_reorder_data_mem1_reg_n_0_[9]\,
      I1 => \adr_read_reorder_data_mem1_reg_n_0_[10]\,
      I2 => \adr_read_reorder_data_mem1_reg_n_0_[8]\,
      I3 => \adr_read_reorder_data_mem1_reg_n_0_[7]\,
      I4 => \active_mem1_in_read_state_reg_n_0_[2]\,
      I5 => \active_mem1_in_read_state_reg_n_0_[1]\,
      O => locked_EN1_i_2_n_0
    );
locked_EN1_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => '1',
      D => locked_EN1_i_1_n_0,
      Q => locked_EN1_reg_n_0,
      R => '0'
    );
locked_EN2_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7077707070707070"
    )
        port map (
      I0 => s00_axis_tuser(11),
      I1 => s00_axis_aresetn,
      I2 => locked_EN2_reg_n_0,
      I3 => \adr_read_reorder_data_mem2[10]_i_4_n_0\,
      I4 => locked_EN2_i_2_n_0,
      I5 => \EN2[15]_i_6_n_0\,
      O => locked_EN2_i_1_n_0
    );
locked_EN2_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0080000000000000"
    )
        port map (
      I0 => \adr_read_reorder_data_mem2_reg_n_0_[9]\,
      I1 => \adr_read_reorder_data_mem2_reg_n_0_[10]\,
      I2 => \adr_read_reorder_data_mem2_reg_n_0_[8]\,
      I3 => \adr_read_reorder_data_mem2_reg_n_0_[7]\,
      I4 => active_mem2_in_read_state_reg(2),
      I5 => active_mem2_in_read_state_reg(1),
      O => locked_EN2_i_2_n_0
    );
locked_EN2_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => '1',
      D => locked_EN2_i_1_n_0,
      Q => locked_EN2_reg_n_0,
      R => '0'
    );
m00_axis_tlast_INST_0: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000200000000"
    )
        port map (
      I0 => m00_axis_tlast_INST_0_i_1_n_0,
      I1 => cnt_last_reg(1),
      I2 => cnt_last_reg(0),
      I3 => cnt_last_reg(3),
      I4 => cnt_last_reg(2),
      I5 => m00_axis_tlast_INST_0_i_2_n_0,
      O => \^m00_axis_tlast\
    );
m00_axis_tlast_INST_0_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000010"
    )
        port map (
      I0 => cnt_last_reg(12),
      I1 => cnt_last_reg(13),
      I2 => cnt_last_reg(10),
      I3 => cnt_last_reg(11),
      I4 => cnt_last_reg(15),
      I5 => cnt_last_reg(14),
      O => m00_axis_tlast_INST_0_i_1_n_0
    );
m00_axis_tlast_INST_0_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0002000000000000"
    )
        port map (
      I0 => cnt_last_reg(7),
      I1 => cnt_last_reg(6),
      I2 => cnt_last_reg(4),
      I3 => cnt_last_reg(5),
      I4 => cnt_last_reg(9),
      I5 => cnt_last_reg(8),
      O => m00_axis_tlast_INST_0_i_2_n_0
    );
m00_axis_tvalid_r_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFF008000800080"
    )
        port map (
      I0 => m00_axis_tvalid_r_i_2_n_0,
      I1 => s00_axis_tuser(11),
      I2 => s00_axis_aresetn,
      I3 => locked_EN1_reg_n_0,
      I4 => m00_axis_tvalid_r_i_3_n_0,
      I5 => m00_axis_tvalid_r_i_4_n_0,
      O => m00_axis_tvalid_r_i_1_n_0
    );
m00_axis_tvalid_r_i_2: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7FFF"
    )
        port map (
      I0 => \adr_read_reorder_data_mem1_reg_n_0_[9]\,
      I1 => \adr_read_reorder_data_mem1_reg_n_0_[7]\,
      I2 => \adr_read_reorder_data_mem1_reg_n_0_[8]\,
      I3 => \adr_read_reorder_data_mem1_reg_n_0_[10]\,
      O => m00_axis_tvalid_r_i_2_n_0
    );
m00_axis_tvalid_r_i_3: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7FFF"
    )
        port map (
      I0 => \adr_read_reorder_data_mem2_reg_n_0_[9]\,
      I1 => \adr_read_reorder_data_mem2_reg_n_0_[7]\,
      I2 => \adr_read_reorder_data_mem2_reg_n_0_[8]\,
      I3 => \adr_read_reorder_data_mem2_reg_n_0_[10]\,
      O => m00_axis_tvalid_r_i_3_n_0
    );
m00_axis_tvalid_r_i_4: unisim.vcomponents.LUT6
    generic map(
      INIT => X"1010101010101000"
    )
        port map (
      I0 => locked_EN2_reg_n_0,
      I1 => s00_axis_tuser(11),
      I2 => s00_axis_aresetn,
      I3 => s00_axis_tuser(12),
      I4 => s00_axis_tuser(13),
      I5 => s00_axis_tuser(14),
      O => m00_axis_tvalid_r_i_4_n_0
    );
m00_axis_tvalid_r_reg: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => m00_axis_tvalid_r_i_1_n_0,
      Q => \^m00_axis_tvalid\,
      R => '0'
    );
multiplexer2_inst: entity work.test_bd_pixelReOrder_0_0_multiplexer
     port map (
      D(23) => \genblk1[3].rams_sp_rf_rst_inst1_n_0\,
      D(22) => \genblk1[3].rams_sp_rf_rst_inst1_n_1\,
      D(21) => \genblk1[3].rams_sp_rf_rst_inst1_n_2\,
      D(20) => \genblk1[3].rams_sp_rf_rst_inst1_n_3\,
      D(19) => \genblk1[3].rams_sp_rf_rst_inst1_n_4\,
      D(18) => \genblk1[3].rams_sp_rf_rst_inst1_n_5\,
      D(17) => \genblk1[3].rams_sp_rf_rst_inst1_n_6\,
      D(16) => \genblk1[3].rams_sp_rf_rst_inst1_n_7\,
      D(15) => \genblk1[3].rams_sp_rf_rst_inst1_n_8\,
      D(14) => \genblk1[3].rams_sp_rf_rst_inst1_n_9\,
      D(13) => \genblk1[3].rams_sp_rf_rst_inst1_n_10\,
      D(12) => \genblk1[3].rams_sp_rf_rst_inst1_n_11\,
      D(11) => \genblk1[3].rams_sp_rf_rst_inst1_n_12\,
      D(10) => \genblk1[3].rams_sp_rf_rst_inst1_n_13\,
      D(9) => \genblk1[3].rams_sp_rf_rst_inst1_n_14\,
      D(8) => \genblk1[3].rams_sp_rf_rst_inst1_n_15\,
      D(7) => \genblk1[3].rams_sp_rf_rst_inst1_n_16\,
      D(6) => \genblk1[3].rams_sp_rf_rst_inst1_n_17\,
      D(5) => \genblk1[3].rams_sp_rf_rst_inst1_n_18\,
      D(4) => \genblk1[3].rams_sp_rf_rst_inst1_n_19\,
      D(3) => \genblk1[3].rams_sp_rf_rst_inst1_n_20\,
      D(2) => \genblk1[3].rams_sp_rf_rst_inst1_n_21\,
      D(1) => \genblk1[3].rams_sp_rf_rst_inst1_n_22\,
      D(0) => \genblk1[3].rams_sp_rf_rst_inst1_n_23\,
      Q(23 downto 0) => out_mux_2(23 downto 0),
      ctrl_mux_out_2(4 downto 0) => ctrl_mux_out_2(4 downto 0)
    );
multiplexer_inst: entity work.test_bd_pixelReOrder_0_0_multiplexer_31
     port map (
      D(23) => \genblk1[3].rams_sp_rf_rst_inst0_n_0\,
      D(22) => \genblk1[3].rams_sp_rf_rst_inst0_n_1\,
      D(21) => \genblk1[3].rams_sp_rf_rst_inst0_n_2\,
      D(20) => \genblk1[3].rams_sp_rf_rst_inst0_n_3\,
      D(19) => \genblk1[3].rams_sp_rf_rst_inst0_n_4\,
      D(18) => \genblk1[3].rams_sp_rf_rst_inst0_n_5\,
      D(17) => \genblk1[3].rams_sp_rf_rst_inst0_n_6\,
      D(16) => \genblk1[3].rams_sp_rf_rst_inst0_n_7\,
      D(15) => \genblk1[3].rams_sp_rf_rst_inst0_n_8\,
      D(14) => \genblk1[3].rams_sp_rf_rst_inst0_n_9\,
      D(13) => \genblk1[3].rams_sp_rf_rst_inst0_n_10\,
      D(12) => \genblk1[3].rams_sp_rf_rst_inst0_n_11\,
      D(11) => \genblk1[3].rams_sp_rf_rst_inst0_n_12\,
      D(10) => \genblk1[3].rams_sp_rf_rst_inst0_n_13\,
      D(9) => \genblk1[3].rams_sp_rf_rst_inst0_n_14\,
      D(8) => \genblk1[3].rams_sp_rf_rst_inst0_n_15\,
      D(7) => \genblk1[3].rams_sp_rf_rst_inst0_n_16\,
      D(6) => \genblk1[3].rams_sp_rf_rst_inst0_n_17\,
      D(5) => \genblk1[3].rams_sp_rf_rst_inst0_n_18\,
      D(4) => \genblk1[3].rams_sp_rf_rst_inst0_n_19\,
      D(3) => \genblk1[3].rams_sp_rf_rst_inst0_n_20\,
      D(2) => \genblk1[3].rams_sp_rf_rst_inst0_n_21\,
      D(1) => \genblk1[3].rams_sp_rf_rst_inst0_n_22\,
      D(0) => \genblk1[3].rams_sp_rf_rst_inst0_n_23\,
      Q(23 downto 0) => out_mux_2(23 downto 0),
      ctrl_mux_out_1(4 downto 0) => ctrl_mux_out_1(4 downto 0),
      m00_axis_tdata(23 downto 0) => m00_axis_tdata(23 downto 0),
      s00_axis_tuser(0) => s00_axis_tuser(11)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity test_bd_pixelReOrder_0_0 is
  port (
    m00_axis_aclk : in STD_LOGIC;
    m00_axis_aresetn : in STD_LOGIC;
    m00_axis_tvalid : out STD_LOGIC;
    m00_axis_tdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    m00_axis_tstrb : out STD_LOGIC_VECTOR ( 3 downto 0 );
    m00_axis_tlast : out STD_LOGIC;
    m00_axis_tready : in STD_LOGIC;
    s00_axis_aclk : in STD_LOGIC;
    s00_axis_aresetn : in STD_LOGIC;
    s00_axis_tready : out STD_LOGIC;
    s00_axis_tdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s00_axis_tuser : in STD_LOGIC_VECTOR ( 47 downto 0 );
    s00_axis_tstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s00_axis_tlast : in STD_LOGIC;
    s00_axis_tvalid : in STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of test_bd_pixelReOrder_0_0 : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of test_bd_pixelReOrder_0_0 : entity is "test_bd_pixelReOrder_0_0,pixelReOrder_v1_0,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of test_bd_pixelReOrder_0_0 : entity is "yes";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of test_bd_pixelReOrder_0_0 : entity is "pixelReOrder_v1_0,Vivado 2019.1";
end test_bd_pixelReOrder_0_0;

architecture STRUCTURE of test_bd_pixelReOrder_0_0 is
  signal \<const0>\ : STD_LOGIC;
  signal \<const1>\ : STD_LOGIC;
  signal \^m00_axis_tdata\ : STD_LOGIC_VECTOR ( 23 downto 0 );
  attribute X_INTERFACE_INFO : string;
  attribute X_INTERFACE_INFO of m00_axis_aclk : signal is "xilinx.com:signal:clock:1.0 M00_AXIS_CLK CLK";
  attribute X_INTERFACE_PARAMETER : string;
  attribute X_INTERFACE_PARAMETER of m00_axis_aclk : signal is "XIL_INTERFACENAME M00_AXIS_CLK, ASSOCIATED_BUSIF M00_AXIS, ASSOCIATED_RESET m00_axis_aresetn, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN test_bd_m00_axis_aclk_0, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of m00_axis_aresetn : signal is "xilinx.com:signal:reset:1.0 M00_AXIS_RST RST";
  attribute X_INTERFACE_PARAMETER of m00_axis_aresetn : signal is "XIL_INTERFACENAME M00_AXIS_RST, POLARITY ACTIVE_LOW, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of m00_axis_tlast : signal is "xilinx.com:interface:axis:1.0 M00_AXIS TLAST";
  attribute X_INTERFACE_INFO of m00_axis_tready : signal is "xilinx.com:interface:axis:1.0 M00_AXIS TREADY";
  attribute X_INTERFACE_PARAMETER of m00_axis_tready : signal is "XIL_INTERFACENAME M00_AXIS, WIZ_DATA_WIDTH 32, TDATA_NUM_BYTES 4, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 1, HAS_TKEEP 0, HAS_TLAST 1, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN test_bd_m00_axis_aclk_0, LAYERED_METADATA undef, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of m00_axis_tvalid : signal is "xilinx.com:interface:axis:1.0 M00_AXIS TVALID";
  attribute X_INTERFACE_INFO of s00_axis_aclk : signal is "xilinx.com:signal:clock:1.0 S00_AXIS_CLK CLK";
  attribute X_INTERFACE_PARAMETER of s00_axis_aclk : signal is "XIL_INTERFACENAME S00_AXIS_CLK, ASSOCIATED_BUSIF S00_AXIS, ASSOCIATED_RESET s00_axis_aresetn, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN test_bd_m00_axis_aclk_0, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of s00_axis_aresetn : signal is "xilinx.com:signal:reset:1.0 S00_AXIS_RST RST";
  attribute X_INTERFACE_PARAMETER of s00_axis_aresetn : signal is "XIL_INTERFACENAME S00_AXIS_RST, POLARITY ACTIVE_LOW, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of s00_axis_tlast : signal is "xilinx.com:interface:axis:1.0 S00_AXIS TLAST";
  attribute X_INTERFACE_INFO of s00_axis_tready : signal is "xilinx.com:interface:axis:1.0 S00_AXIS TREADY";
  attribute X_INTERFACE_INFO of s00_axis_tvalid : signal is "xilinx.com:interface:axis:1.0 S00_AXIS TVALID";
  attribute X_INTERFACE_PARAMETER of s00_axis_tvalid : signal is "XIL_INTERFACENAME S00_AXIS, WIZ_DATA_WIDTH 32, TDATA_NUM_BYTES 4, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 48, HAS_TREADY 1, HAS_TSTRB 1, HAS_TKEEP 0, HAS_TLAST 1, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN test_bd_m00_axis_aclk_0, LAYERED_METADATA undef, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of m00_axis_tdata : signal is "xilinx.com:interface:axis:1.0 M00_AXIS TDATA";
  attribute X_INTERFACE_INFO of m00_axis_tstrb : signal is "xilinx.com:interface:axis:1.0 M00_AXIS TSTRB";
  attribute X_INTERFACE_INFO of s00_axis_tdata : signal is "xilinx.com:interface:axis:1.0 S00_AXIS TDATA";
  attribute X_INTERFACE_INFO of s00_axis_tstrb : signal is "xilinx.com:interface:axis:1.0 S00_AXIS TSTRB";
  attribute X_INTERFACE_INFO of s00_axis_tuser : signal is "xilinx.com:interface:axis:1.0 S00_AXIS TUSER";
begin
  m00_axis_tdata(31) <= \<const0>\;
  m00_axis_tdata(30) <= \<const0>\;
  m00_axis_tdata(29) <= \<const0>\;
  m00_axis_tdata(28) <= \<const0>\;
  m00_axis_tdata(27) <= \<const0>\;
  m00_axis_tdata(26) <= \<const0>\;
  m00_axis_tdata(25) <= \<const0>\;
  m00_axis_tdata(24) <= \<const0>\;
  m00_axis_tdata(23 downto 0) <= \^m00_axis_tdata\(23 downto 0);
  m00_axis_tstrb(3) <= \<const1>\;
  m00_axis_tstrb(2) <= \<const1>\;
  m00_axis_tstrb(1) <= \<const1>\;
  m00_axis_tstrb(0) <= \<const1>\;
  s00_axis_tready <= \<const1>\;
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
VCC: unisim.vcomponents.VCC
     port map (
      P => \<const1>\
    );
inst: entity work.test_bd_pixelReOrder_0_0_pixelReOrder_v1_0
     port map (
      m00_axis_aclk => m00_axis_aclk,
      m00_axis_tdata(23 downto 0) => \^m00_axis_tdata\(23 downto 0),
      m00_axis_tlast => m00_axis_tlast,
      m00_axis_tvalid => m00_axis_tvalid,
      s00_axis_aresetn => s00_axis_aresetn,
      s00_axis_tdata(23 downto 0) => s00_axis_tdata(23 downto 0),
      s00_axis_tuser(14 downto 11) => s00_axis_tuser(23 downto 20),
      s00_axis_tuser(10 downto 0) => s00_axis_tuser(10 downto 0),
      s00_axis_tvalid => s00_axis_tvalid
    );
end STRUCTURE;
