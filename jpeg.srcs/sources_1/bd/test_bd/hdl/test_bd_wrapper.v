//Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
//--------------------------------------------------------------------------------
//Tool Version: Vivado v.2019.1 (lin64) Build 2552052 Fri May 24 14:47:09 MDT 2019
//Date        : Fri Oct 15 21:11:08 2021
//Host        : alex-HP-Compaq-8200-Elite-CMT-PC running 64-bit Ubuntu 20.04.3 LTS
//Command     : generate_target test_bd_wrapper.bd
//Design      : test_bd_wrapper
//Purpose     : IP block netlist
//--------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module test_bd_wrapper
   (M_AXIS_TLAST_0,
    S_AXIS_1_tdata,
    S_AXIS_1_tkeep,
    S_AXIS_1_tlast,
    S_AXIS_1_tready,
    S_AXIS_1_tstrb,
    S_AXIS_1_tvalid,
    S_AXI_1_araddr,
    S_AXI_1_arcache,
    S_AXI_1_arprot,
    S_AXI_1_arready,
    S_AXI_1_arvalid,
    S_AXI_1_awaddr,
    S_AXI_1_awcache,
    S_AXI_1_awprot,
    S_AXI_1_awready,
    S_AXI_1_awvalid,
    S_AXI_1_bready,
    S_AXI_1_bresp,
    S_AXI_1_bvalid,
    S_AXI_1_rdata,
    S_AXI_1_rready,
    S_AXI_1_rresp,
    S_AXI_1_rvalid,
    S_AXI_1_wdata,
    S_AXI_1_wready,
    S_AXI_1_wstrb,
    S_AXI_1_wvalid,
    m00_axis_aclk_0,
    m_axis_aclk_0,
    s00_axis_aresetn_0);
  output M_AXIS_TLAST_0;
  input [31:0]S_AXIS_1_tdata;
  input S_AXIS_1_tkeep;
  input S_AXIS_1_tlast;
  output S_AXIS_1_tready;
  input [3:0]S_AXIS_1_tstrb;
  input S_AXIS_1_tvalid;
  input [31:0]S_AXI_1_araddr;
  input [3:0]S_AXI_1_arcache;
  input [2:0]S_AXI_1_arprot;
  output S_AXI_1_arready;
  input S_AXI_1_arvalid;
  input [31:0]S_AXI_1_awaddr;
  input [3:0]S_AXI_1_awcache;
  input [2:0]S_AXI_1_awprot;
  output S_AXI_1_awready;
  input S_AXI_1_awvalid;
  input S_AXI_1_bready;
  output [1:0]S_AXI_1_bresp;
  output S_AXI_1_bvalid;
  output [31:0]S_AXI_1_rdata;
  input S_AXI_1_rready;
  output [1:0]S_AXI_1_rresp;
  output S_AXI_1_rvalid;
  input [31:0]S_AXI_1_wdata;
  output S_AXI_1_wready;
  input [3:0]S_AXI_1_wstrb;
  input S_AXI_1_wvalid;
  input m00_axis_aclk_0;
  input m_axis_aclk_0;
  input s00_axis_aresetn_0;

  wire M_AXIS_TLAST_0;
  wire [31:0]S_AXIS_1_tdata;
  wire S_AXIS_1_tkeep;
  wire S_AXIS_1_tlast;
  wire S_AXIS_1_tready;
  wire [3:0]S_AXIS_1_tstrb;
  wire S_AXIS_1_tvalid;
  wire [31:0]S_AXI_1_araddr;
  wire [3:0]S_AXI_1_arcache;
  wire [2:0]S_AXI_1_arprot;
  wire S_AXI_1_arready;
  wire S_AXI_1_arvalid;
  wire [31:0]S_AXI_1_awaddr;
  wire [3:0]S_AXI_1_awcache;
  wire [2:0]S_AXI_1_awprot;
  wire S_AXI_1_awready;
  wire S_AXI_1_awvalid;
  wire S_AXI_1_bready;
  wire [1:0]S_AXI_1_bresp;
  wire S_AXI_1_bvalid;
  wire [31:0]S_AXI_1_rdata;
  wire S_AXI_1_rready;
  wire [1:0]S_AXI_1_rresp;
  wire S_AXI_1_rvalid;
  wire [31:0]S_AXI_1_wdata;
  wire S_AXI_1_wready;
  wire [3:0]S_AXI_1_wstrb;
  wire S_AXI_1_wvalid;
  wire m00_axis_aclk_0;
  wire m_axis_aclk_0;
  wire s00_axis_aresetn_0;

  test_bd test_bd_i
       (.M_AXIS_TLAST_0(M_AXIS_TLAST_0),
        .S_AXIS_1_tdata(S_AXIS_1_tdata),
        .S_AXIS_1_tkeep(S_AXIS_1_tkeep),
        .S_AXIS_1_tlast(S_AXIS_1_tlast),
        .S_AXIS_1_tready(S_AXIS_1_tready),
        .S_AXIS_1_tstrb(S_AXIS_1_tstrb),
        .S_AXIS_1_tvalid(S_AXIS_1_tvalid),
        .S_AXI_1_araddr(S_AXI_1_araddr),
        .S_AXI_1_arcache(S_AXI_1_arcache),
        .S_AXI_1_arprot(S_AXI_1_arprot),
        .S_AXI_1_arready(S_AXI_1_arready),
        .S_AXI_1_arvalid(S_AXI_1_arvalid),
        .S_AXI_1_awaddr(S_AXI_1_awaddr),
        .S_AXI_1_awcache(S_AXI_1_awcache),
        .S_AXI_1_awprot(S_AXI_1_awprot),
        .S_AXI_1_awready(S_AXI_1_awready),
        .S_AXI_1_awvalid(S_AXI_1_awvalid),
        .S_AXI_1_bready(S_AXI_1_bready),
        .S_AXI_1_bresp(S_AXI_1_bresp),
        .S_AXI_1_bvalid(S_AXI_1_bvalid),
        .S_AXI_1_rdata(S_AXI_1_rdata),
        .S_AXI_1_rready(S_AXI_1_rready),
        .S_AXI_1_rresp(S_AXI_1_rresp),
        .S_AXI_1_rvalid(S_AXI_1_rvalid),
        .S_AXI_1_wdata(S_AXI_1_wdata),
        .S_AXI_1_wready(S_AXI_1_wready),
        .S_AXI_1_wstrb(S_AXI_1_wstrb),
        .S_AXI_1_wvalid(S_AXI_1_wvalid),
        .m00_axis_aclk_0(m00_axis_aclk_0),
        .m_axis_aclk_0(m_axis_aclk_0),
        .s00_axis_aresetn_0(s00_axis_aresetn_0));
endmodule
