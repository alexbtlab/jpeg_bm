//Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
//--------------------------------------------------------------------------------
//Tool Version: Vivado v.2019.1 (lin64) Build 2552052 Fri May 24 14:47:09 MDT 2019
//Date        : Fri Oct 15 21:11:08 2021
//Host        : alex-HP-Compaq-8200-Elite-CMT-PC running 64-bit Ubuntu 20.04.3 LTS
//Command     : generate_target test_bd.bd
//Design      : test_bd
//Purpose     : IP block netlist
//--------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CORE_GENERATION_INFO = "test_bd,IP_Integrator,{x_ipVendor=xilinx.com,x_ipLibrary=BlockDiagram,x_ipName=test_bd,x_ipVersion=1.00.a,x_ipLanguage=VERILOG,numBlks=3,numReposBlks=3,numNonXlnxBlks=2,numHierBlks=0,maxHierDepth=0,numSysgenBlks=0,numHlsBlks=0,numHdlrefBlks=0,numPkgbdBlks=0,bdsource=USER,synth_mode=OOC_per_IP}" *) (* HW_HANDOFF = "test_bd.hwdef" *) 
module test_bd
   (M_AXIS_TLAST_0,
    S_AXIS_1_tdata,
    S_AXIS_1_tkeep,
    S_AXIS_1_tlast,
    S_AXIS_1_tready,
    S_AXIS_1_tstrb,
    S_AXIS_1_tvalid,
    S_AXI_1_araddr,
    S_AXI_1_arcache,
    S_AXI_1_arprot,
    S_AXI_1_arready,
    S_AXI_1_arvalid,
    S_AXI_1_awaddr,
    S_AXI_1_awcache,
    S_AXI_1_awprot,
    S_AXI_1_awready,
    S_AXI_1_awvalid,
    S_AXI_1_bready,
    S_AXI_1_bresp,
    S_AXI_1_bvalid,
    S_AXI_1_rdata,
    S_AXI_1_rready,
    S_AXI_1_rresp,
    S_AXI_1_rvalid,
    S_AXI_1_wdata,
    S_AXI_1_wready,
    S_AXI_1_wstrb,
    S_AXI_1_wvalid,
    m00_axis_aclk_0,
    m_axis_aclk_0,
    s00_axis_aresetn_0);
  output M_AXIS_TLAST_0;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 S_AXIS_1 TDATA" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME S_AXIS_1, CLK_DOMAIN test_bd_m00_axis_aclk_0, FREQ_HZ 100000000, HAS_TKEEP 1, HAS_TLAST 1, HAS_TREADY 1, HAS_TSTRB 1, INSERT_VIP 0, LAYERED_METADATA undef, PHASE 0.000, TDATA_NUM_BYTES 4, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0" *) input [31:0]S_AXIS_1_tdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 S_AXIS_1 TKEEP" *) input S_AXIS_1_tkeep;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 S_AXIS_1 TLAST" *) input S_AXIS_1_tlast;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 S_AXIS_1 TREADY" *) output S_AXIS_1_tready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 S_AXIS_1 TSTRB" *) input [3:0]S_AXIS_1_tstrb;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 S_AXIS_1 TVALID" *) input S_AXIS_1_tvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S_AXI_1 ARADDR" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME S_AXI_1, ADDR_WIDTH 32, ARUSER_WIDTH 0, AWUSER_WIDTH 0, BUSER_WIDTH 0, CLK_DOMAIN test_bd_m00_axis_aclk_0, DATA_WIDTH 32, FREQ_HZ 100000000, HAS_BRESP 1, HAS_BURST 0, HAS_CACHE 1, HAS_LOCK 0, HAS_PROT 1, HAS_QOS 0, HAS_REGION 0, HAS_RRESP 1, HAS_WSTRB 1, ID_WIDTH 0, INSERT_VIP 0, MAX_BURST_LENGTH 1, NUM_READ_OUTSTANDING 1, NUM_READ_THREADS 1, NUM_WRITE_OUTSTANDING 1, NUM_WRITE_THREADS 1, PHASE 0.000, PROTOCOL AXI4LITE, READ_WRITE_MODE READ_WRITE, RUSER_BITS_PER_BYTE 0, RUSER_WIDTH 0, SUPPORTS_NARROW_BURST 0, WUSER_BITS_PER_BYTE 0, WUSER_WIDTH 0" *) input [31:0]S_AXI_1_araddr;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S_AXI_1 ARCACHE" *) input [3:0]S_AXI_1_arcache;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S_AXI_1 ARPROT" *) input [2:0]S_AXI_1_arprot;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S_AXI_1 ARREADY" *) output S_AXI_1_arready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S_AXI_1 ARVALID" *) input S_AXI_1_arvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S_AXI_1 AWADDR" *) input [31:0]S_AXI_1_awaddr;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S_AXI_1 AWCACHE" *) input [3:0]S_AXI_1_awcache;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S_AXI_1 AWPROT" *) input [2:0]S_AXI_1_awprot;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S_AXI_1 AWREADY" *) output S_AXI_1_awready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S_AXI_1 AWVALID" *) input S_AXI_1_awvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S_AXI_1 BREADY" *) input S_AXI_1_bready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S_AXI_1 BRESP" *) output [1:0]S_AXI_1_bresp;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S_AXI_1 BVALID" *) output S_AXI_1_bvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S_AXI_1 RDATA" *) output [31:0]S_AXI_1_rdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S_AXI_1 RREADY" *) input S_AXI_1_rready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S_AXI_1 RRESP" *) output [1:0]S_AXI_1_rresp;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S_AXI_1 RVALID" *) output S_AXI_1_rvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S_AXI_1 WDATA" *) input [31:0]S_AXI_1_wdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S_AXI_1 WREADY" *) output S_AXI_1_wready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S_AXI_1 WSTRB" *) input [3:0]S_AXI_1_wstrb;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S_AXI_1 WVALID" *) input S_AXI_1_wvalid;
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 CLK.M00_AXIS_ACLK_0 CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME CLK.M00_AXIS_ACLK_0, ASSOCIATED_BUSIF S_AXI_1:S00_AXIS_0:S_AXIS_1, ASSOCIATED_RESET s00_axis_aresetn_0, CLK_DOMAIN test_bd_m00_axis_aclk_0, FREQ_HZ 100000000, INSERT_VIP 0, PHASE 0.000" *) input m00_axis_aclk_0;
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 CLK.M_AXIS_ACLK_0 CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME CLK.M_AXIS_ACLK_0, CLK_DOMAIN test_bd_m_axis_aclk_0, FREQ_HZ 100000000, INSERT_VIP 0, PHASE 0.000" *) input m_axis_aclk_0;
  (* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 RST.S00_AXIS_ARESETN_0 RST" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME RST.S00_AXIS_ARESETN_0, INSERT_VIP 0, POLARITY ACTIVE_LOW" *) input s00_axis_aresetn_0;

  wire [31:0]S_AXIS_0_1_TDATA;
  wire S_AXIS_0_1_TKEEP;
  wire S_AXIS_0_1_TLAST;
  wire S_AXIS_0_1_TREADY;
  wire [3:0]S_AXIS_0_1_TSTRB;
  wire S_AXIS_0_1_TVALID;
  wire [31:0]S_AXI_0_1_ARADDR;
  wire [3:0]S_AXI_0_1_ARCACHE;
  wire [2:0]S_AXI_0_1_ARPROT;
  wire S_AXI_0_1_ARREADY;
  wire S_AXI_0_1_ARVALID;
  wire [31:0]S_AXI_0_1_AWADDR;
  wire [3:0]S_AXI_0_1_AWCACHE;
  wire [2:0]S_AXI_0_1_AWPROT;
  wire S_AXI_0_1_AWREADY;
  wire S_AXI_0_1_AWVALID;
  wire S_AXI_0_1_BREADY;
  wire [1:0]S_AXI_0_1_BRESP;
  wire S_AXI_0_1_BVALID;
  wire [31:0]S_AXI_0_1_RDATA;
  wire S_AXI_0_1_RREADY;
  wire [1:0]S_AXI_0_1_RRESP;
  wire S_AXI_0_1_RVALID;
  wire [31:0]S_AXI_0_1_WDATA;
  wire S_AXI_0_1_WREADY;
  wire [3:0]S_AXI_0_1_WSTRB;
  wire S_AXI_0_1_WVALID;
  wire [31:0]aq_axis_djpeg_0_M_AXIS_TDATA;
  wire aq_axis_djpeg_0_M_AXIS_TLAST;
  wire aq_axis_djpeg_0_M_AXIS_TREADY;
  wire [3:0]aq_axis_djpeg_0_M_AXIS_TSTRB;
  wire [47:0]aq_axis_djpeg_0_M_AXIS_TUSER;
  wire aq_axis_djpeg_0_M_AXIS_TVALID;
  wire m00_axis_aclk_0_1;
  wire m_axis_aclk_0_1;
  wire [31:0]pixelReOrder_0_M00_AXIS_TDATA;
  wire pixelReOrder_0_M00_AXIS_TLAST;
  wire pixelReOrder_0_M00_AXIS_TREADY;
  wire [3:0]pixelReOrder_0_M00_AXIS_TSTRB;
  wire pixelReOrder_0_M00_AXIS_TVALID;
  wire s00_axis_aresetn_0_1;

  assign M_AXIS_TLAST_0 = aq_axis_djpeg_0_M_AXIS_TLAST;
  assign S_AXIS_0_1_TDATA = S_AXIS_1_tdata[31:0];
  assign S_AXIS_0_1_TKEEP = S_AXIS_1_tkeep;
  assign S_AXIS_0_1_TLAST = S_AXIS_1_tlast;
  assign S_AXIS_0_1_TSTRB = S_AXIS_1_tstrb[3:0];
  assign S_AXIS_0_1_TVALID = S_AXIS_1_tvalid;
  assign S_AXIS_1_tready = S_AXIS_0_1_TREADY;
  assign S_AXI_0_1_ARADDR = S_AXI_1_araddr[31:0];
  assign S_AXI_0_1_ARCACHE = S_AXI_1_arcache[3:0];
  assign S_AXI_0_1_ARPROT = S_AXI_1_arprot[2:0];
  assign S_AXI_0_1_ARVALID = S_AXI_1_arvalid;
  assign S_AXI_0_1_AWADDR = S_AXI_1_awaddr[31:0];
  assign S_AXI_0_1_AWCACHE = S_AXI_1_awcache[3:0];
  assign S_AXI_0_1_AWPROT = S_AXI_1_awprot[2:0];
  assign S_AXI_0_1_AWVALID = S_AXI_1_awvalid;
  assign S_AXI_0_1_BREADY = S_AXI_1_bready;
  assign S_AXI_0_1_RREADY = S_AXI_1_rready;
  assign S_AXI_0_1_WDATA = S_AXI_1_wdata[31:0];
  assign S_AXI_0_1_WSTRB = S_AXI_1_wstrb[3:0];
  assign S_AXI_0_1_WVALID = S_AXI_1_wvalid;
  assign S_AXI_1_arready = S_AXI_0_1_ARREADY;
  assign S_AXI_1_awready = S_AXI_0_1_AWREADY;
  assign S_AXI_1_bresp[1:0] = S_AXI_0_1_BRESP;
  assign S_AXI_1_bvalid = S_AXI_0_1_BVALID;
  assign S_AXI_1_rdata[31:0] = S_AXI_0_1_RDATA;
  assign S_AXI_1_rresp[1:0] = S_AXI_0_1_RRESP;
  assign S_AXI_1_rvalid = S_AXI_0_1_RVALID;
  assign S_AXI_1_wready = S_AXI_0_1_WREADY;
  assign m00_axis_aclk_0_1 = m00_axis_aclk_0;
  assign m_axis_aclk_0_1 = m_axis_aclk_0;
  assign s00_axis_aresetn_0_1 = s00_axis_aresetn_0;
  test_bd_aq_axis_djpeg_0_0 aq_axis_djpeg_0
       (.ACLK(m00_axis_aclk_0_1),
        .ARESETN(s00_axis_aresetn_0_1),
        .M_AXIS_TDATA(aq_axis_djpeg_0_M_AXIS_TDATA),
        .M_AXIS_TLAST(aq_axis_djpeg_0_M_AXIS_TLAST),
        .M_AXIS_TREADY(aq_axis_djpeg_0_M_AXIS_TREADY),
        .M_AXIS_TSTRB(aq_axis_djpeg_0_M_AXIS_TSTRB),
        .M_AXIS_TUSER(aq_axis_djpeg_0_M_AXIS_TUSER),
        .M_AXIS_TVALID(aq_axis_djpeg_0_M_AXIS_TVALID),
        .S_AXIS_TDATA(S_AXIS_0_1_TDATA),
        .S_AXIS_TKEEP(S_AXIS_0_1_TKEEP),
        .S_AXIS_TLAST(S_AXIS_0_1_TLAST),
        .S_AXIS_TREADY(S_AXIS_0_1_TREADY),
        .S_AXIS_TSTRB(S_AXIS_0_1_TSTRB),
        .S_AXIS_TVALID(S_AXIS_0_1_TVALID),
        .S_AXI_ARADDR(S_AXI_0_1_ARADDR),
        .S_AXI_ARCACHE(S_AXI_0_1_ARCACHE),
        .S_AXI_ARPROT(S_AXI_0_1_ARPROT),
        .S_AXI_ARREADY(S_AXI_0_1_ARREADY),
        .S_AXI_ARVALID(S_AXI_0_1_ARVALID),
        .S_AXI_AWADDR(S_AXI_0_1_AWADDR),
        .S_AXI_AWCACHE(S_AXI_0_1_AWCACHE),
        .S_AXI_AWPROT(S_AXI_0_1_AWPROT),
        .S_AXI_AWREADY(S_AXI_0_1_AWREADY),
        .S_AXI_AWVALID(S_AXI_0_1_AWVALID),
        .S_AXI_BREADY(S_AXI_0_1_BREADY),
        .S_AXI_BRESP(S_AXI_0_1_BRESP),
        .S_AXI_BVALID(S_AXI_0_1_BVALID),
        .S_AXI_RDATA(S_AXI_0_1_RDATA),
        .S_AXI_RREADY(S_AXI_0_1_RREADY),
        .S_AXI_RRESP(S_AXI_0_1_RRESP),
        .S_AXI_RVALID(S_AXI_0_1_RVALID),
        .S_AXI_WDATA(S_AXI_0_1_WDATA),
        .S_AXI_WREADY(S_AXI_0_1_WREADY),
        .S_AXI_WSTRB(S_AXI_0_1_WSTRB),
        .S_AXI_WVALID(S_AXI_0_1_WVALID),
        .TCLK(m00_axis_aclk_0_1));
  test_bd_axis_data_fifo_0_0 axis_data_fifo_0
       (.m_axis_aclk(m_axis_aclk_0_1),
        .m_axis_tready(1'b1),
        .s_axis_aclk(m00_axis_aclk_0_1),
        .s_axis_aresetn(s00_axis_aresetn_0_1),
        .s_axis_tdata(pixelReOrder_0_M00_AXIS_TDATA),
        .s_axis_tlast(pixelReOrder_0_M00_AXIS_TLAST),
        .s_axis_tready(pixelReOrder_0_M00_AXIS_TREADY),
        .s_axis_tstrb(pixelReOrder_0_M00_AXIS_TSTRB),
        .s_axis_tvalid(pixelReOrder_0_M00_AXIS_TVALID));
  test_bd_pixelReOrder_0_0 pixelReOrder_0
       (.m00_axis_aclk(m00_axis_aclk_0_1),
        .m00_axis_aresetn(s00_axis_aresetn_0_1),
        .m00_axis_tdata(pixelReOrder_0_M00_AXIS_TDATA),
        .m00_axis_tlast(pixelReOrder_0_M00_AXIS_TLAST),
        .m00_axis_tready(pixelReOrder_0_M00_AXIS_TREADY),
        .m00_axis_tstrb(pixelReOrder_0_M00_AXIS_TSTRB),
        .m00_axis_tvalid(pixelReOrder_0_M00_AXIS_TVALID),
        .s00_axis_aclk(m00_axis_aclk_0_1),
        .s00_axis_aresetn(s00_axis_aresetn_0_1),
        .s00_axis_tdata(aq_axis_djpeg_0_M_AXIS_TDATA),
        .s00_axis_tlast(1'b0),
        .s00_axis_tready(aq_axis_djpeg_0_M_AXIS_TREADY),
        .s00_axis_tstrb(aq_axis_djpeg_0_M_AXIS_TSTRB),
        .s00_axis_tuser(aq_axis_djpeg_0_M_AXIS_TUSER),
        .s00_axis_tvalid(aq_axis_djpeg_0_M_AXIS_TVALID));
endmodule
