#ifndef __MAIN_H_
#define __MAIN_H_

	#include "xaxidma.h"
	#include "dma.h"


	#define A_STATUS				0x0
	#define A_SIZE	    			0x4
	#define A_PIXEL	    			0x8

	#define A_STATUS_Decoder_Reset 	    (0x1 << 31)
	#define A_STATUS_Normal_Operation 	(0x0 << 31)

	#define DECODER_BUSY	(Xil_In32(XPAR_JPEG_NEKOMONA_0_BASEADDR + A_STATUS) & 0x1) == 0
	#define START_JPEG 		Xil_Out32(XPAR_JPEG_NEKOMONA_0_BASEADDR + A_STATUS, A_STATUS_Decoder_Reset);	\
							Xil_Out32(XPAR_JPEG_NEKOMONA_0_BASEADDR + A_STATUS, A_STATUS_Normal_Operation);

	extern uint32_t buff_image[];
	extern size_t sizeBufImage;
	extern XAxiDma AxiDma;

#endif
