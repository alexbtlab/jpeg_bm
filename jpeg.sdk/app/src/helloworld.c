#include <stdio.h>
#include "main.h"
#include "platform.h"
#include "xil_printf.h"
#include "xil_io.h"
#include "xil_types.h"
#include "stdbool.h"
#include "xparameters.h"
#include "dma.h"

int main()
{
	init_platform();

    if( init_dma(&AxiDma, DMA_DEV_ID) != XST_SUCCESS )
    	xil_printf("STATUS init_dma: XST_FAILURE\n");

    if(	dmaTransfer() != XST_SUCCESS )
    	xil_printf("STATUS dmaTransfer: XST_FAILURE\n");

    xil_printf("sizeBufImage:%d KiB\n", sizeBufImage / 1024);

    while(true){

			while(	DECODER_BUSY	);										// Wait for JPEGdecoder
			START_JPEG														// if JPEGDecoder is IDLE when send START signal
			startDmaTransfer_MM2S( (uint32_t)buff_image, sizeBufImage );	// Start Transfer
			xil_printf("send Jpeg to Decoder done\n");
    }
}
