#ifndef __DMA_H_
#define __DMA_H_

#include "platform_config.h"
#include "xaxidma.h"

	#define MAX_PKT_LEN				0x20
	#define TEST_START_VALUE		0xC
	#define DDR_BASE_ADDR 			XPAR_PS7_DDR_0_S_AXI_BASEADDR
	#define TX_BUFFER_BASE			(MEM_BASE_ADDR + 0x00100000)
	#define RX_BUFFER_BASE			(MEM_BASE_ADDR + 0x00300000)
	#define RX_BUFFER_HIGH			(MEM_BASE_ADDR + 0x004FFFFF)
	#define MEM_BASE_ADDR			(DDR_BASE_ADDR + 0x1000000)
	#define NUMBER_OF_TRANSFERS		10
	#define DMA_DEV_ID				XPAR_AXIDMA_1_DEVICE_ID
	#define RESET_TIMEOUT_COUNTER 	10000
	#define S2MM_DMACR_OFFSET     	0x30
	#define S2MM_DMASR_OFFSET  		0x34
	#define S2MM_DA_OFFSET    		0x48
	#define S2MM_LENGTH_OFFSET   	0x58
	#define MM2S_DMACR_OFFSET    	0x0
	#define M2SS_DMASR_OFFSET		0x4
	#define MM2S_SA_OFFSET    		0x18
	#define MM2S_LENGTH_OFFSET		0x28

	int dmaTransfer();
	int init_dma(XAxiDma* p_dma_inst, int dma_device_id);
	void startDmaTransfer_S2MM(unsigned int dstAddr, unsigned int len);
	void startDmaTransfer_MM2S(uint32_t srcAddr, unsigned int len);
	void InitializeAxiDMA(u8 enable_DMA);

#endif
